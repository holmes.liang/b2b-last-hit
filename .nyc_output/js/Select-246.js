__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-util/es/KeyCode */ "./node_modules/rc-util/es/KeyCode.js");
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! shallowequal */ "./node_modules/shallowequal/index.js");
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(shallowequal__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! raf */ "./node_modules/raf/index.js");
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(raf__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! dom-scroll-into-view */ "./node_modules/dom-scroll-into-view/lib/index.js");
/* harmony import */ var dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _SelectTrigger__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./SelectTrigger */ "./node_modules/rc-tree-select/es/SelectTrigger.js");
/* harmony import */ var _Base_BaseSelector__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./Base/BaseSelector */ "./node_modules/rc-tree-select/es/Base/BaseSelector.js");
/* harmony import */ var _Base_BasePopup__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./Base/BasePopup */ "./node_modules/rc-tree-select/es/Base/BasePopup.js");
/* harmony import */ var _Selector_SingleSelector__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./Selector/SingleSelector */ "./node_modules/rc-tree-select/es/Selector/SingleSelector.js");
/* harmony import */ var _Selector_MultipleSelector__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./Selector/MultipleSelector */ "./node_modules/rc-tree-select/es/Selector/MultipleSelector/index.js");
/* harmony import */ var _Popup_SinglePopup__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./Popup/SinglePopup */ "./node_modules/rc-tree-select/es/Popup/SinglePopup.js");
/* harmony import */ var _Popup_MultiplePopup__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./Popup/MultiplePopup */ "./node_modules/rc-tree-select/es/Popup/MultiplePopup.js");
/* harmony import */ var _strategies__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./strategies */ "./node_modules/rc-tree-select/es/strategies.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./util */ "./node_modules/rc-tree-select/es/util.js");
/* harmony import */ var _propTypes__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./propTypes */ "./node_modules/rc-tree-select/es/propTypes.js");
/* harmony import */ var _SelectNode__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./SelectNode */ "./node_modules/rc-tree-select/es/SelectNode.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}
/**
 * ARIA: https://www.w3.org/TR/wai-aria/#combobox
 * Sample 1: https://www.w3.org/TR/2017/NOTE-wai-aria-practices-1.1-20171214/examples/combobox/aria1.1pattern/listbox-combo.html
 * Sample 2: https://www.w3.org/blog/wai-components-gallery/widget/combobox-with-aria-autocompleteinline/
 *
 * Tab logic:
 * Popup is close
 * 1. Focus input (mark component as focused)
 * 2. Press enter to show the popup
 * 3. If popup has input, focus it
 *
 * Popup is open
 * 1. press tab to close the popup
 * 2. Focus back to the selection input box
 * 3. Let the native tab going on
 *
 * TreeSelect use 2 design type.
 * In single mode, we should focus on the `span`
 * In multiple mode, we should focus on the `input`
 */






















var Select =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Select, _React$Component);

  function Select(_props) {
    var _this;

    _classCallCheck(this, Select);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Select).call(this, _props));

    _defineProperty(_assertThisInitialized(_this), "onSelectorFocus", function () {
      _this.setState({
        focused: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onSelectorBlur", function () {
      _this.setState({
        focused: false
      }); // TODO: Close when Popup is also not focused
      // this.setState({ open: false });

    });

    _defineProperty(_assertThisInitialized(_this), "onComponentKeyDown", function (event) {
      var open = _this.state.open;
      var keyCode = event.keyCode;

      if (!open) {
        if ([rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_4__["default"].ENTER, rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_4__["default"].DOWN].indexOf(keyCode) !== -1) {
          _this.setOpenState(true);
        }
      } else if (rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_4__["default"].ESC === keyCode) {
        _this.setOpenState(false);
      } else if ([rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_4__["default"].UP, rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_4__["default"].DOWN, rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_4__["default"].LEFT, rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_4__["default"].RIGHT].indexOf(keyCode) !== -1) {
        // TODO: Handle `open` state
        event.stopPropagation();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onDeselect", function (wrappedValue, node, nodeEventInfo) {
      var onDeselect = _this.props.onDeselect;
      if (!onDeselect) return;
      onDeselect(wrappedValue, node, nodeEventInfo);
    });

    _defineProperty(_assertThisInitialized(_this), "onSelectorClear", function (event) {
      var disabled = _this.props.disabled;
      if (disabled) return;

      _this.triggerChange([], []);

      if (!_this.isSearchValueControlled()) {
        _this.setUncontrolledState({
          searchValue: '',
          filteredTreeNodes: null
        });
      }

      event.stopPropagation();
    });

    _defineProperty(_assertThisInitialized(_this), "onMultipleSelectorRemove", function (event, removeValue) {
      event.stopPropagation();
      var _this$state = _this.state,
          valueList = _this$state.valueList,
          missValueList = _this$state.missValueList,
          valueEntities = _this$state.valueEntities;
      var _this$props = _this.props,
          treeCheckable = _this$props.treeCheckable,
          treeCheckStrictly = _this$props.treeCheckStrictly,
          treeNodeLabelProp = _this$props.treeNodeLabelProp,
          disabled = _this$props.disabled;
      if (disabled) return; // Find trigger entity

      var triggerEntity = valueEntities[removeValue]; // Clean up value

      var newValueList = valueList;

      if (triggerEntity) {
        // If value is in tree
        if (treeCheckable && !treeCheckStrictly) {
          newValueList = valueList.filter(function (_ref) {
            var value = _ref.value;
            var entity = valueEntities[value];
            return !Object(_util__WEBPACK_IMPORTED_MODULE_16__["isPosRelated"])(entity.pos, triggerEntity.pos);
          });
        } else {
          newValueList = valueList.filter(function (_ref2) {
            var value = _ref2.value;
            return value !== removeValue;
          });
        }
      }

      var triggerNode = triggerEntity ? triggerEntity.node : null;
      var extraInfo = {
        triggerValue: removeValue,
        triggerNode: triggerNode
      };
      var deselectInfo = {
        node: triggerNode
      }; // [Legacy] Little hack on this to make same action as `onCheck` event.

      if (treeCheckable) {
        var filteredEntityList = newValueList.map(function (_ref3) {
          var value = _ref3.value;
          return valueEntities[value];
        });
        deselectInfo.event = 'check';
        deselectInfo.checked = false;
        deselectInfo.checkedNodes = filteredEntityList.map(function (_ref4) {
          var node = _ref4.node;
          return node;
        });
        deselectInfo.checkedNodesPositions = filteredEntityList.map(function (_ref5) {
          var node = _ref5.node,
              pos = _ref5.pos;
          return {
            node: node,
            pos: pos
          };
        });

        if (treeCheckStrictly) {
          extraInfo.allCheckedNodes = deselectInfo.checkedNodes;
        } else {
          // TODO: It's too expansive to get `halfCheckedKeys` in onDeselect. Not pass this.
          extraInfo.allCheckedNodes = Object(_util__WEBPACK_IMPORTED_MODULE_16__["flatToHierarchy"])(filteredEntityList).map(function (_ref6) {
            var node = _ref6.node;
            return node;
          });
        }
      } else {
        deselectInfo.event = 'select';
        deselectInfo.selected = false;
        deselectInfo.selectedNodes = newValueList.map(function (_ref7) {
          var value = _ref7.value;
          return (valueEntities[value] || {}).node;
        });
      } // Some value user pass prop is not in the tree, we also need clean it


      var newMissValueList = missValueList.filter(function (_ref8) {
        var value = _ref8.value;
        return value !== removeValue;
      });
      var wrappedValue;

      if (_this.isLabelInValue()) {
        wrappedValue = {
          label: triggerNode ? triggerNode.props[treeNodeLabelProp] : null,
          value: removeValue
        };
      } else {
        wrappedValue = removeValue;
      }

      _this.onDeselect(wrappedValue, triggerNode, deselectInfo);

      _this.triggerChange(newMissValueList, newValueList, extraInfo);
    });

    _defineProperty(_assertThisInitialized(_this), "onValueTrigger", function (isAdd, nodeList, nodeEventInfo, nodeExtraInfo) {
      var node = nodeEventInfo.node;
      var value = node.props.value;
      var _this$state2 = _this.state,
          missValueList = _this$state2.missValueList,
          valueEntities = _this$state2.valueEntities,
          keyEntities = _this$state2.keyEntities,
          searchValue = _this$state2.searchValue;
      var _this$props2 = _this.props,
          disabled = _this$props2.disabled,
          inputValue = _this$props2.inputValue,
          treeNodeLabelProp = _this$props2.treeNodeLabelProp,
          onSelect = _this$props2.onSelect,
          onSearch = _this$props2.onSearch,
          multiple = _this$props2.multiple,
          treeCheckable = _this$props2.treeCheckable,
          treeCheckStrictly = _this$props2.treeCheckStrictly,
          autoClearSearchValue = _this$props2.autoClearSearchValue;
      var label = node.props[treeNodeLabelProp];
      if (disabled) return; // Wrap the return value for user

      var wrappedValue;

      if (_this.isLabelInValue()) {
        wrappedValue = {
          value: value,
          label: label
        };
      } else {
        wrappedValue = value;
      } // [Legacy] Origin code not trigger `onDeselect` every time. Let's align the behaviour.


      if (isAdd) {
        if (onSelect) {
          onSelect(wrappedValue, node, nodeEventInfo);
        }
      } else {
        _this.onDeselect(wrappedValue, node, nodeEventInfo);
      } // Get wrapped value list.
      // This is a bit hack cause we use key to match the value.


      var newValueList = nodeList.map(function (_ref9) {
        var props = _ref9.props;
        return {
          value: props.value,
          label: props[treeNodeLabelProp]
        };
      }); // When is `treeCheckable` and with `searchValue`, `valueList` is not full filled.
      // We need calculate the missing nodes.

      if (treeCheckable && !treeCheckStrictly) {
        var keyList = newValueList.map(function (_ref10) {
          var val = _ref10.value;
          return valueEntities[val].key;
        });

        if (isAdd) {
          keyList = Object(_util__WEBPACK_IMPORTED_MODULE_16__["conductCheck"])(keyList, true, keyEntities).checkedKeys;
        } else {
          keyList = Object(_util__WEBPACK_IMPORTED_MODULE_16__["conductCheck"])([valueEntities[value].key], false, keyEntities, {
            checkedKeys: keyList
          }).checkedKeys;
        }

        newValueList = keyList.map(function (key) {
          var props = keyEntities[key].node.props;
          return {
            value: props.value,
            label: props[treeNodeLabelProp]
          };
        });
      } // Clean up `searchValue` when this prop is set


      if (autoClearSearchValue || inputValue === null) {
        // Clean state `searchValue` if uncontrolled
        if (!_this.isSearchValueControlled() && (multiple || treeCheckable)) {
          _this.setUncontrolledState({
            searchValue: '',
            filteredTreeNodes: null
          });
        } // Trigger onSearch if `searchValue` to be empty.
        // We should also trigger onSearch with empty string here
        // since if user use `treeExpandedKeys`, it need user have the ability to reset it.


        if (onSearch && searchValue && searchValue.length) {
          onSearch('');
        }
      } // [Legacy] Provide extra info


      var extraInfo = _objectSpread({}, nodeExtraInfo, {
        triggerValue: value,
        triggerNode: node
      });

      _this.triggerChange(missValueList, newValueList, extraInfo);
    });

    _defineProperty(_assertThisInitialized(_this), "onTreeNodeSelect", function (_, nodeEventInfo) {
      var _this$state3 = _this.state,
          valueList = _this$state3.valueList,
          valueEntities = _this$state3.valueEntities;
      var _this$props3 = _this.props,
          treeCheckable = _this$props3.treeCheckable,
          multiple = _this$props3.multiple;
      if (treeCheckable) return;

      if (!multiple) {
        _this.setOpenState(false);
      }

      var isAdd = nodeEventInfo.selected;
      var selectedValue = nodeEventInfo.node.props.value;
      var newValueList;

      if (!multiple) {
        newValueList = [{
          value: selectedValue
        }];
      } else {
        newValueList = valueList.filter(function (_ref11) {
          var value = _ref11.value;
          return value !== selectedValue;
        });

        if (isAdd) {
          newValueList.push({
            value: selectedValue
          });
        }
      }

      var selectedNodes = newValueList.map(function (_ref12) {
        var value = _ref12.value;
        return valueEntities[value];
      }).filter(function (entity) {
        return entity;
      }).map(function (_ref13) {
        var node = _ref13.node;
        return node;
      });

      _this.onValueTrigger(isAdd, selectedNodes, nodeEventInfo, {
        selected: isAdd
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onTreeNodeCheck", function (_, nodeEventInfo) {
      var _this$state4 = _this.state,
          searchValue = _this$state4.searchValue,
          keyEntities = _this$state4.keyEntities,
          valueEntities = _this$state4.valueEntities,
          valueList = _this$state4.valueList;
      var treeCheckStrictly = _this.props.treeCheckStrictly;
      var checkedNodes = nodeEventInfo.checkedNodes,
          checkedNodesPositions = nodeEventInfo.checkedNodesPositions;
      var isAdd = nodeEventInfo.checked;
      var extraInfo = {
        checked: isAdd
      };
      var checkedNodeList = checkedNodes; // [Legacy] Check event provide `allCheckedNodes`.
      // When `treeCheckStrictly` or internal `searchValue` is set, TreeNode will be unrelated:
      // - Related: Show the top checked nodes and has children prop.
      // - Unrelated: Show all the checked nodes.

      if (searchValue) {
        var oriKeyList = valueList.map(function (_ref14) {
          var value = _ref14.value;
          return valueEntities[value];
        }).filter(function (entity) {
          return entity;
        }).map(function (_ref15) {
          var key = _ref15.key;
          return key;
        });
        var keyList;

        if (isAdd) {
          keyList = Array.from(new Set([].concat(_toConsumableArray(oriKeyList), _toConsumableArray(checkedNodeList.map(function (_ref16) {
            var value = _ref16.props.value;
            return valueEntities[value].key;
          })))));
        } else {
          keyList = Object(_util__WEBPACK_IMPORTED_MODULE_16__["conductCheck"])([nodeEventInfo.node.props.eventKey], false, keyEntities, {
            checkedKeys: oriKeyList
          }).checkedKeys;
        }

        checkedNodeList = keyList.map(function (key) {
          return keyEntities[key].node;
        }); // Let's follow as not `treeCheckStrictly` format

        extraInfo.allCheckedNodes = keyList.map(function (key) {
          return Object(_util__WEBPACK_IMPORTED_MODULE_16__["cleanEntity"])(keyEntities[key]);
        });
      } else if (treeCheckStrictly) {
        extraInfo.allCheckedNodes = nodeEventInfo.checkedNodes;
      } else {
        extraInfo.allCheckedNodes = Object(_util__WEBPACK_IMPORTED_MODULE_16__["flatToHierarchy"])(checkedNodesPositions);
      }

      _this.onValueTrigger(isAdd, checkedNodeList, nodeEventInfo, extraInfo);
    });

    _defineProperty(_assertThisInitialized(_this), "onDropdownVisibleChange", function (open) {
      var _this$props4 = _this.props,
          multiple = _this$props4.multiple,
          treeCheckable = _this$props4.treeCheckable;
      var searchValue = _this.state.searchValue; // When set open success and single mode,
      // we will reset the input content.

      if (open && !multiple && !treeCheckable && searchValue) {
        _this.setUncontrolledState({
          searchValue: '',
          filteredTreeNodes: null
        });
      }

      _this.setOpenState(open, true);
    });

    _defineProperty(_assertThisInitialized(_this), "onSearchInputChange", function (_ref17) {
      var value = _ref17.target.value;
      var _this$state5 = _this.state,
          treeNodes = _this$state5.treeNodes,
          valueEntities = _this$state5.valueEntities;
      var _this$props5 = _this.props,
          onSearch = _this$props5.onSearch,
          filterTreeNode = _this$props5.filterTreeNode,
          treeNodeFilterProp = _this$props5.treeNodeFilterProp;

      if (onSearch) {
        onSearch(value);
      }

      var isSet = false;

      if (!_this.isSearchValueControlled()) {
        isSet = _this.setUncontrolledState({
          searchValue: value
        });

        _this.setOpenState(true);
      }

      if (isSet) {
        // Do the search logic
        var upperSearchValue = String(value).toUpperCase();
        var filterTreeNodeFn = filterTreeNode;

        if (filterTreeNode === false) {
          // Don't filter if is false
          filterTreeNodeFn = function filterTreeNodeFn() {
            return true;
          };
        } else if (typeof filterTreeNodeFn !== 'function') {
          // When is not function (true or undefined), use inner filter
          filterTreeNodeFn = function filterTreeNodeFn(_, node) {
            var nodeValue = String(node.props[treeNodeFilterProp]).toUpperCase();
            return nodeValue.indexOf(upperSearchValue) !== -1;
          };
        }

        _this.setState({
          filteredTreeNodes: Object(_util__WEBPACK_IMPORTED_MODULE_16__["getFilterTree"])(treeNodes, value, filterTreeNodeFn, valueEntities, _SelectNode__WEBPACK_IMPORTED_MODULE_18__["default"])
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onSearchInputKeyDown", function (event) {
      var _this$state6 = _this.state,
          searchValue = _this$state6.searchValue,
          valueList = _this$state6.valueList;
      var keyCode = event.keyCode;

      if (rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_4__["default"].BACKSPACE === keyCode && _this.isMultiple() && !searchValue && valueList.length) {
        var lastValue = valueList[valueList.length - 1].value;

        _this.onMultipleSelectorRemove(event, lastValue);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onChoiceAnimationLeave", function () {
      raf__WEBPACK_IMPORTED_MODULE_6___default()(function () {
        _this.forcePopupAlign();
      });
    });

    _defineProperty(_assertThisInitialized(_this), "setPopupRef", function (popup) {
      _this.popup = popup;
    });

    _defineProperty(_assertThisInitialized(_this), "setUncontrolledState", function (state) {
      var needSync = false;
      var newState = {};
      Object.keys(state).forEach(function (name) {
        if (name in _this.props) return;
        needSync = true;
        newState[name] = state[name];
      });

      if (needSync) {
        _this.setState(newState);
      }

      return needSync;
    });

    _defineProperty(_assertThisInitialized(_this), "setOpenState", function (open) {
      var byTrigger = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var onDropdownVisibleChange = _this.props.onDropdownVisibleChange;

      if (onDropdownVisibleChange && onDropdownVisibleChange(open, {
        documentClickClose: !open && byTrigger
      }) === false) {
        return;
      }

      _this.setUncontrolledState({
        open: open
      });
    });

    _defineProperty(_assertThisInitialized(_this), "isMultiple", function () {
      var _this$props6 = _this.props,
          multiple = _this$props6.multiple,
          treeCheckable = _this$props6.treeCheckable;
      return !!(multiple || treeCheckable);
    });

    _defineProperty(_assertThisInitialized(_this), "isLabelInValue", function () {
      return Object(_util__WEBPACK_IMPORTED_MODULE_16__["isLabelInValue"])(_this.props);
    });

    _defineProperty(_assertThisInitialized(_this), "isSearchValueControlled", function () {
      var inputValue = _this.props.inputValue;
      if ('searchValue' in _this.props) return true;
      return 'inputValue' in _this.props && inputValue !== null;
    });

    _defineProperty(_assertThisInitialized(_this), "forcePopupAlign", function () {
      var $trigger = _this.selectTriggerRef.current;

      if ($trigger) {
        $trigger.forcePopupAlign();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "delayForcePopupAlign", function () {
      // Wait 2 frame to avoid dom update & dom algin in the same time
      // https://github.com/ant-design/ant-design/issues/12031
      raf__WEBPACK_IMPORTED_MODULE_6___default()(function () {
        raf__WEBPACK_IMPORTED_MODULE_6___default()(_this.forcePopupAlign);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "triggerChange", function (missValueList, valueList) {
      var extraInfo = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      var _this$state7 = _this.state,
          valueEntities = _this$state7.valueEntities,
          searchValue = _this$state7.searchValue,
          prevSelectorValueList = _this$state7.selectorValueList;
      var _this$props7 = _this.props,
          onChange = _this$props7.onChange,
          disabled = _this$props7.disabled,
          treeCheckable = _this$props7.treeCheckable,
          treeCheckStrictly = _this$props7.treeCheckStrictly;
      if (disabled) return; // Trigger

      var extra = _objectSpread({
        // [Legacy] Always return as array contains label & value
        preValue: prevSelectorValueList.map(function (_ref18) {
          var label = _ref18.label,
              value = _ref18.value;
          return {
            label: label,
            value: value
          };
        })
      }, extraInfo); // Format value by `treeCheckStrictly`


      var selectorValueList = Object(_util__WEBPACK_IMPORTED_MODULE_16__["formatSelectorValue"])(valueList, _this.props, valueEntities);

      if (!('value' in _this.props)) {
        var newState = {
          missValueList: missValueList,
          valueList: valueList,
          selectorValueList: selectorValueList
        };

        if (searchValue && treeCheckable && !treeCheckStrictly) {
          newState.searchHalfCheckedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_16__["getHalfCheckedKeys"])(valueList, valueEntities);
        }

        _this.setState(newState);
      } // Only do the logic when `onChange` function provided


      if (onChange) {
        var connectValueList; // Get value by mode

        if (_this.isMultiple()) {
          connectValueList = [].concat(_toConsumableArray(missValueList), _toConsumableArray(selectorValueList));
        } else {
          connectValueList = selectorValueList.slice(0, 1);
        }

        var labelList = null;
        var returnValue;

        if (_this.isLabelInValue()) {
          returnValue = connectValueList.map(function (_ref19) {
            var label = _ref19.label,
                value = _ref19.value;
            return {
              label: label,
              value: value
            };
          });
        } else {
          labelList = [];
          returnValue = connectValueList.map(function (_ref20) {
            var label = _ref20.label,
                value = _ref20.value;
            labelList.push(label);
            return value;
          });
        }

        if (!_this.isMultiple()) {
          returnValue = returnValue[0];
        }

        onChange(returnValue, labelList, extra);
      }
    });

    var prefixAria = _props.prefixAria,
        defaultOpen = _props.defaultOpen,
        _open = _props.open;
    _this.state = {
      open: _open || defaultOpen,
      valueList: [],
      searchHalfCheckedKeys: [],
      missValueList: [],
      // Contains the value not in the tree
      selectorValueList: [],
      // Used for multiple selector
      valueEntities: {},
      keyEntities: {},
      searchValue: '',
      init: true
    };
    _this.selectorRef = Object(_util__WEBPACK_IMPORTED_MODULE_16__["createRef"])();
    _this.selectTriggerRef = Object(_util__WEBPACK_IMPORTED_MODULE_16__["createRef"])(); // ARIA need `aria-controls` props mapping
    // Since this need user input. Let's generate ourselves

    _this.ariaId = Object(_util__WEBPACK_IMPORTED_MODULE_16__["generateAriaId"])("".concat(prefixAria, "-list"));
    return _this;
  }

  _createClass(Select, [{
    key: "getChildContext",
    value: function getChildContext() {
      return {
        rcTreeSelect: {
          onSelectorFocus: this.onSelectorFocus,
          onSelectorBlur: this.onSelectorBlur,
          onSelectorKeyDown: this.onComponentKeyDown,
          onSelectorClear: this.onSelectorClear,
          onMultipleSelectorRemove: this.onMultipleSelectorRemove,
          onTreeNodeSelect: this.onTreeNodeSelect,
          onTreeNodeCheck: this.onTreeNodeCheck,
          onPopupKeyDown: this.onComponentKeyDown,
          onSearchInputChange: this.onSearchInputChange,
          onSearchInputKeyDown: this.onSearchInputKeyDown
        }
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props8 = this.props,
          autoFocus = _this$props8.autoFocus,
          disabled = _this$props8.disabled;

      if (autoFocus && !disabled) {
        this.focus();
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(_, prevState) {
      var _this2 = this;

      var prefixCls = this.props.prefixCls;
      var _this$state8 = this.state,
          valueList = _this$state8.valueList,
          open = _this$state8.open,
          selectorValueList = _this$state8.selectorValueList,
          valueEntities = _this$state8.valueEntities;
      var isMultiple = this.isMultiple();

      if (prevState.valueList !== valueList) {
        this.forcePopupAlign();
      } // Scroll to value position, only need sync on single mode


      if (!isMultiple && selectorValueList.length && !prevState.open && open && this.popup) {
        var value = selectorValueList[0].value;

        var _this$popup$getTree = this.popup.getTree(),
            domTreeNodes = _this$popup$getTree.domTreeNodes;

        var _ref21 = valueEntities[value] || {},
            key = _ref21.key;

        var treeNode = domTreeNodes[key];

        if (treeNode) {
          var domNode = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(treeNode);
          raf__WEBPACK_IMPORTED_MODULE_6___default()(function () {
            var popupNode = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(_this2.popup);
            var triggerContainer = Object(_util__WEBPACK_IMPORTED_MODULE_16__["findPopupContainer"])(popupNode, "".concat(prefixCls, "-dropdown"));
            var searchNode = _this2.popup.searchRef.current;

            if (domNode && triggerContainer && searchNode) {
              dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_7___default()(domNode, triggerContainer, {
                onlyScrollIfNeeded: true,
                offsetTop: searchNode.offsetHeight
              });
            }
          });
        }
      }
    } // ==================== Selector ====================

  }, {
    key: "focus",
    value: function focus() {
      this.selectorRef.current.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.selectorRef.current.blur();
    } // ===================== Render =====================

  }, {
    key: "render",
    value: function render() {
      var _this$state9 = this.state,
          valueList = _this$state9.valueList,
          missValueList = _this$state9.missValueList,
          selectorValueList = _this$state9.selectorValueList,
          searchHalfCheckedKeys = _this$state9.searchHalfCheckedKeys,
          valueEntities = _this$state9.valueEntities,
          keyEntities = _this$state9.keyEntities,
          searchValue = _this$state9.searchValue,
          open = _this$state9.open,
          focused = _this$state9.focused,
          treeNodes = _this$state9.treeNodes,
          filteredTreeNodes = _this$state9.filteredTreeNodes;
      var _this$props9 = this.props,
          prefixCls = _this$props9.prefixCls,
          treeExpandedKeys = _this$props9.treeExpandedKeys,
          onTreeExpand = _this$props9.onTreeExpand;
      var isMultiple = this.isMultiple();

      var passProps = _objectSpread({}, this.props, {
        isMultiple: isMultiple,
        valueList: valueList,
        searchHalfCheckedKeys: searchHalfCheckedKeys,
        selectorValueList: [].concat(_toConsumableArray(missValueList), _toConsumableArray(selectorValueList)),
        valueEntities: valueEntities,
        keyEntities: keyEntities,
        searchValue: searchValue,
        upperSearchValue: (searchValue || '').toUpperCase(),
        // Perf save
        open: open,
        focused: focused,
        onChoiceAnimationLeave: this.onChoiceAnimationLeave,
        dropdownPrefixCls: "".concat(prefixCls, "-dropdown"),
        ariaId: this.ariaId
      });

      var Popup = isMultiple ? _Popup_MultiplePopup__WEBPACK_IMPORTED_MODULE_14__["default"] : _Popup_SinglePopup__WEBPACK_IMPORTED_MODULE_13__["default"];
      var $popup = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Popup, _extends({
        ref: this.setPopupRef
      }, passProps, {
        onTreeExpanded: this.delayForcePopupAlign,
        treeNodes: treeNodes,
        filteredTreeNodes: filteredTreeNodes // Tree expanded control
        ,
        treeExpandedKeys: treeExpandedKeys,
        onTreeExpand: onTreeExpand
      }));
      var Selector = isMultiple ? _Selector_MultipleSelector__WEBPACK_IMPORTED_MODULE_12__["default"] : _Selector_SingleSelector__WEBPACK_IMPORTED_MODULE_11__["default"];
      var $selector = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Selector, _extends({}, passProps, {
        ref: this.selectorRef
      }));
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SelectTrigger__WEBPACK_IMPORTED_MODULE_8__["default"], _extends({}, passProps, {
        ref: this.selectTriggerRef,
        popupElement: $popup,
        onKeyDown: this.onKeyDown,
        onDropdownVisibleChange: this.onDropdownVisibleChange
      }), $selector);
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps, prevState) {
      var _prevState$prevProps = prevState.prevProps,
          prevProps = _prevState$prevProps === void 0 ? {} : _prevState$prevProps;
      var treeCheckable = nextProps.treeCheckable,
          treeCheckStrictly = nextProps.treeCheckStrictly,
          filterTreeNode = nextProps.filterTreeNode,
          treeNodeFilterProp = nextProps.treeNodeFilterProp,
          treeDataSimpleMode = nextProps.treeDataSimpleMode;
      var newState = {
        prevProps: nextProps,
        init: false
      }; // Process the state when props updated

      function processState(propName, updater) {
        if (prevProps[propName] !== nextProps[propName]) {
          updater(nextProps[propName], prevProps[propName]);
          return true;
        }

        return false;
      }

      var valueRefresh = false; // Open

      processState('open', function (propValue) {
        newState.open = propValue;
      }); // Tree Nodes

      var treeNodes;
      var treeDataChanged = false;
      var treeDataModeChanged = false;
      processState('treeData', function (propValue) {
        treeNodes = Object(_util__WEBPACK_IMPORTED_MODULE_16__["convertDataToTree"])(propValue);
        treeDataChanged = true;
      });
      processState('treeDataSimpleMode', function (propValue, prevValue) {
        if (!propValue) return;
        var prev = !prevValue || prevValue === true ? {} : prevValue; // Shallow equal to avoid dynamic prop object

        if (!shallowequal__WEBPACK_IMPORTED_MODULE_5___default()(propValue, prev)) {
          treeDataModeChanged = true;
        }
      }); // Parse by `treeDataSimpleMode`

      if (treeDataSimpleMode && (treeDataChanged || treeDataModeChanged)) {
        var simpleMapper = _objectSpread({
          id: 'id',
          pId: 'pId',
          rootPId: null
        }, treeDataSimpleMode !== true ? treeDataSimpleMode : {});

        treeNodes = Object(_util__WEBPACK_IMPORTED_MODULE_16__["convertDataToTree"])(Object(_util__WEBPACK_IMPORTED_MODULE_16__["parseSimpleTreeData"])(nextProps.treeData, simpleMapper));
      } // If `treeData` not provide, use children TreeNodes


      if (!nextProps.treeData) {
        processState('children', function (propValue) {
          treeNodes = Array.isArray(propValue) ? propValue : [propValue];
        });
      } // Convert `treeData` to entities


      if (treeNodes) {
        var entitiesMap = Object(_util__WEBPACK_IMPORTED_MODULE_16__["convertTreeToEntities"])(treeNodes);
        newState.treeNodes = treeNodes;
        newState.posEntities = entitiesMap.posEntities;
        newState.valueEntities = entitiesMap.valueEntities;
        newState.keyEntities = entitiesMap.keyEntities;
        valueRefresh = true;
      } // Value List


      if (prevState.init) {
        processState('defaultValue', function (propValue) {
          newState.valueList = Object(_util__WEBPACK_IMPORTED_MODULE_16__["formatInternalValue"])(propValue, nextProps);
          valueRefresh = true;
        });
      }

      processState('value', function (propValue) {
        newState.valueList = Object(_util__WEBPACK_IMPORTED_MODULE_16__["formatInternalValue"])(propValue, nextProps);
        valueRefresh = true;
      }); // Selector Value List

      if (valueRefresh) {
        // Find out that value not exist in the tree
        var missValueList = [];
        var filteredValueList = [];
        var keyList = []; // Get latest value list

        var latestValueList = newState.valueList;

        if (!latestValueList) {
          // Also need add prev missValueList to avoid new treeNodes contains the value
          latestValueList = [].concat(_toConsumableArray(prevState.valueList), _toConsumableArray(prevState.missValueList));
        } // Get key by value


        var valueLabels = {};
        latestValueList.forEach(function (wrapperValue) {
          var value = wrapperValue.value,
              label = wrapperValue.label;
          var entity = (newState.valueEntities || prevState.valueEntities)[value];
          valueLabels[value] = label;

          if (entity) {
            keyList.push(entity.key);
            filteredValueList.push(wrapperValue);
            return;
          } // If not match, it may caused by ajax load. We need keep this


          missValueList.push(wrapperValue);
        }); // We need calculate the value when tree is checked tree

        if (treeCheckable && !treeCheckStrictly) {
          // Calculate the keys need to be checked
          var _conductCheck = Object(_util__WEBPACK_IMPORTED_MODULE_16__["conductCheck"])(keyList, true, newState.keyEntities || prevState.keyEntities),
              checkedKeys = _conductCheck.checkedKeys; // Format value list again for internal usage


          newState.valueList = checkedKeys.map(function (key) {
            var val = (newState.keyEntities || prevState.keyEntities)[key].value;
            var wrappedValue = {
              value: val
            };

            if (valueLabels[val] !== undefined) {
              wrappedValue.label = valueLabels[val];
            }

            return wrappedValue;
          });
        } else {
          newState.valueList = filteredValueList;
        } // Fill the missValueList, we still need display in the selector


        newState.missValueList = missValueList; // Calculate the value list for `Selector` usage

        newState.selectorValueList = Object(_util__WEBPACK_IMPORTED_MODULE_16__["formatSelectorValue"])(newState.valueList, nextProps, newState.valueEntities || prevState.valueEntities);
      } // [Legacy] To align with `Select` component,
      // We use `searchValue` instead of `inputValue` but still keep the api
      // `inputValue` support `null` to work as `autoClearSearchValue`


      processState('inputValue', function (propValue) {
        if (propValue !== null) {
          newState.searchValue = propValue;
        }
      }); // Search value

      processState('searchValue', function (propValue) {
        newState.searchValue = propValue;
      }); // Do the search logic

      if (newState.searchValue !== undefined || prevState.searchValue && treeNodes) {
        var searchValue = newState.searchValue !== undefined ? newState.searchValue : prevState.searchValue;
        var upperSearchValue = String(searchValue).toUpperCase();
        var filterTreeNodeFn = filterTreeNode;

        if (filterTreeNode === false) {
          // Don't filter if is false
          filterTreeNodeFn = function filterTreeNodeFn() {
            return true;
          };
        } else if (typeof filterTreeNodeFn !== 'function') {
          // When is not function (true or undefined), use inner filter
          filterTreeNodeFn = function filterTreeNodeFn(_, node) {
            var nodeValue = String(node.props[treeNodeFilterProp]).toUpperCase();
            return nodeValue.indexOf(upperSearchValue) !== -1;
          };
        }

        newState.filteredTreeNodes = Object(_util__WEBPACK_IMPORTED_MODULE_16__["getFilterTree"])(newState.treeNodes || prevState.treeNodes, searchValue, filterTreeNodeFn, newState.valueEntities || prevState.valueEntities, _SelectNode__WEBPACK_IMPORTED_MODULE_18__["default"]);
      } // We should re-calculate the halfCheckedKeys when in search mode


      if (valueRefresh && treeCheckable && !treeCheckStrictly && (newState.searchValue || prevState.searchValue)) {
        newState.searchHalfCheckedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_16__["getHalfCheckedKeys"])(newState.valueList, newState.valueEntities || prevState.valueEntities);
      } // Checked Strategy


      processState('showCheckedStrategy', function () {
        newState.selectorValueList = newState.selectorValueList || Object(_util__WEBPACK_IMPORTED_MODULE_16__["formatSelectorValue"])(newState.valueList || prevState.valueList, nextProps, newState.valueEntities || prevState.valueEntities);
      });
      return newState;
    }
  }]);

  return Select;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

_defineProperty(Select, "propTypes", {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  prefixAria: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  multiple: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  showArrow: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  open: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  value: _propTypes__WEBPACK_IMPORTED_MODULE_17__["valueProp"],
  autoFocus: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  defaultOpen: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  defaultValue: _propTypes__WEBPACK_IMPORTED_MODULE_17__["valueProp"],
  showSearch: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  placeholder: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node,
  inputValue: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  // [Legacy] Deprecated. Use `searchValue` instead.
  searchValue: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  autoClearSearchValue: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  searchPlaceholder: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node,
  // [Legacy] Confuse with placeholder
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  children: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node,
  labelInValue: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  maxTagCount: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.number,
  maxTagPlaceholder: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func]),
  maxTagTextLength: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.number,
  showCheckedStrategy: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.oneOf([_strategies__WEBPACK_IMPORTED_MODULE_15__["SHOW_ALL"], _strategies__WEBPACK_IMPORTED_MODULE_15__["SHOW_PARENT"], _strategies__WEBPACK_IMPORTED_MODULE_15__["SHOW_CHILD"]]),
  dropdownMatchSelectWidth: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  treeData: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.array,
  treeDataSimpleMode: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool, prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object]),
  treeNodeFilterProp: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  treeNodeLabelProp: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  treeCheckable: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool, prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node]),
  treeCheckStrictly: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  treeIcon: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  treeLine: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  treeDefaultExpandAll: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  treeDefaultExpandedKeys: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.array,
  treeExpandedKeys: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.array,
  loadData: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  filterTreeNode: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func, prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool]),
  notFoundContent: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node,
  onSearch: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  onSelect: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  onDeselect: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  onChange: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  onDropdownVisibleChange: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  onTreeExpand: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  inputIcon: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func]),
  clearIcon: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func]),
  removeIcon: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func]),
  switcherIcon: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func])
});

_defineProperty(Select, "childContextTypes", {
  rcTreeSelect: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.shape(_objectSpread({}, _Base_BaseSelector__WEBPACK_IMPORTED_MODULE_9__["selectorContextTypes"], {}, _Selector_MultipleSelector__WEBPACK_IMPORTED_MODULE_12__["multipleSelectorContextTypes"], {}, _Base_BasePopup__WEBPACK_IMPORTED_MODULE_10__["popupContextTypes"], {
    onSearchInputChange: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
    onSearchInputKeyDown: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func
  }))
});

_defineProperty(Select, "defaultProps", {
  prefixCls: 'rc-tree-select',
  prefixAria: 'rc-tree-select',
  showArrow: true,
  showSearch: true,
  autoClearSearchValue: true,
  showCheckedStrategy: _strategies__WEBPACK_IMPORTED_MODULE_15__["SHOW_CHILD"],
  // dropdownMatchSelectWidth change the origin design, set to false now
  // ref: https://github.com/react-component/select/blob/4cad95e098a341a09de239ad6981067188842020/src/Select.jsx#L344
  // ref: https://github.com/react-component/select/pull/71
  treeNodeFilterProp: 'value',
  treeNodeLabelProp: 'title',
  treeIcon: false,
  notFoundContent: 'Not Found'
});

Select.TreeNode = _SelectNode__WEBPACK_IMPORTED_MODULE_18__["default"];
Select.SHOW_ALL = _strategies__WEBPACK_IMPORTED_MODULE_15__["SHOW_ALL"];
Select.SHOW_PARENT = _strategies__WEBPACK_IMPORTED_MODULE_15__["SHOW_PARENT"];
Select.SHOW_CHILD = _strategies__WEBPACK_IMPORTED_MODULE_15__["SHOW_CHILD"]; // Let warning show correct component name

Select.displayName = 'TreeSelect';
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__["polyfill"])(Select);
/* harmony default export */ __webpack_exports__["default"] = (Select);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3QvZXMvU2VsZWN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3QvZXMvU2VsZWN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIF9leHRlbmRzKCkgeyBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07IHJldHVybiBfZXh0ZW5kcy5hcHBseSh0aGlzLCBhcmd1bWVudHMpOyB9XG5cbmZ1bmN0aW9uIF90b0NvbnN1bWFibGVBcnJheShhcnIpIHsgcmV0dXJuIF9hcnJheVdpdGhvdXRIb2xlcyhhcnIpIHx8IF9pdGVyYWJsZVRvQXJyYXkoYXJyKSB8fCBfbm9uSXRlcmFibGVTcHJlYWQoKTsgfVxuXG5mdW5jdGlvbiBfbm9uSXRlcmFibGVTcHJlYWQoKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJJbnZhbGlkIGF0dGVtcHQgdG8gc3ByZWFkIG5vbi1pdGVyYWJsZSBpbnN0YW5jZVwiKTsgfVxuXG5mdW5jdGlvbiBfaXRlcmFibGVUb0FycmF5KGl0ZXIpIHsgaWYgKFN5bWJvbC5pdGVyYXRvciBpbiBPYmplY3QoaXRlcikgfHwgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGl0ZXIpID09PSBcIltvYmplY3QgQXJndW1lbnRzXVwiKSByZXR1cm4gQXJyYXkuZnJvbShpdGVyKTsgfVxuXG5mdW5jdGlvbiBfYXJyYXlXaXRob3V0SG9sZXMoYXJyKSB7IGlmIChBcnJheS5pc0FycmF5KGFycikpIHsgZm9yICh2YXIgaSA9IDAsIGFycjIgPSBuZXcgQXJyYXkoYXJyLmxlbmd0aCk7IGkgPCBhcnIubGVuZ3RoOyBpKyspIHsgYXJyMltpXSA9IGFycltpXTsgfSByZXR1cm4gYXJyMjsgfSB9XG5cbmZ1bmN0aW9uIG93bktleXMob2JqZWN0LCBlbnVtZXJhYmxlT25seSkgeyB2YXIga2V5cyA9IE9iamVjdC5rZXlzKG9iamVjdCk7IGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKSB7IHZhciBzeW1ib2xzID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhvYmplY3QpOyBpZiAoZW51bWVyYWJsZU9ubHkpIHN5bWJvbHMgPSBzeW1ib2xzLmZpbHRlcihmdW5jdGlvbiAoc3ltKSB7IHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKG9iamVjdCwgc3ltKS5lbnVtZXJhYmxlOyB9KTsga2V5cy5wdXNoLmFwcGx5KGtleXMsIHN5bWJvbHMpOyB9IHJldHVybiBrZXlzOyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RTcHJlYWQodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV0gIT0gbnVsbCA/IGFyZ3VtZW50c1tpXSA6IHt9OyBpZiAoaSAlIDIpIHsgb3duS2V5cyhPYmplY3Qoc291cmNlKSwgdHJ1ZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IF9kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgc291cmNlW2tleV0pOyB9KTsgfSBlbHNlIGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycykgeyBPYmplY3QuZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKHNvdXJjZSkpOyB9IGVsc2UgeyBvd25LZXlzKE9iamVjdChzb3VyY2UpKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHNvdXJjZSwga2V5KSk7IH0pOyB9IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9XG5cbmZ1bmN0aW9uIF9jcmVhdGVDbGFzcyhDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpKSB7IHJldHVybiBjYWxsOyB9IHJldHVybiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpOyB9XG5cbmZ1bmN0aW9uIF9nZXRQcm90b3R5cGVPZihvKSB7IF9nZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5nZXRQcm90b3R5cGVPZiA6IGZ1bmN0aW9uIF9nZXRQcm90b3R5cGVPZihvKSB7IHJldHVybiBvLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2Yobyk7IH07IHJldHVybiBfZ2V0UHJvdG90eXBlT2Yobyk7IH1cblxuZnVuY3Rpb24gX2Fzc2VydFRoaXNJbml0aWFsaXplZChzZWxmKSB7IGlmIChzZWxmID09PSB2b2lkIDApIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvblwiKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBfc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpOyB9XG5cbmZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7IF9zZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fCBmdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBvLl9fcHJvdG9fXyA9IHA7IHJldHVybiBvOyB9OyByZXR1cm4gX3NldFByb3RvdHlwZU9mKG8sIHApOyB9XG5cbmZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgdmFsdWUpIHsgaWYgKGtleSBpbiBvYmopIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KG9iaiwga2V5LCB7IHZhbHVlOiB2YWx1ZSwgZW51bWVyYWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlLCB3cml0YWJsZTogdHJ1ZSB9KTsgfSBlbHNlIHsgb2JqW2tleV0gPSB2YWx1ZTsgfSByZXR1cm4gb2JqOyB9XG5cbi8qKlxuICogQVJJQTogaHR0cHM6Ly93d3cudzMub3JnL1RSL3dhaS1hcmlhLyNjb21ib2JveFxuICogU2FtcGxlIDE6IGh0dHBzOi8vd3d3LnczLm9yZy9UUi8yMDE3L05PVEUtd2FpLWFyaWEtcHJhY3RpY2VzLTEuMS0yMDE3MTIxNC9leGFtcGxlcy9jb21ib2JveC9hcmlhMS4xcGF0dGVybi9saXN0Ym94LWNvbWJvLmh0bWxcbiAqIFNhbXBsZSAyOiBodHRwczovL3d3dy53My5vcmcvYmxvZy93YWktY29tcG9uZW50cy1nYWxsZXJ5L3dpZGdldC9jb21ib2JveC13aXRoLWFyaWEtYXV0b2NvbXBsZXRlaW5saW5lL1xuICpcbiAqIFRhYiBsb2dpYzpcbiAqIFBvcHVwIGlzIGNsb3NlXG4gKiAxLiBGb2N1cyBpbnB1dCAobWFyayBjb21wb25lbnQgYXMgZm9jdXNlZClcbiAqIDIuIFByZXNzIGVudGVyIHRvIHNob3cgdGhlIHBvcHVwXG4gKiAzLiBJZiBwb3B1cCBoYXMgaW5wdXQsIGZvY3VzIGl0XG4gKlxuICogUG9wdXAgaXMgb3BlblxuICogMS4gcHJlc3MgdGFiIHRvIGNsb3NlIHRoZSBwb3B1cFxuICogMi4gRm9jdXMgYmFjayB0byB0aGUgc2VsZWN0aW9uIGlucHV0IGJveFxuICogMy4gTGV0IHRoZSBuYXRpdmUgdGFiIGdvaW5nIG9uXG4gKlxuICogVHJlZVNlbGVjdCB1c2UgMiBkZXNpZ24gdHlwZS5cbiAqIEluIHNpbmdsZSBtb2RlLCB3ZSBzaG91bGQgZm9jdXMgb24gdGhlIGBzcGFuYFxuICogSW4gbXVsdGlwbGUgbW9kZSwgd2Ugc2hvdWxkIGZvY3VzIG9uIHRoZSBgaW5wdXRgXG4gKi9cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBmaW5kRE9NTm9kZSB9IGZyb20gJ3JlYWN0LWRvbSc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgcG9seWZpbGwgfSBmcm9tICdyZWFjdC1saWZlY3ljbGVzLWNvbXBhdCc7XG5pbXBvcnQgS2V5Q29kZSBmcm9tIFwicmMtdXRpbC9lcy9LZXlDb2RlXCI7XG5pbXBvcnQgc2hhbGxvd0VxdWFsIGZyb20gJ3NoYWxsb3dlcXVhbCc7XG5pbXBvcnQgcmFmIGZyb20gJ3JhZic7XG5pbXBvcnQgc2Nyb2xsSW50b1ZpZXcgZnJvbSAnZG9tLXNjcm9sbC1pbnRvLXZpZXcnO1xuaW1wb3J0IFNlbGVjdFRyaWdnZXIgZnJvbSAnLi9TZWxlY3RUcmlnZ2VyJztcbmltcG9ydCB7IHNlbGVjdG9yQ29udGV4dFR5cGVzIH0gZnJvbSAnLi9CYXNlL0Jhc2VTZWxlY3Rvcic7XG5pbXBvcnQgeyBwb3B1cENvbnRleHRUeXBlcyB9IGZyb20gJy4vQmFzZS9CYXNlUG9wdXAnO1xuaW1wb3J0IFNpbmdsZVNlbGVjdG9yIGZyb20gJy4vU2VsZWN0b3IvU2luZ2xlU2VsZWN0b3InO1xuaW1wb3J0IE11bHRpcGxlU2VsZWN0b3IsIHsgbXVsdGlwbGVTZWxlY3RvckNvbnRleHRUeXBlcyB9IGZyb20gJy4vU2VsZWN0b3IvTXVsdGlwbGVTZWxlY3Rvcic7XG5pbXBvcnQgU2luZ2xlUG9wdXAgZnJvbSAnLi9Qb3B1cC9TaW5nbGVQb3B1cCc7XG5pbXBvcnQgTXVsdGlwbGVQb3B1cCBmcm9tICcuL1BvcHVwL011bHRpcGxlUG9wdXAnO1xuaW1wb3J0IHsgU0hPV19BTEwsIFNIT1dfUEFSRU5ULCBTSE9XX0NISUxEIH0gZnJvbSAnLi9zdHJhdGVnaWVzJztcbmltcG9ydCB7IGNyZWF0ZVJlZiwgZ2VuZXJhdGVBcmlhSWQsIGZvcm1hdEludGVybmFsVmFsdWUsIGZvcm1hdFNlbGVjdG9yVmFsdWUsIHBhcnNlU2ltcGxlVHJlZURhdGEsIGNvbnZlcnREYXRhVG9UcmVlLCBjb252ZXJ0VHJlZVRvRW50aXRpZXMsIGNvbmR1Y3RDaGVjaywgZ2V0SGFsZkNoZWNrZWRLZXlzLCBmbGF0VG9IaWVyYXJjaHksIGlzUG9zUmVsYXRlZCwgaXNMYWJlbEluVmFsdWUsIGdldEZpbHRlclRyZWUsIGNsZWFuRW50aXR5LCBmaW5kUG9wdXBDb250YWluZXIgfSBmcm9tICcuL3V0aWwnO1xuaW1wb3J0IHsgdmFsdWVQcm9wIH0gZnJvbSAnLi9wcm9wVHlwZXMnO1xuaW1wb3J0IFNlbGVjdE5vZGUgZnJvbSAnLi9TZWxlY3ROb2RlJztcblxudmFyIFNlbGVjdCA9XG4vKiNfX1BVUkVfXyovXG5mdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoU2VsZWN0LCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBTZWxlY3QoX3Byb3BzKSB7XG4gICAgdmFyIF90aGlzO1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFNlbGVjdCk7XG5cbiAgICBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9nZXRQcm90b3R5cGVPZihTZWxlY3QpLmNhbGwodGhpcywgX3Byb3BzKSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwib25TZWxlY3RvckZvY3VzXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgZm9jdXNlZDogdHJ1ZVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwib25TZWxlY3RvckJsdXJcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICBmb2N1c2VkOiBmYWxzZVxuICAgICAgfSk7IC8vIFRPRE86IENsb3NlIHdoZW4gUG9wdXAgaXMgYWxzbyBub3QgZm9jdXNlZFxuICAgICAgLy8gdGhpcy5zZXRTdGF0ZSh7IG9wZW46IGZhbHNlIH0pO1xuXG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwib25Db21wb25lbnRLZXlEb3duXCIsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgdmFyIG9wZW4gPSBfdGhpcy5zdGF0ZS5vcGVuO1xuICAgICAgdmFyIGtleUNvZGUgPSBldmVudC5rZXlDb2RlO1xuXG4gICAgICBpZiAoIW9wZW4pIHtcbiAgICAgICAgaWYgKFtLZXlDb2RlLkVOVEVSLCBLZXlDb2RlLkRPV05dLmluZGV4T2Yoa2V5Q29kZSkgIT09IC0xKSB7XG4gICAgICAgICAgX3RoaXMuc2V0T3BlblN0YXRlKHRydWUpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKEtleUNvZGUuRVNDID09PSBrZXlDb2RlKSB7XG4gICAgICAgIF90aGlzLnNldE9wZW5TdGF0ZShmYWxzZSk7XG4gICAgICB9IGVsc2UgaWYgKFtLZXlDb2RlLlVQLCBLZXlDb2RlLkRPV04sIEtleUNvZGUuTEVGVCwgS2V5Q29kZS5SSUdIVF0uaW5kZXhPZihrZXlDb2RlKSAhPT0gLTEpIHtcbiAgICAgICAgLy8gVE9ETzogSGFuZGxlIGBvcGVuYCBzdGF0ZVxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJvbkRlc2VsZWN0XCIsIGZ1bmN0aW9uICh3cmFwcGVkVmFsdWUsIG5vZGUsIG5vZGVFdmVudEluZm8pIHtcbiAgICAgIHZhciBvbkRlc2VsZWN0ID0gX3RoaXMucHJvcHMub25EZXNlbGVjdDtcbiAgICAgIGlmICghb25EZXNlbGVjdCkgcmV0dXJuO1xuICAgICAgb25EZXNlbGVjdCh3cmFwcGVkVmFsdWUsIG5vZGUsIG5vZGVFdmVudEluZm8pO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcIm9uU2VsZWN0b3JDbGVhclwiLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIHZhciBkaXNhYmxlZCA9IF90aGlzLnByb3BzLmRpc2FibGVkO1xuICAgICAgaWYgKGRpc2FibGVkKSByZXR1cm47XG5cbiAgICAgIF90aGlzLnRyaWdnZXJDaGFuZ2UoW10sIFtdKTtcblxuICAgICAgaWYgKCFfdGhpcy5pc1NlYXJjaFZhbHVlQ29udHJvbGxlZCgpKSB7XG4gICAgICAgIF90aGlzLnNldFVuY29udHJvbGxlZFN0YXRlKHtcbiAgICAgICAgICBzZWFyY2hWYWx1ZTogJycsXG4gICAgICAgICAgZmlsdGVyZWRUcmVlTm9kZXM6IG51bGxcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcIm9uTXVsdGlwbGVTZWxlY3RvclJlbW92ZVwiLCBmdW5jdGlvbiAoZXZlbnQsIHJlbW92ZVZhbHVlKSB7XG4gICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgIHZhciBfdGhpcyRzdGF0ZSA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgIHZhbHVlTGlzdCA9IF90aGlzJHN0YXRlLnZhbHVlTGlzdCxcbiAgICAgICAgICBtaXNzVmFsdWVMaXN0ID0gX3RoaXMkc3RhdGUubWlzc1ZhbHVlTGlzdCxcbiAgICAgICAgICB2YWx1ZUVudGl0aWVzID0gX3RoaXMkc3RhdGUudmFsdWVFbnRpdGllcztcbiAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHRyZWVDaGVja2FibGUgPSBfdGhpcyRwcm9wcy50cmVlQ2hlY2thYmxlLFxuICAgICAgICAgIHRyZWVDaGVja1N0cmljdGx5ID0gX3RoaXMkcHJvcHMudHJlZUNoZWNrU3RyaWN0bHksXG4gICAgICAgICAgdHJlZU5vZGVMYWJlbFByb3AgPSBfdGhpcyRwcm9wcy50cmVlTm9kZUxhYmVsUHJvcCxcbiAgICAgICAgICBkaXNhYmxlZCA9IF90aGlzJHByb3BzLmRpc2FibGVkO1xuICAgICAgaWYgKGRpc2FibGVkKSByZXR1cm47IC8vIEZpbmQgdHJpZ2dlciBlbnRpdHlcblxuICAgICAgdmFyIHRyaWdnZXJFbnRpdHkgPSB2YWx1ZUVudGl0aWVzW3JlbW92ZVZhbHVlXTsgLy8gQ2xlYW4gdXAgdmFsdWVcblxuICAgICAgdmFyIG5ld1ZhbHVlTGlzdCA9IHZhbHVlTGlzdDtcblxuICAgICAgaWYgKHRyaWdnZXJFbnRpdHkpIHtcbiAgICAgICAgLy8gSWYgdmFsdWUgaXMgaW4gdHJlZVxuICAgICAgICBpZiAodHJlZUNoZWNrYWJsZSAmJiAhdHJlZUNoZWNrU3RyaWN0bHkpIHtcbiAgICAgICAgICBuZXdWYWx1ZUxpc3QgPSB2YWx1ZUxpc3QuZmlsdGVyKGZ1bmN0aW9uIChfcmVmKSB7XG4gICAgICAgICAgICB2YXIgdmFsdWUgPSBfcmVmLnZhbHVlO1xuICAgICAgICAgICAgdmFyIGVudGl0eSA9IHZhbHVlRW50aXRpZXNbdmFsdWVdO1xuICAgICAgICAgICAgcmV0dXJuICFpc1Bvc1JlbGF0ZWQoZW50aXR5LnBvcywgdHJpZ2dlckVudGl0eS5wb3MpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIG5ld1ZhbHVlTGlzdCA9IHZhbHVlTGlzdC5maWx0ZXIoZnVuY3Rpb24gKF9yZWYyKSB7XG4gICAgICAgICAgICB2YXIgdmFsdWUgPSBfcmVmMi52YWx1ZTtcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZSAhPT0gcmVtb3ZlVmFsdWU7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgdmFyIHRyaWdnZXJOb2RlID0gdHJpZ2dlckVudGl0eSA/IHRyaWdnZXJFbnRpdHkubm9kZSA6IG51bGw7XG4gICAgICB2YXIgZXh0cmFJbmZvID0ge1xuICAgICAgICB0cmlnZ2VyVmFsdWU6IHJlbW92ZVZhbHVlLFxuICAgICAgICB0cmlnZ2VyTm9kZTogdHJpZ2dlck5vZGVcbiAgICAgIH07XG4gICAgICB2YXIgZGVzZWxlY3RJbmZvID0ge1xuICAgICAgICBub2RlOiB0cmlnZ2VyTm9kZVxuICAgICAgfTsgLy8gW0xlZ2FjeV0gTGl0dGxlIGhhY2sgb24gdGhpcyB0byBtYWtlIHNhbWUgYWN0aW9uIGFzIGBvbkNoZWNrYCBldmVudC5cblxuICAgICAgaWYgKHRyZWVDaGVja2FibGUpIHtcbiAgICAgICAgdmFyIGZpbHRlcmVkRW50aXR5TGlzdCA9IG5ld1ZhbHVlTGlzdC5tYXAoZnVuY3Rpb24gKF9yZWYzKSB7XG4gICAgICAgICAgdmFyIHZhbHVlID0gX3JlZjMudmFsdWU7XG4gICAgICAgICAgcmV0dXJuIHZhbHVlRW50aXRpZXNbdmFsdWVdO1xuICAgICAgICB9KTtcbiAgICAgICAgZGVzZWxlY3RJbmZvLmV2ZW50ID0gJ2NoZWNrJztcbiAgICAgICAgZGVzZWxlY3RJbmZvLmNoZWNrZWQgPSBmYWxzZTtcbiAgICAgICAgZGVzZWxlY3RJbmZvLmNoZWNrZWROb2RlcyA9IGZpbHRlcmVkRW50aXR5TGlzdC5tYXAoZnVuY3Rpb24gKF9yZWY0KSB7XG4gICAgICAgICAgdmFyIG5vZGUgPSBfcmVmNC5ub2RlO1xuICAgICAgICAgIHJldHVybiBub2RlO1xuICAgICAgICB9KTtcbiAgICAgICAgZGVzZWxlY3RJbmZvLmNoZWNrZWROb2Rlc1Bvc2l0aW9ucyA9IGZpbHRlcmVkRW50aXR5TGlzdC5tYXAoZnVuY3Rpb24gKF9yZWY1KSB7XG4gICAgICAgICAgdmFyIG5vZGUgPSBfcmVmNS5ub2RlLFxuICAgICAgICAgICAgICBwb3MgPSBfcmVmNS5wb3M7XG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIG5vZGU6IG5vZGUsXG4gICAgICAgICAgICBwb3M6IHBvc1xuICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmICh0cmVlQ2hlY2tTdHJpY3RseSkge1xuICAgICAgICAgIGV4dHJhSW5mby5hbGxDaGVja2VkTm9kZXMgPSBkZXNlbGVjdEluZm8uY2hlY2tlZE5vZGVzO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIFRPRE86IEl0J3MgdG9vIGV4cGFuc2l2ZSB0byBnZXQgYGhhbGZDaGVja2VkS2V5c2AgaW4gb25EZXNlbGVjdC4gTm90IHBhc3MgdGhpcy5cbiAgICAgICAgICBleHRyYUluZm8uYWxsQ2hlY2tlZE5vZGVzID0gZmxhdFRvSGllcmFyY2h5KGZpbHRlcmVkRW50aXR5TGlzdCkubWFwKGZ1bmN0aW9uIChfcmVmNikge1xuICAgICAgICAgICAgdmFyIG5vZGUgPSBfcmVmNi5ub2RlO1xuICAgICAgICAgICAgcmV0dXJuIG5vZGU7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGRlc2VsZWN0SW5mby5ldmVudCA9ICdzZWxlY3QnO1xuICAgICAgICBkZXNlbGVjdEluZm8uc2VsZWN0ZWQgPSBmYWxzZTtcbiAgICAgICAgZGVzZWxlY3RJbmZvLnNlbGVjdGVkTm9kZXMgPSBuZXdWYWx1ZUxpc3QubWFwKGZ1bmN0aW9uIChfcmVmNykge1xuICAgICAgICAgIHZhciB2YWx1ZSA9IF9yZWY3LnZhbHVlO1xuICAgICAgICAgIHJldHVybiAodmFsdWVFbnRpdGllc1t2YWx1ZV0gfHwge30pLm5vZGU7XG4gICAgICAgIH0pO1xuICAgICAgfSAvLyBTb21lIHZhbHVlIHVzZXIgcGFzcyBwcm9wIGlzIG5vdCBpbiB0aGUgdHJlZSwgd2UgYWxzbyBuZWVkIGNsZWFuIGl0XG5cblxuICAgICAgdmFyIG5ld01pc3NWYWx1ZUxpc3QgPSBtaXNzVmFsdWVMaXN0LmZpbHRlcihmdW5jdGlvbiAoX3JlZjgpIHtcbiAgICAgICAgdmFyIHZhbHVlID0gX3JlZjgudmFsdWU7XG4gICAgICAgIHJldHVybiB2YWx1ZSAhPT0gcmVtb3ZlVmFsdWU7XG4gICAgICB9KTtcbiAgICAgIHZhciB3cmFwcGVkVmFsdWU7XG5cbiAgICAgIGlmIChfdGhpcy5pc0xhYmVsSW5WYWx1ZSgpKSB7XG4gICAgICAgIHdyYXBwZWRWYWx1ZSA9IHtcbiAgICAgICAgICBsYWJlbDogdHJpZ2dlck5vZGUgPyB0cmlnZ2VyTm9kZS5wcm9wc1t0cmVlTm9kZUxhYmVsUHJvcF0gOiBudWxsLFxuICAgICAgICAgIHZhbHVlOiByZW1vdmVWYWx1ZVxuICAgICAgICB9O1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgd3JhcHBlZFZhbHVlID0gcmVtb3ZlVmFsdWU7XG4gICAgICB9XG5cbiAgICAgIF90aGlzLm9uRGVzZWxlY3Qod3JhcHBlZFZhbHVlLCB0cmlnZ2VyTm9kZSwgZGVzZWxlY3RJbmZvKTtcblxuICAgICAgX3RoaXMudHJpZ2dlckNoYW5nZShuZXdNaXNzVmFsdWVMaXN0LCBuZXdWYWx1ZUxpc3QsIGV4dHJhSW5mbyk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwib25WYWx1ZVRyaWdnZXJcIiwgZnVuY3Rpb24gKGlzQWRkLCBub2RlTGlzdCwgbm9kZUV2ZW50SW5mbywgbm9kZUV4dHJhSW5mbykge1xuICAgICAgdmFyIG5vZGUgPSBub2RlRXZlbnRJbmZvLm5vZGU7XG4gICAgICB2YXIgdmFsdWUgPSBub2RlLnByb3BzLnZhbHVlO1xuICAgICAgdmFyIF90aGlzJHN0YXRlMiA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgIG1pc3NWYWx1ZUxpc3QgPSBfdGhpcyRzdGF0ZTIubWlzc1ZhbHVlTGlzdCxcbiAgICAgICAgICB2YWx1ZUVudGl0aWVzID0gX3RoaXMkc3RhdGUyLnZhbHVlRW50aXRpZXMsXG4gICAgICAgICAga2V5RW50aXRpZXMgPSBfdGhpcyRzdGF0ZTIua2V5RW50aXRpZXMsXG4gICAgICAgICAgc2VhcmNoVmFsdWUgPSBfdGhpcyRzdGF0ZTIuc2VhcmNoVmFsdWU7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMyID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgZGlzYWJsZWQgPSBfdGhpcyRwcm9wczIuZGlzYWJsZWQsXG4gICAgICAgICAgaW5wdXRWYWx1ZSA9IF90aGlzJHByb3BzMi5pbnB1dFZhbHVlLFxuICAgICAgICAgIHRyZWVOb2RlTGFiZWxQcm9wID0gX3RoaXMkcHJvcHMyLnRyZWVOb2RlTGFiZWxQcm9wLFxuICAgICAgICAgIG9uU2VsZWN0ID0gX3RoaXMkcHJvcHMyLm9uU2VsZWN0LFxuICAgICAgICAgIG9uU2VhcmNoID0gX3RoaXMkcHJvcHMyLm9uU2VhcmNoLFxuICAgICAgICAgIG11bHRpcGxlID0gX3RoaXMkcHJvcHMyLm11bHRpcGxlLFxuICAgICAgICAgIHRyZWVDaGVja2FibGUgPSBfdGhpcyRwcm9wczIudHJlZUNoZWNrYWJsZSxcbiAgICAgICAgICB0cmVlQ2hlY2tTdHJpY3RseSA9IF90aGlzJHByb3BzMi50cmVlQ2hlY2tTdHJpY3RseSxcbiAgICAgICAgICBhdXRvQ2xlYXJTZWFyY2hWYWx1ZSA9IF90aGlzJHByb3BzMi5hdXRvQ2xlYXJTZWFyY2hWYWx1ZTtcbiAgICAgIHZhciBsYWJlbCA9IG5vZGUucHJvcHNbdHJlZU5vZGVMYWJlbFByb3BdO1xuICAgICAgaWYgKGRpc2FibGVkKSByZXR1cm47IC8vIFdyYXAgdGhlIHJldHVybiB2YWx1ZSBmb3IgdXNlclxuXG4gICAgICB2YXIgd3JhcHBlZFZhbHVlO1xuXG4gICAgICBpZiAoX3RoaXMuaXNMYWJlbEluVmFsdWUoKSkge1xuICAgICAgICB3cmFwcGVkVmFsdWUgPSB7XG4gICAgICAgICAgdmFsdWU6IHZhbHVlLFxuICAgICAgICAgIGxhYmVsOiBsYWJlbFxuICAgICAgICB9O1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgd3JhcHBlZFZhbHVlID0gdmFsdWU7XG4gICAgICB9IC8vIFtMZWdhY3ldIE9yaWdpbiBjb2RlIG5vdCB0cmlnZ2VyIGBvbkRlc2VsZWN0YCBldmVyeSB0aW1lLiBMZXQncyBhbGlnbiB0aGUgYmVoYXZpb3VyLlxuXG5cbiAgICAgIGlmIChpc0FkZCkge1xuICAgICAgICBpZiAob25TZWxlY3QpIHtcbiAgICAgICAgICBvblNlbGVjdCh3cmFwcGVkVmFsdWUsIG5vZGUsIG5vZGVFdmVudEluZm8pO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBfdGhpcy5vbkRlc2VsZWN0KHdyYXBwZWRWYWx1ZSwgbm9kZSwgbm9kZUV2ZW50SW5mbyk7XG4gICAgICB9IC8vIEdldCB3cmFwcGVkIHZhbHVlIGxpc3QuXG4gICAgICAvLyBUaGlzIGlzIGEgYml0IGhhY2sgY2F1c2Ugd2UgdXNlIGtleSB0byBtYXRjaCB0aGUgdmFsdWUuXG5cblxuICAgICAgdmFyIG5ld1ZhbHVlTGlzdCA9IG5vZGVMaXN0Lm1hcChmdW5jdGlvbiAoX3JlZjkpIHtcbiAgICAgICAgdmFyIHByb3BzID0gX3JlZjkucHJvcHM7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgdmFsdWU6IHByb3BzLnZhbHVlLFxuICAgICAgICAgIGxhYmVsOiBwcm9wc1t0cmVlTm9kZUxhYmVsUHJvcF1cbiAgICAgICAgfTtcbiAgICAgIH0pOyAvLyBXaGVuIGlzIGB0cmVlQ2hlY2thYmxlYCBhbmQgd2l0aCBgc2VhcmNoVmFsdWVgLCBgdmFsdWVMaXN0YCBpcyBub3QgZnVsbCBmaWxsZWQuXG4gICAgICAvLyBXZSBuZWVkIGNhbGN1bGF0ZSB0aGUgbWlzc2luZyBub2Rlcy5cblxuICAgICAgaWYgKHRyZWVDaGVja2FibGUgJiYgIXRyZWVDaGVja1N0cmljdGx5KSB7XG4gICAgICAgIHZhciBrZXlMaXN0ID0gbmV3VmFsdWVMaXN0Lm1hcChmdW5jdGlvbiAoX3JlZjEwKSB7XG4gICAgICAgICAgdmFyIHZhbCA9IF9yZWYxMC52YWx1ZTtcbiAgICAgICAgICByZXR1cm4gdmFsdWVFbnRpdGllc1t2YWxdLmtleTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKGlzQWRkKSB7XG4gICAgICAgICAga2V5TGlzdCA9IGNvbmR1Y3RDaGVjayhrZXlMaXN0LCB0cnVlLCBrZXlFbnRpdGllcykuY2hlY2tlZEtleXM7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAga2V5TGlzdCA9IGNvbmR1Y3RDaGVjayhbdmFsdWVFbnRpdGllc1t2YWx1ZV0ua2V5XSwgZmFsc2UsIGtleUVudGl0aWVzLCB7XG4gICAgICAgICAgICBjaGVja2VkS2V5czoga2V5TGlzdFxuICAgICAgICAgIH0pLmNoZWNrZWRLZXlzO1xuICAgICAgICB9XG5cbiAgICAgICAgbmV3VmFsdWVMaXN0ID0ga2V5TGlzdC5tYXAoZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgIHZhciBwcm9wcyA9IGtleUVudGl0aWVzW2tleV0ubm9kZS5wcm9wcztcbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdmFsdWU6IHByb3BzLnZhbHVlLFxuICAgICAgICAgICAgbGFiZWw6IHByb3BzW3RyZWVOb2RlTGFiZWxQcm9wXVxuICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuICAgICAgfSAvLyBDbGVhbiB1cCBgc2VhcmNoVmFsdWVgIHdoZW4gdGhpcyBwcm9wIGlzIHNldFxuXG5cbiAgICAgIGlmIChhdXRvQ2xlYXJTZWFyY2hWYWx1ZSB8fCBpbnB1dFZhbHVlID09PSBudWxsKSB7XG4gICAgICAgIC8vIENsZWFuIHN0YXRlIGBzZWFyY2hWYWx1ZWAgaWYgdW5jb250cm9sbGVkXG4gICAgICAgIGlmICghX3RoaXMuaXNTZWFyY2hWYWx1ZUNvbnRyb2xsZWQoKSAmJiAobXVsdGlwbGUgfHwgdHJlZUNoZWNrYWJsZSkpIHtcbiAgICAgICAgICBfdGhpcy5zZXRVbmNvbnRyb2xsZWRTdGF0ZSh7XG4gICAgICAgICAgICBzZWFyY2hWYWx1ZTogJycsXG4gICAgICAgICAgICBmaWx0ZXJlZFRyZWVOb2RlczogbnVsbFxuICAgICAgICAgIH0pO1xuICAgICAgICB9IC8vIFRyaWdnZXIgb25TZWFyY2ggaWYgYHNlYXJjaFZhbHVlYCB0byBiZSBlbXB0eS5cbiAgICAgICAgLy8gV2Ugc2hvdWxkIGFsc28gdHJpZ2dlciBvblNlYXJjaCB3aXRoIGVtcHR5IHN0cmluZyBoZXJlXG4gICAgICAgIC8vIHNpbmNlIGlmIHVzZXIgdXNlIGB0cmVlRXhwYW5kZWRLZXlzYCwgaXQgbmVlZCB1c2VyIGhhdmUgdGhlIGFiaWxpdHkgdG8gcmVzZXQgaXQuXG5cblxuICAgICAgICBpZiAob25TZWFyY2ggJiYgc2VhcmNoVmFsdWUgJiYgc2VhcmNoVmFsdWUubGVuZ3RoKSB7XG4gICAgICAgICAgb25TZWFyY2goJycpO1xuICAgICAgICB9XG4gICAgICB9IC8vIFtMZWdhY3ldIFByb3ZpZGUgZXh0cmEgaW5mb1xuXG5cbiAgICAgIHZhciBleHRyYUluZm8gPSBfb2JqZWN0U3ByZWFkKHt9LCBub2RlRXh0cmFJbmZvLCB7XG4gICAgICAgIHRyaWdnZXJWYWx1ZTogdmFsdWUsXG4gICAgICAgIHRyaWdnZXJOb2RlOiBub2RlXG4gICAgICB9KTtcblxuICAgICAgX3RoaXMudHJpZ2dlckNoYW5nZShtaXNzVmFsdWVMaXN0LCBuZXdWYWx1ZUxpc3QsIGV4dHJhSW5mbyk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwib25UcmVlTm9kZVNlbGVjdFwiLCBmdW5jdGlvbiAoXywgbm9kZUV2ZW50SW5mbykge1xuICAgICAgdmFyIF90aGlzJHN0YXRlMyA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgIHZhbHVlTGlzdCA9IF90aGlzJHN0YXRlMy52YWx1ZUxpc3QsXG4gICAgICAgICAgdmFsdWVFbnRpdGllcyA9IF90aGlzJHN0YXRlMy52YWx1ZUVudGl0aWVzO1xuICAgICAgdmFyIF90aGlzJHByb3BzMyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHRyZWVDaGVja2FibGUgPSBfdGhpcyRwcm9wczMudHJlZUNoZWNrYWJsZSxcbiAgICAgICAgICBtdWx0aXBsZSA9IF90aGlzJHByb3BzMy5tdWx0aXBsZTtcbiAgICAgIGlmICh0cmVlQ2hlY2thYmxlKSByZXR1cm47XG5cbiAgICAgIGlmICghbXVsdGlwbGUpIHtcbiAgICAgICAgX3RoaXMuc2V0T3BlblN0YXRlKGZhbHNlKTtcbiAgICAgIH1cblxuICAgICAgdmFyIGlzQWRkID0gbm9kZUV2ZW50SW5mby5zZWxlY3RlZDtcbiAgICAgIHZhciBzZWxlY3RlZFZhbHVlID0gbm9kZUV2ZW50SW5mby5ub2RlLnByb3BzLnZhbHVlO1xuICAgICAgdmFyIG5ld1ZhbHVlTGlzdDtcblxuICAgICAgaWYgKCFtdWx0aXBsZSkge1xuICAgICAgICBuZXdWYWx1ZUxpc3QgPSBbe1xuICAgICAgICAgIHZhbHVlOiBzZWxlY3RlZFZhbHVlXG4gICAgICAgIH1dO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgbmV3VmFsdWVMaXN0ID0gdmFsdWVMaXN0LmZpbHRlcihmdW5jdGlvbiAoX3JlZjExKSB7XG4gICAgICAgICAgdmFyIHZhbHVlID0gX3JlZjExLnZhbHVlO1xuICAgICAgICAgIHJldHVybiB2YWx1ZSAhPT0gc2VsZWN0ZWRWYWx1ZTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKGlzQWRkKSB7XG4gICAgICAgICAgbmV3VmFsdWVMaXN0LnB1c2goe1xuICAgICAgICAgICAgdmFsdWU6IHNlbGVjdGVkVmFsdWVcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB2YXIgc2VsZWN0ZWROb2RlcyA9IG5ld1ZhbHVlTGlzdC5tYXAoZnVuY3Rpb24gKF9yZWYxMikge1xuICAgICAgICB2YXIgdmFsdWUgPSBfcmVmMTIudmFsdWU7XG4gICAgICAgIHJldHVybiB2YWx1ZUVudGl0aWVzW3ZhbHVlXTtcbiAgICAgIH0pLmZpbHRlcihmdW5jdGlvbiAoZW50aXR5KSB7XG4gICAgICAgIHJldHVybiBlbnRpdHk7XG4gICAgICB9KS5tYXAoZnVuY3Rpb24gKF9yZWYxMykge1xuICAgICAgICB2YXIgbm9kZSA9IF9yZWYxMy5ub2RlO1xuICAgICAgICByZXR1cm4gbm9kZTtcbiAgICAgIH0pO1xuXG4gICAgICBfdGhpcy5vblZhbHVlVHJpZ2dlcihpc0FkZCwgc2VsZWN0ZWROb2Rlcywgbm9kZUV2ZW50SW5mbywge1xuICAgICAgICBzZWxlY3RlZDogaXNBZGRcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcIm9uVHJlZU5vZGVDaGVja1wiLCBmdW5jdGlvbiAoXywgbm9kZUV2ZW50SW5mbykge1xuICAgICAgdmFyIF90aGlzJHN0YXRlNCA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgIHNlYXJjaFZhbHVlID0gX3RoaXMkc3RhdGU0LnNlYXJjaFZhbHVlLFxuICAgICAgICAgIGtleUVudGl0aWVzID0gX3RoaXMkc3RhdGU0LmtleUVudGl0aWVzLFxuICAgICAgICAgIHZhbHVlRW50aXRpZXMgPSBfdGhpcyRzdGF0ZTQudmFsdWVFbnRpdGllcyxcbiAgICAgICAgICB2YWx1ZUxpc3QgPSBfdGhpcyRzdGF0ZTQudmFsdWVMaXN0O1xuICAgICAgdmFyIHRyZWVDaGVja1N0cmljdGx5ID0gX3RoaXMucHJvcHMudHJlZUNoZWNrU3RyaWN0bHk7XG4gICAgICB2YXIgY2hlY2tlZE5vZGVzID0gbm9kZUV2ZW50SW5mby5jaGVja2VkTm9kZXMsXG4gICAgICAgICAgY2hlY2tlZE5vZGVzUG9zaXRpb25zID0gbm9kZUV2ZW50SW5mby5jaGVja2VkTm9kZXNQb3NpdGlvbnM7XG4gICAgICB2YXIgaXNBZGQgPSBub2RlRXZlbnRJbmZvLmNoZWNrZWQ7XG4gICAgICB2YXIgZXh0cmFJbmZvID0ge1xuICAgICAgICBjaGVja2VkOiBpc0FkZFxuICAgICAgfTtcbiAgICAgIHZhciBjaGVja2VkTm9kZUxpc3QgPSBjaGVja2VkTm9kZXM7IC8vIFtMZWdhY3ldIENoZWNrIGV2ZW50IHByb3ZpZGUgYGFsbENoZWNrZWROb2Rlc2AuXG4gICAgICAvLyBXaGVuIGB0cmVlQ2hlY2tTdHJpY3RseWAgb3IgaW50ZXJuYWwgYHNlYXJjaFZhbHVlYCBpcyBzZXQsIFRyZWVOb2RlIHdpbGwgYmUgdW5yZWxhdGVkOlxuICAgICAgLy8gLSBSZWxhdGVkOiBTaG93IHRoZSB0b3AgY2hlY2tlZCBub2RlcyBhbmQgaGFzIGNoaWxkcmVuIHByb3AuXG4gICAgICAvLyAtIFVucmVsYXRlZDogU2hvdyBhbGwgdGhlIGNoZWNrZWQgbm9kZXMuXG5cbiAgICAgIGlmIChzZWFyY2hWYWx1ZSkge1xuICAgICAgICB2YXIgb3JpS2V5TGlzdCA9IHZhbHVlTGlzdC5tYXAoZnVuY3Rpb24gKF9yZWYxNCkge1xuICAgICAgICAgIHZhciB2YWx1ZSA9IF9yZWYxNC52YWx1ZTtcbiAgICAgICAgICByZXR1cm4gdmFsdWVFbnRpdGllc1t2YWx1ZV07XG4gICAgICAgIH0pLmZpbHRlcihmdW5jdGlvbiAoZW50aXR5KSB7XG4gICAgICAgICAgcmV0dXJuIGVudGl0eTtcbiAgICAgICAgfSkubWFwKGZ1bmN0aW9uIChfcmVmMTUpIHtcbiAgICAgICAgICB2YXIga2V5ID0gX3JlZjE1LmtleTtcbiAgICAgICAgICByZXR1cm4ga2V5O1xuICAgICAgICB9KTtcbiAgICAgICAgdmFyIGtleUxpc3Q7XG5cbiAgICAgICAgaWYgKGlzQWRkKSB7XG4gICAgICAgICAga2V5TGlzdCA9IEFycmF5LmZyb20obmV3IFNldChbXS5jb25jYXQoX3RvQ29uc3VtYWJsZUFycmF5KG9yaUtleUxpc3QpLCBfdG9Db25zdW1hYmxlQXJyYXkoY2hlY2tlZE5vZGVMaXN0Lm1hcChmdW5jdGlvbiAoX3JlZjE2KSB7XG4gICAgICAgICAgICB2YXIgdmFsdWUgPSBfcmVmMTYucHJvcHMudmFsdWU7XG4gICAgICAgICAgICByZXR1cm4gdmFsdWVFbnRpdGllc1t2YWx1ZV0ua2V5O1xuICAgICAgICAgIH0pKSkpKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBrZXlMaXN0ID0gY29uZHVjdENoZWNrKFtub2RlRXZlbnRJbmZvLm5vZGUucHJvcHMuZXZlbnRLZXldLCBmYWxzZSwga2V5RW50aXRpZXMsIHtcbiAgICAgICAgICAgIGNoZWNrZWRLZXlzOiBvcmlLZXlMaXN0XG4gICAgICAgICAgfSkuY2hlY2tlZEtleXM7XG4gICAgICAgIH1cblxuICAgICAgICBjaGVja2VkTm9kZUxpc3QgPSBrZXlMaXN0Lm1hcChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgcmV0dXJuIGtleUVudGl0aWVzW2tleV0ubm9kZTtcbiAgICAgICAgfSk7IC8vIExldCdzIGZvbGxvdyBhcyBub3QgYHRyZWVDaGVja1N0cmljdGx5YCBmb3JtYXRcblxuICAgICAgICBleHRyYUluZm8uYWxsQ2hlY2tlZE5vZGVzID0ga2V5TGlzdC5tYXAoZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgIHJldHVybiBjbGVhbkVudGl0eShrZXlFbnRpdGllc1trZXldKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2UgaWYgKHRyZWVDaGVja1N0cmljdGx5KSB7XG4gICAgICAgIGV4dHJhSW5mby5hbGxDaGVja2VkTm9kZXMgPSBub2RlRXZlbnRJbmZvLmNoZWNrZWROb2RlcztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGV4dHJhSW5mby5hbGxDaGVja2VkTm9kZXMgPSBmbGF0VG9IaWVyYXJjaHkoY2hlY2tlZE5vZGVzUG9zaXRpb25zKTtcbiAgICAgIH1cblxuICAgICAgX3RoaXMub25WYWx1ZVRyaWdnZXIoaXNBZGQsIGNoZWNrZWROb2RlTGlzdCwgbm9kZUV2ZW50SW5mbywgZXh0cmFJbmZvKTtcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJvbkRyb3Bkb3duVmlzaWJsZUNoYW5nZVwiLCBmdW5jdGlvbiAob3Blbikge1xuICAgICAgdmFyIF90aGlzJHByb3BzNCA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIG11bHRpcGxlID0gX3RoaXMkcHJvcHM0Lm11bHRpcGxlLFxuICAgICAgICAgIHRyZWVDaGVja2FibGUgPSBfdGhpcyRwcm9wczQudHJlZUNoZWNrYWJsZTtcbiAgICAgIHZhciBzZWFyY2hWYWx1ZSA9IF90aGlzLnN0YXRlLnNlYXJjaFZhbHVlOyAvLyBXaGVuIHNldCBvcGVuIHN1Y2Nlc3MgYW5kIHNpbmdsZSBtb2RlLFxuICAgICAgLy8gd2Ugd2lsbCByZXNldCB0aGUgaW5wdXQgY29udGVudC5cblxuICAgICAgaWYgKG9wZW4gJiYgIW11bHRpcGxlICYmICF0cmVlQ2hlY2thYmxlICYmIHNlYXJjaFZhbHVlKSB7XG4gICAgICAgIF90aGlzLnNldFVuY29udHJvbGxlZFN0YXRlKHtcbiAgICAgICAgICBzZWFyY2hWYWx1ZTogJycsXG4gICAgICAgICAgZmlsdGVyZWRUcmVlTm9kZXM6IG51bGxcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIF90aGlzLnNldE9wZW5TdGF0ZShvcGVuLCB0cnVlKTtcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJvblNlYXJjaElucHV0Q2hhbmdlXCIsIGZ1bmN0aW9uIChfcmVmMTcpIHtcbiAgICAgIHZhciB2YWx1ZSA9IF9yZWYxNy50YXJnZXQudmFsdWU7XG4gICAgICB2YXIgX3RoaXMkc3RhdGU1ID0gX3RoaXMuc3RhdGUsXG4gICAgICAgICAgdHJlZU5vZGVzID0gX3RoaXMkc3RhdGU1LnRyZWVOb2RlcyxcbiAgICAgICAgICB2YWx1ZUVudGl0aWVzID0gX3RoaXMkc3RhdGU1LnZhbHVlRW50aXRpZXM7XG4gICAgICB2YXIgX3RoaXMkcHJvcHM1ID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgb25TZWFyY2ggPSBfdGhpcyRwcm9wczUub25TZWFyY2gsXG4gICAgICAgICAgZmlsdGVyVHJlZU5vZGUgPSBfdGhpcyRwcm9wczUuZmlsdGVyVHJlZU5vZGUsXG4gICAgICAgICAgdHJlZU5vZGVGaWx0ZXJQcm9wID0gX3RoaXMkcHJvcHM1LnRyZWVOb2RlRmlsdGVyUHJvcDtcblxuICAgICAgaWYgKG9uU2VhcmNoKSB7XG4gICAgICAgIG9uU2VhcmNoKHZhbHVlKTtcbiAgICAgIH1cblxuICAgICAgdmFyIGlzU2V0ID0gZmFsc2U7XG5cbiAgICAgIGlmICghX3RoaXMuaXNTZWFyY2hWYWx1ZUNvbnRyb2xsZWQoKSkge1xuICAgICAgICBpc1NldCA9IF90aGlzLnNldFVuY29udHJvbGxlZFN0YXRlKHtcbiAgICAgICAgICBzZWFyY2hWYWx1ZTogdmFsdWVcbiAgICAgICAgfSk7XG5cbiAgICAgICAgX3RoaXMuc2V0T3BlblN0YXRlKHRydWUpO1xuICAgICAgfVxuXG4gICAgICBpZiAoaXNTZXQpIHtcbiAgICAgICAgLy8gRG8gdGhlIHNlYXJjaCBsb2dpY1xuICAgICAgICB2YXIgdXBwZXJTZWFyY2hWYWx1ZSA9IFN0cmluZyh2YWx1ZSkudG9VcHBlckNhc2UoKTtcbiAgICAgICAgdmFyIGZpbHRlclRyZWVOb2RlRm4gPSBmaWx0ZXJUcmVlTm9kZTtcblxuICAgICAgICBpZiAoZmlsdGVyVHJlZU5vZGUgPT09IGZhbHNlKSB7XG4gICAgICAgICAgLy8gRG9uJ3QgZmlsdGVyIGlmIGlzIGZhbHNlXG4gICAgICAgICAgZmlsdGVyVHJlZU5vZGVGbiA9IGZ1bmN0aW9uIGZpbHRlclRyZWVOb2RlRm4oKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICB9O1xuICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiBmaWx0ZXJUcmVlTm9kZUZuICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgLy8gV2hlbiBpcyBub3QgZnVuY3Rpb24gKHRydWUgb3IgdW5kZWZpbmVkKSwgdXNlIGlubmVyIGZpbHRlclxuICAgICAgICAgIGZpbHRlclRyZWVOb2RlRm4gPSBmdW5jdGlvbiBmaWx0ZXJUcmVlTm9kZUZuKF8sIG5vZGUpIHtcbiAgICAgICAgICAgIHZhciBub2RlVmFsdWUgPSBTdHJpbmcobm9kZS5wcm9wc1t0cmVlTm9kZUZpbHRlclByb3BdKS50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgcmV0dXJuIG5vZGVWYWx1ZS5pbmRleE9mKHVwcGVyU2VhcmNoVmFsdWUpICE9PSAtMTtcbiAgICAgICAgICB9O1xuICAgICAgICB9XG5cbiAgICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGZpbHRlcmVkVHJlZU5vZGVzOiBnZXRGaWx0ZXJUcmVlKHRyZWVOb2RlcywgdmFsdWUsIGZpbHRlclRyZWVOb2RlRm4sIHZhbHVlRW50aXRpZXMsIFNlbGVjdE5vZGUpXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcIm9uU2VhcmNoSW5wdXRLZXlEb3duXCIsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgdmFyIF90aGlzJHN0YXRlNiA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgIHNlYXJjaFZhbHVlID0gX3RoaXMkc3RhdGU2LnNlYXJjaFZhbHVlLFxuICAgICAgICAgIHZhbHVlTGlzdCA9IF90aGlzJHN0YXRlNi52YWx1ZUxpc3Q7XG4gICAgICB2YXIga2V5Q29kZSA9IGV2ZW50LmtleUNvZGU7XG5cbiAgICAgIGlmIChLZXlDb2RlLkJBQ0tTUEFDRSA9PT0ga2V5Q29kZSAmJiBfdGhpcy5pc011bHRpcGxlKCkgJiYgIXNlYXJjaFZhbHVlICYmIHZhbHVlTGlzdC5sZW5ndGgpIHtcbiAgICAgICAgdmFyIGxhc3RWYWx1ZSA9IHZhbHVlTGlzdFt2YWx1ZUxpc3QubGVuZ3RoIC0gMV0udmFsdWU7XG5cbiAgICAgICAgX3RoaXMub25NdWx0aXBsZVNlbGVjdG9yUmVtb3ZlKGV2ZW50LCBsYXN0VmFsdWUpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcIm9uQ2hvaWNlQW5pbWF0aW9uTGVhdmVcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgcmFmKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgX3RoaXMuZm9yY2VQb3B1cEFsaWduKCk7XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJzZXRQb3B1cFJlZlwiLCBmdW5jdGlvbiAocG9wdXApIHtcbiAgICAgIF90aGlzLnBvcHVwID0gcG9wdXA7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwic2V0VW5jb250cm9sbGVkU3RhdGVcIiwgZnVuY3Rpb24gKHN0YXRlKSB7XG4gICAgICB2YXIgbmVlZFN5bmMgPSBmYWxzZTtcbiAgICAgIHZhciBuZXdTdGF0ZSA9IHt9O1xuICAgICAgT2JqZWN0LmtleXMoc3RhdGUpLmZvckVhY2goZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgaWYgKG5hbWUgaW4gX3RoaXMucHJvcHMpIHJldHVybjtcbiAgICAgICAgbmVlZFN5bmMgPSB0cnVlO1xuICAgICAgICBuZXdTdGF0ZVtuYW1lXSA9IHN0YXRlW25hbWVdO1xuICAgICAgfSk7XG5cbiAgICAgIGlmIChuZWVkU3luYykge1xuICAgICAgICBfdGhpcy5zZXRTdGF0ZShuZXdTdGF0ZSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBuZWVkU3luYztcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJzZXRPcGVuU3RhdGVcIiwgZnVuY3Rpb24gKG9wZW4pIHtcbiAgICAgIHZhciBieVRyaWdnZXIgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IGZhbHNlO1xuICAgICAgdmFyIG9uRHJvcGRvd25WaXNpYmxlQ2hhbmdlID0gX3RoaXMucHJvcHMub25Ecm9wZG93blZpc2libGVDaGFuZ2U7XG5cbiAgICAgIGlmIChvbkRyb3Bkb3duVmlzaWJsZUNoYW5nZSAmJiBvbkRyb3Bkb3duVmlzaWJsZUNoYW5nZShvcGVuLCB7XG4gICAgICAgIGRvY3VtZW50Q2xpY2tDbG9zZTogIW9wZW4gJiYgYnlUcmlnZ2VyXG4gICAgICB9KSA9PT0gZmFsc2UpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBfdGhpcy5zZXRVbmNvbnRyb2xsZWRTdGF0ZSh7XG4gICAgICAgIG9wZW46IG9wZW5cbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcImlzTXVsdGlwbGVcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzNiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIG11bHRpcGxlID0gX3RoaXMkcHJvcHM2Lm11bHRpcGxlLFxuICAgICAgICAgIHRyZWVDaGVja2FibGUgPSBfdGhpcyRwcm9wczYudHJlZUNoZWNrYWJsZTtcbiAgICAgIHJldHVybiAhIShtdWx0aXBsZSB8fCB0cmVlQ2hlY2thYmxlKTtcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJpc0xhYmVsSW5WYWx1ZVwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gaXNMYWJlbEluVmFsdWUoX3RoaXMucHJvcHMpO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcImlzU2VhcmNoVmFsdWVDb250cm9sbGVkXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBpbnB1dFZhbHVlID0gX3RoaXMucHJvcHMuaW5wdXRWYWx1ZTtcbiAgICAgIGlmICgnc2VhcmNoVmFsdWUnIGluIF90aGlzLnByb3BzKSByZXR1cm4gdHJ1ZTtcbiAgICAgIHJldHVybiAnaW5wdXRWYWx1ZScgaW4gX3RoaXMucHJvcHMgJiYgaW5wdXRWYWx1ZSAhPT0gbnVsbDtcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJmb3JjZVBvcHVwQWxpZ25cIiwgZnVuY3Rpb24gKCkge1xuICAgICAgdmFyICR0cmlnZ2VyID0gX3RoaXMuc2VsZWN0VHJpZ2dlclJlZi5jdXJyZW50O1xuXG4gICAgICBpZiAoJHRyaWdnZXIpIHtcbiAgICAgICAgJHRyaWdnZXIuZm9yY2VQb3B1cEFsaWduKCk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwiZGVsYXlGb3JjZVBvcHVwQWxpZ25cIiwgZnVuY3Rpb24gKCkge1xuICAgICAgLy8gV2FpdCAyIGZyYW1lIHRvIGF2b2lkIGRvbSB1cGRhdGUgJiBkb20gYWxnaW4gaW4gdGhlIHNhbWUgdGltZVxuICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTIwMzFcbiAgICAgIHJhZihmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJhZihfdGhpcy5mb3JjZVBvcHVwQWxpZ24pO1xuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwidHJpZ2dlckNoYW5nZVwiLCBmdW5jdGlvbiAobWlzc1ZhbHVlTGlzdCwgdmFsdWVMaXN0KSB7XG4gICAgICB2YXIgZXh0cmFJbmZvID0gYXJndW1lbnRzLmxlbmd0aCA+IDIgJiYgYXJndW1lbnRzWzJdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMl0gOiB7fTtcbiAgICAgIHZhciBfdGhpcyRzdGF0ZTcgPSBfdGhpcy5zdGF0ZSxcbiAgICAgICAgICB2YWx1ZUVudGl0aWVzID0gX3RoaXMkc3RhdGU3LnZhbHVlRW50aXRpZXMsXG4gICAgICAgICAgc2VhcmNoVmFsdWUgPSBfdGhpcyRzdGF0ZTcuc2VhcmNoVmFsdWUsXG4gICAgICAgICAgcHJldlNlbGVjdG9yVmFsdWVMaXN0ID0gX3RoaXMkc3RhdGU3LnNlbGVjdG9yVmFsdWVMaXN0O1xuICAgICAgdmFyIF90aGlzJHByb3BzNyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIG9uQ2hhbmdlID0gX3RoaXMkcHJvcHM3Lm9uQ2hhbmdlLFxuICAgICAgICAgIGRpc2FibGVkID0gX3RoaXMkcHJvcHM3LmRpc2FibGVkLFxuICAgICAgICAgIHRyZWVDaGVja2FibGUgPSBfdGhpcyRwcm9wczcudHJlZUNoZWNrYWJsZSxcbiAgICAgICAgICB0cmVlQ2hlY2tTdHJpY3RseSA9IF90aGlzJHByb3BzNy50cmVlQ2hlY2tTdHJpY3RseTtcbiAgICAgIGlmIChkaXNhYmxlZCkgcmV0dXJuOyAvLyBUcmlnZ2VyXG5cbiAgICAgIHZhciBleHRyYSA9IF9vYmplY3RTcHJlYWQoe1xuICAgICAgICAvLyBbTGVnYWN5XSBBbHdheXMgcmV0dXJuIGFzIGFycmF5IGNvbnRhaW5zIGxhYmVsICYgdmFsdWVcbiAgICAgICAgcHJlVmFsdWU6IHByZXZTZWxlY3RvclZhbHVlTGlzdC5tYXAoZnVuY3Rpb24gKF9yZWYxOCkge1xuICAgICAgICAgIHZhciBsYWJlbCA9IF9yZWYxOC5sYWJlbCxcbiAgICAgICAgICAgICAgdmFsdWUgPSBfcmVmMTgudmFsdWU7XG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGxhYmVsOiBsYWJlbCxcbiAgICAgICAgICAgIHZhbHVlOiB2YWx1ZVxuICAgICAgICAgIH07XG4gICAgICAgIH0pXG4gICAgICB9LCBleHRyYUluZm8pOyAvLyBGb3JtYXQgdmFsdWUgYnkgYHRyZWVDaGVja1N0cmljdGx5YFxuXG5cbiAgICAgIHZhciBzZWxlY3RvclZhbHVlTGlzdCA9IGZvcm1hdFNlbGVjdG9yVmFsdWUodmFsdWVMaXN0LCBfdGhpcy5wcm9wcywgdmFsdWVFbnRpdGllcyk7XG5cbiAgICAgIGlmICghKCd2YWx1ZScgaW4gX3RoaXMucHJvcHMpKSB7XG4gICAgICAgIHZhciBuZXdTdGF0ZSA9IHtcbiAgICAgICAgICBtaXNzVmFsdWVMaXN0OiBtaXNzVmFsdWVMaXN0LFxuICAgICAgICAgIHZhbHVlTGlzdDogdmFsdWVMaXN0LFxuICAgICAgICAgIHNlbGVjdG9yVmFsdWVMaXN0OiBzZWxlY3RvclZhbHVlTGlzdFxuICAgICAgICB9O1xuXG4gICAgICAgIGlmIChzZWFyY2hWYWx1ZSAmJiB0cmVlQ2hlY2thYmxlICYmICF0cmVlQ2hlY2tTdHJpY3RseSkge1xuICAgICAgICAgIG5ld1N0YXRlLnNlYXJjaEhhbGZDaGVja2VkS2V5cyA9IGdldEhhbGZDaGVja2VkS2V5cyh2YWx1ZUxpc3QsIHZhbHVlRW50aXRpZXMpO1xuICAgICAgICB9XG5cbiAgICAgICAgX3RoaXMuc2V0U3RhdGUobmV3U3RhdGUpO1xuICAgICAgfSAvLyBPbmx5IGRvIHRoZSBsb2dpYyB3aGVuIGBvbkNoYW5nZWAgZnVuY3Rpb24gcHJvdmlkZWRcblxuXG4gICAgICBpZiAob25DaGFuZ2UpIHtcbiAgICAgICAgdmFyIGNvbm5lY3RWYWx1ZUxpc3Q7IC8vIEdldCB2YWx1ZSBieSBtb2RlXG5cbiAgICAgICAgaWYgKF90aGlzLmlzTXVsdGlwbGUoKSkge1xuICAgICAgICAgIGNvbm5lY3RWYWx1ZUxpc3QgPSBbXS5jb25jYXQoX3RvQ29uc3VtYWJsZUFycmF5KG1pc3NWYWx1ZUxpc3QpLCBfdG9Db25zdW1hYmxlQXJyYXkoc2VsZWN0b3JWYWx1ZUxpc3QpKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb25uZWN0VmFsdWVMaXN0ID0gc2VsZWN0b3JWYWx1ZUxpc3Quc2xpY2UoMCwgMSk7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgbGFiZWxMaXN0ID0gbnVsbDtcbiAgICAgICAgdmFyIHJldHVyblZhbHVlO1xuXG4gICAgICAgIGlmIChfdGhpcy5pc0xhYmVsSW5WYWx1ZSgpKSB7XG4gICAgICAgICAgcmV0dXJuVmFsdWUgPSBjb25uZWN0VmFsdWVMaXN0Lm1hcChmdW5jdGlvbiAoX3JlZjE5KSB7XG4gICAgICAgICAgICB2YXIgbGFiZWwgPSBfcmVmMTkubGFiZWwsXG4gICAgICAgICAgICAgICAgdmFsdWUgPSBfcmVmMTkudmFsdWU7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICBsYWJlbDogbGFiZWwsXG4gICAgICAgICAgICAgIHZhbHVlOiB2YWx1ZVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBsYWJlbExpc3QgPSBbXTtcbiAgICAgICAgICByZXR1cm5WYWx1ZSA9IGNvbm5lY3RWYWx1ZUxpc3QubWFwKGZ1bmN0aW9uIChfcmVmMjApIHtcbiAgICAgICAgICAgIHZhciBsYWJlbCA9IF9yZWYyMC5sYWJlbCxcbiAgICAgICAgICAgICAgICB2YWx1ZSA9IF9yZWYyMC52YWx1ZTtcbiAgICAgICAgICAgIGxhYmVsTGlzdC5wdXNoKGxhYmVsKTtcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghX3RoaXMuaXNNdWx0aXBsZSgpKSB7XG4gICAgICAgICAgcmV0dXJuVmFsdWUgPSByZXR1cm5WYWx1ZVswXTtcbiAgICAgICAgfVxuXG4gICAgICAgIG9uQ2hhbmdlKHJldHVyblZhbHVlLCBsYWJlbExpc3QsIGV4dHJhKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHZhciBwcmVmaXhBcmlhID0gX3Byb3BzLnByZWZpeEFyaWEsXG4gICAgICAgIGRlZmF1bHRPcGVuID0gX3Byb3BzLmRlZmF1bHRPcGVuLFxuICAgICAgICBfb3BlbiA9IF9wcm9wcy5vcGVuO1xuICAgIF90aGlzLnN0YXRlID0ge1xuICAgICAgb3BlbjogX29wZW4gfHwgZGVmYXVsdE9wZW4sXG4gICAgICB2YWx1ZUxpc3Q6IFtdLFxuICAgICAgc2VhcmNoSGFsZkNoZWNrZWRLZXlzOiBbXSxcbiAgICAgIG1pc3NWYWx1ZUxpc3Q6IFtdLFxuICAgICAgLy8gQ29udGFpbnMgdGhlIHZhbHVlIG5vdCBpbiB0aGUgdHJlZVxuICAgICAgc2VsZWN0b3JWYWx1ZUxpc3Q6IFtdLFxuICAgICAgLy8gVXNlZCBmb3IgbXVsdGlwbGUgc2VsZWN0b3JcbiAgICAgIHZhbHVlRW50aXRpZXM6IHt9LFxuICAgICAga2V5RW50aXRpZXM6IHt9LFxuICAgICAgc2VhcmNoVmFsdWU6ICcnLFxuICAgICAgaW5pdDogdHJ1ZVxuICAgIH07XG4gICAgX3RoaXMuc2VsZWN0b3JSZWYgPSBjcmVhdGVSZWYoKTtcbiAgICBfdGhpcy5zZWxlY3RUcmlnZ2VyUmVmID0gY3JlYXRlUmVmKCk7IC8vIEFSSUEgbmVlZCBgYXJpYS1jb250cm9sc2AgcHJvcHMgbWFwcGluZ1xuICAgIC8vIFNpbmNlIHRoaXMgbmVlZCB1c2VyIGlucHV0LiBMZXQncyBnZW5lcmF0ZSBvdXJzZWx2ZXNcblxuICAgIF90aGlzLmFyaWFJZCA9IGdlbmVyYXRlQXJpYUlkKFwiXCIuY29uY2F0KHByZWZpeEFyaWEsIFwiLWxpc3RcIikpO1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhTZWxlY3QsIFt7XG4gICAga2V5OiBcImdldENoaWxkQ29udGV4dFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRDaGlsZENvbnRleHQoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICByY1RyZWVTZWxlY3Q6IHtcbiAgICAgICAgICBvblNlbGVjdG9yRm9jdXM6IHRoaXMub25TZWxlY3RvckZvY3VzLFxuICAgICAgICAgIG9uU2VsZWN0b3JCbHVyOiB0aGlzLm9uU2VsZWN0b3JCbHVyLFxuICAgICAgICAgIG9uU2VsZWN0b3JLZXlEb3duOiB0aGlzLm9uQ29tcG9uZW50S2V5RG93bixcbiAgICAgICAgICBvblNlbGVjdG9yQ2xlYXI6IHRoaXMub25TZWxlY3RvckNsZWFyLFxuICAgICAgICAgIG9uTXVsdGlwbGVTZWxlY3RvclJlbW92ZTogdGhpcy5vbk11bHRpcGxlU2VsZWN0b3JSZW1vdmUsXG4gICAgICAgICAgb25UcmVlTm9kZVNlbGVjdDogdGhpcy5vblRyZWVOb2RlU2VsZWN0LFxuICAgICAgICAgIG9uVHJlZU5vZGVDaGVjazogdGhpcy5vblRyZWVOb2RlQ2hlY2ssXG4gICAgICAgICAgb25Qb3B1cEtleURvd246IHRoaXMub25Db21wb25lbnRLZXlEb3duLFxuICAgICAgICAgIG9uU2VhcmNoSW5wdXRDaGFuZ2U6IHRoaXMub25TZWFyY2hJbnB1dENoYW5nZSxcbiAgICAgICAgICBvblNlYXJjaElucHV0S2V5RG93bjogdGhpcy5vblNlYXJjaElucHV0S2V5RG93blxuICAgICAgICB9XG4gICAgICB9O1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJjb21wb25lbnREaWRNb3VudFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczggPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGF1dG9Gb2N1cyA9IF90aGlzJHByb3BzOC5hdXRvRm9jdXMsXG4gICAgICAgICAgZGlzYWJsZWQgPSBfdGhpcyRwcm9wczguZGlzYWJsZWQ7XG5cbiAgICAgIGlmIChhdXRvRm9jdXMgJiYgIWRpc2FibGVkKSB7XG4gICAgICAgIHRoaXMuZm9jdXMoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiY29tcG9uZW50RGlkVXBkYXRlXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZShfLCBwcmV2U3RhdGUpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICB2YXIgcHJlZml4Q2xzID0gdGhpcy5wcm9wcy5wcmVmaXhDbHM7XG4gICAgICB2YXIgX3RoaXMkc3RhdGU4ID0gdGhpcy5zdGF0ZSxcbiAgICAgICAgICB2YWx1ZUxpc3QgPSBfdGhpcyRzdGF0ZTgudmFsdWVMaXN0LFxuICAgICAgICAgIG9wZW4gPSBfdGhpcyRzdGF0ZTgub3BlbixcbiAgICAgICAgICBzZWxlY3RvclZhbHVlTGlzdCA9IF90aGlzJHN0YXRlOC5zZWxlY3RvclZhbHVlTGlzdCxcbiAgICAgICAgICB2YWx1ZUVudGl0aWVzID0gX3RoaXMkc3RhdGU4LnZhbHVlRW50aXRpZXM7XG4gICAgICB2YXIgaXNNdWx0aXBsZSA9IHRoaXMuaXNNdWx0aXBsZSgpO1xuXG4gICAgICBpZiAocHJldlN0YXRlLnZhbHVlTGlzdCAhPT0gdmFsdWVMaXN0KSB7XG4gICAgICAgIHRoaXMuZm9yY2VQb3B1cEFsaWduKCk7XG4gICAgICB9IC8vIFNjcm9sbCB0byB2YWx1ZSBwb3NpdGlvbiwgb25seSBuZWVkIHN5bmMgb24gc2luZ2xlIG1vZGVcblxuXG4gICAgICBpZiAoIWlzTXVsdGlwbGUgJiYgc2VsZWN0b3JWYWx1ZUxpc3QubGVuZ3RoICYmICFwcmV2U3RhdGUub3BlbiAmJiBvcGVuICYmIHRoaXMucG9wdXApIHtcbiAgICAgICAgdmFyIHZhbHVlID0gc2VsZWN0b3JWYWx1ZUxpc3RbMF0udmFsdWU7XG5cbiAgICAgICAgdmFyIF90aGlzJHBvcHVwJGdldFRyZWUgPSB0aGlzLnBvcHVwLmdldFRyZWUoKSxcbiAgICAgICAgICAgIGRvbVRyZWVOb2RlcyA9IF90aGlzJHBvcHVwJGdldFRyZWUuZG9tVHJlZU5vZGVzO1xuXG4gICAgICAgIHZhciBfcmVmMjEgPSB2YWx1ZUVudGl0aWVzW3ZhbHVlXSB8fCB7fSxcbiAgICAgICAgICAgIGtleSA9IF9yZWYyMS5rZXk7XG5cbiAgICAgICAgdmFyIHRyZWVOb2RlID0gZG9tVHJlZU5vZGVzW2tleV07XG5cbiAgICAgICAgaWYgKHRyZWVOb2RlKSB7XG4gICAgICAgICAgdmFyIGRvbU5vZGUgPSBmaW5kRE9NTm9kZSh0cmVlTm9kZSk7XG4gICAgICAgICAgcmFmKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBwb3B1cE5vZGUgPSBmaW5kRE9NTm9kZShfdGhpczIucG9wdXApO1xuICAgICAgICAgICAgdmFyIHRyaWdnZXJDb250YWluZXIgPSBmaW5kUG9wdXBDb250YWluZXIocG9wdXBOb2RlLCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWRyb3Bkb3duXCIpKTtcbiAgICAgICAgICAgIHZhciBzZWFyY2hOb2RlID0gX3RoaXMyLnBvcHVwLnNlYXJjaFJlZi5jdXJyZW50O1xuXG4gICAgICAgICAgICBpZiAoZG9tTm9kZSAmJiB0cmlnZ2VyQ29udGFpbmVyICYmIHNlYXJjaE5vZGUpIHtcbiAgICAgICAgICAgICAgc2Nyb2xsSW50b1ZpZXcoZG9tTm9kZSwgdHJpZ2dlckNvbnRhaW5lciwge1xuICAgICAgICAgICAgICAgIG9ubHlTY3JvbGxJZk5lZWRlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICBvZmZzZXRUb3A6IHNlYXJjaE5vZGUub2Zmc2V0SGVpZ2h0XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSAvLyA9PT09PT09PT09PT09PT09PT09PSBTZWxlY3RvciA9PT09PT09PT09PT09PT09PT09PVxuXG4gIH0sIHtcbiAgICBrZXk6IFwiZm9jdXNcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gZm9jdXMoKSB7XG4gICAgICB0aGlzLnNlbGVjdG9yUmVmLmN1cnJlbnQuZm9jdXMoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiYmx1clwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBibHVyKCkge1xuICAgICAgdGhpcy5zZWxlY3RvclJlZi5jdXJyZW50LmJsdXIoKTtcbiAgICB9IC8vID09PT09PT09PT09PT09PT09PT09PSBSZW5kZXIgPT09PT09PT09PT09PT09PT09PT09XG5cbiAgfSwge1xuICAgIGtleTogXCJyZW5kZXJcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzJHN0YXRlOSA9IHRoaXMuc3RhdGUsXG4gICAgICAgICAgdmFsdWVMaXN0ID0gX3RoaXMkc3RhdGU5LnZhbHVlTGlzdCxcbiAgICAgICAgICBtaXNzVmFsdWVMaXN0ID0gX3RoaXMkc3RhdGU5Lm1pc3NWYWx1ZUxpc3QsXG4gICAgICAgICAgc2VsZWN0b3JWYWx1ZUxpc3QgPSBfdGhpcyRzdGF0ZTkuc2VsZWN0b3JWYWx1ZUxpc3QsXG4gICAgICAgICAgc2VhcmNoSGFsZkNoZWNrZWRLZXlzID0gX3RoaXMkc3RhdGU5LnNlYXJjaEhhbGZDaGVja2VkS2V5cyxcbiAgICAgICAgICB2YWx1ZUVudGl0aWVzID0gX3RoaXMkc3RhdGU5LnZhbHVlRW50aXRpZXMsXG4gICAgICAgICAga2V5RW50aXRpZXMgPSBfdGhpcyRzdGF0ZTkua2V5RW50aXRpZXMsXG4gICAgICAgICAgc2VhcmNoVmFsdWUgPSBfdGhpcyRzdGF0ZTkuc2VhcmNoVmFsdWUsXG4gICAgICAgICAgb3BlbiA9IF90aGlzJHN0YXRlOS5vcGVuLFxuICAgICAgICAgIGZvY3VzZWQgPSBfdGhpcyRzdGF0ZTkuZm9jdXNlZCxcbiAgICAgICAgICB0cmVlTm9kZXMgPSBfdGhpcyRzdGF0ZTkudHJlZU5vZGVzLFxuICAgICAgICAgIGZpbHRlcmVkVHJlZU5vZGVzID0gX3RoaXMkc3RhdGU5LmZpbHRlcmVkVHJlZU5vZGVzO1xuICAgICAgdmFyIF90aGlzJHByb3BzOSA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3RoaXMkcHJvcHM5LnByZWZpeENscyxcbiAgICAgICAgICB0cmVlRXhwYW5kZWRLZXlzID0gX3RoaXMkcHJvcHM5LnRyZWVFeHBhbmRlZEtleXMsXG4gICAgICAgICAgb25UcmVlRXhwYW5kID0gX3RoaXMkcHJvcHM5Lm9uVHJlZUV4cGFuZDtcbiAgICAgIHZhciBpc011bHRpcGxlID0gdGhpcy5pc011bHRpcGxlKCk7XG5cbiAgICAgIHZhciBwYXNzUHJvcHMgPSBfb2JqZWN0U3ByZWFkKHt9LCB0aGlzLnByb3BzLCB7XG4gICAgICAgIGlzTXVsdGlwbGU6IGlzTXVsdGlwbGUsXG4gICAgICAgIHZhbHVlTGlzdDogdmFsdWVMaXN0LFxuICAgICAgICBzZWFyY2hIYWxmQ2hlY2tlZEtleXM6IHNlYXJjaEhhbGZDaGVja2VkS2V5cyxcbiAgICAgICAgc2VsZWN0b3JWYWx1ZUxpc3Q6IFtdLmNvbmNhdChfdG9Db25zdW1hYmxlQXJyYXkobWlzc1ZhbHVlTGlzdCksIF90b0NvbnN1bWFibGVBcnJheShzZWxlY3RvclZhbHVlTGlzdCkpLFxuICAgICAgICB2YWx1ZUVudGl0aWVzOiB2YWx1ZUVudGl0aWVzLFxuICAgICAgICBrZXlFbnRpdGllczoga2V5RW50aXRpZXMsXG4gICAgICAgIHNlYXJjaFZhbHVlOiBzZWFyY2hWYWx1ZSxcbiAgICAgICAgdXBwZXJTZWFyY2hWYWx1ZTogKHNlYXJjaFZhbHVlIHx8ICcnKS50b1VwcGVyQ2FzZSgpLFxuICAgICAgICAvLyBQZXJmIHNhdmVcbiAgICAgICAgb3Blbjogb3BlbixcbiAgICAgICAgZm9jdXNlZDogZm9jdXNlZCxcbiAgICAgICAgb25DaG9pY2VBbmltYXRpb25MZWF2ZTogdGhpcy5vbkNob2ljZUFuaW1hdGlvbkxlYXZlLFxuICAgICAgICBkcm9wZG93blByZWZpeENsczogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1kcm9wZG93blwiKSxcbiAgICAgICAgYXJpYUlkOiB0aGlzLmFyaWFJZFxuICAgICAgfSk7XG5cbiAgICAgIHZhciBQb3B1cCA9IGlzTXVsdGlwbGUgPyBNdWx0aXBsZVBvcHVwIDogU2luZ2xlUG9wdXA7XG4gICAgICB2YXIgJHBvcHVwID0gUmVhY3QuY3JlYXRlRWxlbWVudChQb3B1cCwgX2V4dGVuZHMoe1xuICAgICAgICByZWY6IHRoaXMuc2V0UG9wdXBSZWZcbiAgICAgIH0sIHBhc3NQcm9wcywge1xuICAgICAgICBvblRyZWVFeHBhbmRlZDogdGhpcy5kZWxheUZvcmNlUG9wdXBBbGlnbixcbiAgICAgICAgdHJlZU5vZGVzOiB0cmVlTm9kZXMsXG4gICAgICAgIGZpbHRlcmVkVHJlZU5vZGVzOiBmaWx0ZXJlZFRyZWVOb2RlcyAvLyBUcmVlIGV4cGFuZGVkIGNvbnRyb2xcbiAgICAgICAgLFxuICAgICAgICB0cmVlRXhwYW5kZWRLZXlzOiB0cmVlRXhwYW5kZWRLZXlzLFxuICAgICAgICBvblRyZWVFeHBhbmQ6IG9uVHJlZUV4cGFuZFxuICAgICAgfSkpO1xuICAgICAgdmFyIFNlbGVjdG9yID0gaXNNdWx0aXBsZSA/IE11bHRpcGxlU2VsZWN0b3IgOiBTaW5nbGVTZWxlY3RvcjtcbiAgICAgIHZhciAkc2VsZWN0b3IgPSBSZWFjdC5jcmVhdGVFbGVtZW50KFNlbGVjdG9yLCBfZXh0ZW5kcyh7fSwgcGFzc1Byb3BzLCB7XG4gICAgICAgIHJlZjogdGhpcy5zZWxlY3RvclJlZlxuICAgICAgfSkpO1xuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU2VsZWN0VHJpZ2dlciwgX2V4dGVuZHMoe30sIHBhc3NQcm9wcywge1xuICAgICAgICByZWY6IHRoaXMuc2VsZWN0VHJpZ2dlclJlZixcbiAgICAgICAgcG9wdXBFbGVtZW50OiAkcG9wdXAsXG4gICAgICAgIG9uS2V5RG93bjogdGhpcy5vbktleURvd24sXG4gICAgICAgIG9uRHJvcGRvd25WaXNpYmxlQ2hhbmdlOiB0aGlzLm9uRHJvcGRvd25WaXNpYmxlQ2hhbmdlXG4gICAgICB9KSwgJHNlbGVjdG9yKTtcbiAgICB9XG4gIH1dLCBbe1xuICAgIGtleTogXCJnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHNcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzKG5leHRQcm9wcywgcHJldlN0YXRlKSB7XG4gICAgICB2YXIgX3ByZXZTdGF0ZSRwcmV2UHJvcHMgPSBwcmV2U3RhdGUucHJldlByb3BzLFxuICAgICAgICAgIHByZXZQcm9wcyA9IF9wcmV2U3RhdGUkcHJldlByb3BzID09PSB2b2lkIDAgPyB7fSA6IF9wcmV2U3RhdGUkcHJldlByb3BzO1xuICAgICAgdmFyIHRyZWVDaGVja2FibGUgPSBuZXh0UHJvcHMudHJlZUNoZWNrYWJsZSxcbiAgICAgICAgICB0cmVlQ2hlY2tTdHJpY3RseSA9IG5leHRQcm9wcy50cmVlQ2hlY2tTdHJpY3RseSxcbiAgICAgICAgICBmaWx0ZXJUcmVlTm9kZSA9IG5leHRQcm9wcy5maWx0ZXJUcmVlTm9kZSxcbiAgICAgICAgICB0cmVlTm9kZUZpbHRlclByb3AgPSBuZXh0UHJvcHMudHJlZU5vZGVGaWx0ZXJQcm9wLFxuICAgICAgICAgIHRyZWVEYXRhU2ltcGxlTW9kZSA9IG5leHRQcm9wcy50cmVlRGF0YVNpbXBsZU1vZGU7XG4gICAgICB2YXIgbmV3U3RhdGUgPSB7XG4gICAgICAgIHByZXZQcm9wczogbmV4dFByb3BzLFxuICAgICAgICBpbml0OiBmYWxzZVxuICAgICAgfTsgLy8gUHJvY2VzcyB0aGUgc3RhdGUgd2hlbiBwcm9wcyB1cGRhdGVkXG5cbiAgICAgIGZ1bmN0aW9uIHByb2Nlc3NTdGF0ZShwcm9wTmFtZSwgdXBkYXRlcikge1xuICAgICAgICBpZiAocHJldlByb3BzW3Byb3BOYW1lXSAhPT0gbmV4dFByb3BzW3Byb3BOYW1lXSkge1xuICAgICAgICAgIHVwZGF0ZXIobmV4dFByb3BzW3Byb3BOYW1lXSwgcHJldlByb3BzW3Byb3BOYW1lXSk7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHZhciB2YWx1ZVJlZnJlc2ggPSBmYWxzZTsgLy8gT3BlblxuXG4gICAgICBwcm9jZXNzU3RhdGUoJ29wZW4nLCBmdW5jdGlvbiAocHJvcFZhbHVlKSB7XG4gICAgICAgIG5ld1N0YXRlLm9wZW4gPSBwcm9wVmFsdWU7XG4gICAgICB9KTsgLy8gVHJlZSBOb2Rlc1xuXG4gICAgICB2YXIgdHJlZU5vZGVzO1xuICAgICAgdmFyIHRyZWVEYXRhQ2hhbmdlZCA9IGZhbHNlO1xuICAgICAgdmFyIHRyZWVEYXRhTW9kZUNoYW5nZWQgPSBmYWxzZTtcbiAgICAgIHByb2Nlc3NTdGF0ZSgndHJlZURhdGEnLCBmdW5jdGlvbiAocHJvcFZhbHVlKSB7XG4gICAgICAgIHRyZWVOb2RlcyA9IGNvbnZlcnREYXRhVG9UcmVlKHByb3BWYWx1ZSk7XG4gICAgICAgIHRyZWVEYXRhQ2hhbmdlZCA9IHRydWU7XG4gICAgICB9KTtcbiAgICAgIHByb2Nlc3NTdGF0ZSgndHJlZURhdGFTaW1wbGVNb2RlJywgZnVuY3Rpb24gKHByb3BWYWx1ZSwgcHJldlZhbHVlKSB7XG4gICAgICAgIGlmICghcHJvcFZhbHVlKSByZXR1cm47XG4gICAgICAgIHZhciBwcmV2ID0gIXByZXZWYWx1ZSB8fCBwcmV2VmFsdWUgPT09IHRydWUgPyB7fSA6IHByZXZWYWx1ZTsgLy8gU2hhbGxvdyBlcXVhbCB0byBhdm9pZCBkeW5hbWljIHByb3Agb2JqZWN0XG5cbiAgICAgICAgaWYgKCFzaGFsbG93RXF1YWwocHJvcFZhbHVlLCBwcmV2KSkge1xuICAgICAgICAgIHRyZWVEYXRhTW9kZUNoYW5nZWQgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9KTsgLy8gUGFyc2UgYnkgYHRyZWVEYXRhU2ltcGxlTW9kZWBcblxuICAgICAgaWYgKHRyZWVEYXRhU2ltcGxlTW9kZSAmJiAodHJlZURhdGFDaGFuZ2VkIHx8IHRyZWVEYXRhTW9kZUNoYW5nZWQpKSB7XG4gICAgICAgIHZhciBzaW1wbGVNYXBwZXIgPSBfb2JqZWN0U3ByZWFkKHtcbiAgICAgICAgICBpZDogJ2lkJyxcbiAgICAgICAgICBwSWQ6ICdwSWQnLFxuICAgICAgICAgIHJvb3RQSWQ6IG51bGxcbiAgICAgICAgfSwgdHJlZURhdGFTaW1wbGVNb2RlICE9PSB0cnVlID8gdHJlZURhdGFTaW1wbGVNb2RlIDoge30pO1xuXG4gICAgICAgIHRyZWVOb2RlcyA9IGNvbnZlcnREYXRhVG9UcmVlKHBhcnNlU2ltcGxlVHJlZURhdGEobmV4dFByb3BzLnRyZWVEYXRhLCBzaW1wbGVNYXBwZXIpKTtcbiAgICAgIH0gLy8gSWYgYHRyZWVEYXRhYCBub3QgcHJvdmlkZSwgdXNlIGNoaWxkcmVuIFRyZWVOb2Rlc1xuXG5cbiAgICAgIGlmICghbmV4dFByb3BzLnRyZWVEYXRhKSB7XG4gICAgICAgIHByb2Nlc3NTdGF0ZSgnY2hpbGRyZW4nLCBmdW5jdGlvbiAocHJvcFZhbHVlKSB7XG4gICAgICAgICAgdHJlZU5vZGVzID0gQXJyYXkuaXNBcnJheShwcm9wVmFsdWUpID8gcHJvcFZhbHVlIDogW3Byb3BWYWx1ZV07XG4gICAgICAgIH0pO1xuICAgICAgfSAvLyBDb252ZXJ0IGB0cmVlRGF0YWAgdG8gZW50aXRpZXNcblxuXG4gICAgICBpZiAodHJlZU5vZGVzKSB7XG4gICAgICAgIHZhciBlbnRpdGllc01hcCA9IGNvbnZlcnRUcmVlVG9FbnRpdGllcyh0cmVlTm9kZXMpO1xuICAgICAgICBuZXdTdGF0ZS50cmVlTm9kZXMgPSB0cmVlTm9kZXM7XG4gICAgICAgIG5ld1N0YXRlLnBvc0VudGl0aWVzID0gZW50aXRpZXNNYXAucG9zRW50aXRpZXM7XG4gICAgICAgIG5ld1N0YXRlLnZhbHVlRW50aXRpZXMgPSBlbnRpdGllc01hcC52YWx1ZUVudGl0aWVzO1xuICAgICAgICBuZXdTdGF0ZS5rZXlFbnRpdGllcyA9IGVudGl0aWVzTWFwLmtleUVudGl0aWVzO1xuICAgICAgICB2YWx1ZVJlZnJlc2ggPSB0cnVlO1xuICAgICAgfSAvLyBWYWx1ZSBMaXN0XG5cblxuICAgICAgaWYgKHByZXZTdGF0ZS5pbml0KSB7XG4gICAgICAgIHByb2Nlc3NTdGF0ZSgnZGVmYXVsdFZhbHVlJywgZnVuY3Rpb24gKHByb3BWYWx1ZSkge1xuICAgICAgICAgIG5ld1N0YXRlLnZhbHVlTGlzdCA9IGZvcm1hdEludGVybmFsVmFsdWUocHJvcFZhbHVlLCBuZXh0UHJvcHMpO1xuICAgICAgICAgIHZhbHVlUmVmcmVzaCA9IHRydWU7XG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICBwcm9jZXNzU3RhdGUoJ3ZhbHVlJywgZnVuY3Rpb24gKHByb3BWYWx1ZSkge1xuICAgICAgICBuZXdTdGF0ZS52YWx1ZUxpc3QgPSBmb3JtYXRJbnRlcm5hbFZhbHVlKHByb3BWYWx1ZSwgbmV4dFByb3BzKTtcbiAgICAgICAgdmFsdWVSZWZyZXNoID0gdHJ1ZTtcbiAgICAgIH0pOyAvLyBTZWxlY3RvciBWYWx1ZSBMaXN0XG5cbiAgICAgIGlmICh2YWx1ZVJlZnJlc2gpIHtcbiAgICAgICAgLy8gRmluZCBvdXQgdGhhdCB2YWx1ZSBub3QgZXhpc3QgaW4gdGhlIHRyZWVcbiAgICAgICAgdmFyIG1pc3NWYWx1ZUxpc3QgPSBbXTtcbiAgICAgICAgdmFyIGZpbHRlcmVkVmFsdWVMaXN0ID0gW107XG4gICAgICAgIHZhciBrZXlMaXN0ID0gW107IC8vIEdldCBsYXRlc3QgdmFsdWUgbGlzdFxuXG4gICAgICAgIHZhciBsYXRlc3RWYWx1ZUxpc3QgPSBuZXdTdGF0ZS52YWx1ZUxpc3Q7XG5cbiAgICAgICAgaWYgKCFsYXRlc3RWYWx1ZUxpc3QpIHtcbiAgICAgICAgICAvLyBBbHNvIG5lZWQgYWRkIHByZXYgbWlzc1ZhbHVlTGlzdCB0byBhdm9pZCBuZXcgdHJlZU5vZGVzIGNvbnRhaW5zIHRoZSB2YWx1ZVxuICAgICAgICAgIGxhdGVzdFZhbHVlTGlzdCA9IFtdLmNvbmNhdChfdG9Db25zdW1hYmxlQXJyYXkocHJldlN0YXRlLnZhbHVlTGlzdCksIF90b0NvbnN1bWFibGVBcnJheShwcmV2U3RhdGUubWlzc1ZhbHVlTGlzdCkpO1xuICAgICAgICB9IC8vIEdldCBrZXkgYnkgdmFsdWVcblxuXG4gICAgICAgIHZhciB2YWx1ZUxhYmVscyA9IHt9O1xuICAgICAgICBsYXRlc3RWYWx1ZUxpc3QuZm9yRWFjaChmdW5jdGlvbiAod3JhcHBlclZhbHVlKSB7XG4gICAgICAgICAgdmFyIHZhbHVlID0gd3JhcHBlclZhbHVlLnZhbHVlLFxuICAgICAgICAgICAgICBsYWJlbCA9IHdyYXBwZXJWYWx1ZS5sYWJlbDtcbiAgICAgICAgICB2YXIgZW50aXR5ID0gKG5ld1N0YXRlLnZhbHVlRW50aXRpZXMgfHwgcHJldlN0YXRlLnZhbHVlRW50aXRpZXMpW3ZhbHVlXTtcbiAgICAgICAgICB2YWx1ZUxhYmVsc1t2YWx1ZV0gPSBsYWJlbDtcblxuICAgICAgICAgIGlmIChlbnRpdHkpIHtcbiAgICAgICAgICAgIGtleUxpc3QucHVzaChlbnRpdHkua2V5KTtcbiAgICAgICAgICAgIGZpbHRlcmVkVmFsdWVMaXN0LnB1c2god3JhcHBlclZhbHVlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9IC8vIElmIG5vdCBtYXRjaCwgaXQgbWF5IGNhdXNlZCBieSBhamF4IGxvYWQuIFdlIG5lZWQga2VlcCB0aGlzXG5cblxuICAgICAgICAgIG1pc3NWYWx1ZUxpc3QucHVzaCh3cmFwcGVyVmFsdWUpO1xuICAgICAgICB9KTsgLy8gV2UgbmVlZCBjYWxjdWxhdGUgdGhlIHZhbHVlIHdoZW4gdHJlZSBpcyBjaGVja2VkIHRyZWVcblxuICAgICAgICBpZiAodHJlZUNoZWNrYWJsZSAmJiAhdHJlZUNoZWNrU3RyaWN0bHkpIHtcbiAgICAgICAgICAvLyBDYWxjdWxhdGUgdGhlIGtleXMgbmVlZCB0byBiZSBjaGVja2VkXG4gICAgICAgICAgdmFyIF9jb25kdWN0Q2hlY2sgPSBjb25kdWN0Q2hlY2soa2V5TGlzdCwgdHJ1ZSwgbmV3U3RhdGUua2V5RW50aXRpZXMgfHwgcHJldlN0YXRlLmtleUVudGl0aWVzKSxcbiAgICAgICAgICAgICAgY2hlY2tlZEtleXMgPSBfY29uZHVjdENoZWNrLmNoZWNrZWRLZXlzOyAvLyBGb3JtYXQgdmFsdWUgbGlzdCBhZ2FpbiBmb3IgaW50ZXJuYWwgdXNhZ2VcblxuXG4gICAgICAgICAgbmV3U3RhdGUudmFsdWVMaXN0ID0gY2hlY2tlZEtleXMubWFwKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIHZhciB2YWwgPSAobmV3U3RhdGUua2V5RW50aXRpZXMgfHwgcHJldlN0YXRlLmtleUVudGl0aWVzKVtrZXldLnZhbHVlO1xuICAgICAgICAgICAgdmFyIHdyYXBwZWRWYWx1ZSA9IHtcbiAgICAgICAgICAgICAgdmFsdWU6IHZhbFxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgaWYgKHZhbHVlTGFiZWxzW3ZhbF0gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICB3cmFwcGVkVmFsdWUubGFiZWwgPSB2YWx1ZUxhYmVsc1t2YWxdO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gd3JhcHBlZFZhbHVlO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIG5ld1N0YXRlLnZhbHVlTGlzdCA9IGZpbHRlcmVkVmFsdWVMaXN0O1xuICAgICAgICB9IC8vIEZpbGwgdGhlIG1pc3NWYWx1ZUxpc3QsIHdlIHN0aWxsIG5lZWQgZGlzcGxheSBpbiB0aGUgc2VsZWN0b3JcblxuXG4gICAgICAgIG5ld1N0YXRlLm1pc3NWYWx1ZUxpc3QgPSBtaXNzVmFsdWVMaXN0OyAvLyBDYWxjdWxhdGUgdGhlIHZhbHVlIGxpc3QgZm9yIGBTZWxlY3RvcmAgdXNhZ2VcblxuICAgICAgICBuZXdTdGF0ZS5zZWxlY3RvclZhbHVlTGlzdCA9IGZvcm1hdFNlbGVjdG9yVmFsdWUobmV3U3RhdGUudmFsdWVMaXN0LCBuZXh0UHJvcHMsIG5ld1N0YXRlLnZhbHVlRW50aXRpZXMgfHwgcHJldlN0YXRlLnZhbHVlRW50aXRpZXMpO1xuICAgICAgfSAvLyBbTGVnYWN5XSBUbyBhbGlnbiB3aXRoIGBTZWxlY3RgIGNvbXBvbmVudCxcbiAgICAgIC8vIFdlIHVzZSBgc2VhcmNoVmFsdWVgIGluc3RlYWQgb2YgYGlucHV0VmFsdWVgIGJ1dCBzdGlsbCBrZWVwIHRoZSBhcGlcbiAgICAgIC8vIGBpbnB1dFZhbHVlYCBzdXBwb3J0IGBudWxsYCB0byB3b3JrIGFzIGBhdXRvQ2xlYXJTZWFyY2hWYWx1ZWBcblxuXG4gICAgICBwcm9jZXNzU3RhdGUoJ2lucHV0VmFsdWUnLCBmdW5jdGlvbiAocHJvcFZhbHVlKSB7XG4gICAgICAgIGlmIChwcm9wVmFsdWUgIT09IG51bGwpIHtcbiAgICAgICAgICBuZXdTdGF0ZS5zZWFyY2hWYWx1ZSA9IHByb3BWYWx1ZTtcbiAgICAgICAgfVxuICAgICAgfSk7IC8vIFNlYXJjaCB2YWx1ZVxuXG4gICAgICBwcm9jZXNzU3RhdGUoJ3NlYXJjaFZhbHVlJywgZnVuY3Rpb24gKHByb3BWYWx1ZSkge1xuICAgICAgICBuZXdTdGF0ZS5zZWFyY2hWYWx1ZSA9IHByb3BWYWx1ZTtcbiAgICAgIH0pOyAvLyBEbyB0aGUgc2VhcmNoIGxvZ2ljXG5cbiAgICAgIGlmIChuZXdTdGF0ZS5zZWFyY2hWYWx1ZSAhPT0gdW5kZWZpbmVkIHx8IHByZXZTdGF0ZS5zZWFyY2hWYWx1ZSAmJiB0cmVlTm9kZXMpIHtcbiAgICAgICAgdmFyIHNlYXJjaFZhbHVlID0gbmV3U3RhdGUuc2VhcmNoVmFsdWUgIT09IHVuZGVmaW5lZCA/IG5ld1N0YXRlLnNlYXJjaFZhbHVlIDogcHJldlN0YXRlLnNlYXJjaFZhbHVlO1xuICAgICAgICB2YXIgdXBwZXJTZWFyY2hWYWx1ZSA9IFN0cmluZyhzZWFyY2hWYWx1ZSkudG9VcHBlckNhc2UoKTtcbiAgICAgICAgdmFyIGZpbHRlclRyZWVOb2RlRm4gPSBmaWx0ZXJUcmVlTm9kZTtcblxuICAgICAgICBpZiAoZmlsdGVyVHJlZU5vZGUgPT09IGZhbHNlKSB7XG4gICAgICAgICAgLy8gRG9uJ3QgZmlsdGVyIGlmIGlzIGZhbHNlXG4gICAgICAgICAgZmlsdGVyVHJlZU5vZGVGbiA9IGZ1bmN0aW9uIGZpbHRlclRyZWVOb2RlRm4oKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICB9O1xuICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiBmaWx0ZXJUcmVlTm9kZUZuICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgLy8gV2hlbiBpcyBub3QgZnVuY3Rpb24gKHRydWUgb3IgdW5kZWZpbmVkKSwgdXNlIGlubmVyIGZpbHRlclxuICAgICAgICAgIGZpbHRlclRyZWVOb2RlRm4gPSBmdW5jdGlvbiBmaWx0ZXJUcmVlTm9kZUZuKF8sIG5vZGUpIHtcbiAgICAgICAgICAgIHZhciBub2RlVmFsdWUgPSBTdHJpbmcobm9kZS5wcm9wc1t0cmVlTm9kZUZpbHRlclByb3BdKS50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgcmV0dXJuIG5vZGVWYWx1ZS5pbmRleE9mKHVwcGVyU2VhcmNoVmFsdWUpICE9PSAtMTtcbiAgICAgICAgICB9O1xuICAgICAgICB9XG5cbiAgICAgICAgbmV3U3RhdGUuZmlsdGVyZWRUcmVlTm9kZXMgPSBnZXRGaWx0ZXJUcmVlKG5ld1N0YXRlLnRyZWVOb2RlcyB8fCBwcmV2U3RhdGUudHJlZU5vZGVzLCBzZWFyY2hWYWx1ZSwgZmlsdGVyVHJlZU5vZGVGbiwgbmV3U3RhdGUudmFsdWVFbnRpdGllcyB8fCBwcmV2U3RhdGUudmFsdWVFbnRpdGllcywgU2VsZWN0Tm9kZSk7XG4gICAgICB9IC8vIFdlIHNob3VsZCByZS1jYWxjdWxhdGUgdGhlIGhhbGZDaGVja2VkS2V5cyB3aGVuIGluIHNlYXJjaCBtb2RlXG5cblxuICAgICAgaWYgKHZhbHVlUmVmcmVzaCAmJiB0cmVlQ2hlY2thYmxlICYmICF0cmVlQ2hlY2tTdHJpY3RseSAmJiAobmV3U3RhdGUuc2VhcmNoVmFsdWUgfHwgcHJldlN0YXRlLnNlYXJjaFZhbHVlKSkge1xuICAgICAgICBuZXdTdGF0ZS5zZWFyY2hIYWxmQ2hlY2tlZEtleXMgPSBnZXRIYWxmQ2hlY2tlZEtleXMobmV3U3RhdGUudmFsdWVMaXN0LCBuZXdTdGF0ZS52YWx1ZUVudGl0aWVzIHx8IHByZXZTdGF0ZS52YWx1ZUVudGl0aWVzKTtcbiAgICAgIH0gLy8gQ2hlY2tlZCBTdHJhdGVneVxuXG5cbiAgICAgIHByb2Nlc3NTdGF0ZSgnc2hvd0NoZWNrZWRTdHJhdGVneScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgbmV3U3RhdGUuc2VsZWN0b3JWYWx1ZUxpc3QgPSBuZXdTdGF0ZS5zZWxlY3RvclZhbHVlTGlzdCB8fCBmb3JtYXRTZWxlY3RvclZhbHVlKG5ld1N0YXRlLnZhbHVlTGlzdCB8fCBwcmV2U3RhdGUudmFsdWVMaXN0LCBuZXh0UHJvcHMsIG5ld1N0YXRlLnZhbHVlRW50aXRpZXMgfHwgcHJldlN0YXRlLnZhbHVlRW50aXRpZXMpO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gbmV3U3RhdGU7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIFNlbGVjdDtcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxuX2RlZmluZVByb3BlcnR5KFNlbGVjdCwgXCJwcm9wVHlwZXNcIiwge1xuICBwcmVmaXhDbHM6IFByb3BUeXBlcy5zdHJpbmcsXG4gIHByZWZpeEFyaWE6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG11bHRpcGxlOiBQcm9wVHlwZXMuYm9vbCxcbiAgc2hvd0Fycm93OiBQcm9wVHlwZXMuYm9vbCxcbiAgb3BlbjogUHJvcFR5cGVzLmJvb2wsXG4gIHZhbHVlOiB2YWx1ZVByb3AsXG4gIGF1dG9Gb2N1czogUHJvcFR5cGVzLmJvb2wsXG4gIGRlZmF1bHRPcGVuOiBQcm9wVHlwZXMuYm9vbCxcbiAgZGVmYXVsdFZhbHVlOiB2YWx1ZVByb3AsXG4gIHNob3dTZWFyY2g6IFByb3BUeXBlcy5ib29sLFxuICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLm5vZGUsXG4gIGlucHV0VmFsdWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIC8vIFtMZWdhY3ldIERlcHJlY2F0ZWQuIFVzZSBgc2VhcmNoVmFsdWVgIGluc3RlYWQuXG4gIHNlYXJjaFZhbHVlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBhdXRvQ2xlYXJTZWFyY2hWYWx1ZTogUHJvcFR5cGVzLmJvb2wsXG4gIHNlYXJjaFBsYWNlaG9sZGVyOiBQcm9wVHlwZXMubm9kZSxcbiAgLy8gW0xlZ2FjeV0gQ29uZnVzZSB3aXRoIHBsYWNlaG9sZGVyXG4gIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgY2hpbGRyZW46IFByb3BUeXBlcy5ub2RlLFxuICBsYWJlbEluVmFsdWU6IFByb3BUeXBlcy5ib29sLFxuICBtYXhUYWdDb3VudDogUHJvcFR5cGVzLm51bWJlcixcbiAgbWF4VGFnUGxhY2Vob2xkZXI6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5ub2RlLCBQcm9wVHlwZXMuZnVuY10pLFxuICBtYXhUYWdUZXh0TGVuZ3RoOiBQcm9wVHlwZXMubnVtYmVyLFxuICBzaG93Q2hlY2tlZFN0cmF0ZWd5OiBQcm9wVHlwZXMub25lT2YoW1NIT1dfQUxMLCBTSE9XX1BBUkVOVCwgU0hPV19DSElMRF0pLFxuICBkcm9wZG93bk1hdGNoU2VsZWN0V2lkdGg6IFByb3BUeXBlcy5ib29sLFxuICB0cmVlRGF0YTogUHJvcFR5cGVzLmFycmF5LFxuICB0cmVlRGF0YVNpbXBsZU1vZGU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5ib29sLCBQcm9wVHlwZXMub2JqZWN0XSksXG4gIHRyZWVOb2RlRmlsdGVyUHJvcDogUHJvcFR5cGVzLnN0cmluZyxcbiAgdHJlZU5vZGVMYWJlbFByb3A6IFByb3BUeXBlcy5zdHJpbmcsXG4gIHRyZWVDaGVja2FibGU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5ib29sLCBQcm9wVHlwZXMubm9kZV0pLFxuICB0cmVlQ2hlY2tTdHJpY3RseTogUHJvcFR5cGVzLmJvb2wsXG4gIHRyZWVJY29uOiBQcm9wVHlwZXMuYm9vbCxcbiAgdHJlZUxpbmU6IFByb3BUeXBlcy5ib29sLFxuICB0cmVlRGVmYXVsdEV4cGFuZEFsbDogUHJvcFR5cGVzLmJvb2wsXG4gIHRyZWVEZWZhdWx0RXhwYW5kZWRLZXlzOiBQcm9wVHlwZXMuYXJyYXksXG4gIHRyZWVFeHBhbmRlZEtleXM6IFByb3BUeXBlcy5hcnJheSxcbiAgbG9hZERhdGE6IFByb3BUeXBlcy5mdW5jLFxuICBmaWx0ZXJUcmVlTm9kZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLmZ1bmMsIFByb3BUeXBlcy5ib29sXSksXG4gIG5vdEZvdW5kQ29udGVudDogUHJvcFR5cGVzLm5vZGUsXG4gIG9uU2VhcmNoOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25TZWxlY3Q6IFByb3BUeXBlcy5mdW5jLFxuICBvbkRlc2VsZWN0OiBQcm9wVHlwZXMuZnVuYyxcbiAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICBvbkRyb3Bkb3duVmlzaWJsZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uVHJlZUV4cGFuZDogUHJvcFR5cGVzLmZ1bmMsXG4gIGlucHV0SWNvbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm5vZGUsIFByb3BUeXBlcy5mdW5jXSksXG4gIGNsZWFySWNvbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm5vZGUsIFByb3BUeXBlcy5mdW5jXSksXG4gIHJlbW92ZUljb246IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5ub2RlLCBQcm9wVHlwZXMuZnVuY10pLFxuICBzd2l0Y2hlckljb246IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5ub2RlLCBQcm9wVHlwZXMuZnVuY10pXG59KTtcblxuX2RlZmluZVByb3BlcnR5KFNlbGVjdCwgXCJjaGlsZENvbnRleHRUeXBlc1wiLCB7XG4gIHJjVHJlZVNlbGVjdDogUHJvcFR5cGVzLnNoYXBlKF9vYmplY3RTcHJlYWQoe30sIHNlbGVjdG9yQ29udGV4dFR5cGVzLCB7fSwgbXVsdGlwbGVTZWxlY3RvckNvbnRleHRUeXBlcywge30sIHBvcHVwQ29udGV4dFR5cGVzLCB7XG4gICAgb25TZWFyY2hJbnB1dENoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgb25TZWFyY2hJbnB1dEtleURvd246IFByb3BUeXBlcy5mdW5jXG4gIH0pKVxufSk7XG5cbl9kZWZpbmVQcm9wZXJ0eShTZWxlY3QsIFwiZGVmYXVsdFByb3BzXCIsIHtcbiAgcHJlZml4Q2xzOiAncmMtdHJlZS1zZWxlY3QnLFxuICBwcmVmaXhBcmlhOiAncmMtdHJlZS1zZWxlY3QnLFxuICBzaG93QXJyb3c6IHRydWUsXG4gIHNob3dTZWFyY2g6IHRydWUsXG4gIGF1dG9DbGVhclNlYXJjaFZhbHVlOiB0cnVlLFxuICBzaG93Q2hlY2tlZFN0cmF0ZWd5OiBTSE9XX0NISUxELFxuICAvLyBkcm9wZG93bk1hdGNoU2VsZWN0V2lkdGggY2hhbmdlIHRoZSBvcmlnaW4gZGVzaWduLCBzZXQgdG8gZmFsc2Ugbm93XG4gIC8vIHJlZjogaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0LWNvbXBvbmVudC9zZWxlY3QvYmxvYi80Y2FkOTVlMDk4YTM0MWEwOWRlMjM5YWQ2OTgxMDY3MTg4ODQyMDIwL3NyYy9TZWxlY3QuanN4I0wzNDRcbiAgLy8gcmVmOiBodHRwczovL2dpdGh1Yi5jb20vcmVhY3QtY29tcG9uZW50L3NlbGVjdC9wdWxsLzcxXG4gIHRyZWVOb2RlRmlsdGVyUHJvcDogJ3ZhbHVlJyxcbiAgdHJlZU5vZGVMYWJlbFByb3A6ICd0aXRsZScsXG4gIHRyZWVJY29uOiBmYWxzZSxcbiAgbm90Rm91bmRDb250ZW50OiAnTm90IEZvdW5kJ1xufSk7XG5cblNlbGVjdC5UcmVlTm9kZSA9IFNlbGVjdE5vZGU7XG5TZWxlY3QuU0hPV19BTEwgPSBTSE9XX0FMTDtcblNlbGVjdC5TSE9XX1BBUkVOVCA9IFNIT1dfUEFSRU5UO1xuU2VsZWN0LlNIT1dfQ0hJTEQgPSBTSE9XX0NISUxEOyAvLyBMZXQgd2FybmluZyBzaG93IGNvcnJlY3QgY29tcG9uZW50IG5hbWVcblxuU2VsZWN0LmRpc3BsYXlOYW1lID0gJ1RyZWVTZWxlY3QnO1xucG9seWZpbGwoU2VsZWN0KTtcbmV4cG9ydCBkZWZhdWx0IFNlbGVjdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBVEE7QUFDQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVhBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBREE7QUFjQTtBQWpCQTtBQW1CQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE5Q0E7QUFnREE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFkQTtBQUNBO0FBZ0JBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBM0RBO0FBNkRBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTdMQTtBQUNBO0FBK0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFqREE7QUFDQTtBQW1EQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBREE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFiQTtBQUNBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tree-select/es/Select.js
