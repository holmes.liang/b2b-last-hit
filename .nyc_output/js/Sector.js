var Path = __webpack_require__(/*! ../Path */ "./node_modules/zrender/lib/graphic/Path.js");

var fixClipWithShadow = __webpack_require__(/*! ../helper/fixClipWithShadow */ "./node_modules/zrender/lib/graphic/helper/fixClipWithShadow.js");
/**
 * 扇形
 * @module zrender/graphic/shape/Sector
 */


var _default = Path.extend({
  type: 'sector',
  shape: {
    cx: 0,
    cy: 0,
    r0: 0,
    r: 0,
    startAngle: 0,
    endAngle: Math.PI * 2,
    clockwise: true
  },
  brush: fixClipWithShadow(Path.prototype.brush),
  buildPath: function buildPath(ctx, shape) {
    var x = shape.cx;
    var y = shape.cy;
    var r0 = Math.max(shape.r0 || 0, 0);
    var r = Math.max(shape.r, 0);
    var startAngle = shape.startAngle;
    var endAngle = shape.endAngle;
    var clockwise = shape.clockwise;
    var unitX = Math.cos(startAngle);
    var unitY = Math.sin(startAngle);
    ctx.moveTo(unitX * r0 + x, unitY * r0 + y);
    ctx.lineTo(unitX * r + x, unitY * r + y);
    ctx.arc(x, y, r, startAngle, endAngle, !clockwise);
    ctx.lineTo(Math.cos(endAngle) * r0 + x, Math.sin(endAngle) * r0 + y);

    if (r0 !== 0) {
      ctx.arc(x, y, r0, endAngle, startAngle, clockwise);
    }

    ctx.closePath();
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9TZWN0b3IuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9ncmFwaGljL3NoYXBlL1NlY3Rvci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgUGF0aCA9IHJlcXVpcmUoXCIuLi9QYXRoXCIpO1xuXG52YXIgZml4Q2xpcFdpdGhTaGFkb3cgPSByZXF1aXJlKFwiLi4vaGVscGVyL2ZpeENsaXBXaXRoU2hhZG93XCIpO1xuXG4vKipcbiAqIOaJh+W9olxuICogQG1vZHVsZSB6cmVuZGVyL2dyYXBoaWMvc2hhcGUvU2VjdG9yXG4gKi9cbnZhciBfZGVmYXVsdCA9IFBhdGguZXh0ZW5kKHtcbiAgdHlwZTogJ3NlY3RvcicsXG4gIHNoYXBlOiB7XG4gICAgY3g6IDAsXG4gICAgY3k6IDAsXG4gICAgcjA6IDAsXG4gICAgcjogMCxcbiAgICBzdGFydEFuZ2xlOiAwLFxuICAgIGVuZEFuZ2xlOiBNYXRoLlBJICogMixcbiAgICBjbG9ja3dpc2U6IHRydWVcbiAgfSxcbiAgYnJ1c2g6IGZpeENsaXBXaXRoU2hhZG93KFBhdGgucHJvdG90eXBlLmJydXNoKSxcbiAgYnVpbGRQYXRoOiBmdW5jdGlvbiAoY3R4LCBzaGFwZSkge1xuICAgIHZhciB4ID0gc2hhcGUuY3g7XG4gICAgdmFyIHkgPSBzaGFwZS5jeTtcbiAgICB2YXIgcjAgPSBNYXRoLm1heChzaGFwZS5yMCB8fCAwLCAwKTtcbiAgICB2YXIgciA9IE1hdGgubWF4KHNoYXBlLnIsIDApO1xuICAgIHZhciBzdGFydEFuZ2xlID0gc2hhcGUuc3RhcnRBbmdsZTtcbiAgICB2YXIgZW5kQW5nbGUgPSBzaGFwZS5lbmRBbmdsZTtcbiAgICB2YXIgY2xvY2t3aXNlID0gc2hhcGUuY2xvY2t3aXNlO1xuICAgIHZhciB1bml0WCA9IE1hdGguY29zKHN0YXJ0QW5nbGUpO1xuICAgIHZhciB1bml0WSA9IE1hdGguc2luKHN0YXJ0QW5nbGUpO1xuICAgIGN0eC5tb3ZlVG8odW5pdFggKiByMCArIHgsIHVuaXRZICogcjAgKyB5KTtcbiAgICBjdHgubGluZVRvKHVuaXRYICogciArIHgsIHVuaXRZICogciArIHkpO1xuICAgIGN0eC5hcmMoeCwgeSwgciwgc3RhcnRBbmdsZSwgZW5kQW5nbGUsICFjbG9ja3dpc2UpO1xuICAgIGN0eC5saW5lVG8oTWF0aC5jb3MoZW5kQW5nbGUpICogcjAgKyB4LCBNYXRoLnNpbihlbmRBbmdsZSkgKiByMCArIHkpO1xuXG4gICAgaWYgKHIwICE9PSAwKSB7XG4gICAgICBjdHguYXJjKHgsIHksIHIwLCBlbmRBbmdsZSwgc3RhcnRBbmdsZSwgY2xvY2t3aXNlKTtcbiAgICB9XG5cbiAgICBjdHguY2xvc2VQYXRoKCk7XG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBRUE7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhDQTtBQUNBO0FBa0NBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/shape/Sector.js
