// mutationobserver-shim v0.3.2 (github.com/megawac/MutationObserver.js)
// Authors: Graeme Yeates (github.com/megawac) 
window.MutationObserver = window.MutationObserver || function (w) {
  function v(a) {
    this.i = [];
    this.m = a;
  }

  function I(a) {
    (function c() {
      var d = a.takeRecords();
      d.length && a.m(d, a);
      a.h = setTimeout(c, v._period);
    })();
  }

  function p(a) {
    var b = {
      type: null,
      target: null,
      addedNodes: [],
      removedNodes: [],
      previousSibling: null,
      nextSibling: null,
      attributeName: null,
      attributeNamespace: null,
      oldValue: null
    },
        c;

    for (c in a) {
      b[c] !== w && a[c] !== w && (b[c] = a[c]);
    }

    return b;
  }

  function J(a, b) {
    var c = C(a, b);
    return function (d) {
      var f = d.length,
          n;
      b.a && 3 === a.nodeType && a.nodeValue !== c.a && d.push(new p({
        type: "characterData",
        target: a,
        oldValue: c.a
      }));
      b.b && c.b && A(d, a, c.b, b.f);
      if (b.c || b.g) n = K(d, a, c, b);
      if (n || d.length !== f) c = C(a, b);
    };
  }

  function L(a, b) {
    return b.value;
  }

  function M(a, b) {
    return "style" !== b.name ? b.value : a.style.cssText;
  }

  function A(a, b, c, d) {
    for (var f = {}, n = b.attributes, k, g, x = n.length; x--;) {
      k = n[x], g = k.name, d && d[g] === w || (D(b, k) !== c[g] && a.push(p({
        type: "attributes",
        target: b,
        attributeName: g,
        oldValue: c[g],
        attributeNamespace: k.namespaceURI
      })), f[g] = !0);
    }

    for (g in c) {
      f[g] || a.push(p({
        target: b,
        type: "attributes",
        attributeName: g,
        oldValue: c[g]
      }));
    }
  }

  function K(a, b, c, d) {
    function f(b, c, f, k, y) {
      var g = b.length - 1;
      y = -~((g - y) / 2);

      for (var h, l, e; e = b.pop();) {
        h = f[e.j], l = k[e.l], d.c && y && Math.abs(e.j - e.l) >= g && (a.push(p({
          type: "childList",
          target: c,
          addedNodes: [h],
          removedNodes: [h],
          nextSibling: h.nextSibling,
          previousSibling: h.previousSibling
        })), y--), d.b && l.b && A(a, h, l.b, d.f), d.a && 3 === h.nodeType && h.nodeValue !== l.a && a.push(p({
          type: "characterData",
          target: h,
          oldValue: l.a
        })), d.g && n(h, l);
      }
    }

    function n(b, c) {
      for (var g = b.childNodes, q = c.c, x = g.length, v = q ? q.length : 0, h, l, e, m, t, z = 0, u = 0, r = 0; u < x || r < v;) {
        m = g[u], t = (e = q[r]) && e.node, m === t ? (d.b && e.b && A(a, m, e.b, d.f), d.a && e.a !== w && m.nodeValue !== e.a && a.push(p({
          type: "characterData",
          target: m,
          oldValue: e.a
        })), l && f(l, b, g, q, z), d.g && (m.childNodes.length || e.c && e.c.length) && n(m, e), u++, r++) : (k = !0, h || (h = {}, l = []), m && (h[e = E(m)] || (h[e] = !0, -1 === (e = F(q, m, r, "node")) ? d.c && (a.push(p({
          type: "childList",
          target: b,
          addedNodes: [m],
          nextSibling: m.nextSibling,
          previousSibling: m.previousSibling
        })), z++) : l.push({
          j: u,
          l: e
        })), u++), t && t !== g[u] && (h[e = E(t)] || (h[e] = !0, -1 === (e = F(g, t, u)) ? d.c && (a.push(p({
          type: "childList",
          target: c.node,
          removedNodes: [t],
          nextSibling: q[r + 1],
          previousSibling: q[r - 1]
        })), z--) : l.push({
          j: e,
          l: r
        })), r++));
      }

      l && f(l, b, g, q, z);
    }

    var k;
    n(b, c);
    return k;
  }

  function C(a, b) {
    var c = !0;
    return function f(a) {
      var k = {
        node: a
      };
      !b.a || 3 !== a.nodeType && 8 !== a.nodeType ? (b.b && c && 1 === a.nodeType && (k.b = G(a.attributes, function (c, f) {
        if (!b.f || b.f[f.name]) c[f.name] = D(a, f);
        return c;
      })), c && (b.c || b.a || b.b && b.g) && (k.c = N(a.childNodes, f)), c = b.g) : k.a = a.nodeValue;
      return k;
    }(a);
  }

  function E(a) {
    try {
      return a.id || (a.mo_id = a.mo_id || H++);
    } catch (b) {
      try {
        return a.nodeValue;
      } catch (c) {
        return H++;
      }
    }
  }

  function N(a, b) {
    for (var c = [], d = 0; d < a.length; d++) {
      c[d] = b(a[d], d, a);
    }

    return c;
  }

  function G(a, b) {
    for (var c = {}, d = 0; d < a.length; d++) {
      c = b(c, a[d], d, a);
    }

    return c;
  }

  function F(a, b, c, d) {
    for (; c < a.length; c++) {
      if ((d ? a[c][d] : a[c]) === b) return c;
    }

    return -1;
  }

  v._period = 30;
  v.prototype = {
    observe: function observe(a, b) {
      for (var c = {
        b: !!(b.attributes || b.attributeFilter || b.attributeOldValue),
        c: !!b.childList,
        g: !!b.subtree,
        a: !(!b.characterData && !b.characterDataOldValue)
      }, d = this.i, f = 0; f < d.length; f++) {
        d[f].s === a && d.splice(f, 1);
      }

      b.attributeFilter && (c.f = G(b.attributeFilter, function (a, b) {
        a[b] = !0;
        return a;
      }));
      d.push({
        s: a,
        o: J(a, c)
      });
      this.h || I(this);
    },
    takeRecords: function takeRecords() {
      for (var a = [], b = this.i, c = 0; c < b.length; c++) {
        b[c].o(a);
      }

      return a;
    },
    disconnect: function disconnect() {
      this.i = [];
      clearTimeout(this.h);
      this.h = null;
    }
  };
  var B = document.createElement("i");
  B.style.top = 0;
  var D = (B = "null" != B.attributes.style.value) ? L : M,
      H = 1;
  return v;
}(void 0);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvbXV0YXRpb25vYnNlcnZlci1zaGltL2Rpc3QvbXV0YXRpb25vYnNlcnZlci5taW4uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9tdXRhdGlvbm9ic2VydmVyLXNoaW0vZGlzdC9tdXRhdGlvbm9ic2VydmVyLm1pbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBtdXRhdGlvbm9ic2VydmVyLXNoaW0gdjAuMy4yIChnaXRodWIuY29tL21lZ2F3YWMvTXV0YXRpb25PYnNlcnZlci5qcylcbi8vIEF1dGhvcnM6IEdyYWVtZSBZZWF0ZXMgKGdpdGh1Yi5jb20vbWVnYXdhYykgXG53aW5kb3cuTXV0YXRpb25PYnNlcnZlcj13aW5kb3cuTXV0YXRpb25PYnNlcnZlcnx8ZnVuY3Rpb24odyl7ZnVuY3Rpb24gdihhKXt0aGlzLmk9W107dGhpcy5tPWF9ZnVuY3Rpb24gSShhKXsoZnVuY3Rpb24gYygpe3ZhciBkPWEudGFrZVJlY29yZHMoKTtkLmxlbmd0aCYmYS5tKGQsYSk7YS5oPXNldFRpbWVvdXQoYyx2Ll9wZXJpb2QpfSkoKX1mdW5jdGlvbiBwKGEpe3ZhciBiPXt0eXBlOm51bGwsdGFyZ2V0Om51bGwsYWRkZWROb2RlczpbXSxyZW1vdmVkTm9kZXM6W10scHJldmlvdXNTaWJsaW5nOm51bGwsbmV4dFNpYmxpbmc6bnVsbCxhdHRyaWJ1dGVOYW1lOm51bGwsYXR0cmlidXRlTmFtZXNwYWNlOm51bGwsb2xkVmFsdWU6bnVsbH0sYztmb3IoYyBpbiBhKWJbY10hPT13JiZhW2NdIT09dyYmKGJbY109YVtjXSk7cmV0dXJuIGJ9ZnVuY3Rpb24gSihhLGIpe3ZhciBjPUMoYSxiKTtyZXR1cm4gZnVuY3Rpb24oZCl7dmFyIGY9ZC5sZW5ndGgsbjtiLmEmJjM9PT1hLm5vZGVUeXBlJiZcbmEubm9kZVZhbHVlIT09Yy5hJiZkLnB1c2gobmV3IHAoe3R5cGU6XCJjaGFyYWN0ZXJEYXRhXCIsdGFyZ2V0OmEsb2xkVmFsdWU6Yy5hfSkpO2IuYiYmYy5iJiZBKGQsYSxjLmIsYi5mKTtpZihiLmN8fGIuZyluPUsoZCxhLGMsYik7aWYobnx8ZC5sZW5ndGghPT1mKWM9QyhhLGIpfX1mdW5jdGlvbiBMKGEsYil7cmV0dXJuIGIudmFsdWV9ZnVuY3Rpb24gTShhLGIpe3JldHVyblwic3R5bGVcIiE9PWIubmFtZT9iLnZhbHVlOmEuc3R5bGUuY3NzVGV4dH1mdW5jdGlvbiBBKGEsYixjLGQpe2Zvcih2YXIgZj17fSxuPWIuYXR0cmlidXRlcyxrLGcseD1uLmxlbmd0aDt4LS07KWs9blt4XSxnPWsubmFtZSxkJiZkW2ddPT09d3x8KEQoYixrKSE9PWNbZ10mJmEucHVzaChwKHt0eXBlOlwiYXR0cmlidXRlc1wiLHRhcmdldDpiLGF0dHJpYnV0ZU5hbWU6ZyxvbGRWYWx1ZTpjW2ddLGF0dHJpYnV0ZU5hbWVzcGFjZTprLm5hbWVzcGFjZVVSSX0pKSxmW2ddPSEwKTtmb3IoZyBpbiBjKWZbZ118fGEucHVzaChwKHt0YXJnZXQ6YixcbnR5cGU6XCJhdHRyaWJ1dGVzXCIsYXR0cmlidXRlTmFtZTpnLG9sZFZhbHVlOmNbZ119KSl9ZnVuY3Rpb24gSyhhLGIsYyxkKXtmdW5jdGlvbiBmKGIsYyxmLGsseSl7dmFyIGc9Yi5sZW5ndGgtMTt5PS1+KChnLXkpLzIpO2Zvcih2YXIgaCxsLGU7ZT1iLnBvcCgpOyloPWZbZS5qXSxsPWtbZS5sXSxkLmMmJnkmJk1hdGguYWJzKGUuai1lLmwpPj1nJiYoYS5wdXNoKHAoe3R5cGU6XCJjaGlsZExpc3RcIix0YXJnZXQ6YyxhZGRlZE5vZGVzOltoXSxyZW1vdmVkTm9kZXM6W2hdLG5leHRTaWJsaW5nOmgubmV4dFNpYmxpbmcscHJldmlvdXNTaWJsaW5nOmgucHJldmlvdXNTaWJsaW5nfSkpLHktLSksZC5iJiZsLmImJkEoYSxoLGwuYixkLmYpLGQuYSYmMz09PWgubm9kZVR5cGUmJmgubm9kZVZhbHVlIT09bC5hJiZhLnB1c2gocCh7dHlwZTpcImNoYXJhY3RlckRhdGFcIix0YXJnZXQ6aCxvbGRWYWx1ZTpsLmF9KSksZC5nJiZuKGgsbCl9ZnVuY3Rpb24gbihiLGMpe2Zvcih2YXIgZz1iLmNoaWxkTm9kZXMsXG5xPWMuYyx4PWcubGVuZ3RoLHY9cT9xLmxlbmd0aDowLGgsbCxlLG0sdCx6PTAsdT0wLHI9MDt1PHh8fHI8djspbT1nW3VdLHQ9KGU9cVtyXSkmJmUubm9kZSxtPT09dD8oZC5iJiZlLmImJkEoYSxtLGUuYixkLmYpLGQuYSYmZS5hIT09dyYmbS5ub2RlVmFsdWUhPT1lLmEmJmEucHVzaChwKHt0eXBlOlwiY2hhcmFjdGVyRGF0YVwiLHRhcmdldDptLG9sZFZhbHVlOmUuYX0pKSxsJiZmKGwsYixnLHEseiksZC5nJiYobS5jaGlsZE5vZGVzLmxlbmd0aHx8ZS5jJiZlLmMubGVuZ3RoKSYmbihtLGUpLHUrKyxyKyspOihrPSEwLGh8fChoPXt9LGw9W10pLG0mJihoW2U9RShtKV18fChoW2VdPSEwLC0xPT09KGU9RihxLG0scixcIm5vZGVcIikpP2QuYyYmKGEucHVzaChwKHt0eXBlOlwiY2hpbGRMaXN0XCIsdGFyZ2V0OmIsYWRkZWROb2RlczpbbV0sbmV4dFNpYmxpbmc6bS5uZXh0U2libGluZyxwcmV2aW91c1NpYmxpbmc6bS5wcmV2aW91c1NpYmxpbmd9KSkseisrKTpsLnB1c2goe2o6dSxsOmV9KSksXG51KyspLHQmJnQhPT1nW3VdJiYoaFtlPUUodCldfHwoaFtlXT0hMCwtMT09PShlPUYoZyx0LHUpKT9kLmMmJihhLnB1c2gocCh7dHlwZTpcImNoaWxkTGlzdFwiLHRhcmdldDpjLm5vZGUscmVtb3ZlZE5vZGVzOlt0XSxuZXh0U2libGluZzpxW3IrMV0scHJldmlvdXNTaWJsaW5nOnFbci0xXX0pKSx6LS0pOmwucHVzaCh7ajplLGw6cn0pKSxyKyspKTtsJiZmKGwsYixnLHEseil9dmFyIGs7bihiLGMpO3JldHVybiBrfWZ1bmN0aW9uIEMoYSxiKXt2YXIgYz0hMDtyZXR1cm4gZnVuY3Rpb24gZihhKXt2YXIgaz17bm9kZTphfTshYi5hfHwzIT09YS5ub2RlVHlwZSYmOCE9PWEubm9kZVR5cGU/KGIuYiYmYyYmMT09PWEubm9kZVR5cGUmJihrLmI9RyhhLmF0dHJpYnV0ZXMsZnVuY3Rpb24oYyxmKXtpZighYi5mfHxiLmZbZi5uYW1lXSljW2YubmFtZV09RChhLGYpO3JldHVybiBjfSkpLGMmJihiLmN8fGIuYXx8Yi5iJiZiLmcpJiYoay5jPU4oYS5jaGlsZE5vZGVzLGYpKSxjPWIuZyk6ay5hPVxuYS5ub2RlVmFsdWU7cmV0dXJuIGt9KGEpfWZ1bmN0aW9uIEUoYSl7dHJ5e3JldHVybiBhLmlkfHwoYS5tb19pZD1hLm1vX2lkfHxIKyspfWNhdGNoKGIpe3RyeXtyZXR1cm4gYS5ub2RlVmFsdWV9Y2F0Y2goYyl7cmV0dXJuIEgrK319fWZ1bmN0aW9uIE4oYSxiKXtmb3IodmFyIGM9W10sZD0wO2Q8YS5sZW5ndGg7ZCsrKWNbZF09YihhW2RdLGQsYSk7cmV0dXJuIGN9ZnVuY3Rpb24gRyhhLGIpe2Zvcih2YXIgYz17fSxkPTA7ZDxhLmxlbmd0aDtkKyspYz1iKGMsYVtkXSxkLGEpO3JldHVybiBjfWZ1bmN0aW9uIEYoYSxiLGMsZCl7Zm9yKDtjPGEubGVuZ3RoO2MrKylpZigoZD9hW2NdW2RdOmFbY10pPT09YilyZXR1cm4gYztyZXR1cm4tMX12Ll9wZXJpb2Q9MzA7di5wcm90b3R5cGU9e29ic2VydmU6ZnVuY3Rpb24oYSxiKXtmb3IodmFyIGM9e2I6ISEoYi5hdHRyaWJ1dGVzfHxiLmF0dHJpYnV0ZUZpbHRlcnx8Yi5hdHRyaWJ1dGVPbGRWYWx1ZSksYzohIWIuY2hpbGRMaXN0LGc6ISFiLnN1YnRyZWUsXG5hOiEoIWIuY2hhcmFjdGVyRGF0YSYmIWIuY2hhcmFjdGVyRGF0YU9sZFZhbHVlKX0sZD10aGlzLmksZj0wO2Y8ZC5sZW5ndGg7ZisrKWRbZl0ucz09PWEmJmQuc3BsaWNlKGYsMSk7Yi5hdHRyaWJ1dGVGaWx0ZXImJihjLmY9RyhiLmF0dHJpYnV0ZUZpbHRlcixmdW5jdGlvbihhLGIpe2FbYl09ITA7cmV0dXJuIGF9KSk7ZC5wdXNoKHtzOmEsbzpKKGEsYyl9KTt0aGlzLmh8fEkodGhpcyl9LHRha2VSZWNvcmRzOmZ1bmN0aW9uKCl7Zm9yKHZhciBhPVtdLGI9dGhpcy5pLGM9MDtjPGIubGVuZ3RoO2MrKyliW2NdLm8oYSk7cmV0dXJuIGF9LGRpc2Nvbm5lY3Q6ZnVuY3Rpb24oKXt0aGlzLmk9W107Y2xlYXJUaW1lb3V0KHRoaXMuaCk7dGhpcy5oPW51bGx9fTt2YXIgQj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaVwiKTtCLnN0eWxlLnRvcD0wO3ZhciBEPShCPVwibnVsbFwiIT1CLmF0dHJpYnV0ZXMuc3R5bGUudmFsdWUpP0w6TSxIPTE7cmV0dXJuIHZ9KHZvaWQgMCk7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/mutationobserver-shim/dist/mutationobserver.min.js
