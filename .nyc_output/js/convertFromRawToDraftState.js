/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule convertFromRawToDraftState
 * @format
 * 
 */


var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var ContentBlock = __webpack_require__(/*! ./ContentBlock */ "./node_modules/draft-js/lib/ContentBlock.js");

var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var ContentState = __webpack_require__(/*! ./ContentState */ "./node_modules/draft-js/lib/ContentState.js");

var DraftEntity = __webpack_require__(/*! ./DraftEntity */ "./node_modules/draft-js/lib/DraftEntity.js");

var DraftFeatureFlags = __webpack_require__(/*! ./DraftFeatureFlags */ "./node_modules/draft-js/lib/DraftFeatureFlags.js");

var DraftTreeAdapter = __webpack_require__(/*! ./DraftTreeAdapter */ "./node_modules/draft-js/lib/DraftTreeAdapter.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var SelectionState = __webpack_require__(/*! ./SelectionState */ "./node_modules/draft-js/lib/SelectionState.js");

var createCharacterList = __webpack_require__(/*! ./createCharacterList */ "./node_modules/draft-js/lib/createCharacterList.js");

var decodeEntityRanges = __webpack_require__(/*! ./decodeEntityRanges */ "./node_modules/draft-js/lib/decodeEntityRanges.js");

var decodeInlineStyleRanges = __webpack_require__(/*! ./decodeInlineStyleRanges */ "./node_modules/draft-js/lib/decodeInlineStyleRanges.js");

var generateRandomKey = __webpack_require__(/*! ./generateRandomKey */ "./node_modules/draft-js/lib/generateRandomKey.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var experimentalTreeDataSupport = DraftFeatureFlags.draft_tree_data_support;
var List = Immutable.List,
    Map = Immutable.Map,
    OrderedMap = Immutable.OrderedMap;

var decodeBlockNodeConfig = function decodeBlockNodeConfig(block, entityMap) {
  var key = block.key,
      type = block.type,
      data = block.data,
      text = block.text,
      depth = block.depth;
  var blockNodeConfig = {
    text: text,
    depth: depth || 0,
    type: type || 'unstyled',
    key: key || generateRandomKey(),
    data: Map(data),
    characterList: decodeCharacterList(block, entityMap)
  };
  return blockNodeConfig;
};

var decodeCharacterList = function decodeCharacterList(block, entityMap) {
  var text = block.text,
      rawEntityRanges = block.entityRanges,
      rawInlineStyleRanges = block.inlineStyleRanges;
  var entityRanges = rawEntityRanges || [];
  var inlineStyleRanges = rawInlineStyleRanges || []; // Translate entity range keys to the DraftEntity map.

  return createCharacterList(decodeInlineStyleRanges(text, inlineStyleRanges), decodeEntityRanges(text, entityRanges.filter(function (range) {
    return entityMap.hasOwnProperty(range.key);
  }).map(function (range) {
    return _extends({}, range, {
      key: entityMap[range.key]
    });
  })));
};

var addKeyIfMissing = function addKeyIfMissing(block) {
  return _extends({}, block, {
    key: block.key || generateRandomKey()
  });
};
/**
 * Node stack is responsible to ensure we traverse the tree only once
 * in depth order, while also providing parent refs to inner nodes to
 * construct their links.
 */


var updateNodeStack = function updateNodeStack(stack, nodes, parentRef) {
  var nodesWithParentRef = nodes.map(function (block) {
    return _extends({}, block, {
      parentRef: parentRef
    });
  }); // since we pop nodes from the stack we need to insert them in reverse

  return stack.concat(nodesWithParentRef.reverse());
};
/**
 * This will build a tree draft content state by creating the node
 * reference links into a single tree walk. Each node has a link
 * reference to "parent", "children", "nextSibling" and "prevSibling"
 * blockMap will be created using depth ordering.
 */


var decodeContentBlockNodes = function decodeContentBlockNodes(blocks, entityMap) {
  return blocks // ensure children have valid keys to enable sibling links
  .map(addKeyIfMissing).reduce(function (blockMap, block, index) {
    !Array.isArray(block.children) ?  true ? invariant(false, 'invalid RawDraftContentBlock can not be converted to ContentBlockNode') : undefined : void 0; // ensure children have valid keys to enable sibling links

    var children = block.children.map(addKeyIfMissing); // root level nodes

    var contentBlockNode = new ContentBlockNode(_extends({}, decodeBlockNodeConfig(block, entityMap), {
      prevSibling: index === 0 ? null : blocks[index - 1].key,
      nextSibling: index === blocks.length - 1 ? null : blocks[index + 1].key,
      children: List(children.map(function (child) {
        return child.key;
      }))
    })); // push root node to blockMap

    blockMap = blockMap.set(contentBlockNode.getKey(), contentBlockNode); // this stack is used to ensure we visit all nodes respecting depth ordering

    var stack = updateNodeStack([], children, contentBlockNode); // start computing children nodes

    while (stack.length > 0) {
      // we pop from the stack and start processing this node
      var node = stack.pop(); // parentRef already points to a converted ContentBlockNode

      var parentRef = node.parentRef;
      var siblings = parentRef.getChildKeys();

      var _index = siblings.indexOf(node.key);

      var isValidBlock = Array.isArray(node.children);

      if (!isValidBlock) {
        !isValidBlock ?  true ? invariant(false, 'invalid RawDraftContentBlock can not be converted to ContentBlockNode') : undefined : void 0;
        break;
      } // ensure children have valid keys to enable sibling links


      var _children = node.children.map(addKeyIfMissing);

      var _contentBlockNode = new ContentBlockNode(_extends({}, decodeBlockNodeConfig(node, entityMap), {
        parent: parentRef.getKey(),
        children: List(_children.map(function (child) {
          return child.key;
        })),
        prevSibling: _index === 0 ? null : siblings.get(_index - 1),
        nextSibling: _index === siblings.size - 1 ? null : siblings.get(_index + 1)
      })); // push node to blockMap


      blockMap = blockMap.set(_contentBlockNode.getKey(), _contentBlockNode); // this stack is used to ensure we visit all nodes respecting depth ordering

      stack = updateNodeStack(stack, _children, _contentBlockNode);
    }

    return blockMap;
  }, OrderedMap());
};

var decodeContentBlocks = function decodeContentBlocks(blocks, entityMap) {
  return OrderedMap(blocks.map(function (block) {
    var contentBlock = new ContentBlock(decodeBlockNodeConfig(block, entityMap));
    return [contentBlock.getKey(), contentBlock];
  }));
};

var decodeRawBlocks = function decodeRawBlocks(rawState, entityMap) {
  var isTreeRawBlock = Array.isArray(rawState.blocks[0].children);
  var rawBlocks = experimentalTreeDataSupport && !isTreeRawBlock ? DraftTreeAdapter.fromRawStateToRawTreeState(rawState).blocks : rawState.blocks;

  if (!experimentalTreeDataSupport) {
    return decodeContentBlocks(isTreeRawBlock ? DraftTreeAdapter.fromRawTreeStateToRawState(rawState).blocks : rawBlocks, entityMap);
  }

  return decodeContentBlockNodes(rawBlocks, entityMap);
};

var decodeRawEntityMap = function decodeRawEntityMap(rawState) {
  var rawEntityMap = rawState.entityMap;
  var entityMap = {}; // TODO: Update this once we completely remove DraftEntity

  Object.keys(rawEntityMap).forEach(function (rawEntityKey) {
    var _rawEntityMap$rawEnti = rawEntityMap[rawEntityKey],
        type = _rawEntityMap$rawEnti.type,
        mutability = _rawEntityMap$rawEnti.mutability,
        data = _rawEntityMap$rawEnti.data; // get the key reference to created entity

    entityMap[rawEntityKey] = DraftEntity.__create(type, mutability, data || {});
  });
  return entityMap;
};

var convertFromRawToDraftState = function convertFromRawToDraftState(rawState) {
  !Array.isArray(rawState.blocks) ?  true ? invariant(false, 'invalid RawDraftContentState') : undefined : void 0; // decode entities

  var entityMap = decodeRawEntityMap(rawState); // decode blockMap

  var blockMap = decodeRawBlocks(rawState, entityMap); // create initial selection

  var selectionState = blockMap.isEmpty() ? new SelectionState() : SelectionState.createEmpty(blockMap.first().getKey());
  return new ContentState({
    blockMap: blockMap,
    entityMap: entityMap,
    selectionBefore: selectionState,
    selectionAfter: selectionState
  });
};

module.exports = convertFromRawToDraftState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2NvbnZlcnRGcm9tUmF3VG9EcmFmdFN0YXRlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2NvbnZlcnRGcm9tUmF3VG9EcmFmdFN0YXRlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgY29udmVydEZyb21SYXdUb0RyYWZ0U3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9hc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XG5cbnZhciBfZXh0ZW5kcyA9IF9hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBDb250ZW50QmxvY2sgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9jaycpO1xudmFyIENvbnRlbnRCbG9ja05vZGUgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9ja05vZGUnKTtcbnZhciBDb250ZW50U3RhdGUgPSByZXF1aXJlKCcuL0NvbnRlbnRTdGF0ZScpO1xudmFyIERyYWZ0RW50aXR5ID0gcmVxdWlyZSgnLi9EcmFmdEVudGl0eScpO1xudmFyIERyYWZ0RmVhdHVyZUZsYWdzID0gcmVxdWlyZSgnLi9EcmFmdEZlYXR1cmVGbGFncycpO1xudmFyIERyYWZ0VHJlZUFkYXB0ZXIgPSByZXF1aXJlKCcuL0RyYWZ0VHJlZUFkYXB0ZXInKTtcbnZhciBJbW11dGFibGUgPSByZXF1aXJlKCdpbW11dGFibGUnKTtcbnZhciBTZWxlY3Rpb25TdGF0ZSA9IHJlcXVpcmUoJy4vU2VsZWN0aW9uU3RhdGUnKTtcblxudmFyIGNyZWF0ZUNoYXJhY3Rlckxpc3QgPSByZXF1aXJlKCcuL2NyZWF0ZUNoYXJhY3Rlckxpc3QnKTtcbnZhciBkZWNvZGVFbnRpdHlSYW5nZXMgPSByZXF1aXJlKCcuL2RlY29kZUVudGl0eVJhbmdlcycpO1xudmFyIGRlY29kZUlubGluZVN0eWxlUmFuZ2VzID0gcmVxdWlyZSgnLi9kZWNvZGVJbmxpbmVTdHlsZVJhbmdlcycpO1xudmFyIGdlbmVyYXRlUmFuZG9tS2V5ID0gcmVxdWlyZSgnLi9nZW5lcmF0ZVJhbmRvbUtleScpO1xudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJ2ZianMvbGliL2ludmFyaWFudCcpO1xuXG52YXIgZXhwZXJpbWVudGFsVHJlZURhdGFTdXBwb3J0ID0gRHJhZnRGZWF0dXJlRmxhZ3MuZHJhZnRfdHJlZV9kYXRhX3N1cHBvcnQ7XG5cbnZhciBMaXN0ID0gSW1tdXRhYmxlLkxpc3QsXG4gICAgTWFwID0gSW1tdXRhYmxlLk1hcCxcbiAgICBPcmRlcmVkTWFwID0gSW1tdXRhYmxlLk9yZGVyZWRNYXA7XG5cblxudmFyIGRlY29kZUJsb2NrTm9kZUNvbmZpZyA9IGZ1bmN0aW9uIGRlY29kZUJsb2NrTm9kZUNvbmZpZyhibG9jaywgZW50aXR5TWFwKSB7XG4gIHZhciBrZXkgPSBibG9jay5rZXksXG4gICAgICB0eXBlID0gYmxvY2sudHlwZSxcbiAgICAgIGRhdGEgPSBibG9jay5kYXRhLFxuICAgICAgdGV4dCA9IGJsb2NrLnRleHQsXG4gICAgICBkZXB0aCA9IGJsb2NrLmRlcHRoO1xuXG5cbiAgdmFyIGJsb2NrTm9kZUNvbmZpZyA9IHtcbiAgICB0ZXh0OiB0ZXh0LFxuICAgIGRlcHRoOiBkZXB0aCB8fCAwLFxuICAgIHR5cGU6IHR5cGUgfHwgJ3Vuc3R5bGVkJyxcbiAgICBrZXk6IGtleSB8fCBnZW5lcmF0ZVJhbmRvbUtleSgpLFxuICAgIGRhdGE6IE1hcChkYXRhKSxcbiAgICBjaGFyYWN0ZXJMaXN0OiBkZWNvZGVDaGFyYWN0ZXJMaXN0KGJsb2NrLCBlbnRpdHlNYXApXG4gIH07XG5cbiAgcmV0dXJuIGJsb2NrTm9kZUNvbmZpZztcbn07XG5cbnZhciBkZWNvZGVDaGFyYWN0ZXJMaXN0ID0gZnVuY3Rpb24gZGVjb2RlQ2hhcmFjdGVyTGlzdChibG9jaywgZW50aXR5TWFwKSB7XG4gIHZhciB0ZXh0ID0gYmxvY2sudGV4dCxcbiAgICAgIHJhd0VudGl0eVJhbmdlcyA9IGJsb2NrLmVudGl0eVJhbmdlcyxcbiAgICAgIHJhd0lubGluZVN0eWxlUmFuZ2VzID0gYmxvY2suaW5saW5lU3R5bGVSYW5nZXM7XG5cblxuICB2YXIgZW50aXR5UmFuZ2VzID0gcmF3RW50aXR5UmFuZ2VzIHx8IFtdO1xuICB2YXIgaW5saW5lU3R5bGVSYW5nZXMgPSByYXdJbmxpbmVTdHlsZVJhbmdlcyB8fCBbXTtcblxuICAvLyBUcmFuc2xhdGUgZW50aXR5IHJhbmdlIGtleXMgdG8gdGhlIERyYWZ0RW50aXR5IG1hcC5cbiAgcmV0dXJuIGNyZWF0ZUNoYXJhY3Rlckxpc3QoZGVjb2RlSW5saW5lU3R5bGVSYW5nZXModGV4dCwgaW5saW5lU3R5bGVSYW5nZXMpLCBkZWNvZGVFbnRpdHlSYW5nZXModGV4dCwgZW50aXR5UmFuZ2VzLmZpbHRlcihmdW5jdGlvbiAocmFuZ2UpIHtcbiAgICByZXR1cm4gZW50aXR5TWFwLmhhc093blByb3BlcnR5KHJhbmdlLmtleSk7XG4gIH0pLm1hcChmdW5jdGlvbiAocmFuZ2UpIHtcbiAgICByZXR1cm4gX2V4dGVuZHMoe30sIHJhbmdlLCB7IGtleTogZW50aXR5TWFwW3JhbmdlLmtleV0gfSk7XG4gIH0pKSk7XG59O1xuXG52YXIgYWRkS2V5SWZNaXNzaW5nID0gZnVuY3Rpb24gYWRkS2V5SWZNaXNzaW5nKGJsb2NrKSB7XG4gIHJldHVybiBfZXh0ZW5kcyh7fSwgYmxvY2ssIHtcbiAgICBrZXk6IGJsb2NrLmtleSB8fCBnZW5lcmF0ZVJhbmRvbUtleSgpXG4gIH0pO1xufTtcblxuLyoqXG4gKiBOb2RlIHN0YWNrIGlzIHJlc3BvbnNpYmxlIHRvIGVuc3VyZSB3ZSB0cmF2ZXJzZSB0aGUgdHJlZSBvbmx5IG9uY2VcbiAqIGluIGRlcHRoIG9yZGVyLCB3aGlsZSBhbHNvIHByb3ZpZGluZyBwYXJlbnQgcmVmcyB0byBpbm5lciBub2RlcyB0b1xuICogY29uc3RydWN0IHRoZWlyIGxpbmtzLlxuICovXG52YXIgdXBkYXRlTm9kZVN0YWNrID0gZnVuY3Rpb24gdXBkYXRlTm9kZVN0YWNrKHN0YWNrLCBub2RlcywgcGFyZW50UmVmKSB7XG4gIHZhciBub2Rlc1dpdGhQYXJlbnRSZWYgPSBub2Rlcy5tYXAoZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgcmV0dXJuIF9leHRlbmRzKHt9LCBibG9jaywge1xuICAgICAgcGFyZW50UmVmOiBwYXJlbnRSZWZcbiAgICB9KTtcbiAgfSk7XG5cbiAgLy8gc2luY2Ugd2UgcG9wIG5vZGVzIGZyb20gdGhlIHN0YWNrIHdlIG5lZWQgdG8gaW5zZXJ0IHRoZW0gaW4gcmV2ZXJzZVxuICByZXR1cm4gc3RhY2suY29uY2F0KG5vZGVzV2l0aFBhcmVudFJlZi5yZXZlcnNlKCkpO1xufTtcblxuLyoqXG4gKiBUaGlzIHdpbGwgYnVpbGQgYSB0cmVlIGRyYWZ0IGNvbnRlbnQgc3RhdGUgYnkgY3JlYXRpbmcgdGhlIG5vZGVcbiAqIHJlZmVyZW5jZSBsaW5rcyBpbnRvIGEgc2luZ2xlIHRyZWUgd2Fsay4gRWFjaCBub2RlIGhhcyBhIGxpbmtcbiAqIHJlZmVyZW5jZSB0byBcInBhcmVudFwiLCBcImNoaWxkcmVuXCIsIFwibmV4dFNpYmxpbmdcIiBhbmQgXCJwcmV2U2libGluZ1wiXG4gKiBibG9ja01hcCB3aWxsIGJlIGNyZWF0ZWQgdXNpbmcgZGVwdGggb3JkZXJpbmcuXG4gKi9cbnZhciBkZWNvZGVDb250ZW50QmxvY2tOb2RlcyA9IGZ1bmN0aW9uIGRlY29kZUNvbnRlbnRCbG9ja05vZGVzKGJsb2NrcywgZW50aXR5TWFwKSB7XG4gIHJldHVybiBibG9ja3NcbiAgLy8gZW5zdXJlIGNoaWxkcmVuIGhhdmUgdmFsaWQga2V5cyB0byBlbmFibGUgc2libGluZyBsaW5rc1xuICAubWFwKGFkZEtleUlmTWlzc2luZykucmVkdWNlKGZ1bmN0aW9uIChibG9ja01hcCwgYmxvY2ssIGluZGV4KSB7XG4gICAgIUFycmF5LmlzQXJyYXkoYmxvY2suY2hpbGRyZW4pID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ2ludmFsaWQgUmF3RHJhZnRDb250ZW50QmxvY2sgY2FuIG5vdCBiZSBjb252ZXJ0ZWQgdG8gQ29udGVudEJsb2NrTm9kZScpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcblxuICAgIC8vIGVuc3VyZSBjaGlsZHJlbiBoYXZlIHZhbGlkIGtleXMgdG8gZW5hYmxlIHNpYmxpbmcgbGlua3NcbiAgICB2YXIgY2hpbGRyZW4gPSBibG9jay5jaGlsZHJlbi5tYXAoYWRkS2V5SWZNaXNzaW5nKTtcblxuICAgIC8vIHJvb3QgbGV2ZWwgbm9kZXNcbiAgICB2YXIgY29udGVudEJsb2NrTm9kZSA9IG5ldyBDb250ZW50QmxvY2tOb2RlKF9leHRlbmRzKHt9LCBkZWNvZGVCbG9ja05vZGVDb25maWcoYmxvY2ssIGVudGl0eU1hcCksIHtcbiAgICAgIHByZXZTaWJsaW5nOiBpbmRleCA9PT0gMCA/IG51bGwgOiBibG9ja3NbaW5kZXggLSAxXS5rZXksXG4gICAgICBuZXh0U2libGluZzogaW5kZXggPT09IGJsb2Nrcy5sZW5ndGggLSAxID8gbnVsbCA6IGJsb2Nrc1tpbmRleCArIDFdLmtleSxcbiAgICAgIGNoaWxkcmVuOiBMaXN0KGNoaWxkcmVuLm1hcChmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgcmV0dXJuIGNoaWxkLmtleTtcbiAgICAgIH0pKVxuICAgIH0pKTtcblxuICAgIC8vIHB1c2ggcm9vdCBub2RlIHRvIGJsb2NrTWFwXG4gICAgYmxvY2tNYXAgPSBibG9ja01hcC5zZXQoY29udGVudEJsb2NrTm9kZS5nZXRLZXkoKSwgY29udGVudEJsb2NrTm9kZSk7XG5cbiAgICAvLyB0aGlzIHN0YWNrIGlzIHVzZWQgdG8gZW5zdXJlIHdlIHZpc2l0IGFsbCBub2RlcyByZXNwZWN0aW5nIGRlcHRoIG9yZGVyaW5nXG4gICAgdmFyIHN0YWNrID0gdXBkYXRlTm9kZVN0YWNrKFtdLCBjaGlsZHJlbiwgY29udGVudEJsb2NrTm9kZSk7XG5cbiAgICAvLyBzdGFydCBjb21wdXRpbmcgY2hpbGRyZW4gbm9kZXNcbiAgICB3aGlsZSAoc3RhY2subGVuZ3RoID4gMCkge1xuICAgICAgLy8gd2UgcG9wIGZyb20gdGhlIHN0YWNrIGFuZCBzdGFydCBwcm9jZXNzaW5nIHRoaXMgbm9kZVxuICAgICAgdmFyIG5vZGUgPSBzdGFjay5wb3AoKTtcblxuICAgICAgLy8gcGFyZW50UmVmIGFscmVhZHkgcG9pbnRzIHRvIGEgY29udmVydGVkIENvbnRlbnRCbG9ja05vZGVcbiAgICAgIHZhciBwYXJlbnRSZWYgPSBub2RlLnBhcmVudFJlZjtcbiAgICAgIHZhciBzaWJsaW5ncyA9IHBhcmVudFJlZi5nZXRDaGlsZEtleXMoKTtcbiAgICAgIHZhciBfaW5kZXggPSBzaWJsaW5ncy5pbmRleE9mKG5vZGUua2V5KTtcbiAgICAgIHZhciBpc1ZhbGlkQmxvY2sgPSBBcnJheS5pc0FycmF5KG5vZGUuY2hpbGRyZW4pO1xuXG4gICAgICBpZiAoIWlzVmFsaWRCbG9jaykge1xuICAgICAgICAhaXNWYWxpZEJsb2NrID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ2ludmFsaWQgUmF3RHJhZnRDb250ZW50QmxvY2sgY2FuIG5vdCBiZSBjb252ZXJ0ZWQgdG8gQ29udGVudEJsb2NrTm9kZScpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIC8vIGVuc3VyZSBjaGlsZHJlbiBoYXZlIHZhbGlkIGtleXMgdG8gZW5hYmxlIHNpYmxpbmcgbGlua3NcbiAgICAgIHZhciBfY2hpbGRyZW4gPSBub2RlLmNoaWxkcmVuLm1hcChhZGRLZXlJZk1pc3NpbmcpO1xuXG4gICAgICB2YXIgX2NvbnRlbnRCbG9ja05vZGUgPSBuZXcgQ29udGVudEJsb2NrTm9kZShfZXh0ZW5kcyh7fSwgZGVjb2RlQmxvY2tOb2RlQ29uZmlnKG5vZGUsIGVudGl0eU1hcCksIHtcbiAgICAgICAgcGFyZW50OiBwYXJlbnRSZWYuZ2V0S2V5KCksXG4gICAgICAgIGNoaWxkcmVuOiBMaXN0KF9jaGlsZHJlbi5tYXAoZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICAgICAgcmV0dXJuIGNoaWxkLmtleTtcbiAgICAgICAgfSkpLFxuICAgICAgICBwcmV2U2libGluZzogX2luZGV4ID09PSAwID8gbnVsbCA6IHNpYmxpbmdzLmdldChfaW5kZXggLSAxKSxcbiAgICAgICAgbmV4dFNpYmxpbmc6IF9pbmRleCA9PT0gc2libGluZ3Muc2l6ZSAtIDEgPyBudWxsIDogc2libGluZ3MuZ2V0KF9pbmRleCArIDEpXG4gICAgICB9KSk7XG5cbiAgICAgIC8vIHB1c2ggbm9kZSB0byBibG9ja01hcFxuICAgICAgYmxvY2tNYXAgPSBibG9ja01hcC5zZXQoX2NvbnRlbnRCbG9ja05vZGUuZ2V0S2V5KCksIF9jb250ZW50QmxvY2tOb2RlKTtcblxuICAgICAgLy8gdGhpcyBzdGFjayBpcyB1c2VkIHRvIGVuc3VyZSB3ZSB2aXNpdCBhbGwgbm9kZXMgcmVzcGVjdGluZyBkZXB0aCBvcmRlcmluZ1xuICAgICAgc3RhY2sgPSB1cGRhdGVOb2RlU3RhY2soc3RhY2ssIF9jaGlsZHJlbiwgX2NvbnRlbnRCbG9ja05vZGUpO1xuICAgIH1cblxuICAgIHJldHVybiBibG9ja01hcDtcbiAgfSwgT3JkZXJlZE1hcCgpKTtcbn07XG5cbnZhciBkZWNvZGVDb250ZW50QmxvY2tzID0gZnVuY3Rpb24gZGVjb2RlQ29udGVudEJsb2NrcyhibG9ja3MsIGVudGl0eU1hcCkge1xuICByZXR1cm4gT3JkZXJlZE1hcChibG9ja3MubWFwKGZ1bmN0aW9uIChibG9jaykge1xuICAgIHZhciBjb250ZW50QmxvY2sgPSBuZXcgQ29udGVudEJsb2NrKGRlY29kZUJsb2NrTm9kZUNvbmZpZyhibG9jaywgZW50aXR5TWFwKSk7XG4gICAgcmV0dXJuIFtjb250ZW50QmxvY2suZ2V0S2V5KCksIGNvbnRlbnRCbG9ja107XG4gIH0pKTtcbn07XG5cbnZhciBkZWNvZGVSYXdCbG9ja3MgPSBmdW5jdGlvbiBkZWNvZGVSYXdCbG9ja3MocmF3U3RhdGUsIGVudGl0eU1hcCkge1xuICB2YXIgaXNUcmVlUmF3QmxvY2sgPSBBcnJheS5pc0FycmF5KHJhd1N0YXRlLmJsb2Nrc1swXS5jaGlsZHJlbik7XG4gIHZhciByYXdCbG9ja3MgPSBleHBlcmltZW50YWxUcmVlRGF0YVN1cHBvcnQgJiYgIWlzVHJlZVJhd0Jsb2NrID8gRHJhZnRUcmVlQWRhcHRlci5mcm9tUmF3U3RhdGVUb1Jhd1RyZWVTdGF0ZShyYXdTdGF0ZSkuYmxvY2tzIDogcmF3U3RhdGUuYmxvY2tzO1xuXG4gIGlmICghZXhwZXJpbWVudGFsVHJlZURhdGFTdXBwb3J0KSB7XG4gICAgcmV0dXJuIGRlY29kZUNvbnRlbnRCbG9ja3MoaXNUcmVlUmF3QmxvY2sgPyBEcmFmdFRyZWVBZGFwdGVyLmZyb21SYXdUcmVlU3RhdGVUb1Jhd1N0YXRlKHJhd1N0YXRlKS5ibG9ja3MgOiByYXdCbG9ja3MsIGVudGl0eU1hcCk7XG4gIH1cblxuICByZXR1cm4gZGVjb2RlQ29udGVudEJsb2NrTm9kZXMocmF3QmxvY2tzLCBlbnRpdHlNYXApO1xufTtcblxudmFyIGRlY29kZVJhd0VudGl0eU1hcCA9IGZ1bmN0aW9uIGRlY29kZVJhd0VudGl0eU1hcChyYXdTdGF0ZSkge1xuICB2YXIgcmF3RW50aXR5TWFwID0gcmF3U3RhdGUuZW50aXR5TWFwO1xuXG4gIHZhciBlbnRpdHlNYXAgPSB7fTtcblxuICAvLyBUT0RPOiBVcGRhdGUgdGhpcyBvbmNlIHdlIGNvbXBsZXRlbHkgcmVtb3ZlIERyYWZ0RW50aXR5XG4gIE9iamVjdC5rZXlzKHJhd0VudGl0eU1hcCkuZm9yRWFjaChmdW5jdGlvbiAocmF3RW50aXR5S2V5KSB7XG4gICAgdmFyIF9yYXdFbnRpdHlNYXAkcmF3RW50aSA9IHJhd0VudGl0eU1hcFtyYXdFbnRpdHlLZXldLFxuICAgICAgICB0eXBlID0gX3Jhd0VudGl0eU1hcCRyYXdFbnRpLnR5cGUsXG4gICAgICAgIG11dGFiaWxpdHkgPSBfcmF3RW50aXR5TWFwJHJhd0VudGkubXV0YWJpbGl0eSxcbiAgICAgICAgZGF0YSA9IF9yYXdFbnRpdHlNYXAkcmF3RW50aS5kYXRhO1xuXG4gICAgLy8gZ2V0IHRoZSBrZXkgcmVmZXJlbmNlIHRvIGNyZWF0ZWQgZW50aXR5XG5cbiAgICBlbnRpdHlNYXBbcmF3RW50aXR5S2V5XSA9IERyYWZ0RW50aXR5Ll9fY3JlYXRlKHR5cGUsIG11dGFiaWxpdHksIGRhdGEgfHwge30pO1xuICB9KTtcblxuICByZXR1cm4gZW50aXR5TWFwO1xufTtcblxudmFyIGNvbnZlcnRGcm9tUmF3VG9EcmFmdFN0YXRlID0gZnVuY3Rpb24gY29udmVydEZyb21SYXdUb0RyYWZ0U3RhdGUocmF3U3RhdGUpIHtcbiAgIUFycmF5LmlzQXJyYXkocmF3U3RhdGUuYmxvY2tzKSA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdpbnZhbGlkIFJhd0RyYWZ0Q29udGVudFN0YXRlJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gIC8vIGRlY29kZSBlbnRpdGllc1xuICB2YXIgZW50aXR5TWFwID0gZGVjb2RlUmF3RW50aXR5TWFwKHJhd1N0YXRlKTtcblxuICAvLyBkZWNvZGUgYmxvY2tNYXBcbiAgdmFyIGJsb2NrTWFwID0gZGVjb2RlUmF3QmxvY2tzKHJhd1N0YXRlLCBlbnRpdHlNYXApO1xuXG4gIC8vIGNyZWF0ZSBpbml0aWFsIHNlbGVjdGlvblxuICB2YXIgc2VsZWN0aW9uU3RhdGUgPSBibG9ja01hcC5pc0VtcHR5KCkgPyBuZXcgU2VsZWN0aW9uU3RhdGUoKSA6IFNlbGVjdGlvblN0YXRlLmNyZWF0ZUVtcHR5KGJsb2NrTWFwLmZpcnN0KCkuZ2V0S2V5KCkpO1xuXG4gIHJldHVybiBuZXcgQ29udGVudFN0YXRlKHtcbiAgICBibG9ja01hcDogYmxvY2tNYXAsXG4gICAgZW50aXR5TWFwOiBlbnRpdHlNYXAsXG4gICAgc2VsZWN0aW9uQmVmb3JlOiBzZWxlY3Rpb25TdGF0ZSxcbiAgICBzZWxlY3Rpb25BZnRlcjogc2VsZWN0aW9uU3RhdGVcbiAgfSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbnZlcnRGcm9tUmF3VG9EcmFmdFN0YXRlOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUVBOzs7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7Ozs7Ozs7O0FBTUE7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFDQTtBQUNBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFNQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/convertFromRawToDraftState.js
