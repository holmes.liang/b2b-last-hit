__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentRegistry", function() { return ComponentRegistry; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeskComponentRegistry", function() { return DeskComponentRegistry; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");



var ComponentRegistry =
/*#__PURE__*/
function () {
  function ComponentRegistry() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, ComponentRegistry);

    this.all = {};
    this.readonlyAll = [];
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(ComponentRegistry, [{
    key: "register",
    value: function register(name, comp, readonly) {
      var _this = this;

      if (typeof name === "string") {
        this.all[name] = Object(_component__WEBPACK_IMPORTED_MODULE_2__["VisibleWidget"])(comp);
      } else {
        Object.keys(name).forEach(function (key) {
          _this.all[key] = Object(_component__WEBPACK_IMPORTED_MODULE_2__["VisibleWidget"])(name[key]);
        });
      }

      return this;
    }
  }, {
    key: "find",
    value: function find(name) {
      var comp = this.all[name];

      if (comp == null) {
        throw new Error("Component[".concat(name, "] not found."));
      }

      return comp;
    }
  }, {
    key: "asReadOnly",
    value: function asReadOnly(comp) {
      var _this2 = this;

      this.readonlyAll.push(comp);

      for (var _len = arguments.length, rest = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        rest[_key - 1] = arguments[_key];
      }

      rest.forEach(function (one) {
        return _this2.readonlyAll.push(one);
      });
      return this;
    }
  }, {
    key: "isReadOnly",
    value: function isReadOnly(comp) {
      return this.readonlyAll.includes(comp) || (comp.getOriginalWidget ? this.readonlyAll.includes(comp.getOriginalWidget()) : false);
    }
  }]);

  return ComponentRegistry;
}();
var DeskComponentRegistry = new ComponentRegistry();//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tcG9uZW50L2NvbXBvbmVudC1yZWdpc3RyeS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21wb25lbnQvY29tcG9uZW50LXJlZ2lzdHJ5LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBWaXNpYmxlV2lkZ2V0IH0gZnJvbSBcIkBjb21wb25lbnRcIjtcblxuZXhwb3J0IGNsYXNzIENvbXBvbmVudFJlZ2lzdHJ5IHtcbiAgcHJpdmF0ZSBhbGwgPSB7fSBhcyB7IFtwcm9wTmFtZTogc3RyaW5nXTogYW55IH07XG4gIHByaXZhdGUgcmVhZG9ubHlBbGwgPSBbXSBhcyBhbnlbXTtcblxuICByZWdpc3RlcihcbiAgICBuYW1lOlxuICAgICAgfCBzdHJpbmdcbiAgICAgIHwge1xuICAgICAgW3Byb3BOYW1lOiBzdHJpbmddOiBhbnk7XG4gICAgfSxcbiAgICBjb21wPzogYW55LFxuICAgIHJlYWRvbmx5PzogYm9vbGVhbixcbiAgKTogdGhpcyB7XG4gICAgaWYgKHR5cGVvZiBuYW1lID09PSBcInN0cmluZ1wiKSB7XG4gICAgICB0aGlzLmFsbFtuYW1lXSA9IFZpc2libGVXaWRnZXQoY29tcCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIE9iamVjdC5rZXlzKG5hbWUpLmZvckVhY2goa2V5ID0+IHtcbiAgICAgICAgdGhpcy5hbGxba2V5XSA9IFZpc2libGVXaWRnZXQobmFtZVtrZXldKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIGZpbmQobmFtZTogc3RyaW5nKTogYW55IHtcbiAgICBjb25zdCBjb21wID0gdGhpcy5hbGxbbmFtZV07XG4gICAgaWYgKGNvbXAgPT0gbnVsbCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKGBDb21wb25lbnRbJHtuYW1lfV0gbm90IGZvdW5kLmApO1xuICAgIH1cbiAgICByZXR1cm4gY29tcDtcbiAgfVxuXG4gIGFzUmVhZE9ubHkoY29tcDogYW55LCAuLi5yZXN0OiBhbnlbXSk6IHRoaXMge1xuICAgIHRoaXMucmVhZG9ubHlBbGwucHVzaChjb21wKTtcbiAgICByZXN0LmZvckVhY2gob25lID0+IHRoaXMucmVhZG9ubHlBbGwucHVzaChvbmUpKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIGlzUmVhZE9ubHkoY29tcDogYW55KTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIChcbiAgICAgIHRoaXMucmVhZG9ubHlBbGwuaW5jbHVkZXMoY29tcCkgfHxcbiAgICAgIChjb21wLmdldE9yaWdpbmFsV2lkZ2V0XG4gICAgICAgID8gdGhpcy5yZWFkb25seUFsbC5pbmNsdWRlcyhjb21wLmdldE9yaWdpbmFsV2lkZ2V0KCkpXG4gICAgICAgIDogZmFsc2UpXG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgY29uc3QgRGVza0NvbXBvbmVudFJlZ2lzdHJ5ID0gbmV3IENvbXBvbmVudFJlZ2lzdHJ5KCk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQXJCQTtBQUFBO0FBQUE7QUF3QkE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQTdCQTtBQUFBO0FBQUE7QUErQkE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFuQ0E7QUFBQTtBQUFBO0FBc0NBO0FBTUE7QUE1Q0E7QUFDQTtBQURBO0FBQUE7QUErQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/component/component-registry.tsx
