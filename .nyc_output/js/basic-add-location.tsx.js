__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicAddLocation", function() { return BasicAddLocation; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var _all_steps__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/antd/button-back */ "./src/app/desk/component/antd/button-back.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_16__);








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/basic-add-location.tsx";









var formDateLayout = {
  labelCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  },
  wrapperCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  }
};
var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();

var BasicAddLocation =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(BasicAddLocation, _QuoteStep);

  function BasicAddLocation() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, BasicAddLocation);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicAddLocation)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.oldCoverages = {};
    _this.loadCodeTables =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var codeTables, arr;
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              codeTables = _this.state.codeTables;
              arr = [];
              ["nationality"].forEach(function (item) {
                arr.push(new Promise(function (resolve, reject) {
                  _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].get("/mastertable", {
                    itntCode: _this.getValueFromModel(_this.getProps("itntCode", "policy")),
                    productCode: _this.getValueFromModel(_this.getProps("productCode", "policy")),
                    productVersion: _this.getValueFromModel(_this.getProps("productVersion", "policy")),
                    tableName: item
                  }, {}).then(function (response) {
                    var respData = response.body.respData;
                    codeTables[item] = respData.items || [];
                    resolve();
                  }).catch(function (error) {});
                }));
              });
              Promise.all(arr).then(function (values) {
                _this.setState({
                  codeTables: codeTables
                });
              }, function (reason) {
                console.log(reason);
              });

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    _this.getMasterTableItemText = function (tableName, itemId) {
      var codeTables = _this.state.codeTables;

      if (itemId) {
        var item = (codeTables[tableName] || []).find(function (option) {
          return itemId.toString() === option.id.toString();
        }) || {};
        return item.text || "";
      } else {
        return "";
      }
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(BasicAddLocation, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicAddLocation.prototype), "initComponents", this).call(this));
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        loading: false,
        countryCode: "SGP",
        action: "",
        codeTables: {}
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var action = lodash__WEBPACK_IMPORTED_MODULE_16___default.a.get(this.props.model, "member.action", "");

      if (action === "edit") {
        this.setState({
          action: action,
          index: lodash__WEBPACK_IMPORTED_MODULE_16___default.a.get(this.props.model, "member.index")
        });
      } else {
        this.setState({
          action: "new"
        });
      }

      this.loadCodeTables();
    }
  }, {
    key: "renderTitle",
    value: function renderTitle() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "handleNext",
    value: function handleNext() {}
  }, {
    key: "renderActions",
    value: function renderActions() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "renderRightActions",
    value: function renderRightActions() {
      var _this2 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "action ".concat(isMobile ? "mobile-action-large" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_15__["ButtonBack"], {
        handel: function handel() {
          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__["pageTo"])(_this2, _all_steps__WEBPACK_IMPORTED_MODULE_14__["AllSteps"].LOCATION);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 136
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Button"], {
        size: "large",
        type: "primary",
        onClick: function onClick() {
          return _this2.handleNext();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Submit").thai("Submit").my("Submit").getMessage()));
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      return "";
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      return "";
    }
  }, {
    key: "getProps",
    value: function getProps(propName, dataFixed) {
      return "";
    }
  }, {
    key: "renderAddress",
    value: function renderAddress() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "renderCoverage",
    value: function renderCoverage() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var formProps = {
        form: this.props.form,
        model: this.props.model
      };
      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      var countryCode = this.state.countryCode;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Spin"], {
        size: "large",
        spinning: this.state.loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 179
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        type: "flex",
        justify: "space-between",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 180
        },
        __self: this
      }, this.renderSideInfo(), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        sm: 18,
        xs: 23,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 182
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 183
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Location").thai("Location").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NCollapse"], {
        defaultActiveKey: ["1", "2"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 186
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("LOCATION ADDRESS").thai("LOCATION ADDRESS").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 187
        },
        __self: this
      }, this.renderAddress()), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPanel"], {
        key: "2",
        header: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("COVERAGES").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 195
        },
        __self: this
      }, this.renderCoverage())), this.renderRightActions())));
    }
  }]);

  return BasicAddLocation;
}(_quote_step__WEBPACK_IMPORTED_MODULE_13__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtYWRkLWxvY2F0aW9uLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL1NBSUMvaWFyL2Jhc2ljLWFkZC1sb2NhdGlvbi50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IEJ1dHRvbiwgQ29sLCBub3RpZmljYXRpb24sIFJvdywgU3BpbiB9IGZyb20gXCJhbnRkXCI7XG5cbmltcG9ydCB7IEFqYXgsIExhbmd1YWdlLCBVdGlscywgQXBpcywgQ29uc3RzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5Db2xsYXBzZSwgTkRhdGUsIE5EYXRlRmlsdGVyLCBOUGFuZWwsIE5SYWRpbywgTlNlbGVjdCwgTlRleHQgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IHBhZ2VUbyB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvd2l6YXJkXCI7XG5cbmltcG9ydCBRdW90ZVN0ZXAsIHsgU3RlcENvbXBvbmVudHMsIFN0ZXBQcm9wcyB9IGZyb20gXCIuLi8uLi9xdW90ZS1zdGVwXCI7XG5pbXBvcnQgeyBBbGxTdGVwcyB9IGZyb20gXCIuLi9hbGwtc3RlcHNcIjtcbmltcG9ydCB7IEJ1dHRvbkJhY2sgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L2FudGQvYnV0dG9uLWJhY2tcIjtcbmltcG9ydCBTaWRlSW5mbyBmcm9tIFwiLi9jb21wb25lbnQvc2lkZS1pbmZvXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBsaXN0IH0gZnJvbSBcIi4vaW5kZXhcIjtcbmltcG9ydCB7IEFqYXhSZXNwb25zZSB9IGZyb20gXCJAbXktdHlwZXNcIjtcblxuY29uc3QgZm9ybURhdGVMYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTIgfSxcbiAgICBzbTogeyBzcGFuOiAxMiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTIgfSxcbiAgICBzbTogeyBzcGFuOiAxMiB9LFxuICB9LFxufTtcbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcblxuaW50ZXJmYWNlIElTdGF0ZSB7XG4gIGxvYWRpbmc6IGJvb2xlYW47XG4gIGNvdW50cnlDb2RlOiBhbnk7XG4gIGFjdGlvbjogYW55O1xuICBpbmRleDogYW55O1xuICBjb2RlVGFibGVzOiBhbnk7XG59XG5cbmludGVyZmFjZSBJUHJvcHMgZXh0ZW5kcyBTdGVwUHJvcHMge1xufVxuXG5jbGFzcyBCYXNpY0FkZExvY2F0aW9uPFAgZXh0ZW5kcyBJUHJvcHMsIFMgZXh0ZW5kcyBJU3RhdGUsIEMgZXh0ZW5kcyBTdGVwQ29tcG9uZW50cz4gZXh0ZW5kcyBRdW90ZVN0ZXA8UCwgUywgQz4ge1xuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdENvbXBvbmVudHMoKSkgYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgICAgY291bnRyeUNvZGU6IFwiU0dQXCIsXG4gICAgICBhY3Rpb246IFwiXCIsXG4gICAgICBjb2RlVGFibGVzOiB7fSxcbiAgICB9IGFzIFM7XG4gIH1cblxuICBvbGRDb3ZlcmFnZXM6IGFueSA9IHt9O1xuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGNvbnN0IGFjdGlvbiA9IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIFwibWVtYmVyLmFjdGlvblwiLCBcIlwiKTtcbiAgICBpZiAoYWN0aW9uID09PSBcImVkaXRcIikge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGFjdGlvbixcbiAgICAgICAgaW5kZXg6IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIFwibWVtYmVyLmluZGV4XCIpLFxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBhY3Rpb246IFwibmV3XCIsXG4gICAgICB9KTtcbiAgICB9XG4gICAgdGhpcy5sb2FkQ29kZVRhYmxlcygpO1xuICB9XG5cbiAgbG9hZENvZGVUYWJsZXMgPSBhc3luYyAoKSA9PiB7XG4gICAgbGV0IHsgY29kZVRhYmxlcyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBsZXQgYXJyOiBhbnkgPSBbXTtcbiAgICBbXG4gICAgICBcIm5hdGlvbmFsaXR5XCIsXG4gICAgXS5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgYXJyLnB1c2goXG4gICAgICAgIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICBBamF4LmdldChcbiAgICAgICAgICAgIGAvbWFzdGVydGFibGVgLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBpdG50Q29kZTogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKFwiaXRudENvZGVcIiwgXCJwb2xpY3lcIikpLFxuICAgICAgICAgICAgICBwcm9kdWN0Q29kZTogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKFwicHJvZHVjdENvZGVcIiwgXCJwb2xpY3lcIikpLFxuICAgICAgICAgICAgICBwcm9kdWN0VmVyc2lvbjogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKFwicHJvZHVjdFZlcnNpb25cIiwgXCJwb2xpY3lcIikpLFxuICAgICAgICAgICAgICB0YWJsZU5hbWU6IGl0ZW0sXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge30sXG4gICAgICAgICAgKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzcG9uc2UuYm9keTtcbiAgICAgICAgICAgICAgY29kZVRhYmxlc1tpdGVtXSA9IHJlc3BEYXRhLml0ZW1zIHx8IFtdO1xuICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KSxcbiAgICAgICk7XG4gICAgfSk7XG4gICAgUHJvbWlzZS5hbGwoYXJyKS50aGVuKFxuICAgICAgdmFsdWVzID0+IHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGNvZGVUYWJsZXM6IGNvZGVUYWJsZXMgfSk7XG4gICAgICB9LFxuICAgICAgcmVhc29uID0+IHtcbiAgICAgICAgY29uc29sZS5sb2cocmVhc29uKTtcbiAgICAgIH0sXG4gICAgKTtcbiAgfTtcblxuICBnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0ID0gKHRhYmxlTmFtZTogc3RyaW5nLCBpdGVtSWQ6IHN0cmluZykgPT4ge1xuICAgIGNvbnN0IHsgY29kZVRhYmxlcyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBpZiAoaXRlbUlkKSB7XG4gICAgICBjb25zdCBpdGVtID1cbiAgICAgICAgKGNvZGVUYWJsZXNbdGFibGVOYW1lXSB8fCBbXSkuZmluZCgob3B0aW9uOiBhbnkpID0+IHtcbiAgICAgICAgICByZXR1cm4gaXRlbUlkLnRvU3RyaW5nKCkgPT09IG9wdGlvbi5pZC50b1N0cmluZygpO1xuICAgICAgICB9KSB8fCB7fTtcbiAgICAgIHJldHVybiBpdGVtLnRleHQgfHwgXCJcIjtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIFwiXCI7XG4gICAgfVxuICB9O1xuXG4gIHByb3RlY3RlZCByZW5kZXJUaXRsZSgpIHtcbiAgICByZXR1cm4gPD48Lz47XG4gIH1cblxuICBoYW5kbGVOZXh0KCkge1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlckFjdGlvbnMoKSB7XG4gICAgcmV0dXJuIDw+PC8+O1xuICB9XG5cbiAgcmVuZGVyUmlnaHRBY3Rpb25zKCkge1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgYWN0aW9uICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1hY3Rpb24tbGFyZ2VcIiA6IFwiXCJ9YH0+XG4gICAgICAgIDxCdXR0b25CYWNrIGhhbmRlbD17KCkgPT4ge1xuICAgICAgICAgIHBhZ2VUbyh0aGlzLCBBbGxTdGVwcy5MT0NBVElPTik7XG4gICAgICAgIH19Lz5cbiAgICAgICAgPEJ1dHRvbiBzaXplPVwibGFyZ2VcIiB0eXBlPVwicHJpbWFyeVwiIG9uQ2xpY2s9eygpID0+IHRoaXMuaGFuZGxlTmV4dCgpfT5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJTdWJtaXRcIikudGhhaShcIlN1Ym1pdFwiKS5teShcIlN1Ym1pdFwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvQnV0dG9uPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxuXG4gIGdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkUHJlZml4Pzogc3RyaW5nKTogc3RyaW5nIHtcbiAgICByZXR1cm4gXCJcIjtcbiAgfVxuXG4gIGdldFByb3AocHJvcE5hbWU/OiBzdHJpbmcpIHtcbiAgICByZXR1cm4gXCJcIjtcbiAgfVxuXG4gIGdldFByb3BzKHByb3BOYW1lOiBhbnksIGRhdGFGaXhlZD86IGFueSkge1xuICAgIHJldHVybiBcIlwiO1xuICB9XG5cbiAgcmVuZGVyQWRkcmVzcygpIHtcbiAgICByZXR1cm4gPD48Lz47XG4gIH1cblxuICByZW5kZXJDb3ZlcmFnZSgpIHtcbiAgICByZXR1cm4gPD48Lz47XG4gIH1cblxuICByZW5kZXJTaWRlSW5mbygpIHtcbiAgICByZXR1cm4gPD48Lz47XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyQ29udGVudCgpIHtcbiAgICBjb25zdCBmb3JtUHJvcHMgPSB7XG4gICAgICBmb3JtOiB0aGlzLnByb3BzLmZvcm0sXG4gICAgICBtb2RlbDogdGhpcy5wcm9wcy5tb2RlbCxcbiAgICB9O1xuICAgIGNvbnN0IHsgZm9ybSwgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBjb3VudHJ5Q29kZSB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHJldHVybiAoXG4gICAgICA8U3BpbiBzaXplPVwibGFyZ2VcIiBzcGlubmluZz17dGhpcy5zdGF0ZS5sb2FkaW5nfT5cbiAgICAgICAgPFJvdyB0eXBlPVwiZmxleFwiIGp1c3RpZnk9e1wic3BhY2UtYmV0d2VlblwifT5cbiAgICAgICAgICB7dGhpcy5yZW5kZXJTaWRlSW5mbygpfVxuICAgICAgICAgIDxDb2wgc209ezE4fSB4cz17MjN9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0aXRsZVwiPlxuICAgICAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJMb2NhdGlvblwiKS50aGFpKFwiTG9jYXRpb25cIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8TkNvbGxhcHNlIGRlZmF1bHRBY3RpdmVLZXk9e1tcIjFcIiwgXCIyXCJdfT5cbiAgICAgICAgICAgICAgPE5QYW5lbFxuICAgICAgICAgICAgICAgIGtleT1cIjFcIlxuICAgICAgICAgICAgICAgIGhlYWRlcj17TGFuZ3VhZ2UuZW4oXCJMT0NBVElPTiBBRERSRVNTXCIpXG4gICAgICAgICAgICAgICAgICAudGhhaShcIkxPQ0FUSU9OIEFERFJFU1NcIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJBZGRyZXNzKCl9XG4gICAgICAgICAgICAgIDwvTlBhbmVsPlxuICAgICAgICAgICAgICA8TlBhbmVsIGtleT1cIjJcIlxuICAgICAgICAgICAgICAgICAgICAgIGhlYWRlcj17TGFuZ3VhZ2UuZW4oXCJDT1ZFUkFHRVNcIikuZ2V0TWVzc2FnZSgpfT5cbiAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJDb3ZlcmFnZSgpfVxuICAgICAgICAgICAgICA8L05QYW5lbD5cbiAgICAgICAgICAgIDwvTkNvbGxhcHNlPlxuICAgICAgICAgICAge3RoaXMucmVuZGVyUmlnaHRBY3Rpb25zKCl9XG4gICAgICAgICAgPC9Db2w+XG4gICAgICAgIDwvUm93PlxuICAgICAgPC9TcGluPlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IHsgQmFzaWNBZGRMb2NhdGlvbiB9O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFVQTtBQUNBO0FBWUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBY0E7QUFpQkE7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUVBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBRUE7QUFDQTtBQUNBO0FBR0E7QUFFQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFuQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBaEZBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTs7O0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7OztBQXFEQTtBQUNBO0FBQ0E7OztBQUVBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7Ozs7QUF0S0E7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/basic-add-location.tsx
