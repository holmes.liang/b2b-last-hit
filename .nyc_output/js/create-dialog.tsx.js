__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/create-dialog.tsx";
// mask.js


/* harmony default export */ __webpack_exports__["default"] = ({
  dom: null,
  //被append的元素
  dialogType: "ant-modal-mask",
  create: function create(_ref) {
    var _this = this;

    var Component = _ref.Component,
        _onOk = _ref.onOk,
        _onCancel = _ref.onCancel,
        dialogType = _ref.dialogType;
    this.initDom();
    dialogType && (this.dialogType = dialogType);
    this.dom = document.createElement("div"); // JSX代码

    var JSXdom = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Component, {
      onOk: function onOk() {
        return _this.onOk(_onOk);
      },
      onCancel: function onCancel() {
        return _this.onCancel(_onCancel);
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }));
    react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(JSXdom, this.dom);
    if (this.dom !== null) document.body.appendChild(this.dom);
  },
  onCancel: function onCancel(_onCancel2) {
    _onCancel2 instanceof Function && _onCancel2();
    this.close();
  },
  onOk: function onOk(_onOk2) {
    _onOk2 instanceof Function && _onOk2();
    this.close();
  },
  initDom: function initDom() {
    this.dom && this.dom.remove();
  },
  close: function close() {
    this.dom && this.dom.remove();
    var antMasks = this.dialogType && document.getElementsByClassName(this.dialogType);
    antMasks && antMasks.length > 0 && antMasks[antMasks.length - 1].parentNode.remove();
  }
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2NyZWF0ZS1kaWFsb2cudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2NyZWF0ZS1kaWFsb2cudHN4Il0sInNvdXJjZXNDb250ZW50IjpbIi8vIG1hc2suanNcbmltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBSZWFjdERPTSBmcm9tIFwicmVhY3QtZG9tXCI7XG5cbmludGVyZmFjZSBJQ3JlYXRlIHtcbiAgQ29tcG9uZW50OiBhbnk7XG4gIG9uT2s/OiBGdW5jdGlvbjtcbiAgb25DYW5jZWw/OiBGdW5jdGlvbjtcbiAgZGlhbG9nVHlwZT86IFwiYW50LW1vZGFsLW1hc2tcIiB8IFwiYW50LWRyYXdlclwiO1xufVxuXG5pbnRlcmZhY2UgSURpYWxvZyB7XG4gIGRvbTogYW55O1xuICBkaWFsb2dUeXBlOiBcImFudC1tb2RhbC1tYXNrXCIgfCBcImFudC1kcmF3ZXJcIiB8IHVuZGVmaW5lZDtcbiAgY3JlYXRlOiBhbnk7XG4gIG9uQ2FuY2VsOiBhbnk7XG4gIG9uT2s6IGFueTtcbiAgaW5pdERvbTogYW55O1xuICBjbG9zZTogYW55O1xufVxuXG5leHBvcnQgZGVmYXVsdCB7XG4gIGRvbTogbnVsbCwgLy/ooqthcHBlbmTnmoTlhYPntKBcbiAgZGlhbG9nVHlwZTogXCJhbnQtbW9kYWwtbWFza1wiLFxuICBjcmVhdGUoeyBDb21wb25lbnQsIG9uT2ssIG9uQ2FuY2VsLCBkaWFsb2dUeXBlIH06IElDcmVhdGUpIHtcbiAgICB0aGlzLmluaXREb20oKTtcbiAgICBkaWFsb2dUeXBlICYmICh0aGlzLmRpYWxvZ1R5cGUgPSBkaWFsb2dUeXBlKTtcblxuICAgIHRoaXMuZG9tID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKSBhcyBhbnk7XG4gICAgLy8gSlNY5Luj56CBXG4gICAgY29uc3QgSlNYZG9tID0gKFxuICAgICAgPGRpdj5cbiAgICAgICAgPENvbXBvbmVudCBvbk9rPXsoKSA9PiB0aGlzLm9uT2sob25Payl9IG9uQ2FuY2VsPXsoKSA9PiB0aGlzLm9uQ2FuY2VsKG9uQ2FuY2VsKX0vPlxuICAgICAgPC9kaXY+XG4gICAgKTtcblxuICAgIFJlYWN0RE9NLnJlbmRlcihKU1hkb20sIHRoaXMuZG9tKTtcbiAgICBpZiAodGhpcy5kb20gIT09IG51bGwpIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5kb20pO1xuXG4gIH0sXG5cbiAgb25DYW5jZWwob25DYW5jZWw6IGFueSkge1xuICAgIG9uQ2FuY2VsIGluc3RhbmNlb2YgRnVuY3Rpb24gJiYgb25DYW5jZWwoKTtcbiAgICB0aGlzLmNsb3NlKCk7XG4gIH0sXG5cbiAgb25Payhvbk9rOiBhbnkpIHtcbiAgICBvbk9rIGluc3RhbmNlb2YgRnVuY3Rpb24gJiYgb25PaygpO1xuICAgIHRoaXMuY2xvc2UoKTtcbiAgfSxcbiAgaW5pdERvbSgpIHtcbiAgICB0aGlzLmRvbSAmJiB0aGlzLmRvbS5yZW1vdmUoKTtcbiAgfSxcblxuICBjbG9zZSgpIHtcbiAgICB0aGlzLmRvbSAmJiB0aGlzLmRvbS5yZW1vdmUoKTtcbiAgICBjb25zdCBhbnRNYXNrczogYW55ID0gdGhpcy5kaWFsb2dUeXBlICYmIGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUodGhpcy5kaWFsb2dUeXBlKTtcbiAgICBhbnRNYXNrcyAmJiBhbnRNYXNrcy5sZW5ndGggPiAwICYmIGFudE1hc2tzW2FudE1hc2tzLmxlbmd0aCAtIDFdLnBhcmVudE5vZGUucmVtb3ZlKCk7XG4gIH0sXG59IGFzIElEaWFsb2c7XG4iXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQW1CQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBckNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/create-dialog.tsx
