__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dom-scroll-into-view */ "./node_modules/dom-scroll-into-view/lib/index.js");
/* harmony import */ var dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var lodash_has__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash/has */ "./node_modules/lodash/has.js");
/* harmony import */ var lodash_has__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash_has__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _createBaseForm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./createBaseForm */ "./node_modules/rc-form/es/createBaseForm.js");
/* harmony import */ var _createForm__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./createForm */ "./node_modules/rc-form/es/createForm.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./utils */ "./node_modules/rc-form/es/utils.js");








function computedStyle(el, prop) {
  var getComputedStyle = window.getComputedStyle;
  var style = // If we have getComputedStyle
  getComputedStyle ? // Query it
  // TODO: From CSS-Query notes, we might need (node, null) for FF
  getComputedStyle(el) : // Otherwise, we are in IE and use currentStyle
  el.currentStyle;

  if (style) {
    return style[// Switch to camelCase for CSSOM
    // DEV: Grabbed from jQuery
    // https://github.com/jquery/jquery/blob/1.9-stable/src/css.js#L191-L194
    // https://github.com/jquery/jquery/blob/1.9-stable/src/core.js#L593-L597
    prop.replace(/-(\w)/gi, function (word, letter) {
      return letter.toUpperCase();
    })];
  }

  return undefined;
}

function getScrollableContainer(n) {
  var node = n;
  var nodeName = void 0;
  /* eslint no-cond-assign:0 */

  while ((nodeName = node.nodeName.toLowerCase()) !== 'body') {
    var overflowY = computedStyle(node, 'overflowY'); // https://stackoverflow.com/a/36900407/3040605

    if (node !== n && (overflowY === 'auto' || overflowY === 'scroll') && node.scrollHeight > node.clientHeight) {
      return node;
    }

    node = node.parentNode;
  }

  return nodeName === 'body' ? node.ownerDocument : node;
}

var mixin = {
  getForm: function getForm() {
    return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, _createForm__WEBPACK_IMPORTED_MODULE_5__["mixin"].getForm.call(this), {
      validateFieldsAndScroll: this.validateFieldsAndScroll
    });
  },
  validateFieldsAndScroll: function validateFieldsAndScroll(ns, opt, cb) {
    var _this = this;

    var _getParams = Object(_utils__WEBPACK_IMPORTED_MODULE_6__["getParams"])(ns, opt, cb),
        names = _getParams.names,
        callback = _getParams.callback,
        options = _getParams.options;

    var newCb = function newCb(error, values) {
      if (error) {
        var validNames = _this.fieldsStore.getValidFieldsName();

        var firstNode = void 0;
        var firstTop = void 0;
        validNames.forEach(function (name) {
          if (lodash_has__WEBPACK_IMPORTED_MODULE_3___default()(error, name)) {
            var instance = _this.getFieldInstance(name);

            if (instance) {
              var node = react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.findDOMNode(instance);
              var top = node.getBoundingClientRect().top;

              if (node.type !== 'hidden' && (firstTop === undefined || firstTop > top)) {
                firstTop = top;
                firstNode = node;
              }
            }
          }
        });

        if (firstNode) {
          var c = options.container || getScrollableContainer(firstNode);
          dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_2___default()(firstNode, c, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
            onlyScrollIfNeeded: true
          }, options.scroll));
        }
      }

      if (typeof callback === 'function') {
        callback(error, values);
      }
    };

    return this.validateFields(names, options, newCb);
  }
};

function createDOMForm(option) {
  return Object(_createBaseForm__WEBPACK_IMPORTED_MODULE_4__["default"])(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, option), [mixin]);
}

/* harmony default export */ __webpack_exports__["default"] = (createDOMForm);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZm9ybS9lcy9jcmVhdGVET01Gb3JtLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZm9ybS9lcy9jcmVhdGVET01Gb3JtLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfZXh0ZW5kcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcyc7XG5pbXBvcnQgUmVhY3RET00gZnJvbSAncmVhY3QtZG9tJztcbmltcG9ydCBzY3JvbGxJbnRvVmlldyBmcm9tICdkb20tc2Nyb2xsLWludG8tdmlldyc7XG5pbXBvcnQgaGFzIGZyb20gJ2xvZGFzaC9oYXMnO1xuaW1wb3J0IGNyZWF0ZUJhc2VGb3JtIGZyb20gJy4vY3JlYXRlQmFzZUZvcm0nO1xuaW1wb3J0IHsgbWl4aW4gYXMgZm9ybU1peGluIH0gZnJvbSAnLi9jcmVhdGVGb3JtJztcbmltcG9ydCB7IGdldFBhcmFtcyB9IGZyb20gJy4vdXRpbHMnO1xuXG5mdW5jdGlvbiBjb21wdXRlZFN0eWxlKGVsLCBwcm9wKSB7XG4gIHZhciBnZXRDb21wdXRlZFN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGU7XG4gIHZhciBzdHlsZSA9XG4gIC8vIElmIHdlIGhhdmUgZ2V0Q29tcHV0ZWRTdHlsZVxuICBnZXRDb21wdXRlZFN0eWxlID9cbiAgLy8gUXVlcnkgaXRcbiAgLy8gVE9ETzogRnJvbSBDU1MtUXVlcnkgbm90ZXMsIHdlIG1pZ2h0IG5lZWQgKG5vZGUsIG51bGwpIGZvciBGRlxuICBnZXRDb21wdXRlZFN0eWxlKGVsKSA6XG5cbiAgLy8gT3RoZXJ3aXNlLCB3ZSBhcmUgaW4gSUUgYW5kIHVzZSBjdXJyZW50U3R5bGVcbiAgZWwuY3VycmVudFN0eWxlO1xuICBpZiAoc3R5bGUpIHtcbiAgICByZXR1cm4gc3R5bGVbXG4gICAgLy8gU3dpdGNoIHRvIGNhbWVsQ2FzZSBmb3IgQ1NTT01cbiAgICAvLyBERVY6IEdyYWJiZWQgZnJvbSBqUXVlcnlcbiAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vanF1ZXJ5L2pxdWVyeS9ibG9iLzEuOS1zdGFibGUvc3JjL2Nzcy5qcyNMMTkxLUwxOTRcbiAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vanF1ZXJ5L2pxdWVyeS9ibG9iLzEuOS1zdGFibGUvc3JjL2NvcmUuanMjTDU5My1MNTk3XG4gICAgcHJvcC5yZXBsYWNlKC8tKFxcdykvZ2ksIGZ1bmN0aW9uICh3b3JkLCBsZXR0ZXIpIHtcbiAgICAgIHJldHVybiBsZXR0ZXIudG9VcHBlckNhc2UoKTtcbiAgICB9KV07XG4gIH1cbiAgcmV0dXJuIHVuZGVmaW5lZDtcbn1cblxuZnVuY3Rpb24gZ2V0U2Nyb2xsYWJsZUNvbnRhaW5lcihuKSB7XG4gIHZhciBub2RlID0gbjtcbiAgdmFyIG5vZGVOYW1lID0gdm9pZCAwO1xuICAvKiBlc2xpbnQgbm8tY29uZC1hc3NpZ246MCAqL1xuICB3aGlsZSAoKG5vZGVOYW1lID0gbm9kZS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpKSAhPT0gJ2JvZHknKSB7XG4gICAgdmFyIG92ZXJmbG93WSA9IGNvbXB1dGVkU3R5bGUobm9kZSwgJ292ZXJmbG93WScpO1xuICAgIC8vIGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8zNjkwMDQwNy8zMDQwNjA1XG4gICAgaWYgKG5vZGUgIT09IG4gJiYgKG92ZXJmbG93WSA9PT0gJ2F1dG8nIHx8IG92ZXJmbG93WSA9PT0gJ3Njcm9sbCcpICYmIG5vZGUuc2Nyb2xsSGVpZ2h0ID4gbm9kZS5jbGllbnRIZWlnaHQpIHtcbiAgICAgIHJldHVybiBub2RlO1xuICAgIH1cbiAgICBub2RlID0gbm9kZS5wYXJlbnROb2RlO1xuICB9XG4gIHJldHVybiBub2RlTmFtZSA9PT0gJ2JvZHknID8gbm9kZS5vd25lckRvY3VtZW50IDogbm9kZTtcbn1cblxudmFyIG1peGluID0ge1xuICBnZXRGb3JtOiBmdW5jdGlvbiBnZXRGb3JtKCkge1xuICAgIHJldHVybiBfZXh0ZW5kcyh7fSwgZm9ybU1peGluLmdldEZvcm0uY2FsbCh0aGlzKSwge1xuICAgICAgdmFsaWRhdGVGaWVsZHNBbmRTY3JvbGw6IHRoaXMudmFsaWRhdGVGaWVsZHNBbmRTY3JvbGxcbiAgICB9KTtcbiAgfSxcbiAgdmFsaWRhdGVGaWVsZHNBbmRTY3JvbGw6IGZ1bmN0aW9uIHZhbGlkYXRlRmllbGRzQW5kU2Nyb2xsKG5zLCBvcHQsIGNiKSB7XG4gICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgIHZhciBfZ2V0UGFyYW1zID0gZ2V0UGFyYW1zKG5zLCBvcHQsIGNiKSxcbiAgICAgICAgbmFtZXMgPSBfZ2V0UGFyYW1zLm5hbWVzLFxuICAgICAgICBjYWxsYmFjayA9IF9nZXRQYXJhbXMuY2FsbGJhY2ssXG4gICAgICAgIG9wdGlvbnMgPSBfZ2V0UGFyYW1zLm9wdGlvbnM7XG5cbiAgICB2YXIgbmV3Q2IgPSBmdW5jdGlvbiBuZXdDYihlcnJvciwgdmFsdWVzKSB7XG4gICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgdmFyIHZhbGlkTmFtZXMgPSBfdGhpcy5maWVsZHNTdG9yZS5nZXRWYWxpZEZpZWxkc05hbWUoKTtcbiAgICAgICAgdmFyIGZpcnN0Tm9kZSA9IHZvaWQgMDtcbiAgICAgICAgdmFyIGZpcnN0VG9wID0gdm9pZCAwO1xuXG4gICAgICAgIHZhbGlkTmFtZXMuZm9yRWFjaChmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICAgIGlmIChoYXMoZXJyb3IsIG5hbWUpKSB7XG4gICAgICAgICAgICB2YXIgaW5zdGFuY2UgPSBfdGhpcy5nZXRGaWVsZEluc3RhbmNlKG5hbWUpO1xuICAgICAgICAgICAgaWYgKGluc3RhbmNlKSB7XG4gICAgICAgICAgICAgIHZhciBub2RlID0gUmVhY3RET00uZmluZERPTU5vZGUoaW5zdGFuY2UpO1xuICAgICAgICAgICAgICB2YXIgdG9wID0gbm9kZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS50b3A7XG4gICAgICAgICAgICAgIGlmIChub2RlLnR5cGUgIT09ICdoaWRkZW4nICYmIChmaXJzdFRvcCA9PT0gdW5kZWZpbmVkIHx8IGZpcnN0VG9wID4gdG9wKSkge1xuICAgICAgICAgICAgICAgIGZpcnN0VG9wID0gdG9wO1xuICAgICAgICAgICAgICAgIGZpcnN0Tm9kZSA9IG5vZGU7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmIChmaXJzdE5vZGUpIHtcbiAgICAgICAgICB2YXIgYyA9IG9wdGlvbnMuY29udGFpbmVyIHx8IGdldFNjcm9sbGFibGVDb250YWluZXIoZmlyc3ROb2RlKTtcbiAgICAgICAgICBzY3JvbGxJbnRvVmlldyhmaXJzdE5vZGUsIGMsIF9leHRlbmRzKHtcbiAgICAgICAgICAgIG9ubHlTY3JvbGxJZk5lZWRlZDogdHJ1ZVxuICAgICAgICAgIH0sIG9wdGlvbnMuc2Nyb2xsKSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKHR5cGVvZiBjYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICBjYWxsYmFjayhlcnJvciwgdmFsdWVzKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMudmFsaWRhdGVGaWVsZHMobmFtZXMsIG9wdGlvbnMsIG5ld0NiKTtcbiAgfVxufTtcblxuZnVuY3Rpb24gY3JlYXRlRE9NRm9ybShvcHRpb24pIHtcbiAgcmV0dXJuIGNyZWF0ZUJhc2VGb3JtKF9leHRlbmRzKHt9LCBvcHRpb24pLCBbbWl4aW5dKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlRE9NRm9ybTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoREE7QUFDQTtBQWtEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-form/es/createDOMForm.js
