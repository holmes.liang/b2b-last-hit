__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicClause", function() { return BasicClause; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var _all_steps__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/antd/button-back */ "./src/app/desk/component/antd/button-back.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/basic-clause.tsx";








var formDateLayout = {
  labelCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  },
  wrapperCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  }
};
var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();

var BasicClause =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(BasicClause, _QuoteStep);

  function BasicClause() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, BasicClause);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicClause).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(BasicClause, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicClause.prototype), "initComponents", this).call(this));
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        loading: false,
        text: ""
      };
    }
  }, {
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "renderTitle",
    value: function renderTitle() {}
  }, {
    key: "handleNext",
    value: function handleNext() {}
  }, {
    key: "renderActions",
    value: function renderActions() {}
  }, {
    key: "renderClause",
    value: function renderClause() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "renderRightActions",
    value: function renderRightActions() {
      var _this = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        style: {
          marginTop: 30
        },
        className: "action ".concat(isMobile ? "mobile-action-large" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_15__["ButtonBack"], {
        handel: function handel() {
          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__["pageTo"])(_this, _all_steps__WEBPACK_IMPORTED_MODULE_14__["AllSteps"].LOCATION);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Button"], {
        size: "large",
        type: "primary",
        onClick: function onClick() {
          return _this.handleNext();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Save and Continue").thai("Save and Continue").my("Save and Continue").getMessage()));
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      return "";
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      return "";
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var formProps = {
        form: this.props.form,
        model: this.props.model
      };
      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Spin"], {
        size: "large",
        spinning: this.state.loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        type: "flex",
        justify: "space-between",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        },
        __self: this
      }, this.renderSideInfo(), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        sm: 18,
        xs: 23,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 93
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("SPECIAL CLAUSES").thai("SPECIAL CLAUSES").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NCollapse"], {
        defaultActiveKey: ["1", "2"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("SPECIAL CLAUSES").thai("SPECIAL CLAUSES").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }, this.renderClause())), this.renderRightActions())));
    }
  }]);

  return BasicClause;
}(_quote_step__WEBPACK_IMPORTED_MODULE_13__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtY2xhdXNlLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL1NBSUMvaWFyL2Jhc2ljLWNsYXVzZS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IEJ1dHRvbiwgQ29sLCBJbnB1dCwgUm93LCBTcGluIH0gZnJvbSBcImFudGRcIjtcblxuaW1wb3J0IHsgQWpheCwgTGFuZ3VhZ2UsIFV0aWxzLCBBcGlzLCBDb25zdHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgTkNvbGxhcHNlLCBORGF0ZSwgTkRhdGVGaWx0ZXIsIE5QYW5lbCwgTlJhZGlvLCBOU2VsZWN0LCBOVGV4dCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgcGFnZVRvIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC93aXphcmRcIjtcblxuaW1wb3J0IFF1b3RlU3RlcCwgeyBTdGVwQ29tcG9uZW50cywgU3RlcFByb3BzIH0gZnJvbSBcIi4uLy4uL3F1b3RlLXN0ZXBcIjtcbmltcG9ydCB7IEFsbFN0ZXBzIH0gZnJvbSBcIi4uL2FsbC1zdGVwc1wiO1xuaW1wb3J0IHsgQnV0dG9uQmFjayB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvYW50ZC9idXR0b24tYmFja1wiO1xuXG5jb25zdCBmb3JtRGF0ZUxheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxMiB9LFxuICAgIHNtOiB7IHNwYW46IDEyIH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxMiB9LFxuICAgIHNtOiB7IHNwYW46IDEyIH0sXG4gIH0sXG59O1xuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuXG5pbnRlcmZhY2UgSVN0YXRlIHtcbiAgbG9hZGluZzogYm9vbGVhbjtcbiAgdGV4dDogYW55O1xufVxuXG5jbGFzcyBCYXNpY0NsYXVzZTxQIGV4dGVuZHMgU3RlcFByb3BzLCBTIGV4dGVuZHMgSVN0YXRlLCBDIGV4dGVuZHMgU3RlcENvbXBvbmVudHM+IGV4dGVuZHMgUXVvdGVTdGVwPFAsIFMsIEM+IHtcbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRDb21wb25lbnRzKCkpIGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiB7XG4gICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgIHRleHQ6IFwiXCIsXG4gICAgfSBhcyBTO1xuICB9XG5cbiAgYXN5bmMgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyVGl0bGUoKSB7XG4gIH1cblxuICBoYW5kbGVOZXh0KCkge1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlckFjdGlvbnMoKSB7XG4gIH1cblxuICByZW5kZXJDbGF1c2UoKSB7XG4gICAgcmV0dXJuIDw+PC8+O1xuICB9XG5cbiAgcmVuZGVyU2lkZUluZm8oKSB7XG4gICAgcmV0dXJuIDw+PC8+O1xuICB9XG5cbiAgcmVuZGVyUmlnaHRBY3Rpb25zKCkge1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgc3R5bGU9e3sgbWFyZ2luVG9wOiAzMCB9fSBjbGFzc05hbWU9e2BhY3Rpb24gJHtpc01vYmlsZSA/IFwibW9iaWxlLWFjdGlvbi1sYXJnZVwiIDogXCJcIn1gfT5cbiAgICAgICAgPEJ1dHRvbkJhY2sgaGFuZGVsPXsoKSA9PiB7XG4gICAgICAgICAgcGFnZVRvKHRoaXMsIEFsbFN0ZXBzLkxPQ0FUSU9OKTtcbiAgICAgICAgfX0vPlxuICAgICAgICA8QnV0dG9uIHNpemU9XCJsYXJnZVwiIHR5cGU9XCJwcmltYXJ5XCIgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVOZXh0KCl9PlxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIlNhdmUgYW5kIENvbnRpbnVlXCIpLnRoYWkoXCJTYXZlIGFuZCBDb250aW51ZVwiKS5teShcIlNhdmUgYW5kIENvbnRpbnVlXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPC9CdXR0b24+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZTogc3RyaW5nLCBkYXRhSWRQcmVmaXg/OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIHJldHVybiBcIlwiO1xuICB9XG5cbiAgZ2V0UHJvcChwcm9wTmFtZT86IHN0cmluZykge1xuICAgIHJldHVybiBcIlwiO1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlckNvbnRlbnQoKSB7XG4gICAgY29uc3QgZm9ybVByb3BzID0ge1xuICAgICAgZm9ybTogdGhpcy5wcm9wcy5mb3JtLFxuICAgICAgbW9kZWw6IHRoaXMucHJvcHMubW9kZWwsXG4gICAgfTtcbiAgICBjb25zdCB7IGZvcm0sIG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8U3BpbiBzaXplPVwibGFyZ2VcIiBzcGlubmluZz17dGhpcy5zdGF0ZS5sb2FkaW5nfT5cbiAgICAgICAgPFJvdyB0eXBlPVwiZmxleFwiIGp1c3RpZnk9e1wic3BhY2UtYmV0d2VlblwifT5cbiAgICAgICAgICB7dGhpcy5yZW5kZXJTaWRlSW5mbygpfVxuICAgICAgICAgIDxDb2wgc209ezE4fSB4cz17MjN9PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0aXRsZVwiPlxuICAgICAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJTUEVDSUFMIENMQVVTRVNcIikudGhhaShcIlNQRUNJQUwgQ0xBVVNFU1wiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxOQ29sbGFwc2UgZGVmYXVsdEFjdGl2ZUtleT17W1wiMVwiLCBcIjJcIl19PlxuICAgICAgICAgICAgICA8TlBhbmVsXG4gICAgICAgICAgICAgICAga2V5PVwiMVwiXG4gICAgICAgICAgICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcIlNQRUNJQUwgQ0xBVVNFU1wiKVxuICAgICAgICAgICAgICAgICAgLnRoYWkoXCJTUEVDSUFMIENMQVVTRVNcIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJDbGF1c2UoKX1cbiAgICAgICAgICAgICAgICB7Lyo8TkZvcm1JdGVtKi99XG4gICAgICAgICAgICAgICAgey8qICBmb3JtPXtmb3JtfSovfVxuICAgICAgICAgICAgICAgIHsvKiAgbW9kZWw9e21vZGVsfSovfVxuICAgICAgICAgICAgICAgIHsvKiAgcmVxdWlyZWQ9e3RydWV9Ki99XG4gICAgICAgICAgICAgICAgey8qICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwic3BlY2lhbENsYXVzZXNcIil9Ki99XG4gICAgICAgICAgICAgICAgey8qICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJDTEFVU0UgVEVYVFNcIikqL31cbiAgICAgICAgICAgICAgICB7LyogICAgLnRoYWkoXCJDTEFVU0UgVEVYVFNcIikqL31cbiAgICAgICAgICAgICAgICB7LyogICAgLmdldE1lc3NhZ2UoKX0qL31cbiAgICAgICAgICAgICAgICB7Lyo+Ki99XG4gICAgICAgICAgICAgICAgey8qICA8SW5wdXQuVGV4dEFyZWEqL31cbiAgICAgICAgICAgICAgICB7LyogICAgc3R5bGU9e3sgcmVzaXplOiBcIm5vbmVcIiB9fSovfVxuICAgICAgICAgICAgICAgIHsvKiAgICByb3dzPXs4fSovfVxuICAgICAgICAgICAgICAgIHsvKiAgICBhdXRvQ29tcGxldGU9XCJmYWxzZVwiKi99XG4gICAgICAgICAgICAgICAgey8qICAvPiovfVxuXG4gICAgICAgICAgICAgICAgey8qPC9ORm9ybUl0ZW0+Ki99XG4gICAgICAgICAgICAgIDwvTlBhbmVsPlxuICAgICAgICAgICAgPC9OQ29sbGFwc2U+XG4gICAgICAgICAgICB7dGhpcy5yZW5kZXJSaWdodEFjdGlvbnMoKX1cbiAgICAgICAgICA8L0NvbD5cbiAgICAgICAgPC9Sb3c+XG4gICAgICA8L1NwaW4+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgeyBCYXNpY0NsYXVzZSB9O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFMQTtBQVVBO0FBQ0E7QUFNQTs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUtBOzs7QUFHQTs7O0FBR0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQURBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUE4QkE7Ozs7QUFuR0E7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/basic-clause.tsx
