__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_component_view_item__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-component/view-item */ "./src/app/desk/component/view-item/index.tsx");
/* harmony import */ var _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk/claims/handing/details/SAIC/gpc/view/sections/consequence/components/common */ "./src/app/desk/claims/handing/details/SAIC/gpc/view/sections/consequence/components/common/index.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/ph/company-info-view.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var defaultLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};

var ViewItem = function ViewItem(props) {
  return Object(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_14__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__["default"])({}, props, {
    layout: props.layoutCol || defaultLayout
  }));
};

var CompanyInfoView =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(CompanyInfoView, _ModelWidget);

  function CompanyInfoView(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, CompanyInfoView);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(CompanyInfoView).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(CompanyInfoView, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_15__["default"].loadCodeTables(this, ["nationcode", "nationality"]);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "generatePropName",
    value: function generatePropName(propName) {
      var propNameFixed = this.props.propNameFixed;
      var propsName = propNameFixed || "ext.policyholder";
      if (!propName) return propsName;
      return "".concat(propsName, ".").concat(propName);
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();

      var _this$props = this.props,
          model = _this$props.model,
          type = _this$props.type,
          propNameFixed = _this$props.propNameFixed,
          layoutCol = _this$props.layoutCol,
          isMailingAddress = _this$props.isMailingAddress,
          isCedant = _this$props.isCedant,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$props, ["model", "type", "propNameFixed", "layoutCol", "isMailingAddress", "isCedant"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Name").thai("Name").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, this.generatePropName("name"))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Registration No.").thai("Registration No.").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, this.generatePropName("registrationNo"))), this.props.childDom || "", _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Email").thai("อีเมล์").my("အီးမေးလ်က").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, this.generatePropName("email"))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Office No.").thai("Office No.").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_15__["default"].getMasterTableItemText("nationcode", lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, this.generatePropName("officeTelNationCode")), this), " ", " ", lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, this.generatePropName("officeTel"))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Mobile No.").thai("Mobile No.").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 102
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_15__["default"].getMasterTableItemText("nationcode", lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, this.generatePropName("mobileNationCode")), this), " ", " ", lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, this.generatePropName("mobile"))), isCedant && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Country").thai("Country").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_15__["default"].getMasterTableItemText("nationality", lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, this.generatePropName("address.countryCode")), this)), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Address").thai("Address").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120
        },
        __self: this
      }, this.getValueFromModel(this.generatePropName("mailingAddress")) === "Y" && _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Same as location").thai("Same as location").getMessage(), this.getValueFromModel(this.generatePropName("mailingAddress")) !== "Y" && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_13__["ShowAddress"], {
        model: model,
        addressFixed: this.generatePropName("address"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 130
        },
        __self: this
      }))));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(CompanyInfoView.prototype), "initState", this).call(this), {
        codeTables: []
      });
    }
  }]);

  return CompanyInfoView;
}(_component__WEBPACK_IMPORTED_MODULE_11__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (CompanyInfoView);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BoL2NvbXBhbnktaW5mby12aWV3LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9waC9jb21wYW55LWluZm8tdmlldy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuXG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBMYW5ndWFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgU2hvd0FkZHJlc3MgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50XCI7XG5pbXBvcnQgVmlld0l0ZW0xIGZyb20gXCJAZGVzay1jb21wb25lbnQvdmlldy1pdGVtXCI7XG5pbXBvcnQgQ29tbW9uR3BjQ2xhaW0gZnJvbSBcIkBkZXNrL2NsYWltcy9oYW5kaW5nL2RldGFpbHMvU0FJQy9ncGMvdmlldy9zZWN0aW9ucy9jb25zZXF1ZW5jZS9jb21wb25lbnRzL2NvbW1vblwiO1xuXG5jb25zdCBkZWZhdWx0TGF5b3V0ID0ge1xuICBsYWJlbENvbDoge1xuICAgIHhzOiB7IHNwYW46IDggfSxcbiAgICBzbTogeyBzcGFuOiA2IH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxNiB9LFxuICAgIHNtOiB7IHNwYW46IDEzIH0sXG4gIH0sXG59O1xuXG5jb25zdCBWaWV3SXRlbSA9IChwcm9wczogYW55KSA9PiBWaWV3SXRlbTEoeyAuLi5wcm9wcywgbGF5b3V0OiBwcm9wcy5sYXlvdXRDb2wgfHwgZGVmYXVsdExheW91dCB9KTtcbnR5cGUgUGF5ZXJTdGF0ZSA9IHtcbiAgY29kZVRhYmxlczogYW55O1xufTtcblxuZXhwb3J0IHR5cGUgU3R5bGVkRElWID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHt9LCBuZXZlcj47XG5cbmV4cG9ydCB0eXBlIFBheWVyUGFnZUNvbXBvbmVudHMgPSB7XG4gIEJveENvbnRlbnQ6IFN0eWxlZERJVjtcbn07XG5leHBvcnQgdHlwZSBQYXllclByb3BzID0ge1xuICBtb2RlbDogYW55O1xuICBpc0NlZGFudD86IGJvb2xlYW47XG4gIHByb3BOYW1lRml4ZWQ/OiBhbnk7XG4gIGxheW91dENvbD86IGFueTtcbiAgY2hpbGREb20/OiBhbnk7XG4gIHR5cGU/OiBcIkVYVFJBLUlORk9cIiB8IHVuZGVmaW5lZDtcbiAgaXNNYWlsaW5nQWRkcmVzcz86IGJvb2xlYW47XG59ICYgTW9kZWxXaWRnZXRQcm9wcztcblxuY2xhc3MgQ29tcGFueUluZm9WaWV3PFAgZXh0ZW5kcyBQYXllclByb3BzLCBTIGV4dGVuZHMgUGF5ZXJTdGF0ZSwgQyBleHRlbmRzIFBheWVyUGFnZUNvbXBvbmVudHM+XG4gIGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzOiBQYXllclByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgQ29tbW9uR3BjQ2xhaW0ubG9hZENvZGVUYWJsZXModGhpcywgW1wibmF0aW9uY29kZVwiLCBcIm5hdGlvbmFsaXR5XCJdKTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICB9XG5cbiAgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZT86IHN0cmluZykge1xuICAgIGNvbnN0IHsgcHJvcE5hbWVGaXhlZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBwcm9wc05hbWUgPSBwcm9wTmFtZUZpeGVkIHx8IFwiZXh0LnBvbGljeWhvbGRlclwiO1xuICAgIGlmICghcHJvcE5hbWUpIHJldHVybiBwcm9wc05hbWU7XG4gICAgcmV0dXJuIGAke3Byb3BzTmFtZX0uJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCB7IG1vZGVsLCB0eXBlLCBwcm9wTmFtZUZpeGVkLCBsYXlvdXRDb2wsIGlzTWFpbGluZ0FkZHJlc3MsIGlzQ2VkYW50LCAuLi5yZXN0IH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3hDb250ZW50IHsuLi5yZXN0fT5cbiAgICAgICAgPD5cbiAgICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICAgIGxheW91dENvbD17bGF5b3V0Q29sfVxuICAgICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiTmFtZVwiKS50aGFpKFwiTmFtZVwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAge18uZ2V0KG1vZGVsLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJuYW1lXCIpKX1cbiAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgICAgbGF5b3V0Q29sPXtsYXlvdXRDb2x9XG4gICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJSZWdpc3RyYXRpb24gTm8uXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwiUmVnaXN0cmF0aW9uIE5vLlwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIHtfLmdldChtb2RlbCwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicmVnaXN0cmF0aW9uTm9cIikpfVxuICAgICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgICAge3RoaXMucHJvcHMuY2hpbGREb20gfHwgXCJcIn1cbiAgICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICAgIGxheW91dENvbD17bGF5b3V0Q29sfVxuICAgICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiRW1haWxcIilcbiAgICAgICAgICAgICAgLnRoYWkoXCLguK3guLXguYDguKHguKXguYxcIilcbiAgICAgICAgICAgICAgLm15KFwi4YCh4YCu4YC44YCZ4YCx4YC44YCc4YC64YCAXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAge18uZ2V0KG1vZGVsLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJlbWFpbFwiKSl9XG4gICAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICAgIGxheW91dENvbD17bGF5b3V0Q29sfVxuICAgICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiT2ZmaWNlIE5vLlwiKS50aGFpKFwiT2ZmaWNlIE5vLlwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAge0NvbW1vbkdwY0NsYWltLmdldE1hc3RlclRhYmxlSXRlbVRleHQoXCJuYXRpb25jb2RlXCIsXG4gICAgICAgICAgICAgIF8uZ2V0KG1vZGVsLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJvZmZpY2VUZWxOYXRpb25Db2RlXCIpKSwgdGhpcyl9IHtcIiBcIn1cbiAgICAgICAgICAgIHtfLmdldChtb2RlbCwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwib2ZmaWNlVGVsXCIpKX1cbiAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgICAgbGF5b3V0Q29sPXtsYXlvdXRDb2x9XG4gICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJNb2JpbGUgTm8uXCIpLnRoYWkoXCJNb2JpbGUgTm8uXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICB7Q29tbW9uR3BjQ2xhaW0uZ2V0TWFzdGVyVGFibGVJdGVtVGV4dChcIm5hdGlvbmNvZGVcIixcbiAgICAgICAgICAgICAgXy5nZXQobW9kZWwsIHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIm1vYmlsZU5hdGlvbkNvZGVcIikpLCB0aGlzKX0ge1wiIFwifVxuICAgICAgICAgICAge18uZ2V0KG1vZGVsLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJtb2JpbGVcIikpfVxuICAgICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgICAge1xuICAgICAgICAgICAgaXNDZWRhbnQgJiZcbiAgICAgICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiQ291bnRyeVwiKS50aGFpKFwiQ291bnRyeVwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIHtDb21tb25HcGNDbGFpbS5nZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0KFwibmF0aW9uYWxpdHlcIixcbiAgICAgICAgICAgICAgICBfLmdldChtb2RlbCwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiYWRkcmVzcy5jb3VudHJ5Q29kZVwiKSksIHRoaXMpfVxuICAgICAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgICB9XG4gICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIkFkZHJlc3NcIilcbiAgICAgICAgICAgICAgLnRoYWkoXCJBZGRyZXNzXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgeyh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIm1haWxpbmdBZGRyZXNzXCIpKSA9PT0gXCJZXCIpXG4gICAgICAgICAgICAmJiBMYW5ndWFnZS5lbihcIlNhbWUgYXMgbG9jYXRpb25cIikudGhhaShcIlNhbWUgYXMgbG9jYXRpb25cIikuZ2V0TWVzc2FnZSgpfVxuXG4gICAgICAgICAgICB7KHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibWFpbGluZ0FkZHJlc3NcIikpICE9PSBcIllcIilcbiAgICAgICAgICAgICYmIDxTaG93QWRkcmVzcyBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWRkcmVzc0ZpeGVkPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzXCIpfS8+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgPC8+XG4gICAgICA8L0MuQm94Q29udGVudD5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBCb3hDb250ZW50OiBTdHlsZWQuZGl2YFxuICAgICAgICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICBjb2RlVGFibGVzOiBbXSxcbiAgICB9KSBhcyBTO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IENvbXBhbnlJbmZvVmlldztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQW1CQTs7Ozs7QUFHQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7OztBQUdBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7O0FBMUdBO0FBQ0E7QUE0R0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/ph/company-info-view.tsx
