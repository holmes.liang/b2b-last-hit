/* WEBPACK VAR INJECTION */(function(global) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule setDraftEditorSelection
 * @format
 * 
 */


var DraftJsDebugLogging = __webpack_require__(/*! ./DraftJsDebugLogging */ "./node_modules/draft-js/lib/DraftJsDebugLogging.js");

var containsNode = __webpack_require__(/*! fbjs/lib/containsNode */ "./node_modules/fbjs/lib/containsNode.js");

var getActiveElement = __webpack_require__(/*! fbjs/lib/getActiveElement */ "./node_modules/fbjs/lib/getActiveElement.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

function getAnonymizedDOM(node, getNodeLabels) {
  if (!node) {
    return '[empty]';
  }

  var anonymized = anonymizeTextWithin(node, getNodeLabels);

  if (anonymized.nodeType === Node.TEXT_NODE) {
    return anonymized.textContent;
  }

  !(anonymized instanceof Element) ?  true ? invariant(false, 'Node must be an Element if it is not a text node.') : undefined : void 0;
  return anonymized.outerHTML;
}

function anonymizeTextWithin(node, getNodeLabels) {
  var labels = getNodeLabels !== undefined ? getNodeLabels(node) : [];

  if (node.nodeType === Node.TEXT_NODE) {
    var length = node.textContent.length;
    return document.createTextNode('[text ' + length + (labels.length ? ' | ' + labels.join(', ') : '') + ']');
  }

  var clone = node.cloneNode();

  if (clone.nodeType === 1 && labels.length) {
    clone.setAttribute('data-labels', labels.join(', '));
  }

  var childNodes = node.childNodes;

  for (var ii = 0; ii < childNodes.length; ii++) {
    clone.appendChild(anonymizeTextWithin(childNodes[ii], getNodeLabels));
  }

  return clone;
}

function getAnonymizedEditorDOM(node, getNodeLabels) {
  // grabbing the DOM content of the Draft editor
  var currentNode = node;

  while (currentNode) {
    if (currentNode instanceof Element && currentNode.hasAttribute('contenteditable')) {
      // found the Draft editor container
      return getAnonymizedDOM(currentNode, getNodeLabels);
    } else {
      currentNode = currentNode.parentNode;
    }
  }

  return 'Could not find contentEditable parent of node';
}

function getNodeLength(node) {
  return node.nodeValue === null ? node.childNodes.length : node.nodeValue.length;
}
/**
 * In modern non-IE browsers, we can support both forward and backward
 * selections.
 *
 * Note: IE10+ supports the Selection object, but it does not support
 * the `extend` method, which means that even in modern IE, it's not possible
 * to programatically create a backward selection. Thus, for all IE
 * versions, we use the old IE API to create our selections.
 */


function setDraftEditorSelection(selectionState, node, blockKey, nodeStart, nodeEnd) {
  // It's possible that the editor has been removed from the DOM but
  // our selection code doesn't know it yet. Forcing selection in
  // this case may lead to errors, so just bail now.
  if (!containsNode(document.documentElement, node)) {
    return;
  }

  var selection = global.getSelection();
  var anchorKey = selectionState.getAnchorKey();
  var anchorOffset = selectionState.getAnchorOffset();
  var focusKey = selectionState.getFocusKey();
  var focusOffset = selectionState.getFocusOffset();
  var isBackward = selectionState.getIsBackward(); // IE doesn't support backward selection. Swap key/offset pairs.

  if (!selection.extend && isBackward) {
    var tempKey = anchorKey;
    var tempOffset = anchorOffset;
    anchorKey = focusKey;
    anchorOffset = focusOffset;
    focusKey = tempKey;
    focusOffset = tempOffset;
    isBackward = false;
  }

  var hasAnchor = anchorKey === blockKey && nodeStart <= anchorOffset && nodeEnd >= anchorOffset;
  var hasFocus = focusKey === blockKey && nodeStart <= focusOffset && nodeEnd >= focusOffset; // If the selection is entirely bound within this node, set the selection
  // and be done.

  if (hasAnchor && hasFocus) {
    selection.removeAllRanges();
    addPointToSelection(selection, node, anchorOffset - nodeStart, selectionState);
    addFocusToSelection(selection, node, focusOffset - nodeStart, selectionState);
    return;
  }

  if (!isBackward) {
    // If the anchor is within this node, set the range start.
    if (hasAnchor) {
      selection.removeAllRanges();
      addPointToSelection(selection, node, anchorOffset - nodeStart, selectionState);
    } // If the focus is within this node, we can assume that we have
    // already set the appropriate start range on the selection, and
    // can simply extend the selection.


    if (hasFocus) {
      addFocusToSelection(selection, node, focusOffset - nodeStart, selectionState);
    }
  } else {
    // If this node has the focus, set the selection range to be a
    // collapsed range beginning here. Later, when we encounter the anchor,
    // we'll use this information to extend the selection.
    if (hasFocus) {
      selection.removeAllRanges();
      addPointToSelection(selection, node, focusOffset - nodeStart, selectionState);
    } // If this node has the anchor, we may assume that the correct
    // focus information is already stored on the selection object.
    // We keep track of it, reset the selection range, and extend it
    // back to the focus point.


    if (hasAnchor) {
      var storedFocusNode = selection.focusNode;
      var storedFocusOffset = selection.focusOffset;
      selection.removeAllRanges();
      addPointToSelection(selection, node, anchorOffset - nodeStart, selectionState);
      addFocusToSelection(selection, storedFocusNode, storedFocusOffset, selectionState);
    }
  }
}
/**
 * Extend selection towards focus point.
 */


function addFocusToSelection(selection, node, offset, selectionState) {
  var activeElement = getActiveElement();

  if (selection.extend && containsNode(activeElement, node)) {
    // If `extend` is called while another element has focus, an error is
    // thrown. We therefore disable `extend` if the active element is somewhere
    // other than the node we are selecting. This should only occur in Firefox,
    // since it is the only browser to support multiple selections.
    // See https://bugzilla.mozilla.org/show_bug.cgi?id=921444.
    // logging to catch bug that is being reported in t16250795
    if (offset > getNodeLength(node)) {
      // the call to 'selection.extend' is about to throw
      DraftJsDebugLogging.logSelectionStateFailure({
        anonymizedDom: getAnonymizedEditorDOM(node),
        extraParams: JSON.stringify({
          offset: offset
        }),
        selectionState: JSON.stringify(selectionState.toJS())
      });
    } // logging to catch bug that is being reported in t18110632


    var nodeWasFocus = node === selection.focusNode;

    try {
      selection.extend(node, offset);
    } catch (e) {
      DraftJsDebugLogging.logSelectionStateFailure({
        anonymizedDom: getAnonymizedEditorDOM(node, function (n) {
          var labels = [];

          if (n === activeElement) {
            labels.push('active element');
          }

          if (n === selection.anchorNode) {
            labels.push('selection anchor node');
          }

          if (n === selection.focusNode) {
            labels.push('selection focus node');
          }

          return labels;
        }),
        extraParams: JSON.stringify({
          activeElementName: activeElement ? activeElement.nodeName : null,
          nodeIsFocus: node === selection.focusNode,
          nodeWasFocus: nodeWasFocus,
          selectionRangeCount: selection.rangeCount,
          selectionAnchorNodeName: selection.anchorNode ? selection.anchorNode.nodeName : null,
          selectionAnchorOffset: selection.anchorOffset,
          selectionFocusNodeName: selection.focusNode ? selection.focusNode.nodeName : null,
          selectionFocusOffset: selection.focusOffset,
          message: e ? '' + e : null,
          offset: offset
        }, null, 2),
        selectionState: JSON.stringify(selectionState.toJS(), null, 2)
      }); // allow the error to be thrown -
      // better than continuing in a broken state

      throw e;
    }
  } else {
    // IE doesn't support extend. This will mean no backward selection.
    // Extract the existing selection range and add focus to it.
    // Additionally, clone the selection range. IE11 throws an
    // InvalidStateError when attempting to access selection properties
    // after the range is detached.
    var range = selection.getRangeAt(0);
    range.setEnd(node, offset);
    selection.addRange(range.cloneRange());
  }
}

function addPointToSelection(selection, node, offset, selectionState) {
  var range = document.createRange(); // logging to catch bug that is being reported in t16250795

  if (offset > getNodeLength(node)) {
    // in this case we know that the call to 'range.setStart' is about to throw
    DraftJsDebugLogging.logSelectionStateFailure({
      anonymizedDom: getAnonymizedEditorDOM(node),
      extraParams: JSON.stringify({
        offset: offset
      }),
      selectionState: JSON.stringify(selectionState.toJS())
    });
  }

  range.setStart(node, offset);
  selection.addRange(range);
}

module.exports = setDraftEditorSelection;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL3NldERyYWZ0RWRpdG9yU2VsZWN0aW9uLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL3NldERyYWZ0RWRpdG9yU2VsZWN0aW9uLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgc2V0RHJhZnRFZGl0b3JTZWxlY3Rpb25cbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0SnNEZWJ1Z0xvZ2dpbmcgPSByZXF1aXJlKCcuL0RyYWZ0SnNEZWJ1Z0xvZ2dpbmcnKTtcblxudmFyIGNvbnRhaW5zTm9kZSA9IHJlcXVpcmUoJ2ZianMvbGliL2NvbnRhaW5zTm9kZScpO1xudmFyIGdldEFjdGl2ZUVsZW1lbnQgPSByZXF1aXJlKCdmYmpzL2xpYi9nZXRBY3RpdmVFbGVtZW50Jyk7XG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG5cbmZ1bmN0aW9uIGdldEFub255bWl6ZWRET00obm9kZSwgZ2V0Tm9kZUxhYmVscykge1xuICBpZiAoIW5vZGUpIHtcbiAgICByZXR1cm4gJ1tlbXB0eV0nO1xuICB9XG5cbiAgdmFyIGFub255bWl6ZWQgPSBhbm9ueW1pemVUZXh0V2l0aGluKG5vZGUsIGdldE5vZGVMYWJlbHMpO1xuICBpZiAoYW5vbnltaXplZC5ub2RlVHlwZSA9PT0gTm9kZS5URVhUX05PREUpIHtcbiAgICByZXR1cm4gYW5vbnltaXplZC50ZXh0Q29udGVudDtcbiAgfVxuXG4gICEoYW5vbnltaXplZCBpbnN0YW5jZW9mIEVsZW1lbnQpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ05vZGUgbXVzdCBiZSBhbiBFbGVtZW50IGlmIGl0IGlzIG5vdCBhIHRleHQgbm9kZS4nKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG4gIHJldHVybiBhbm9ueW1pemVkLm91dGVySFRNTDtcbn1cblxuZnVuY3Rpb24gYW5vbnltaXplVGV4dFdpdGhpbihub2RlLCBnZXROb2RlTGFiZWxzKSB7XG4gIHZhciBsYWJlbHMgPSBnZXROb2RlTGFiZWxzICE9PSB1bmRlZmluZWQgPyBnZXROb2RlTGFiZWxzKG5vZGUpIDogW107XG5cbiAgaWYgKG5vZGUubm9kZVR5cGUgPT09IE5vZGUuVEVYVF9OT0RFKSB7XG4gICAgdmFyIGxlbmd0aCA9IG5vZGUudGV4dENvbnRlbnQubGVuZ3RoO1xuICAgIHJldHVybiBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZSgnW3RleHQgJyArIGxlbmd0aCArIChsYWJlbHMubGVuZ3RoID8gJyB8ICcgKyBsYWJlbHMuam9pbignLCAnKSA6ICcnKSArICddJyk7XG4gIH1cblxuICB2YXIgY2xvbmUgPSBub2RlLmNsb25lTm9kZSgpO1xuICBpZiAoY2xvbmUubm9kZVR5cGUgPT09IDEgJiYgbGFiZWxzLmxlbmd0aCkge1xuICAgIGNsb25lLnNldEF0dHJpYnV0ZSgnZGF0YS1sYWJlbHMnLCBsYWJlbHMuam9pbignLCAnKSk7XG4gIH1cbiAgdmFyIGNoaWxkTm9kZXMgPSBub2RlLmNoaWxkTm9kZXM7XG4gIGZvciAodmFyIGlpID0gMDsgaWkgPCBjaGlsZE5vZGVzLmxlbmd0aDsgaWkrKykge1xuICAgIGNsb25lLmFwcGVuZENoaWxkKGFub255bWl6ZVRleHRXaXRoaW4oY2hpbGROb2Rlc1tpaV0sIGdldE5vZGVMYWJlbHMpKTtcbiAgfVxuXG4gIHJldHVybiBjbG9uZTtcbn1cblxuZnVuY3Rpb24gZ2V0QW5vbnltaXplZEVkaXRvckRPTShub2RlLCBnZXROb2RlTGFiZWxzKSB7XG4gIC8vIGdyYWJiaW5nIHRoZSBET00gY29udGVudCBvZiB0aGUgRHJhZnQgZWRpdG9yXG4gIHZhciBjdXJyZW50Tm9kZSA9IG5vZGU7XG4gIHdoaWxlIChjdXJyZW50Tm9kZSkge1xuICAgIGlmIChjdXJyZW50Tm9kZSBpbnN0YW5jZW9mIEVsZW1lbnQgJiYgY3VycmVudE5vZGUuaGFzQXR0cmlidXRlKCdjb250ZW50ZWRpdGFibGUnKSkge1xuICAgICAgLy8gZm91bmQgdGhlIERyYWZ0IGVkaXRvciBjb250YWluZXJcbiAgICAgIHJldHVybiBnZXRBbm9ueW1pemVkRE9NKGN1cnJlbnROb2RlLCBnZXROb2RlTGFiZWxzKTtcbiAgICB9IGVsc2Uge1xuICAgICAgY3VycmVudE5vZGUgPSBjdXJyZW50Tm9kZS5wYXJlbnROb2RlO1xuICAgIH1cbiAgfVxuICByZXR1cm4gJ0NvdWxkIG5vdCBmaW5kIGNvbnRlbnRFZGl0YWJsZSBwYXJlbnQgb2Ygbm9kZSc7XG59XG5cbmZ1bmN0aW9uIGdldE5vZGVMZW5ndGgobm9kZSkge1xuICByZXR1cm4gbm9kZS5ub2RlVmFsdWUgPT09IG51bGwgPyBub2RlLmNoaWxkTm9kZXMubGVuZ3RoIDogbm9kZS5ub2RlVmFsdWUubGVuZ3RoO1xufVxuXG4vKipcbiAqIEluIG1vZGVybiBub24tSUUgYnJvd3NlcnMsIHdlIGNhbiBzdXBwb3J0IGJvdGggZm9yd2FyZCBhbmQgYmFja3dhcmRcbiAqIHNlbGVjdGlvbnMuXG4gKlxuICogTm90ZTogSUUxMCsgc3VwcG9ydHMgdGhlIFNlbGVjdGlvbiBvYmplY3QsIGJ1dCBpdCBkb2VzIG5vdCBzdXBwb3J0XG4gKiB0aGUgYGV4dGVuZGAgbWV0aG9kLCB3aGljaCBtZWFucyB0aGF0IGV2ZW4gaW4gbW9kZXJuIElFLCBpdCdzIG5vdCBwb3NzaWJsZVxuICogdG8gcHJvZ3JhbWF0aWNhbGx5IGNyZWF0ZSBhIGJhY2t3YXJkIHNlbGVjdGlvbi4gVGh1cywgZm9yIGFsbCBJRVxuICogdmVyc2lvbnMsIHdlIHVzZSB0aGUgb2xkIElFIEFQSSB0byBjcmVhdGUgb3VyIHNlbGVjdGlvbnMuXG4gKi9cbmZ1bmN0aW9uIHNldERyYWZ0RWRpdG9yU2VsZWN0aW9uKHNlbGVjdGlvblN0YXRlLCBub2RlLCBibG9ja0tleSwgbm9kZVN0YXJ0LCBub2RlRW5kKSB7XG4gIC8vIEl0J3MgcG9zc2libGUgdGhhdCB0aGUgZWRpdG9yIGhhcyBiZWVuIHJlbW92ZWQgZnJvbSB0aGUgRE9NIGJ1dFxuICAvLyBvdXIgc2VsZWN0aW9uIGNvZGUgZG9lc24ndCBrbm93IGl0IHlldC4gRm9yY2luZyBzZWxlY3Rpb24gaW5cbiAgLy8gdGhpcyBjYXNlIG1heSBsZWFkIHRvIGVycm9ycywgc28ganVzdCBiYWlsIG5vdy5cbiAgaWYgKCFjb250YWluc05vZGUoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCBub2RlKSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBzZWxlY3Rpb24gPSBnbG9iYWwuZ2V0U2VsZWN0aW9uKCk7XG4gIHZhciBhbmNob3JLZXkgPSBzZWxlY3Rpb25TdGF0ZS5nZXRBbmNob3JLZXkoKTtcbiAgdmFyIGFuY2hvck9mZnNldCA9IHNlbGVjdGlvblN0YXRlLmdldEFuY2hvck9mZnNldCgpO1xuICB2YXIgZm9jdXNLZXkgPSBzZWxlY3Rpb25TdGF0ZS5nZXRGb2N1c0tleSgpO1xuICB2YXIgZm9jdXNPZmZzZXQgPSBzZWxlY3Rpb25TdGF0ZS5nZXRGb2N1c09mZnNldCgpO1xuICB2YXIgaXNCYWNrd2FyZCA9IHNlbGVjdGlvblN0YXRlLmdldElzQmFja3dhcmQoKTtcblxuICAvLyBJRSBkb2Vzbid0IHN1cHBvcnQgYmFja3dhcmQgc2VsZWN0aW9uLiBTd2FwIGtleS9vZmZzZXQgcGFpcnMuXG4gIGlmICghc2VsZWN0aW9uLmV4dGVuZCAmJiBpc0JhY2t3YXJkKSB7XG4gICAgdmFyIHRlbXBLZXkgPSBhbmNob3JLZXk7XG4gICAgdmFyIHRlbXBPZmZzZXQgPSBhbmNob3JPZmZzZXQ7XG4gICAgYW5jaG9yS2V5ID0gZm9jdXNLZXk7XG4gICAgYW5jaG9yT2Zmc2V0ID0gZm9jdXNPZmZzZXQ7XG4gICAgZm9jdXNLZXkgPSB0ZW1wS2V5O1xuICAgIGZvY3VzT2Zmc2V0ID0gdGVtcE9mZnNldDtcbiAgICBpc0JhY2t3YXJkID0gZmFsc2U7XG4gIH1cblxuICB2YXIgaGFzQW5jaG9yID0gYW5jaG9yS2V5ID09PSBibG9ja0tleSAmJiBub2RlU3RhcnQgPD0gYW5jaG9yT2Zmc2V0ICYmIG5vZGVFbmQgPj0gYW5jaG9yT2Zmc2V0O1xuXG4gIHZhciBoYXNGb2N1cyA9IGZvY3VzS2V5ID09PSBibG9ja0tleSAmJiBub2RlU3RhcnQgPD0gZm9jdXNPZmZzZXQgJiYgbm9kZUVuZCA+PSBmb2N1c09mZnNldDtcblxuICAvLyBJZiB0aGUgc2VsZWN0aW9uIGlzIGVudGlyZWx5IGJvdW5kIHdpdGhpbiB0aGlzIG5vZGUsIHNldCB0aGUgc2VsZWN0aW9uXG4gIC8vIGFuZCBiZSBkb25lLlxuICBpZiAoaGFzQW5jaG9yICYmIGhhc0ZvY3VzKSB7XG4gICAgc2VsZWN0aW9uLnJlbW92ZUFsbFJhbmdlcygpO1xuICAgIGFkZFBvaW50VG9TZWxlY3Rpb24oc2VsZWN0aW9uLCBub2RlLCBhbmNob3JPZmZzZXQgLSBub2RlU3RhcnQsIHNlbGVjdGlvblN0YXRlKTtcbiAgICBhZGRGb2N1c1RvU2VsZWN0aW9uKHNlbGVjdGlvbiwgbm9kZSwgZm9jdXNPZmZzZXQgLSBub2RlU3RhcnQsIHNlbGVjdGlvblN0YXRlKTtcbiAgICByZXR1cm47XG4gIH1cblxuICBpZiAoIWlzQmFja3dhcmQpIHtcbiAgICAvLyBJZiB0aGUgYW5jaG9yIGlzIHdpdGhpbiB0aGlzIG5vZGUsIHNldCB0aGUgcmFuZ2Ugc3RhcnQuXG4gICAgaWYgKGhhc0FuY2hvcikge1xuICAgICAgc2VsZWN0aW9uLnJlbW92ZUFsbFJhbmdlcygpO1xuICAgICAgYWRkUG9pbnRUb1NlbGVjdGlvbihzZWxlY3Rpb24sIG5vZGUsIGFuY2hvck9mZnNldCAtIG5vZGVTdGFydCwgc2VsZWN0aW9uU3RhdGUpO1xuICAgIH1cblxuICAgIC8vIElmIHRoZSBmb2N1cyBpcyB3aXRoaW4gdGhpcyBub2RlLCB3ZSBjYW4gYXNzdW1lIHRoYXQgd2UgaGF2ZVxuICAgIC8vIGFscmVhZHkgc2V0IHRoZSBhcHByb3ByaWF0ZSBzdGFydCByYW5nZSBvbiB0aGUgc2VsZWN0aW9uLCBhbmRcbiAgICAvLyBjYW4gc2ltcGx5IGV4dGVuZCB0aGUgc2VsZWN0aW9uLlxuICAgIGlmIChoYXNGb2N1cykge1xuICAgICAgYWRkRm9jdXNUb1NlbGVjdGlvbihzZWxlY3Rpb24sIG5vZGUsIGZvY3VzT2Zmc2V0IC0gbm9kZVN0YXJ0LCBzZWxlY3Rpb25TdGF0ZSk7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIC8vIElmIHRoaXMgbm9kZSBoYXMgdGhlIGZvY3VzLCBzZXQgdGhlIHNlbGVjdGlvbiByYW5nZSB0byBiZSBhXG4gICAgLy8gY29sbGFwc2VkIHJhbmdlIGJlZ2lubmluZyBoZXJlLiBMYXRlciwgd2hlbiB3ZSBlbmNvdW50ZXIgdGhlIGFuY2hvcixcbiAgICAvLyB3ZSdsbCB1c2UgdGhpcyBpbmZvcm1hdGlvbiB0byBleHRlbmQgdGhlIHNlbGVjdGlvbi5cbiAgICBpZiAoaGFzRm9jdXMpIHtcbiAgICAgIHNlbGVjdGlvbi5yZW1vdmVBbGxSYW5nZXMoKTtcbiAgICAgIGFkZFBvaW50VG9TZWxlY3Rpb24oc2VsZWN0aW9uLCBub2RlLCBmb2N1c09mZnNldCAtIG5vZGVTdGFydCwgc2VsZWN0aW9uU3RhdGUpO1xuICAgIH1cblxuICAgIC8vIElmIHRoaXMgbm9kZSBoYXMgdGhlIGFuY2hvciwgd2UgbWF5IGFzc3VtZSB0aGF0IHRoZSBjb3JyZWN0XG4gICAgLy8gZm9jdXMgaW5mb3JtYXRpb24gaXMgYWxyZWFkeSBzdG9yZWQgb24gdGhlIHNlbGVjdGlvbiBvYmplY3QuXG4gICAgLy8gV2Uga2VlcCB0cmFjayBvZiBpdCwgcmVzZXQgdGhlIHNlbGVjdGlvbiByYW5nZSwgYW5kIGV4dGVuZCBpdFxuICAgIC8vIGJhY2sgdG8gdGhlIGZvY3VzIHBvaW50LlxuICAgIGlmIChoYXNBbmNob3IpIHtcbiAgICAgIHZhciBzdG9yZWRGb2N1c05vZGUgPSBzZWxlY3Rpb24uZm9jdXNOb2RlO1xuICAgICAgdmFyIHN0b3JlZEZvY3VzT2Zmc2V0ID0gc2VsZWN0aW9uLmZvY3VzT2Zmc2V0O1xuXG4gICAgICBzZWxlY3Rpb24ucmVtb3ZlQWxsUmFuZ2VzKCk7XG4gICAgICBhZGRQb2ludFRvU2VsZWN0aW9uKHNlbGVjdGlvbiwgbm9kZSwgYW5jaG9yT2Zmc2V0IC0gbm9kZVN0YXJ0LCBzZWxlY3Rpb25TdGF0ZSk7XG4gICAgICBhZGRGb2N1c1RvU2VsZWN0aW9uKHNlbGVjdGlvbiwgc3RvcmVkRm9jdXNOb2RlLCBzdG9yZWRGb2N1c09mZnNldCwgc2VsZWN0aW9uU3RhdGUpO1xuICAgIH1cbiAgfVxufVxuXG4vKipcbiAqIEV4dGVuZCBzZWxlY3Rpb24gdG93YXJkcyBmb2N1cyBwb2ludC5cbiAqL1xuZnVuY3Rpb24gYWRkRm9jdXNUb1NlbGVjdGlvbihzZWxlY3Rpb24sIG5vZGUsIG9mZnNldCwgc2VsZWN0aW9uU3RhdGUpIHtcbiAgdmFyIGFjdGl2ZUVsZW1lbnQgPSBnZXRBY3RpdmVFbGVtZW50KCk7XG4gIGlmIChzZWxlY3Rpb24uZXh0ZW5kICYmIGNvbnRhaW5zTm9kZShhY3RpdmVFbGVtZW50LCBub2RlKSkge1xuICAgIC8vIElmIGBleHRlbmRgIGlzIGNhbGxlZCB3aGlsZSBhbm90aGVyIGVsZW1lbnQgaGFzIGZvY3VzLCBhbiBlcnJvciBpc1xuICAgIC8vIHRocm93bi4gV2UgdGhlcmVmb3JlIGRpc2FibGUgYGV4dGVuZGAgaWYgdGhlIGFjdGl2ZSBlbGVtZW50IGlzIHNvbWV3aGVyZVxuICAgIC8vIG90aGVyIHRoYW4gdGhlIG5vZGUgd2UgYXJlIHNlbGVjdGluZy4gVGhpcyBzaG91bGQgb25seSBvY2N1ciBpbiBGaXJlZm94LFxuICAgIC8vIHNpbmNlIGl0IGlzIHRoZSBvbmx5IGJyb3dzZXIgdG8gc3VwcG9ydCBtdWx0aXBsZSBzZWxlY3Rpb25zLlxuICAgIC8vIFNlZSBodHRwczovL2J1Z3ppbGxhLm1vemlsbGEub3JnL3Nob3dfYnVnLmNnaT9pZD05MjE0NDQuXG5cbiAgICAvLyBsb2dnaW5nIHRvIGNhdGNoIGJ1ZyB0aGF0IGlzIGJlaW5nIHJlcG9ydGVkIGluIHQxNjI1MDc5NVxuICAgIGlmIChvZmZzZXQgPiBnZXROb2RlTGVuZ3RoKG5vZGUpKSB7XG4gICAgICAvLyB0aGUgY2FsbCB0byAnc2VsZWN0aW9uLmV4dGVuZCcgaXMgYWJvdXQgdG8gdGhyb3dcbiAgICAgIERyYWZ0SnNEZWJ1Z0xvZ2dpbmcubG9nU2VsZWN0aW9uU3RhdGVGYWlsdXJlKHtcbiAgICAgICAgYW5vbnltaXplZERvbTogZ2V0QW5vbnltaXplZEVkaXRvckRPTShub2RlKSxcbiAgICAgICAgZXh0cmFQYXJhbXM6IEpTT04uc3RyaW5naWZ5KHsgb2Zmc2V0OiBvZmZzZXQgfSksXG4gICAgICAgIHNlbGVjdGlvblN0YXRlOiBKU09OLnN0cmluZ2lmeShzZWxlY3Rpb25TdGF0ZS50b0pTKCkpXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBsb2dnaW5nIHRvIGNhdGNoIGJ1ZyB0aGF0IGlzIGJlaW5nIHJlcG9ydGVkIGluIHQxODExMDYzMlxuICAgIHZhciBub2RlV2FzRm9jdXMgPSBub2RlID09PSBzZWxlY3Rpb24uZm9jdXNOb2RlO1xuICAgIHRyeSB7XG4gICAgICBzZWxlY3Rpb24uZXh0ZW5kKG5vZGUsIG9mZnNldCk7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgRHJhZnRKc0RlYnVnTG9nZ2luZy5sb2dTZWxlY3Rpb25TdGF0ZUZhaWx1cmUoe1xuICAgICAgICBhbm9ueW1pemVkRG9tOiBnZXRBbm9ueW1pemVkRWRpdG9yRE9NKG5vZGUsIGZ1bmN0aW9uIChuKSB7XG4gICAgICAgICAgdmFyIGxhYmVscyA9IFtdO1xuICAgICAgICAgIGlmIChuID09PSBhY3RpdmVFbGVtZW50KSB7XG4gICAgICAgICAgICBsYWJlbHMucHVzaCgnYWN0aXZlIGVsZW1lbnQnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKG4gPT09IHNlbGVjdGlvbi5hbmNob3JOb2RlKSB7XG4gICAgICAgICAgICBsYWJlbHMucHVzaCgnc2VsZWN0aW9uIGFuY2hvciBub2RlJyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChuID09PSBzZWxlY3Rpb24uZm9jdXNOb2RlKSB7XG4gICAgICAgICAgICBsYWJlbHMucHVzaCgnc2VsZWN0aW9uIGZvY3VzIG5vZGUnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGxhYmVscztcbiAgICAgICAgfSksXG4gICAgICAgIGV4dHJhUGFyYW1zOiBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgYWN0aXZlRWxlbWVudE5hbWU6IGFjdGl2ZUVsZW1lbnQgPyBhY3RpdmVFbGVtZW50Lm5vZGVOYW1lIDogbnVsbCxcbiAgICAgICAgICBub2RlSXNGb2N1czogbm9kZSA9PT0gc2VsZWN0aW9uLmZvY3VzTm9kZSxcbiAgICAgICAgICBub2RlV2FzRm9jdXM6IG5vZGVXYXNGb2N1cyxcbiAgICAgICAgICBzZWxlY3Rpb25SYW5nZUNvdW50OiBzZWxlY3Rpb24ucmFuZ2VDb3VudCxcbiAgICAgICAgICBzZWxlY3Rpb25BbmNob3JOb2RlTmFtZTogc2VsZWN0aW9uLmFuY2hvck5vZGUgPyBzZWxlY3Rpb24uYW5jaG9yTm9kZS5ub2RlTmFtZSA6IG51bGwsXG4gICAgICAgICAgc2VsZWN0aW9uQW5jaG9yT2Zmc2V0OiBzZWxlY3Rpb24uYW5jaG9yT2Zmc2V0LFxuICAgICAgICAgIHNlbGVjdGlvbkZvY3VzTm9kZU5hbWU6IHNlbGVjdGlvbi5mb2N1c05vZGUgPyBzZWxlY3Rpb24uZm9jdXNOb2RlLm5vZGVOYW1lIDogbnVsbCxcbiAgICAgICAgICBzZWxlY3Rpb25Gb2N1c09mZnNldDogc2VsZWN0aW9uLmZvY3VzT2Zmc2V0LFxuICAgICAgICAgIG1lc3NhZ2U6IGUgPyAnJyArIGUgOiBudWxsLFxuICAgICAgICAgIG9mZnNldDogb2Zmc2V0XG4gICAgICAgIH0sIG51bGwsIDIpLFxuICAgICAgICBzZWxlY3Rpb25TdGF0ZTogSlNPTi5zdHJpbmdpZnkoc2VsZWN0aW9uU3RhdGUudG9KUygpLCBudWxsLCAyKVxuICAgICAgfSk7XG4gICAgICAvLyBhbGxvdyB0aGUgZXJyb3IgdG8gYmUgdGhyb3duIC1cbiAgICAgIC8vIGJldHRlciB0aGFuIGNvbnRpbnVpbmcgaW4gYSBicm9rZW4gc3RhdGVcbiAgICAgIHRocm93IGU7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIC8vIElFIGRvZXNuJ3Qgc3VwcG9ydCBleHRlbmQuIFRoaXMgd2lsbCBtZWFuIG5vIGJhY2t3YXJkIHNlbGVjdGlvbi5cbiAgICAvLyBFeHRyYWN0IHRoZSBleGlzdGluZyBzZWxlY3Rpb24gcmFuZ2UgYW5kIGFkZCBmb2N1cyB0byBpdC5cbiAgICAvLyBBZGRpdGlvbmFsbHksIGNsb25lIHRoZSBzZWxlY3Rpb24gcmFuZ2UuIElFMTEgdGhyb3dzIGFuXG4gICAgLy8gSW52YWxpZFN0YXRlRXJyb3Igd2hlbiBhdHRlbXB0aW5nIHRvIGFjY2VzcyBzZWxlY3Rpb24gcHJvcGVydGllc1xuICAgIC8vIGFmdGVyIHRoZSByYW5nZSBpcyBkZXRhY2hlZC5cbiAgICB2YXIgcmFuZ2UgPSBzZWxlY3Rpb24uZ2V0UmFuZ2VBdCgwKTtcbiAgICByYW5nZS5zZXRFbmQobm9kZSwgb2Zmc2V0KTtcbiAgICBzZWxlY3Rpb24uYWRkUmFuZ2UocmFuZ2UuY2xvbmVSYW5nZSgpKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBhZGRQb2ludFRvU2VsZWN0aW9uKHNlbGVjdGlvbiwgbm9kZSwgb2Zmc2V0LCBzZWxlY3Rpb25TdGF0ZSkge1xuICB2YXIgcmFuZ2UgPSBkb2N1bWVudC5jcmVhdGVSYW5nZSgpO1xuICAvLyBsb2dnaW5nIHRvIGNhdGNoIGJ1ZyB0aGF0IGlzIGJlaW5nIHJlcG9ydGVkIGluIHQxNjI1MDc5NVxuICBpZiAob2Zmc2V0ID4gZ2V0Tm9kZUxlbmd0aChub2RlKSkge1xuICAgIC8vIGluIHRoaXMgY2FzZSB3ZSBrbm93IHRoYXQgdGhlIGNhbGwgdG8gJ3JhbmdlLnNldFN0YXJ0JyBpcyBhYm91dCB0byB0aHJvd1xuICAgIERyYWZ0SnNEZWJ1Z0xvZ2dpbmcubG9nU2VsZWN0aW9uU3RhdGVGYWlsdXJlKHtcbiAgICAgIGFub255bWl6ZWREb206IGdldEFub255bWl6ZWRFZGl0b3JET00obm9kZSksXG4gICAgICBleHRyYVBhcmFtczogSlNPTi5zdHJpbmdpZnkoeyBvZmZzZXQ6IG9mZnNldCB9KSxcbiAgICAgIHNlbGVjdGlvblN0YXRlOiBKU09OLnN0cmluZ2lmeShzZWxlY3Rpb25TdGF0ZS50b0pTKCkpXG4gICAgfSk7XG4gIH1cbiAgcmFuZ2Uuc2V0U3RhcnQobm9kZSwgb2Zmc2V0KTtcbiAgc2VsZWN0aW9uLmFkZFJhbmdlKHJhbmdlKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBzZXREcmFmdEVkaXRvclNlbGVjdGlvbjsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFZQTtBQTFCQTtBQTZCQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/setDraftEditorSelection.js
