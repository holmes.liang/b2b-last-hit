

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _noImportant = __webpack_require__(/*! aphrodite/no-important */ "./node_modules/aphrodite/no-important.js");

var _theme = __webpack_require__(/*! ../theme */ "./node_modules/react-images/lib/theme.js");

var _theme2 = _interopRequireDefault(_theme);

var _deepMerge = __webpack_require__(/*! ../utils/deepMerge */ "./node_modules/react-images/lib/utils/deepMerge.js");

var _deepMerge2 = _interopRequireDefault(_deepMerge);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function Thumbnail(_ref, _ref2) {
  var index = _ref.index,
      src = _ref.src,
      thumbnail = _ref.thumbnail,
      active = _ref.active,
      _onClick = _ref.onClick;
  var theme = _ref2.theme;
  var url = thumbnail ? thumbnail : src;

  var classes = _noImportant.StyleSheet.create((0, _deepMerge2.default)(defaultStyles, theme));

  return _react2.default.createElement('div', {
    className: (0, _noImportant.css)(classes.thumbnail, active && classes.thumbnail__active),
    onClick: function onClick(e) {
      e.preventDefault();
      e.stopPropagation();

      _onClick(index);
    },
    style: {
      backgroundImage: 'url("' + url + '")'
    }
  });
}

Thumbnail.propTypes = {
  active: _propTypes2.default.bool,
  index: _propTypes2.default.number,
  onClick: _propTypes2.default.func.isRequired,
  src: _propTypes2.default.string,
  thumbnail: _propTypes2.default.string
};
Thumbnail.contextTypes = {
  theme: _propTypes2.default.object.isRequired
};
var defaultStyles = {
  thumbnail: {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    borderRadius: 2,
    boxShadow: 'inset 0 0 0 1px hsla(0,0%,100%,.2)',
    cursor: 'pointer',
    display: 'inline-block',
    height: _theme2.default.thumbnail.size,
    margin: _theme2.default.thumbnail.gutter,
    overflow: 'hidden',
    width: _theme2.default.thumbnail.size
  },
  thumbnail__active: {
    boxShadow: 'inset 0 0 0 2px ' + _theme2.default.thumbnail.activeBorderColor
  }
};
exports.default = Thumbnail;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi9jb21wb25lbnRzL1RodW1ibmFpbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JlYWN0LWltYWdlcy9saWIvY29tcG9uZW50cy9UaHVtYm5haWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX25vSW1wb3J0YW50ID0gcmVxdWlyZSgnYXBocm9kaXRlL25vLWltcG9ydGFudCcpO1xuXG52YXIgX3RoZW1lID0gcmVxdWlyZSgnLi4vdGhlbWUnKTtcblxudmFyIF90aGVtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF90aGVtZSk7XG5cbnZhciBfZGVlcE1lcmdlID0gcmVxdWlyZSgnLi4vdXRpbHMvZGVlcE1lcmdlJyk7XG5cbnZhciBfZGVlcE1lcmdlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZXBNZXJnZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIFRodW1ibmFpbChfcmVmLCBfcmVmMikge1xuXHR2YXIgaW5kZXggPSBfcmVmLmluZGV4LFxuXHQgICAgc3JjID0gX3JlZi5zcmMsXG5cdCAgICB0aHVtYm5haWwgPSBfcmVmLnRodW1ibmFpbCxcblx0ICAgIGFjdGl2ZSA9IF9yZWYuYWN0aXZlLFxuXHQgICAgX29uQ2xpY2sgPSBfcmVmLm9uQ2xpY2s7XG5cdHZhciB0aGVtZSA9IF9yZWYyLnRoZW1lO1xuXG5cdHZhciB1cmwgPSB0aHVtYm5haWwgPyB0aHVtYm5haWwgOiBzcmM7XG5cdHZhciBjbGFzc2VzID0gX25vSW1wb3J0YW50LlN0eWxlU2hlZXQuY3JlYXRlKCgwLCBfZGVlcE1lcmdlMi5kZWZhdWx0KShkZWZhdWx0U3R5bGVzLCB0aGVtZSkpO1xuXG5cdHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnZGl2Jywge1xuXHRcdGNsYXNzTmFtZTogKDAsIF9ub0ltcG9ydGFudC5jc3MpKGNsYXNzZXMudGh1bWJuYWlsLCBhY3RpdmUgJiYgY2xhc3Nlcy50aHVtYm5haWxfX2FjdGl2ZSksXG5cdFx0b25DbGljazogZnVuY3Rpb24gb25DbGljayhlKSB7XG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0X29uQ2xpY2soaW5kZXgpO1xuXHRcdH0sXG5cdFx0c3R5bGU6IHsgYmFja2dyb3VuZEltYWdlOiAndXJsKFwiJyArIHVybCArICdcIiknIH1cblx0fSk7XG59XG5cblRodW1ibmFpbC5wcm9wVHlwZXMgPSB7XG5cdGFjdGl2ZTogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sLFxuXHRpbmRleDogX3Byb3BUeXBlczIuZGVmYXVsdC5udW1iZXIsXG5cdG9uQ2xpY2s6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuXHRzcmM6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLFxuXHR0aHVtYm5haWw6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nXG59O1xuXG5UaHVtYm5haWwuY29udGV4dFR5cGVzID0ge1xuXHR0aGVtZTogX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3QuaXNSZXF1aXJlZFxufTtcblxudmFyIGRlZmF1bHRTdHlsZXMgPSB7XG5cdHRodW1ibmFpbDoge1xuXHRcdGJhY2tncm91bmRQb3NpdGlvbjogJ2NlbnRlcicsXG5cdFx0YmFja2dyb3VuZFNpemU6ICdjb3ZlcicsXG5cdFx0Ym9yZGVyUmFkaXVzOiAyLFxuXHRcdGJveFNoYWRvdzogJ2luc2V0IDAgMCAwIDFweCBoc2xhKDAsMCUsMTAwJSwuMiknLFxuXHRcdGN1cnNvcjogJ3BvaW50ZXInLFxuXHRcdGRpc3BsYXk6ICdpbmxpbmUtYmxvY2snLFxuXHRcdGhlaWdodDogX3RoZW1lMi5kZWZhdWx0LnRodW1ibmFpbC5zaXplLFxuXHRcdG1hcmdpbjogX3RoZW1lMi5kZWZhdWx0LnRodW1ibmFpbC5ndXR0ZXIsXG5cdFx0b3ZlcmZsb3c6ICdoaWRkZW4nLFxuXHRcdHdpZHRoOiBfdGhlbWUyLmRlZmF1bHQudGh1bWJuYWlsLnNpemVcblx0fSxcblx0dGh1bWJuYWlsX19hY3RpdmU6IHtcblx0XHRib3hTaGFkb3c6ICdpbnNldCAwIDAgMCAycHggJyArIF90aGVtZTIuZGVmYXVsdC50aHVtYm5haWwuYWN0aXZlQm9yZGVyQ29sb3Jcblx0fVxufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gVGh1bWJuYWlsOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTtBQURBO0FBYkE7QUFrQkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-images/lib/components/Thumbnail.js
