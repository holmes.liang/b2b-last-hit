/* WEBPACK VAR INJECTION */(function(global) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule editOnBeforeInput
 * @format
 * 
 */


var BlockTree = __webpack_require__(/*! ./BlockTree */ "./node_modules/draft-js/lib/BlockTree.js");

var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var UserAgent = __webpack_require__(/*! fbjs/lib/UserAgent */ "./node_modules/fbjs/lib/UserAgent.js");

var getEntityKeyForSelection = __webpack_require__(/*! ./getEntityKeyForSelection */ "./node_modules/draft-js/lib/getEntityKeyForSelection.js");

var isEventHandled = __webpack_require__(/*! ./isEventHandled */ "./node_modules/draft-js/lib/isEventHandled.js");

var isSelectionAtLeafStart = __webpack_require__(/*! ./isSelectionAtLeafStart */ "./node_modules/draft-js/lib/isSelectionAtLeafStart.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");

var setImmediate = __webpack_require__(/*! fbjs/lib/setImmediate */ "./node_modules/fbjs/lib/setImmediate.js"); // When nothing is focused, Firefox regards two characters, `'` and `/`, as
// commands that should open and focus the "quickfind" search bar. This should
// *never* happen while a contenteditable is focused, but as of v28, it
// sometimes does, even when the keypress event target is the contenteditable.
// This breaks the input. Special case these characters to ensure that when
// they are typed, we prevent default on the event to make sure not to
// trigger quickfind.


var FF_QUICKFIND_CHAR = "'";
var FF_QUICKFIND_LINK_CHAR = '/';
var isFirefox = UserAgent.isBrowser('Firefox');

function mustPreventDefaultForCharacter(character) {
  return isFirefox && (character == FF_QUICKFIND_CHAR || character == FF_QUICKFIND_LINK_CHAR);
}
/**
 * Replace the current selection with the specified text string, with the
 * inline style and entity key applied to the newly inserted text.
 */


function replaceText(editorState, text, inlineStyle, entityKey) {
  var contentState = DraftModifier.replaceText(editorState.getCurrentContent(), editorState.getSelection(), text, inlineStyle, entityKey);
  return EditorState.push(editorState, contentState, 'insert-characters');
}
/**
 * When `onBeforeInput` executes, the browser is attempting to insert a
 * character into the editor. Apply this character data to the document,
 * allowing native insertion if possible.
 *
 * Native insertion is encouraged in order to limit re-rendering and to
 * preserve spellcheck highlighting, which disappears or flashes if re-render
 * occurs on the relevant text nodes.
 */


function editOnBeforeInput(editor, e) {
  if (editor._pendingStateFromBeforeInput !== undefined) {
    editor.update(editor._pendingStateFromBeforeInput);
    editor._pendingStateFromBeforeInput = undefined;
  }

  var editorState = editor._latestEditorState;
  var chars = e.data; // In some cases (ex: IE ideographic space insertion) no character data
  // is provided. There's nothing to do when this happens.

  if (!chars) {
    return;
  } // Allow the top-level component to handle the insertion manually. This is
  // useful when triggering interesting behaviors for a character insertion,
  // Simple examples: replacing a raw text ':)' with a smile emoji or image
  // decorator, or setting a block to be a list item after typing '- ' at the
  // start of the block.


  if (editor.props.handleBeforeInput && isEventHandled(editor.props.handleBeforeInput(chars, editorState))) {
    e.preventDefault();
    return;
  } // If selection is collapsed, conditionally allow native behavior. This
  // reduces re-renders and preserves spellcheck highlighting. If the selection
  // is not collapsed, we will re-render.


  var selection = editorState.getSelection();
  var selectionStart = selection.getStartOffset();
  var selectionEnd = selection.getEndOffset();
  var anchorKey = selection.getAnchorKey();

  if (!selection.isCollapsed()) {
    e.preventDefault(); // If the currently selected text matches what the user is trying to
    // replace it with, let's just update the `SelectionState`. If not, update
    // the `ContentState` with the new text.

    var currentlySelectedChars = editorState.getCurrentContent().getPlainText().slice(selectionStart, selectionEnd);

    if (chars === currentlySelectedChars) {
      editor.update(EditorState.forceSelection(editorState, selection.merge({
        focusOffset: selectionEnd
      })));
    } else {
      editor.update(replaceText(editorState, chars, editorState.getCurrentInlineStyle(), getEntityKeyForSelection(editorState.getCurrentContent(), editorState.getSelection())));
    }

    return;
  }

  var newEditorState = replaceText(editorState, chars, editorState.getCurrentInlineStyle(), getEntityKeyForSelection(editorState.getCurrentContent(), editorState.getSelection())); // Bunch of different cases follow where we need to prevent native insertion.

  var mustPreventNative = false;

  if (!mustPreventNative) {
    // Browsers tend to insert text in weird places in the DOM when typing at
    // the start of a leaf, so we'll handle it ourselves.
    mustPreventNative = isSelectionAtLeafStart(editor._latestCommittedEditorState);
  }

  if (!mustPreventNative) {
    // Chrome will also split up a node into two pieces if it contains a Tab
    // char, for no explicable reason. Seemingly caused by this commit:
    // https://chromium.googlesource.com/chromium/src/+/013ac5eaf3%5E%21/
    var nativeSelection = global.getSelection(); // Selection is necessarily collapsed at this point due to earlier check.

    if (nativeSelection.anchorNode && nativeSelection.anchorNode.nodeType === Node.TEXT_NODE) {
      // See isTabHTMLSpanElement in chromium EditingUtilities.cpp.
      var parentNode = nativeSelection.anchorNode.parentNode;
      mustPreventNative = parentNode.nodeName === 'SPAN' && parentNode.firstChild.nodeType === Node.TEXT_NODE && parentNode.firstChild.nodeValue.indexOf('\t') !== -1;
    }
  }

  if (!mustPreventNative) {
    // Check the old and new "fingerprints" of the current block to determine
    // whether this insertion requires any addition or removal of text nodes,
    // in which case we would prevent the native character insertion.
    var originalFingerprint = BlockTree.getFingerprint(editorState.getBlockTree(anchorKey));
    var newFingerprint = BlockTree.getFingerprint(newEditorState.getBlockTree(anchorKey));
    mustPreventNative = originalFingerprint !== newFingerprint;
  }

  if (!mustPreventNative) {
    mustPreventNative = mustPreventDefaultForCharacter(chars);
  }

  if (!mustPreventNative) {
    mustPreventNative = nullthrows(newEditorState.getDirectionMap()).get(anchorKey) !== nullthrows(editorState.getDirectionMap()).get(anchorKey);
  }

  if (mustPreventNative) {
    e.preventDefault();
    editor.update(newEditorState);
    return;
  } // We made it all the way! Let the browser do its thing and insert the char.


  newEditorState = EditorState.set(newEditorState, {
    nativelyRenderedContent: newEditorState.getCurrentContent()
  }); // The native event is allowed to occur. To allow user onChange handlers to
  // change the inserted text, we wait until the text is actually inserted
  // before we actually update our state. That way when we rerender, the text
  // we see in the DOM will already have been inserted properly.

  editor._pendingStateFromBeforeInput = newEditorState;
  setImmediate(function () {
    if (editor._pendingStateFromBeforeInput !== undefined) {
      editor.update(editor._pendingStateFromBeforeInput);
      editor._pendingStateFromBeforeInput = undefined;
    }
  });
}

module.exports = editOnBeforeInput;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbkJlZm9yZUlucHV0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbkJlZm9yZUlucHV0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZWRpdE9uQmVmb3JlSW5wdXRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEJsb2NrVHJlZSA9IHJlcXVpcmUoJy4vQmxvY2tUcmVlJyk7XG52YXIgRHJhZnRNb2RpZmllciA9IHJlcXVpcmUoJy4vRHJhZnRNb2RpZmllcicpO1xudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xudmFyIFVzZXJBZ2VudCA9IHJlcXVpcmUoJ2ZianMvbGliL1VzZXJBZ2VudCcpO1xuXG52YXIgZ2V0RW50aXR5S2V5Rm9yU2VsZWN0aW9uID0gcmVxdWlyZSgnLi9nZXRFbnRpdHlLZXlGb3JTZWxlY3Rpb24nKTtcbnZhciBpc0V2ZW50SGFuZGxlZCA9IHJlcXVpcmUoJy4vaXNFdmVudEhhbmRsZWQnKTtcbnZhciBpc1NlbGVjdGlvbkF0TGVhZlN0YXJ0ID0gcmVxdWlyZSgnLi9pc1NlbGVjdGlvbkF0TGVhZlN0YXJ0Jyk7XG52YXIgbnVsbHRocm93cyA9IHJlcXVpcmUoJ2ZianMvbGliL251bGx0aHJvd3MnKTtcbnZhciBzZXRJbW1lZGlhdGUgPSByZXF1aXJlKCdmYmpzL2xpYi9zZXRJbW1lZGlhdGUnKTtcblxuLy8gV2hlbiBub3RoaW5nIGlzIGZvY3VzZWQsIEZpcmVmb3ggcmVnYXJkcyB0d28gY2hhcmFjdGVycywgYCdgIGFuZCBgL2AsIGFzXG4vLyBjb21tYW5kcyB0aGF0IHNob3VsZCBvcGVuIGFuZCBmb2N1cyB0aGUgXCJxdWlja2ZpbmRcIiBzZWFyY2ggYmFyLiBUaGlzIHNob3VsZFxuLy8gKm5ldmVyKiBoYXBwZW4gd2hpbGUgYSBjb250ZW50ZWRpdGFibGUgaXMgZm9jdXNlZCwgYnV0IGFzIG9mIHYyOCwgaXRcbi8vIHNvbWV0aW1lcyBkb2VzLCBldmVuIHdoZW4gdGhlIGtleXByZXNzIGV2ZW50IHRhcmdldCBpcyB0aGUgY29udGVudGVkaXRhYmxlLlxuLy8gVGhpcyBicmVha3MgdGhlIGlucHV0LiBTcGVjaWFsIGNhc2UgdGhlc2UgY2hhcmFjdGVycyB0byBlbnN1cmUgdGhhdCB3aGVuXG4vLyB0aGV5IGFyZSB0eXBlZCwgd2UgcHJldmVudCBkZWZhdWx0IG9uIHRoZSBldmVudCB0byBtYWtlIHN1cmUgbm90IHRvXG4vLyB0cmlnZ2VyIHF1aWNrZmluZC5cbnZhciBGRl9RVUlDS0ZJTkRfQ0hBUiA9IFwiJ1wiO1xudmFyIEZGX1FVSUNLRklORF9MSU5LX0NIQVIgPSAnLyc7XG52YXIgaXNGaXJlZm94ID0gVXNlckFnZW50LmlzQnJvd3NlcignRmlyZWZveCcpO1xuXG5mdW5jdGlvbiBtdXN0UHJldmVudERlZmF1bHRGb3JDaGFyYWN0ZXIoY2hhcmFjdGVyKSB7XG4gIHJldHVybiBpc0ZpcmVmb3ggJiYgKGNoYXJhY3RlciA9PSBGRl9RVUlDS0ZJTkRfQ0hBUiB8fCBjaGFyYWN0ZXIgPT0gRkZfUVVJQ0tGSU5EX0xJTktfQ0hBUik7XG59XG5cbi8qKlxuICogUmVwbGFjZSB0aGUgY3VycmVudCBzZWxlY3Rpb24gd2l0aCB0aGUgc3BlY2lmaWVkIHRleHQgc3RyaW5nLCB3aXRoIHRoZVxuICogaW5saW5lIHN0eWxlIGFuZCBlbnRpdHkga2V5IGFwcGxpZWQgdG8gdGhlIG5ld2x5IGluc2VydGVkIHRleHQuXG4gKi9cbmZ1bmN0aW9uIHJlcGxhY2VUZXh0KGVkaXRvclN0YXRlLCB0ZXh0LCBpbmxpbmVTdHlsZSwgZW50aXR5S2V5KSB7XG4gIHZhciBjb250ZW50U3RhdGUgPSBEcmFmdE1vZGlmaWVyLnJlcGxhY2VUZXh0KGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCksIGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpLCB0ZXh0LCBpbmxpbmVTdHlsZSwgZW50aXR5S2V5KTtcbiAgcmV0dXJuIEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIGNvbnRlbnRTdGF0ZSwgJ2luc2VydC1jaGFyYWN0ZXJzJyk7XG59XG5cbi8qKlxuICogV2hlbiBgb25CZWZvcmVJbnB1dGAgZXhlY3V0ZXMsIHRoZSBicm93c2VyIGlzIGF0dGVtcHRpbmcgdG8gaW5zZXJ0IGFcbiAqIGNoYXJhY3RlciBpbnRvIHRoZSBlZGl0b3IuIEFwcGx5IHRoaXMgY2hhcmFjdGVyIGRhdGEgdG8gdGhlIGRvY3VtZW50LFxuICogYWxsb3dpbmcgbmF0aXZlIGluc2VydGlvbiBpZiBwb3NzaWJsZS5cbiAqXG4gKiBOYXRpdmUgaW5zZXJ0aW9uIGlzIGVuY291cmFnZWQgaW4gb3JkZXIgdG8gbGltaXQgcmUtcmVuZGVyaW5nIGFuZCB0b1xuICogcHJlc2VydmUgc3BlbGxjaGVjayBoaWdobGlnaHRpbmcsIHdoaWNoIGRpc2FwcGVhcnMgb3IgZmxhc2hlcyBpZiByZS1yZW5kZXJcbiAqIG9jY3VycyBvbiB0aGUgcmVsZXZhbnQgdGV4dCBub2Rlcy5cbiAqL1xuZnVuY3Rpb24gZWRpdE9uQmVmb3JlSW5wdXQoZWRpdG9yLCBlKSB7XG4gIGlmIChlZGl0b3IuX3BlbmRpbmdTdGF0ZUZyb21CZWZvcmVJbnB1dCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgZWRpdG9yLnVwZGF0ZShlZGl0b3IuX3BlbmRpbmdTdGF0ZUZyb21CZWZvcmVJbnB1dCk7XG4gICAgZWRpdG9yLl9wZW5kaW5nU3RhdGVGcm9tQmVmb3JlSW5wdXQgPSB1bmRlZmluZWQ7XG4gIH1cblxuICB2YXIgZWRpdG9yU3RhdGUgPSBlZGl0b3IuX2xhdGVzdEVkaXRvclN0YXRlO1xuXG4gIHZhciBjaGFycyA9IGUuZGF0YTtcblxuICAvLyBJbiBzb21lIGNhc2VzIChleDogSUUgaWRlb2dyYXBoaWMgc3BhY2UgaW5zZXJ0aW9uKSBubyBjaGFyYWN0ZXIgZGF0YVxuICAvLyBpcyBwcm92aWRlZC4gVGhlcmUncyBub3RoaW5nIHRvIGRvIHdoZW4gdGhpcyBoYXBwZW5zLlxuICBpZiAoIWNoYXJzKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLy8gQWxsb3cgdGhlIHRvcC1sZXZlbCBjb21wb25lbnQgdG8gaGFuZGxlIHRoZSBpbnNlcnRpb24gbWFudWFsbHkuIFRoaXMgaXNcbiAgLy8gdXNlZnVsIHdoZW4gdHJpZ2dlcmluZyBpbnRlcmVzdGluZyBiZWhhdmlvcnMgZm9yIGEgY2hhcmFjdGVyIGluc2VydGlvbixcbiAgLy8gU2ltcGxlIGV4YW1wbGVzOiByZXBsYWNpbmcgYSByYXcgdGV4dCAnOiknIHdpdGggYSBzbWlsZSBlbW9qaSBvciBpbWFnZVxuICAvLyBkZWNvcmF0b3IsIG9yIHNldHRpbmcgYSBibG9jayB0byBiZSBhIGxpc3QgaXRlbSBhZnRlciB0eXBpbmcgJy0gJyBhdCB0aGVcbiAgLy8gc3RhcnQgb2YgdGhlIGJsb2NrLlxuICBpZiAoZWRpdG9yLnByb3BzLmhhbmRsZUJlZm9yZUlucHV0ICYmIGlzRXZlbnRIYW5kbGVkKGVkaXRvci5wcm9wcy5oYW5kbGVCZWZvcmVJbnB1dChjaGFycywgZWRpdG9yU3RhdGUpKSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICByZXR1cm47XG4gIH1cblxuICAvLyBJZiBzZWxlY3Rpb24gaXMgY29sbGFwc2VkLCBjb25kaXRpb25hbGx5IGFsbG93IG5hdGl2ZSBiZWhhdmlvci4gVGhpc1xuICAvLyByZWR1Y2VzIHJlLXJlbmRlcnMgYW5kIHByZXNlcnZlcyBzcGVsbGNoZWNrIGhpZ2hsaWdodGluZy4gSWYgdGhlIHNlbGVjdGlvblxuICAvLyBpcyBub3QgY29sbGFwc2VkLCB3ZSB3aWxsIHJlLXJlbmRlci5cbiAgdmFyIHNlbGVjdGlvbiA9IGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpO1xuICB2YXIgc2VsZWN0aW9uU3RhcnQgPSBzZWxlY3Rpb24uZ2V0U3RhcnRPZmZzZXQoKTtcbiAgdmFyIHNlbGVjdGlvbkVuZCA9IHNlbGVjdGlvbi5nZXRFbmRPZmZzZXQoKTtcbiAgdmFyIGFuY2hvcktleSA9IHNlbGVjdGlvbi5nZXRBbmNob3JLZXkoKTtcblxuICBpZiAoIXNlbGVjdGlvbi5pc0NvbGxhcHNlZCgpKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgLy8gSWYgdGhlIGN1cnJlbnRseSBzZWxlY3RlZCB0ZXh0IG1hdGNoZXMgd2hhdCB0aGUgdXNlciBpcyB0cnlpbmcgdG9cbiAgICAvLyByZXBsYWNlIGl0IHdpdGgsIGxldCdzIGp1c3QgdXBkYXRlIHRoZSBgU2VsZWN0aW9uU3RhdGVgLiBJZiBub3QsIHVwZGF0ZVxuICAgIC8vIHRoZSBgQ29udGVudFN0YXRlYCB3aXRoIHRoZSBuZXcgdGV4dC5cbiAgICB2YXIgY3VycmVudGx5U2VsZWN0ZWRDaGFycyA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCkuZ2V0UGxhaW5UZXh0KCkuc2xpY2Uoc2VsZWN0aW9uU3RhcnQsIHNlbGVjdGlvbkVuZCk7XG4gICAgaWYgKGNoYXJzID09PSBjdXJyZW50bHlTZWxlY3RlZENoYXJzKSB7XG4gICAgICBlZGl0b3IudXBkYXRlKEVkaXRvclN0YXRlLmZvcmNlU2VsZWN0aW9uKGVkaXRvclN0YXRlLCBzZWxlY3Rpb24ubWVyZ2Uoe1xuICAgICAgICBmb2N1c09mZnNldDogc2VsZWN0aW9uRW5kXG4gICAgICB9KSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICBlZGl0b3IudXBkYXRlKHJlcGxhY2VUZXh0KGVkaXRvclN0YXRlLCBjaGFycywgZWRpdG9yU3RhdGUuZ2V0Q3VycmVudElubGluZVN0eWxlKCksIGdldEVudGl0eUtleUZvclNlbGVjdGlvbihlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpLCBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKSkpKTtcbiAgICB9XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIG5ld0VkaXRvclN0YXRlID0gcmVwbGFjZVRleHQoZWRpdG9yU3RhdGUsIGNoYXJzLCBlZGl0b3JTdGF0ZS5nZXRDdXJyZW50SW5saW5lU3R5bGUoKSwgZ2V0RW50aXR5S2V5Rm9yU2VsZWN0aW9uKGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCksIGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpKSk7XG5cbiAgLy8gQnVuY2ggb2YgZGlmZmVyZW50IGNhc2VzIGZvbGxvdyB3aGVyZSB3ZSBuZWVkIHRvIHByZXZlbnQgbmF0aXZlIGluc2VydGlvbi5cbiAgdmFyIG11c3RQcmV2ZW50TmF0aXZlID0gZmFsc2U7XG4gIGlmICghbXVzdFByZXZlbnROYXRpdmUpIHtcbiAgICAvLyBCcm93c2VycyB0ZW5kIHRvIGluc2VydCB0ZXh0IGluIHdlaXJkIHBsYWNlcyBpbiB0aGUgRE9NIHdoZW4gdHlwaW5nIGF0XG4gICAgLy8gdGhlIHN0YXJ0IG9mIGEgbGVhZiwgc28gd2UnbGwgaGFuZGxlIGl0IG91cnNlbHZlcy5cbiAgICBtdXN0UHJldmVudE5hdGl2ZSA9IGlzU2VsZWN0aW9uQXRMZWFmU3RhcnQoZWRpdG9yLl9sYXRlc3RDb21taXR0ZWRFZGl0b3JTdGF0ZSk7XG4gIH1cbiAgaWYgKCFtdXN0UHJldmVudE5hdGl2ZSkge1xuICAgIC8vIENocm9tZSB3aWxsIGFsc28gc3BsaXQgdXAgYSBub2RlIGludG8gdHdvIHBpZWNlcyBpZiBpdCBjb250YWlucyBhIFRhYlxuICAgIC8vIGNoYXIsIGZvciBubyBleHBsaWNhYmxlIHJlYXNvbi4gU2VlbWluZ2x5IGNhdXNlZCBieSB0aGlzIGNvbW1pdDpcbiAgICAvLyBodHRwczovL2Nocm9taXVtLmdvb2dsZXNvdXJjZS5jb20vY2hyb21pdW0vc3JjLysvMDEzYWM1ZWFmMyU1RSUyMS9cbiAgICB2YXIgbmF0aXZlU2VsZWN0aW9uID0gZ2xvYmFsLmdldFNlbGVjdGlvbigpO1xuICAgIC8vIFNlbGVjdGlvbiBpcyBuZWNlc3NhcmlseSBjb2xsYXBzZWQgYXQgdGhpcyBwb2ludCBkdWUgdG8gZWFybGllciBjaGVjay5cbiAgICBpZiAobmF0aXZlU2VsZWN0aW9uLmFuY2hvck5vZGUgJiYgbmF0aXZlU2VsZWN0aW9uLmFuY2hvck5vZGUubm9kZVR5cGUgPT09IE5vZGUuVEVYVF9OT0RFKSB7XG4gICAgICAvLyBTZWUgaXNUYWJIVE1MU3BhbkVsZW1lbnQgaW4gY2hyb21pdW0gRWRpdGluZ1V0aWxpdGllcy5jcHAuXG4gICAgICB2YXIgcGFyZW50Tm9kZSA9IG5hdGl2ZVNlbGVjdGlvbi5hbmNob3JOb2RlLnBhcmVudE5vZGU7XG4gICAgICBtdXN0UHJldmVudE5hdGl2ZSA9IHBhcmVudE5vZGUubm9kZU5hbWUgPT09ICdTUEFOJyAmJiBwYXJlbnROb2RlLmZpcnN0Q2hpbGQubm9kZVR5cGUgPT09IE5vZGUuVEVYVF9OT0RFICYmIHBhcmVudE5vZGUuZmlyc3RDaGlsZC5ub2RlVmFsdWUuaW5kZXhPZignXFx0JykgIT09IC0xO1xuICAgIH1cbiAgfVxuICBpZiAoIW11c3RQcmV2ZW50TmF0aXZlKSB7XG4gICAgLy8gQ2hlY2sgdGhlIG9sZCBhbmQgbmV3IFwiZmluZ2VycHJpbnRzXCIgb2YgdGhlIGN1cnJlbnQgYmxvY2sgdG8gZGV0ZXJtaW5lXG4gICAgLy8gd2hldGhlciB0aGlzIGluc2VydGlvbiByZXF1aXJlcyBhbnkgYWRkaXRpb24gb3IgcmVtb3ZhbCBvZiB0ZXh0IG5vZGVzLFxuICAgIC8vIGluIHdoaWNoIGNhc2Ugd2Ugd291bGQgcHJldmVudCB0aGUgbmF0aXZlIGNoYXJhY3RlciBpbnNlcnRpb24uXG4gICAgdmFyIG9yaWdpbmFsRmluZ2VycHJpbnQgPSBCbG9ja1RyZWUuZ2V0RmluZ2VycHJpbnQoZWRpdG9yU3RhdGUuZ2V0QmxvY2tUcmVlKGFuY2hvcktleSkpO1xuICAgIHZhciBuZXdGaW5nZXJwcmludCA9IEJsb2NrVHJlZS5nZXRGaW5nZXJwcmludChuZXdFZGl0b3JTdGF0ZS5nZXRCbG9ja1RyZWUoYW5jaG9yS2V5KSk7XG4gICAgbXVzdFByZXZlbnROYXRpdmUgPSBvcmlnaW5hbEZpbmdlcnByaW50ICE9PSBuZXdGaW5nZXJwcmludDtcbiAgfVxuICBpZiAoIW11c3RQcmV2ZW50TmF0aXZlKSB7XG4gICAgbXVzdFByZXZlbnROYXRpdmUgPSBtdXN0UHJldmVudERlZmF1bHRGb3JDaGFyYWN0ZXIoY2hhcnMpO1xuICB9XG4gIGlmICghbXVzdFByZXZlbnROYXRpdmUpIHtcbiAgICBtdXN0UHJldmVudE5hdGl2ZSA9IG51bGx0aHJvd3MobmV3RWRpdG9yU3RhdGUuZ2V0RGlyZWN0aW9uTWFwKCkpLmdldChhbmNob3JLZXkpICE9PSBudWxsdGhyb3dzKGVkaXRvclN0YXRlLmdldERpcmVjdGlvbk1hcCgpKS5nZXQoYW5jaG9yS2V5KTtcbiAgfVxuXG4gIGlmIChtdXN0UHJldmVudE5hdGl2ZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBlZGl0b3IudXBkYXRlKG5ld0VkaXRvclN0YXRlKTtcbiAgICByZXR1cm47XG4gIH1cblxuICAvLyBXZSBtYWRlIGl0IGFsbCB0aGUgd2F5ISBMZXQgdGhlIGJyb3dzZXIgZG8gaXRzIHRoaW5nIGFuZCBpbnNlcnQgdGhlIGNoYXIuXG4gIG5ld0VkaXRvclN0YXRlID0gRWRpdG9yU3RhdGUuc2V0KG5ld0VkaXRvclN0YXRlLCB7XG4gICAgbmF0aXZlbHlSZW5kZXJlZENvbnRlbnQ6IG5ld0VkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KClcbiAgfSk7XG4gIC8vIFRoZSBuYXRpdmUgZXZlbnQgaXMgYWxsb3dlZCB0byBvY2N1ci4gVG8gYWxsb3cgdXNlciBvbkNoYW5nZSBoYW5kbGVycyB0b1xuICAvLyBjaGFuZ2UgdGhlIGluc2VydGVkIHRleHQsIHdlIHdhaXQgdW50aWwgdGhlIHRleHQgaXMgYWN0dWFsbHkgaW5zZXJ0ZWRcbiAgLy8gYmVmb3JlIHdlIGFjdHVhbGx5IHVwZGF0ZSBvdXIgc3RhdGUuIFRoYXQgd2F5IHdoZW4gd2UgcmVyZW5kZXIsIHRoZSB0ZXh0XG4gIC8vIHdlIHNlZSBpbiB0aGUgRE9NIHdpbGwgYWxyZWFkeSBoYXZlIGJlZW4gaW5zZXJ0ZWQgcHJvcGVybHkuXG4gIGVkaXRvci5fcGVuZGluZ1N0YXRlRnJvbUJlZm9yZUlucHV0ID0gbmV3RWRpdG9yU3RhdGU7XG4gIHNldEltbWVkaWF0ZShmdW5jdGlvbiAoKSB7XG4gICAgaWYgKGVkaXRvci5fcGVuZGluZ1N0YXRlRnJvbUJlZm9yZUlucHV0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIGVkaXRvci51cGRhdGUoZWRpdG9yLl9wZW5kaW5nU3RhdGVGcm9tQmVmb3JlSW5wdXQpO1xuICAgICAgZWRpdG9yLl9wZW5kaW5nU3RhdGVGcm9tQmVmb3JlSW5wdXQgPSB1bmRlZmluZWQ7XG4gICAgfVxuICB9KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBlZGl0T25CZWZvcmVJbnB1dDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/editOnBeforeInput.js
