/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Cartesian coordinate system
 * @module  echarts/coord/Cartesian
 *
 */


function dimAxisMapper(dim) {
  return this._axes[dim];
}
/**
 * @alias module:echarts/coord/Cartesian
 * @constructor
 */


var Cartesian = function Cartesian(name) {
  this._axes = {};
  this._dimList = [];
  /**
   * @type {string}
   */

  this.name = name || '';
};

Cartesian.prototype = {
  constructor: Cartesian,
  type: 'cartesian',

  /**
   * Get axis
   * @param  {number|string} dim
   * @return {module:echarts/coord/Cartesian~Axis}
   */
  getAxis: function getAxis(dim) {
    return this._axes[dim];
  },

  /**
   * Get axes list
   * @return {Array.<module:echarts/coord/Cartesian~Axis>}
   */
  getAxes: function getAxes() {
    return zrUtil.map(this._dimList, dimAxisMapper, this);
  },

  /**
   * Get axes list by given scale type
   */
  getAxesByScale: function getAxesByScale(scaleType) {
    scaleType = scaleType.toLowerCase();
    return zrUtil.filter(this.getAxes(), function (axis) {
      return axis.scale.type === scaleType;
    });
  },

  /**
   * Add axis
   * @param {module:echarts/coord/Cartesian.Axis}
   */
  addAxis: function addAxis(axis) {
    var dim = axis.dim;
    this._axes[dim] = axis;

    this._dimList.push(dim);
  },

  /**
   * Convert data to coord in nd space
   * @param {Array.<number>|Object.<string, number>} val
   * @return {Array.<number>|Object.<string, number>}
   */
  dataToCoord: function dataToCoord(val) {
    return this._dataCoordConvert(val, 'dataToCoord');
  },

  /**
   * Convert coord in nd space to data
   * @param  {Array.<number>|Object.<string, number>} val
   * @return {Array.<number>|Object.<string, number>}
   */
  coordToData: function coordToData(val) {
    return this._dataCoordConvert(val, 'coordToData');
  },
  _dataCoordConvert: function _dataCoordConvert(input, method) {
    var dimList = this._dimList;
    var output = input instanceof Array ? [] : {};

    for (var i = 0; i < dimList.length; i++) {
      var dim = dimList[i];
      var axis = this._axes[dim];
      output[dim] = axis[method](input[dim]);
    }

    return output;
  }
};
var _default = Cartesian;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvY2FydGVzaWFuL0NhcnRlc2lhbi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2Nvb3JkL2NhcnRlc2lhbi9DYXJ0ZXNpYW4uanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qKlxuICogQ2FydGVzaWFuIGNvb3JkaW5hdGUgc3lzdGVtXG4gKiBAbW9kdWxlICBlY2hhcnRzL2Nvb3JkL0NhcnRlc2lhblxuICpcbiAqL1xuZnVuY3Rpb24gZGltQXhpc01hcHBlcihkaW0pIHtcbiAgcmV0dXJuIHRoaXMuX2F4ZXNbZGltXTtcbn1cbi8qKlxuICogQGFsaWFzIG1vZHVsZTplY2hhcnRzL2Nvb3JkL0NhcnRlc2lhblxuICogQGNvbnN0cnVjdG9yXG4gKi9cblxuXG52YXIgQ2FydGVzaWFuID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgdGhpcy5fYXhlcyA9IHt9O1xuICB0aGlzLl9kaW1MaXN0ID0gW107XG4gIC8qKlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cblxuICB0aGlzLm5hbWUgPSBuYW1lIHx8ICcnO1xufTtcblxuQ2FydGVzaWFuLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IENhcnRlc2lhbixcbiAgdHlwZTogJ2NhcnRlc2lhbicsXG5cbiAgLyoqXG4gICAqIEdldCBheGlzXG4gICAqIEBwYXJhbSAge251bWJlcnxzdHJpbmd9IGRpbVxuICAgKiBAcmV0dXJuIHttb2R1bGU6ZWNoYXJ0cy9jb29yZC9DYXJ0ZXNpYW5+QXhpc31cbiAgICovXG4gIGdldEF4aXM6IGZ1bmN0aW9uIChkaW0pIHtcbiAgICByZXR1cm4gdGhpcy5fYXhlc1tkaW1dO1xuICB9LFxuXG4gIC8qKlxuICAgKiBHZXQgYXhlcyBsaXN0XG4gICAqIEByZXR1cm4ge0FycmF5Ljxtb2R1bGU6ZWNoYXJ0cy9jb29yZC9DYXJ0ZXNpYW5+QXhpcz59XG4gICAqL1xuICBnZXRBeGVzOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHpyVXRpbC5tYXAodGhpcy5fZGltTGlzdCwgZGltQXhpc01hcHBlciwgdGhpcyk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEdldCBheGVzIGxpc3QgYnkgZ2l2ZW4gc2NhbGUgdHlwZVxuICAgKi9cbiAgZ2V0QXhlc0J5U2NhbGU6IGZ1bmN0aW9uIChzY2FsZVR5cGUpIHtcbiAgICBzY2FsZVR5cGUgPSBzY2FsZVR5cGUudG9Mb3dlckNhc2UoKTtcbiAgICByZXR1cm4genJVdGlsLmZpbHRlcih0aGlzLmdldEF4ZXMoKSwgZnVuY3Rpb24gKGF4aXMpIHtcbiAgICAgIHJldHVybiBheGlzLnNjYWxlLnR5cGUgPT09IHNjYWxlVHlwZTtcbiAgICB9KTtcbiAgfSxcblxuICAvKipcbiAgICogQWRkIGF4aXNcbiAgICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9jb29yZC9DYXJ0ZXNpYW4uQXhpc31cbiAgICovXG4gIGFkZEF4aXM6IGZ1bmN0aW9uIChheGlzKSB7XG4gICAgdmFyIGRpbSA9IGF4aXMuZGltO1xuICAgIHRoaXMuX2F4ZXNbZGltXSA9IGF4aXM7XG5cbiAgICB0aGlzLl9kaW1MaXN0LnB1c2goZGltKTtcbiAgfSxcblxuICAvKipcbiAgICogQ29udmVydCBkYXRhIHRvIGNvb3JkIGluIG5kIHNwYWNlXG4gICAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj58T2JqZWN0LjxzdHJpbmcsIG51bWJlcj59IHZhbFxuICAgKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPnxPYmplY3QuPHN0cmluZywgbnVtYmVyPn1cbiAgICovXG4gIGRhdGFUb0Nvb3JkOiBmdW5jdGlvbiAodmFsKSB7XG4gICAgcmV0dXJuIHRoaXMuX2RhdGFDb29yZENvbnZlcnQodmFsLCAnZGF0YVRvQ29vcmQnKTtcbiAgfSxcblxuICAvKipcbiAgICogQ29udmVydCBjb29yZCBpbiBuZCBzcGFjZSB0byBkYXRhXG4gICAqIEBwYXJhbSAge0FycmF5LjxudW1iZXI+fE9iamVjdC48c3RyaW5nLCBudW1iZXI+fSB2YWxcbiAgICogQHJldHVybiB7QXJyYXkuPG51bWJlcj58T2JqZWN0LjxzdHJpbmcsIG51bWJlcj59XG4gICAqL1xuICBjb29yZFRvRGF0YTogZnVuY3Rpb24gKHZhbCkge1xuICAgIHJldHVybiB0aGlzLl9kYXRhQ29vcmRDb252ZXJ0KHZhbCwgJ2Nvb3JkVG9EYXRhJyk7XG4gIH0sXG4gIF9kYXRhQ29vcmRDb252ZXJ0OiBmdW5jdGlvbiAoaW5wdXQsIG1ldGhvZCkge1xuICAgIHZhciBkaW1MaXN0ID0gdGhpcy5fZGltTGlzdDtcbiAgICB2YXIgb3V0cHV0ID0gaW5wdXQgaW5zdGFuY2VvZiBBcnJheSA/IFtdIDoge307XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGRpbUxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkaW0gPSBkaW1MaXN0W2ldO1xuICAgICAgdmFyIGF4aXMgPSB0aGlzLl9heGVzW2RpbV07XG4gICAgICBvdXRwdXRbZGltXSA9IGF4aXNbbWV0aG9kXShpbnB1dFtkaW1dKTtcbiAgICB9XG5cbiAgICByZXR1cm4gb3V0cHV0O1xuICB9XG59O1xudmFyIF9kZWZhdWx0ID0gQ2FydGVzaWFuO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBdEVBO0FBd0VBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/cartesian/Cartesian.js
