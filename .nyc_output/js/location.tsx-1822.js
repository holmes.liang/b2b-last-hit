__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RightDiv", function() { return RightDiv; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Location", function() { return Location; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var number_precision__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! number-precision */ "./node_modules/number-precision/build/index.js");
/* harmony import */ var number_precision__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(number_precision__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _desk_quote_SAIC_iar_basic_location__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk/quote/SAIC/iar/basic-location */ "./src/app/desk/quote/SAIC/iar/basic-location.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk/quote/SAIC/iar/index */ "./src/app/desk/quote/SAIC/iar/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk/quote/SAIC/all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _component_render_location_table__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./component/render-location-table */ "./src/app/desk/quote/SAIC/iar/component/render-location-table.tsx");
/* harmony import */ var _component_render_fee_summary__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./component/render-fee-summary */ "./src/app/desk/quote/SAIC/iar/component/render-fee-summary.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_component_location_view__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @desk/quote/SAIC/iar/component/location-view */ "./src/app/desk/quote/SAIC/iar/component/location-view.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @desk/quote/SAIC/phs/components/side-info */ "./src/app/desk/quote/SAIC/phs/components/side-info.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_component_co_insurance__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @desk/quote/SAIC/iar/component/co-insurance */ "./src/app/desk/quote/SAIC/iar/component/co-insurance.tsx");
/* harmony import */ var _desk_quote_components_panyment_arramgement_payment_arrangement__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @desk/quote/components/panyment-arramgement/payment-arrangement */ "./src/app/desk/quote/components/panyment-arramgement/payment-arrangement.tsx");
/* harmony import */ var _desk_component_NPanelShowOutputs__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @desk-component/NPanelShowOutputs */ "./src/app/desk/component/NPanelShowOutputs.tsx");
/* harmony import */ var _desk_component_spin__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @desk-component/spin */ "./src/app/desk/component/spin.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/location.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__["default"])(["\n  .ant-input-number-input { \n    text-align: right\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}




















var RightDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_19__["Styled"].div(_templateObject());

var Location =
/*#__PURE__*/
function (_BasicLocation) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(Location, _BasicLocation);

  function Location() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Location);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Location)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.columns = [{
      title: "Postal Code",
      dataIndex: "location.postalCode",
      key: "location.postalCode"
    }, {
      title: "Address",
      dataIndex: "location",
      key: "location",
      render: function render(col, record, index, rowIndex) {
        var model = _this.props.model;
        return react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_component__WEBPACK_IMPORTED_MODULE_22__["ShowAddress"], {
          model: model,
          addressFixed: "policy.ext.locations.".concat(index, ".location"),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 53
          },
          __self: this
        });
      }
    }, {
      title: function title() {
        var dataCurrency = _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].formatCurrencyInfo("", _this.getValueFromModel("policy.siCurrencyCode"));
        return "Sum Insured (".concat(dataCurrency.currencyText, ")");
      },
      dataIndex: "coverages",
      key: "coverages",
      className: "amount-right",
      render: function render(col, record) {
        var coverages = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(record, "coverages", []);

        var sum = 0;
        (coverages || []).forEach(function (item) {
          if (item.selected === "Y") {
            sum += parseFloat(item.aggregatedSi) || 0;
          }
        });
        sum = sum.toFixed(0);
        return _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].toThousands(sum);
      }
    }, {
      className: "amount-right",
      title: function title() {
        var dataCurr = _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].formatCurrencyInfo("", _this.getValueFromModel("policy.premCurrencyCode"));
        return "Premium (".concat(dataCurr.currencyText, ")");
      },
      dataIndex: "premium.lumpsum.gwp",
      key: "premium",
      render: function render(col, record) {
        var code = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(record, "location.postalCode", "");

        var breakDown = _this.getValueFromModel("policy.policyPremium.insuredPremiums") || [];
        var locationPremium = breakDown.find(function (item) {
          return item.insuredKey === code;
        });
        var premium = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(locationPremium, "gwp", 0) || 0;
        var data = _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].formatCurrencyInfo(premium, _this.getValueFromModel("policy.premCurrencyCode"));
        return "".concat(data.amount);
      }
    }, {
      title: "Actions",
      align: "center",
      render: function render(col, record, index) {
        return _this.renderOperColumn(record, index);
      }
    }];

    _this.afterUploadPolicyDoc = function () {
      var newCart = _this.props.mergePolicyToServiceModel();

      _common__WEBPACK_IMPORTED_MODULE_14__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_14__["Apis"].CART_MERGE, newCart).then(function (response) {
        _desk_component_spin__WEBPACK_IMPORTED_MODULE_27__["default"].spinClose();

        _this.props.mergePolicyToUIModel(response.body.respData || {});

        if (response.body.respCode !== "0000") return;
      }).catch(function (error) {
        console.error(error);
        _desk_component_spin__WEBPACK_IMPORTED_MODULE_27__["default"].spinClose();
      });
    };

    _this.handleMemberView = function (record) {
      _this.setValueToModel(record, "member.ext");

      _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_20__["default"].create({
        Component: function Component(_ref) {
          var onCancel = _ref.onCancel,
              onOk = _ref.onOk;
          return react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_quote_SAIC_iar_component_location_view__WEBPACK_IMPORTED_MODULE_21__["LocationView"], {
            onCancel: onCancel,
            model: _this.props.model,
            getMasterTableItemText: _this.getMasterTableItemText.bind(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this)),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 209
            },
            __self: this
          });
        }
      });
    };

    _this.initQueryParams = function () {
      var initParams = {
        policyId: _this.getValueFromModel("policy.policyId")
      };
      return initParams;
    };

    _this.loadCodeTables =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var codeTables, arr;
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              codeTables = _this.state.codeTables;
              arr = [];
              ["nationality"].forEach(function (item) {
                arr.push(new Promise(function (resolve, reject) {
                  _common__WEBPACK_IMPORTED_MODULE_14__["Ajax"].get("/mastertable", {
                    itntCode: _this.getValueFromModel(_this.getProps("itntCode", "policy")),
                    productCode: _this.getValueFromModel(_this.getProps("productCode", "policy")),
                    productVersion: _this.getValueFromModel(_this.getProps("productVersion", "policy")),
                    tableName: item
                  }, {}).then(function (response) {
                    var respData = response.body.respData;
                    codeTables[item] = respData.items || [];
                    resolve();
                  }).catch(function (error) {});
                }));
              });
              Promise.all(arr).then(function (values) {
                _this.setState({
                  codeTables: codeTables
                });
              }, function (reason) {
                console.log(reason);
              });

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    _this.ajaxDelete = function (index) {
      var locations = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(_this.props.model, "policy.ext.locations");

      var newLocations = locations.filter(function (item, i) {
        return i !== index;
      });

      _this.setValueToModel(newLocations, "policy.ext.locations");

      var cartModel = _this.props.mergePolicyToServiceModel();

      _common__WEBPACK_IMPORTED_MODULE_14__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_14__["Apis"].CART_MERGE, cartModel, {
        loading: true
      }).then(function (res) {
        var newCart = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(res, "body.respData", {}) || {};

        if (newCart) {
          _this.props.mergePolicyToUIModel(newCart);
        }
      });
    };

    _this.calcGst = function () {
      return _this.getValueFromModel(_this.getProp("policyPremium.lumpsum.gstVat"));
    };

    _this.calcAmount = function () {
      return _this.getValueFromModel(_this.getProp("policyPremium.lumpsum.ar"));
    };

    _this.calcCommissionGst = function () {
      var commissionP = _this.getValueFromModel(_this.generatePropName("commissionAmount", "feeSummary"));

      if (_common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isNum(commissionP)) {
        return number_precision__WEBPACK_IMPORTED_MODULE_11___default.a.times(commissionP, 0.07);
      }

      return 0;
    };

    _this.calcTotalCommission = function () {
      var commissionP = _this.getValueFromModel(_this.generatePropName("commissionAmount", "feeSummary"));

      if (_common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isNum(commissionP)) {
        var commissionGst = _this.calcCommissionGst();

        return number_precision__WEBPACK_IMPORTED_MODULE_11___default.a.plus(commissionP, commissionGst);
      }

      return 0;
    };

    _this.calcNetPremium = function () {
      var totalPremiun = _this.calcAmount();

      var totalCommission = _this.calcTotalCommission();

      if (_common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isNum(totalPremiun)) {
        return number_precision__WEBPACK_IMPORTED_MODULE_11___default.a.minus(totalPremiun, totalCommission);
      }

      return 0;
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Location, [{
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.loadCodeTables();

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "getIsCoinsuranceEnabled",
    value: function getIsCoinsuranceEnabled() {
      return this.getValueFromModel("policy.isCoinsuranceEnabled");
    }
  }, {
    key: "getIsArrangementEnabled",
    // renders
    value: function getIsArrangementEnabled() {
      var isInstallmentEnabled = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(this.props.model, "policy.isInstallmentEnabled") || false;
      return isInstallmentEnabled;
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      var model = this.props.model;
      return react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_23__["default"], {
        lineList: _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_13__["list"],
        premiumId: "cartPremium.totalPremium",
        model: model,
        dataFixed: "policy",
        activeStep: "Locations",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      });
    }
  }, {
    key: "renderLocationTable",
    value: function renderLocationTable() {
      var locations = this.getValueFromModel("policy.ext.locations") || [];
      return react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_component_render_location_table__WEBPACK_IMPORTED_MODULE_17__["default"], {
        locations: locations,
        columns: this.columns,
        handleMember: this.handleMember,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      });
    }
  }, {
    key: "renderFeeSummary",
    value: function renderFeeSummary() {
      var currency = this.getValueFromModel("policy.currencySymbol");
      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_component_render_fee_summary__WEBPACK_IMPORTED_MODULE_18__["default"], {
        form: form,
        model: model,
        currency: currency,
        that: this,
        premiumId: "policyPremium",
        generatePropName: this.generatePropName,
        getProp: this.getProp,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 130
        },
        __self: this
      });
    }
  }, {
    key: "renderCoInsurance",
    value: function renderCoInsurance() {
      var _this$props2 = this.props,
          form = _this$props2.form,
          model = _this$props2.model;
      return react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 146
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_quote_SAIC_iar_component_co_insurance__WEBPACK_IMPORTED_MODULE_24__["default"], {
        form: form,
        model: model,
        that: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 147
        },
        __self: this
      }));
    }
  }, {
    key: "renderPaymentArrangement",
    value: function renderPaymentArrangement() {
      var defaultLayout = {
        labelCol: {
          xs: {
            span: 8
          },
          sm: {
            span: 6
          }
        },
        wrapperCol: {
          xs: {
            span: 8
          },
          sm: {
            span: 10
          }
        }
      };
      var _this$props3 = this.props,
          form = _this$props3.form,
          model = _this$props3.model;
      return react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_quote_components_panyment_arramgement_payment_arrangement__WEBPACK_IMPORTED_MODULE_25__["default"], {
        form: form,
        model: model,
        layout: defaultLayout,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 170
        },
        __self: this
      });
    }
  }, {
    key: "renderOutput",
    value: function renderOutput() {
      return react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_component_NPanelShowOutputs__WEBPACK_IMPORTED_MODULE_26__["default"], {
        isEndo: false,
        dataFixed: "policy",
        key: "5",
        that: this,
        autoShow: true,
        isPolicyDoc: true,
        beforeDownload: true,
        afterUploadPolicyDoc: this.afterUploadPolicyDoc,
        model: this.props.model,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 178
        },
        __self: this
      });
    } // methods

  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      if (dataIdPrefix) return "policy.ext.".concat(dataIdPrefix, ".").concat(propName);
      return "policy.ext.".concat(propName);
    }
  }, {
    key: "getProps",
    value: function getProps(propName, dataFixed) {
      if (dataFixed) {
        return "".concat(dataFixed, ".").concat(propName);
      } else {
        return propName;
      }
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      if (lodash__WEBPACK_IMPORTED_MODULE_10___default.a.isEmpty(propName)) {
        return "policy";
      }

      return "policy.".concat(propName);
    }
  }, {
    key: "handleNext",
    value: function handleNext() {
      var _this2 = this;

      this.props.form.validateFields(function (err, fieldsValue) {
        if (err) {
          return;
        }

        var totalP = _this2.getValueFromModel(_this2.getProp("policyPremium.lumpsum.gwp"));

        var gst = _this2.calcGst();

        var amount = _this2.calcAmount();

        var commissionGst = _this2.calcCommissionGst();

        var totalCommission = _this2.calcTotalCommission();

        var netPremium = _this2.calcNetPremium();

        var newCart = _this2.props.mergePolicyToServiceModel();

        lodash__WEBPACK_IMPORTED_MODULE_10___default.a.set(newCart, "policy.ext.feeSummary.premium", totalP);

        lodash__WEBPACK_IMPORTED_MODULE_10___default.a.set(newCart, "policy.ext.feeSummary.gst", gst);

        lodash__WEBPACK_IMPORTED_MODULE_10___default.a.set(newCart, "policy.ext.feeSummary.totalPremium", amount);

        lodash__WEBPACK_IMPORTED_MODULE_10___default.a.set(newCart, "policy.ext.feeSummary.commissionGst", commissionGst);

        lodash__WEBPACK_IMPORTED_MODULE_10___default.a.set(newCart, "policy.ext.feeSummary.totalCommission", totalCommission);

        lodash__WEBPACK_IMPORTED_MODULE_10___default.a.set(newCart, "policy.ext.feeSummary.netPremium", netPremium);

        lodash__WEBPACK_IMPORTED_MODULE_10___default.a.set(newCart, "policy.ext._ui.step", _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_15__["AllSteps"].CLAUSE);

        _common__WEBPACK_IMPORTED_MODULE_14__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_14__["Apis"].CART_MERGE, newCart, {
          loading: true
        }).then(function (response) {
          _this2.props.mergePolicyToUIModel(response.body.respData || {});

          if (response.body.respCode !== "0000") return;
          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_16__["pageTo"])(_this2, _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_15__["AllSteps"].CLAUSE);
        }).catch(function (error) {
          return console.error(error);
        });
      });
    }
  }]);

  return Location;
}(_desk_quote_SAIC_iar_basic_location__WEBPACK_IMPORTED_MODULE_12__["BasicLocation"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvbG9jYXRpb24udHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvbG9jYXRpb24udHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCBOUCBmcm9tIFwibnVtYmVyLXByZWNpc2lvblwiO1xuXG5pbXBvcnQgeyBCYXNpY0xvY2F0aW9uIH0gZnJvbSBcIkBkZXNrL3F1b3RlL1NBSUMvaWFyL2Jhc2ljLWxvY2F0aW9uXCI7XG5pbXBvcnQgeyBsaXN0IH0gZnJvbSBcIkBkZXNrL3F1b3RlL1NBSUMvaWFyL2luZGV4XCI7XG5pbXBvcnQgeyBBamF4LCBBcGlzLCBSdWxlcywgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgQWxsU3RlcHMgfSBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9hbGwtc3RlcHNcIjtcbmltcG9ydCB7IHBhZ2VUbyB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvd2l6YXJkXCI7XG5pbXBvcnQgUmVuZGVyTG9jYXRpb25UYWJsZSBmcm9tIFwiLi9jb21wb25lbnQvcmVuZGVyLWxvY2F0aW9uLXRhYmxlXCI7XG5pbXBvcnQgUmVuZGVyRmVlU3VtbWFyeSBmcm9tIFwiLi9jb21wb25lbnQvcmVuZGVyLWZlZS1zdW1tYXJ5XCI7XG5pbXBvcnQgeyBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCBNYXNrIGZyb20gXCJAZGVzay1jb21wb25lbnQvY3JlYXRlLWRpYWxvZ1wiO1xuaW1wb3J0IHsgTG9jYXRpb25WaWV3IH0gZnJvbSBcIkBkZXNrL3F1b3RlL1NBSUMvaWFyL2NvbXBvbmVudC9sb2NhdGlvbi12aWV3XCI7XG5pbXBvcnQgeyBTaG93QWRkcmVzcyB9IGZyb20gXCJAZGVzay1jb21wb25lbnRcIjtcbmltcG9ydCBTaWRlSW5mb1BocyBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9waHMvY29tcG9uZW50cy9zaWRlLWluZm9cIjtcbmltcG9ydCBDb0luc3VyYW5jZSBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L2NvLWluc3VyYW5jZVwiO1xuaW1wb3J0IFBheW1lbnRBcnJhbmdlbWVudCBmcm9tIFwiQGRlc2svcXVvdGUvY29tcG9uZW50cy9wYW55bWVudC1hcnJhbWdlbWVudC9wYXltZW50LWFycmFuZ2VtZW50XCI7XG5pbXBvcnQgTlBhbmVsU2hvd091dHB1dHMgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9OUGFuZWxTaG93T3V0cHV0c1wiO1xuaW1wb3J0IHNwaW4gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9zcGluXCI7XG5cbmV4cG9ydCBjb25zdCBSaWdodERpdiA9IFN0eWxlZC5kaXZgXG4gIC5hbnQtaW5wdXQtbnVtYmVyLWlucHV0IHsgXG4gICAgdGV4dC1hbGlnbjogcmlnaHRcbiAgfVxuYDtcblxuY2xhc3MgTG9jYXRpb24gZXh0ZW5kcyBCYXNpY0xvY2F0aW9uPGFueSwgYW55LCBhbnk+IHtcbiAgYXN5bmMgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgYXdhaXQgdGhpcy5sb2FkQ29kZVRhYmxlcygpO1xuICB9XG5cbiAgZ2V0SXNDb2luc3VyYW5jZUVuYWJsZWQoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJwb2xpY3kuaXNDb2luc3VyYW5jZUVuYWJsZWRcIik7XG4gIH1cblxuXG4gIGNvbHVtbnMgPSBbXG4gICAge1xuICAgICAgdGl0bGU6IFwiUG9zdGFsIENvZGVcIixcbiAgICAgIGRhdGFJbmRleDogXCJsb2NhdGlvbi5wb3N0YWxDb2RlXCIsXG4gICAgICBrZXk6IFwibG9jYXRpb24ucG9zdGFsQ29kZVwiLFxuICAgIH0sIHtcbiAgICAgIHRpdGxlOiBcIkFkZHJlc3NcIixcbiAgICAgIGRhdGFJbmRleDogXCJsb2NhdGlvblwiLFxuICAgICAga2V5OiBcImxvY2F0aW9uXCIsXG4gICAgICByZW5kZXI6IChjb2w6IGFueSwgcmVjb3JkOiBhbnksIGluZGV4OiBudW1iZXIsIHJvd0luZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgY29uc3Qge1xuICAgICAgICAgIG1vZGVsLFxuICAgICAgICB9ID0gdGhpcy5wcm9wcztcblxuICAgICAgICByZXR1cm4gPFNob3dBZGRyZXNzIG1vZGVsPXttb2RlbH0gYWRkcmVzc0ZpeGVkPXtgcG9saWN5LmV4dC5sb2NhdGlvbnMuJHtpbmRleH0ubG9jYXRpb25gfS8+O1xuXG4gICAgICB9LFxuICAgIH0sIHtcbiAgICAgIHRpdGxlOiAoKSA9PiB7XG4gICAgICAgIGNvbnN0IGRhdGFDdXJyZW5jeSA9IFV0aWxzLmZvcm1hdEN1cnJlbmN5SW5mbyhcIlwiLCB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicG9saWN5LnNpQ3VycmVuY3lDb2RlXCIpKTtcbiAgICAgICAgcmV0dXJuIGBTdW0gSW5zdXJlZCAoJHtkYXRhQ3VycmVuY3kuY3VycmVuY3lUZXh0fSlgO1xuICAgICAgfSxcbiAgICAgIGRhdGFJbmRleDogXCJjb3ZlcmFnZXNcIixcbiAgICAgIGtleTogXCJjb3ZlcmFnZXNcIixcbiAgICAgIGNsYXNzTmFtZTogXCJhbW91bnQtcmlnaHRcIixcbiAgICAgIHJlbmRlcjogKGNvbDogYW55LCByZWNvcmQ6IGFueSkgPT4ge1xuICAgICAgICBjb25zdCBjb3ZlcmFnZXMgPSBfLmdldChyZWNvcmQsIFwiY292ZXJhZ2VzXCIsIFtdKTtcbiAgICAgICAgbGV0IHN1bTogYW55ID0gMDtcbiAgICAgICAgKGNvdmVyYWdlcyB8fCBbXSkuZm9yRWFjaCgoaXRlbTogYW55KSA9PiB7XG4gICAgICAgICAgaWYgKGl0ZW0uc2VsZWN0ZWQgPT09IFwiWVwiKSB7XG4gICAgICAgICAgICBzdW0gKz0gcGFyc2VGbG9hdChpdGVtLmFnZ3JlZ2F0ZWRTaSkgfHwgMDtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBzdW0gPSBzdW0udG9GaXhlZCgwKTtcbiAgICAgICAgcmV0dXJuIFV0aWxzLnRvVGhvdXNhbmRzKHN1bSk7XG4gICAgICB9LFxuICAgIH0sIHtcbiAgICAgIGNsYXNzTmFtZTogXCJhbW91bnQtcmlnaHRcIixcbiAgICAgIHRpdGxlOiAoKSA9PiB7XG4gICAgICAgIGNvbnN0IGRhdGFDdXJyID0gVXRpbHMuZm9ybWF0Q3VycmVuY3lJbmZvKFwiXCIsIHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJwb2xpY3kucHJlbUN1cnJlbmN5Q29kZVwiKSk7XG4gICAgICAgIHJldHVybiBgUHJlbWl1bSAoJHtkYXRhQ3Vyci5jdXJyZW5jeVRleHR9KWA7XG4gICAgICB9LFxuICAgICAgZGF0YUluZGV4OiBcInByZW1pdW0ubHVtcHN1bS5nd3BcIixcbiAgICAgIGtleTogXCJwcmVtaXVtXCIsXG4gICAgICByZW5kZXI6IChjb2w6IGFueSwgcmVjb3JkOiBhbnkpID0+IHtcbiAgICAgICAgY29uc3QgY29kZSA9IF8uZ2V0KHJlY29yZCwgXCJsb2NhdGlvbi5wb3N0YWxDb2RlXCIsIFwiXCIpO1xuICAgICAgICBjb25zdCBicmVha0Rvd24gPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicG9saWN5LnBvbGljeVByZW1pdW0uaW5zdXJlZFByZW1pdW1zXCIpIHx8IFtdO1xuICAgICAgICBjb25zdCBsb2NhdGlvblByZW1pdW0gPSBicmVha0Rvd24uZmluZCgoaXRlbTogYW55KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIGl0ZW0uaW5zdXJlZEtleSA9PT0gY29kZTtcbiAgICAgICAgfSk7XG4gICAgICAgIGNvbnN0IHByZW1pdW0gPSBfLmdldChsb2NhdGlvblByZW1pdW0sIFwiZ3dwXCIsIDApIHx8IDA7XG4gICAgICAgIGNvbnN0IGRhdGEgPSBVdGlscy5mb3JtYXRDdXJyZW5jeUluZm8ocHJlbWl1bSwgdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeS5wcmVtQ3VycmVuY3lDb2RlXCIpKTtcbiAgICAgICAgcmV0dXJuIGAke2RhdGEuYW1vdW50fWA7XG4gICAgICB9LFxuICAgIH0sIHtcbiAgICAgIHRpdGxlOiBcIkFjdGlvbnNcIixcbiAgICAgIGFsaWduOiBcImNlbnRlclwiLFxuICAgICAgcmVuZGVyOiAoY29sOiBhbnksIHJlY29yZDogYW55LCBpbmRleDogbnVtYmVyKSA9PiB0aGlzLnJlbmRlck9wZXJDb2x1bW4ocmVjb3JkLCBpbmRleCksXG4gICAgfV07XG5cbiAgLy8gcmVuZGVyc1xuICBnZXRJc0FycmFuZ2VtZW50RW5hYmxlZCgpOiBib29sZWFuIHtcbiAgICBjb25zdCBpc0luc3RhbGxtZW50RW5hYmxlZCA9IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIFwicG9saWN5LmlzSW5zdGFsbG1lbnRFbmFibGVkXCIpIHx8IGZhbHNlO1xuICAgIHJldHVybiBpc0luc3RhbGxtZW50RW5hYmxlZDtcbiAgfVxuXG4gIHJlbmRlclNpZGVJbmZvKCk6IGFueSB7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPFNpZGVJbmZvUGhzXG4gICAgICBsaW5lTGlzdD17bGlzdH1cbiAgICAgIHByZW1pdW1JZD17XCJjYXJ0UHJlbWl1bS50b3RhbFByZW1pdW1cIn1cbiAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgIGRhdGFGaXhlZD17XCJwb2xpY3lcIn1cbiAgICAgIGFjdGl2ZVN0ZXA9e1wiTG9jYXRpb25zXCJ9Lz47XG4gIH1cblxuICByZW5kZXJMb2NhdGlvblRhYmxlKCk6IGFueSB7XG4gICAgY29uc3QgbG9jYXRpb25zID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeS5leHQubG9jYXRpb25zXCIpIHx8IFtdO1xuICAgIHJldHVybiA8UmVuZGVyTG9jYXRpb25UYWJsZVxuICAgICAgbG9jYXRpb25zPXtsb2NhdGlvbnN9XG4gICAgICBjb2x1bW5zPXt0aGlzLmNvbHVtbnN9XG4gICAgICBoYW5kbGVNZW1iZXI9e3RoaXMuaGFuZGxlTWVtYmVyfVxuICAgIC8+O1xuICB9XG5cbiAgcmVuZGVyRmVlU3VtbWFyeSgpOiBhbnkge1xuICAgIGNvbnN0IGN1cnJlbmN5ID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeS5jdXJyZW5jeVN5bWJvbFwiKTtcbiAgICBjb25zdCB7XG4gICAgICBmb3JtLFxuICAgICAgbW9kZWwsXG4gICAgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIDxSZW5kZXJGZWVTdW1tYXJ5XG4gICAgICBmb3JtPXtmb3JtfVxuICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgY3VycmVuY3k9e2N1cnJlbmN5fVxuICAgICAgdGhhdD17dGhpc31cbiAgICAgIHByZW1pdW1JZD17XCJwb2xpY3lQcmVtaXVtXCJ9XG4gICAgICBnZW5lcmF0ZVByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWV9XG4gICAgICBnZXRQcm9wPXt0aGlzLmdldFByb3B9XG4gICAgLz47XG4gIH1cblxuICByZW5kZXJDb0luc3VyYW5jZSgpIHtcbiAgICBjb25zdCB7XG4gICAgICBmb3JtLFxuICAgICAgbW9kZWwsXG4gICAgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIDxkaXY+XG4gICAgICA8Q29JbnN1cmFuY2VcbiAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICB0aGF0PXt0aGlzfVxuICAgICAgLz5cbiAgICA8L2Rpdj47XG4gIH1cblxuICByZW5kZXJQYXltZW50QXJyYW5nZW1lbnQoKTogYW55IHtcbiAgICBjb25zdCBkZWZhdWx0TGF5b3V0ID0ge1xuICAgICAgbGFiZWxDb2w6IHtcbiAgICAgICAgeHM6IHsgc3BhbjogOCB9LFxuICAgICAgICBzbTogeyBzcGFuOiA2IH0sXG4gICAgICB9LFxuICAgICAgd3JhcHBlckNvbDoge1xuICAgICAgICB4czogeyBzcGFuOiA4IH0sXG4gICAgICAgIHNtOiB7IHNwYW46IDEwIH0sXG4gICAgICB9LFxuICAgIH07XG4gICAgY29uc3Qge1xuICAgICAgZm9ybSxcbiAgICAgIG1vZGVsLFxuICAgIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiA8UGF5bWVudEFycmFuZ2VtZW50XG4gICAgICBmb3JtPXtmb3JtfVxuICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgbGF5b3V0PXtkZWZhdWx0TGF5b3V0fVxuICAgIC8+O1xuICB9XG5cbiAgcmVuZGVyT3V0cHV0KCkge1xuICAgIHJldHVybiA8TlBhbmVsU2hvd091dHB1dHNcbiAgICAgIGlzRW5kbz17ZmFsc2V9XG4gICAgICBkYXRhRml4ZWQ9e1wicG9saWN5XCJ9XG4gICAgICBrZXk9XCI1XCJcbiAgICAgIHRoYXQ9e3RoaXN9XG4gICAgICBhdXRvU2hvdz17dHJ1ZX1cbiAgICAgIGlzUG9saWN5RG9jPXt0cnVlfVxuICAgICAgYmVmb3JlRG93bmxvYWQ9e3RydWV9XG4gICAgICBhZnRlclVwbG9hZFBvbGljeURvYz17dGhpcy5hZnRlclVwbG9hZFBvbGljeURvY31cbiAgICAgIG1vZGVsPXt0aGlzLnByb3BzLm1vZGVsfS8+O1xuICB9XG5cbiAgLy8gbWV0aG9kc1xuICBhZnRlclVwbG9hZFBvbGljeURvYyA9ICgpID0+IHtcbiAgICBjb25zdCBuZXdDYXJ0ID0gdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvU2VydmljZU1vZGVsKCk7XG5cbiAgICBBamF4LnBvc3QoQXBpcy5DQVJUX01FUkdFLCBuZXdDYXJ0KVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICBzcGluLnNwaW5DbG9zZSgpO1xuICAgICAgICB0aGlzLnByb3BzLm1lcmdlUG9saWN5VG9VSU1vZGVsKHJlc3BvbnNlLmJvZHkucmVzcERhdGEgfHwge30pO1xuICAgICAgICBpZiAocmVzcG9uc2UuYm9keS5yZXNwQ29kZSAhPT0gXCIwMDAwXCIpIHJldHVybjtcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgICAgc3Bpbi5zcGluQ2xvc2UoKTtcbiAgICAgIH0pO1xuICB9O1xuXG4gIGhhbmRsZU1lbWJlclZpZXcgPSAocmVjb3JkOiBhbnkpID0+IHtcbiAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChyZWNvcmQsIFwibWVtYmVyLmV4dFwiKTtcbiAgICBNYXNrLmNyZWF0ZSh7XG4gICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsLCBvbk9rIH06IGFueSkgPT4gPExvY2F0aW9uVmlld1xuICAgICAgICBvbkNhbmNlbD17b25DYW5jZWx9XG4gICAgICAgIG1vZGVsPXt0aGlzLnByb3BzLm1vZGVsfVxuICAgICAgICBnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0PXt0aGlzLmdldE1hc3RlclRhYmxlSXRlbVRleHQuYmluZCh0aGlzKX1cbiAgICAgIC8+LFxuICAgIH0pO1xuICB9O1xuXG4gIGdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkUHJlZml4Pzogc3RyaW5nKTogc3RyaW5nIHtcbiAgICBpZiAoZGF0YUlkUHJlZml4KSByZXR1cm4gYHBvbGljeS5leHQuJHtkYXRhSWRQcmVmaXh9LiR7cHJvcE5hbWV9YDtcbiAgICByZXR1cm4gYHBvbGljeS5leHQuJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgZ2V0UHJvcHMocHJvcE5hbWU6IGFueSwgZGF0YUZpeGVkPzogYW55KSB7XG4gICAgaWYgKGRhdGFGaXhlZCkge1xuICAgICAgcmV0dXJuIGAke2RhdGFGaXhlZH0uJHtwcm9wTmFtZX1gO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gcHJvcE5hbWU7XG4gICAgfVxuICB9XG5cbiAgZ2V0UHJvcChwcm9wTmFtZT86IHN0cmluZykge1xuICAgIGlmIChfLmlzRW1wdHkocHJvcE5hbWUpKSB7XG4gICAgICByZXR1cm4gXCJwb2xpY3lcIjtcbiAgICB9XG4gICAgcmV0dXJuIGBwb2xpY3kuJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgaW5pdFF1ZXJ5UGFyYW1zID0gKCkgPT4ge1xuICAgIGNvbnN0IGluaXRQYXJhbXMgPSB7IHBvbGljeUlkOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicG9saWN5LnBvbGljeUlkXCIpIH07XG5cbiAgICByZXR1cm4gaW5pdFBhcmFtcztcbiAgfTtcblxuICBsb2FkQ29kZVRhYmxlcyA9IGFzeW5jICgpID0+IHtcbiAgICBsZXQgeyBjb2RlVGFibGVzIH0gPSB0aGlzLnN0YXRlO1xuICAgIGxldCBhcnI6IGFueSA9IFtdO1xuICAgIFtcbiAgICAgIFwibmF0aW9uYWxpdHlcIixcbiAgICBdLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICBhcnIucHVzaChcbiAgICAgICAgbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgIEFqYXguZ2V0KFxuICAgICAgICAgICAgYC9tYXN0ZXJ0YWJsZWAsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGl0bnRDb2RlOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoXCJpdG50Q29kZVwiLCBcInBvbGljeVwiKSksXG4gICAgICAgICAgICAgIHByb2R1Y3RDb2RlOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoXCJwcm9kdWN0Q29kZVwiLCBcInBvbGljeVwiKSksXG4gICAgICAgICAgICAgIHByb2R1Y3RWZXJzaW9uOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoXCJwcm9kdWN0VmVyc2lvblwiLCBcInBvbGljeVwiKSksXG4gICAgICAgICAgICAgIHRhYmxlTmFtZTogaXRlbSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7fSxcbiAgICAgICAgICApXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5O1xuICAgICAgICAgICAgICBjb2RlVGFibGVzW2l0ZW1dID0gcmVzcERhdGEuaXRlbXMgfHwgW107XG4gICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pLFxuICAgICAgKTtcbiAgICB9KTtcbiAgICBQcm9taXNlLmFsbChhcnIpLnRoZW4oXG4gICAgICB2YWx1ZXMgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgY29kZVRhYmxlczogY29kZVRhYmxlcyB9KTtcbiAgICAgIH0sXG4gICAgICByZWFzb24gPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhyZWFzb24pO1xuICAgICAgfSxcbiAgICApO1xuICB9O1xuXG4gIGFqYXhEZWxldGUgPSAoaW5kZXg6IGFueSkgPT4ge1xuICAgIGNvbnN0IGxvY2F0aW9uczogYW55ID0gXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgXCJwb2xpY3kuZXh0LmxvY2F0aW9uc1wiKTtcbiAgICBjb25zdCBuZXdMb2NhdGlvbnMgPSBsb2NhdGlvbnMuZmlsdGVyKChpdGVtOiBhbnksIGk6IG51bWJlcikgPT4ge1xuICAgICAgcmV0dXJuIGkgIT09IGluZGV4O1xuICAgIH0pO1xuICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKG5ld0xvY2F0aW9ucywgXCJwb2xpY3kuZXh0LmxvY2F0aW9uc1wiKTtcbiAgICBjb25zdCBjYXJ0TW9kZWwgPSB0aGlzLnByb3BzLm1lcmdlUG9saWN5VG9TZXJ2aWNlTW9kZWwoKTtcblxuICAgIEFqYXgucG9zdChBcGlzLkNBUlRfTUVSR0UsIGNhcnRNb2RlbCwgeyBsb2FkaW5nOiB0cnVlIH0pLnRoZW4oKHJlcykgPT4ge1xuICAgICAgY29uc3QgbmV3Q2FydCA9IF8uZ2V0KHJlcywgXCJib2R5LnJlc3BEYXRhXCIsIHt9KSB8fCB7fTtcbiAgICAgIGlmIChuZXdDYXJ0KSB7XG4gICAgICAgIHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1VJTW9kZWwobmV3Q2FydCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH07XG5cbiAgaGFuZGxlTmV4dCgpIHtcbiAgICB0aGlzLnByb3BzLmZvcm0udmFsaWRhdGVGaWVsZHMoKGVycjogYW55LCBmaWVsZHNWYWx1ZTogYW55KSA9PiB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGNvbnN0IHRvdGFsUCA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wKFwicG9saWN5UHJlbWl1bS5sdW1wc3VtLmd3cFwiKSk7XG4gICAgICBjb25zdCBnc3QgPSB0aGlzLmNhbGNHc3QoKTtcbiAgICAgIGNvbnN0IGFtb3VudCA9IHRoaXMuY2FsY0Ftb3VudCgpO1xuICAgICAgY29uc3QgY29tbWlzc2lvbkdzdCA9IHRoaXMuY2FsY0NvbW1pc3Npb25Hc3QoKTtcbiAgICAgIGNvbnN0IHRvdGFsQ29tbWlzc2lvbiA9IHRoaXMuY2FsY1RvdGFsQ29tbWlzc2lvbigpO1xuICAgICAgY29uc3QgbmV0UHJlbWl1bSA9IHRoaXMuY2FsY05ldFByZW1pdW0oKTtcbiAgICAgIGNvbnN0IG5ld0NhcnQgPSB0aGlzLnByb3BzLm1lcmdlUG9saWN5VG9TZXJ2aWNlTW9kZWwoKTtcbiAgICAgIF8uc2V0KG5ld0NhcnQsIFwicG9saWN5LmV4dC5mZWVTdW1tYXJ5LnByZW1pdW1cIiwgdG90YWxQKTtcbiAgICAgIF8uc2V0KG5ld0NhcnQsIFwicG9saWN5LmV4dC5mZWVTdW1tYXJ5LmdzdFwiLCBnc3QpO1xuICAgICAgXy5zZXQobmV3Q2FydCwgXCJwb2xpY3kuZXh0LmZlZVN1bW1hcnkudG90YWxQcmVtaXVtXCIsIGFtb3VudCk7XG4gICAgICBfLnNldChuZXdDYXJ0LCBcInBvbGljeS5leHQuZmVlU3VtbWFyeS5jb21taXNzaW9uR3N0XCIsIGNvbW1pc3Npb25Hc3QpO1xuICAgICAgXy5zZXQobmV3Q2FydCwgXCJwb2xpY3kuZXh0LmZlZVN1bW1hcnkudG90YWxDb21taXNzaW9uXCIsIHRvdGFsQ29tbWlzc2lvbik7XG4gICAgICBfLnNldChuZXdDYXJ0LCBcInBvbGljeS5leHQuZmVlU3VtbWFyeS5uZXRQcmVtaXVtXCIsIG5ldFByZW1pdW0pO1xuICAgICAgXy5zZXQobmV3Q2FydCwgXCJwb2xpY3kuZXh0Ll91aS5zdGVwXCIsIEFsbFN0ZXBzLkNMQVVTRSk7XG4gICAgICBBamF4LnBvc3QoQXBpcy5DQVJUX01FUkdFLCBuZXdDYXJ0LCB7IGxvYWRpbmc6IHRydWUgfSlcbiAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgIHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1VJTW9kZWwocmVzcG9uc2UuYm9keS5yZXNwRGF0YSB8fCB7fSk7XG4gICAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkucmVzcENvZGUgIT09IFwiMDAwMFwiKSByZXR1cm47XG4gICAgICAgICAgcGFnZVRvKHRoaXMsIEFsbFN0ZXBzLkNMQVVTRSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSk7XG4gICAgfSk7XG4gIH1cblxuICBjYWxjR3N0ID0gKCkgPT4ge1xuICAgIHJldHVybiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcChcInBvbGljeVByZW1pdW0ubHVtcHN1bS5nc3RWYXRcIikpO1xuICB9O1xuXG4gIGNhbGNBbW91bnQgPSAoKSA9PiB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wKFwicG9saWN5UHJlbWl1bS5sdW1wc3VtLmFyXCIpKTtcbiAgfTtcblxuICBjYWxjQ29tbWlzc2lvbkdzdCA9ICgpID0+IHtcbiAgICBjb25zdCBjb21taXNzaW9uUCA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY29tbWlzc2lvbkFtb3VudFwiLCBcImZlZVN1bW1hcnlcIikpO1xuICAgIGlmIChSdWxlcy5pc051bShjb21taXNzaW9uUCkpIHtcbiAgICAgIHJldHVybiBOUC50aW1lcyhjb21taXNzaW9uUCwgMC4wNyk7XG4gICAgfVxuICAgIHJldHVybiAwO1xuICB9O1xuXG4gIGNhbGNUb3RhbENvbW1pc3Npb24gPSAoKSA9PiB7XG4gICAgY29uc3QgY29tbWlzc2lvblAgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImNvbW1pc3Npb25BbW91bnRcIiwgXCJmZWVTdW1tYXJ5XCIpKTtcbiAgICBpZiAoUnVsZXMuaXNOdW0oY29tbWlzc2lvblApKSB7XG4gICAgICBjb25zdCBjb21taXNzaW9uR3N0ID0gdGhpcy5jYWxjQ29tbWlzc2lvbkdzdCgpO1xuICAgICAgcmV0dXJuIE5QLnBsdXMoY29tbWlzc2lvblAsIGNvbW1pc3Npb25Hc3QpO1xuICAgIH1cbiAgICByZXR1cm4gMDtcbiAgfTtcblxuICBjYWxjTmV0UHJlbWl1bSA9ICgpID0+IHtcbiAgICBjb25zdCB0b3RhbFByZW1pdW4gPSB0aGlzLmNhbGNBbW91bnQoKTtcbiAgICBjb25zdCB0b3RhbENvbW1pc3Npb24gPSB0aGlzLmNhbGNUb3RhbENvbW1pc3Npb24oKTtcbiAgICBpZiAoUnVsZXMuaXNOdW0odG90YWxQcmVtaXVuKSkge1xuICAgICAgcmV0dXJuIE5QLm1pbnVzKHRvdGFsUHJlbWl1biwgdG90YWxDb21taXNzaW9uKTtcbiAgICB9XG4gICAgcmV0dXJuIDA7XG4gIH07XG59XG5cbmV4cG9ydCB7IExvY2F0aW9uIH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBS0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBVUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQVhBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFsQkE7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBakJBO0FBbUJBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFIQTtBQUNBO0FBaUdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBT0E7QUFDQTtBQXFCQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFFQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFRQTtBQUVBO0FBQ0E7QUFDQTtBQUdBO0FBRUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBbkNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFxQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUF4VUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQTtBQUNBO0FBQ0E7OztBQStEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBOzs7QUFFQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBREE7QUFBQTtBQUFBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7OztBQTRCQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUE4REE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7O0FBdlNBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/location.tsx
