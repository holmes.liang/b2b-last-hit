(function (global, factory) {
   true ? module.exports = factory() : undefined;
})(this, function () {
  'use strict';

  function arrayTreeFilter(data, filterFn, options) {
    options = options || {};
    options.childrenKeyName = options.childrenKeyName || "children";
    var children = data || [];
    var result = [];
    var level = 0;

    do {
      var foundItem = children.filter(function (item) {
        return filterFn(item, level);
      })[0];

      if (!foundItem) {
        break;
      }

      result.push(foundItem);
      children = foundItem[options.childrenKeyName] || [];
      level += 1;
    } while (children.length > 0);

    return result;
  }

  return arrayTreeFilter;
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYXJyYXktdHJlZS1maWx0ZXIvbGliL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvYXJyYXktdHJlZS1maWx0ZXIvbGliL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiAoZ2xvYmFsLCBmYWN0b3J5KSB7XG5cdHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyA/IG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpIDpcblx0dHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kID8gZGVmaW5lKGZhY3RvcnkpIDpcblx0KGdsb2JhbC5hcnJheVRyZWVGaWx0ZXIgPSBmYWN0b3J5KCkpO1xufSh0aGlzLCAoZnVuY3Rpb24gKCkgeyAndXNlIHN0cmljdCc7XG5cbmZ1bmN0aW9uIGFycmF5VHJlZUZpbHRlcihkYXRhLCBmaWx0ZXJGbiwgb3B0aW9ucykge1xuICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICAgIG9wdGlvbnMuY2hpbGRyZW5LZXlOYW1lID0gb3B0aW9ucy5jaGlsZHJlbktleU5hbWUgfHwgXCJjaGlsZHJlblwiO1xuICAgIHZhciBjaGlsZHJlbiA9IGRhdGEgfHwgW107XG4gICAgdmFyIHJlc3VsdCA9IFtdO1xuICAgIHZhciBsZXZlbCA9IDA7XG4gICAgZG8ge1xuICAgICAgICB2YXIgZm91bmRJdGVtID0gY2hpbGRyZW4uZmlsdGVyKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICByZXR1cm4gZmlsdGVyRm4oaXRlbSwgbGV2ZWwpO1xuICAgICAgICB9KVswXTtcbiAgICAgICAgaWYgKCFmb3VuZEl0ZW0pIHtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIHJlc3VsdC5wdXNoKGZvdW5kSXRlbSk7XG4gICAgICAgIGNoaWxkcmVuID0gZm91bmRJdGVtW29wdGlvbnMuY2hpbGRyZW5LZXlOYW1lXSB8fCBbXTtcbiAgICAgICAgbGV2ZWwgKz0gMTtcbiAgICB9IHdoaWxlIChjaGlsZHJlbi5sZW5ndGggPiAwKTtcbiAgICByZXR1cm4gcmVzdWx0O1xufVxuXG5yZXR1cm4gYXJyYXlUcmVlRmlsdGVyO1xuXG59KSkpO1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBR0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/array-tree-filter/lib/index.js
