/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule splitBlockInContentState
 * @format
 * 
 */


var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var generateRandomKey = __webpack_require__(/*! ./generateRandomKey */ "./node_modules/draft-js/lib/generateRandomKey.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var List = Immutable.List,
    Map = Immutable.Map;

var transformBlock = function transformBlock(key, blockMap, func) {
  if (!key) {
    return;
  }

  var block = blockMap.get(key);

  if (!block) {
    return;
  }

  blockMap.set(key, func(block));
};

var updateBlockMapLinks = function updateBlockMapLinks(blockMap, originalBlock, belowBlock) {
  return blockMap.withMutations(function (blocks) {
    var originalBlockKey = originalBlock.getKey();
    var belowBlockKey = belowBlock.getKey(); // update block parent

    transformBlock(originalBlock.getParentKey(), blocks, function (block) {
      var parentChildrenList = block.getChildKeys();
      var insertionIndex = parentChildrenList.indexOf(originalBlockKey) + 1;
      var newChildrenArray = parentChildrenList.toArray();
      newChildrenArray.splice(insertionIndex, 0, belowBlockKey);
      return block.merge({
        children: List(newChildrenArray)
      });
    }); // update original next block

    transformBlock(originalBlock.getNextSiblingKey(), blocks, function (block) {
      return block.merge({
        prevSibling: belowBlockKey
      });
    }); // update original block

    transformBlock(originalBlockKey, blocks, function (block) {
      return block.merge({
        nextSibling: belowBlockKey
      });
    }); // update below block

    transformBlock(belowBlockKey, blocks, function (block) {
      return block.merge({
        prevSibling: originalBlockKey
      });
    });
  });
};

var splitBlockInContentState = function splitBlockInContentState(contentState, selectionState) {
  !selectionState.isCollapsed() ?  true ? invariant(false, 'Selection range must be collapsed.') : undefined : void 0;
  var key = selectionState.getAnchorKey();
  var offset = selectionState.getAnchorOffset();
  var blockMap = contentState.getBlockMap();
  var blockToSplit = blockMap.get(key);
  var text = blockToSplit.getText();
  var chars = blockToSplit.getCharacterList();
  var keyBelow = generateRandomKey();
  var isExperimentalTreeBlock = blockToSplit instanceof ContentBlockNode;
  var blockAbove = blockToSplit.merge({
    text: text.slice(0, offset),
    characterList: chars.slice(0, offset)
  });
  var blockBelow = blockAbove.merge({
    key: keyBelow,
    text: text.slice(offset),
    characterList: chars.slice(offset),
    data: Map()
  });
  var blocksBefore = blockMap.toSeq().takeUntil(function (v) {
    return v === blockToSplit;
  });
  var blocksAfter = blockMap.toSeq().skipUntil(function (v) {
    return v === blockToSplit;
  }).rest();
  var newBlocks = blocksBefore.concat([[key, blockAbove], [keyBelow, blockBelow]], blocksAfter).toOrderedMap();

  if (isExperimentalTreeBlock) {
    !blockToSplit.getChildKeys().isEmpty() ?  true ? invariant(false, 'ContentBlockNode must not have children') : undefined : void 0;
    newBlocks = updateBlockMapLinks(newBlocks, blockAbove, blockBelow);
  }

  return contentState.merge({
    blockMap: newBlocks,
    selectionBefore: selectionState,
    selectionAfter: selectionState.merge({
      anchorKey: keyBelow,
      anchorOffset: 0,
      focusKey: keyBelow,
      focusOffset: 0,
      isBackward: false
    })
  });
};

module.exports = splitBlockInContentState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL3NwbGl0QmxvY2tJbkNvbnRlbnRTdGF0ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9zcGxpdEJsb2NrSW5Db250ZW50U3RhdGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBzcGxpdEJsb2NrSW5Db250ZW50U3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIENvbnRlbnRCbG9ja05vZGUgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9ja05vZGUnKTtcbnZhciBJbW11dGFibGUgPSByZXF1aXJlKCdpbW11dGFibGUnKTtcblxudmFyIGdlbmVyYXRlUmFuZG9tS2V5ID0gcmVxdWlyZSgnLi9nZW5lcmF0ZVJhbmRvbUtleScpO1xudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJ2ZianMvbGliL2ludmFyaWFudCcpO1xuXG52YXIgTGlzdCA9IEltbXV0YWJsZS5MaXN0LFxuICAgIE1hcCA9IEltbXV0YWJsZS5NYXA7XG5cblxudmFyIHRyYW5zZm9ybUJsb2NrID0gZnVuY3Rpb24gdHJhbnNmb3JtQmxvY2soa2V5LCBibG9ja01hcCwgZnVuYykge1xuICBpZiAoIWtleSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBibG9jayA9IGJsb2NrTWFwLmdldChrZXkpO1xuXG4gIGlmICghYmxvY2spIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBibG9ja01hcC5zZXQoa2V5LCBmdW5jKGJsb2NrKSk7XG59O1xuXG52YXIgdXBkYXRlQmxvY2tNYXBMaW5rcyA9IGZ1bmN0aW9uIHVwZGF0ZUJsb2NrTWFwTGlua3MoYmxvY2tNYXAsIG9yaWdpbmFsQmxvY2ssIGJlbG93QmxvY2spIHtcbiAgcmV0dXJuIGJsb2NrTWFwLndpdGhNdXRhdGlvbnMoZnVuY3Rpb24gKGJsb2Nrcykge1xuICAgIHZhciBvcmlnaW5hbEJsb2NrS2V5ID0gb3JpZ2luYWxCbG9jay5nZXRLZXkoKTtcbiAgICB2YXIgYmVsb3dCbG9ja0tleSA9IGJlbG93QmxvY2suZ2V0S2V5KCk7XG5cbiAgICAvLyB1cGRhdGUgYmxvY2sgcGFyZW50XG4gICAgdHJhbnNmb3JtQmxvY2sob3JpZ2luYWxCbG9jay5nZXRQYXJlbnRLZXkoKSwgYmxvY2tzLCBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgIHZhciBwYXJlbnRDaGlsZHJlbkxpc3QgPSBibG9jay5nZXRDaGlsZEtleXMoKTtcbiAgICAgIHZhciBpbnNlcnRpb25JbmRleCA9IHBhcmVudENoaWxkcmVuTGlzdC5pbmRleE9mKG9yaWdpbmFsQmxvY2tLZXkpICsgMTtcbiAgICAgIHZhciBuZXdDaGlsZHJlbkFycmF5ID0gcGFyZW50Q2hpbGRyZW5MaXN0LnRvQXJyYXkoKTtcblxuICAgICAgbmV3Q2hpbGRyZW5BcnJheS5zcGxpY2UoaW5zZXJ0aW9uSW5kZXgsIDAsIGJlbG93QmxvY2tLZXkpO1xuXG4gICAgICByZXR1cm4gYmxvY2subWVyZ2Uoe1xuICAgICAgICBjaGlsZHJlbjogTGlzdChuZXdDaGlsZHJlbkFycmF5KVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICAvLyB1cGRhdGUgb3JpZ2luYWwgbmV4dCBibG9ja1xuICAgIHRyYW5zZm9ybUJsb2NrKG9yaWdpbmFsQmxvY2suZ2V0TmV4dFNpYmxpbmdLZXkoKSwgYmxvY2tzLCBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgIHJldHVybiBibG9jay5tZXJnZSh7XG4gICAgICAgIHByZXZTaWJsaW5nOiBiZWxvd0Jsb2NrS2V5XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIC8vIHVwZGF0ZSBvcmlnaW5hbCBibG9ja1xuICAgIHRyYW5zZm9ybUJsb2NrKG9yaWdpbmFsQmxvY2tLZXksIGJsb2NrcywgZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgICByZXR1cm4gYmxvY2subWVyZ2Uoe1xuICAgICAgICBuZXh0U2libGluZzogYmVsb3dCbG9ja0tleVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICAvLyB1cGRhdGUgYmVsb3cgYmxvY2tcbiAgICB0cmFuc2Zvcm1CbG9jayhiZWxvd0Jsb2NrS2V5LCBibG9ja3MsIGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHtcbiAgICAgICAgcHJldlNpYmxpbmc6IG9yaWdpbmFsQmxvY2tLZXlcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9KTtcbn07XG5cbnZhciBzcGxpdEJsb2NrSW5Db250ZW50U3RhdGUgPSBmdW5jdGlvbiBzcGxpdEJsb2NrSW5Db250ZW50U3RhdGUoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSkge1xuICAhc2VsZWN0aW9uU3RhdGUuaXNDb2xsYXBzZWQoKSA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdTZWxlY3Rpb24gcmFuZ2UgbXVzdCBiZSBjb2xsYXBzZWQuJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gIHZhciBrZXkgPSBzZWxlY3Rpb25TdGF0ZS5nZXRBbmNob3JLZXkoKTtcbiAgdmFyIG9mZnNldCA9IHNlbGVjdGlvblN0YXRlLmdldEFuY2hvck9mZnNldCgpO1xuICB2YXIgYmxvY2tNYXAgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tNYXAoKTtcbiAgdmFyIGJsb2NrVG9TcGxpdCA9IGJsb2NrTWFwLmdldChrZXkpO1xuICB2YXIgdGV4dCA9IGJsb2NrVG9TcGxpdC5nZXRUZXh0KCk7XG4gIHZhciBjaGFycyA9IGJsb2NrVG9TcGxpdC5nZXRDaGFyYWN0ZXJMaXN0KCk7XG4gIHZhciBrZXlCZWxvdyA9IGdlbmVyYXRlUmFuZG9tS2V5KCk7XG4gIHZhciBpc0V4cGVyaW1lbnRhbFRyZWVCbG9jayA9IGJsb2NrVG9TcGxpdCBpbnN0YW5jZW9mIENvbnRlbnRCbG9ja05vZGU7XG5cbiAgdmFyIGJsb2NrQWJvdmUgPSBibG9ja1RvU3BsaXQubWVyZ2Uoe1xuICAgIHRleHQ6IHRleHQuc2xpY2UoMCwgb2Zmc2V0KSxcbiAgICBjaGFyYWN0ZXJMaXN0OiBjaGFycy5zbGljZSgwLCBvZmZzZXQpXG4gIH0pO1xuICB2YXIgYmxvY2tCZWxvdyA9IGJsb2NrQWJvdmUubWVyZ2Uoe1xuICAgIGtleToga2V5QmVsb3csXG4gICAgdGV4dDogdGV4dC5zbGljZShvZmZzZXQpLFxuICAgIGNoYXJhY3Rlckxpc3Q6IGNoYXJzLnNsaWNlKG9mZnNldCksXG4gICAgZGF0YTogTWFwKClcbiAgfSk7XG5cbiAgdmFyIGJsb2Nrc0JlZm9yZSA9IGJsb2NrTWFwLnRvU2VxKCkudGFrZVVudGlsKGZ1bmN0aW9uICh2KSB7XG4gICAgcmV0dXJuIHYgPT09IGJsb2NrVG9TcGxpdDtcbiAgfSk7XG4gIHZhciBibG9ja3NBZnRlciA9IGJsb2NrTWFwLnRvU2VxKCkuc2tpcFVudGlsKGZ1bmN0aW9uICh2KSB7XG4gICAgcmV0dXJuIHYgPT09IGJsb2NrVG9TcGxpdDtcbiAgfSkucmVzdCgpO1xuICB2YXIgbmV3QmxvY2tzID0gYmxvY2tzQmVmb3JlLmNvbmNhdChbW2tleSwgYmxvY2tBYm92ZV0sIFtrZXlCZWxvdywgYmxvY2tCZWxvd11dLCBibG9ja3NBZnRlcikudG9PcmRlcmVkTWFwKCk7XG5cbiAgaWYgKGlzRXhwZXJpbWVudGFsVHJlZUJsb2NrKSB7XG4gICAgIWJsb2NrVG9TcGxpdC5nZXRDaGlsZEtleXMoKS5pc0VtcHR5KCkgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAnQ29udGVudEJsb2NrTm9kZSBtdXN0IG5vdCBoYXZlIGNoaWxkcmVuJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gICAgbmV3QmxvY2tzID0gdXBkYXRlQmxvY2tNYXBMaW5rcyhuZXdCbG9ja3MsIGJsb2NrQWJvdmUsIGJsb2NrQmVsb3cpO1xuICB9XG5cbiAgcmV0dXJuIGNvbnRlbnRTdGF0ZS5tZXJnZSh7XG4gICAgYmxvY2tNYXA6IG5ld0Jsb2NrcyxcbiAgICBzZWxlY3Rpb25CZWZvcmU6IHNlbGVjdGlvblN0YXRlLFxuICAgIHNlbGVjdGlvbkFmdGVyOiBzZWxlY3Rpb25TdGF0ZS5tZXJnZSh7XG4gICAgICBhbmNob3JLZXk6IGtleUJlbG93LFxuICAgICAgYW5jaG9yT2Zmc2V0OiAwLFxuICAgICAgZm9jdXNLZXk6IGtleUJlbG93LFxuICAgICAgZm9jdXNPZmZzZXQ6IDAsXG4gICAgICBpc0JhY2t3YXJkOiBmYWxzZVxuICAgIH0pXG4gIH0pO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBzcGxpdEJsb2NrSW5Db250ZW50U3RhdGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBSEE7QUFXQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/splitBlockInContentState.js
