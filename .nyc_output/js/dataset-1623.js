/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var ComponentModel = __webpack_require__(/*! ../model/Component */ "./node_modules/echarts/lib/model/Component.js");

var ComponentView = __webpack_require__(/*! ../view/Component */ "./node_modules/echarts/lib/view/Component.js");

var _sourceHelper = __webpack_require__(/*! ../data/helper/sourceHelper */ "./node_modules/echarts/lib/data/helper/sourceHelper.js");

var detectSourceFormat = _sourceHelper.detectSourceFormat;

var _sourceType = __webpack_require__(/*! ../data/helper/sourceType */ "./node_modules/echarts/lib/data/helper/sourceType.js");

var SERIES_LAYOUT_BY_COLUMN = _sourceType.SERIES_LAYOUT_BY_COLUMN;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * This module is imported by echarts directly.
 *
 * Notice:
 * Always keep this file exists for backward compatibility.
 * Because before 4.1.0, dataset is an optional component,
 * some users may import this module manually.
 */

ComponentModel.extend({
  type: 'dataset',

  /**
   * @protected
   */
  defaultOption: {
    // 'row', 'column'
    seriesLayoutBy: SERIES_LAYOUT_BY_COLUMN,
    // null/'auto': auto detect header, see "module:echarts/data/helper/sourceHelper"
    sourceHeader: null,
    dimensions: null,
    source: null
  },
  optionUpdated: function optionUpdated() {
    detectSourceFormat(this);
  }
});
ComponentView.extend({
  type: 'dataset'
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2RhdGFzZXQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb21wb25lbnQvZGF0YXNldC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIENvbXBvbmVudE1vZGVsID0gcmVxdWlyZShcIi4uL21vZGVsL0NvbXBvbmVudFwiKTtcblxudmFyIENvbXBvbmVudFZpZXcgPSByZXF1aXJlKFwiLi4vdmlldy9Db21wb25lbnRcIik7XG5cbnZhciBfc291cmNlSGVscGVyID0gcmVxdWlyZShcIi4uL2RhdGEvaGVscGVyL3NvdXJjZUhlbHBlclwiKTtcblxudmFyIGRldGVjdFNvdXJjZUZvcm1hdCA9IF9zb3VyY2VIZWxwZXIuZGV0ZWN0U291cmNlRm9ybWF0O1xuXG52YXIgX3NvdXJjZVR5cGUgPSByZXF1aXJlKFwiLi4vZGF0YS9oZWxwZXIvc291cmNlVHlwZVwiKTtcblxudmFyIFNFUklFU19MQVlPVVRfQllfQ09MVU1OID0gX3NvdXJjZVR5cGUuU0VSSUVTX0xBWU9VVF9CWV9DT0xVTU47XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBUaGlzIG1vZHVsZSBpcyBpbXBvcnRlZCBieSBlY2hhcnRzIGRpcmVjdGx5LlxuICpcbiAqIE5vdGljZTpcbiAqIEFsd2F5cyBrZWVwIHRoaXMgZmlsZSBleGlzdHMgZm9yIGJhY2t3YXJkIGNvbXBhdGliaWxpdHkuXG4gKiBCZWNhdXNlIGJlZm9yZSA0LjEuMCwgZGF0YXNldCBpcyBhbiBvcHRpb25hbCBjb21wb25lbnQsXG4gKiBzb21lIHVzZXJzIG1heSBpbXBvcnQgdGhpcyBtb2R1bGUgbWFudWFsbHkuXG4gKi9cbkNvbXBvbmVudE1vZGVsLmV4dGVuZCh7XG4gIHR5cGU6ICdkYXRhc2V0JyxcblxuICAvKipcbiAgICogQHByb3RlY3RlZFxuICAgKi9cbiAgZGVmYXVsdE9wdGlvbjoge1xuICAgIC8vICdyb3cnLCAnY29sdW1uJ1xuICAgIHNlcmllc0xheW91dEJ5OiBTRVJJRVNfTEFZT1VUX0JZX0NPTFVNTixcbiAgICAvLyBudWxsLydhdXRvJzogYXV0byBkZXRlY3QgaGVhZGVyLCBzZWUgXCJtb2R1bGU6ZWNoYXJ0cy9kYXRhL2hlbHBlci9zb3VyY2VIZWxwZXJcIlxuICAgIHNvdXJjZUhlYWRlcjogbnVsbCxcbiAgICBkaW1lbnNpb25zOiBudWxsLFxuICAgIHNvdXJjZTogbnVsbFxuICB9LFxuICBvcHRpb25VcGRhdGVkOiBmdW5jdGlvbiAoKSB7XG4gICAgZGV0ZWN0U291cmNlRm9ybWF0KHRoaXMpO1xuICB9XG59KTtcbkNvbXBvbmVudFZpZXcuZXh0ZW5kKHtcbiAgdHlwZTogJ2RhdGFzZXQnXG59KTsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQWhCQTtBQWtCQTtBQUNBO0FBREEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/dataset.js
