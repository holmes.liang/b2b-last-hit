__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");






/**
 * storage delegate
 */
var StorageDelegate =
/*#__PURE__*/
function () {
  function StorageDelegate(name, storage) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, StorageDelegate);

    this.name = void 0;
    this.storage = void 0;
    this.name = name;
    this.storage = storage;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(StorageDelegate, [{
    key: "set",
    value: function set(key, value) {
      var json = this.current();
      json[key] = value;
      this.storage.setItem(this.name, JSON.stringify(json));
      return this;
    }
  }, {
    key: "get",
    value: function get(key) {
      return this.current()[key];
    }
  }, {
    key: "remove",
    value: function remove(key) {
      var current = this.current();
      delete current[key];
      this.storage.setItem(this.name, JSON.stringify(current));
      return this;
    }
    /**
     * get current storage values, returns {} if nothing
     */

  }, {
    key: "current",
    value: function current() {
      var currentValue = this.storage.getItem(this.name);
      return currentValue ? JSON.parse(currentValue) : {};
    }
  }, {
    key: "clear",
    value: function clear() {
      this.storage.removeItem(this.name);
      return this;
    }
  }]);

  return StorageDelegate;
}();
/**
 * storage proxy, to session storage and local storage
 */


var StorageProxy =
/*#__PURE__*/
function () {
  function StorageProxy(name) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, StorageProxy);

    this.sessionStorage = void 0;
    this.localStorage = void 0;
    this.sessionStorage = new StorageDelegate(name, window.sessionStorage);
    this.localStorage = new StorageDelegate(name, window.localStorage);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(StorageProxy, [{
    key: "session",
    value: function session() {
      return this.sessionStorage;
    }
  }, {
    key: "local",
    value: function local() {
      return this.localStorage;
    }
  }, {
    key: "get",
    value: function get(key) {
      return this.session().get(key) || this.local().get(key);
    }
  }, {
    key: "set",
    value: function set(key, value) {
      this.session().set(key, value);
      this.local().set(key, value);
      return this;
    }
  }, {
    key: "remove",
    value: function remove(key) {
      this.session().remove(key);
      this.local().remove(key);
      return this;
    }
  }, {
    key: "clear",
    value: function clear() {
      this.session().clear();
      this.local().clear();
      return this;
    }
  }]);

  return StorageProxy;
}();
/**
 * theme storage
 */


var ThemeStorage =
/*#__PURE__*/
function (_StorageProxy) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_2__["default"])(ThemeStorage, _StorageProxy);

  function ThemeStorage() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, ThemeStorage);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_1__["default"])(ThemeStorage).call(this, "Insmate-Theme"));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(ThemeStorage, [{
    key: "set",
    value: function set(key, value) {
      this.session().set(key, value);

      if (true) {
        this.local().set(key, value);
      }

      return this;
    }
  }]);

  return ThemeStorage;
}(StorageProxy); // initialize storages


var Auth = new StorageProxy("Insmate-Auth");
var Account = new StorageProxy("Insmate-Account");
var Env = new StorageProxy("Insmate-Env");
var currentRouters = new StorageProxy("Insmate-CurrentRouters");
var TitleLogo = new StorageProxy("Insmate-TitleLogo");
var Theme = new ThemeStorage();
var AllowToManageMiniSite = new StorageProxy("Insmate-AllowToManageMiniSite");
var GlobalParams = new StorageProxy("Insmate-GlobalParams");
/* harmony default export */ __webpack_exports__["default"] = ({
  Auth: Auth,
  Account: Account,
  Env: Env,
  Theme: Theme,
  TitleLogo: TitleLogo,
  currentRouters: currentRouters,
  AllowToManageMiniSite: AllowToManageMiniSite,
  GlobalParams: GlobalParams,
  clear: function clear() {
    window.sessionStorage.clear();
    Auth.local().clear();
    Env.local().clear();
  }
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL3N0b3JhZ2UudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tbW9uL3N0b3JhZ2UudHN4Il0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogc3RvcmFnZSBkZWxlZ2F0ZVxuICovXG5jbGFzcyBTdG9yYWdlRGVsZWdhdGUge1xuICBwcml2YXRlIG5hbWU6IHN0cmluZztcbiAgcHJpdmF0ZSBzdG9yYWdlOiBTdG9yYWdlO1xuXG4gIGNvbnN0cnVjdG9yKG5hbWU6IHN0cmluZywgc3RvcmFnZTogU3RvcmFnZSkge1xuICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgdGhpcy5zdG9yYWdlID0gc3RvcmFnZTtcbiAgfVxuXG4gIHNldChrZXk6IHN0cmluZywgdmFsdWU6IGFueSk6IHRoaXMge1xuICAgIGNvbnN0IGpzb24gPSB0aGlzLmN1cnJlbnQoKTtcbiAgICBqc29uW2tleV0gPSB2YWx1ZTtcbiAgICB0aGlzLnN0b3JhZ2Uuc2V0SXRlbSh0aGlzLm5hbWUsIEpTT04uc3RyaW5naWZ5KGpzb24pKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIGdldChrZXk6IHN0cmluZyk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMuY3VycmVudCgpW2tleV07XG4gIH1cblxuICByZW1vdmUoa2V5OiBzdHJpbmcpOiB0aGlzIHtcbiAgICBjb25zdCBjdXJyZW50ID0gdGhpcy5jdXJyZW50KCk7XG4gICAgZGVsZXRlIGN1cnJlbnRba2V5XTtcbiAgICB0aGlzLnN0b3JhZ2Uuc2V0SXRlbSh0aGlzLm5hbWUsIEpTT04uc3RyaW5naWZ5KGN1cnJlbnQpKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIC8qKlxuICAgKiBnZXQgY3VycmVudCBzdG9yYWdlIHZhbHVlcywgcmV0dXJucyB7fSBpZiBub3RoaW5nXG4gICAqL1xuICBjdXJyZW50KCk6IGFueSB7XG4gICAgY29uc3QgY3VycmVudFZhbHVlID0gdGhpcy5zdG9yYWdlLmdldEl0ZW0odGhpcy5uYW1lKTtcbiAgICByZXR1cm4gY3VycmVudFZhbHVlID8gSlNPTi5wYXJzZShjdXJyZW50VmFsdWUpIDoge307XG4gIH1cblxuICBjbGVhcigpOiB0aGlzIHtcbiAgICB0aGlzLnN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLm5hbWUpO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG59XG5cbi8qKlxuICogc3RvcmFnZSBwcm94eSwgdG8gc2Vzc2lvbiBzdG9yYWdlIGFuZCBsb2NhbCBzdG9yYWdlXG4gKi9cbmNsYXNzIFN0b3JhZ2VQcm94eSB7XG4gIHByaXZhdGUgc2Vzc2lvblN0b3JhZ2U6IFN0b3JhZ2VEZWxlZ2F0ZTtcbiAgcHJpdmF0ZSBsb2NhbFN0b3JhZ2U6IFN0b3JhZ2VEZWxlZ2F0ZTtcblxuICBjb25zdHJ1Y3RvcihuYW1lOiBzdHJpbmcpIHtcbiAgICB0aGlzLnNlc3Npb25TdG9yYWdlID0gbmV3IFN0b3JhZ2VEZWxlZ2F0ZShuYW1lLCB3aW5kb3cuc2Vzc2lvblN0b3JhZ2UpO1xuICAgIHRoaXMubG9jYWxTdG9yYWdlID0gbmV3IFN0b3JhZ2VEZWxlZ2F0ZShuYW1lLCB3aW5kb3cubG9jYWxTdG9yYWdlKTtcbiAgfVxuXG4gIHNlc3Npb24oKTogU3RvcmFnZURlbGVnYXRlIHtcbiAgICByZXR1cm4gdGhpcy5zZXNzaW9uU3RvcmFnZTtcbiAgfVxuXG4gIGxvY2FsKCk6IFN0b3JhZ2VEZWxlZ2F0ZSB7XG4gICAgcmV0dXJuIHRoaXMubG9jYWxTdG9yYWdlO1xuICB9XG5cbiAgZ2V0KGtleTogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5zZXNzaW9uKCkuZ2V0KGtleSkgfHwgdGhpcy5sb2NhbCgpLmdldChrZXkpO1xuICB9XG5cbiAgc2V0KGtleTogc3RyaW5nLCB2YWx1ZTogYW55KTogdGhpcyB7XG4gICAgdGhpcy5zZXNzaW9uKCkuc2V0KGtleSwgdmFsdWUpO1xuICAgIHRoaXMubG9jYWwoKS5zZXQoa2V5LCB2YWx1ZSk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICByZW1vdmUoa2V5OiBzdHJpbmcpOiB0aGlzIHtcbiAgICB0aGlzLnNlc3Npb24oKS5yZW1vdmUoa2V5KTtcbiAgICB0aGlzLmxvY2FsKCkucmVtb3ZlKGtleSk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICBjbGVhcigpOiB0aGlzIHtcbiAgICB0aGlzLnNlc3Npb24oKS5jbGVhcigpO1xuICAgIHRoaXMubG9jYWwoKS5jbGVhcigpO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG59XG5cbi8qKlxuICogdGhlbWUgc3RvcmFnZVxuICovXG5jbGFzcyBUaGVtZVN0b3JhZ2UgZXh0ZW5kcyBTdG9yYWdlUHJveHkge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcihcIkluc21hdGUtVGhlbWVcIik7XG4gIH1cblxuICBzZXQoa2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkpOiB0aGlzIHtcbiAgICB0aGlzLnNlc3Npb24oKS5zZXQoa2V5LCB2YWx1ZSk7XG4gICAgaWYgKHByb2Nlc3MuZW52LlJFQUNUX0FQUF9FTlZfTkFNRSAhPT0gXCJMT0NBTFwiKSB7XG4gICAgICB0aGlzLmxvY2FsKCkuc2V0KGtleSwgdmFsdWUpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcztcbiAgfVxufVxuXG5cbi8vIGluaXRpYWxpemUgc3RvcmFnZXNcbmNvbnN0IEF1dGggPSBuZXcgU3RvcmFnZVByb3h5KFwiSW5zbWF0ZS1BdXRoXCIpO1xuY29uc3QgQWNjb3VudCA9IG5ldyBTdG9yYWdlUHJveHkoXCJJbnNtYXRlLUFjY291bnRcIik7XG5jb25zdCBFbnYgPSBuZXcgU3RvcmFnZVByb3h5KFwiSW5zbWF0ZS1FbnZcIik7XG5jb25zdCBjdXJyZW50Um91dGVycyA9IG5ldyBTdG9yYWdlUHJveHkoXCJJbnNtYXRlLUN1cnJlbnRSb3V0ZXJzXCIpO1xuY29uc3QgVGl0bGVMb2dvID0gbmV3IFN0b3JhZ2VQcm94eShcIkluc21hdGUtVGl0bGVMb2dvXCIpO1xuY29uc3QgVGhlbWUgPSBuZXcgVGhlbWVTdG9yYWdlKCk7XG5jb25zdCBBbGxvd1RvTWFuYWdlTWluaVNpdGUgPSBuZXcgU3RvcmFnZVByb3h5KFwiSW5zbWF0ZS1BbGxvd1RvTWFuYWdlTWluaVNpdGVcIik7XG5jb25zdCBHbG9iYWxQYXJhbXMgPSBuZXcgU3RvcmFnZVByb3h5KFwiSW5zbWF0ZS1HbG9iYWxQYXJhbXNcIik7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgQXV0aCxcbiAgQWNjb3VudCxcbiAgRW52LFxuICBUaGVtZSxcbiAgVGl0bGVMb2dvLFxuICBjdXJyZW50Um91dGVycyxcbiAgQWxsb3dUb01hbmFnZU1pbmlTaXRlLFxuICBHbG9iYWxQYXJhbXMsXG4gIGNsZWFyOiAoKTogdm9pZCA9PiB7XG4gICAgd2luZG93LnNlc3Npb25TdG9yYWdlLmNsZWFyKCk7XG4gICAgQXV0aC5sb2NhbCgpLmNsZWFyKCk7XG4gICAgRW52LmxvY2FsKCkuY2xlYXIoKTtcbiAgfSxcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBOzs7QUFHQTs7O0FBSUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFHQTs7Ozs7QUFHQTs7O0FBSUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUdBOzs7OztBQUdBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7O0FBWEE7QUFDQTtBQUNBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFiQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/common/storage.tsx
