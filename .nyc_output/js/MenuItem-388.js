__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MenuItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_menu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-menu */ "./node_modules/rc-menu/es/index.js");
/* harmony import */ var _MenuContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MenuContext */ "./node_modules/antd/es/menu/MenuContext.js");
/* harmony import */ var _tooltip__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../tooltip */ "./node_modules/antd/es/tooltip/index.js");
/* harmony import */ var _layout_Sider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../layout/Sider */ "./node_modules/antd/es/layout/Sider.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};







var MenuItem =
/*#__PURE__*/
function (_React$Component) {
  _inherits(MenuItem, _React$Component);

  function MenuItem() {
    var _this;

    _classCallCheck(this, MenuItem);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(MenuItem).apply(this, arguments));

    _this.onKeyDown = function (e) {
      _this.menuItem.onKeyDown(e);
    };

    _this.saveMenuItem = function (menuItem) {
      _this.menuItem = menuItem;
    };

    _this.renderItem = function (_ref) {
      var siderCollapsed = _ref.siderCollapsed;
      var _this$props = _this.props,
          level = _this$props.level,
          children = _this$props.children,
          rootPrefixCls = _this$props.rootPrefixCls;

      var _a = _this.props,
          title = _a.title,
          rest = __rest(_a, ["title"]);

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_MenuContext__WEBPACK_IMPORTED_MODULE_2__["default"].Consumer, null, function (_ref2) {
        var inlineCollapsed = _ref2.inlineCollapsed;
        var tooltipProps = {
          title: title || (level === 1 ? children : '')
        };

        if (!siderCollapsed && !inlineCollapsed) {
          tooltipProps.title = null; // Reset `visible` to fix control mode tooltip display not correct
          // ref: https://github.com/ant-design/ant-design/issues/16742

          tooltipProps.visible = false;
        }

        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_tooltip__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({}, tooltipProps, {
          placement: "right",
          overlayClassName: "".concat(rootPrefixCls, "-inline-collapsed-tooltip")
        }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_menu__WEBPACK_IMPORTED_MODULE_1__["Item"], _extends({}, rest, {
          title: title,
          ref: _this.saveMenuItem
        })));
      });
    };

    return _this;
  }

  _createClass(MenuItem, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_layout_Sider__WEBPACK_IMPORTED_MODULE_4__["SiderContext"].Consumer, null, this.renderItem);
    }
  }]);

  return MenuItem;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


MenuItem.isMenuItem = true;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9tZW51L01lbnVJdGVtLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9tZW51L01lbnVJdGVtLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBJdGVtIH0gZnJvbSAncmMtbWVudSc7XG5pbXBvcnQgTWVudUNvbnRleHQgZnJvbSAnLi9NZW51Q29udGV4dCc7XG5pbXBvcnQgVG9vbHRpcCBmcm9tICcuLi90b29sdGlwJztcbmltcG9ydCB7IFNpZGVyQ29udGV4dCB9IGZyb20gJy4uL2xheW91dC9TaWRlcic7XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNZW51SXRlbSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMub25LZXlEb3duID0gKGUpID0+IHtcbiAgICAgICAgICAgIHRoaXMubWVudUl0ZW0ub25LZXlEb3duKGUpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnNhdmVNZW51SXRlbSA9IChtZW51SXRlbSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5tZW51SXRlbSA9IG1lbnVJdGVtO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlckl0ZW0gPSAoeyBzaWRlckNvbGxhcHNlZCB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGxldmVsLCBjaGlsZHJlbiwgcm9vdFByZWZpeENscyB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IF9hID0gdGhpcy5wcm9wcywgeyB0aXRsZSB9ID0gX2EsIHJlc3QgPSBfX3Jlc3QoX2EsIFtcInRpdGxlXCJdKTtcbiAgICAgICAgICAgIHJldHVybiAoPE1lbnVDb250ZXh0LkNvbnN1bWVyPlxuICAgICAgICB7KHsgaW5saW5lQ29sbGFwc2VkIH0pID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCB0b29sdGlwUHJvcHMgPSB7XG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiB0aXRsZSB8fCAobGV2ZWwgPT09IDEgPyBjaGlsZHJlbiA6ICcnKSxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIGlmICghc2lkZXJDb2xsYXBzZWQgJiYgIWlubGluZUNvbGxhcHNlZCkge1xuICAgICAgICAgICAgICAgICAgICB0b29sdGlwUHJvcHMudGl0bGUgPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICAvLyBSZXNldCBgdmlzaWJsZWAgdG8gZml4IGNvbnRyb2wgbW9kZSB0b29sdGlwIGRpc3BsYXkgbm90IGNvcnJlY3RcbiAgICAgICAgICAgICAgICAgICAgLy8gcmVmOiBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xNjc0MlxuICAgICAgICAgICAgICAgICAgICB0b29sdGlwUHJvcHMudmlzaWJsZSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gKDxUb29sdGlwIHsuLi50b29sdGlwUHJvcHN9IHBsYWNlbWVudD1cInJpZ2h0XCIgb3ZlcmxheUNsYXNzTmFtZT17YCR7cm9vdFByZWZpeENsc30taW5saW5lLWNvbGxhcHNlZC10b29sdGlwYH0+XG4gICAgICAgICAgICAgIDxJdGVtIHsuLi5yZXN0fSB0aXRsZT17dGl0bGV9IHJlZj17dGhpcy5zYXZlTWVudUl0ZW19Lz5cbiAgICAgICAgICAgIDwvVG9vbHRpcD4pO1xuICAgICAgICAgICAgfX1cbiAgICAgIDwvTWVudUNvbnRleHQuQ29uc3VtZXI+KTtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gPFNpZGVyQ29udGV4dC5Db25zdW1lcj57dGhpcy5yZW5kZXJJdGVtfTwvU2lkZXJDb250ZXh0LkNvbnN1bWVyPjtcbiAgICB9XG59XG5NZW51SXRlbS5pc01lbnVJdGVtID0gdHJ1ZTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFaQTtBQUhBO0FBQ0E7QUFUQTtBQTRCQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7Ozs7QUFoQ0E7QUFDQTtBQURBO0FBa0NBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/menu/MenuItem.js
