var _vector = __webpack_require__(/*! ../../core/vector */ "./node_modules/zrender/lib/core/vector.js");

var v2Min = _vector.min;
var v2Max = _vector.max;
var v2Scale = _vector.scale;
var v2Distance = _vector.distance;
var v2Add = _vector.add;
var v2Clone = _vector.clone;
var v2Sub = _vector.sub;
/**
 * 贝塞尔平滑曲线
 * @module zrender/shape/util/smoothBezier
 * @author pissang (https://www.github.com/pissang)
 *         Kener (@Kener-林峰, kener.linfeng@gmail.com)
 *         errorrik (errorrik@gmail.com)
 */

/**
 * 贝塞尔平滑曲线
 * @alias module:zrender/shape/util/smoothBezier
 * @param {Array} points 线段顶点数组
 * @param {number} smooth 平滑等级, 0-1
 * @param {boolean} isLoop
 * @param {Array} constraint 将计算出来的控制点约束在一个包围盒内
 *                           比如 [[0, 0], [100, 100]], 这个包围盒会与
 *                           整个折线的包围盒做一个并集用来约束控制点。
 * @param {Array} 计算出来的控制点数组
 */

function _default(points, smooth, isLoop, constraint) {
  var cps = [];
  var v = [];
  var v1 = [];
  var v2 = [];
  var prevPoint;
  var nextPoint;
  var min;
  var max;

  if (constraint) {
    min = [Infinity, Infinity];
    max = [-Infinity, -Infinity];

    for (var i = 0, len = points.length; i < len; i++) {
      v2Min(min, min, points[i]);
      v2Max(max, max, points[i]);
    } // 与指定的包围盒做并集


    v2Min(min, min, constraint[0]);
    v2Max(max, max, constraint[1]);
  }

  for (var i = 0, len = points.length; i < len; i++) {
    var point = points[i];

    if (isLoop) {
      prevPoint = points[i ? i - 1 : len - 1];
      nextPoint = points[(i + 1) % len];
    } else {
      if (i === 0 || i === len - 1) {
        cps.push(v2Clone(points[i]));
        continue;
      } else {
        prevPoint = points[i - 1];
        nextPoint = points[i + 1];
      }
    }

    v2Sub(v, nextPoint, prevPoint); // use degree to scale the handle length

    v2Scale(v, v, smooth);
    var d0 = v2Distance(point, prevPoint);
    var d1 = v2Distance(point, nextPoint);
    var sum = d0 + d1;

    if (sum !== 0) {
      d0 /= sum;
      d1 /= sum;
    }

    v2Scale(v1, v, -d0);
    v2Scale(v2, v, d1);
    var cp0 = v2Add([], point, v1);
    var cp1 = v2Add([], point, v2);

    if (constraint) {
      v2Max(cp0, cp0, min);
      v2Min(cp0, cp0, max);
      v2Max(cp1, cp1, min);
      v2Min(cp1, cp1, max);
    }

    cps.push(cp0);
    cps.push(cp1);
  }

  if (isLoop) {
    cps.push(cps.shift());
  }

  return cps;
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvc21vb3RoQmV6aWVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvc21vb3RoQmV6aWVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBfdmVjdG9yID0gcmVxdWlyZShcIi4uLy4uL2NvcmUvdmVjdG9yXCIpO1xuXG52YXIgdjJNaW4gPSBfdmVjdG9yLm1pbjtcbnZhciB2Mk1heCA9IF92ZWN0b3IubWF4O1xudmFyIHYyU2NhbGUgPSBfdmVjdG9yLnNjYWxlO1xudmFyIHYyRGlzdGFuY2UgPSBfdmVjdG9yLmRpc3RhbmNlO1xudmFyIHYyQWRkID0gX3ZlY3Rvci5hZGQ7XG52YXIgdjJDbG9uZSA9IF92ZWN0b3IuY2xvbmU7XG52YXIgdjJTdWIgPSBfdmVjdG9yLnN1YjtcblxuLyoqXG4gKiDotJ3loZ7lsJTlubPmu5Hmm7Lnur9cbiAqIEBtb2R1bGUgenJlbmRlci9zaGFwZS91dGlsL3Ntb290aEJlemllclxuICogQGF1dGhvciBwaXNzYW5nIChodHRwczovL3d3dy5naXRodWIuY29tL3Bpc3NhbmcpXG4gKiAgICAgICAgIEtlbmVyIChAS2VuZXIt5p6X5bOwLCBrZW5lci5saW5mZW5nQGdtYWlsLmNvbSlcbiAqICAgICAgICAgZXJyb3JyaWsgKGVycm9ycmlrQGdtYWlsLmNvbSlcbiAqL1xuXG4vKipcbiAqIOi0neWhnuWwlOW5s+a7keabsue6v1xuICogQGFsaWFzIG1vZHVsZTp6cmVuZGVyL3NoYXBlL3V0aWwvc21vb3RoQmV6aWVyXG4gKiBAcGFyYW0ge0FycmF5fSBwb2ludHMg57q/5q616aG254K55pWw57uEXG4gKiBAcGFyYW0ge251bWJlcn0gc21vb3RoIOW5s+a7keetiee6pywgMC0xXG4gKiBAcGFyYW0ge2Jvb2xlYW59IGlzTG9vcFxuICogQHBhcmFtIHtBcnJheX0gY29uc3RyYWludCDlsIborqHnrpflh7rmnaXnmoTmjqfliLbngrnnuqbmnZ/lnKjkuIDkuKrljIXlm7Tnm5LlhoVcbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAg5q+U5aaCIFtbMCwgMF0sIFsxMDAsIDEwMF1dLCDov5nkuKrljIXlm7Tnm5LkvJrkuI5cbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAg5pW05Liq5oqY57q/55qE5YyF5Zu055uS5YGa5LiA5Liq5bm26ZuG55So5p2l57qm5p2f5o6n5Yi254K544CCXG4gKiBAcGFyYW0ge0FycmF5fSDorqHnrpflh7rmnaXnmoTmjqfliLbngrnmlbDnu4RcbiAqL1xuZnVuY3Rpb24gX2RlZmF1bHQocG9pbnRzLCBzbW9vdGgsIGlzTG9vcCwgY29uc3RyYWludCkge1xuICB2YXIgY3BzID0gW107XG4gIHZhciB2ID0gW107XG4gIHZhciB2MSA9IFtdO1xuICB2YXIgdjIgPSBbXTtcbiAgdmFyIHByZXZQb2ludDtcbiAgdmFyIG5leHRQb2ludDtcbiAgdmFyIG1pbjtcbiAgdmFyIG1heDtcblxuICBpZiAoY29uc3RyYWludCkge1xuICAgIG1pbiA9IFtJbmZpbml0eSwgSW5maW5pdHldO1xuICAgIG1heCA9IFstSW5maW5pdHksIC1JbmZpbml0eV07XG5cbiAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gcG9pbnRzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICB2Mk1pbihtaW4sIG1pbiwgcG9pbnRzW2ldKTtcbiAgICAgIHYyTWF4KG1heCwgbWF4LCBwb2ludHNbaV0pO1xuICAgIH0gLy8g5LiO5oyH5a6a55qE5YyF5Zu055uS5YGa5bm26ZuGXG5cblxuICAgIHYyTWluKG1pbiwgbWluLCBjb25zdHJhaW50WzBdKTtcbiAgICB2Mk1heChtYXgsIG1heCwgY29uc3RyYWludFsxXSk7XG4gIH1cblxuICBmb3IgKHZhciBpID0gMCwgbGVuID0gcG9pbnRzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgdmFyIHBvaW50ID0gcG9pbnRzW2ldO1xuXG4gICAgaWYgKGlzTG9vcCkge1xuICAgICAgcHJldlBvaW50ID0gcG9pbnRzW2kgPyBpIC0gMSA6IGxlbiAtIDFdO1xuICAgICAgbmV4dFBvaW50ID0gcG9pbnRzWyhpICsgMSkgJSBsZW5dO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoaSA9PT0gMCB8fCBpID09PSBsZW4gLSAxKSB7XG4gICAgICAgIGNwcy5wdXNoKHYyQ2xvbmUocG9pbnRzW2ldKSk7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcHJldlBvaW50ID0gcG9pbnRzW2kgLSAxXTtcbiAgICAgICAgbmV4dFBvaW50ID0gcG9pbnRzW2kgKyAxXTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB2MlN1Yih2LCBuZXh0UG9pbnQsIHByZXZQb2ludCk7IC8vIHVzZSBkZWdyZWUgdG8gc2NhbGUgdGhlIGhhbmRsZSBsZW5ndGhcblxuICAgIHYyU2NhbGUodiwgdiwgc21vb3RoKTtcbiAgICB2YXIgZDAgPSB2MkRpc3RhbmNlKHBvaW50LCBwcmV2UG9pbnQpO1xuICAgIHZhciBkMSA9IHYyRGlzdGFuY2UocG9pbnQsIG5leHRQb2ludCk7XG4gICAgdmFyIHN1bSA9IGQwICsgZDE7XG5cbiAgICBpZiAoc3VtICE9PSAwKSB7XG4gICAgICBkMCAvPSBzdW07XG4gICAgICBkMSAvPSBzdW07XG4gICAgfVxuXG4gICAgdjJTY2FsZSh2MSwgdiwgLWQwKTtcbiAgICB2MlNjYWxlKHYyLCB2LCBkMSk7XG4gICAgdmFyIGNwMCA9IHYyQWRkKFtdLCBwb2ludCwgdjEpO1xuICAgIHZhciBjcDEgPSB2MkFkZChbXSwgcG9pbnQsIHYyKTtcblxuICAgIGlmIChjb25zdHJhaW50KSB7XG4gICAgICB2Mk1heChjcDAsIGNwMCwgbWluKTtcbiAgICAgIHYyTWluKGNwMCwgY3AwLCBtYXgpO1xuICAgICAgdjJNYXgoY3AxLCBjcDEsIG1pbik7XG4gICAgICB2Mk1pbihjcDEsIGNwMSwgbWF4KTtcbiAgICB9XG5cbiAgICBjcHMucHVzaChjcDApO1xuICAgIGNwcy5wdXNoKGNwMSk7XG4gIH1cblxuICBpZiAoaXNMb29wKSB7XG4gICAgY3BzLnB1c2goY3BzLnNoaWZ0KCkpO1xuICB9XG5cbiAgcmV0dXJuIGNwcztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7OztBQVFBOzs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/helper/smoothBezier.js
