__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_useful_form_item__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-component/useful-form-item */ "./src/app/desk/component/useful-form-item/index.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_component_ph_componnets_extra_info__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk-component/ph/componnets/extra-info */ "./src/app/desk/component/ph/componnets/extra-info.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/ph/policyholder.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}










var Policyholder =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__["default"])(Policyholder, _ModelWidget);

  function Policyholder(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Policyholder);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(Policyholder).call(this, props, context));
    _this.queryPage = {};
    _this.dobPage = {};
    _this.groupCustomerPage = void 0;
    _this.timeout = null;

    _this.groupCustomerRef = function (ref) {
      _this.groupCustomerPage = ref;
    };

    _this.addressRef = function (ref) {
      _this.queryPage = ref;
    };

    _this.dobRef = function (ref) {
      _this.dobPage = ref;
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(Policyholder, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.partyType === "ORG") {
        this.setValueToModel("OFFICE", this.getDataIdProp("addressType"));
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.timeout) clearInterval(this.timeout);
    }
  }, {
    key: "changeInfo",
    value: function changeInfo() {
      var _this2 = this;

      var _this$props = this.props,
          model = _this$props.model,
          extraArrObj = _this$props.extraArrObj,
          type = _this$props.type;
      ["email", "title", "name", "mobile", "mobileNationCode", "idType", "idNo", "address"].concat(extraArrObj || []).forEach(function (item) {
        var initValue = _this2.getValueFromModel(_this2.getDataIdProp("".concat(item)));

        _this2.setValueToModel(initValue || "", _this2.getDataIdProp("".concat(item)));

        _this2.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])({}, _this2.getDataIdProp("".concat(item)), initValue || ""));

        var addressRefCurrent = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this2.queryPage, "idNoPage");

        var namePage = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this2.groupCustomerPage, "namePage");

        var idNoPage = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this2.groupCustomerPage, "idNoPage");

        if (lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, _this2.getDataIdProp("address.countryCode")) === "SGP") {
          addressRefCurrent && addressRefCurrent.getAddress(lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, _this2.getDataIdProp("address.postalCode")));
        }

        idNoPage && idNoPage.setShowLabel(_this2.getValueFromModel(_this2.getDataIdProp("idNo")));
        namePage && namePage.setShowLabel(_this2.getValueFromModel(_this2.getDataIdProp("name")));

        if (type === "EXTRA-INFO") {
          _this2.dobPage && _this2.dobPage.setDob();
        }
      });
    }
  }, {
    key: "getDataIdProp",
    value: function getDataIdProp(propName) {
      var _this$props2 = this.props,
          dataFixed = _this$props2.dataFixed,
          propsNameFixed = _this$props2.propsNameFixed;
      var extraPropsName = propsNameFixed ? "".concat(propsNameFixed) : "ext.policyholder";
      var propsName = dataFixed ? "".concat(dataFixed, ".").concat(extraPropsName) : extraPropsName;
      if (propName) return "".concat(propsName, ".").concat(propName);
      return "".concat(propsName);
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var C = this.getComponents();

      var _this$props3 = this.props,
          model = _this$props3.model,
          form = _this$props3.form,
          dataFixed = _this$props3.dataFixed,
          type = _this$props3.type,
          _this$props3$isPartyT = _this$props3.isPartyType,
          isPartyType = _this$props3$isPartyT === void 0 ? true : _this$props3$isPartyT,
          notDob = _this$props3.notDob,
          defaultCountryCode = _this$props3.defaultCountryCode,
          isCountry = _this$props3.isCountry,
          propsNameFixed = _this$props3.propsNameFixed,
          partyType = _this$props3.partyType,
          extraArrObj = _this$props3.extraArrObj,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$props3, ["model", "form", "dataFixed", "type", "isPartyType", "notDob", "defaultCountryCode", "isCountry", "propsNameFixed", "partyType", "extraArrObj"]);

      var that = this;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null, isPartyType && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NRadio"], {
        form: form,
        model: model,
        required: true,
        dataFixed: dataFixed,
        options: [{
          id: "INDI",
          text: "Individual"
        }, {
          id: "ORG",
          text: "Organization"
        }],
        propName: this.getDataIdProp("partyType"),
        label: _common__WEBPACK_IMPORTED_MODULE_13__["Language"].en("Type of Policyholder").thai("Type of Policyholder").getMessage(),
        onChange: function onChange() {
          _this3.changeInfo();

          _this3.setValueToModel("", _this3.getDataIdProp("idType"));

          _this3.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])({}, _this3.getDataIdProp("idType"), ""));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_15__["GroupCustomer"], {
        dataFixed: dataFixed,
        ref: this.groupCustomerRef,
        model: model,
        form: form,
        notDob: notDob,
        extraArrObj: extraArrObj,
        onSelectChange: function onSelectChange(item) {
          that.setValuesToModel(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])({}, _this3.getDataIdProp("address"), item.address));

          var addressRefCurrent = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this3.queryPage, "idNoPage");

          if (isCountry) {
            _this3.queryPage && _this3.queryPage.changeCountry(lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "address.countryCode", ""));
          }

          if (lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item.address, "countryCode") === "SGP") {
            addressRefCurrent && addressRefCurrent.getAddress(lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "address.postalCode"));
          }
        },
        groupType: partyType,
        dataId: this.getDataIdProp(),
        onChangeAddress: function onChangeAddress() {
          _this3.changeInfo();
        },
        partyType: partyType,
        countryCode: "SGP",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 119
        },
        __self: this
      }), type === "EXTRA-INFO" && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component_ph_componnets_extra_info__WEBPACK_IMPORTED_MODULE_16__["default"], {
        model: model,
        form: form,
        ref: this.dobRef,
        dataFixed: dataFixed,
        propsNameFixed: propsNameFixed,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }), this.props.childDom || "", _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component_useful_form_item__WEBPACK_IMPORTED_MODULE_14__["Mobile"], {
        form: form,
        model: model,
        dataFixed: dataFixed,
        dataId: this.getDataIdProp(),
        isPF: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 150
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component_useful_form_item__WEBPACK_IMPORTED_MODULE_14__["FormItemEmail"], {
        form: form,
        model: model,
        propName: this.getDataIdProp("email"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 151
        },
        __self: this
      }), partyType !== "ORG" && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NRadio"], {
        form: form,
        model: model,
        tableName: "addresstype",
        required: true,
        propName: this.getDataIdProp("addressType"),
        label: _common__WEBPACK_IMPORTED_MODULE_13__["Language"].en("Address Type").thai("สถานที่ทำงาน (กรณีไม่มี ให้เลือกที่อยู่ปัจจุบัน)").my("လိပ်စာအမျိုးအစား").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 156
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_15__["CommonAddress"], {
        model: model,
        form: form,
        ref: this.addressRef,
        defaultCountryCode: isCountry ? defaultCountryCode : defaultCountryCode || "SGP",
        dataIdOrPrefix: this.getDataIdProp(),
        propLabel: "Address",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 168
        },
        __self: this
      })));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(Policyholder.prototype), "initState", this).call(this), {
        entityOptions: []
      });
    }
  }]);

  return Policyholder;
}(_component__WEBPACK_IMPORTED_MODULE_11__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Policyholder);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BoL3BvbGljeWhvbGRlci50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcGgvcG9saWN5aG9sZGVyLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBOUmFkaW8gfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IExhbmd1YWdlIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBGb3JtSXRlbUVtYWlsLCBNb2JpbGUgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW1cIjtcbmltcG9ydCB7IENvbW1vbkFkZHJlc3MsIEdyb3VwQ3VzdG9tZXIgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50XCI7XG5pbXBvcnQgRXh0cmFJbmZvIGZyb20gXCJAZGVzay1jb21wb25lbnQvcGgvY29tcG9ubmV0cy9leHRyYS1pbmZvXCI7XG5cbnR5cGUgUGF5ZXJTdGF0ZSA9IHt9O1xuXG5leHBvcnQgdHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxuZXhwb3J0IHR5cGUgUGF5ZXJQYWdlQ29tcG9uZW50cyA9IHtcbiAgQm94Q29udGVudDogU3R5bGVkRElWO1xufTtcbmV4cG9ydCB0eXBlIFBheWVyUHJvcHMgPSB7XG4gIG1vZGVsOiBhbnk7XG4gIGZvcm06IGFueTtcbiAgZGF0YUZpeGVkPzogYW55O1xuICBwcm9wc05hbWVGaXhlZD86IGFueTtcbiAgaXNQYXJ0eVR5cGU/OiBib29sZWFuO1xuICBwYXJ0eVR5cGU/OiBcIklORElcIiB8IFwiT1JHXCIgfCB1bmRlZmluZWQ7XG4gIHR5cGU/OiBcIkVYVFJBLUlORk9cIiB8IHVuZGVmaW5lZDtcbiAgZXh0cmFBcnJPYmo/OiBhbnlbXTtcbiAgaXNDb3VudHJ5PzogYm9vbGVhbjtcbiAgY2hpbGREb20/OiBhbnk7XG4gIG5vdERvYj86IGJvb2xlYW47XG4gIGRlZmF1bHRDb3VudHJ5Q29kZT86IGFueTtcbn0gJiBNb2RlbFdpZGdldFByb3BzO1xuXG5jbGFzcyBQb2xpY3lob2xkZXI8UCBleHRlbmRzIFBheWVyUHJvcHMsIFMgZXh0ZW5kcyBQYXllclN0YXRlLCBDIGV4dGVuZHMgUGF5ZXJQYWdlQ29tcG9uZW50cz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG5cbiAgcXVlcnlQYWdlOiBhbnkgPSB7fTtcbiAgZG9iUGFnZTogYW55ID0ge307XG5cbiAgcHJvdGVjdGVkIGdyb3VwQ3VzdG9tZXJQYWdlOiBhbnk7XG4gIHByb3RlY3RlZCB0aW1lb3V0OiBhbnkgPSBudWxsO1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzOiBQYXllclByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgZ3JvdXBDdXN0b21lclJlZiA9IChyZWY6IGFueSkgPT4ge1xuICAgIHRoaXMuZ3JvdXBDdXN0b21lclBhZ2UgPSByZWY7XG4gIH07XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgaWYgKHRoaXMucHJvcHMucGFydHlUeXBlID09PSBcIk9SR1wiKSB7XG4gICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChcIk9GRklDRVwiLCB0aGlzLmdldERhdGFJZFByb3AoYGFkZHJlc3NUeXBlYCkpO1xuICAgIH1cbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIGlmICh0aGlzLnRpbWVvdXQpIGNsZWFySW50ZXJ2YWwodGhpcy50aW1lb3V0KTtcbiAgfVxuXG4gIGNoYW5nZUluZm8oKSB7XG4gICAgY29uc3QgeyBtb2RlbCwgZXh0cmFBcnJPYmosIHR5cGUgfSA9IHRoaXMucHJvcHM7XG4gICAgKFtcImVtYWlsXCIsIFwidGl0bGVcIiwgXCJuYW1lXCIsIFwibW9iaWxlXCIsIFwibW9iaWxlTmF0aW9uQ29kZVwiLCBcImlkVHlwZVwiLCBcImlkTm9cIiwgXCJhZGRyZXNzXCJdLmNvbmNhdCgoZXh0cmFBcnJPYmogfHwgW10pIGFzIGFueSkpXG4gICAgICAuZm9yRWFjaCgoaXRlbTogYW55KSA9PiB7XG4gICAgICAgIGNvbnN0IGluaXRWYWx1ZSA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXREYXRhSWRQcm9wKGAke2l0ZW19YCkpO1xuICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChpbml0VmFsdWUgfHwgXCJcIiwgdGhpcy5nZXREYXRhSWRQcm9wKGAke2l0ZW19YCkpO1xuICAgICAgICB0aGlzLnByb3BzLmZvcm0uc2V0RmllbGRzVmFsdWUoeyBbdGhpcy5nZXREYXRhSWRQcm9wKGAke2l0ZW19YCldOiBpbml0VmFsdWUgfHwgXCJcIiB9KTtcbiAgICAgICAgY29uc3QgYWRkcmVzc1JlZkN1cnJlbnQ6IGFueSA9IF8uZ2V0KHRoaXMucXVlcnlQYWdlLCBcImlkTm9QYWdlXCIpO1xuICAgICAgICBjb25zdCBuYW1lUGFnZTogYW55ID0gXy5nZXQodGhpcy5ncm91cEN1c3RvbWVyUGFnZSwgXCJuYW1lUGFnZVwiKTtcbiAgICAgICAgY29uc3QgaWROb1BhZ2U6IGFueSA9IF8uZ2V0KHRoaXMuZ3JvdXBDdXN0b21lclBhZ2UsIFwiaWROb1BhZ2VcIik7XG4gICAgICAgIGlmIChfLmdldChtb2RlbCwgdGhpcy5nZXREYXRhSWRQcm9wKGBhZGRyZXNzLmNvdW50cnlDb2RlYCkpID09PSBcIlNHUFwiKSB7XG4gICAgICAgICAgYWRkcmVzc1JlZkN1cnJlbnQgJiYgYWRkcmVzc1JlZkN1cnJlbnQuZ2V0QWRkcmVzcyhfLmdldChtb2RlbCwgdGhpcy5nZXREYXRhSWRQcm9wKGBhZGRyZXNzLnBvc3RhbENvZGVgKSkpO1xuICAgICAgICB9XG4gICAgICAgIGlkTm9QYWdlICYmIGlkTm9QYWdlLnNldFNob3dMYWJlbCh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0RGF0YUlkUHJvcChgaWROb2ApKSk7XG4gICAgICAgIG5hbWVQYWdlICYmIG5hbWVQYWdlLnNldFNob3dMYWJlbCh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0RGF0YUlkUHJvcChgbmFtZWApKSk7XG4gICAgICAgIGlmICh0eXBlID09PSBcIkVYVFJBLUlORk9cIikge1xuICAgICAgICAgIHRoaXMuZG9iUGFnZSAmJiB0aGlzLmRvYlBhZ2Uuc2V0RG9iKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0RGF0YUlkUHJvcChwcm9wTmFtZT86IHN0cmluZykge1xuICAgIGNvbnN0IHsgZGF0YUZpeGVkLCBwcm9wc05hbWVGaXhlZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBleHRyYVByb3BzTmFtZSA9IHByb3BzTmFtZUZpeGVkID8gYCR7cHJvcHNOYW1lRml4ZWR9YCA6IGBleHQucG9saWN5aG9sZGVyYDtcbiAgICBjb25zdCBwcm9wc05hbWUgPSBkYXRhRml4ZWQgPyBgJHtkYXRhRml4ZWR9LiR7ZXh0cmFQcm9wc05hbWV9YCA6IGV4dHJhUHJvcHNOYW1lO1xuICAgIGlmIChwcm9wTmFtZSkgcmV0dXJuIGAke3Byb3BzTmFtZX0uJHtwcm9wTmFtZX1gO1xuICAgIHJldHVybiBgJHtwcm9wc05hbWV9YDtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3Qge1xuICAgICAgbW9kZWwsIGZvcm0sIGRhdGFGaXhlZCwgdHlwZSwgaXNQYXJ0eVR5cGUgPSB0cnVlLCBub3REb2IsIGRlZmF1bHRDb3VudHJ5Q29kZSxcbiAgICAgIGlzQ291bnRyeSxcbiAgICAgIHByb3BzTmFtZUZpeGVkLCBwYXJ0eVR5cGUsIGV4dHJhQXJyT2JqLCAuLi5yZXN0XG4gICAgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgdGhhdCA9IHRoaXM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDLkJveENvbnRlbnQgey4uLnJlc3R9PlxuICAgICAgICA8PlxuICAgICAgICAgIHtpc1BhcnR5VHlwZSAmJiA8TlJhZGlvXG4gICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICBkYXRhRml4ZWQ9e2RhdGFGaXhlZH1cbiAgICAgICAgICAgIG9wdGlvbnM9e1tcbiAgICAgICAgICAgICAgeyBpZDogXCJJTkRJXCIsIHRleHQ6IFwiSW5kaXZpZHVhbFwiIH0sXG4gICAgICAgICAgICAgIHsgaWQ6IFwiT1JHXCIsIHRleHQ6IFwiT3JnYW5pemF0aW9uXCIgfSxcbiAgICAgICAgICAgIF19XG4gICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZXREYXRhSWRQcm9wKGBwYXJ0eVR5cGVgKX1cbiAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlR5cGUgb2YgUG9saWN5aG9sZGVyXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwiVHlwZSBvZiBQb2xpY3lob2xkZXJcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXsoKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuY2hhbmdlSW5mbygpO1xuICAgICAgICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChcIlwiLCB0aGlzLmdldERhdGFJZFByb3AoYGlkVHlwZWApKTtcbiAgICAgICAgICAgICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHsgW3RoaXMuZ2V0RGF0YUlkUHJvcChgaWRUeXBlYCldOiBcIlwiIH0pO1xuICAgICAgICAgICAgfX1cbiAgICAgICAgICAvPn1cbiAgICAgICAgICA8R3JvdXBDdXN0b21lciBkYXRhRml4ZWQ9e2RhdGFGaXhlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICByZWY9e3RoaXMuZ3JvdXBDdXN0b21lclJlZn1cbiAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBub3REb2I9e25vdERvYn1cbiAgICAgICAgICAgICAgICAgICAgICAgICBleHRyYUFyck9iaj17ZXh0cmFBcnJPYmp9XG4gICAgICAgICAgICAgICAgICAgICAgICAgb25TZWxlY3RDaGFuZ2U9eyhpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuc2V0VmFsdWVzVG9Nb2RlbCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIFt0aGlzLmdldERhdGFJZFByb3AoYGFkZHJlc3NgKV06IGl0ZW0uYWRkcmVzcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYWRkcmVzc1JlZkN1cnJlbnQ6IGFueSA9IF8uZ2V0KHRoaXMucXVlcnlQYWdlLCBcImlkTm9QYWdlXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzQ291bnRyeSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnF1ZXJ5UGFnZSAmJiB0aGlzLnF1ZXJ5UGFnZS5jaGFuZ2VDb3VudHJ5KF8uZ2V0KGl0ZW0sIFwiYWRkcmVzcy5jb3VudHJ5Q29kZVwiLCBcIlwiKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoXy5nZXQoaXRlbS5hZGRyZXNzLCBcImNvdW50cnlDb2RlXCIpID09PSBcIlNHUFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFkZHJlc3NSZWZDdXJyZW50ICYmIGFkZHJlc3NSZWZDdXJyZW50LmdldEFkZHJlc3MoXy5nZXQoaXRlbSwgXCJhZGRyZXNzLnBvc3RhbENvZGVcIikpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgZ3JvdXBUeXBlPXtwYXJ0eVR5cGV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgZGF0YUlkPXt0aGlzLmdldERhdGFJZFByb3AoKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZUFkZHJlc3M9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hhbmdlSW5mbygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgcGFydHlUeXBlPXtwYXJ0eVR5cGV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgY291bnRyeUNvZGU9e1wiU0dQXCJ9Lz5cbiAgICAgICAgICB7dHlwZSA9PT0gXCJFWFRSQS1JTkZPXCIgJiYgPEV4dHJhSW5mbyBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlZj17dGhpcy5kb2JSZWZ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFGaXhlZD17ZGF0YUZpeGVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9wc05hbWVGaXhlZD17cHJvcHNOYW1lRml4ZWR9Lz59XG4gICAgICAgICAge3RoaXMucHJvcHMuY2hpbGREb20gfHwgXCJcIn1cbiAgICAgICAgICA8TW9iaWxlIGZvcm09e2Zvcm19IG1vZGVsPXttb2RlbH0gZGF0YUZpeGVkPXtkYXRhRml4ZWR9IGRhdGFJZD17dGhpcy5nZXREYXRhSWRQcm9wKCl9IGlzUEY9e3RydWV9Lz5cbiAgICAgICAgICA8Rm9ybUl0ZW1FbWFpbFxuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdldERhdGFJZFByb3AoYGVtYWlsYCl9XG4gICAgICAgICAgLz5cbiAgICAgICAgICB7cGFydHlUeXBlICE9PSBcIk9SR1wiICYmIDxOUmFkaW9cbiAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICB0YWJsZU5hbWU9XCJhZGRyZXNzdHlwZVwiXG4gICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdldERhdGFJZFByb3AoYGFkZHJlc3NUeXBlYCl9XG4gICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJBZGRyZXNzIFR5cGVcIilcbiAgICAgICAgICAgICAgLnRoYWkoXCLguKrguJbguLLguJnguJfguLXguYjguJfguLPguIfguLLguJkgKOC4geC4o+C4k+C4teC5hOC4oeC5iOC4oeC4tSDguYPguKvguYnguYDguKXguLfguK3guIHguJfguLXguYjguK3guKLguLnguYjguJvguLHguIjguIjguLjguJrguLHguJkpXCIpXG4gICAgICAgICAgICAgIC5teShcIuGAnOGAreGAleGAuuGAheGArOGAoeGAmeGAu+GAreGAr+GAuOGAoeGAheGArOGAuFwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgfVxuICAgICAgICAgIDxDb21tb25BZGRyZXNzIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgICAgIHJlZj17dGhpcy5hZGRyZXNzUmVmfVxuICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRDb3VudHJ5Q29kZT17aXNDb3VudHJ5ID8gZGVmYXVsdENvdW50cnlDb2RlIDogKGRlZmF1bHRDb3VudHJ5Q29kZSB8fCBcIlNHUFwiKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhSWRPclByZWZpeD17dGhpcy5nZXREYXRhSWRQcm9wKCl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgcHJvcExhYmVsPXtcIkFkZHJlc3NcIn1cbiAgICAgICAgICAvPlxuICAgICAgICA8Lz5cbiAgICAgIDwvQy5Cb3hDb250ZW50PlxuICAgICk7XG4gIH1cblxuICBhZGRyZXNzUmVmID0gKHJlZjogYW55KSA9PiB7XG4gICAgdGhpcy5xdWVyeVBhZ2UgPSByZWY7XG4gIH07XG5cbiAgZG9iUmVmID0gKHJlZjogYW55KSA9PiB7XG4gICAgdGhpcy5kb2JQYWdlID0gcmVmO1xuICB9O1xuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94Q29udGVudDogU3R5bGVkLmRpdmBcbiAgICAgICAgICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgZW50aXR5T3B0aW9uczogW10sXG4gICAgfSkgYXMgUztcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWZhdWx0IFBvbGljeWhvbGRlcjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQXVCQTs7Ozs7QUFRQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFQQTtBQTJJQTtBQUNBO0FBQ0E7QUE3SUE7QUErSUE7QUFDQTtBQUNBO0FBakpBO0FBRUE7QUFDQTs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQWpCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXhCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTs7O0FBVUE7QUFDQTtBQUNBO0FBREE7QUFJQTs7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTs7OztBQXJLQTtBQUNBO0FBd0tBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/ph/policyholder.tsx
