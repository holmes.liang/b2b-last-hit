var PathProxy = __webpack_require__(/*! ../core/PathProxy */ "./node_modules/zrender/lib/core/PathProxy.js");

var line = __webpack_require__(/*! ./line */ "./node_modules/zrender/lib/contain/line.js");

var cubic = __webpack_require__(/*! ./cubic */ "./node_modules/zrender/lib/contain/cubic.js");

var quadratic = __webpack_require__(/*! ./quadratic */ "./node_modules/zrender/lib/contain/quadratic.js");

var arc = __webpack_require__(/*! ./arc */ "./node_modules/zrender/lib/contain/arc.js");

var _util = __webpack_require__(/*! ./util */ "./node_modules/zrender/lib/contain/util.js");

var normalizeRadian = _util.normalizeRadian;

var curve = __webpack_require__(/*! ../core/curve */ "./node_modules/zrender/lib/core/curve.js");

var windingLine = __webpack_require__(/*! ./windingLine */ "./node_modules/zrender/lib/contain/windingLine.js");

var CMD = PathProxy.CMD;
var PI2 = Math.PI * 2;
var EPSILON = 1e-4;

function isAroundEqual(a, b) {
  return Math.abs(a - b) < EPSILON;
} // 临时数组


var roots = [-1, -1, -1];
var extrema = [-1, -1];

function swapExtrema() {
  var tmp = extrema[0];
  extrema[0] = extrema[1];
  extrema[1] = tmp;
}

function windingCubic(x0, y0, x1, y1, x2, y2, x3, y3, x, y) {
  // Quick reject
  if (y > y0 && y > y1 && y > y2 && y > y3 || y < y0 && y < y1 && y < y2 && y < y3) {
    return 0;
  }

  var nRoots = curve.cubicRootAt(y0, y1, y2, y3, y, roots);

  if (nRoots === 0) {
    return 0;
  } else {
    var w = 0;
    var nExtrema = -1;
    var y0_;
    var y1_;

    for (var i = 0; i < nRoots; i++) {
      var t = roots[i]; // Avoid winding error when intersection point is the connect point of two line of polygon

      var unit = t === 0 || t === 1 ? 0.5 : 1;
      var x_ = curve.cubicAt(x0, x1, x2, x3, t);

      if (x_ < x) {
        // Quick reject
        continue;
      }

      if (nExtrema < 0) {
        nExtrema = curve.cubicExtrema(y0, y1, y2, y3, extrema);

        if (extrema[1] < extrema[0] && nExtrema > 1) {
          swapExtrema();
        }

        y0_ = curve.cubicAt(y0, y1, y2, y3, extrema[0]);

        if (nExtrema > 1) {
          y1_ = curve.cubicAt(y0, y1, y2, y3, extrema[1]);
        }
      }

      if (nExtrema === 2) {
        // 分成三段单调函数
        if (t < extrema[0]) {
          w += y0_ < y0 ? unit : -unit;
        } else if (t < extrema[1]) {
          w += y1_ < y0_ ? unit : -unit;
        } else {
          w += y3 < y1_ ? unit : -unit;
        }
      } else {
        // 分成两段单调函数
        if (t < extrema[0]) {
          w += y0_ < y0 ? unit : -unit;
        } else {
          w += y3 < y0_ ? unit : -unit;
        }
      }
    }

    return w;
  }
}

function windingQuadratic(x0, y0, x1, y1, x2, y2, x, y) {
  // Quick reject
  if (y > y0 && y > y1 && y > y2 || y < y0 && y < y1 && y < y2) {
    return 0;
  }

  var nRoots = curve.quadraticRootAt(y0, y1, y2, y, roots);

  if (nRoots === 0) {
    return 0;
  } else {
    var t = curve.quadraticExtremum(y0, y1, y2);

    if (t >= 0 && t <= 1) {
      var w = 0;
      var y_ = curve.quadraticAt(y0, y1, y2, t);

      for (var i = 0; i < nRoots; i++) {
        // Remove one endpoint.
        var unit = roots[i] === 0 || roots[i] === 1 ? 0.5 : 1;
        var x_ = curve.quadraticAt(x0, x1, x2, roots[i]);

        if (x_ < x) {
          // Quick reject
          continue;
        }

        if (roots[i] < t) {
          w += y_ < y0 ? unit : -unit;
        } else {
          w += y2 < y_ ? unit : -unit;
        }
      }

      return w;
    } else {
      // Remove one endpoint.
      var unit = roots[0] === 0 || roots[0] === 1 ? 0.5 : 1;
      var x_ = curve.quadraticAt(x0, x1, x2, roots[0]);

      if (x_ < x) {
        // Quick reject
        return 0;
      }

      return y2 < y0 ? unit : -unit;
    }
  }
} // TODO
// Arc 旋转


function windingArc(cx, cy, r, startAngle, endAngle, anticlockwise, x, y) {
  y -= cy;

  if (y > r || y < -r) {
    return 0;
  }

  var tmp = Math.sqrt(r * r - y * y);
  roots[0] = -tmp;
  roots[1] = tmp;
  var diff = Math.abs(startAngle - endAngle);

  if (diff < 1e-4) {
    return 0;
  }

  if (diff % PI2 < 1e-4) {
    // Is a circle
    startAngle = 0;
    endAngle = PI2;
    var dir = anticlockwise ? 1 : -1;

    if (x >= roots[0] + cx && x <= roots[1] + cx) {
      return dir;
    } else {
      return 0;
    }
  }

  if (anticlockwise) {
    var tmp = startAngle;
    startAngle = normalizeRadian(endAngle);
    endAngle = normalizeRadian(tmp);
  } else {
    startAngle = normalizeRadian(startAngle);
    endAngle = normalizeRadian(endAngle);
  }

  if (startAngle > endAngle) {
    endAngle += PI2;
  }

  var w = 0;

  for (var i = 0; i < 2; i++) {
    var x_ = roots[i];

    if (x_ + cx > x) {
      var angle = Math.atan2(y, x_);
      var dir = anticlockwise ? 1 : -1;

      if (angle < 0) {
        angle = PI2 + angle;
      }

      if (angle >= startAngle && angle <= endAngle || angle + PI2 >= startAngle && angle + PI2 <= endAngle) {
        if (angle > Math.PI / 2 && angle < Math.PI * 1.5) {
          dir = -dir;
        }

        w += dir;
      }
    }
  }

  return w;
}

function containPath(data, lineWidth, isStroke, x, y) {
  var w = 0;
  var xi = 0;
  var yi = 0;
  var x0 = 0;
  var y0 = 0;

  for (var i = 0; i < data.length;) {
    var cmd = data[i++]; // Begin a new subpath

    if (cmd === CMD.M && i > 1) {
      // Close previous subpath
      if (!isStroke) {
        w += windingLine(xi, yi, x0, y0, x, y);
      } // 如果被任何一个 subpath 包含
      // if (w !== 0) {
      //     return true;
      // }

    }

    if (i === 1) {
      // 如果第一个命令是 L, C, Q
      // 则 previous point 同绘制命令的第一个 point
      //
      // 第一个命令为 Arc 的情况下会在后面特殊处理
      xi = data[i];
      yi = data[i + 1];
      x0 = xi;
      y0 = yi;
    }

    switch (cmd) {
      case CMD.M:
        // moveTo 命令重新创建一个新的 subpath, 并且更新新的起点
        // 在 closePath 的时候使用
        x0 = data[i++];
        y0 = data[i++];
        xi = x0;
        yi = y0;
        break;

      case CMD.L:
        if (isStroke) {
          if (line.containStroke(xi, yi, data[i], data[i + 1], lineWidth, x, y)) {
            return true;
          }
        } else {
          // NOTE 在第一个命令为 L, C, Q 的时候会计算出 NaN
          w += windingLine(xi, yi, data[i], data[i + 1], x, y) || 0;
        }

        xi = data[i++];
        yi = data[i++];
        break;

      case CMD.C:
        if (isStroke) {
          if (cubic.containStroke(xi, yi, data[i++], data[i++], data[i++], data[i++], data[i], data[i + 1], lineWidth, x, y)) {
            return true;
          }
        } else {
          w += windingCubic(xi, yi, data[i++], data[i++], data[i++], data[i++], data[i], data[i + 1], x, y) || 0;
        }

        xi = data[i++];
        yi = data[i++];
        break;

      case CMD.Q:
        if (isStroke) {
          if (quadratic.containStroke(xi, yi, data[i++], data[i++], data[i], data[i + 1], lineWidth, x, y)) {
            return true;
          }
        } else {
          w += windingQuadratic(xi, yi, data[i++], data[i++], data[i], data[i + 1], x, y) || 0;
        }

        xi = data[i++];
        yi = data[i++];
        break;

      case CMD.A:
        // TODO Arc 判断的开销比较大
        var cx = data[i++];
        var cy = data[i++];
        var rx = data[i++];
        var ry = data[i++];
        var theta = data[i++];
        var dTheta = data[i++]; // TODO Arc 旋转

        i += 1;
        var anticlockwise = 1 - data[i++];
        var x1 = Math.cos(theta) * rx + cx;
        var y1 = Math.sin(theta) * ry + cy; // 不是直接使用 arc 命令

        if (i > 1) {
          w += windingLine(xi, yi, x1, y1, x, y);
        } else {
          // 第一个命令起点还未定义
          x0 = x1;
          y0 = y1;
        } // zr 使用scale来模拟椭圆, 这里也对x做一定的缩放


        var _x = (x - cx) * ry / rx + cx;

        if (isStroke) {
          if (arc.containStroke(cx, cy, ry, theta, theta + dTheta, anticlockwise, lineWidth, _x, y)) {
            return true;
          }
        } else {
          w += windingArc(cx, cy, ry, theta, theta + dTheta, anticlockwise, _x, y);
        }

        xi = Math.cos(theta + dTheta) * rx + cx;
        yi = Math.sin(theta + dTheta) * ry + cy;
        break;

      case CMD.R:
        x0 = xi = data[i++];
        y0 = yi = data[i++];
        var width = data[i++];
        var height = data[i++];
        var x1 = x0 + width;
        var y1 = y0 + height;

        if (isStroke) {
          if (line.containStroke(x0, y0, x1, y0, lineWidth, x, y) || line.containStroke(x1, y0, x1, y1, lineWidth, x, y) || line.containStroke(x1, y1, x0, y1, lineWidth, x, y) || line.containStroke(x0, y1, x0, y0, lineWidth, x, y)) {
            return true;
          }
        } else {
          // FIXME Clockwise ?
          w += windingLine(x1, y0, x1, y1, x, y);
          w += windingLine(x0, y1, x0, y0, x, y);
        }

        break;

      case CMD.Z:
        if (isStroke) {
          if (line.containStroke(xi, yi, x0, y0, lineWidth, x, y)) {
            return true;
          }
        } else {
          // Close a subpath
          w += windingLine(xi, yi, x0, y0, x, y); // 如果被任何一个 subpath 包含
          // FIXME subpaths may overlap
          // if (w !== 0) {
          //     return true;
          // }
        }

        xi = x0;
        yi = y0;
        break;
    }
  }

  if (!isStroke && !isAroundEqual(yi, y0)) {
    w += windingLine(xi, yi, x0, y0, x, y) || 0;
  }

  return w !== 0;
}

function contain(pathData, x, y) {
  return containPath(pathData, 0, false, x, y);
}

function containStroke(pathData, lineWidth, x, y) {
  return containPath(pathData, lineWidth, true, x, y);
}

exports.contain = contain;
exports.containStroke = containStroke;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi9wYXRoLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi9wYXRoLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBQYXRoUHJveHkgPSByZXF1aXJlKFwiLi4vY29yZS9QYXRoUHJveHlcIik7XG5cbnZhciBsaW5lID0gcmVxdWlyZShcIi4vbGluZVwiKTtcblxudmFyIGN1YmljID0gcmVxdWlyZShcIi4vY3ViaWNcIik7XG5cbnZhciBxdWFkcmF0aWMgPSByZXF1aXJlKFwiLi9xdWFkcmF0aWNcIik7XG5cbnZhciBhcmMgPSByZXF1aXJlKFwiLi9hcmNcIik7XG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCIuL3V0aWxcIik7XG5cbnZhciBub3JtYWxpemVSYWRpYW4gPSBfdXRpbC5ub3JtYWxpemVSYWRpYW47XG5cbnZhciBjdXJ2ZSA9IHJlcXVpcmUoXCIuLi9jb3JlL2N1cnZlXCIpO1xuXG52YXIgd2luZGluZ0xpbmUgPSByZXF1aXJlKFwiLi93aW5kaW5nTGluZVwiKTtcblxudmFyIENNRCA9IFBhdGhQcm94eS5DTUQ7XG52YXIgUEkyID0gTWF0aC5QSSAqIDI7XG52YXIgRVBTSUxPTiA9IDFlLTQ7XG5cbmZ1bmN0aW9uIGlzQXJvdW5kRXF1YWwoYSwgYikge1xuICByZXR1cm4gTWF0aC5hYnMoYSAtIGIpIDwgRVBTSUxPTjtcbn0gLy8g5Li05pe25pWw57uEXG5cblxudmFyIHJvb3RzID0gWy0xLCAtMSwgLTFdO1xudmFyIGV4dHJlbWEgPSBbLTEsIC0xXTtcblxuZnVuY3Rpb24gc3dhcEV4dHJlbWEoKSB7XG4gIHZhciB0bXAgPSBleHRyZW1hWzBdO1xuICBleHRyZW1hWzBdID0gZXh0cmVtYVsxXTtcbiAgZXh0cmVtYVsxXSA9IHRtcDtcbn1cblxuZnVuY3Rpb24gd2luZGluZ0N1YmljKHgwLCB5MCwgeDEsIHkxLCB4MiwgeTIsIHgzLCB5MywgeCwgeSkge1xuICAvLyBRdWljayByZWplY3RcbiAgaWYgKHkgPiB5MCAmJiB5ID4geTEgJiYgeSA+IHkyICYmIHkgPiB5MyB8fCB5IDwgeTAgJiYgeSA8IHkxICYmIHkgPCB5MiAmJiB5IDwgeTMpIHtcbiAgICByZXR1cm4gMDtcbiAgfVxuXG4gIHZhciBuUm9vdHMgPSBjdXJ2ZS5jdWJpY1Jvb3RBdCh5MCwgeTEsIHkyLCB5MywgeSwgcm9vdHMpO1xuXG4gIGlmIChuUm9vdHMgPT09IDApIHtcbiAgICByZXR1cm4gMDtcbiAgfSBlbHNlIHtcbiAgICB2YXIgdyA9IDA7XG4gICAgdmFyIG5FeHRyZW1hID0gLTE7XG4gICAgdmFyIHkwXztcbiAgICB2YXIgeTFfO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBuUm9vdHM7IGkrKykge1xuICAgICAgdmFyIHQgPSByb290c1tpXTsgLy8gQXZvaWQgd2luZGluZyBlcnJvciB3aGVuIGludGVyc2VjdGlvbiBwb2ludCBpcyB0aGUgY29ubmVjdCBwb2ludCBvZiB0d28gbGluZSBvZiBwb2x5Z29uXG5cbiAgICAgIHZhciB1bml0ID0gdCA9PT0gMCB8fCB0ID09PSAxID8gMC41IDogMTtcbiAgICAgIHZhciB4XyA9IGN1cnZlLmN1YmljQXQoeDAsIHgxLCB4MiwgeDMsIHQpO1xuXG4gICAgICBpZiAoeF8gPCB4KSB7XG4gICAgICAgIC8vIFF1aWNrIHJlamVjdFxuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgaWYgKG5FeHRyZW1hIDwgMCkge1xuICAgICAgICBuRXh0cmVtYSA9IGN1cnZlLmN1YmljRXh0cmVtYSh5MCwgeTEsIHkyLCB5MywgZXh0cmVtYSk7XG5cbiAgICAgICAgaWYgKGV4dHJlbWFbMV0gPCBleHRyZW1hWzBdICYmIG5FeHRyZW1hID4gMSkge1xuICAgICAgICAgIHN3YXBFeHRyZW1hKCk7XG4gICAgICAgIH1cblxuICAgICAgICB5MF8gPSBjdXJ2ZS5jdWJpY0F0KHkwLCB5MSwgeTIsIHkzLCBleHRyZW1hWzBdKTtcblxuICAgICAgICBpZiAobkV4dHJlbWEgPiAxKSB7XG4gICAgICAgICAgeTFfID0gY3VydmUuY3ViaWNBdCh5MCwgeTEsIHkyLCB5MywgZXh0cmVtYVsxXSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKG5FeHRyZW1hID09PSAyKSB7XG4gICAgICAgIC8vIOWIhuaIkOS4ieauteWNleiwg+WHveaVsFxuICAgICAgICBpZiAodCA8IGV4dHJlbWFbMF0pIHtcbiAgICAgICAgICB3ICs9IHkwXyA8IHkwID8gdW5pdCA6IC11bml0O1xuICAgICAgICB9IGVsc2UgaWYgKHQgPCBleHRyZW1hWzFdKSB7XG4gICAgICAgICAgdyArPSB5MV8gPCB5MF8gPyB1bml0IDogLXVuaXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdyArPSB5MyA8IHkxXyA/IHVuaXQgOiAtdW5pdDtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8g5YiG5oiQ5Lik5q615Y2V6LCD5Ye95pWwXG4gICAgICAgIGlmICh0IDwgZXh0cmVtYVswXSkge1xuICAgICAgICAgIHcgKz0geTBfIDwgeTAgPyB1bml0IDogLXVuaXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdyArPSB5MyA8IHkwXyA/IHVuaXQgOiAtdW5pdDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB3O1xuICB9XG59XG5cbmZ1bmN0aW9uIHdpbmRpbmdRdWFkcmF0aWMoeDAsIHkwLCB4MSwgeTEsIHgyLCB5MiwgeCwgeSkge1xuICAvLyBRdWljayByZWplY3RcbiAgaWYgKHkgPiB5MCAmJiB5ID4geTEgJiYgeSA+IHkyIHx8IHkgPCB5MCAmJiB5IDwgeTEgJiYgeSA8IHkyKSB7XG4gICAgcmV0dXJuIDA7XG4gIH1cblxuICB2YXIgblJvb3RzID0gY3VydmUucXVhZHJhdGljUm9vdEF0KHkwLCB5MSwgeTIsIHksIHJvb3RzKTtcblxuICBpZiAoblJvb3RzID09PSAwKSB7XG4gICAgcmV0dXJuIDA7XG4gIH0gZWxzZSB7XG4gICAgdmFyIHQgPSBjdXJ2ZS5xdWFkcmF0aWNFeHRyZW11bSh5MCwgeTEsIHkyKTtcblxuICAgIGlmICh0ID49IDAgJiYgdCA8PSAxKSB7XG4gICAgICB2YXIgdyA9IDA7XG4gICAgICB2YXIgeV8gPSBjdXJ2ZS5xdWFkcmF0aWNBdCh5MCwgeTEsIHkyLCB0KTtcblxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBuUm9vdHM7IGkrKykge1xuICAgICAgICAvLyBSZW1vdmUgb25lIGVuZHBvaW50LlxuICAgICAgICB2YXIgdW5pdCA9IHJvb3RzW2ldID09PSAwIHx8IHJvb3RzW2ldID09PSAxID8gMC41IDogMTtcbiAgICAgICAgdmFyIHhfID0gY3VydmUucXVhZHJhdGljQXQoeDAsIHgxLCB4Miwgcm9vdHNbaV0pO1xuXG4gICAgICAgIGlmICh4XyA8IHgpIHtcbiAgICAgICAgICAvLyBRdWljayByZWplY3RcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChyb290c1tpXSA8IHQpIHtcbiAgICAgICAgICB3ICs9IHlfIDwgeTAgPyB1bml0IDogLXVuaXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdyArPSB5MiA8IHlfID8gdW5pdCA6IC11bml0O1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB3O1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBSZW1vdmUgb25lIGVuZHBvaW50LlxuICAgICAgdmFyIHVuaXQgPSByb290c1swXSA9PT0gMCB8fCByb290c1swXSA9PT0gMSA/IDAuNSA6IDE7XG4gICAgICB2YXIgeF8gPSBjdXJ2ZS5xdWFkcmF0aWNBdCh4MCwgeDEsIHgyLCByb290c1swXSk7XG5cbiAgICAgIGlmICh4XyA8IHgpIHtcbiAgICAgICAgLy8gUXVpY2sgcmVqZWN0XG4gICAgICAgIHJldHVybiAwO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4geTIgPCB5MCA/IHVuaXQgOiAtdW5pdDtcbiAgICB9XG4gIH1cbn0gLy8gVE9ET1xuLy8gQXJjIOaXi+i9rFxuXG5cbmZ1bmN0aW9uIHdpbmRpbmdBcmMoY3gsIGN5LCByLCBzdGFydEFuZ2xlLCBlbmRBbmdsZSwgYW50aWNsb2Nrd2lzZSwgeCwgeSkge1xuICB5IC09IGN5O1xuXG4gIGlmICh5ID4gciB8fCB5IDwgLXIpIHtcbiAgICByZXR1cm4gMDtcbiAgfVxuXG4gIHZhciB0bXAgPSBNYXRoLnNxcnQociAqIHIgLSB5ICogeSk7XG4gIHJvb3RzWzBdID0gLXRtcDtcbiAgcm9vdHNbMV0gPSB0bXA7XG4gIHZhciBkaWZmID0gTWF0aC5hYnMoc3RhcnRBbmdsZSAtIGVuZEFuZ2xlKTtcblxuICBpZiAoZGlmZiA8IDFlLTQpIHtcbiAgICByZXR1cm4gMDtcbiAgfVxuXG4gIGlmIChkaWZmICUgUEkyIDwgMWUtNCkge1xuICAgIC8vIElzIGEgY2lyY2xlXG4gICAgc3RhcnRBbmdsZSA9IDA7XG4gICAgZW5kQW5nbGUgPSBQSTI7XG4gICAgdmFyIGRpciA9IGFudGljbG9ja3dpc2UgPyAxIDogLTE7XG5cbiAgICBpZiAoeCA+PSByb290c1swXSArIGN4ICYmIHggPD0gcm9vdHNbMV0gKyBjeCkge1xuICAgICAgcmV0dXJuIGRpcjtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfVxuICB9XG5cbiAgaWYgKGFudGljbG9ja3dpc2UpIHtcbiAgICB2YXIgdG1wID0gc3RhcnRBbmdsZTtcbiAgICBzdGFydEFuZ2xlID0gbm9ybWFsaXplUmFkaWFuKGVuZEFuZ2xlKTtcbiAgICBlbmRBbmdsZSA9IG5vcm1hbGl6ZVJhZGlhbih0bXApO1xuICB9IGVsc2Uge1xuICAgIHN0YXJ0QW5nbGUgPSBub3JtYWxpemVSYWRpYW4oc3RhcnRBbmdsZSk7XG4gICAgZW5kQW5nbGUgPSBub3JtYWxpemVSYWRpYW4oZW5kQW5nbGUpO1xuICB9XG5cbiAgaWYgKHN0YXJ0QW5nbGUgPiBlbmRBbmdsZSkge1xuICAgIGVuZEFuZ2xlICs9IFBJMjtcbiAgfVxuXG4gIHZhciB3ID0gMDtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IDI7IGkrKykge1xuICAgIHZhciB4XyA9IHJvb3RzW2ldO1xuXG4gICAgaWYgKHhfICsgY3ggPiB4KSB7XG4gICAgICB2YXIgYW5nbGUgPSBNYXRoLmF0YW4yKHksIHhfKTtcbiAgICAgIHZhciBkaXIgPSBhbnRpY2xvY2t3aXNlID8gMSA6IC0xO1xuXG4gICAgICBpZiAoYW5nbGUgPCAwKSB7XG4gICAgICAgIGFuZ2xlID0gUEkyICsgYW5nbGU7XG4gICAgICB9XG5cbiAgICAgIGlmIChhbmdsZSA+PSBzdGFydEFuZ2xlICYmIGFuZ2xlIDw9IGVuZEFuZ2xlIHx8IGFuZ2xlICsgUEkyID49IHN0YXJ0QW5nbGUgJiYgYW5nbGUgKyBQSTIgPD0gZW5kQW5nbGUpIHtcbiAgICAgICAgaWYgKGFuZ2xlID4gTWF0aC5QSSAvIDIgJiYgYW5nbGUgPCBNYXRoLlBJICogMS41KSB7XG4gICAgICAgICAgZGlyID0gLWRpcjtcbiAgICAgICAgfVxuXG4gICAgICAgIHcgKz0gZGlyO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB3O1xufVxuXG5mdW5jdGlvbiBjb250YWluUGF0aChkYXRhLCBsaW5lV2lkdGgsIGlzU3Ryb2tlLCB4LCB5KSB7XG4gIHZhciB3ID0gMDtcbiAgdmFyIHhpID0gMDtcbiAgdmFyIHlpID0gMDtcbiAgdmFyIHgwID0gMDtcbiAgdmFyIHkwID0gMDtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGRhdGEubGVuZ3RoOykge1xuICAgIHZhciBjbWQgPSBkYXRhW2krK107IC8vIEJlZ2luIGEgbmV3IHN1YnBhdGhcblxuICAgIGlmIChjbWQgPT09IENNRC5NICYmIGkgPiAxKSB7XG4gICAgICAvLyBDbG9zZSBwcmV2aW91cyBzdWJwYXRoXG4gICAgICBpZiAoIWlzU3Ryb2tlKSB7XG4gICAgICAgIHcgKz0gd2luZGluZ0xpbmUoeGksIHlpLCB4MCwgeTAsIHgsIHkpO1xuICAgICAgfSAvLyDlpoLmnpzooqvku7vkvZXkuIDkuKogc3VicGF0aCDljIXlkKtcbiAgICAgIC8vIGlmICh3ICE9PSAwKSB7XG4gICAgICAvLyAgICAgcmV0dXJuIHRydWU7XG4gICAgICAvLyB9XG5cbiAgICB9XG5cbiAgICBpZiAoaSA9PT0gMSkge1xuICAgICAgLy8g5aaC5p6c56ys5LiA5Liq5ZG95Luk5pivIEwsIEMsIFFcbiAgICAgIC8vIOWImSBwcmV2aW91cyBwb2ludCDlkIznu5jliLblkb3ku6TnmoTnrKzkuIDkuKogcG9pbnRcbiAgICAgIC8vXG4gICAgICAvLyDnrKzkuIDkuKrlkb3ku6TkuLogQXJjIOeahOaDheWGteS4i+S8muWcqOWQjumdoueJueauiuWkhOeQhlxuICAgICAgeGkgPSBkYXRhW2ldO1xuICAgICAgeWkgPSBkYXRhW2kgKyAxXTtcbiAgICAgIHgwID0geGk7XG4gICAgICB5MCA9IHlpO1xuICAgIH1cblxuICAgIHN3aXRjaCAoY21kKSB7XG4gICAgICBjYXNlIENNRC5NOlxuICAgICAgICAvLyBtb3ZlVG8g5ZG95Luk6YeN5paw5Yib5bu65LiA5Liq5paw55qEIHN1YnBhdGgsIOW5tuS4lOabtOaWsOaWsOeahOi1t+eCuVxuICAgICAgICAvLyDlnKggY2xvc2VQYXRoIOeahOaXtuWAmeS9v+eUqFxuICAgICAgICB4MCA9IGRhdGFbaSsrXTtcbiAgICAgICAgeTAgPSBkYXRhW2krK107XG4gICAgICAgIHhpID0geDA7XG4gICAgICAgIHlpID0geTA7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIENNRC5MOlxuICAgICAgICBpZiAoaXNTdHJva2UpIHtcbiAgICAgICAgICBpZiAobGluZS5jb250YWluU3Ryb2tlKHhpLCB5aSwgZGF0YVtpXSwgZGF0YVtpICsgMV0sIGxpbmVXaWR0aCwgeCwgeSkpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBOT1RFIOWcqOesrOS4gOS4quWRveS7pOS4uiBMLCBDLCBRIOeahOaXtuWAmeS8muiuoeeul+WHuiBOYU5cbiAgICAgICAgICB3ICs9IHdpbmRpbmdMaW5lKHhpLCB5aSwgZGF0YVtpXSwgZGF0YVtpICsgMV0sIHgsIHkpIHx8IDA7XG4gICAgICAgIH1cblxuICAgICAgICB4aSA9IGRhdGFbaSsrXTtcbiAgICAgICAgeWkgPSBkYXRhW2krK107XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIENNRC5DOlxuICAgICAgICBpZiAoaXNTdHJva2UpIHtcbiAgICAgICAgICBpZiAoY3ViaWMuY29udGFpblN0cm9rZSh4aSwgeWksIGRhdGFbaSsrXSwgZGF0YVtpKytdLCBkYXRhW2krK10sIGRhdGFbaSsrXSwgZGF0YVtpXSwgZGF0YVtpICsgMV0sIGxpbmVXaWR0aCwgeCwgeSkpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB3ICs9IHdpbmRpbmdDdWJpYyh4aSwgeWksIGRhdGFbaSsrXSwgZGF0YVtpKytdLCBkYXRhW2krK10sIGRhdGFbaSsrXSwgZGF0YVtpXSwgZGF0YVtpICsgMV0sIHgsIHkpIHx8IDA7XG4gICAgICAgIH1cblxuICAgICAgICB4aSA9IGRhdGFbaSsrXTtcbiAgICAgICAgeWkgPSBkYXRhW2krK107XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIENNRC5ROlxuICAgICAgICBpZiAoaXNTdHJva2UpIHtcbiAgICAgICAgICBpZiAocXVhZHJhdGljLmNvbnRhaW5TdHJva2UoeGksIHlpLCBkYXRhW2krK10sIGRhdGFbaSsrXSwgZGF0YVtpXSwgZGF0YVtpICsgMV0sIGxpbmVXaWR0aCwgeCwgeSkpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB3ICs9IHdpbmRpbmdRdWFkcmF0aWMoeGksIHlpLCBkYXRhW2krK10sIGRhdGFbaSsrXSwgZGF0YVtpXSwgZGF0YVtpICsgMV0sIHgsIHkpIHx8IDA7XG4gICAgICAgIH1cblxuICAgICAgICB4aSA9IGRhdGFbaSsrXTtcbiAgICAgICAgeWkgPSBkYXRhW2krK107XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIENNRC5BOlxuICAgICAgICAvLyBUT0RPIEFyYyDliKTmlq3nmoTlvIDplIDmr5TovoPlpKdcbiAgICAgICAgdmFyIGN4ID0gZGF0YVtpKytdO1xuICAgICAgICB2YXIgY3kgPSBkYXRhW2krK107XG4gICAgICAgIHZhciByeCA9IGRhdGFbaSsrXTtcbiAgICAgICAgdmFyIHJ5ID0gZGF0YVtpKytdO1xuICAgICAgICB2YXIgdGhldGEgPSBkYXRhW2krK107XG4gICAgICAgIHZhciBkVGhldGEgPSBkYXRhW2krK107IC8vIFRPRE8gQXJjIOaXi+i9rFxuXG4gICAgICAgIGkgKz0gMTtcbiAgICAgICAgdmFyIGFudGljbG9ja3dpc2UgPSAxIC0gZGF0YVtpKytdO1xuICAgICAgICB2YXIgeDEgPSBNYXRoLmNvcyh0aGV0YSkgKiByeCArIGN4O1xuICAgICAgICB2YXIgeTEgPSBNYXRoLnNpbih0aGV0YSkgKiByeSArIGN5OyAvLyDkuI3mmK/nm7TmjqXkvb/nlKggYXJjIOWRveS7pFxuXG4gICAgICAgIGlmIChpID4gMSkge1xuICAgICAgICAgIHcgKz0gd2luZGluZ0xpbmUoeGksIHlpLCB4MSwgeTEsIHgsIHkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIOesrOS4gOS4quWRveS7pOi1t+eCuei/mOacquWumuS5iVxuICAgICAgICAgIHgwID0geDE7XG4gICAgICAgICAgeTAgPSB5MTtcbiAgICAgICAgfSAvLyB6ciDkvb/nlKhzY2FsZeadpeaooeaLn+akreWchiwg6L+Z6YeM5Lmf5a+5eOWBmuS4gOWumueahOe8qeaUvlxuXG5cbiAgICAgICAgdmFyIF94ID0gKHggLSBjeCkgKiByeSAvIHJ4ICsgY3g7XG5cbiAgICAgICAgaWYgKGlzU3Ryb2tlKSB7XG4gICAgICAgICAgaWYgKGFyYy5jb250YWluU3Ryb2tlKGN4LCBjeSwgcnksIHRoZXRhLCB0aGV0YSArIGRUaGV0YSwgYW50aWNsb2Nrd2lzZSwgbGluZVdpZHRoLCBfeCwgeSkpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB3ICs9IHdpbmRpbmdBcmMoY3gsIGN5LCByeSwgdGhldGEsIHRoZXRhICsgZFRoZXRhLCBhbnRpY2xvY2t3aXNlLCBfeCwgeSk7XG4gICAgICAgIH1cblxuICAgICAgICB4aSA9IE1hdGguY29zKHRoZXRhICsgZFRoZXRhKSAqIHJ4ICsgY3g7XG4gICAgICAgIHlpID0gTWF0aC5zaW4odGhldGEgKyBkVGhldGEpICogcnkgKyBjeTtcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgQ01ELlI6XG4gICAgICAgIHgwID0geGkgPSBkYXRhW2krK107XG4gICAgICAgIHkwID0geWkgPSBkYXRhW2krK107XG4gICAgICAgIHZhciB3aWR0aCA9IGRhdGFbaSsrXTtcbiAgICAgICAgdmFyIGhlaWdodCA9IGRhdGFbaSsrXTtcbiAgICAgICAgdmFyIHgxID0geDAgKyB3aWR0aDtcbiAgICAgICAgdmFyIHkxID0geTAgKyBoZWlnaHQ7XG5cbiAgICAgICAgaWYgKGlzU3Ryb2tlKSB7XG4gICAgICAgICAgaWYgKGxpbmUuY29udGFpblN0cm9rZSh4MCwgeTAsIHgxLCB5MCwgbGluZVdpZHRoLCB4LCB5KSB8fCBsaW5lLmNvbnRhaW5TdHJva2UoeDEsIHkwLCB4MSwgeTEsIGxpbmVXaWR0aCwgeCwgeSkgfHwgbGluZS5jb250YWluU3Ryb2tlKHgxLCB5MSwgeDAsIHkxLCBsaW5lV2lkdGgsIHgsIHkpIHx8IGxpbmUuY29udGFpblN0cm9rZSh4MCwgeTEsIHgwLCB5MCwgbGluZVdpZHRoLCB4LCB5KSkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIEZJWE1FIENsb2Nrd2lzZSA/XG4gICAgICAgICAgdyArPSB3aW5kaW5nTGluZSh4MSwgeTAsIHgxLCB5MSwgeCwgeSk7XG4gICAgICAgICAgdyArPSB3aW5kaW5nTGluZSh4MCwgeTEsIHgwLCB5MCwgeCwgeSk7XG4gICAgICAgIH1cblxuICAgICAgICBicmVhaztcblxuICAgICAgY2FzZSBDTUQuWjpcbiAgICAgICAgaWYgKGlzU3Ryb2tlKSB7XG4gICAgICAgICAgaWYgKGxpbmUuY29udGFpblN0cm9rZSh4aSwgeWksIHgwLCB5MCwgbGluZVdpZHRoLCB4LCB5KSkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIENsb3NlIGEgc3VicGF0aFxuICAgICAgICAgIHcgKz0gd2luZGluZ0xpbmUoeGksIHlpLCB4MCwgeTAsIHgsIHkpOyAvLyDlpoLmnpzooqvku7vkvZXkuIDkuKogc3VicGF0aCDljIXlkKtcbiAgICAgICAgICAvLyBGSVhNRSBzdWJwYXRocyBtYXkgb3ZlcmxhcFxuICAgICAgICAgIC8vIGlmICh3ICE9PSAwKSB7XG4gICAgICAgICAgLy8gICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIC8vIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHhpID0geDA7XG4gICAgICAgIHlpID0geTA7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIGlmICghaXNTdHJva2UgJiYgIWlzQXJvdW5kRXF1YWwoeWksIHkwKSkge1xuICAgIHcgKz0gd2luZGluZ0xpbmUoeGksIHlpLCB4MCwgeTAsIHgsIHkpIHx8IDA7XG4gIH1cblxuICByZXR1cm4gdyAhPT0gMDtcbn1cblxuZnVuY3Rpb24gY29udGFpbihwYXRoRGF0YSwgeCwgeSkge1xuICByZXR1cm4gY29udGFpblBhdGgocGF0aERhdGEsIDAsIGZhbHNlLCB4LCB5KTtcbn1cblxuZnVuY3Rpb24gY29udGFpblN0cm9rZShwYXRoRGF0YSwgbGluZVdpZHRoLCB4LCB5KSB7XG4gIHJldHVybiBjb250YWluUGF0aChwYXRoRGF0YSwgbGluZVdpZHRoLCB0cnVlLCB4LCB5KTtcbn1cblxuZXhwb3J0cy5jb250YWluID0gY29udGFpbjtcbmV4cG9ydHMuY29udGFpblN0cm9rZSA9IGNvbnRhaW5TdHJva2U7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTNIQTtBQTZIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/contain/path.js
