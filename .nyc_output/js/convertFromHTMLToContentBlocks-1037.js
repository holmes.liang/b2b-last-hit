/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule convertFromHTMLToContentBlocks
 * @format
 * 
 */


var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var _knownListItemDepthCl,
    _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var ContentBlock = __webpack_require__(/*! ./ContentBlock */ "./node_modules/draft-js/lib/ContentBlock.js");

var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var DefaultDraftBlockRenderMap = __webpack_require__(/*! ./DefaultDraftBlockRenderMap */ "./node_modules/draft-js/lib/DefaultDraftBlockRenderMap.js");

var DraftEntity = __webpack_require__(/*! ./DraftEntity */ "./node_modules/draft-js/lib/DraftEntity.js");

var DraftFeatureFlags = __webpack_require__(/*! ./DraftFeatureFlags */ "./node_modules/draft-js/lib/DraftFeatureFlags.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var _require = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js"),
    Set = _require.Set;

var URI = __webpack_require__(/*! fbjs/lib/URI */ "./node_modules/fbjs/lib/URI.js");

var cx = __webpack_require__(/*! fbjs/lib/cx */ "./node_modules/fbjs/lib/cx.js");

var generateRandomKey = __webpack_require__(/*! ./generateRandomKey */ "./node_modules/draft-js/lib/generateRandomKey.js");

var getSafeBodyFromHTML = __webpack_require__(/*! ./getSafeBodyFromHTML */ "./node_modules/draft-js/lib/getSafeBodyFromHTML.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var sanitizeDraftText = __webpack_require__(/*! ./sanitizeDraftText */ "./node_modules/draft-js/lib/sanitizeDraftText.js");

var experimentalTreeDataSupport = DraftFeatureFlags.draft_tree_data_support;
var List = Immutable.List,
    OrderedSet = Immutable.OrderedSet;
var NBSP = '&nbsp;';
var SPACE = ' '; // Arbitrary max indent

var MAX_DEPTH = 4; // used for replacing characters in HTML

var REGEX_CR = new RegExp('\r', 'g');
var REGEX_LF = new RegExp('\n', 'g');
var REGEX_NBSP = new RegExp(NBSP, 'g');
var REGEX_CARRIAGE = new RegExp('&#13;?', 'g');
var REGEX_ZWS = new RegExp('&#8203;?', 'g'); // https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight

var boldValues = ['bold', 'bolder', '500', '600', '700', '800', '900'];
var notBoldValues = ['light', 'lighter', '100', '200', '300', '400']; // Block tag flow is different because LIs do not have
// a deterministic style ;_;

var inlineTags = {
  b: 'BOLD',
  code: 'CODE',
  del: 'STRIKETHROUGH',
  em: 'ITALIC',
  i: 'ITALIC',
  s: 'STRIKETHROUGH',
  strike: 'STRIKETHROUGH',
  strong: 'BOLD',
  u: 'UNDERLINE'
};
var knownListItemDepthClasses = (_knownListItemDepthCl = {}, _defineProperty(_knownListItemDepthCl, cx('public/DraftStyleDefault/depth0'), 0), _defineProperty(_knownListItemDepthCl, cx('public/DraftStyleDefault/depth1'), 1), _defineProperty(_knownListItemDepthCl, cx('public/DraftStyleDefault/depth2'), 2), _defineProperty(_knownListItemDepthCl, cx('public/DraftStyleDefault/depth3'), 3), _defineProperty(_knownListItemDepthCl, cx('public/DraftStyleDefault/depth4'), 4), _knownListItemDepthCl);
var anchorAttr = ['className', 'href', 'rel', 'target', 'title'];
var imgAttr = ['alt', 'className', 'height', 'src', 'width'];
var lastBlock = void 0;
var EMPTY_CHUNK = {
  text: '',
  inlines: [],
  entities: [],
  blocks: []
};
var EMPTY_BLOCK = {
  children: List(),
  depth: 0,
  key: '',
  type: ''
};

var getListBlockType = function getListBlockType(tag, lastList) {
  if (tag === 'li') {
    return lastList === 'ol' ? 'ordered-list-item' : 'unordered-list-item';
  }

  return null;
};

var getBlockMapSupportedTags = function getBlockMapSupportedTags(blockRenderMap) {
  var unstyledElement = blockRenderMap.get('unstyled').element;
  var tags = Set([]);
  blockRenderMap.forEach(function (draftBlock) {
    if (draftBlock.aliasedElements) {
      draftBlock.aliasedElements.forEach(function (tag) {
        tags = tags.add(tag);
      });
    }

    tags = tags.add(draftBlock.element);
  });
  return tags.filter(function (tag) {
    return tag && tag !== unstyledElement;
  }).toArray().sort();
}; // custom element conversions


var getMultiMatchedType = function getMultiMatchedType(tag, lastList, multiMatchExtractor) {
  for (var ii = 0; ii < multiMatchExtractor.length; ii++) {
    var matchType = multiMatchExtractor[ii](tag, lastList);

    if (matchType) {
      return matchType;
    }
  }

  return null;
};

var getBlockTypeForTag = function getBlockTypeForTag(tag, lastList, blockRenderMap) {
  var matchedTypes = blockRenderMap.filter(function (draftBlock) {
    return draftBlock.element === tag || draftBlock.wrapper === tag || draftBlock.aliasedElements && draftBlock.aliasedElements.some(function (alias) {
      return alias === tag;
    });
  }).keySeq().toSet().toArray().sort(); // if we dont have any matched type, return unstyled
  // if we have one matched type return it
  // if we have multi matched types use the multi-match function to gather type

  switch (matchedTypes.length) {
    case 0:
      return 'unstyled';

    case 1:
      return matchedTypes[0];

    default:
      return getMultiMatchedType(tag, lastList, [getListBlockType]) || 'unstyled';
  }
};

var processInlineTag = function processInlineTag(tag, node, currentStyle) {
  var styleToCheck = inlineTags[tag];

  if (styleToCheck) {
    currentStyle = currentStyle.add(styleToCheck).toOrderedSet();
  } else if (node instanceof HTMLElement) {
    var htmlElement = node;
    currentStyle = currentStyle.withMutations(function (style) {
      var fontWeight = htmlElement.style.fontWeight;
      var fontStyle = htmlElement.style.fontStyle;
      var textDecoration = htmlElement.style.textDecoration;

      if (boldValues.indexOf(fontWeight) >= 0) {
        style.add('BOLD');
      } else if (notBoldValues.indexOf(fontWeight) >= 0) {
        style.remove('BOLD');
      }

      if (fontStyle === 'italic') {
        style.add('ITALIC');
      } else if (fontStyle === 'normal') {
        style.remove('ITALIC');
      }

      if (textDecoration === 'underline') {
        style.add('UNDERLINE');
      }

      if (textDecoration === 'line-through') {
        style.add('STRIKETHROUGH');
      }

      if (textDecoration === 'none') {
        style.remove('UNDERLINE');
        style.remove('STRIKETHROUGH');
      }
    }).toOrderedSet();
  }

  return currentStyle;
};

var joinChunks = function joinChunks(A, B, experimentalHasNestedBlocks) {
  // Sometimes two blocks will touch in the DOM and we need to strip the
  // extra delimiter to preserve niceness.
  var lastInA = A.text.slice(-1);
  var firstInB = B.text.slice(0, 1);

  if (lastInA === '\r' && firstInB === '\r' && !experimentalHasNestedBlocks) {
    A.text = A.text.slice(0, -1);
    A.inlines.pop();
    A.entities.pop();
    A.blocks.pop();
  } // Kill whitespace after blocks


  if (lastInA === '\r') {
    if (B.text === SPACE || B.text === '\n') {
      return A;
    } else if (firstInB === SPACE || firstInB === '\n') {
      B.text = B.text.slice(1);
      B.inlines.shift();
      B.entities.shift();
    }
  }

  return {
    text: A.text + B.text,
    inlines: A.inlines.concat(B.inlines),
    entities: A.entities.concat(B.entities),
    blocks: A.blocks.concat(B.blocks)
  };
};
/**
 * Check to see if we have anything like <p> <blockquote> <h1>... to create
 * block tags from. If we do, we can use those and ignore <div> tags. If we
 * don't, we can treat <div> tags as meaningful (unstyled) blocks.
 */


var containsSemanticBlockMarkup = function containsSemanticBlockMarkup(html, blockTags) {
  return blockTags.some(function (tag) {
    return html.indexOf('<' + tag) !== -1;
  });
};

var hasValidLinkText = function hasValidLinkText(link) {
  !(link instanceof HTMLAnchorElement) ?  true ? invariant(false, 'Link must be an HTMLAnchorElement.') : undefined : void 0;
  var protocol = link.protocol;
  return protocol === 'http:' || protocol === 'https:' || protocol === 'mailto:';
};

var getWhitespaceChunk = function getWhitespaceChunk(inEntity) {
  var entities = new Array(1);

  if (inEntity) {
    entities[0] = inEntity;
  }

  return _extends({}, EMPTY_CHUNK, {
    text: SPACE,
    inlines: [OrderedSet()],
    entities: entities
  });
};

var getSoftNewlineChunk = function getSoftNewlineChunk() {
  return _extends({}, EMPTY_CHUNK, {
    text: '\n',
    inlines: [OrderedSet()],
    entities: new Array(1)
  });
};

var getChunkedBlock = function getChunkedBlock() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  return _extends({}, EMPTY_BLOCK, props);
};

var getBlockDividerChunk = function getBlockDividerChunk(block, depth) {
  var parentKey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  return {
    text: '\r',
    inlines: [OrderedSet()],
    entities: new Array(1),
    blocks: [getChunkedBlock({
      parent: parentKey,
      key: generateRandomKey(),
      type: block,
      depth: Math.max(0, Math.min(MAX_DEPTH, depth))
    })]
  };
};
/**
 *  If we're pasting from one DraftEditor to another we can check to see if
 *  existing list item depth classes are being used and preserve this style
 */


var getListItemDepth = function getListItemDepth(node) {
  var depth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  Object.keys(knownListItemDepthClasses).some(function (depthClass) {
    if (node.classList.contains(depthClass)) {
      depth = knownListItemDepthClasses[depthClass];
    }
  });
  return depth;
};

var genFragment = function genFragment(entityMap, node, inlineStyle, lastList, inBlock, blockTags, depth, blockRenderMap, inEntity, parentKey) {
  var lastLastBlock = lastBlock;
  var nodeName = node.nodeName.toLowerCase();
  var newEntityMap = entityMap;
  var nextBlockType = 'unstyled';
  var newBlock = false;
  var inBlockType = inBlock && getBlockTypeForTag(inBlock, lastList, blockRenderMap);

  var chunk = _extends({}, EMPTY_CHUNK);

  var newChunk = null;
  var blockKey = void 0; // Base Case

  if (nodeName === '#text') {
    var _text = node.textContent;

    var nodeTextContent = _text.trim(); // We should not create blocks for leading spaces that are
    // existing around ol/ul and their children list items


    if (lastList && nodeTextContent === '' && node.parentElement) {
      var parentNodeName = node.parentElement.nodeName.toLowerCase();

      if (parentNodeName === 'ol' || parentNodeName === 'ul') {
        return {
          chunk: _extends({}, EMPTY_CHUNK),
          entityMap: entityMap
        };
      }
    }

    if (nodeTextContent === '' && inBlock !== 'pre') {
      return {
        chunk: getWhitespaceChunk(inEntity),
        entityMap: entityMap
      };
    }

    if (inBlock !== 'pre') {
      // Can't use empty string because MSWord
      _text = _text.replace(REGEX_LF, SPACE);
    } // save the last block so we can use it later


    lastBlock = nodeName;
    return {
      chunk: {
        text: _text,
        inlines: Array(_text.length).fill(inlineStyle),
        entities: Array(_text.length).fill(inEntity),
        blocks: []
      },
      entityMap: entityMap
    };
  } // save the last block so we can use it later


  lastBlock = nodeName; // BR tags

  if (nodeName === 'br') {
    if (lastLastBlock === 'br' && (!inBlock || inBlockType === 'unstyled')) {
      return {
        chunk: getBlockDividerChunk('unstyled', depth, parentKey),
        entityMap: entityMap
      };
    }

    return {
      chunk: getSoftNewlineChunk(),
      entityMap: entityMap
    };
  } // IMG tags


  if (nodeName === 'img' && node instanceof HTMLImageElement && node.attributes.getNamedItem('src') && node.attributes.getNamedItem('src').value) {
    var image = node;
    var entityConfig = {};
    imgAttr.forEach(function (attr) {
      var imageAttribute = image.getAttribute(attr);

      if (imageAttribute) {
        entityConfig[attr] = imageAttribute;
      }
    }); // Forcing this node to have children because otherwise no entity will be
    // created for this node.
    // The child text node cannot just have a space or return as content -
    // we strip those out.
    // See https://github.com/facebook/draft-js/issues/231 for some context.

    node.textContent = "\uD83D\uDCF7"; // TODO: update this when we remove DraftEntity entirely

    inEntity = DraftEntity.__create('IMAGE', 'MUTABLE', entityConfig || {});
  } // Inline tags


  inlineStyle = processInlineTag(nodeName, node, inlineStyle); // Handle lists

  if (nodeName === 'ul' || nodeName === 'ol') {
    if (lastList) {
      depth += 1;
    }

    lastList = nodeName;
  }

  if (!experimentalTreeDataSupport && nodeName === 'li' && node instanceof HTMLElement) {
    depth = getListItemDepth(node, depth);
  }

  var blockType = getBlockTypeForTag(nodeName, lastList, blockRenderMap);
  var inListBlock = lastList && inBlock === 'li' && nodeName === 'li';
  var inBlockOrHasNestedBlocks = (!inBlock || experimentalTreeDataSupport) && blockTags.indexOf(nodeName) !== -1; // Block Tags

  if (inListBlock || inBlockOrHasNestedBlocks) {
    chunk = getBlockDividerChunk(blockType, depth, parentKey);
    blockKey = chunk.blocks[0].key;
    inBlock = nodeName;
    newBlock = !experimentalTreeDataSupport;
  } // this is required so that we can handle 'ul' and 'ol'


  if (inListBlock) {
    nextBlockType = lastList === 'ul' ? 'unordered-list-item' : 'ordered-list-item';
  } // Recurse through children


  var child = node.firstChild;

  if (child != null) {
    nodeName = child.nodeName.toLowerCase();
  }

  var entityId = null;

  while (child) {
    if (child instanceof HTMLAnchorElement && child.href && hasValidLinkText(child)) {
      (function () {
        var anchor = child;
        var entityConfig = {};
        anchorAttr.forEach(function (attr) {
          var anchorAttribute = anchor.getAttribute(attr);

          if (anchorAttribute) {
            entityConfig[attr] = anchorAttribute;
          }
        });
        entityConfig.url = new URI(anchor.href).toString(); // TODO: update this when we remove DraftEntity completely

        entityId = DraftEntity.__create('LINK', 'MUTABLE', entityConfig || {});
      })();
    } else {
      entityId = undefined;
    }

    var _genFragment = genFragment(newEntityMap, child, inlineStyle, lastList, inBlock, blockTags, depth, blockRenderMap, entityId || inEntity, experimentalTreeDataSupport ? blockKey : null),
        generatedChunk = _genFragment.chunk,
        maybeUpdatedEntityMap = _genFragment.entityMap;

    newChunk = generatedChunk;
    newEntityMap = maybeUpdatedEntityMap;
    chunk = joinChunks(chunk, newChunk, experimentalTreeDataSupport);
    var sibling = child.nextSibling; // Put in a newline to break up blocks inside blocks

    if (!parentKey && sibling && blockTags.indexOf(nodeName) >= 0 && inBlock) {
      chunk = joinChunks(chunk, getSoftNewlineChunk());
    }

    if (sibling) {
      nodeName = sibling.nodeName.toLowerCase();
    }

    child = sibling;
  }

  if (newBlock) {
    chunk = joinChunks(chunk, getBlockDividerChunk(nextBlockType, depth, parentKey));
  }

  return {
    chunk: chunk,
    entityMap: newEntityMap
  };
};

var getChunkForHTML = function getChunkForHTML(html, DOMBuilder, blockRenderMap, entityMap) {
  html = html.trim().replace(REGEX_CR, '').replace(REGEX_NBSP, SPACE).replace(REGEX_CARRIAGE, '').replace(REGEX_ZWS, '');
  var supportedBlockTags = getBlockMapSupportedTags(blockRenderMap);
  var safeBody = DOMBuilder(html);

  if (!safeBody) {
    return null;
  }

  lastBlock = null; // Sometimes we aren't dealing with content that contains nice semantic
  // tags. In this case, use divs to separate everything out into paragraphs
  // and hope for the best.

  var workingBlocks = containsSemanticBlockMarkup(html, supportedBlockTags) ? supportedBlockTags : ['div']; // Start with -1 block depth to offset the fact that we are passing in a fake
  // UL block to start with.

  var fragment = genFragment(entityMap, safeBody, OrderedSet(), 'ul', null, workingBlocks, -1, blockRenderMap);
  var chunk = fragment.chunk;
  var newEntityMap = fragment.entityMap; // join with previous block to prevent weirdness on paste

  if (chunk.text.indexOf('\r') === 0) {
    chunk = {
      text: chunk.text.slice(1),
      inlines: chunk.inlines.slice(1),
      entities: chunk.entities.slice(1),
      blocks: chunk.blocks
    };
  } // Kill block delimiter at the end


  if (chunk.text.slice(-1) === '\r') {
    chunk.text = chunk.text.slice(0, -1);
    chunk.inlines = chunk.inlines.slice(0, -1);
    chunk.entities = chunk.entities.slice(0, -1);
    chunk.blocks.pop();
  } // If we saw no block tags, put an unstyled one in


  if (chunk.blocks.length === 0) {
    chunk.blocks.push(_extends({}, EMPTY_CHUNK, {
      type: 'unstyled',
      depth: 0
    }));
  } // Sometimes we start with text that isn't in a block, which is then
  // followed by blocks. Need to fix up the blocks to add in
  // an unstyled block for this content


  if (chunk.text.split('\r').length === chunk.blocks.length + 1) {
    chunk.blocks.unshift({
      type: 'unstyled',
      depth: 0
    });
  }

  return {
    chunk: chunk,
    entityMap: newEntityMap
  };
};

var convertChunkToContentBlocks = function convertChunkToContentBlocks(chunk) {
  if (!chunk || !chunk.text || !Array.isArray(chunk.blocks)) {
    return null;
  }

  var initialState = {
    cacheRef: {},
    contentBlocks: []
  };
  var start = 0;
  var rawBlocks = chunk.blocks,
      rawInlines = chunk.inlines,
      rawEntities = chunk.entities;
  var BlockNodeRecord = experimentalTreeDataSupport ? ContentBlockNode : ContentBlock;
  return chunk.text.split('\r').reduce(function (acc, textBlock, index) {
    // Make absolutely certain that our text is acceptable.
    textBlock = sanitizeDraftText(textBlock);
    var block = rawBlocks[index];
    var end = start + textBlock.length;
    var inlines = rawInlines.slice(start, end);
    var entities = rawEntities.slice(start, end);
    var characterList = List(inlines.map(function (style, index) {
      var data = {
        style: style,
        entity: null
      };

      if (entities[index]) {
        data.entity = entities[index];
      }

      return CharacterMetadata.create(data);
    }));
    start = end + 1;
    var depth = block.depth,
        type = block.type,
        parent = block.parent;
    var key = block.key || generateRandomKey();
    var parentTextNodeKey = null; // will be used to store container text nodes
    // childrens add themselves to their parents since we are iterating in order

    if (parent) {
      var parentIndex = acc.cacheRef[parent];
      var parentRecord = acc.contentBlocks[parentIndex]; // if parent has text we need to split it into a separate unstyled element

      if (parentRecord.getChildKeys().isEmpty() && parentRecord.getText()) {
        var parentCharacterList = parentRecord.getCharacterList();
        var parentText = parentRecord.getText();
        parentTextNodeKey = generateRandomKey();
        var textNode = new ContentBlockNode({
          key: parentTextNodeKey,
          text: parentText,
          characterList: parentCharacterList,
          parent: parent,
          nextSibling: key
        });
        acc.contentBlocks.push(textNode);
        parentRecord = parentRecord.withMutations(function (block) {
          block.set('characterList', List()).set('text', '').set('children', parentRecord.children.push(textNode.getKey()));
        });
      }

      acc.contentBlocks[parentIndex] = parentRecord.set('children', parentRecord.children.push(key));
    }

    var blockNode = new BlockNodeRecord({
      key: key,
      parent: parent,
      type: type,
      depth: depth,
      text: textBlock,
      characterList: characterList,
      prevSibling: parentTextNodeKey || (index === 0 || rawBlocks[index - 1].parent !== parent ? null : rawBlocks[index - 1].key),
      nextSibling: index === rawBlocks.length - 1 || rawBlocks[index + 1].parent !== parent ? null : rawBlocks[index + 1].key
    }); // insert node

    acc.contentBlocks.push(blockNode); // cache ref for building links

    acc.cacheRef[blockNode.key] = index;
    return acc;
  }, initialState).contentBlocks;
};

var convertFromHTMLtoContentBlocks = function convertFromHTMLtoContentBlocks(html) {
  var DOMBuilder = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : getSafeBodyFromHTML;
  var blockRenderMap = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : DefaultDraftBlockRenderMap; // Be ABSOLUTELY SURE that the dom builder you pass here won't execute
  // arbitrary code in whatever environment you're running this in. For an
  // example of how we try to do this in-browser, see getSafeBodyFromHTML.
  // TODO: replace DraftEntity with an OrderedMap here

  var chunkData = getChunkForHTML(html, DOMBuilder, blockRenderMap, DraftEntity);

  if (chunkData == null) {
    return null;
  }

  var chunk = chunkData.chunk,
      entityMap = chunkData.entityMap;
  var contentBlocks = convertChunkToContentBlocks(chunk);
  return {
    contentBlocks: contentBlocks,
    entityMap: entityMap
  };
};

module.exports = convertFromHTMLtoContentBlocks;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2NvbnZlcnRGcm9tSFRNTFRvQ29udGVudEJsb2Nrcy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9jb252ZXJ0RnJvbUhUTUxUb0NvbnRlbnRCbG9ja3MuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBjb252ZXJ0RnJvbUhUTUxUb0NvbnRlbnRCbG9ja3NcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9leHRlbmRzID0gX2Fzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9rbm93bkxpc3RJdGVtRGVwdGhDbCxcbiAgICBfYXNzaWduID0gcmVxdWlyZSgnb2JqZWN0LWFzc2lnbicpO1xuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7IGlmIChrZXkgaW4gb2JqKSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgeyB2YWx1ZTogdmFsdWUsIGVudW1lcmFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSwgd3JpdGFibGU6IHRydWUgfSk7IH0gZWxzZSB7IG9ialtrZXldID0gdmFsdWU7IH0gcmV0dXJuIG9iajsgfVxuXG52YXIgQ2hhcmFjdGVyTWV0YWRhdGEgPSByZXF1aXJlKCcuL0NoYXJhY3Rlck1ldGFkYXRhJyk7XG52YXIgQ29udGVudEJsb2NrID0gcmVxdWlyZSgnLi9Db250ZW50QmxvY2snKTtcbnZhciBDb250ZW50QmxvY2tOb2RlID0gcmVxdWlyZSgnLi9Db250ZW50QmxvY2tOb2RlJyk7XG52YXIgRGVmYXVsdERyYWZ0QmxvY2tSZW5kZXJNYXAgPSByZXF1aXJlKCcuL0RlZmF1bHREcmFmdEJsb2NrUmVuZGVyTWFwJyk7XG52YXIgRHJhZnRFbnRpdHkgPSByZXF1aXJlKCcuL0RyYWZ0RW50aXR5Jyk7XG52YXIgRHJhZnRGZWF0dXJlRmxhZ3MgPSByZXF1aXJlKCcuL0RyYWZ0RmVhdHVyZUZsYWdzJyk7XG52YXIgSW1tdXRhYmxlID0gcmVxdWlyZSgnaW1tdXRhYmxlJyk7XG5cbnZhciBfcmVxdWlyZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpLFxuICAgIFNldCA9IF9yZXF1aXJlLlNldDtcblxudmFyIFVSSSA9IHJlcXVpcmUoJ2ZianMvbGliL1VSSScpO1xuXG52YXIgY3ggPSByZXF1aXJlKCdmYmpzL2xpYi9jeCcpO1xudmFyIGdlbmVyYXRlUmFuZG9tS2V5ID0gcmVxdWlyZSgnLi9nZW5lcmF0ZVJhbmRvbUtleScpO1xudmFyIGdldFNhZmVCb2R5RnJvbUhUTUwgPSByZXF1aXJlKCcuL2dldFNhZmVCb2R5RnJvbUhUTUwnKTtcbnZhciBpbnZhcmlhbnQgPSByZXF1aXJlKCdmYmpzL2xpYi9pbnZhcmlhbnQnKTtcbnZhciBzYW5pdGl6ZURyYWZ0VGV4dCA9IHJlcXVpcmUoJy4vc2FuaXRpemVEcmFmdFRleHQnKTtcblxudmFyIGV4cGVyaW1lbnRhbFRyZWVEYXRhU3VwcG9ydCA9IERyYWZ0RmVhdHVyZUZsYWdzLmRyYWZ0X3RyZWVfZGF0YV9zdXBwb3J0O1xuXG52YXIgTGlzdCA9IEltbXV0YWJsZS5MaXN0LFxuICAgIE9yZGVyZWRTZXQgPSBJbW11dGFibGUuT3JkZXJlZFNldDtcblxuXG52YXIgTkJTUCA9ICcmbmJzcDsnO1xudmFyIFNQQUNFID0gJyAnO1xuXG4vLyBBcmJpdHJhcnkgbWF4IGluZGVudFxudmFyIE1BWF9ERVBUSCA9IDQ7XG5cbi8vIHVzZWQgZm9yIHJlcGxhY2luZyBjaGFyYWN0ZXJzIGluIEhUTUxcbnZhciBSRUdFWF9DUiA9IG5ldyBSZWdFeHAoJ1xccicsICdnJyk7XG52YXIgUkVHRVhfTEYgPSBuZXcgUmVnRXhwKCdcXG4nLCAnZycpO1xudmFyIFJFR0VYX05CU1AgPSBuZXcgUmVnRXhwKE5CU1AsICdnJyk7XG52YXIgUkVHRVhfQ0FSUklBR0UgPSBuZXcgUmVnRXhwKCcmIzEzOz8nLCAnZycpO1xudmFyIFJFR0VYX1pXUyA9IG5ldyBSZWdFeHAoJyYjODIwMzs/JywgJ2cnKTtcblxuLy8gaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvQ1NTL2ZvbnQtd2VpZ2h0XG52YXIgYm9sZFZhbHVlcyA9IFsnYm9sZCcsICdib2xkZXInLCAnNTAwJywgJzYwMCcsICc3MDAnLCAnODAwJywgJzkwMCddO1xudmFyIG5vdEJvbGRWYWx1ZXMgPSBbJ2xpZ2h0JywgJ2xpZ2h0ZXInLCAnMTAwJywgJzIwMCcsICczMDAnLCAnNDAwJ107XG5cbi8vIEJsb2NrIHRhZyBmbG93IGlzIGRpZmZlcmVudCBiZWNhdXNlIExJcyBkbyBub3QgaGF2ZVxuLy8gYSBkZXRlcm1pbmlzdGljIHN0eWxlIDtfO1xudmFyIGlubGluZVRhZ3MgPSB7XG4gIGI6ICdCT0xEJyxcbiAgY29kZTogJ0NPREUnLFxuICBkZWw6ICdTVFJJS0VUSFJPVUdIJyxcbiAgZW06ICdJVEFMSUMnLFxuICBpOiAnSVRBTElDJyxcbiAgczogJ1NUUklLRVRIUk9VR0gnLFxuICBzdHJpa2U6ICdTVFJJS0VUSFJPVUdIJyxcbiAgc3Ryb25nOiAnQk9MRCcsXG4gIHU6ICdVTkRFUkxJTkUnXG59O1xuXG52YXIga25vd25MaXN0SXRlbURlcHRoQ2xhc3NlcyA9IChfa25vd25MaXN0SXRlbURlcHRoQ2wgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9rbm93bkxpc3RJdGVtRGVwdGhDbCwgY3goJ3B1YmxpYy9EcmFmdFN0eWxlRGVmYXVsdC9kZXB0aDAnKSwgMCksIF9kZWZpbmVQcm9wZXJ0eShfa25vd25MaXN0SXRlbURlcHRoQ2wsIGN4KCdwdWJsaWMvRHJhZnRTdHlsZURlZmF1bHQvZGVwdGgxJyksIDEpLCBfZGVmaW5lUHJvcGVydHkoX2tub3duTGlzdEl0ZW1EZXB0aENsLCBjeCgncHVibGljL0RyYWZ0U3R5bGVEZWZhdWx0L2RlcHRoMicpLCAyKSwgX2RlZmluZVByb3BlcnR5KF9rbm93bkxpc3RJdGVtRGVwdGhDbCwgY3goJ3B1YmxpYy9EcmFmdFN0eWxlRGVmYXVsdC9kZXB0aDMnKSwgMyksIF9kZWZpbmVQcm9wZXJ0eShfa25vd25MaXN0SXRlbURlcHRoQ2wsIGN4KCdwdWJsaWMvRHJhZnRTdHlsZURlZmF1bHQvZGVwdGg0JyksIDQpLCBfa25vd25MaXN0SXRlbURlcHRoQ2wpO1xuXG52YXIgYW5jaG9yQXR0ciA9IFsnY2xhc3NOYW1lJywgJ2hyZWYnLCAncmVsJywgJ3RhcmdldCcsICd0aXRsZSddO1xuXG52YXIgaW1nQXR0ciA9IFsnYWx0JywgJ2NsYXNzTmFtZScsICdoZWlnaHQnLCAnc3JjJywgJ3dpZHRoJ107XG5cbnZhciBsYXN0QmxvY2sgPSB2b2lkIDA7XG5cbnZhciBFTVBUWV9DSFVOSyA9IHtcbiAgdGV4dDogJycsXG4gIGlubGluZXM6IFtdLFxuICBlbnRpdGllczogW10sXG4gIGJsb2NrczogW11cbn07XG5cbnZhciBFTVBUWV9CTE9DSyA9IHtcbiAgY2hpbGRyZW46IExpc3QoKSxcbiAgZGVwdGg6IDAsXG4gIGtleTogJycsXG4gIHR5cGU6ICcnXG59O1xuXG52YXIgZ2V0TGlzdEJsb2NrVHlwZSA9IGZ1bmN0aW9uIGdldExpc3RCbG9ja1R5cGUodGFnLCBsYXN0TGlzdCkge1xuICBpZiAodGFnID09PSAnbGknKSB7XG4gICAgcmV0dXJuIGxhc3RMaXN0ID09PSAnb2wnID8gJ29yZGVyZWQtbGlzdC1pdGVtJyA6ICd1bm9yZGVyZWQtbGlzdC1pdGVtJztcbiAgfVxuICByZXR1cm4gbnVsbDtcbn07XG5cbnZhciBnZXRCbG9ja01hcFN1cHBvcnRlZFRhZ3MgPSBmdW5jdGlvbiBnZXRCbG9ja01hcFN1cHBvcnRlZFRhZ3MoYmxvY2tSZW5kZXJNYXApIHtcbiAgdmFyIHVuc3R5bGVkRWxlbWVudCA9IGJsb2NrUmVuZGVyTWFwLmdldCgndW5zdHlsZWQnKS5lbGVtZW50O1xuICB2YXIgdGFncyA9IFNldChbXSk7XG5cbiAgYmxvY2tSZW5kZXJNYXAuZm9yRWFjaChmdW5jdGlvbiAoZHJhZnRCbG9jaykge1xuICAgIGlmIChkcmFmdEJsb2NrLmFsaWFzZWRFbGVtZW50cykge1xuICAgICAgZHJhZnRCbG9jay5hbGlhc2VkRWxlbWVudHMuZm9yRWFjaChmdW5jdGlvbiAodGFnKSB7XG4gICAgICAgIHRhZ3MgPSB0YWdzLmFkZCh0YWcpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgdGFncyA9IHRhZ3MuYWRkKGRyYWZ0QmxvY2suZWxlbWVudCk7XG4gIH0pO1xuXG4gIHJldHVybiB0YWdzLmZpbHRlcihmdW5jdGlvbiAodGFnKSB7XG4gICAgcmV0dXJuIHRhZyAmJiB0YWcgIT09IHVuc3R5bGVkRWxlbWVudDtcbiAgfSkudG9BcnJheSgpLnNvcnQoKTtcbn07XG5cbi8vIGN1c3RvbSBlbGVtZW50IGNvbnZlcnNpb25zXG52YXIgZ2V0TXVsdGlNYXRjaGVkVHlwZSA9IGZ1bmN0aW9uIGdldE11bHRpTWF0Y2hlZFR5cGUodGFnLCBsYXN0TGlzdCwgbXVsdGlNYXRjaEV4dHJhY3Rvcikge1xuICBmb3IgKHZhciBpaSA9IDA7IGlpIDwgbXVsdGlNYXRjaEV4dHJhY3Rvci5sZW5ndGg7IGlpKyspIHtcbiAgICB2YXIgbWF0Y2hUeXBlID0gbXVsdGlNYXRjaEV4dHJhY3RvcltpaV0odGFnLCBsYXN0TGlzdCk7XG4gICAgaWYgKG1hdGNoVHlwZSkge1xuICAgICAgcmV0dXJuIG1hdGNoVHlwZTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIG51bGw7XG59O1xuXG52YXIgZ2V0QmxvY2tUeXBlRm9yVGFnID0gZnVuY3Rpb24gZ2V0QmxvY2tUeXBlRm9yVGFnKHRhZywgbGFzdExpc3QsIGJsb2NrUmVuZGVyTWFwKSB7XG4gIHZhciBtYXRjaGVkVHlwZXMgPSBibG9ja1JlbmRlck1hcC5maWx0ZXIoZnVuY3Rpb24gKGRyYWZ0QmxvY2spIHtcbiAgICByZXR1cm4gZHJhZnRCbG9jay5lbGVtZW50ID09PSB0YWcgfHwgZHJhZnRCbG9jay53cmFwcGVyID09PSB0YWcgfHwgZHJhZnRCbG9jay5hbGlhc2VkRWxlbWVudHMgJiYgZHJhZnRCbG9jay5hbGlhc2VkRWxlbWVudHMuc29tZShmdW5jdGlvbiAoYWxpYXMpIHtcbiAgICAgIHJldHVybiBhbGlhcyA9PT0gdGFnO1xuICAgIH0pO1xuICB9KS5rZXlTZXEoKS50b1NldCgpLnRvQXJyYXkoKS5zb3J0KCk7XG5cbiAgLy8gaWYgd2UgZG9udCBoYXZlIGFueSBtYXRjaGVkIHR5cGUsIHJldHVybiB1bnN0eWxlZFxuICAvLyBpZiB3ZSBoYXZlIG9uZSBtYXRjaGVkIHR5cGUgcmV0dXJuIGl0XG4gIC8vIGlmIHdlIGhhdmUgbXVsdGkgbWF0Y2hlZCB0eXBlcyB1c2UgdGhlIG11bHRpLW1hdGNoIGZ1bmN0aW9uIHRvIGdhdGhlciB0eXBlXG4gIHN3aXRjaCAobWF0Y2hlZFR5cGVzLmxlbmd0aCkge1xuICAgIGNhc2UgMDpcbiAgICAgIHJldHVybiAndW5zdHlsZWQnO1xuICAgIGNhc2UgMTpcbiAgICAgIHJldHVybiBtYXRjaGVkVHlwZXNbMF07XG4gICAgZGVmYXVsdDpcbiAgICAgIHJldHVybiBnZXRNdWx0aU1hdGNoZWRUeXBlKHRhZywgbGFzdExpc3QsIFtnZXRMaXN0QmxvY2tUeXBlXSkgfHwgJ3Vuc3R5bGVkJztcbiAgfVxufTtcblxudmFyIHByb2Nlc3NJbmxpbmVUYWcgPSBmdW5jdGlvbiBwcm9jZXNzSW5saW5lVGFnKHRhZywgbm9kZSwgY3VycmVudFN0eWxlKSB7XG4gIHZhciBzdHlsZVRvQ2hlY2sgPSBpbmxpbmVUYWdzW3RhZ107XG4gIGlmIChzdHlsZVRvQ2hlY2spIHtcbiAgICBjdXJyZW50U3R5bGUgPSBjdXJyZW50U3R5bGUuYWRkKHN0eWxlVG9DaGVjaykudG9PcmRlcmVkU2V0KCk7XG4gIH0gZWxzZSBpZiAobm9kZSBpbnN0YW5jZW9mIEhUTUxFbGVtZW50KSB7XG4gICAgdmFyIGh0bWxFbGVtZW50ID0gbm9kZTtcbiAgICBjdXJyZW50U3R5bGUgPSBjdXJyZW50U3R5bGUud2l0aE11dGF0aW9ucyhmdW5jdGlvbiAoc3R5bGUpIHtcbiAgICAgIHZhciBmb250V2VpZ2h0ID0gaHRtbEVsZW1lbnQuc3R5bGUuZm9udFdlaWdodDtcbiAgICAgIHZhciBmb250U3R5bGUgPSBodG1sRWxlbWVudC5zdHlsZS5mb250U3R5bGU7XG4gICAgICB2YXIgdGV4dERlY29yYXRpb24gPSBodG1sRWxlbWVudC5zdHlsZS50ZXh0RGVjb3JhdGlvbjtcblxuICAgICAgaWYgKGJvbGRWYWx1ZXMuaW5kZXhPZihmb250V2VpZ2h0KSA+PSAwKSB7XG4gICAgICAgIHN0eWxlLmFkZCgnQk9MRCcpO1xuICAgICAgfSBlbHNlIGlmIChub3RCb2xkVmFsdWVzLmluZGV4T2YoZm9udFdlaWdodCkgPj0gMCkge1xuICAgICAgICBzdHlsZS5yZW1vdmUoJ0JPTEQnKTtcbiAgICAgIH1cblxuICAgICAgaWYgKGZvbnRTdHlsZSA9PT0gJ2l0YWxpYycpIHtcbiAgICAgICAgc3R5bGUuYWRkKCdJVEFMSUMnKTtcbiAgICAgIH0gZWxzZSBpZiAoZm9udFN0eWxlID09PSAnbm9ybWFsJykge1xuICAgICAgICBzdHlsZS5yZW1vdmUoJ0lUQUxJQycpO1xuICAgICAgfVxuXG4gICAgICBpZiAodGV4dERlY29yYXRpb24gPT09ICd1bmRlcmxpbmUnKSB7XG4gICAgICAgIHN0eWxlLmFkZCgnVU5ERVJMSU5FJyk7XG4gICAgICB9XG4gICAgICBpZiAodGV4dERlY29yYXRpb24gPT09ICdsaW5lLXRocm91Z2gnKSB7XG4gICAgICAgIHN0eWxlLmFkZCgnU1RSSUtFVEhST1VHSCcpO1xuICAgICAgfVxuICAgICAgaWYgKHRleHREZWNvcmF0aW9uID09PSAnbm9uZScpIHtcbiAgICAgICAgc3R5bGUucmVtb3ZlKCdVTkRFUkxJTkUnKTtcbiAgICAgICAgc3R5bGUucmVtb3ZlKCdTVFJJS0VUSFJPVUdIJyk7XG4gICAgICB9XG4gICAgfSkudG9PcmRlcmVkU2V0KCk7XG4gIH1cbiAgcmV0dXJuIGN1cnJlbnRTdHlsZTtcbn07XG5cbnZhciBqb2luQ2h1bmtzID0gZnVuY3Rpb24gam9pbkNodW5rcyhBLCBCLCBleHBlcmltZW50YWxIYXNOZXN0ZWRCbG9ja3MpIHtcbiAgLy8gU29tZXRpbWVzIHR3byBibG9ja3Mgd2lsbCB0b3VjaCBpbiB0aGUgRE9NIGFuZCB3ZSBuZWVkIHRvIHN0cmlwIHRoZVxuICAvLyBleHRyYSBkZWxpbWl0ZXIgdG8gcHJlc2VydmUgbmljZW5lc3MuXG4gIHZhciBsYXN0SW5BID0gQS50ZXh0LnNsaWNlKC0xKTtcbiAgdmFyIGZpcnN0SW5CID0gQi50ZXh0LnNsaWNlKDAsIDEpO1xuXG4gIGlmIChsYXN0SW5BID09PSAnXFxyJyAmJiBmaXJzdEluQiA9PT0gJ1xccicgJiYgIWV4cGVyaW1lbnRhbEhhc05lc3RlZEJsb2Nrcykge1xuICAgIEEudGV4dCA9IEEudGV4dC5zbGljZSgwLCAtMSk7XG4gICAgQS5pbmxpbmVzLnBvcCgpO1xuICAgIEEuZW50aXRpZXMucG9wKCk7XG4gICAgQS5ibG9ja3MucG9wKCk7XG4gIH1cblxuICAvLyBLaWxsIHdoaXRlc3BhY2UgYWZ0ZXIgYmxvY2tzXG4gIGlmIChsYXN0SW5BID09PSAnXFxyJykge1xuICAgIGlmIChCLnRleHQgPT09IFNQQUNFIHx8IEIudGV4dCA9PT0gJ1xcbicpIHtcbiAgICAgIHJldHVybiBBO1xuICAgIH0gZWxzZSBpZiAoZmlyc3RJbkIgPT09IFNQQUNFIHx8IGZpcnN0SW5CID09PSAnXFxuJykge1xuICAgICAgQi50ZXh0ID0gQi50ZXh0LnNsaWNlKDEpO1xuICAgICAgQi5pbmxpbmVzLnNoaWZ0KCk7XG4gICAgICBCLmVudGl0aWVzLnNoaWZ0KCk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICB0ZXh0OiBBLnRleHQgKyBCLnRleHQsXG4gICAgaW5saW5lczogQS5pbmxpbmVzLmNvbmNhdChCLmlubGluZXMpLFxuICAgIGVudGl0aWVzOiBBLmVudGl0aWVzLmNvbmNhdChCLmVudGl0aWVzKSxcbiAgICBibG9ja3M6IEEuYmxvY2tzLmNvbmNhdChCLmJsb2NrcylcbiAgfTtcbn07XG5cbi8qKlxuICogQ2hlY2sgdG8gc2VlIGlmIHdlIGhhdmUgYW55dGhpbmcgbGlrZSA8cD4gPGJsb2NrcXVvdGU+IDxoMT4uLi4gdG8gY3JlYXRlXG4gKiBibG9jayB0YWdzIGZyb20uIElmIHdlIGRvLCB3ZSBjYW4gdXNlIHRob3NlIGFuZCBpZ25vcmUgPGRpdj4gdGFncy4gSWYgd2VcbiAqIGRvbid0LCB3ZSBjYW4gdHJlYXQgPGRpdj4gdGFncyBhcyBtZWFuaW5nZnVsICh1bnN0eWxlZCkgYmxvY2tzLlxuICovXG52YXIgY29udGFpbnNTZW1hbnRpY0Jsb2NrTWFya3VwID0gZnVuY3Rpb24gY29udGFpbnNTZW1hbnRpY0Jsb2NrTWFya3VwKGh0bWwsIGJsb2NrVGFncykge1xuICByZXR1cm4gYmxvY2tUYWdzLnNvbWUoZnVuY3Rpb24gKHRhZykge1xuICAgIHJldHVybiBodG1sLmluZGV4T2YoJzwnICsgdGFnKSAhPT0gLTE7XG4gIH0pO1xufTtcblxudmFyIGhhc1ZhbGlkTGlua1RleHQgPSBmdW5jdGlvbiBoYXNWYWxpZExpbmtUZXh0KGxpbmspIHtcbiAgIShsaW5rIGluc3RhbmNlb2YgSFRNTEFuY2hvckVsZW1lbnQpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ0xpbmsgbXVzdCBiZSBhbiBIVE1MQW5jaG9yRWxlbWVudC4nKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG4gIHZhciBwcm90b2NvbCA9IGxpbmsucHJvdG9jb2w7XG4gIHJldHVybiBwcm90b2NvbCA9PT0gJ2h0dHA6JyB8fCBwcm90b2NvbCA9PT0gJ2h0dHBzOicgfHwgcHJvdG9jb2wgPT09ICdtYWlsdG86Jztcbn07XG5cbnZhciBnZXRXaGl0ZXNwYWNlQ2h1bmsgPSBmdW5jdGlvbiBnZXRXaGl0ZXNwYWNlQ2h1bmsoaW5FbnRpdHkpIHtcbiAgdmFyIGVudGl0aWVzID0gbmV3IEFycmF5KDEpO1xuICBpZiAoaW5FbnRpdHkpIHtcbiAgICBlbnRpdGllc1swXSA9IGluRW50aXR5O1xuICB9XG4gIHJldHVybiBfZXh0ZW5kcyh7fSwgRU1QVFlfQ0hVTkssIHtcbiAgICB0ZXh0OiBTUEFDRSxcbiAgICBpbmxpbmVzOiBbT3JkZXJlZFNldCgpXSxcbiAgICBlbnRpdGllczogZW50aXRpZXNcbiAgfSk7XG59O1xuXG52YXIgZ2V0U29mdE5ld2xpbmVDaHVuayA9IGZ1bmN0aW9uIGdldFNvZnROZXdsaW5lQ2h1bmsoKSB7XG4gIHJldHVybiBfZXh0ZW5kcyh7fSwgRU1QVFlfQ0hVTkssIHtcbiAgICB0ZXh0OiAnXFxuJyxcbiAgICBpbmxpbmVzOiBbT3JkZXJlZFNldCgpXSxcbiAgICBlbnRpdGllczogbmV3IEFycmF5KDEpXG4gIH0pO1xufTtcblxudmFyIGdldENodW5rZWRCbG9jayA9IGZ1bmN0aW9uIGdldENodW5rZWRCbG9jaygpIHtcbiAgdmFyIHByb3BzID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiB7fTtcblxuICByZXR1cm4gX2V4dGVuZHMoe30sIEVNUFRZX0JMT0NLLCBwcm9wcyk7XG59O1xuXG52YXIgZ2V0QmxvY2tEaXZpZGVyQ2h1bmsgPSBmdW5jdGlvbiBnZXRCbG9ja0RpdmlkZXJDaHVuayhibG9jaywgZGVwdGgpIHtcbiAgdmFyIHBhcmVudEtleSA9IGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIGFyZ3VtZW50c1syXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzJdIDogbnVsbDtcblxuICByZXR1cm4ge1xuICAgIHRleHQ6ICdcXHInLFxuICAgIGlubGluZXM6IFtPcmRlcmVkU2V0KCldLFxuICAgIGVudGl0aWVzOiBuZXcgQXJyYXkoMSksXG4gICAgYmxvY2tzOiBbZ2V0Q2h1bmtlZEJsb2NrKHtcbiAgICAgIHBhcmVudDogcGFyZW50S2V5LFxuICAgICAga2V5OiBnZW5lcmF0ZVJhbmRvbUtleSgpLFxuICAgICAgdHlwZTogYmxvY2ssXG4gICAgICBkZXB0aDogTWF0aC5tYXgoMCwgTWF0aC5taW4oTUFYX0RFUFRILCBkZXB0aCkpXG4gICAgfSldXG4gIH07XG59O1xuXG4vKipcbiAqICBJZiB3ZSdyZSBwYXN0aW5nIGZyb20gb25lIERyYWZ0RWRpdG9yIHRvIGFub3RoZXIgd2UgY2FuIGNoZWNrIHRvIHNlZSBpZlxuICogIGV4aXN0aW5nIGxpc3QgaXRlbSBkZXB0aCBjbGFzc2VzIGFyZSBiZWluZyB1c2VkIGFuZCBwcmVzZXJ2ZSB0aGlzIHN0eWxlXG4gKi9cbnZhciBnZXRMaXN0SXRlbURlcHRoID0gZnVuY3Rpb24gZ2V0TGlzdEl0ZW1EZXB0aChub2RlKSB7XG4gIHZhciBkZXB0aCA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogMDtcblxuICBPYmplY3Qua2V5cyhrbm93bkxpc3RJdGVtRGVwdGhDbGFzc2VzKS5zb21lKGZ1bmN0aW9uIChkZXB0aENsYXNzKSB7XG4gICAgaWYgKG5vZGUuY2xhc3NMaXN0LmNvbnRhaW5zKGRlcHRoQ2xhc3MpKSB7XG4gICAgICBkZXB0aCA9IGtub3duTGlzdEl0ZW1EZXB0aENsYXNzZXNbZGVwdGhDbGFzc107XG4gICAgfVxuICB9KTtcbiAgcmV0dXJuIGRlcHRoO1xufTtcblxudmFyIGdlbkZyYWdtZW50ID0gZnVuY3Rpb24gZ2VuRnJhZ21lbnQoZW50aXR5TWFwLCBub2RlLCBpbmxpbmVTdHlsZSwgbGFzdExpc3QsIGluQmxvY2ssIGJsb2NrVGFncywgZGVwdGgsIGJsb2NrUmVuZGVyTWFwLCBpbkVudGl0eSwgcGFyZW50S2V5KSB7XG4gIHZhciBsYXN0TGFzdEJsb2NrID0gbGFzdEJsb2NrO1xuICB2YXIgbm9kZU5hbWUgPSBub2RlLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk7XG4gIHZhciBuZXdFbnRpdHlNYXAgPSBlbnRpdHlNYXA7XG4gIHZhciBuZXh0QmxvY2tUeXBlID0gJ3Vuc3R5bGVkJztcbiAgdmFyIG5ld0Jsb2NrID0gZmFsc2U7XG4gIHZhciBpbkJsb2NrVHlwZSA9IGluQmxvY2sgJiYgZ2V0QmxvY2tUeXBlRm9yVGFnKGluQmxvY2ssIGxhc3RMaXN0LCBibG9ja1JlbmRlck1hcCk7XG4gIHZhciBjaHVuayA9IF9leHRlbmRzKHt9LCBFTVBUWV9DSFVOSyk7XG4gIHZhciBuZXdDaHVuayA9IG51bGw7XG4gIHZhciBibG9ja0tleSA9IHZvaWQgMDtcblxuICAvLyBCYXNlIENhc2VcbiAgaWYgKG5vZGVOYW1lID09PSAnI3RleHQnKSB7XG4gICAgdmFyIF90ZXh0ID0gbm9kZS50ZXh0Q29udGVudDtcbiAgICB2YXIgbm9kZVRleHRDb250ZW50ID0gX3RleHQudHJpbSgpO1xuXG4gICAgLy8gV2Ugc2hvdWxkIG5vdCBjcmVhdGUgYmxvY2tzIGZvciBsZWFkaW5nIHNwYWNlcyB0aGF0IGFyZVxuICAgIC8vIGV4aXN0aW5nIGFyb3VuZCBvbC91bCBhbmQgdGhlaXIgY2hpbGRyZW4gbGlzdCBpdGVtc1xuICAgIGlmIChsYXN0TGlzdCAmJiBub2RlVGV4dENvbnRlbnQgPT09ICcnICYmIG5vZGUucGFyZW50RWxlbWVudCkge1xuICAgICAgdmFyIHBhcmVudE5vZGVOYW1lID0gbm9kZS5wYXJlbnRFbGVtZW50Lm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk7XG4gICAgICBpZiAocGFyZW50Tm9kZU5hbWUgPT09ICdvbCcgfHwgcGFyZW50Tm9kZU5hbWUgPT09ICd1bCcpIHtcbiAgICAgICAgcmV0dXJuIHsgY2h1bms6IF9leHRlbmRzKHt9LCBFTVBUWV9DSFVOSyksIGVudGl0eU1hcDogZW50aXR5TWFwIH07XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKG5vZGVUZXh0Q29udGVudCA9PT0gJycgJiYgaW5CbG9jayAhPT0gJ3ByZScpIHtcbiAgICAgIHJldHVybiB7IGNodW5rOiBnZXRXaGl0ZXNwYWNlQ2h1bmsoaW5FbnRpdHkpLCBlbnRpdHlNYXA6IGVudGl0eU1hcCB9O1xuICAgIH1cbiAgICBpZiAoaW5CbG9jayAhPT0gJ3ByZScpIHtcbiAgICAgIC8vIENhbid0IHVzZSBlbXB0eSBzdHJpbmcgYmVjYXVzZSBNU1dvcmRcbiAgICAgIF90ZXh0ID0gX3RleHQucmVwbGFjZShSRUdFWF9MRiwgU1BBQ0UpO1xuICAgIH1cblxuICAgIC8vIHNhdmUgdGhlIGxhc3QgYmxvY2sgc28gd2UgY2FuIHVzZSBpdCBsYXRlclxuICAgIGxhc3RCbG9jayA9IG5vZGVOYW1lO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIGNodW5rOiB7XG4gICAgICAgIHRleHQ6IF90ZXh0LFxuICAgICAgICBpbmxpbmVzOiBBcnJheShfdGV4dC5sZW5ndGgpLmZpbGwoaW5saW5lU3R5bGUpLFxuICAgICAgICBlbnRpdGllczogQXJyYXkoX3RleHQubGVuZ3RoKS5maWxsKGluRW50aXR5KSxcbiAgICAgICAgYmxvY2tzOiBbXVxuICAgICAgfSxcbiAgICAgIGVudGl0eU1hcDogZW50aXR5TWFwXG4gICAgfTtcbiAgfVxuXG4gIC8vIHNhdmUgdGhlIGxhc3QgYmxvY2sgc28gd2UgY2FuIHVzZSBpdCBsYXRlclxuICBsYXN0QmxvY2sgPSBub2RlTmFtZTtcblxuICAvLyBCUiB0YWdzXG4gIGlmIChub2RlTmFtZSA9PT0gJ2JyJykge1xuICAgIGlmIChsYXN0TGFzdEJsb2NrID09PSAnYnInICYmICghaW5CbG9jayB8fCBpbkJsb2NrVHlwZSA9PT0gJ3Vuc3R5bGVkJykpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGNodW5rOiBnZXRCbG9ja0RpdmlkZXJDaHVuaygndW5zdHlsZWQnLCBkZXB0aCwgcGFyZW50S2V5KSxcbiAgICAgICAgZW50aXR5TWFwOiBlbnRpdHlNYXBcbiAgICAgIH07XG4gICAgfVxuICAgIHJldHVybiB7IGNodW5rOiBnZXRTb2Z0TmV3bGluZUNodW5rKCksIGVudGl0eU1hcDogZW50aXR5TWFwIH07XG4gIH1cblxuICAvLyBJTUcgdGFnc1xuICBpZiAobm9kZU5hbWUgPT09ICdpbWcnICYmIG5vZGUgaW5zdGFuY2VvZiBIVE1MSW1hZ2VFbGVtZW50ICYmIG5vZGUuYXR0cmlidXRlcy5nZXROYW1lZEl0ZW0oJ3NyYycpICYmIG5vZGUuYXR0cmlidXRlcy5nZXROYW1lZEl0ZW0oJ3NyYycpLnZhbHVlKSB7XG4gICAgdmFyIGltYWdlID0gbm9kZTtcbiAgICB2YXIgZW50aXR5Q29uZmlnID0ge307XG5cbiAgICBpbWdBdHRyLmZvckVhY2goZnVuY3Rpb24gKGF0dHIpIHtcbiAgICAgIHZhciBpbWFnZUF0dHJpYnV0ZSA9IGltYWdlLmdldEF0dHJpYnV0ZShhdHRyKTtcbiAgICAgIGlmIChpbWFnZUF0dHJpYnV0ZSkge1xuICAgICAgICBlbnRpdHlDb25maWdbYXR0cl0gPSBpbWFnZUF0dHJpYnV0ZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICAvLyBGb3JjaW5nIHRoaXMgbm9kZSB0byBoYXZlIGNoaWxkcmVuIGJlY2F1c2Ugb3RoZXJ3aXNlIG5vIGVudGl0eSB3aWxsIGJlXG4gICAgLy8gY3JlYXRlZCBmb3IgdGhpcyBub2RlLlxuICAgIC8vIFRoZSBjaGlsZCB0ZXh0IG5vZGUgY2Fubm90IGp1c3QgaGF2ZSBhIHNwYWNlIG9yIHJldHVybiBhcyBjb250ZW50IC1cbiAgICAvLyB3ZSBzdHJpcCB0aG9zZSBvdXQuXG4gICAgLy8gU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9kcmFmdC1qcy9pc3N1ZXMvMjMxIGZvciBzb21lIGNvbnRleHQuXG4gICAgbm9kZS50ZXh0Q29udGVudCA9ICdcXHVEODNEXFx1RENGNyc7XG5cbiAgICAvLyBUT0RPOiB1cGRhdGUgdGhpcyB3aGVuIHdlIHJlbW92ZSBEcmFmdEVudGl0eSBlbnRpcmVseVxuICAgIGluRW50aXR5ID0gRHJhZnRFbnRpdHkuX19jcmVhdGUoJ0lNQUdFJywgJ01VVEFCTEUnLCBlbnRpdHlDb25maWcgfHwge30pO1xuICB9XG5cbiAgLy8gSW5saW5lIHRhZ3NcbiAgaW5saW5lU3R5bGUgPSBwcm9jZXNzSW5saW5lVGFnKG5vZGVOYW1lLCBub2RlLCBpbmxpbmVTdHlsZSk7XG5cbiAgLy8gSGFuZGxlIGxpc3RzXG4gIGlmIChub2RlTmFtZSA9PT0gJ3VsJyB8fCBub2RlTmFtZSA9PT0gJ29sJykge1xuICAgIGlmIChsYXN0TGlzdCkge1xuICAgICAgZGVwdGggKz0gMTtcbiAgICB9XG4gICAgbGFzdExpc3QgPSBub2RlTmFtZTtcbiAgfVxuXG4gIGlmICghZXhwZXJpbWVudGFsVHJlZURhdGFTdXBwb3J0ICYmIG5vZGVOYW1lID09PSAnbGknICYmIG5vZGUgaW5zdGFuY2VvZiBIVE1MRWxlbWVudCkge1xuICAgIGRlcHRoID0gZ2V0TGlzdEl0ZW1EZXB0aChub2RlLCBkZXB0aCk7XG4gIH1cblxuICB2YXIgYmxvY2tUeXBlID0gZ2V0QmxvY2tUeXBlRm9yVGFnKG5vZGVOYW1lLCBsYXN0TGlzdCwgYmxvY2tSZW5kZXJNYXApO1xuICB2YXIgaW5MaXN0QmxvY2sgPSBsYXN0TGlzdCAmJiBpbkJsb2NrID09PSAnbGknICYmIG5vZGVOYW1lID09PSAnbGknO1xuICB2YXIgaW5CbG9ja09ySGFzTmVzdGVkQmxvY2tzID0gKCFpbkJsb2NrIHx8IGV4cGVyaW1lbnRhbFRyZWVEYXRhU3VwcG9ydCkgJiYgYmxvY2tUYWdzLmluZGV4T2Yobm9kZU5hbWUpICE9PSAtMTtcblxuICAvLyBCbG9jayBUYWdzXG4gIGlmIChpbkxpc3RCbG9jayB8fCBpbkJsb2NrT3JIYXNOZXN0ZWRCbG9ja3MpIHtcbiAgICBjaHVuayA9IGdldEJsb2NrRGl2aWRlckNodW5rKGJsb2NrVHlwZSwgZGVwdGgsIHBhcmVudEtleSk7XG4gICAgYmxvY2tLZXkgPSBjaHVuay5ibG9ja3NbMF0ua2V5O1xuICAgIGluQmxvY2sgPSBub2RlTmFtZTtcbiAgICBuZXdCbG9jayA9ICFleHBlcmltZW50YWxUcmVlRGF0YVN1cHBvcnQ7XG4gIH1cblxuICAvLyB0aGlzIGlzIHJlcXVpcmVkIHNvIHRoYXQgd2UgY2FuIGhhbmRsZSAndWwnIGFuZCAnb2wnXG4gIGlmIChpbkxpc3RCbG9jaykge1xuICAgIG5leHRCbG9ja1R5cGUgPSBsYXN0TGlzdCA9PT0gJ3VsJyA/ICd1bm9yZGVyZWQtbGlzdC1pdGVtJyA6ICdvcmRlcmVkLWxpc3QtaXRlbSc7XG4gIH1cblxuICAvLyBSZWN1cnNlIHRocm91Z2ggY2hpbGRyZW5cbiAgdmFyIGNoaWxkID0gbm9kZS5maXJzdENoaWxkO1xuICBpZiAoY2hpbGQgIT0gbnVsbCkge1xuICAgIG5vZGVOYW1lID0gY2hpbGQubm9kZU5hbWUudG9Mb3dlckNhc2UoKTtcbiAgfVxuXG4gIHZhciBlbnRpdHlJZCA9IG51bGw7XG5cbiAgd2hpbGUgKGNoaWxkKSB7XG4gICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgSFRNTEFuY2hvckVsZW1lbnQgJiYgY2hpbGQuaHJlZiAmJiBoYXNWYWxpZExpbmtUZXh0KGNoaWxkKSkge1xuICAgICAgKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGFuY2hvciA9IGNoaWxkO1xuICAgICAgICB2YXIgZW50aXR5Q29uZmlnID0ge307XG5cbiAgICAgICAgYW5jaG9yQXR0ci5mb3JFYWNoKGZ1bmN0aW9uIChhdHRyKSB7XG4gICAgICAgICAgdmFyIGFuY2hvckF0dHJpYnV0ZSA9IGFuY2hvci5nZXRBdHRyaWJ1dGUoYXR0cik7XG4gICAgICAgICAgaWYgKGFuY2hvckF0dHJpYnV0ZSkge1xuICAgICAgICAgICAgZW50aXR5Q29uZmlnW2F0dHJdID0gYW5jaG9yQXR0cmlidXRlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgZW50aXR5Q29uZmlnLnVybCA9IG5ldyBVUkkoYW5jaG9yLmhyZWYpLnRvU3RyaW5nKCk7XG4gICAgICAgIC8vIFRPRE86IHVwZGF0ZSB0aGlzIHdoZW4gd2UgcmVtb3ZlIERyYWZ0RW50aXR5IGNvbXBsZXRlbHlcbiAgICAgICAgZW50aXR5SWQgPSBEcmFmdEVudGl0eS5fX2NyZWF0ZSgnTElOSycsICdNVVRBQkxFJywgZW50aXR5Q29uZmlnIHx8IHt9KTtcbiAgICAgIH0pKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGVudGl0eUlkID0gdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIHZhciBfZ2VuRnJhZ21lbnQgPSBnZW5GcmFnbWVudChuZXdFbnRpdHlNYXAsIGNoaWxkLCBpbmxpbmVTdHlsZSwgbGFzdExpc3QsIGluQmxvY2ssIGJsb2NrVGFncywgZGVwdGgsIGJsb2NrUmVuZGVyTWFwLCBlbnRpdHlJZCB8fCBpbkVudGl0eSwgZXhwZXJpbWVudGFsVHJlZURhdGFTdXBwb3J0ID8gYmxvY2tLZXkgOiBudWxsKSxcbiAgICAgICAgZ2VuZXJhdGVkQ2h1bmsgPSBfZ2VuRnJhZ21lbnQuY2h1bmssXG4gICAgICAgIG1heWJlVXBkYXRlZEVudGl0eU1hcCA9IF9nZW5GcmFnbWVudC5lbnRpdHlNYXA7XG5cbiAgICBuZXdDaHVuayA9IGdlbmVyYXRlZENodW5rO1xuICAgIG5ld0VudGl0eU1hcCA9IG1heWJlVXBkYXRlZEVudGl0eU1hcDtcblxuICAgIGNodW5rID0gam9pbkNodW5rcyhjaHVuaywgbmV3Q2h1bmssIGV4cGVyaW1lbnRhbFRyZWVEYXRhU3VwcG9ydCk7XG4gICAgdmFyIHNpYmxpbmcgPSBjaGlsZC5uZXh0U2libGluZztcblxuICAgIC8vIFB1dCBpbiBhIG5ld2xpbmUgdG8gYnJlYWsgdXAgYmxvY2tzIGluc2lkZSBibG9ja3NcbiAgICBpZiAoIXBhcmVudEtleSAmJiBzaWJsaW5nICYmIGJsb2NrVGFncy5pbmRleE9mKG5vZGVOYW1lKSA+PSAwICYmIGluQmxvY2spIHtcbiAgICAgIGNodW5rID0gam9pbkNodW5rcyhjaHVuaywgZ2V0U29mdE5ld2xpbmVDaHVuaygpKTtcbiAgICB9XG4gICAgaWYgKHNpYmxpbmcpIHtcbiAgICAgIG5vZGVOYW1lID0gc2libGluZy5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpO1xuICAgIH1cbiAgICBjaGlsZCA9IHNpYmxpbmc7XG4gIH1cblxuICBpZiAobmV3QmxvY2spIHtcbiAgICBjaHVuayA9IGpvaW5DaHVua3MoY2h1bmssIGdldEJsb2NrRGl2aWRlckNodW5rKG5leHRCbG9ja1R5cGUsIGRlcHRoLCBwYXJlbnRLZXkpKTtcbiAgfVxuXG4gIHJldHVybiB7IGNodW5rOiBjaHVuaywgZW50aXR5TWFwOiBuZXdFbnRpdHlNYXAgfTtcbn07XG5cbnZhciBnZXRDaHVua0ZvckhUTUwgPSBmdW5jdGlvbiBnZXRDaHVua0ZvckhUTUwoaHRtbCwgRE9NQnVpbGRlciwgYmxvY2tSZW5kZXJNYXAsIGVudGl0eU1hcCkge1xuICBodG1sID0gaHRtbC50cmltKCkucmVwbGFjZShSRUdFWF9DUiwgJycpLnJlcGxhY2UoUkVHRVhfTkJTUCwgU1BBQ0UpLnJlcGxhY2UoUkVHRVhfQ0FSUklBR0UsICcnKS5yZXBsYWNlKFJFR0VYX1pXUywgJycpO1xuXG4gIHZhciBzdXBwb3J0ZWRCbG9ja1RhZ3MgPSBnZXRCbG9ja01hcFN1cHBvcnRlZFRhZ3MoYmxvY2tSZW5kZXJNYXApO1xuXG4gIHZhciBzYWZlQm9keSA9IERPTUJ1aWxkZXIoaHRtbCk7XG4gIGlmICghc2FmZUJvZHkpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuICBsYXN0QmxvY2sgPSBudWxsO1xuXG4gIC8vIFNvbWV0aW1lcyB3ZSBhcmVuJ3QgZGVhbGluZyB3aXRoIGNvbnRlbnQgdGhhdCBjb250YWlucyBuaWNlIHNlbWFudGljXG4gIC8vIHRhZ3MuIEluIHRoaXMgY2FzZSwgdXNlIGRpdnMgdG8gc2VwYXJhdGUgZXZlcnl0aGluZyBvdXQgaW50byBwYXJhZ3JhcGhzXG4gIC8vIGFuZCBob3BlIGZvciB0aGUgYmVzdC5cbiAgdmFyIHdvcmtpbmdCbG9ja3MgPSBjb250YWluc1NlbWFudGljQmxvY2tNYXJrdXAoaHRtbCwgc3VwcG9ydGVkQmxvY2tUYWdzKSA/IHN1cHBvcnRlZEJsb2NrVGFncyA6IFsnZGl2J107XG5cbiAgLy8gU3RhcnQgd2l0aCAtMSBibG9jayBkZXB0aCB0byBvZmZzZXQgdGhlIGZhY3QgdGhhdCB3ZSBhcmUgcGFzc2luZyBpbiBhIGZha2VcbiAgLy8gVUwgYmxvY2sgdG8gc3RhcnQgd2l0aC5cbiAgdmFyIGZyYWdtZW50ID0gZ2VuRnJhZ21lbnQoZW50aXR5TWFwLCBzYWZlQm9keSwgT3JkZXJlZFNldCgpLCAndWwnLCBudWxsLCB3b3JraW5nQmxvY2tzLCAtMSwgYmxvY2tSZW5kZXJNYXApO1xuXG4gIHZhciBjaHVuayA9IGZyYWdtZW50LmNodW5rO1xuICB2YXIgbmV3RW50aXR5TWFwID0gZnJhZ21lbnQuZW50aXR5TWFwO1xuXG4gIC8vIGpvaW4gd2l0aCBwcmV2aW91cyBibG9jayB0byBwcmV2ZW50IHdlaXJkbmVzcyBvbiBwYXN0ZVxuICBpZiAoY2h1bmsudGV4dC5pbmRleE9mKCdcXHInKSA9PT0gMCkge1xuICAgIGNodW5rID0ge1xuICAgICAgdGV4dDogY2h1bmsudGV4dC5zbGljZSgxKSxcbiAgICAgIGlubGluZXM6IGNodW5rLmlubGluZXMuc2xpY2UoMSksXG4gICAgICBlbnRpdGllczogY2h1bmsuZW50aXRpZXMuc2xpY2UoMSksXG4gICAgICBibG9ja3M6IGNodW5rLmJsb2Nrc1xuICAgIH07XG4gIH1cblxuICAvLyBLaWxsIGJsb2NrIGRlbGltaXRlciBhdCB0aGUgZW5kXG4gIGlmIChjaHVuay50ZXh0LnNsaWNlKC0xKSA9PT0gJ1xccicpIHtcbiAgICBjaHVuay50ZXh0ID0gY2h1bmsudGV4dC5zbGljZSgwLCAtMSk7XG4gICAgY2h1bmsuaW5saW5lcyA9IGNodW5rLmlubGluZXMuc2xpY2UoMCwgLTEpO1xuICAgIGNodW5rLmVudGl0aWVzID0gY2h1bmsuZW50aXRpZXMuc2xpY2UoMCwgLTEpO1xuICAgIGNodW5rLmJsb2Nrcy5wb3AoKTtcbiAgfVxuXG4gIC8vIElmIHdlIHNhdyBubyBibG9jayB0YWdzLCBwdXQgYW4gdW5zdHlsZWQgb25lIGluXG4gIGlmIChjaHVuay5ibG9ja3MubGVuZ3RoID09PSAwKSB7XG4gICAgY2h1bmsuYmxvY2tzLnB1c2goX2V4dGVuZHMoe30sIEVNUFRZX0NIVU5LLCB7XG4gICAgICB0eXBlOiAndW5zdHlsZWQnLFxuICAgICAgZGVwdGg6IDBcbiAgICB9KSk7XG4gIH1cblxuICAvLyBTb21ldGltZXMgd2Ugc3RhcnQgd2l0aCB0ZXh0IHRoYXQgaXNuJ3QgaW4gYSBibG9jaywgd2hpY2ggaXMgdGhlblxuICAvLyBmb2xsb3dlZCBieSBibG9ja3MuIE5lZWQgdG8gZml4IHVwIHRoZSBibG9ja3MgdG8gYWRkIGluXG4gIC8vIGFuIHVuc3R5bGVkIGJsb2NrIGZvciB0aGlzIGNvbnRlbnRcbiAgaWYgKGNodW5rLnRleHQuc3BsaXQoJ1xccicpLmxlbmd0aCA9PT0gY2h1bmsuYmxvY2tzLmxlbmd0aCArIDEpIHtcbiAgICBjaHVuay5ibG9ja3MudW5zaGlmdCh7IHR5cGU6ICd1bnN0eWxlZCcsIGRlcHRoOiAwIH0pO1xuICB9XG5cbiAgcmV0dXJuIHsgY2h1bms6IGNodW5rLCBlbnRpdHlNYXA6IG5ld0VudGl0eU1hcCB9O1xufTtcblxudmFyIGNvbnZlcnRDaHVua1RvQ29udGVudEJsb2NrcyA9IGZ1bmN0aW9uIGNvbnZlcnRDaHVua1RvQ29udGVudEJsb2NrcyhjaHVuaykge1xuICBpZiAoIWNodW5rIHx8ICFjaHVuay50ZXh0IHx8ICFBcnJheS5pc0FycmF5KGNodW5rLmJsb2NrcykpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHZhciBpbml0aWFsU3RhdGUgPSB7XG4gICAgY2FjaGVSZWY6IHt9LFxuICAgIGNvbnRlbnRCbG9ja3M6IFtdXG4gIH07XG5cbiAgdmFyIHN0YXJ0ID0gMDtcblxuICB2YXIgcmF3QmxvY2tzID0gY2h1bmsuYmxvY2tzLFxuICAgICAgcmF3SW5saW5lcyA9IGNodW5rLmlubGluZXMsXG4gICAgICByYXdFbnRpdGllcyA9IGNodW5rLmVudGl0aWVzO1xuXG5cbiAgdmFyIEJsb2NrTm9kZVJlY29yZCA9IGV4cGVyaW1lbnRhbFRyZWVEYXRhU3VwcG9ydCA/IENvbnRlbnRCbG9ja05vZGUgOiBDb250ZW50QmxvY2s7XG5cbiAgcmV0dXJuIGNodW5rLnRleHQuc3BsaXQoJ1xccicpLnJlZHVjZShmdW5jdGlvbiAoYWNjLCB0ZXh0QmxvY2ssIGluZGV4KSB7XG4gICAgLy8gTWFrZSBhYnNvbHV0ZWx5IGNlcnRhaW4gdGhhdCBvdXIgdGV4dCBpcyBhY2NlcHRhYmxlLlxuICAgIHRleHRCbG9jayA9IHNhbml0aXplRHJhZnRUZXh0KHRleHRCbG9jayk7XG5cbiAgICB2YXIgYmxvY2sgPSByYXdCbG9ja3NbaW5kZXhdO1xuICAgIHZhciBlbmQgPSBzdGFydCArIHRleHRCbG9jay5sZW5ndGg7XG4gICAgdmFyIGlubGluZXMgPSByYXdJbmxpbmVzLnNsaWNlKHN0YXJ0LCBlbmQpO1xuICAgIHZhciBlbnRpdGllcyA9IHJhd0VudGl0aWVzLnNsaWNlKHN0YXJ0LCBlbmQpO1xuICAgIHZhciBjaGFyYWN0ZXJMaXN0ID0gTGlzdChpbmxpbmVzLm1hcChmdW5jdGlvbiAoc3R5bGUsIGluZGV4KSB7XG4gICAgICB2YXIgZGF0YSA9IHsgc3R5bGU6IHN0eWxlLCBlbnRpdHk6IG51bGwgfTtcbiAgICAgIGlmIChlbnRpdGllc1tpbmRleF0pIHtcbiAgICAgICAgZGF0YS5lbnRpdHkgPSBlbnRpdGllc1tpbmRleF07XG4gICAgICB9XG4gICAgICByZXR1cm4gQ2hhcmFjdGVyTWV0YWRhdGEuY3JlYXRlKGRhdGEpO1xuICAgIH0pKTtcbiAgICBzdGFydCA9IGVuZCArIDE7XG5cbiAgICB2YXIgZGVwdGggPSBibG9jay5kZXB0aCxcbiAgICAgICAgdHlwZSA9IGJsb2NrLnR5cGUsXG4gICAgICAgIHBhcmVudCA9IGJsb2NrLnBhcmVudDtcblxuXG4gICAgdmFyIGtleSA9IGJsb2NrLmtleSB8fCBnZW5lcmF0ZVJhbmRvbUtleSgpO1xuICAgIHZhciBwYXJlbnRUZXh0Tm9kZUtleSA9IG51bGw7IC8vIHdpbGwgYmUgdXNlZCB0byBzdG9yZSBjb250YWluZXIgdGV4dCBub2Rlc1xuXG4gICAgLy8gY2hpbGRyZW5zIGFkZCB0aGVtc2VsdmVzIHRvIHRoZWlyIHBhcmVudHMgc2luY2Ugd2UgYXJlIGl0ZXJhdGluZyBpbiBvcmRlclxuICAgIGlmIChwYXJlbnQpIHtcbiAgICAgIHZhciBwYXJlbnRJbmRleCA9IGFjYy5jYWNoZVJlZltwYXJlbnRdO1xuICAgICAgdmFyIHBhcmVudFJlY29yZCA9IGFjYy5jb250ZW50QmxvY2tzW3BhcmVudEluZGV4XTtcblxuICAgICAgLy8gaWYgcGFyZW50IGhhcyB0ZXh0IHdlIG5lZWQgdG8gc3BsaXQgaXQgaW50byBhIHNlcGFyYXRlIHVuc3R5bGVkIGVsZW1lbnRcbiAgICAgIGlmIChwYXJlbnRSZWNvcmQuZ2V0Q2hpbGRLZXlzKCkuaXNFbXB0eSgpICYmIHBhcmVudFJlY29yZC5nZXRUZXh0KCkpIHtcbiAgICAgICAgdmFyIHBhcmVudENoYXJhY3Rlckxpc3QgPSBwYXJlbnRSZWNvcmQuZ2V0Q2hhcmFjdGVyTGlzdCgpO1xuICAgICAgICB2YXIgcGFyZW50VGV4dCA9IHBhcmVudFJlY29yZC5nZXRUZXh0KCk7XG4gICAgICAgIHBhcmVudFRleHROb2RlS2V5ID0gZ2VuZXJhdGVSYW5kb21LZXkoKTtcblxuICAgICAgICB2YXIgdGV4dE5vZGUgPSBuZXcgQ29udGVudEJsb2NrTm9kZSh7XG4gICAgICAgICAga2V5OiBwYXJlbnRUZXh0Tm9kZUtleSxcbiAgICAgICAgICB0ZXh0OiBwYXJlbnRUZXh0LFxuICAgICAgICAgIGNoYXJhY3Rlckxpc3Q6IHBhcmVudENoYXJhY3Rlckxpc3QsXG4gICAgICAgICAgcGFyZW50OiBwYXJlbnQsXG4gICAgICAgICAgbmV4dFNpYmxpbmc6IGtleVxuICAgICAgICB9KTtcblxuICAgICAgICBhY2MuY29udGVudEJsb2Nrcy5wdXNoKHRleHROb2RlKTtcblxuICAgICAgICBwYXJlbnRSZWNvcmQgPSBwYXJlbnRSZWNvcmQud2l0aE11dGF0aW9ucyhmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgICAgICBibG9jay5zZXQoJ2NoYXJhY3Rlckxpc3QnLCBMaXN0KCkpLnNldCgndGV4dCcsICcnKS5zZXQoJ2NoaWxkcmVuJywgcGFyZW50UmVjb3JkLmNoaWxkcmVuLnB1c2godGV4dE5vZGUuZ2V0S2V5KCkpKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIGFjYy5jb250ZW50QmxvY2tzW3BhcmVudEluZGV4XSA9IHBhcmVudFJlY29yZC5zZXQoJ2NoaWxkcmVuJywgcGFyZW50UmVjb3JkLmNoaWxkcmVuLnB1c2goa2V5KSk7XG4gICAgfVxuXG4gICAgdmFyIGJsb2NrTm9kZSA9IG5ldyBCbG9ja05vZGVSZWNvcmQoe1xuICAgICAga2V5OiBrZXksXG4gICAgICBwYXJlbnQ6IHBhcmVudCxcbiAgICAgIHR5cGU6IHR5cGUsXG4gICAgICBkZXB0aDogZGVwdGgsXG4gICAgICB0ZXh0OiB0ZXh0QmxvY2ssXG4gICAgICBjaGFyYWN0ZXJMaXN0OiBjaGFyYWN0ZXJMaXN0LFxuICAgICAgcHJldlNpYmxpbmc6IHBhcmVudFRleHROb2RlS2V5IHx8IChpbmRleCA9PT0gMCB8fCByYXdCbG9ja3NbaW5kZXggLSAxXS5wYXJlbnQgIT09IHBhcmVudCA/IG51bGwgOiByYXdCbG9ja3NbaW5kZXggLSAxXS5rZXkpLFxuICAgICAgbmV4dFNpYmxpbmc6IGluZGV4ID09PSByYXdCbG9ja3MubGVuZ3RoIC0gMSB8fCByYXdCbG9ja3NbaW5kZXggKyAxXS5wYXJlbnQgIT09IHBhcmVudCA/IG51bGwgOiByYXdCbG9ja3NbaW5kZXggKyAxXS5rZXlcbiAgICB9KTtcblxuICAgIC8vIGluc2VydCBub2RlXG4gICAgYWNjLmNvbnRlbnRCbG9ja3MucHVzaChibG9ja05vZGUpO1xuXG4gICAgLy8gY2FjaGUgcmVmIGZvciBidWlsZGluZyBsaW5rc1xuICAgIGFjYy5jYWNoZVJlZltibG9ja05vZGUua2V5XSA9IGluZGV4O1xuXG4gICAgcmV0dXJuIGFjYztcbiAgfSwgaW5pdGlhbFN0YXRlKS5jb250ZW50QmxvY2tzO1xufTtcblxudmFyIGNvbnZlcnRGcm9tSFRNTHRvQ29udGVudEJsb2NrcyA9IGZ1bmN0aW9uIGNvbnZlcnRGcm9tSFRNTHRvQ29udGVudEJsb2NrcyhodG1sKSB7XG4gIHZhciBET01CdWlsZGVyID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiBnZXRTYWZlQm9keUZyb21IVE1MO1xuICB2YXIgYmxvY2tSZW5kZXJNYXAgPSBhcmd1bWVudHMubGVuZ3RoID4gMiAmJiBhcmd1bWVudHNbMl0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1syXSA6IERlZmF1bHREcmFmdEJsb2NrUmVuZGVyTWFwO1xuXG4gIC8vIEJlIEFCU09MVVRFTFkgU1VSRSB0aGF0IHRoZSBkb20gYnVpbGRlciB5b3UgcGFzcyBoZXJlIHdvbid0IGV4ZWN1dGVcbiAgLy8gYXJiaXRyYXJ5IGNvZGUgaW4gd2hhdGV2ZXIgZW52aXJvbm1lbnQgeW91J3JlIHJ1bm5pbmcgdGhpcyBpbi4gRm9yIGFuXG4gIC8vIGV4YW1wbGUgb2YgaG93IHdlIHRyeSB0byBkbyB0aGlzIGluLWJyb3dzZXIsIHNlZSBnZXRTYWZlQm9keUZyb21IVE1MLlxuXG4gIC8vIFRPRE86IHJlcGxhY2UgRHJhZnRFbnRpdHkgd2l0aCBhbiBPcmRlcmVkTWFwIGhlcmVcbiAgdmFyIGNodW5rRGF0YSA9IGdldENodW5rRm9ySFRNTChodG1sLCBET01CdWlsZGVyLCBibG9ja1JlbmRlck1hcCwgRHJhZnRFbnRpdHkpO1xuXG4gIGlmIChjaHVua0RhdGEgPT0gbnVsbCkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgdmFyIGNodW5rID0gY2h1bmtEYXRhLmNodW5rLFxuICAgICAgZW50aXR5TWFwID0gY2h1bmtEYXRhLmVudGl0eU1hcDtcblxuICB2YXIgY29udGVudEJsb2NrcyA9IGNvbnZlcnRDaHVua1RvQ29udGVudEJsb2NrcyhjaHVuayk7XG5cbiAgcmV0dXJuIHtcbiAgICBjb250ZW50QmxvY2tzOiBjb250ZW50QmxvY2tzLFxuICAgIGVudGl0eU1hcDogZW50aXR5TWFwXG4gIH07XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbnZlcnRGcm9tSFRNTHRvQ29udGVudEJsb2NrczsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBWUE7QUFFQTtBQUVBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUVBOzs7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBSkE7QUFXQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUVBO0FBQUE7QUFBQTtBQUtBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBUUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFDQTtBQVdBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/convertFromHTMLToContentBlocks.js
