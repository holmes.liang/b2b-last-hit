__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NText", function() { return NText; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/component/NText.tsx";




var NText =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(NText, _ModelWidget);

  function NText(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, NText);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(NText).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(NText, [{
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model,
          label = _this$props.label,
          propName = _this$props.propName,
          size = _this$props.size,
          onChange = _this$props.onChange,
          rules = _this$props.rules,
          required = _this$props.required,
          layoutCol = _this$props.layoutCol,
          password = _this$props.password,
          placeholder = _this$props.placeholder,
          transform = _this$props.transform,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["form", "model", "label", "propName", "size", "onChange", "rules", "required", "layoutCol", "password", "placeholder", "transform"]);

      if (password) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_8__["NFormItem"], Object.assign({
          form: form,
          model: model,
          label: label,
          propName: propName,
          onChange: onChange,
          rules: rules,
          required: required,
          layoutCol: layoutCol
        }, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 31
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Input"].Password, Object.assign({
          placeholder: placeholder,
          autoComplete: "new-password"
        }, rest, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 32
          },
          __self: this
        })));
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_8__["NFormItem"], Object.assign({
        form: form,
        size: size,
        model: model,
        label: label,
        propName: propName,
        onChange: onChange,
        rules: rules,
        required: required,
        layoutCol: layoutCol,
        transform: transform
      }, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Input"], Object.assign({
        size: size || "large"
      }, rest, {
        placeholder: placeholder,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        },
        __self: this
      })));
    }
  }]);

  return NText;
}(_component__WEBPACK_IMPORTED_MODULE_8__["ModelWidget"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2NvbXBvbmVudC9OVGV4dC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvY29tcG9uZW50L05UZXh0LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgSW5wdXRQcm9wcyB9IGZyb20gXCJhbnRkL2xpYi9pbnB1dC9JbnB1dFwiO1xuaW1wb3J0IHsgSW5wdXQgfSBmcm9tIFwiYW50ZFwiO1xuXG5pbXBvcnQgeyBNb2RlbFdpZGdldCwgTkZvcm1JdGVtIH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IE5Gb3JtSXRlbVByb3BzIH0gZnJvbSBcIkBjb21wb25lbnQvTkZvcm1JdGVtXCI7XG5cbmV4cG9ydCB0eXBlIE5UZXh0UHJvcHMgPSB7XG4gIHBhc3N3b3JkPzogYm9vbGVhbixcbiAgcGxhY2Vob2xkZXI/OiBzdHJpbmc7XG4gIHNpemU/OiBzdHJpbmc7XG59ICYgTkZvcm1JdGVtUHJvcHMgJiBJbnB1dFByb3BzO1xuXG5jbGFzcyBOVGV4dDxQIGV4dGVuZHMgTlRleHRQcm9wcywgUywgQz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIGNvbnN0cnVjdG9yKHByb3BzOiBOVGV4dFByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7fSBhcyBDO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHtcbiAgICAgIGZvcm0sIG1vZGVsLCBsYWJlbCwgcHJvcE5hbWUsIHNpemUsIG9uQ2hhbmdlLCBydWxlcywgcmVxdWlyZWQsIGxheW91dENvbCxcbiAgICAgIHBhc3N3b3JkLCBwbGFjZWhvbGRlciwgdHJhbnNmb3JtLCAuLi5yZXN0XG4gICAgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBpZiAocGFzc3dvcmQpIHtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxORm9ybUl0ZW0gey4uLnsgZm9ybSwgbW9kZWwsIGxhYmVsLCBwcm9wTmFtZSwgb25DaGFuZ2UsIHJ1bGVzLCByZXF1aXJlZCwgbGF5b3V0Q29sIH19PlxuICAgICAgICAgIDxJbnB1dC5QYXNzd29yZCBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9IGF1dG9Db21wbGV0ZT1cIm5ldy1wYXNzd29yZFwiIHsuLi5yZXN0fS8+XG4gICAgICAgIDwvTkZvcm1JdGVtPlxuICAgICAgKTtcbiAgICB9XG4gICAgcmV0dXJuIChcbiAgICAgIDxORm9ybUl0ZW0gey4uLnsgZm9ybSwgc2l6ZSwgbW9kZWwsIGxhYmVsLCBwcm9wTmFtZSwgb25DaGFuZ2UsIHJ1bGVzLCByZXF1aXJlZCwgbGF5b3V0Q29sLCB0cmFuc2Zvcm0gfX0+XG4gICAgICAgIDxJbnB1dFxuICAgICAgICAgIHNpemU9e3NpemUgfHwgXCJsYXJnZVwifVxuICAgICAgICAgIHsuLi5yZXN0fVxuICAgICAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cbiAgICAgICAgLz5cbiAgICAgIDwvTkZvcm1JdGVtPlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IHsgTlRleHQgfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFFQTtBQUVBO0FBQ0E7QUFRQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFHQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BOzs7O0FBL0JBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/component/NText.tsx
