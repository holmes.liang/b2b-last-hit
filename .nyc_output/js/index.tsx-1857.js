__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useLoadMemoHistory", function() { return useLoadMemoHistory; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _history_timeline_item__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./history-timeline-item */ "./src/app/desk/component/history-timeline/history-timeline-item.tsx");

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/history-timeline/index.tsx";






var HistoryTimeline = function HistoryTimeline(props) {
  var policyId = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.model, props.dataFixed ? "".concat(props.dataFixed, ".policyId") : "policyId");

  var _useLoadMemoHistory = useLoadMemoHistory(policyId),
      _useLoadMemoHistory2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useLoadMemoHistory, 2),
      data = _useLoadMemoHistory2[0],
      loadMemoHistory = _useLoadMemoHistory2[1];

  _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].useEffect(function () {
    loadMemoHistory();
  }, []);
  return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Timeline"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, data.map(function (item, index) {
    return Object(_history_timeline_item__WEBPACK_IMPORTED_MODULE_5__["default"])(item, index);
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (HistoryTimeline);
var useLoadMemoHistory = function useLoadMemoHistory(policyId) {
  var _React$useState = _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].useState([]),
      _React$useState2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_React$useState, 2),
      data = _React$useState2[0],
      changeData = _React$useState2[1];

  var loadMemoHistory = function loadMemoHistory() {
    if (!policyId) return;
    fetchMemoHistoryByClaimId(policyId).then(function (data) {
      return changeData(data);
    });
  };

  return [data, loadMemoHistory];
};

var fetchMemoHistoryByClaimId = function fetchMemoHistoryByClaimId(policyId) {
  return _common__WEBPACK_IMPORTED_MODULE_4__["Ajax"].post("/policies/hisq", {
    pageSize: 50,
    policyId: policyId
  }).then(function (res) {
    var historyItems = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(res, "body.respData.items", []);

    return historyItems;
  });
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2hpc3RvcnktdGltZWxpbmUvaW5kZXgudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2hpc3RvcnktdGltZWxpbmUvaW5kZXgudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBUaW1lbGluZSB9IGZyb20gXCJhbnRkXCI7XG5cbmltcG9ydCB7IEFqYXggfSBmcm9tIFwiQGNvbW1vblwiO1xuXG5pbXBvcnQgdGltZUxpbmVJdGVtIGZyb20gXCIuL2hpc3RvcnktdGltZWxpbmUtaXRlbVwiO1xuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgbW9kZWw6IGFueTtcbiAgZGF0YUZpeGVkPzogYW55O1xufVxuXG5jb25zdCBIaXN0b3J5VGltZWxpbmUgPSAocHJvcHM6IElQcm9wcykgPT4ge1xuICBjb25zdCBwb2xpY3lJZCA9IF8uZ2V0KHByb3BzLm1vZGVsLCBwcm9wcy5kYXRhRml4ZWQgPyBgJHtwcm9wcy5kYXRhRml4ZWR9LnBvbGljeUlkYCA6IFwicG9saWN5SWRcIik7XG4gIGNvbnN0IFtkYXRhLCBsb2FkTWVtb0hpc3RvcnldOiBhbnkgPSB1c2VMb2FkTWVtb0hpc3RvcnkocG9saWN5SWQpO1xuXG4gIFJlYWN0LnVzZUVmZmVjdCgoKSA9PiB7XG4gICAgbG9hZE1lbW9IaXN0b3J5KCk7XG4gIH0sIFtdKTtcbiAgcmV0dXJuIDxkaXY+XG4gICAgPFRpbWVsaW5lPlxuICAgICAge2RhdGEubWFwKChpdGVtOiBhbnksIGluZGV4OiBudW1iZXIpID0+IHRpbWVMaW5lSXRlbShpdGVtLCBpbmRleCkpfVxuICAgIDwvVGltZWxpbmU+XG4gIDwvZGl2Pjtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IEhpc3RvcnlUaW1lbGluZTtcblxuXG5leHBvcnQgY29uc3QgdXNlTG9hZE1lbW9IaXN0b3J5ID0gKHBvbGljeUlkOiBzdHJpbmcpID0+IHtcbiAgY29uc3QgW2RhdGEsIGNoYW5nZURhdGFdID0gUmVhY3QudXNlU3RhdGUoW10pO1xuICBjb25zdCBsb2FkTWVtb0hpc3RvcnkgPSAoKSA9PiB7XG4gICAgaWYgKCFwb2xpY3lJZCkgcmV0dXJuO1xuICAgIGZldGNoTWVtb0hpc3RvcnlCeUNsYWltSWQocG9saWN5SWQpXG4gICAgICAudGhlbihkYXRhID0+IGNoYW5nZURhdGEoZGF0YSkpO1xuICB9O1xuICByZXR1cm4gW2RhdGEsIGxvYWRNZW1vSGlzdG9yeV07XG59O1xuXG5cbmNvbnN0IGZldGNoTWVtb0hpc3RvcnlCeUNsYWltSWQgPSAocG9saWN5SWQ6IHN0cmluZykgPT4ge1xuICByZXR1cm4gQWpheC5wb3N0KGAvcG9saWNpZXMvaGlzcWAsXG4gICAge1xuICAgICAgcGFnZVNpemU6IDUwLFxuICAgICAgcG9saWN5SWQ6IHBvbGljeUlkLFxuICAgIH0pXG4gICAgLnRoZW4ocmVzID0+IHtcbiAgICAgIGNvbnN0IGhpc3RvcnlJdGVtcyA9IF8uZ2V0KHJlcywgXCJib2R5LnJlc3BEYXRhLml0ZW1zXCIsIFtdKTtcbiAgICAgIHJldHVybiBoaXN0b3J5SXRlbXM7XG4gICAgfSk7XG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/history-timeline/index.tsx
