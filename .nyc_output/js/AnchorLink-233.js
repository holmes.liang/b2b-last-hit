__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}







var AnchorLink =
/*#__PURE__*/
function (_React$Component) {
  _inherits(AnchorLink, _React$Component);

  function AnchorLink() {
    var _this;

    _classCallCheck(this, AnchorLink);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AnchorLink).apply(this, arguments));

    _this.handleClick = function (e) {
      var _this$context$antAnch = _this.context.antAnchor,
          scrollTo = _this$context$antAnch.scrollTo,
          onClick = _this$context$antAnch.onClick;
      var _this$props = _this.props,
          href = _this$props.href,
          title = _this$props.title;

      if (onClick) {
        onClick(e, {
          title: title,
          href: href
        });
      }

      scrollTo(href);
    };

    _this.renderAnchorLink = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;
      var _this$props2 = _this.props,
          customizePrefixCls = _this$props2.prefixCls,
          href = _this$props2.href,
          title = _this$props2.title,
          children = _this$props2.children,
          className = _this$props2.className,
          target = _this$props2.target;
      var prefixCls = getPrefixCls('anchor', customizePrefixCls);
      var active = _this.context.antAnchor.activeLink === href;
      var wrapperClassName = classnames__WEBPACK_IMPORTED_MODULE_3___default()(className, "".concat(prefixCls, "-link"), _defineProperty({}, "".concat(prefixCls, "-link-active"), active));
      var titleClassName = classnames__WEBPACK_IMPORTED_MODULE_3___default()("".concat(prefixCls, "-link-title"), _defineProperty({}, "".concat(prefixCls, "-link-title-active"), active));
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: wrapperClassName
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", {
        className: titleClassName,
        href: href,
        title: typeof title === 'string' ? title : '',
        target: target,
        onClick: _this.handleClick
      }, title), children);
    };

    return _this;
  }

  _createClass(AnchorLink, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.context.antAnchor.registerLink(this.props.href);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(_ref2) {
      var prevHref = _ref2.href;
      var href = this.props.href;

      if (prevHref !== href) {
        this.context.antAnchor.unregisterLink(prevHref);
        this.context.antAnchor.registerLink(href);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.context.antAnchor.unregisterLink(this.props.href);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_4__["ConfigConsumer"], null, this.renderAnchorLink);
    }
  }]);

  return AnchorLink;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

AnchorLink.defaultProps = {
  href: '#'
};
AnchorLink.contextTypes = {
  antAnchor: prop_types__WEBPACK_IMPORTED_MODULE_1__["object"]
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__["polyfill"])(AnchorLink);
/* harmony default export */ __webpack_exports__["default"] = (AnchorLink);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9hbmNob3IvQW5jaG9yTGluay5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvYW5jaG9yL0FuY2hvckxpbmsuanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5jbGFzcyBBbmNob3JMaW5rIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcbiAgICAgICAgdGhpcy5oYW5kbGVDbGljayA9IChlKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHNjcm9sbFRvLCBvbkNsaWNrIH0gPSB0aGlzLmNvbnRleHQuYW50QW5jaG9yO1xuICAgICAgICAgICAgY29uc3QgeyBocmVmLCB0aXRsZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChvbkNsaWNrKSB7XG4gICAgICAgICAgICAgICAgb25DbGljayhlLCB7IHRpdGxlLCBocmVmIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2Nyb2xsVG8oaHJlZik7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyQW5jaG9yTGluayA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBocmVmLCB0aXRsZSwgY2hpbGRyZW4sIGNsYXNzTmFtZSwgdGFyZ2V0IH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdhbmNob3InLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3QgYWN0aXZlID0gdGhpcy5jb250ZXh0LmFudEFuY2hvci5hY3RpdmVMaW5rID09PSBocmVmO1xuICAgICAgICAgICAgY29uc3Qgd3JhcHBlckNsYXNzTmFtZSA9IGNsYXNzTmFtZXMoY2xhc3NOYW1lLCBgJHtwcmVmaXhDbHN9LWxpbmtgLCB7XG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tbGluay1hY3RpdmVgXTogYWN0aXZlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCB0aXRsZUNsYXNzTmFtZSA9IGNsYXNzTmFtZXMoYCR7cHJlZml4Q2xzfS1saW5rLXRpdGxlYCwge1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWxpbmstdGl0bGUtYWN0aXZlYF06IGFjdGl2ZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuICg8ZGl2IGNsYXNzTmFtZT17d3JhcHBlckNsYXNzTmFtZX0+XG4gICAgICAgIDxhIGNsYXNzTmFtZT17dGl0bGVDbGFzc05hbWV9IGhyZWY9e2hyZWZ9IHRpdGxlPXt0eXBlb2YgdGl0bGUgPT09ICdzdHJpbmcnID8gdGl0bGUgOiAnJ30gdGFyZ2V0PXt0YXJnZXR9IG9uQ2xpY2s9e3RoaXMuaGFuZGxlQ2xpY2t9PlxuICAgICAgICAgIHt0aXRsZX1cbiAgICAgICAgPC9hPlxuICAgICAgICB7Y2hpbGRyZW59XG4gICAgICA8L2Rpdj4pO1xuICAgICAgICB9O1xuICAgIH1cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgdGhpcy5jb250ZXh0LmFudEFuY2hvci5yZWdpc3RlckxpbmsodGhpcy5wcm9wcy5ocmVmKTtcbiAgICB9XG4gICAgY29tcG9uZW50RGlkVXBkYXRlKHsgaHJlZjogcHJldkhyZWYgfSkge1xuICAgICAgICBjb25zdCB7IGhyZWYgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmIChwcmV2SHJlZiAhPT0gaHJlZikge1xuICAgICAgICAgICAgdGhpcy5jb250ZXh0LmFudEFuY2hvci51bnJlZ2lzdGVyTGluayhwcmV2SHJlZik7XG4gICAgICAgICAgICB0aGlzLmNvbnRleHQuYW50QW5jaG9yLnJlZ2lzdGVyTGluayhocmVmKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgICAgdGhpcy5jb250ZXh0LmFudEFuY2hvci51bnJlZ2lzdGVyTGluayh0aGlzLnByb3BzLmhyZWYpO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyQW5jaG9yTGlua308L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG5BbmNob3JMaW5rLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBocmVmOiAnIycsXG59O1xuQW5jaG9yTGluay5jb250ZXh0VHlwZXMgPSB7XG4gICAgYW50QW5jaG9yOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcbnBvbHlmaWxsKEFuY2hvckxpbmspO1xuZXhwb3J0IGRlZmF1bHQgQW5jaG9yTGluaztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBTkE7QUFDQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUdBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWEE7QUFDQTtBQVhBO0FBMkJBO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7OztBQTVDQTtBQUNBO0FBNkNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/anchor/AnchorLink.js
