__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextDiv", function() { return TextDiv; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return RenderFeeSummary; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var number_precision__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! number-precision */ "./node_modules/number-precision/build/index.js");
/* harmony import */ var number_precision__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(number_precision__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _desk_component_view_item_view_item_copy__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-component/view-item/view-item-copy */ "./src/app/desk/component/view-item/view-item-copy.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/component/render-fee-summary.tsx";

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__["default"])(["\n  .ant-input-number-input {\n    text-align: right\n  }\n  .ant-col.group {\n    >div:nth-child(1) {\n      width: 40% !important;\n      margin-right: 10px\n      .ant-row.ant-form-item {\n        width: 100% !important\n        // .ant-input-number{\n        //   height: 40px\n        //   .ant-input-number-input-wrap {\n        //     height: 100%;\n        //     .ant-input-number-input {\n        //     height: 100%;\n        //     }\n        //   }\n        // }\n      }\n    }\n    >div:nth-child(2) {\n     width: 100% !important\n     .ant-row.ant-form-item {\n      width: 100% !important\n     }\n    }\n  }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__["default"])(["\n  .ant-row {\n   margin-left: -12px;\n   margin-right: -12px;\n   display: flex;\n   align-items: center;\n  }\n  .endo-label-col {\n    text-transform : uppercase;\n    padding: 10px 14px 10px 12px;\n    text-align: right;\n    color: #9e9e9e;\n  }\n  .endo-text-col {\n    display: flex;\n    text-align: right;\n    align-items: center;\n    padding: 0 12px;\n    .rate-div-text {\n      height:100%;\n      width: 30%;\n      font-size: 16px;\n      color: rgba(0,0,0,0.85);\n    }\n    .amount-div-text {\n      width: 100%;\n      height:100%;\n      font-size: 16px;\n      color: rgba(0,0,0,0.85);\n    }\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__["default"])(["\n  .ant-col.itemTitle {\n    padding-right: 14px !important\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}









var ViewDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_13__["Styled"].div(_templateObject());
var EndoText = _common_3rd__WEBPACK_IMPORTED_MODULE_13__["Styled"].div(_templateObject2());
var TextDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_13__["Styled"].div(_templateObject3());
var defaultLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 10
    }
  }
};

var ViewItem = function ViewItem(props) {
  return react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ViewDiv, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91
    },
    __self: this
  }, Object(_desk_component_view_item_view_item_copy__WEBPACK_IMPORTED_MODULE_14__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_6__["default"])({}, props, {
    layout: defaultLayout
  })));
};

var RenderFeeSummary =
/*#__PURE__*/
function (_Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(RenderFeeSummary, _Component);

  function RenderFeeSummary() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, RenderFeeSummary);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(RenderFeeSummary)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      commissionRate: ""
    };
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(RenderFeeSummary, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          isEndo = _this$props.isEndo,
          that = _this$props.that,
          generatePropName = _this$props.generatePropName,
          premiumId = _this$props.premiumId,
          getProp = _this$props.getProp;

      if (isEndo) {
        var modelRate = that.getValueFromModel(generatePropName("commissionRate", "feeSummary"));
        this.setState({
          commissionRate: modelRate
        }); // const ad = that.getValueFromModel(generatePropName("adjustedPremium", "feeSummary"));

        var commissionP; // if (ad === "" || ad === null || !ad) {

        var totalP = that.getValueFromModel(getProp("".concat(premiumId, ".lumpsum.gwp")));
        commissionP = (parseFloat(totalP) * (parseFloat(modelRate) || 0)).toFixed(2); // } else {
        //   commissionP = ((parseFloat(ad) || 0) * (parseFloat(modelRate) || 0)).toFixed(2);
        //
        // }

        that.setValueToModel(commissionP, generatePropName("commissionAmount", "feeSummary"));
      } else {
        var commissionRate = that.getValueFromModel(generatePropName("commissionRate", "feeSummary"));
        this.setState({
          commissionRate: commissionRate
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          form = _this$props2.form,
          model = _this$props2.model,
          currency = _this$props2.currency,
          that = _this$props2.that,
          generatePropName = _this$props2.generatePropName,
          getProp = _this$props2.getProp,
          premiumId = _this$props2.premiumId,
          isEndo = _this$props2.isEndo;
      return react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_8___default.a.Fragment, null, isEndo ? react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Endorsement Premium").thai("Endorsement Premium").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 157
        },
        __self: this
      }, currency, " ", that.renderThousandNum(that.getValueFromModel(getProp("".concat(premiumId, ".lumpsum.gwp"))))) : react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("PREMIUM").thai("PREMIUM").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 162
        },
        __self: this
      }, currency, " ", that.renderThousandNum(that.getValueFromModel(getProp("".concat(premiumId, ".lumpsum.gwp"))))), react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("GST").thai("GST").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 190
        },
        __self: this
      }, currency, " ", that.renderThousandNum(that.calcGst())), react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ViewItem, {
        title: isEndo ? _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Total").thai("Total").getMessage() : _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Total Premium").thai("Total Premium").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 195
        },
        __self: this
      }, currency, " ", that.renderThousandNum(that.calcAmount())), isEndo ? react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(EndoText, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 207
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_15__["Row"], {
        justify: "start",
        align: "middle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 208
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_15__["Col"], {
        className: "endo-label-col",
        xs: 8,
        sm: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 209
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("COMMISSION").thai("COMMISSION").getMessage()), react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_15__["Col"], {
        className: "endo-text-col",
        xs: 8,
        sm: 10,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 214
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("div", {
        className: "rate-div-text",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 215
        },
        __self: this
      }, number_precision__WEBPACK_IMPORTED_MODULE_10___default.a.times(that.getValueFromModel(generatePropName("commissionRate", "feeSummary")), 100), "%"), react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("div", {
        className: "amount-div-text",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 218
        },
        __self: this
      }, currency, " ", that.calcCommission())))) : react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(TextDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 225
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(_desk_component__WEBPACK_IMPORTED_MODULE_12__["FieldGroup"], {
        className: "date-group ndate-group",
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 8,
          sm: 10
        },
        firstChildWithPercent: 70,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 226
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NRate"], {
        label: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Commission").thai("Commission").getMessage(),
        propName: generatePropName("commissionRate", "feeSummary"),
        style: {
          width: "100%"
        },
        precision: 2,
        allowZero: true,
        size: "large",
        originalValue: this.state.commissionRate,
        model: model,
        form: form,
        required: true,
        onChange: function onChange(value) {
          var modelRate = that.getValueFromModel(generatePropName("commissionRate", "feeSummary"));

          if (_common__WEBPACK_IMPORTED_MODULE_9__["Rules"].isNum(modelRate)) {
            // const ad = that.getValueFromModel(generatePropName("adjustedPremium", "feeSummary"));
            var commissionP; // if (ad === "" || ad === null || !ad) {

            var totalP = that.getValueFromModel(getProp("".concat(premiumId, ".lumpsum.gwp")));
            commissionP = (parseFloat(totalP || 0) * (parseFloat(modelRate) || 0)).toFixed(2); // } else {
            //   commissionP = ((parseFloat(ad) || 0) * (parseFloat(modelRate) || 0)).toFixed(2);
            //
            // }

            that.setValueToModel(commissionP, generatePropName("commissionAmount", "feeSummary"));

            _this2.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])({}, generatePropName("commissionAmount", "feeSummary"), commissionP));
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 230
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPrice"], {
        form: form,
        model: model,
        allowZero: true,
        propName: generatePropName("commissionAmount", "feeSummary"),
        label: "",
        size: "large",
        rules: [{
          required: true,
          message: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Commission is required").thai("Commission is required").getMessage()
        }],
        onChange: function onChange(value) {
          if (_common__WEBPACK_IMPORTED_MODULE_9__["Rules"].isNum(value)) {
            // const ad = that.getValueFromModel(generatePropName("adjustedPremium", "feeSummary"));
            var commissionR; // if (ad === "" || ad === null || !ad) {

            var totalP = that.getValueFromModel(getProp("".concat(premiumId, ".lumpsum.gwp")));

            if (totalP) {
              commissionR = ((parseFloat(value) || 0) / parseFloat(totalP) || 0).toFixed(4); // } else {
              //   commissionR = (((parseFloat(value) || 0) / (parseFloat(ad) || 0)) || 0).toFixed(4);
              // }
              // that.setValueToModel(commissionR, generatePropName("commissionRate", "feeSummary"));
              // this.props.form.setFieldsValue({
              //   [generatePropName("commissionRate", "feeSummary")]: commissionR,
              // });

              _this2.setState({
                commissionRate: commissionR
              });
            }
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 262
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Commission GST").thai("Commission GST").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 300
        },
        __self: this
      }, currency, " ", that.renderThousandNum(that.calcCommissionGst())), react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Total Commission").thai("Total Commission").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 305
        },
        __self: this
      }, currency, " ", that.renderThousandNum(that.calcTotalCommission())), react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Net Premium").thai("Net Premium").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 310
        },
        __self: this
      }, currency, " ", that.renderThousandNum(that.calcNetPremium())));
    }
  }]);

  return RenderFeeSummary;
}(react__WEBPACK_IMPORTED_MODULE_8__["Component"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3JlbmRlci1mZWUtc3VtbWFyeS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9TQUlDL2lhci9jb21wb25lbnQvcmVuZGVyLWZlZS1zdW1tYXJ5LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBMYW5ndWFnZSwgUnVsZXMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IE5QIGZyb20gXCJudW1iZXItcHJlY2lzaW9uXCI7XG5cbmltcG9ydCB7IE5QcmljZSwgTlJhdGUgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IEZpZWxkR3JvdXAgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50XCI7XG5pbXBvcnQgeyBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCBWaWV3SXRlbTEgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC92aWV3LWl0ZW0vdmlldy1pdGVtLWNvcHlcIjtcbmltcG9ydCB7IENvbCwgUm93IH0gZnJvbSBcImFudGRcIjtcblxuY29uc3QgVmlld0RpdiA9IFN0eWxlZC5kaXZgXG4gIC5hbnQtY29sLml0ZW1UaXRsZSB7XG4gICAgcGFkZGluZy1yaWdodDogMTRweCAhaW1wb3J0YW50XG4gIH1cbmA7XG5cbmNvbnN0IEVuZG9UZXh0ID0gU3R5bGVkLmRpdmBcbiAgLmFudC1yb3cge1xuICAgbWFyZ2luLWxlZnQ6IC0xMnB4O1xuICAgbWFyZ2luLXJpZ2h0OiAtMTJweDtcbiAgIGRpc3BsYXk6IGZsZXg7XG4gICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC5lbmRvLWxhYmVsLWNvbCB7XG4gICAgdGV4dC10cmFuc2Zvcm0gOiB1cHBlcmNhc2U7XG4gICAgcGFkZGluZzogMTBweCAxNHB4IDEwcHggMTJweDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBjb2xvcjogIzllOWU5ZTtcbiAgfVxuICAuZW5kby10ZXh0LWNvbCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHBhZGRpbmc6IDAgMTJweDtcbiAgICAucmF0ZS1kaXYtdGV4dCB7XG4gICAgICBoZWlnaHQ6MTAwJTtcbiAgICAgIHdpZHRoOiAzMCU7XG4gICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICBjb2xvcjogcmdiYSgwLDAsMCwwLjg1KTtcbiAgICB9XG4gICAgLmFtb3VudC1kaXYtdGV4dCB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDoxMDAlO1xuICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgY29sb3I6IHJnYmEoMCwwLDAsMC44NSk7XG4gICAgfVxuICB9XG5gO1xuXG5leHBvcnQgY29uc3QgVGV4dERpdiA9IFN0eWxlZC5kaXZgXG4gIC5hbnQtaW5wdXQtbnVtYmVyLWlucHV0IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodFxuICB9XG4gIC5hbnQtY29sLmdyb3VwIHtcbiAgICA+ZGl2Om50aC1jaGlsZCgxKSB7XG4gICAgICB3aWR0aDogNDAlICFpbXBvcnRhbnQ7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDEwcHhcbiAgICAgIC5hbnQtcm93LmFudC1mb3JtLWl0ZW0ge1xuICAgICAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50XG4gICAgICAgIC8vIC5hbnQtaW5wdXQtbnVtYmVye1xuICAgICAgICAvLyAgIGhlaWdodDogNDBweFxuICAgICAgICAvLyAgIC5hbnQtaW5wdXQtbnVtYmVyLWlucHV0LXdyYXAge1xuICAgICAgICAvLyAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICAvLyAgICAgLmFudC1pbnB1dC1udW1iZXItaW5wdXQge1xuICAgICAgICAvLyAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyAgIH1cbiAgICAgICAgLy8gfVxuICAgICAgfVxuICAgIH1cbiAgICA+ZGl2Om50aC1jaGlsZCgyKSB7XG4gICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnRcbiAgICAgLmFudC1yb3cuYW50LWZvcm0taXRlbSB7XG4gICAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50XG4gICAgIH1cbiAgICB9XG4gIH1cbmA7XG5cbmNvbnN0IGRlZmF1bHRMYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogOCB9LFxuICAgIHNtOiB7IHNwYW46IDYgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDggfSxcbiAgICBzbTogeyBzcGFuOiAxMCB9LFxuICB9LFxufTtcblxuY29uc3QgVmlld0l0ZW0gPSAocHJvcHM6IGFueSkgPT4gPFZpZXdEaXY+e1ZpZXdJdGVtMSh7IC4uLnByb3BzLCBsYXlvdXQ6IGRlZmF1bHRMYXlvdXQgfSl9PC9WaWV3RGl2PjtcblxuaW50ZXJmYWNlIElQcm9wcyB7XG4gIGZvcm06IGFueSxcbiAgbW9kZWw6IGFueSxcbiAgY3VycmVuY3k6IGFueSxcbiAgdGhhdDogYW55LFxuICBnZW5lcmF0ZVByb3BOYW1lOiBhbnksXG4gIGdldFByb3A6IGFueSxcbiAgcHJlbWl1bUlkOiBhbnksXG4gIGlzRW5kbz86IGJvb2xlYW5cbn1cblxuaW50ZXJmYWNlIElTdGF0ZSB7XG4gIGNvbW1pc3Npb25SYXRlOiBhbnlcbn1cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmVuZGVyRmVlU3VtbWFyeSBleHRlbmRzIENvbXBvbmVudDxJUHJvcHMsIElTdGF0ZT4ge1xuICBzdGF0ZTogSVN0YXRlID0ge1xuICAgIGNvbW1pc3Npb25SYXRlOiBcIlwiLFxuICB9O1xuXG4gIGNvbXBvbmVudERpZE1vdW50KCk6IHZvaWQge1xuICAgIGNvbnN0IHtcbiAgICAgIGlzRW5kbyxcbiAgICAgIHRoYXQsXG4gICAgICBnZW5lcmF0ZVByb3BOYW1lLFxuICAgICAgcHJlbWl1bUlkLFxuICAgICAgZ2V0UHJvcCxcbiAgICB9ID0gdGhpcy5wcm9wcztcbiAgICBpZiAoaXNFbmRvKSB7XG4gICAgICBjb25zdCBtb2RlbFJhdGUgPSB0aGF0LmdldFZhbHVlRnJvbU1vZGVsKGdlbmVyYXRlUHJvcE5hbWUoXCJjb21taXNzaW9uUmF0ZVwiLCBcImZlZVN1bW1hcnlcIikpO1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGNvbW1pc3Npb25SYXRlOiBtb2RlbFJhdGUsXG4gICAgICB9KTtcbiAgICAgIC8vIGNvbnN0IGFkID0gdGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZW5lcmF0ZVByb3BOYW1lKFwiYWRqdXN0ZWRQcmVtaXVtXCIsIFwiZmVlU3VtbWFyeVwiKSk7XG4gICAgICBsZXQgY29tbWlzc2lvblA6IGFueTtcbiAgICAgIC8vIGlmIChhZCA9PT0gXCJcIiB8fCBhZCA9PT0gbnVsbCB8fCAhYWQpIHtcbiAgICAgIGNvbnN0IHRvdGFsUCA9IHRoYXQuZ2V0VmFsdWVGcm9tTW9kZWwoZ2V0UHJvcChgJHtwcmVtaXVtSWR9Lmx1bXBzdW0uZ3dwYCkpO1xuICAgICAgY29tbWlzc2lvblAgPSAocGFyc2VGbG9hdCh0b3RhbFApICogKHBhcnNlRmxvYXQobW9kZWxSYXRlKSB8fCAwKSkudG9GaXhlZCgyKTtcbiAgICAgIC8vIH0gZWxzZSB7XG4gICAgICAvLyAgIGNvbW1pc3Npb25QID0gKChwYXJzZUZsb2F0KGFkKSB8fCAwKSAqIChwYXJzZUZsb2F0KG1vZGVsUmF0ZSkgfHwgMCkpLnRvRml4ZWQoMik7XG4gICAgICAvL1xuICAgICAgLy8gfVxuICAgICAgdGhhdC5zZXRWYWx1ZVRvTW9kZWwoY29tbWlzc2lvblAsIGdlbmVyYXRlUHJvcE5hbWUoXCJjb21taXNzaW9uQW1vdW50XCIsIFwiZmVlU3VtbWFyeVwiKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IGNvbW1pc3Npb25SYXRlID0gdGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZW5lcmF0ZVByb3BOYW1lKFwiY29tbWlzc2lvblJhdGVcIiwgXCJmZWVTdW1tYXJ5XCIpKTtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBjb21taXNzaW9uUmF0ZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7XG4gICAgICBmb3JtLFxuICAgICAgbW9kZWwsXG4gICAgICBjdXJyZW5jeSxcbiAgICAgIHRoYXQsXG4gICAgICBnZW5lcmF0ZVByb3BOYW1lLFxuICAgICAgZ2V0UHJvcCxcbiAgICAgIHByZW1pdW1JZCxcbiAgICAgIGlzRW5kbyxcbiAgICB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPD5cbiAgICAgIHtpc0VuZG8gP1xuICAgICAgICA8Vmlld0l0ZW0gdGl0bGU9e0xhbmd1YWdlLmVuKFwiRW5kb3JzZW1lbnQgUHJlbWl1bVwiKVxuICAgICAgICAgIC50aGFpKFwiRW5kb3JzZW1lbnQgUHJlbWl1bVwiKVxuICAgICAgICAgIC5nZXRNZXNzYWdlKCl9PlxuICAgICAgICAgIHtjdXJyZW5jeX0ge3RoYXQucmVuZGVyVGhvdXNhbmROdW0odGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZXRQcm9wKGAke3ByZW1pdW1JZH0ubHVtcHN1bS5nd3BgKSkpfVxuICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICA6IDxWaWV3SXRlbSB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJQUkVNSVVNXCIpXG4gICAgICAgICAgLnRoYWkoXCJQUkVNSVVNXCIpXG4gICAgICAgICAgLmdldE1lc3NhZ2UoKX0+XG4gICAgICAgICAge2N1cnJlbmN5fSB7dGhhdC5yZW5kZXJUaG91c2FuZE51bSh0aGF0LmdldFZhbHVlRnJvbU1vZGVsKGdldFByb3AoYCR7cHJlbWl1bUlkfS5sdW1wc3VtLmd3cGApKSl9XG4gICAgICAgIDwvVmlld0l0ZW0+fVxuICAgICAgey8qPFJpZ2h0RGl2PiovfVxuICAgICAgey8qICA8TlByaWNlKi99XG4gICAgICB7LyogICAgZm9ybT17Zm9ybX0qL31cbiAgICAgIHsvKiAgICBtb2RlbD17bW9kZWx9Ki99XG4gICAgICB7LyogICAgc2l6ZT1cImxhcmdlXCIqL31cbiAgICAgIHsvKiAgICBsYXlvdXRDb2w9e2RlZmF1bHRMYXlvdXR9Ki99XG4gICAgICB7LyogICAgcHJvcE5hbWU9e2dlbmVyYXRlUHJvcE5hbWUoXCJhZGp1c3RlZFByZW1pdW1cIiwgXCJmZWVTdW1tYXJ5XCIpfSovfVxuICAgICAgey8qICAgIHJlcXVpcmVkPXt0cnVlfSovfVxuICAgICAgey8qICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIkFESlVTVEVEIFBSRU1JVU1cIikqL31cbiAgICAgIHsvKiAgICAgIC50aGFpKFwiQURKVVNURUQgUFJFTUlVTVwiKS5teShcIkFESlVTVEVEIFBSRU1JVU1cIikqL31cbiAgICAgIHsvKiAgICAgIC5nZXRNZXNzYWdlKCl9Ki99XG4gICAgICB7LyogICAgb25DaGFuZ2U9eyh2YWx1ZTogYW55KSA9PiB7Ki99XG4gICAgICB7LyogICAgICBpZiAoUnVsZXMuaXNOdW0odmFsdWUpKSB7Ki99XG4gICAgICB7LyogICAgICAgIGNvbnN0IGNvbW1pc3Npb25SYXRlID0gdGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZW5lcmF0ZVByb3BOYW1lKFwiY29tbWlzc2lvblJhdGVcIiwgXCJmZWVTdW1tYXJ5XCIpKTsqL31cbiAgICAgIHsvKiAgICAgICAgY29uc3QgY29tbWlzc2lvblAgPSAocGFyc2VGbG9hdCh2YWx1ZSkgKiBwYXJzZUZsb2F0KGNvbW1pc3Npb25SYXRlKSkgfHwgMDsqL31cbiAgICAgIHsvKiAgICAgICAgdGhhdC5zZXRWYWx1ZVRvTW9kZWwoY29tbWlzc2lvblAsIGdlbmVyYXRlUHJvcE5hbWUoXCJjb21taXNzaW9uQW1vdW50XCIsIFwiZmVlU3VtbWFyeVwiKSk7Ki99XG4gICAgICB7LyogICAgICAgIHRoaXMucHJvcHMuZm9ybS5zZXRGaWVsZHNWYWx1ZSh7Ki99XG4gICAgICB7LyogICAgICAgICAgW2dlbmVyYXRlUHJvcE5hbWUoXCJjb21taXNzaW9uQW1vdW50XCIsIFwiZmVlU3VtbWFyeVwiKV06IGNvbW1pc3Npb25QLCovfVxuICAgICAgey8qICAgICAgICB9KTsqL31cbiAgICAgIHsvKiAgICAgIH0qL31cbiAgICAgIHsvKiAgICB9fSovfVxuICAgICAgey8qICAvPiovfVxuICAgICAgey8qPC9SaWdodERpdj4qL31cbiAgICAgIDxWaWV3SXRlbSB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJHU1RcIilcbiAgICAgICAgLnRoYWkoXCJHU1RcIilcbiAgICAgICAgLmdldE1lc3NhZ2UoKX0+XG4gICAgICAgIHtjdXJyZW5jeX0ge3RoYXQucmVuZGVyVGhvdXNhbmROdW0odGhhdC5jYWxjR3N0KCkpfVxuICAgICAgPC9WaWV3SXRlbT5cbiAgICAgIDxWaWV3SXRlbSB0aXRsZT17aXNFbmRvID9cbiAgICAgICAgTGFuZ3VhZ2UuZW4oXCJUb3RhbFwiKVxuICAgICAgICAgIC50aGFpKFwiVG90YWxcIilcbiAgICAgICAgICAuZ2V0TWVzc2FnZSgpXG4gICAgICAgIDpcbiAgICAgICAgTGFuZ3VhZ2UuZW4oXCJUb3RhbCBQcmVtaXVtXCIpXG4gICAgICAgICAgLnRoYWkoXCJUb3RhbCBQcmVtaXVtXCIpXG4gICAgICAgICAgLmdldE1lc3NhZ2UoKX0+XG4gICAgICAgIHtjdXJyZW5jeX0ge3RoYXQucmVuZGVyVGhvdXNhbmROdW0odGhhdC5jYWxjQW1vdW50KCkpfVxuICAgICAgPC9WaWV3SXRlbT5cbiAgICAgIHtcbiAgICAgICAgaXNFbmRvID9cbiAgICAgICAgICA8RW5kb1RleHQ+XG4gICAgICAgICAgICA8Um93IGp1c3RpZnk9e1wic3RhcnRcIn0gYWxpZ249XCJtaWRkbGVcIj5cbiAgICAgICAgICAgICAgPENvbCBjbGFzc05hbWU9e1wiZW5kby1sYWJlbC1jb2xcIn0geHM9ezh9IHNtPXs2fT5cbiAgICAgICAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJDT01NSVNTSU9OXCIpXG4gICAgICAgICAgICAgICAgICAudGhhaShcIkNPTU1JU1NJT05cIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICA8Q29sIGNsYXNzTmFtZT17XCJlbmRvLXRleHQtY29sXCJ9IHhzPXs4fSBzbT17MTB9PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtcInJhdGUtZGl2LXRleHRcIn0+XG4gICAgICAgICAgICAgICAgICB7TlAudGltZXModGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZW5lcmF0ZVByb3BOYW1lKFwiY29tbWlzc2lvblJhdGVcIiwgXCJmZWVTdW1tYXJ5XCIpKSwgMTAwKX0lXG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e1wiYW1vdW50LWRpdi10ZXh0XCJ9PlxuICAgICAgICAgICAgICAgICAge2N1cnJlbmN5fSB7dGhhdC5jYWxjQ29tbWlzc2lvbigpfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgIDwvRW5kb1RleHQ+XG4gICAgICAgICAgOlxuICAgICAgICAgIDxUZXh0RGl2PlxuICAgICAgICAgICAgPEZpZWxkR3JvdXAgY2xhc3NOYW1lPVwiZGF0ZS1ncm91cCBuZGF0ZS1ncm91cFwiIHNlbGVjdFhzU209e3sgeHM6IDgsIHNtOiA2IH19XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0WHNTbT17eyB4czogOCwgc206IDEwIH19XG4gICAgICAgICAgICAgICAgICAgICAgICBmaXJzdENoaWxkV2l0aFBlcmNlbnQ9ezcwfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8TlJhdGVcbiAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJDb21taXNzaW9uXCIpXG4gICAgICAgICAgICAgICAgICAudGhhaShcIkNvbW1pc3Npb25cIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgcHJvcE5hbWU9e2dlbmVyYXRlUHJvcE5hbWUoXCJjb21taXNzaW9uUmF0ZVwiLCBcImZlZVN1bW1hcnlcIil9XG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiMTAwJVwiIH19XG4gICAgICAgICAgICAgICAgcHJlY2lzaW9uPXsyfVxuICAgICAgICAgICAgICAgIGFsbG93WmVybz17dHJ1ZX1cbiAgICAgICAgICAgICAgICBzaXplPVwibGFyZ2VcIlxuICAgICAgICAgICAgICAgIG9yaWdpbmFsVmFsdWU9e3RoaXMuc3RhdGUuY29tbWlzc2lvblJhdGV9XG4gICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgcmVxdWlyZWRcbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgIGNvbnN0IG1vZGVsUmF0ZSA9IHRoYXQuZ2V0VmFsdWVGcm9tTW9kZWwoZ2VuZXJhdGVQcm9wTmFtZShcImNvbW1pc3Npb25SYXRlXCIsIFwiZmVlU3VtbWFyeVwiKSk7XG4gICAgICAgICAgICAgICAgICBpZiAoUnVsZXMuaXNOdW0obW9kZWxSYXRlKSkge1xuICAgICAgICAgICAgICAgICAgICAvLyBjb25zdCBhZCA9IHRoYXQuZ2V0VmFsdWVGcm9tTW9kZWwoZ2VuZXJhdGVQcm9wTmFtZShcImFkanVzdGVkUHJlbWl1bVwiLCBcImZlZVN1bW1hcnlcIikpO1xuICAgICAgICAgICAgICAgICAgICBsZXQgY29tbWlzc2lvblA6IGFueTtcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgKGFkID09PSBcIlwiIHx8IGFkID09PSBudWxsIHx8ICFhZCkge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCB0b3RhbFAgPSB0aGF0LmdldFZhbHVlRnJvbU1vZGVsKGdldFByb3AoYCR7cHJlbWl1bUlkfS5sdW1wc3VtLmd3cGApKTtcbiAgICAgICAgICAgICAgICAgICAgY29tbWlzc2lvblAgPSAocGFyc2VGbG9hdCh0b3RhbFAgfHwgMCkgKiAocGFyc2VGbG9hdChtb2RlbFJhdGUpIHx8IDApKS50b0ZpeGVkKDIpO1xuICAgICAgICAgICAgICAgICAgICAvLyB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAvLyAgIGNvbW1pc3Npb25QID0gKChwYXJzZUZsb2F0KGFkKSB8fCAwKSAqIChwYXJzZUZsb2F0KG1vZGVsUmF0ZSkgfHwgMCkpLnRvRml4ZWQoMik7XG4gICAgICAgICAgICAgICAgICAgIC8vXG4gICAgICAgICAgICAgICAgICAgIC8vIH1cbiAgICAgICAgICAgICAgICAgICAgdGhhdC5zZXRWYWx1ZVRvTW9kZWwoY29tbWlzc2lvblAsIGdlbmVyYXRlUHJvcE5hbWUoXCJjb21taXNzaW9uQW1vdW50XCIsIFwiZmVlU3VtbWFyeVwiKSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZm9ybS5zZXRGaWVsZHNWYWx1ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgW2dlbmVyYXRlUHJvcE5hbWUoXCJjb21taXNzaW9uQW1vdW50XCIsIFwiZmVlU3VtbWFyeVwiKV06IGNvbW1pc3Npb25QLFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8TlByaWNlXG4gICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgYWxsb3daZXJvPXt0cnVlfVxuICAgICAgICAgICAgICAgIHByb3BOYW1lPXtnZW5lcmF0ZVByb3BOYW1lKFwiY29tbWlzc2lvbkFtb3VudFwiLCBcImZlZVN1bW1hcnlcIil9XG4gICAgICAgICAgICAgICAgbGFiZWw9e1wiXCJ9XG4gICAgICAgICAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICAgICAgICBydWxlcz17W1xuICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogTGFuZ3VhZ2UuZW4oXCJDb21taXNzaW9uIGlzIHJlcXVpcmVkXCIpLnRoYWkoXCJDb21taXNzaW9uIGlzIHJlcXVpcmVkXCIpLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgXX1cbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgIGlmIChSdWxlcy5pc051bSh2YWx1ZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gY29uc3QgYWQgPSB0aGF0LmdldFZhbHVlRnJvbU1vZGVsKGdlbmVyYXRlUHJvcE5hbWUoXCJhZGp1c3RlZFByZW1pdW1cIiwgXCJmZWVTdW1tYXJ5XCIpKTtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGNvbW1pc3Npb25SOiBhbnk7XG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIChhZCA9PT0gXCJcIiB8fCBhZCA9PT0gbnVsbCB8fCAhYWQpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdG90YWxQID0gdGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZXRQcm9wKGAke3ByZW1pdW1JZH0ubHVtcHN1bS5nd3BgKSk7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0b3RhbFApIHtcbiAgICAgICAgICAgICAgICAgICAgICBjb21taXNzaW9uUiA9ICgoKHBhcnNlRmxvYXQodmFsdWUpIHx8IDApIC8gcGFyc2VGbG9hdCh0b3RhbFApKSB8fCAwKS50b0ZpeGVkKDQpO1xuICAgICAgICAgICAgICAgICAgICAgIC8vIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgLy8gICBjb21taXNzaW9uUiA9ICgoKHBhcnNlRmxvYXQodmFsdWUpIHx8IDApIC8gKHBhcnNlRmxvYXQoYWQpIHx8IDApKSB8fCAwKS50b0ZpeGVkKDQpO1xuICAgICAgICAgICAgICAgICAgICAgIC8vIH1cbiAgICAgICAgICAgICAgICAgICAgICAvLyB0aGF0LnNldFZhbHVlVG9Nb2RlbChjb21taXNzaW9uUiwgZ2VuZXJhdGVQcm9wTmFtZShcImNvbW1pc3Npb25SYXRlXCIsIFwiZmVlU3VtbWFyeVwiKSk7XG4gICAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHtcbiAgICAgICAgICAgICAgICAgICAgICAvLyAgIFtnZW5lcmF0ZVByb3BOYW1lKFwiY29tbWlzc2lvblJhdGVcIiwgXCJmZWVTdW1tYXJ5XCIpXTogY29tbWlzc2lvblIsXG4gICAgICAgICAgICAgICAgICAgICAgLy8gfSk7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb21taXNzaW9uUmF0ZTogY29tbWlzc2lvblIsXG4gICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9GaWVsZEdyb3VwPlxuICAgICAgICAgIDwvVGV4dERpdj5cbiAgICAgIH1cbiAgICAgIDxWaWV3SXRlbSB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJDb21taXNzaW9uIEdTVFwiKVxuICAgICAgICAudGhhaShcIkNvbW1pc3Npb24gR1NUXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCl9PlxuICAgICAgICB7Y3VycmVuY3l9IHt0aGF0LnJlbmRlclRob3VzYW5kTnVtKHRoYXQuY2FsY0NvbW1pc3Npb25Hc3QoKSl9XG4gICAgICA8L1ZpZXdJdGVtPlxuICAgICAgPFZpZXdJdGVtIHRpdGxlPXtMYW5ndWFnZS5lbihcIlRvdGFsIENvbW1pc3Npb25cIilcbiAgICAgICAgLnRoYWkoXCJUb3RhbCBDb21taXNzaW9uXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCl9PlxuICAgICAgICB7Y3VycmVuY3l9IHt0aGF0LnJlbmRlclRob3VzYW5kTnVtKHRoYXQuY2FsY1RvdGFsQ29tbWlzc2lvbigpKX1cbiAgICAgIDwvVmlld0l0ZW0+XG4gICAgICA8Vmlld0l0ZW0gdGl0bGU9e0xhbmd1YWdlLmVuKFwiTmV0IFByZW1pdW1cIilcbiAgICAgICAgLnRoYWkoXCJOZXQgUHJlbWl1bVwiKVxuICAgICAgICAuZ2V0TWVzc2FnZSgpfT5cbiAgICAgICAge2N1cnJlbmN5fSB7dGhhdC5yZW5kZXJUaG91c2FuZE51bSh0aGF0LmNhbGNOZXRQcmVtaXVtKCkpfVxuICAgICAgPC9WaWV3SXRlbT5cbiAgICA8Lz47XG4gIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQU1BO0FBaUNBO0FBOEJBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFMQTtBQUNBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBZ0JBOzs7Ozs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFEQTs7Ozs7O0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUE0QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUE5QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFqQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBc0NBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BOzs7O0FBaE5BO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/component/render-fee-summary.tsx
