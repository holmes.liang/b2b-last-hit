/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var BoundingRect = __webpack_require__(/*! zrender/lib/core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");

var bbox = __webpack_require__(/*! zrender/lib/core/bbox */ "./node_modules/zrender/lib/core/bbox.js");

var vec2 = __webpack_require__(/*! zrender/lib/core/vector */ "./node_modules/zrender/lib/core/vector.js");

var polygonContain = __webpack_require__(/*! zrender/lib/contain/polygon */ "./node_modules/zrender/lib/contain/polygon.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @module echarts/coord/geo/Region
 */

/**
 * @param {string|Region} name
 * @param {Array} geometries
 * @param {Array.<number>} cp
 */


function Region(name, geometries, cp) {
  /**
   * @type {string}
   * @readOnly
   */
  this.name = name;
  /**
   * @type {Array.<Array>}
   * @readOnly
   */

  this.geometries = geometries;

  if (!cp) {
    var rect = this.getBoundingRect();
    cp = [rect.x + rect.width / 2, rect.y + rect.height / 2];
  } else {
    cp = [cp[0], cp[1]];
  }
  /**
   * @type {Array.<number>}
   */


  this.center = cp;
}

Region.prototype = {
  constructor: Region,
  properties: null,

  /**
   * @return {module:zrender/core/BoundingRect}
   */
  getBoundingRect: function getBoundingRect() {
    var rect = this._rect;

    if (rect) {
      return rect;
    }

    var MAX_NUMBER = Number.MAX_VALUE;
    var min = [MAX_NUMBER, MAX_NUMBER];
    var max = [-MAX_NUMBER, -MAX_NUMBER];
    var min2 = [];
    var max2 = [];
    var geometries = this.geometries;

    for (var i = 0; i < geometries.length; i++) {
      // Only support polygon
      if (geometries[i].type !== 'polygon') {
        continue;
      } // Doesn't consider hole


      var exterior = geometries[i].exterior;
      bbox.fromPoints(exterior, min2, max2);
      vec2.min(min, min, min2);
      vec2.max(max, max, max2);
    } // No data


    if (i === 0) {
      min[0] = min[1] = max[0] = max[1] = 0;
    }

    return this._rect = new BoundingRect(min[0], min[1], max[0] - min[0], max[1] - min[1]);
  },

  /**
   * @param {<Array.<number>} coord
   * @return {boolean}
   */
  contain: function contain(coord) {
    var rect = this.getBoundingRect();
    var geometries = this.geometries;

    if (!rect.contain(coord[0], coord[1])) {
      return false;
    }

    loopGeo: for (var i = 0, len = geometries.length; i < len; i++) {
      // Only support polygon.
      if (geometries[i].type !== 'polygon') {
        continue;
      }

      var exterior = geometries[i].exterior;
      var interiors = geometries[i].interiors;

      if (polygonContain.contain(exterior, coord[0], coord[1])) {
        // Not in the region if point is in the hole.
        for (var k = 0; k < (interiors ? interiors.length : 0); k++) {
          if (polygonContain.contain(interiors[k])) {
            continue loopGeo;
          }
        }

        return true;
      }
    }

    return false;
  },
  transformTo: function transformTo(x, y, width, height) {
    var rect = this.getBoundingRect();
    var aspect = rect.width / rect.height;

    if (!width) {
      width = aspect * height;
    } else if (!height) {
      height = width / aspect;
    }

    var target = new BoundingRect(x, y, width, height);
    var transform = rect.calculateTransform(target);
    var geometries = this.geometries;

    for (var i = 0; i < geometries.length; i++) {
      // Only support polygon.
      if (geometries[i].type !== 'polygon') {
        continue;
      }

      var exterior = geometries[i].exterior;
      var interiors = geometries[i].interiors;

      for (var p = 0; p < exterior.length; p++) {
        vec2.applyTransform(exterior[p], exterior[p], transform);
      }

      for (var h = 0; h < (interiors ? interiors.length : 0); h++) {
        for (var p = 0; p < interiors[h].length; p++) {
          vec2.applyTransform(interiors[h][p], interiors[h][p], transform);
        }
      }
    }

    rect = this._rect;
    rect.copy(target); // Update center

    this.center = [rect.x + rect.width / 2, rect.y + rect.height / 2];
  },
  cloneShallow: function cloneShallow(name) {
    name == null && (name = this.name);
    var newRegion = new Region(name, this.geometries, this.center);
    newRegion._rect = this._rect;
    newRegion.transformTo = null; // Simply avoid to be called.

    return newRegion;
  }
};
var _default = Region;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvZ2VvL1JlZ2lvbi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2Nvb3JkL2dlby9SZWdpb24uanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBCb3VuZGluZ1JlY3QgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS9Cb3VuZGluZ1JlY3RcIik7XG5cbnZhciBiYm94ID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvYmJveFwiKTtcblxudmFyIHZlYzIgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS92ZWN0b3JcIik7XG5cbnZhciBwb2x5Z29uQ29udGFpbiA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb250YWluL3BvbHlnb25cIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBAbW9kdWxlIGVjaGFydHMvY29vcmQvZ2VvL1JlZ2lvblxuICovXG5cbi8qKlxuICogQHBhcmFtIHtzdHJpbmd8UmVnaW9ufSBuYW1lXG4gKiBAcGFyYW0ge0FycmF5fSBnZW9tZXRyaWVzXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBjcFxuICovXG5mdW5jdGlvbiBSZWdpb24obmFtZSwgZ2VvbWV0cmllcywgY3ApIHtcbiAgLyoqXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqIEByZWFkT25seVxuICAgKi9cbiAgdGhpcy5uYW1lID0gbmFtZTtcbiAgLyoqXG4gICAqIEB0eXBlIHtBcnJheS48QXJyYXk+fVxuICAgKiBAcmVhZE9ubHlcbiAgICovXG5cbiAgdGhpcy5nZW9tZXRyaWVzID0gZ2VvbWV0cmllcztcblxuICBpZiAoIWNwKSB7XG4gICAgdmFyIHJlY3QgPSB0aGlzLmdldEJvdW5kaW5nUmVjdCgpO1xuICAgIGNwID0gW3JlY3QueCArIHJlY3Qud2lkdGggLyAyLCByZWN0LnkgKyByZWN0LmhlaWdodCAvIDJdO1xuICB9IGVsc2Uge1xuICAgIGNwID0gW2NwWzBdLCBjcFsxXV07XG4gIH1cbiAgLyoqXG4gICAqIEB0eXBlIHtBcnJheS48bnVtYmVyPn1cbiAgICovXG5cblxuICB0aGlzLmNlbnRlciA9IGNwO1xufVxuXG5SZWdpb24ucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogUmVnaW9uLFxuICBwcm9wZXJ0aWVzOiBudWxsLFxuXG4gIC8qKlxuICAgKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9jb3JlL0JvdW5kaW5nUmVjdH1cbiAgICovXG4gIGdldEJvdW5kaW5nUmVjdDogZnVuY3Rpb24gKCkge1xuICAgIHZhciByZWN0ID0gdGhpcy5fcmVjdDtcblxuICAgIGlmIChyZWN0KSB7XG4gICAgICByZXR1cm4gcmVjdDtcbiAgICB9XG5cbiAgICB2YXIgTUFYX05VTUJFUiA9IE51bWJlci5NQVhfVkFMVUU7XG4gICAgdmFyIG1pbiA9IFtNQVhfTlVNQkVSLCBNQVhfTlVNQkVSXTtcbiAgICB2YXIgbWF4ID0gWy1NQVhfTlVNQkVSLCAtTUFYX05VTUJFUl07XG4gICAgdmFyIG1pbjIgPSBbXTtcbiAgICB2YXIgbWF4MiA9IFtdO1xuICAgIHZhciBnZW9tZXRyaWVzID0gdGhpcy5nZW9tZXRyaWVzO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBnZW9tZXRyaWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAvLyBPbmx5IHN1cHBvcnQgcG9seWdvblxuICAgICAgaWYgKGdlb21ldHJpZXNbaV0udHlwZSAhPT0gJ3BvbHlnb24nKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfSAvLyBEb2Vzbid0IGNvbnNpZGVyIGhvbGVcblxuXG4gICAgICB2YXIgZXh0ZXJpb3IgPSBnZW9tZXRyaWVzW2ldLmV4dGVyaW9yO1xuICAgICAgYmJveC5mcm9tUG9pbnRzKGV4dGVyaW9yLCBtaW4yLCBtYXgyKTtcbiAgICAgIHZlYzIubWluKG1pbiwgbWluLCBtaW4yKTtcbiAgICAgIHZlYzIubWF4KG1heCwgbWF4LCBtYXgyKTtcbiAgICB9IC8vIE5vIGRhdGFcblxuXG4gICAgaWYgKGkgPT09IDApIHtcbiAgICAgIG1pblswXSA9IG1pblsxXSA9IG1heFswXSA9IG1heFsxXSA9IDA7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuX3JlY3QgPSBuZXcgQm91bmRpbmdSZWN0KG1pblswXSwgbWluWzFdLCBtYXhbMF0gLSBtaW5bMF0sIG1heFsxXSAtIG1pblsxXSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7PEFycmF5LjxudW1iZXI+fSBjb29yZFxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgY29udGFpbjogZnVuY3Rpb24gKGNvb3JkKSB7XG4gICAgdmFyIHJlY3QgPSB0aGlzLmdldEJvdW5kaW5nUmVjdCgpO1xuICAgIHZhciBnZW9tZXRyaWVzID0gdGhpcy5nZW9tZXRyaWVzO1xuXG4gICAgaWYgKCFyZWN0LmNvbnRhaW4oY29vcmRbMF0sIGNvb3JkWzFdKSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGxvb3BHZW86IGZvciAodmFyIGkgPSAwLCBsZW4gPSBnZW9tZXRyaWVzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAvLyBPbmx5IHN1cHBvcnQgcG9seWdvbi5cbiAgICAgIGlmIChnZW9tZXRyaWVzW2ldLnR5cGUgIT09ICdwb2x5Z29uJykge1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgdmFyIGV4dGVyaW9yID0gZ2VvbWV0cmllc1tpXS5leHRlcmlvcjtcbiAgICAgIHZhciBpbnRlcmlvcnMgPSBnZW9tZXRyaWVzW2ldLmludGVyaW9ycztcblxuICAgICAgaWYgKHBvbHlnb25Db250YWluLmNvbnRhaW4oZXh0ZXJpb3IsIGNvb3JkWzBdLCBjb29yZFsxXSkpIHtcbiAgICAgICAgLy8gTm90IGluIHRoZSByZWdpb24gaWYgcG9pbnQgaXMgaW4gdGhlIGhvbGUuXG4gICAgICAgIGZvciAodmFyIGsgPSAwOyBrIDwgKGludGVyaW9ycyA/IGludGVyaW9ycy5sZW5ndGggOiAwKTsgaysrKSB7XG4gICAgICAgICAgaWYgKHBvbHlnb25Db250YWluLmNvbnRhaW4oaW50ZXJpb3JzW2tdKSkge1xuICAgICAgICAgICAgY29udGludWUgbG9vcEdlbztcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gZmFsc2U7XG4gIH0sXG4gIHRyYW5zZm9ybVRvOiBmdW5jdGlvbiAoeCwgeSwgd2lkdGgsIGhlaWdodCkge1xuICAgIHZhciByZWN0ID0gdGhpcy5nZXRCb3VuZGluZ1JlY3QoKTtcbiAgICB2YXIgYXNwZWN0ID0gcmVjdC53aWR0aCAvIHJlY3QuaGVpZ2h0O1xuXG4gICAgaWYgKCF3aWR0aCkge1xuICAgICAgd2lkdGggPSBhc3BlY3QgKiBoZWlnaHQ7XG4gICAgfSBlbHNlIGlmICghaGVpZ2h0KSB7XG4gICAgICBoZWlnaHQgPSB3aWR0aCAvIGFzcGVjdDtcbiAgICB9XG5cbiAgICB2YXIgdGFyZ2V0ID0gbmV3IEJvdW5kaW5nUmVjdCh4LCB5LCB3aWR0aCwgaGVpZ2h0KTtcbiAgICB2YXIgdHJhbnNmb3JtID0gcmVjdC5jYWxjdWxhdGVUcmFuc2Zvcm0odGFyZ2V0KTtcbiAgICB2YXIgZ2VvbWV0cmllcyA9IHRoaXMuZ2VvbWV0cmllcztcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZ2VvbWV0cmllcy5sZW5ndGg7IGkrKykge1xuICAgICAgLy8gT25seSBzdXBwb3J0IHBvbHlnb24uXG4gICAgICBpZiAoZ2VvbWV0cmllc1tpXS50eXBlICE9PSAncG9seWdvbicpIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIHZhciBleHRlcmlvciA9IGdlb21ldHJpZXNbaV0uZXh0ZXJpb3I7XG4gICAgICB2YXIgaW50ZXJpb3JzID0gZ2VvbWV0cmllc1tpXS5pbnRlcmlvcnM7XG5cbiAgICAgIGZvciAodmFyIHAgPSAwOyBwIDwgZXh0ZXJpb3IubGVuZ3RoOyBwKyspIHtcbiAgICAgICAgdmVjMi5hcHBseVRyYW5zZm9ybShleHRlcmlvcltwXSwgZXh0ZXJpb3JbcF0sIHRyYW5zZm9ybSk7XG4gICAgICB9XG5cbiAgICAgIGZvciAodmFyIGggPSAwOyBoIDwgKGludGVyaW9ycyA/IGludGVyaW9ycy5sZW5ndGggOiAwKTsgaCsrKSB7XG4gICAgICAgIGZvciAodmFyIHAgPSAwOyBwIDwgaW50ZXJpb3JzW2hdLmxlbmd0aDsgcCsrKSB7XG4gICAgICAgICAgdmVjMi5hcHBseVRyYW5zZm9ybShpbnRlcmlvcnNbaF1bcF0sIGludGVyaW9yc1toXVtwXSwgdHJhbnNmb3JtKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJlY3QgPSB0aGlzLl9yZWN0O1xuICAgIHJlY3QuY29weSh0YXJnZXQpOyAvLyBVcGRhdGUgY2VudGVyXG5cbiAgICB0aGlzLmNlbnRlciA9IFtyZWN0LnggKyByZWN0LndpZHRoIC8gMiwgcmVjdC55ICsgcmVjdC5oZWlnaHQgLyAyXTtcbiAgfSxcbiAgY2xvbmVTaGFsbG93OiBmdW5jdGlvbiAobmFtZSkge1xuICAgIG5hbWUgPT0gbnVsbCAmJiAobmFtZSA9IHRoaXMubmFtZSk7XG4gICAgdmFyIG5ld1JlZ2lvbiA9IG5ldyBSZWdpb24obmFtZSwgdGhpcy5nZW9tZXRyaWVzLCB0aGlzLmNlbnRlcik7XG4gICAgbmV3UmVnaW9uLl9yZWN0ID0gdGhpcy5fcmVjdDtcbiAgICBuZXdSZWdpb24udHJhbnNmb3JtVG8gPSBudWxsOyAvLyBTaW1wbHkgYXZvaWQgdG8gYmUgY2FsbGVkLlxuXG4gICAgcmV0dXJuIG5ld1JlZ2lvbjtcbiAgfVxufTtcbnZhciBfZGVmYXVsdCA9IFJlZ2lvbjtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7QUFJQTs7Ozs7OztBQUtBO0FBQ0E7Ozs7QUFJQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUEzSEE7QUE2SEE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/geo/Region.js
