__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Dialog */ "./node_modules/rc-dialog/es/Dialog.js");
/* harmony import */ var rc_util_es_PortalWrapper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-util/es/PortalWrapper */ "./node_modules/rc-util/es/PortalWrapper.js");



 // fix issue #10656

/*
* getContainer remarks
* Custom container should not be return, because in the Portal component, it will remove the
* return container element here, if the custom container is the only child of it's component,
* like issue #10656, It will has a conflict with removeChild method in react-dom.
* So here should add a child (div element) to custom container.
* */

/* harmony default export */ __webpack_exports__["default"] = (function (props) {
  var visible = props.visible,
      getContainer = props.getContainer,
      forceRender = props.forceRender; // 渲染在当前 dom 里；

  if (getContainer === false) {
    return react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_Dialog__WEBPACK_IMPORTED_MODULE_2__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props, {
      getOpenCount: function getOpenCount() {
        return 2;
      }
    }));
  }

  return react__WEBPACK_IMPORTED_MODULE_1__["createElement"](rc_util_es_PortalWrapper__WEBPACK_IMPORTED_MODULE_3__["default"], {
    visible: visible,
    forceRender: forceRender,
    getContainer: getContainer
  }, function (childProps) {
    return react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_Dialog__WEBPACK_IMPORTED_MODULE_2__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props, childProps));
  });
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZGlhbG9nL2VzL0RpYWxvZ1dyYXAuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1kaWFsb2cvZXMvRGlhbG9nV3JhcC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2V4dGVuZHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnO1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IERpYWxvZyBmcm9tICcuL0RpYWxvZyc7XG5pbXBvcnQgUG9ydGFsIGZyb20gJ3JjLXV0aWwvZXMvUG9ydGFsV3JhcHBlcic7XG4vLyBmaXggaXNzdWUgIzEwNjU2XG4vKlxuKiBnZXRDb250YWluZXIgcmVtYXJrc1xuKiBDdXN0b20gY29udGFpbmVyIHNob3VsZCBub3QgYmUgcmV0dXJuLCBiZWNhdXNlIGluIHRoZSBQb3J0YWwgY29tcG9uZW50LCBpdCB3aWxsIHJlbW92ZSB0aGVcbiogcmV0dXJuIGNvbnRhaW5lciBlbGVtZW50IGhlcmUsIGlmIHRoZSBjdXN0b20gY29udGFpbmVyIGlzIHRoZSBvbmx5IGNoaWxkIG9mIGl0J3MgY29tcG9uZW50LFxuKiBsaWtlIGlzc3VlICMxMDY1NiwgSXQgd2lsbCBoYXMgYSBjb25mbGljdCB3aXRoIHJlbW92ZUNoaWxkIG1ldGhvZCBpbiByZWFjdC1kb20uXG4qIFNvIGhlcmUgc2hvdWxkIGFkZCBhIGNoaWxkIChkaXYgZWxlbWVudCkgdG8gY3VzdG9tIGNvbnRhaW5lci5cbiogKi9cbmV4cG9ydCBkZWZhdWx0IChmdW5jdGlvbiAocHJvcHMpIHtcbiAgICB2YXIgdmlzaWJsZSA9IHByb3BzLnZpc2libGUsXG4gICAgICAgIGdldENvbnRhaW5lciA9IHByb3BzLmdldENvbnRhaW5lcixcbiAgICAgICAgZm9yY2VSZW5kZXIgPSBwcm9wcy5mb3JjZVJlbmRlcjtcbiAgICAvLyDmuLLmn5PlnKjlvZPliY0gZG9tIOmHjO+8m1xuXG4gICAgaWYgKGdldENvbnRhaW5lciA9PT0gZmFsc2UpIHtcbiAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRGlhbG9nLCBfZXh0ZW5kcyh7fSwgcHJvcHMsIHsgZ2V0T3BlbkNvdW50OiBmdW5jdGlvbiBnZXRPcGVuQ291bnQoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIDI7XG4gICAgICAgICAgICB9IH0pKTtcbiAgICB9XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoUG9ydGFsLCB7IHZpc2libGU6IHZpc2libGUsIGZvcmNlUmVuZGVyOiBmb3JjZVJlbmRlciwgZ2V0Q29udGFpbmVyOiBnZXRDb250YWluZXIgfSwgZnVuY3Rpb24gKGNoaWxkUHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRGlhbG9nLCBfZXh0ZW5kcyh7fSwgcHJvcHMsIGNoaWxkUHJvcHMpKTtcbiAgICB9KTtcbn0pOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBT0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-dialog/es/DialogWrap.js
