__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddLocation", function() { return AddLocation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddLocationEndo", function() { return AddLocationEndo; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _desk_quote_SAIC_iar_basic_add_location__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @desk/quote/SAIC/iar/basic-add-location */ "./src/app/desk/quote/SAIC/iar/basic-add-location.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @desk/quote/SAIC/all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_coverage_entry__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk/quote/SAIC/iar/coverage-entry */ "./src/app/desk/quote/SAIC/iar/coverage-entry.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_component_side_info__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk/quote/SAIC/iar/component/side-info */ "./src/app/desk/quote/SAIC/iar/component/side-info.tsx");
/* harmony import */ var _desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk/quote/SAIC/phs/components/side-info */ "./src/app/desk/quote/SAIC/phs/components/side-info.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk/quote/SAIC/iar/index */ "./src/app/desk/quote/SAIC/iar/index.tsx");
/* harmony import */ var _desk_component_view_item__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @desk-component/view-item */ "./src/app/desk/component/view-item/index.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/add-Location.tsx";














var AddLocation =
/*#__PURE__*/
function (_BasicAddLocation) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(AddLocation, _BasicAddLocation);

  function AddLocation() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, AddLocation);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(AddLocation).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(AddLocation, [{
    key: "handleNext",
    value: function handleNext() {
      var _this = this;

      this.props.form.validateFields(function (err, fieldsValue) {
        if (err) {
          return;
        }

        var coverages = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_this.props.model, "member.ext.coverages", []) || [];
        var newCoverages = coverages.map(function (item, i) {
          return lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_this.refs["coverage".concat(i)], "state.model", {}) || {};
        }); // const coverages = Object.keys(this.oldCoverages).map((item: any, index: number) => {
        //   return this.oldCoverages[item];
        // });

        lodash__WEBPACK_IMPORTED_MODULE_7___default.a.set(_this.props.model, "member.ext.coverages", newCoverages);

        var _this$state = _this.state,
            action = _this$state.action,
            index = _this$state.index;
        var locations = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_this.props.model, "policy.ext.locations", []) || [];

        var postalCode = _this.getValueFromModel("member.ext.location.postalCode");

        if (action === "new") {
          var repeatArr = locations.filter(function (item) {
            return lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(item, "location.postalCode", "") === postalCode;
          });

          if (!lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(repeatArr)) {
            antd__WEBPACK_IMPORTED_MODULE_8__["notification"].error({
              message: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Please enter a unique postal code").thai("Please enter a unique postal code").my("Please enter a unique postal code").getMessage()
            });
            return;
          }
        } else {
          var _repeatArr = locations.filter(function (item) {
            return lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(item, "location.postalCode", "") === postalCode;
          });

          if (!lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(_repeatArr)) {
            if (lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_repeatArr, "0.location.postalCode") !== lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(locations, "".concat(index, ".location.postalCode"))) {
              antd__WEBPACK_IMPORTED_MODULE_8__["notification"].error({
                message: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Please enter a unique postal code").thai("Please enter a unique postal code").my("Please enter a unique postal code").getMessage()
              });
              return;
            }
          }
        }

        var newLocation = _this.getValueFromModel("member.ext");

        if (action === "edit") {
          locations[index] = newLocation;
        } else {
          locations.push(newLocation);
        }

        _this.setValueToModel(locations, "policy.ext.locations"); // this.setValueToModel("", "policy.ext.feeSummary.adjustedPremium");


        var newCart = _this.props.mergePolicyToServiceModel();

        _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_9__["Apis"].CART_MERGE, newCart, {
          loading: true
        }).then(function (res) {
          var cart = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(res, "body.respData", {});

          _this.props.mergePolicyToUIModel(cart);

          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_10__["pageTo"])(_this, _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_11__["AllSteps"].LOCATION);
        });
      });
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      if (dataIdPrefix) return "".concat(dataIdPrefix, ".").concat(propName);
      return "".concat(propName);
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      if (lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(propName)) {
        return "policy";
      }

      return "policy.".concat(propName);
    }
  }, {
    key: "getProps",
    value: function getProps(propName, dataFixed) {
      if (dataFixed) {
        return "".concat(dataFixed, ".").concat(propName);
      } else {
        return propName;
      }
    }
  }, {
    key: "renderAddress",
    value: function renderAddress() {
      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_desk_component__WEBPACK_IMPORTED_MODULE_12__["CommonAddress"], {
        model: model,
        form: form,
        addressFix: "location",
        dataIdOrPrefix: "member.ext",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      });
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      var model = this.props.model;
      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_15__["default"], {
        lineList: _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_16__["list"],
        premiumId: "cartPremium",
        model: model,
        dataFixed: "policy",
        activeStep: "Locations",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        __self: this
      });
    }
  }, {
    key: "renderCoverage",
    value: function renderCoverage() {
      var _this$props2 = this.props,
          form = _this$props2.form,
          model = _this$props2.model;

      var premiumCurrency = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "policy.premCurrencyCode", "");

      var siCurrency = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "policy.siCurrencyCode", "");

      var coverages = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "member.ext.coverages", []) || [];

      var effDate = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "policy.effDate", "");

      var expDate = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "policy.expDate", "");

      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_5___default.a.Fragment, null, coverages.map(function (item, i) {
        return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_desk_quote_SAIC_iar_coverage_entry__WEBPACK_IMPORTED_MODULE_13__["default"], {
          form: form,
          key: i,
          ref: "coverage".concat(i),
          siCurrency: siCurrency,
          premiumCurrency: premiumCurrency,
          effDate: effDate,
          expDate: expDate,
          model: model,
          dataIndex: i,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 125
          },
          __self: this
        });
      }));
    }
  }]);

  return AddLocation;
}(_desk_quote_SAIC_iar_basic_add_location__WEBPACK_IMPORTED_MODULE_6__["BasicAddLocation"]);



var AddLocationEndo =
/*#__PURE__*/
function (_BasicAddLocation2) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(AddLocationEndo, _BasicAddLocation2);

  function AddLocationEndo() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, AddLocationEndo);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(AddLocationEndo).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(AddLocationEndo, [{
    key: "handleNext",
    value: function handleNext() {
      var _this2 = this;

      this.props.form.validateFields(function (err, fieldsValue) {
        if (err) {
          return;
        }

        var coverages = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_this2.props.model, "member.ext.coverages", []) || [];
        var newCoverages = coverages.map(function (item, i) {
          return lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_this2.refs["coverage".concat(i)], "state.model", {}) || {};
        });

        lodash__WEBPACK_IMPORTED_MODULE_7___default.a.set(_this2.props.model, "member.ext.coverages", newCoverages);

        var _this2$state = _this2.state,
            action = _this2$state.action,
            index = _this2$state.index;
        var locations = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_this2.props.model, "ext.changes.locations", []) || [];

        var postalCode = _this2.getValueFromModel("member.ext.location.postalCode");

        if (action === "new") {
          var repeatArr = locations.filter(function (item) {
            return lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(item, "location.postalCode", "") === postalCode;
          });

          if (!lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(repeatArr)) {
            antd__WEBPACK_IMPORTED_MODULE_8__["notification"].error({
              message: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Please enter a unique postalCode").thai("Please enter a unique postalCode").my("Please enter a unique postalCode").getMessage()
            });
            return;
          }
        } else {
          var _repeatArr2 = locations.filter(function (item) {
            return lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(item, "location.postalCode", "") === postalCode;
          });

          if (!lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(_repeatArr2)) {
            if (lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_repeatArr2, "0.location.postalCode") !== lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(locations, "".concat(index, ".location.postalCode"))) {
              antd__WEBPACK_IMPORTED_MODULE_8__["notification"].error({
                message: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Please enter a unique postalCode").thai("Please enter a unique postalCode").my("Please enter a unique postalCode").getMessage()
              });
              return;
            }
          }
        }

        var newLocation = _this2.getValueFromModel("member.ext");

        if (action === "edit") {
          locations[index] = newLocation;
        } else {
          locations.push(newLocation);
        }

        _this2.setValueToModel(locations, "ext.changes.locations"); // this.setValueToModel("", "ext.changes.feeSummary.adjustedPremium");


        var newEndo = _this2.props.mergePolicyToServiceModel();

        _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_9__["Apis"].ENDO_MERGE, newEndo, {
          loading: true
        }).then(function (res) {
          var endo = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(res, "body.respData.endo", {});

          _this2.props.mergePolicyToUIModel(endo);

          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_10__["pageTo"])(_this2, _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_11__["AllSteps"].LOCATION);
        });
      });
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      if (dataIdPrefix) return "".concat(dataIdPrefix, ".").concat(propName);
      return "".concat(propName);
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      if (lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(propName)) {
        return "policy";
      }

      return "policy.".concat(propName);
    }
  }, {
    key: "getProps",
    value: function getProps(propName, dataFixed) {
      return propName;
    }
  }, {
    key: "getValue",
    value: function getValue(propName) {
      return this.getValueFromModel("member.ext.".concat(propName));
    }
  }, {
    key: "renderAddress",
    value: function renderAddress() {
      var _this$props3 = this.props,
          form = _this$props3.form,
          model = _this$props3.model;
      var action = this.state.action;
      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_5___default.a.Fragment, null, action === "edit" ? react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_5___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_17__["default"], {
        title: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Country").thai("Country").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 228
        },
        __self: this
      }, this.getMasterTableItemText("nationality", this.getValue("location.countryCode"))), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_17__["default"], {
        title: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Address").thai("Address").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 233
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_desk_component__WEBPACK_IMPORTED_MODULE_12__["ShowAddress"], {
        model: model,
        addressFixed: "member.ext.location",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 236
        },
        __self: this
      }))) : react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_desk_component__WEBPACK_IMPORTED_MODULE_12__["CommonAddress"], {
        model: model,
        form: form,
        addressFix: "location",
        dataIdOrPrefix: "member.ext",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 240
        },
        __self: this
      }));
    }
  }, {
    key: "renderCoverage",
    value: function renderCoverage() {
      var _this$props4 = this.props,
          form = _this$props4.form,
          model = _this$props4.model;

      var premiumCurrency = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "ext.changes.premiumCurrency", "");

      var siCurrency = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "ext.changes.siCurrency", "");

      var coverages = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "member.ext.coverages", []) || [];

      var effDate = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "ext.changes.effDate", "");

      var expDate = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "ext.changes.expDate", "");

      var breakDown = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "oldPolicyPremium.wrappedPrems.breakdown", []) || [];
      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_5___default.a.Fragment, null, coverages.map(function (item, i) {
        return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_desk_quote_SAIC_iar_coverage_entry__WEBPACK_IMPORTED_MODULE_13__["default"], {
          form: form,
          key: i,
          effDate: effDate,
          expDate: expDate // wrappedComponentRef={(form: any) => {
          //   if (form) {
          //     this.oldCoverages[`coverage${i}`] = form.state.model;
          //   }
          // }}
          ,
          isEndo: true,
          breakDown: breakDown,
          ref: "coverage".concat(i),
          siCurrency: siCurrency,
          premiumCurrency: premiumCurrency,
          model: model,
          dataIndex: i,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 259
          },
          __self: this
        });
      }));
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      var model = this.props.model;
      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_desk_quote_SAIC_iar_component_side_info__WEBPACK_IMPORTED_MODULE_14__["default"], {
        lineList: _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_16__["list"],
        model: model,
        premiumId: "deltaPolicyPremium",
        endoFixed: "ext.changes",
        activeStep: "Locations",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 282
        },
        __self: this
      });
    }
  }]);

  return AddLocationEndo;
}(_desk_quote_SAIC_iar_basic_add_location__WEBPACK_IMPORTED_MODULE_6__["BasicAddLocation"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYWRkLUxvY2F0aW9uLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL1NBSUMvaWFyL2FkZC1Mb2NhdGlvbi50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgQmFzaWNBZGRMb2NhdGlvbiB9IGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL2lhci9iYXNpYy1hZGQtbG9jYXRpb25cIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IG5vdGlmaWNhdGlvbiwgUm93IH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IEFqYXgsIEFwaXMsIExhbmd1YWdlIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IHBhZ2VUbyB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvd2l6YXJkXCI7XG5pbXBvcnQgeyBBbGxTdGVwcyB9IGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL2FsbC1zdGVwc1wiO1xuaW1wb3J0IHsgQ29tbW9uQWRkcmVzcywgU2hvd0FkZHJlc3MgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50XCI7XG5pbXBvcnQgQ292ZXJhZ2VFbnRyeSBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9pYXIvY292ZXJhZ2UtZW50cnlcIjtcbmltcG9ydCBTaWRlSW5mbyBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3NpZGUtaW5mb1wiO1xuaW1wb3J0IFNpZGVJbmZvUGhzIGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL3Bocy9jb21wb25lbnRzL3NpZGUtaW5mb1wiO1xuaW1wb3J0IHsgbGlzdCB9IGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL2lhci9pbmRleFwiO1xuaW1wb3J0IFZpZXdJdGVtIGZyb20gXCJAZGVzay1jb21wb25lbnQvdmlldy1pdGVtXCI7XG5cbmNsYXNzIEFkZExvY2F0aW9uIGV4dGVuZHMgQmFzaWNBZGRMb2NhdGlvbjxhbnksIGFueSwgYW55PiB7XG4gIGhhbmRsZU5leHQoKSB7XG4gICAgdGhpcy5wcm9wcy5mb3JtLnZhbGlkYXRlRmllbGRzKChlcnI6IGFueSwgZmllbGRzVmFsdWU6IGFueSkgPT4ge1xuICAgICAgaWYgKGVycikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBjb25zdCBjb3ZlcmFnZXMgPSBfLmdldCh0aGlzLnByb3BzLm1vZGVsLCBcIm1lbWJlci5leHQuY292ZXJhZ2VzXCIsIFtdKSB8fCBbXTtcbiAgICAgIGNvbnN0IG5ld0NvdmVyYWdlcyA9IGNvdmVyYWdlcy5tYXAoKGl0ZW06IGFueSwgaTogYW55KSA9PiB7XG4gICAgICAgIHJldHVybiBfLmdldCh0aGlzLnJlZnNbYGNvdmVyYWdlJHtpfWBdLCBcInN0YXRlLm1vZGVsXCIsIHt9KSB8fCB7fTtcbiAgICAgIH0pO1xuICAgICAgLy8gY29uc3QgY292ZXJhZ2VzID0gT2JqZWN0LmtleXModGhpcy5vbGRDb3ZlcmFnZXMpLm1hcCgoaXRlbTogYW55LCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICAvLyAgIHJldHVybiB0aGlzLm9sZENvdmVyYWdlc1tpdGVtXTtcbiAgICAgIC8vIH0pO1xuICAgICAgXy5zZXQodGhpcy5wcm9wcy5tb2RlbCwgXCJtZW1iZXIuZXh0LmNvdmVyYWdlc1wiLCBuZXdDb3ZlcmFnZXMpO1xuICAgICAgY29uc3QgeyBhY3Rpb24sIGluZGV4IH0gPSB0aGlzLnN0YXRlO1xuICAgICAgY29uc3QgbG9jYXRpb25zID0gXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgXCJwb2xpY3kuZXh0LmxvY2F0aW9uc1wiLCBbXSkgfHwgW107XG4gICAgICBjb25zdCBwb3N0YWxDb2RlID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcIm1lbWJlci5leHQubG9jYXRpb24ucG9zdGFsQ29kZVwiKTtcbiAgICAgIGlmIChhY3Rpb24gPT09IFwibmV3XCIpIHtcbiAgICAgICAgY29uc3QgcmVwZWF0QXJyOiBhbnkgPSBsb2NhdGlvbnMuZmlsdGVyKChpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICByZXR1cm4gXy5nZXQoaXRlbSwgXCJsb2NhdGlvbi5wb3N0YWxDb2RlXCIsIFwiXCIpID09PSBwb3N0YWxDb2RlO1xuICAgICAgICB9KTtcbiAgICAgICAgaWYgKCFfLmlzRW1wdHkocmVwZWF0QXJyKSkge1xuICAgICAgICAgIG5vdGlmaWNhdGlvbi5lcnJvcih7XG4gICAgICAgICAgICBtZXNzYWdlOiBMYW5ndWFnZS5lbihcIlBsZWFzZSBlbnRlciBhIHVuaXF1ZSBwb3N0YWwgY29kZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIlBsZWFzZSBlbnRlciBhIHVuaXF1ZSBwb3N0YWwgY29kZVwiKVxuICAgICAgICAgICAgICAubXkoXCJQbGVhc2UgZW50ZXIgYSB1bmlxdWUgcG9zdGFsIGNvZGVcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnN0IHJlcGVhdEFycjogYW55ID0gbG9jYXRpb25zLmZpbHRlcigoaXRlbTogYW55KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIF8uZ2V0KGl0ZW0sIFwibG9jYXRpb24ucG9zdGFsQ29kZVwiLCBcIlwiKSA9PT0gcG9zdGFsQ29kZTtcbiAgICAgICAgfSk7XG4gICAgICAgIGlmICghXy5pc0VtcHR5KHJlcGVhdEFycikpIHtcbiAgICAgICAgICBpZiAoXy5nZXQocmVwZWF0QXJyLCBcIjAubG9jYXRpb24ucG9zdGFsQ29kZVwiKSAhPT0gXy5nZXQobG9jYXRpb25zLCBgJHtpbmRleH0ubG9jYXRpb24ucG9zdGFsQ29kZWApKSB7XG4gICAgICAgICAgICBub3RpZmljYXRpb24uZXJyb3Ioe1xuICAgICAgICAgICAgICBtZXNzYWdlOiBMYW5ndWFnZS5lbihcIlBsZWFzZSBlbnRlciBhIHVuaXF1ZSBwb3N0YWwgY29kZVwiKVxuICAgICAgICAgICAgICAgIC50aGFpKFwiUGxlYXNlIGVudGVyIGEgdW5pcXVlIHBvc3RhbCBjb2RlXCIpXG4gICAgICAgICAgICAgICAgLm15KFwiUGxlYXNlIGVudGVyIGEgdW5pcXVlIHBvc3RhbCBjb2RlXCIpXG4gICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgY29uc3QgbmV3TG9jYXRpb24gPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwibWVtYmVyLmV4dFwiKTtcbiAgICAgIGlmIChhY3Rpb24gPT09IFwiZWRpdFwiKSB7XG4gICAgICAgIGxvY2F0aW9uc1tpbmRleF0gPSBuZXdMb2NhdGlvbjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGxvY2F0aW9ucy5wdXNoKG5ld0xvY2F0aW9uKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKGxvY2F0aW9ucywgXCJwb2xpY3kuZXh0LmxvY2F0aW9uc1wiKTtcbiAgICAgIC8vIHRoaXMuc2V0VmFsdWVUb01vZGVsKFwiXCIsIFwicG9saWN5LmV4dC5mZWVTdW1tYXJ5LmFkanVzdGVkUHJlbWl1bVwiKTtcbiAgICAgIGNvbnN0IG5ld0NhcnQgPSB0aGlzLnByb3BzLm1lcmdlUG9saWN5VG9TZXJ2aWNlTW9kZWwoKTtcbiAgICAgIEFqYXgucG9zdChBcGlzLkNBUlRfTUVSR0UsIG5ld0NhcnQsIHsgbG9hZGluZzogdHJ1ZSB9KS50aGVuKChyZXMpID0+IHtcbiAgICAgICAgY29uc3QgY2FydCA9IF8uZ2V0KHJlcywgXCJib2R5LnJlc3BEYXRhXCIsIHt9KTtcbiAgICAgICAgdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvVUlNb2RlbChjYXJ0KTtcbiAgICAgICAgcGFnZVRvKHRoaXMsIEFsbFN0ZXBzLkxPQ0FUSU9OKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZTogc3RyaW5nLCBkYXRhSWRQcmVmaXg/OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIGlmIChkYXRhSWRQcmVmaXgpIHJldHVybiBgJHtkYXRhSWRQcmVmaXh9LiR7cHJvcE5hbWV9YDtcbiAgICByZXR1cm4gYCR7cHJvcE5hbWV9YDtcbiAgfVxuXG4gIGdldFByb3AocHJvcE5hbWU/OiBzdHJpbmcpIHtcbiAgICBpZiAoXy5pc0VtcHR5KHByb3BOYW1lKSkge1xuICAgICAgcmV0dXJuIFwicG9saWN5XCI7XG4gICAgfVxuICAgIHJldHVybiBgcG9saWN5LiR7cHJvcE5hbWV9YDtcbiAgfVxuXG4gIGdldFByb3BzKHByb3BOYW1lOiBhbnksIGRhdGFGaXhlZD86IGFueSkge1xuICAgIGlmIChkYXRhRml4ZWQpIHtcbiAgICAgIHJldHVybiBgJHtkYXRhRml4ZWR9LiR7cHJvcE5hbWV9YDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHByb3BOYW1lO1xuICAgIH1cbiAgfVxuXG4gIHJlbmRlckFkZHJlc3MoKSB7XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPENvbW1vbkFkZHJlc3MgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgICAgICBhZGRyZXNzRml4PXtcImxvY2F0aW9uXCJ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFJZE9yUHJlZml4PXtcIm1lbWJlci5leHRcIn1cbiAgICAvPjtcbiAgfVxuXG4gIHJlbmRlclNpZGVJbmZvKCk6IGFueSB7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPFNpZGVJbmZvUGhzXG4gICAgICBsaW5lTGlzdD17bGlzdH1cbiAgICAgIHByZW1pdW1JZD17XCJjYXJ0UHJlbWl1bVwifVxuICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgZGF0YUZpeGVkPXtcInBvbGljeVwifVxuICAgICAgYWN0aXZlU3RlcD17XCJMb2NhdGlvbnNcIn0vPjtcbiAgfVxuXG4gIHJlbmRlckNvdmVyYWdlKCk6IGFueSB7XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBwcmVtaXVtQ3VycmVuY3kgPSBfLmdldChtb2RlbCwgXCJwb2xpY3kucHJlbUN1cnJlbmN5Q29kZVwiLCBcIlwiKTtcbiAgICBjb25zdCBzaUN1cnJlbmN5ID0gXy5nZXQobW9kZWwsIFwicG9saWN5LnNpQ3VycmVuY3lDb2RlXCIsIFwiXCIpO1xuICAgIGNvbnN0IGNvdmVyYWdlcyA9IF8uZ2V0KG1vZGVsLCBcIm1lbWJlci5leHQuY292ZXJhZ2VzXCIsIFtdKSB8fCBbXTtcbiAgICBjb25zdCBlZmZEYXRlID0gXy5nZXQobW9kZWwsIFwicG9saWN5LmVmZkRhdGVcIiwgXCJcIik7XG4gICAgY29uc3QgZXhwRGF0ZSA9IF8uZ2V0KG1vZGVsLCBcInBvbGljeS5leHBEYXRlXCIsIFwiXCIpO1xuICAgIHJldHVybiA8Pntjb3ZlcmFnZXMubWFwKChpdGVtOiBhbnksIGk6IG51bWJlcikgPT4ge1xuICAgICAgcmV0dXJuIDxDb3ZlcmFnZUVudHJ5XG4gICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgIGtleT17aX1cbiAgICAgICAgcmVmPXtgY292ZXJhZ2Uke2l9YH1cbiAgICAgICAgc2lDdXJyZW5jeT17c2lDdXJyZW5jeX1cbiAgICAgICAgcHJlbWl1bUN1cnJlbmN5PXtwcmVtaXVtQ3VycmVuY3l9XG4gICAgICAgIGVmZkRhdGU9e2VmZkRhdGV9XG4gICAgICAgIGV4cERhdGU9e2V4cERhdGV9XG4gICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgZGF0YUluZGV4PXtpfS8+O1xuICAgIH0pfVxuICAgIDwvPjtcbiAgfVxufVxuXG5leHBvcnQgeyBBZGRMb2NhdGlvbiB9O1xuXG5jbGFzcyBBZGRMb2NhdGlvbkVuZG8gZXh0ZW5kcyBCYXNpY0FkZExvY2F0aW9uPGFueSwgYW55LCBhbnk+IHtcbiAgaGFuZGxlTmV4dCgpIHtcbiAgICB0aGlzLnByb3BzLmZvcm0udmFsaWRhdGVGaWVsZHMoKGVycjogYW55LCBmaWVsZHNWYWx1ZTogYW55KSA9PiB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGNvbnN0IGNvdmVyYWdlcyA9IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIFwibWVtYmVyLmV4dC5jb3ZlcmFnZXNcIiwgW10pIHx8IFtdO1xuICAgICAgY29uc3QgbmV3Q292ZXJhZ2VzID0gY292ZXJhZ2VzLm1hcCgoaXRlbTogYW55LCBpOiBhbnkpID0+IHtcbiAgICAgICAgcmV0dXJuIF8uZ2V0KHRoaXMucmVmc1tgY292ZXJhZ2Uke2l9YF0sIFwic3RhdGUubW9kZWxcIiwge30pIHx8IHt9O1xuICAgICAgfSk7XG4gICAgICBfLnNldCh0aGlzLnByb3BzLm1vZGVsLCBcIm1lbWJlci5leHQuY292ZXJhZ2VzXCIsIG5ld0NvdmVyYWdlcyk7XG4gICAgICBjb25zdCB7IGFjdGlvbiwgaW5kZXggfSA9IHRoaXMuc3RhdGU7XG4gICAgICBjb25zdCBsb2NhdGlvbnMgPSBfLmdldCh0aGlzLnByb3BzLm1vZGVsLCBcImV4dC5jaGFuZ2VzLmxvY2F0aW9uc1wiLCBbXSkgfHwgW107XG4gICAgICBjb25zdCBwb3N0YWxDb2RlID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcIm1lbWJlci5leHQubG9jYXRpb24ucG9zdGFsQ29kZVwiKTtcbiAgICAgIGlmIChhY3Rpb24gPT09IFwibmV3XCIpIHtcbiAgICAgICAgY29uc3QgcmVwZWF0QXJyOiBhbnkgPSBsb2NhdGlvbnMuZmlsdGVyKChpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICByZXR1cm4gXy5nZXQoaXRlbSwgXCJsb2NhdGlvbi5wb3N0YWxDb2RlXCIsIFwiXCIpID09PSBwb3N0YWxDb2RlO1xuICAgICAgICB9KTtcbiAgICAgICAgaWYgKCFfLmlzRW1wdHkocmVwZWF0QXJyKSkge1xuICAgICAgICAgIG5vdGlmaWNhdGlvbi5lcnJvcih7XG4gICAgICAgICAgICBtZXNzYWdlOiBMYW5ndWFnZS5lbihcIlBsZWFzZSBlbnRlciBhIHVuaXF1ZSBwb3N0YWxDb2RlXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwiUGxlYXNlIGVudGVyIGEgdW5pcXVlIHBvc3RhbENvZGVcIilcbiAgICAgICAgICAgICAgLm15KFwiUGxlYXNlIGVudGVyIGEgdW5pcXVlIHBvc3RhbENvZGVcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnN0IHJlcGVhdEFycjogYW55ID0gbG9jYXRpb25zLmZpbHRlcigoaXRlbTogYW55KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIF8uZ2V0KGl0ZW0sIFwibG9jYXRpb24ucG9zdGFsQ29kZVwiLCBcIlwiKSA9PT0gcG9zdGFsQ29kZTtcbiAgICAgICAgfSk7XG4gICAgICAgIGlmICghXy5pc0VtcHR5KHJlcGVhdEFycikpIHtcbiAgICAgICAgICBpZiAoXy5nZXQocmVwZWF0QXJyLCBcIjAubG9jYXRpb24ucG9zdGFsQ29kZVwiKSAhPT0gXy5nZXQobG9jYXRpb25zLCBgJHtpbmRleH0ubG9jYXRpb24ucG9zdGFsQ29kZWApKSB7XG4gICAgICAgICAgICBub3RpZmljYXRpb24uZXJyb3Ioe1xuICAgICAgICAgICAgICBtZXNzYWdlOiBMYW5ndWFnZS5lbihcIlBsZWFzZSBlbnRlciBhIHVuaXF1ZSBwb3N0YWxDb2RlXCIpXG4gICAgICAgICAgICAgICAgLnRoYWkoXCJQbGVhc2UgZW50ZXIgYSB1bmlxdWUgcG9zdGFsQ29kZVwiKVxuICAgICAgICAgICAgICAgIC5teShcIlBsZWFzZSBlbnRlciBhIHVuaXF1ZSBwb3N0YWxDb2RlXCIpXG4gICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgY29uc3QgbmV3TG9jYXRpb24gPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwibWVtYmVyLmV4dFwiKTtcbiAgICAgIGlmIChhY3Rpb24gPT09IFwiZWRpdFwiKSB7XG4gICAgICAgIGxvY2F0aW9uc1tpbmRleF0gPSBuZXdMb2NhdGlvbjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGxvY2F0aW9ucy5wdXNoKG5ld0xvY2F0aW9uKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKGxvY2F0aW9ucywgXCJleHQuY2hhbmdlcy5sb2NhdGlvbnNcIik7XG4gICAgICAvLyB0aGlzLnNldFZhbHVlVG9Nb2RlbChcIlwiLCBcImV4dC5jaGFuZ2VzLmZlZVN1bW1hcnkuYWRqdXN0ZWRQcmVtaXVtXCIpO1xuICAgICAgY29uc3QgbmV3RW5kbyA9IHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbCgpO1xuICAgICAgQWpheC5wb3N0KEFwaXMuRU5ET19NRVJHRSwgbmV3RW5kbywgeyBsb2FkaW5nOiB0cnVlIH0pLnRoZW4oKHJlcykgPT4ge1xuICAgICAgICBjb25zdCBlbmRvID0gXy5nZXQocmVzLCBcImJvZHkucmVzcERhdGEuZW5kb1wiLCB7fSk7XG4gICAgICAgIHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1VJTW9kZWwoZW5kbyk7XG4gICAgICAgIHBhZ2VUbyh0aGlzLCBBbGxTdGVwcy5MT0NBVElPTik7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkUHJlZml4Pzogc3RyaW5nKTogc3RyaW5nIHtcbiAgICBpZiAoZGF0YUlkUHJlZml4KSByZXR1cm4gYCR7ZGF0YUlkUHJlZml4fS4ke3Byb3BOYW1lfWA7XG4gICAgcmV0dXJuIGAke3Byb3BOYW1lfWA7XG4gIH1cblxuICBnZXRQcm9wKHByb3BOYW1lPzogc3RyaW5nKSB7XG4gICAgaWYgKF8uaXNFbXB0eShwcm9wTmFtZSkpIHtcbiAgICAgIHJldHVybiBcInBvbGljeVwiO1xuICAgIH1cbiAgICByZXR1cm4gYHBvbGljeS4ke3Byb3BOYW1lfWA7XG4gIH1cblxuICBnZXRQcm9wcyhwcm9wTmFtZTogYW55LCBkYXRhRml4ZWQ/OiBhbnkpIHtcbiAgICByZXR1cm4gcHJvcE5hbWU7XG4gIH1cblxuICBwcml2YXRlIGdldFZhbHVlKHByb3BOYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGBtZW1iZXIuZXh0LiR7cHJvcE5hbWV9YCk7XG4gIH1cblxuICByZW5kZXJBZGRyZXNzKCkge1xuICAgIGNvbnN0IHsgZm9ybSwgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBhY3Rpb24gfSA9IHRoaXMuc3RhdGU7XG4gICAgcmV0dXJuIDw+XG4gICAgICB7YWN0aW9uID09PSBcImVkaXRcIiA/XG4gICAgICAgIDw+XG4gICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJDb3VudHJ5XCIpLnRoYWkoXCJDb3VudHJ5XCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICB7dGhpcy5nZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0KFwibmF0aW9uYWxpdHlcIiwgdGhpcy5nZXRWYWx1ZShcImxvY2F0aW9uLmNvdW50cnlDb2RlXCIpKX1cbiAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiQWRkcmVzc1wiKS50aGFpKFwiQWRkcmVzc1wiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPFNob3dBZGRyZXNzIG1vZGVsPXttb2RlbH0gYWRkcmVzc0ZpeGVkPXtgbWVtYmVyLmV4dC5sb2NhdGlvbmB9Lz5cbiAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICA8Lz5cbiAgICAgICAgOlxuICAgICAgICA8Q29tbW9uQWRkcmVzcyBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICAgIGFkZHJlc3NGaXg9e1wibG9jYXRpb25cIn1cbiAgICAgICAgICAgICAgICAgICAgICAgZGF0YUlkT3JQcmVmaXg9e1wibWVtYmVyLmV4dFwifVxuICAgICAgICAvPlxuICAgICAgfVxuICAgIDwvPjtcblxuICB9XG5cbiAgcmVuZGVyQ292ZXJhZ2UoKTogYW55IHtcbiAgICBjb25zdCB7IGZvcm0sIG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHByZW1pdW1DdXJyZW5jeSA9IF8uZ2V0KG1vZGVsLCBcImV4dC5jaGFuZ2VzLnByZW1pdW1DdXJyZW5jeVwiLCBcIlwiKTtcbiAgICBjb25zdCBzaUN1cnJlbmN5ID0gXy5nZXQobW9kZWwsIFwiZXh0LmNoYW5nZXMuc2lDdXJyZW5jeVwiLCBcIlwiKTtcbiAgICBjb25zdCBjb3ZlcmFnZXMgPSBfLmdldChtb2RlbCwgXCJtZW1iZXIuZXh0LmNvdmVyYWdlc1wiLCBbXSkgfHwgW107XG4gICAgY29uc3QgZWZmRGF0ZSA9IF8uZ2V0KG1vZGVsLCBcImV4dC5jaGFuZ2VzLmVmZkRhdGVcIiwgXCJcIik7XG4gICAgY29uc3QgZXhwRGF0ZSA9IF8uZ2V0KG1vZGVsLCBcImV4dC5jaGFuZ2VzLmV4cERhdGVcIiwgXCJcIik7XG4gICAgY29uc3QgYnJlYWtEb3duID0gXy5nZXQobW9kZWwsIFwib2xkUG9saWN5UHJlbWl1bS53cmFwcGVkUHJlbXMuYnJlYWtkb3duXCIsIFtdKSB8fCBbXTtcbiAgICByZXR1cm4gPD57Y292ZXJhZ2VzLm1hcCgoaXRlbTogYW55LCBpOiBudW1iZXIpID0+IHtcbiAgICAgIHJldHVybiA8Q292ZXJhZ2VFbnRyeVxuICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICBrZXk9e2l9XG4gICAgICAgIGVmZkRhdGU9e2VmZkRhdGV9XG4gICAgICAgIGV4cERhdGU9e2V4cERhdGV9XG4gICAgICAgIC8vIHdyYXBwZWRDb21wb25lbnRSZWY9eyhmb3JtOiBhbnkpID0+IHtcbiAgICAgICAgLy8gICBpZiAoZm9ybSkge1xuICAgICAgICAvLyAgICAgdGhpcy5vbGRDb3ZlcmFnZXNbYGNvdmVyYWdlJHtpfWBdID0gZm9ybS5zdGF0ZS5tb2RlbDtcbiAgICAgICAgLy8gICB9XG4gICAgICAgIC8vIH19XG4gICAgICAgIGlzRW5kbz17dHJ1ZX1cbiAgICAgICAgYnJlYWtEb3duPXticmVha0Rvd259XG4gICAgICAgIHJlZj17YGNvdmVyYWdlJHtpfWB9XG4gICAgICAgIHNpQ3VycmVuY3k9e3NpQ3VycmVuY3l9XG4gICAgICAgIHByZW1pdW1DdXJyZW5jeT17cHJlbWl1bUN1cnJlbmN5fVxuICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgIGRhdGFJbmRleD17aX0vPjtcbiAgICB9KX1cbiAgICA8Lz47XG4gIH1cblxuICByZW5kZXJTaWRlSW5mbygpOiBhbnkge1xuICAgIGNvbnN0IHsgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIDxTaWRlSW5mb1xuICAgICAgbGluZUxpc3Q9e2xpc3R9XG4gICAgICBtb2RlbD17bW9kZWx9XG4gICAgICBwcmVtaXVtSWQ9e1wiZGVsdGFQb2xpY3lQcmVtaXVtXCJ9XG4gICAgICBlbmRvRml4ZWQ9e1wiZXh0LmNoYW5nZXNcIn1cbiAgICAgIGFjdGl2ZVN0ZXA9e1wiTG9jYXRpb25zXCJ9Lz47XG4gIH1cbn1cblxuZXhwb3J0IHsgQWRkTG9jYXRpb25FbmRvIH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQVpBO0FBQUE7QUFBQTtBQWFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7OztBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBRUE7Ozs7QUExSEE7QUFDQTtBQTRIQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFUQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUlBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUJBO0FBRUE7OztBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTs7OztBQWxKQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/add-Location.tsx
