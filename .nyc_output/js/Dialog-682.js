__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rc-util/es/KeyCode */ "./node_modules/rc-util/es/KeyCode.js");
/* harmony import */ var rc_util_es_Dom_contains__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rc-util/es/Dom/contains */ "./node_modules/rc-util/es/Dom/contains.js");
/* harmony import */ var rc_animate__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rc-animate */ "./node_modules/rc-animate/es/Animate.js");
/* harmony import */ var _LazyRenderBox__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./LazyRenderBox */ "./node_modules/rc-dialog/es/LazyRenderBox.js");










var uuid = 0;
/* eslint react/no-is-mounted:0 */

function getScroll(w, top) {
  var ret = w['page' + (top ? 'Y' : 'X') + 'Offset'];
  var method = 'scroll' + (top ? 'Top' : 'Left');

  if (typeof ret !== 'number') {
    var d = w.document;
    ret = d.documentElement[method];

    if (typeof ret !== 'number') {
      ret = d.body[method];
    }
  }

  return ret;
}

function setTransformOrigin(node, value) {
  var style = node.style;
  ['Webkit', 'Moz', 'Ms', 'ms'].forEach(function (prefix) {
    style[prefix + 'TransformOrigin'] = value;
  });
  style['transformOrigin'] = value;
}

function offset(el) {
  var rect = el.getBoundingClientRect();
  var pos = {
    left: rect.left,
    top: rect.top
  };
  var doc = el.ownerDocument;
  var w = doc.defaultView || doc.parentWindow;
  pos.left += getScroll(w);
  pos.top += getScroll(w, true);
  return pos;
}

var Dialog = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default()(Dialog, _React$Component);

  function Dialog(props) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Dialog);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(this, _React$Component.call(this, props));

    _this.inTransition = false;

    _this.onAnimateLeave = function () {
      var afterClose = _this.props.afterClose; // need demo?
      // https://github.com/react-component/dialog/pull/28

      if (_this.wrap) {
        _this.wrap.style.display = 'none';
      }

      _this.inTransition = false;

      _this.switchScrollingEffect();

      if (afterClose) {
        afterClose();
      }
    };

    _this.onDialogMouseDown = function () {
      _this.dialogMouseDown = true;
    };

    _this.onMaskMouseUp = function () {
      if (_this.dialogMouseDown) {
        _this.timeoutId = setTimeout(function () {
          _this.dialogMouseDown = false;
        }, 0);
      }
    };

    _this.onMaskClick = function (e) {
      // android trigger click on open (fastclick??)
      if (Date.now() - _this.openTime < 300) {
        return;
      }

      if (e.target === e.currentTarget && !_this.dialogMouseDown) {
        _this.close(e);
      }
    };

    _this.onKeyDown = function (e) {
      var props = _this.props;

      if (props.keyboard && e.keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_6__["default"].ESC) {
        e.stopPropagation();

        _this.close(e);

        return;
      } // keep focus inside dialog


      if (props.visible) {
        if (e.keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_6__["default"].TAB) {
          var activeElement = document.activeElement;
          var sentinelStart = _this.sentinelStart;

          if (e.shiftKey) {
            if (activeElement === sentinelStart) {
              _this.sentinelEnd.focus();
            }
          } else if (activeElement === _this.sentinelEnd) {
            sentinelStart.focus();
          }
        }
      }
    };

    _this.getDialogElement = function () {
      var props = _this.props;
      var closable = props.closable;
      var prefixCls = props.prefixCls;
      var dest = {};

      if (props.width !== undefined) {
        dest.width = props.width;
      }

      if (props.height !== undefined) {
        dest.height = props.height;
      }

      var footer = void 0;

      if (props.footer) {
        footer = react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("div", {
          className: prefixCls + '-footer',
          ref: _this.saveRef('footer')
        }, props.footer);
      }

      var header = void 0;

      if (props.title) {
        header = react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("div", {
          className: prefixCls + '-header',
          ref: _this.saveRef('header')
        }, react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("div", {
          className: prefixCls + '-title',
          id: _this.titleId
        }, props.title));
      }

      var closer = void 0;

      if (closable) {
        closer = react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("button", {
          type: "button",
          onClick: _this.close,
          "aria-label": "Close",
          className: prefixCls + '-close'
        }, props.closeIcon || react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("span", {
          className: prefixCls + '-close-x'
        }));
      }

      var style = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props.style, dest);

      var sentinelStyle = {
        width: 0,
        height: 0,
        overflow: 'hidden'
      };

      var transitionName = _this.getTransitionName();

      var dialogElement = react__WEBPACK_IMPORTED_MODULE_4__["createElement"](_LazyRenderBox__WEBPACK_IMPORTED_MODULE_9__["default"], {
        key: "dialog-element",
        role: "document",
        ref: _this.saveRef('dialog'),
        style: style,
        className: prefixCls + ' ' + (props.className || ''),
        visible: props.visible,
        forceRender: props.forceRender,
        onMouseDown: _this.onDialogMouseDown
      }, react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("div", {
        tabIndex: 0,
        ref: _this.saveRef('sentinelStart'),
        style: sentinelStyle,
        "aria-hidden": "true"
      }), react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("div", {
        className: prefixCls + '-content'
      }, closer, header, react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("div", babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
        className: prefixCls + '-body',
        style: props.bodyStyle,
        ref: _this.saveRef('body')
      }, props.bodyProps), props.children), footer), react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("div", {
        tabIndex: 0,
        ref: _this.saveRef('sentinelEnd'),
        style: sentinelStyle,
        "aria-hidden": "true"
      }));
      return react__WEBPACK_IMPORTED_MODULE_4__["createElement"](rc_animate__WEBPACK_IMPORTED_MODULE_8__["default"], {
        key: "dialog",
        showProp: "visible",
        onLeave: _this.onAnimateLeave,
        transitionName: transitionName,
        component: "",
        transitionAppear: true
      }, props.visible || !props.destroyOnClose ? dialogElement : null);
    };

    _this.getZIndexStyle = function () {
      var style = {};
      var props = _this.props;

      if (props.zIndex !== undefined) {
        style.zIndex = props.zIndex;
      }

      return style;
    };

    _this.getWrapStyle = function () {
      return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, _this.getZIndexStyle(), _this.props.wrapStyle);
    };

    _this.getMaskStyle = function () {
      return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, _this.getZIndexStyle(), _this.props.maskStyle);
    };

    _this.getMaskElement = function () {
      var props = _this.props;
      var maskElement = void 0;

      if (props.mask) {
        var maskTransition = _this.getMaskTransitionName();

        maskElement = react__WEBPACK_IMPORTED_MODULE_4__["createElement"](_LazyRenderBox__WEBPACK_IMPORTED_MODULE_9__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
          style: _this.getMaskStyle(),
          key: "mask",
          className: props.prefixCls + '-mask',
          hiddenClassName: props.prefixCls + '-mask-hidden',
          visible: props.visible
        }, props.maskProps));

        if (maskTransition) {
          maskElement = react__WEBPACK_IMPORTED_MODULE_4__["createElement"](rc_animate__WEBPACK_IMPORTED_MODULE_8__["default"], {
            key: "mask",
            showProp: "visible",
            transitionAppear: true,
            component: "",
            transitionName: maskTransition
          }, maskElement);
        }
      }

      return maskElement;
    };

    _this.getMaskTransitionName = function () {
      var props = _this.props;
      var transitionName = props.maskTransitionName;
      var animation = props.maskAnimation;

      if (!transitionName && animation) {
        transitionName = props.prefixCls + '-' + animation;
      }

      return transitionName;
    };

    _this.getTransitionName = function () {
      var props = _this.props;
      var transitionName = props.transitionName;
      var animation = props.animation;

      if (!transitionName && animation) {
        transitionName = props.prefixCls + '-' + animation;
      }

      return transitionName;
    };

    _this.close = function (e) {
      var onClose = _this.props.onClose;

      if (onClose) {
        onClose(e);
      }
    };

    _this.saveRef = function (name) {
      return function (node) {
        _this[name] = node;
      };
    };

    _this.titleId = 'rcDialogTitle' + uuid++;

    _this.switchScrollingEffect = props.switchScrollingEffect || function () {};

    return _this;
  }

  Dialog.prototype.componentDidMount = function componentDidMount() {
    this.componentDidUpdate({}); // if forceRender is true, set element style display to be none;

    if ((this.props.forceRender || this.props.getContainer === false && !this.props.visible) && this.wrap) {
      this.wrap.style.display = 'none';
    }
  };

  Dialog.prototype.componentDidUpdate = function componentDidUpdate(prevProps) {
    var _props = this.props,
        visible = _props.visible,
        mask = _props.mask,
        focusTriggerAfterClose = _props.focusTriggerAfterClose;
    var mousePosition = this.props.mousePosition;

    if (visible) {
      // first show
      if (!prevProps.visible) {
        this.openTime = Date.now();
        this.switchScrollingEffect();
        this.tryFocus();
        var dialogNode = react_dom__WEBPACK_IMPORTED_MODULE_5__["findDOMNode"](this.dialog);

        if (mousePosition) {
          var elOffset = offset(dialogNode);
          setTransformOrigin(dialogNode, mousePosition.x - elOffset.left + 'px ' + (mousePosition.y - elOffset.top) + 'px');
        } else {
          setTransformOrigin(dialogNode, '');
        }
      }
    } else if (prevProps.visible) {
      this.inTransition = true;

      if (mask && this.lastOutSideFocusNode && focusTriggerAfterClose) {
        try {
          this.lastOutSideFocusNode.focus();
        } catch (e) {
          this.lastOutSideFocusNode = null;
        }

        this.lastOutSideFocusNode = null;
      }
    }
  };

  Dialog.prototype.componentWillUnmount = function componentWillUnmount() {
    var _props2 = this.props,
        visible = _props2.visible,
        getOpenCount = _props2.getOpenCount;

    if ((visible || this.inTransition) && !getOpenCount()) {
      this.switchScrollingEffect();
    }

    clearTimeout(this.timeoutId);
  };

  Dialog.prototype.tryFocus = function tryFocus() {
    if (!Object(rc_util_es_Dom_contains__WEBPACK_IMPORTED_MODULE_7__["default"])(this.wrap, document.activeElement)) {
      this.lastOutSideFocusNode = document.activeElement;
      this.sentinelStart.focus();
    }
  };

  Dialog.prototype.render = function render() {
    var props = this.props;
    var prefixCls = props.prefixCls,
        maskClosable = props.maskClosable;
    var style = this.getWrapStyle(); // clear hide display
    // and only set display after async anim, not here for hide

    if (props.visible) {
      style.display = null;
    }

    return react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("div", {
      className: prefixCls + '-root'
    }, this.getMaskElement(), react__WEBPACK_IMPORTED_MODULE_4__["createElement"]("div", babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
      tabIndex: -1,
      onKeyDown: this.onKeyDown,
      className: prefixCls + '-wrap ' + (props.wrapClassName || ''),
      ref: this.saveRef('wrap'),
      onClick: maskClosable ? this.onMaskClick : null,
      onMouseUp: maskClosable ? this.onMaskMouseUp : null,
      role: "dialog",
      "aria-labelledby": props.title ? this.titleId : null,
      style: style
    }, props.wrapProps), this.getDialogElement()));
  };

  return Dialog;
}(react__WEBPACK_IMPORTED_MODULE_4__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Dialog);
Dialog.defaultProps = {
  className: '',
  mask: true,
  visible: false,
  keyboard: true,
  closable: true,
  maskClosable: true,
  destroyOnClose: false,
  prefixCls: 'rc-dialog',
  focusTriggerAfterClose: true
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZGlhbG9nL2VzL0RpYWxvZy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWRpYWxvZy9lcy9EaWFsb2cuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBfY2xhc3NDYWxsQ2hlY2sgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJztcbmltcG9ydCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybiBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybic7XG5pbXBvcnQgX2luaGVyaXRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cyc7XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgKiBhcyBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xuaW1wb3J0IEtleUNvZGUgZnJvbSAncmMtdXRpbC9lcy9LZXlDb2RlJztcbmltcG9ydCBjb250YWlucyBmcm9tICdyYy11dGlsL2VzL0RvbS9jb250YWlucyc7XG5pbXBvcnQgQW5pbWF0ZSBmcm9tICdyYy1hbmltYXRlJztcbmltcG9ydCBMYXp5UmVuZGVyQm94IGZyb20gJy4vTGF6eVJlbmRlckJveCc7XG52YXIgdXVpZCA9IDA7XG4vKiBlc2xpbnQgcmVhY3Qvbm8taXMtbW91bnRlZDowICovXG5mdW5jdGlvbiBnZXRTY3JvbGwodywgdG9wKSB7XG4gICAgdmFyIHJldCA9IHdbJ3BhZ2UnICsgKHRvcCA/ICdZJyA6ICdYJykgKyAnT2Zmc2V0J107XG4gICAgdmFyIG1ldGhvZCA9ICdzY3JvbGwnICsgKHRvcCA/ICdUb3AnIDogJ0xlZnQnKTtcbiAgICBpZiAodHlwZW9mIHJldCAhPT0gJ251bWJlcicpIHtcbiAgICAgICAgdmFyIGQgPSB3LmRvY3VtZW50O1xuICAgICAgICByZXQgPSBkLmRvY3VtZW50RWxlbWVudFttZXRob2RdO1xuICAgICAgICBpZiAodHlwZW9mIHJldCAhPT0gJ251bWJlcicpIHtcbiAgICAgICAgICAgIHJldCA9IGQuYm9keVttZXRob2RdO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiByZXQ7XG59XG5mdW5jdGlvbiBzZXRUcmFuc2Zvcm1PcmlnaW4obm9kZSwgdmFsdWUpIHtcbiAgICB2YXIgc3R5bGUgPSBub2RlLnN0eWxlO1xuICAgIFsnV2Via2l0JywgJ01veicsICdNcycsICdtcyddLmZvckVhY2goZnVuY3Rpb24gKHByZWZpeCkge1xuICAgICAgICBzdHlsZVtwcmVmaXggKyAnVHJhbnNmb3JtT3JpZ2luJ10gPSB2YWx1ZTtcbiAgICB9KTtcbiAgICBzdHlsZVsndHJhbnNmb3JtT3JpZ2luJ10gPSB2YWx1ZTtcbn1cbmZ1bmN0aW9uIG9mZnNldChlbCkge1xuICAgIHZhciByZWN0ID0gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgdmFyIHBvcyA9IHtcbiAgICAgICAgbGVmdDogcmVjdC5sZWZ0LFxuICAgICAgICB0b3A6IHJlY3QudG9wXG4gICAgfTtcbiAgICB2YXIgZG9jID0gZWwub3duZXJEb2N1bWVudDtcbiAgICB2YXIgdyA9IGRvYy5kZWZhdWx0VmlldyB8fCBkb2MucGFyZW50V2luZG93O1xuICAgIHBvcy5sZWZ0ICs9IGdldFNjcm9sbCh3KTtcbiAgICBwb3MudG9wICs9IGdldFNjcm9sbCh3LCB0cnVlKTtcbiAgICByZXR1cm4gcG9zO1xufVxuXG52YXIgRGlhbG9nID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgICBfaW5oZXJpdHMoRGlhbG9nLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICAgIGZ1bmN0aW9uIERpYWxvZyhwcm9wcykge1xuICAgICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgRGlhbG9nKTtcblxuICAgICAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmNhbGwodGhpcywgcHJvcHMpKTtcblxuICAgICAgICBfdGhpcy5pblRyYW5zaXRpb24gPSBmYWxzZTtcbiAgICAgICAgX3RoaXMub25BbmltYXRlTGVhdmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYWZ0ZXJDbG9zZSA9IF90aGlzLnByb3BzLmFmdGVyQ2xvc2U7XG4gICAgICAgICAgICAvLyBuZWVkIGRlbW8/XG4gICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vcmVhY3QtY29tcG9uZW50L2RpYWxvZy9wdWxsLzI4XG5cbiAgICAgICAgICAgIGlmIChfdGhpcy53cmFwKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMud3JhcC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgX3RoaXMuaW5UcmFuc2l0aW9uID0gZmFsc2U7XG4gICAgICAgICAgICBfdGhpcy5zd2l0Y2hTY3JvbGxpbmdFZmZlY3QoKTtcbiAgICAgICAgICAgIGlmIChhZnRlckNsb3NlKSB7XG4gICAgICAgICAgICAgICAgYWZ0ZXJDbG9zZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICBfdGhpcy5vbkRpYWxvZ01vdXNlRG93biA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIF90aGlzLmRpYWxvZ01vdXNlRG93biA9IHRydWU7XG4gICAgICAgIH07XG4gICAgICAgIF90aGlzLm9uTWFza01vdXNlVXAgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAoX3RoaXMuZGlhbG9nTW91c2VEb3duKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMudGltZW91dElkID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIF90aGlzLmRpYWxvZ01vdXNlRG93biA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0sIDApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICBfdGhpcy5vbk1hc2tDbGljayA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAvLyBhbmRyb2lkIHRyaWdnZXIgY2xpY2sgb24gb3BlbiAoZmFzdGNsaWNrPz8pXG4gICAgICAgICAgICBpZiAoRGF0ZS5ub3coKSAtIF90aGlzLm9wZW5UaW1lIDwgMzAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGUudGFyZ2V0ID09PSBlLmN1cnJlbnRUYXJnZXQgJiYgIV90aGlzLmRpYWxvZ01vdXNlRG93bikge1xuICAgICAgICAgICAgICAgIF90aGlzLmNsb3NlKGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICBfdGhpcy5vbktleURvd24gPSBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAocHJvcHMua2V5Ym9hcmQgJiYgZS5rZXlDb2RlID09PSBLZXlDb2RlLkVTQykge1xuICAgICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgICAgX3RoaXMuY2xvc2UoZSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8ga2VlcCBmb2N1cyBpbnNpZGUgZGlhbG9nXG4gICAgICAgICAgICBpZiAocHJvcHMudmlzaWJsZSkge1xuICAgICAgICAgICAgICAgIGlmIChlLmtleUNvZGUgPT09IEtleUNvZGUuVEFCKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBhY3RpdmVFbGVtZW50ID0gZG9jdW1lbnQuYWN0aXZlRWxlbWVudDtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHNlbnRpbmVsU3RhcnQgPSBfdGhpcy5zZW50aW5lbFN0YXJ0O1xuICAgICAgICAgICAgICAgICAgICBpZiAoZS5zaGlmdEtleSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFjdGl2ZUVsZW1lbnQgPT09IHNlbnRpbmVsU3RhcnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5zZW50aW5lbEVuZC5mb2N1cygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGFjdGl2ZUVsZW1lbnQgPT09IF90aGlzLnNlbnRpbmVsRW5kKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZW50aW5lbFN0YXJ0LmZvY3VzKCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIF90aGlzLmdldERpYWxvZ0VsZW1lbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgcHJvcHMgPSBfdGhpcy5wcm9wcztcbiAgICAgICAgICAgIHZhciBjbG9zYWJsZSA9IHByb3BzLmNsb3NhYmxlO1xuICAgICAgICAgICAgdmFyIHByZWZpeENscyA9IHByb3BzLnByZWZpeENscztcbiAgICAgICAgICAgIHZhciBkZXN0ID0ge307XG4gICAgICAgICAgICBpZiAocHJvcHMud2lkdGggIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIGRlc3Qud2lkdGggPSBwcm9wcy53aWR0aDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChwcm9wcy5oZWlnaHQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIGRlc3QuaGVpZ2h0ID0gcHJvcHMuaGVpZ2h0O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIGZvb3RlciA9IHZvaWQgMDtcbiAgICAgICAgICAgIGlmIChwcm9wcy5mb290ZXIpIHtcbiAgICAgICAgICAgICAgICBmb290ZXIgPSBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHsgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLWZvb3RlcicsIHJlZjogX3RoaXMuc2F2ZVJlZignZm9vdGVyJykgfSwgcHJvcHMuZm9vdGVyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHZhciBoZWFkZXIgPSB2b2lkIDA7XG4gICAgICAgICAgICBpZiAocHJvcHMudGl0bGUpIHtcbiAgICAgICAgICAgICAgICBoZWFkZXIgPSBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHsgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLWhlYWRlcicsIHJlZjogX3RoaXMuc2F2ZVJlZignaGVhZGVyJykgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7IGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy10aXRsZScsIGlkOiBfdGhpcy50aXRsZUlkIH0sIHByb3BzLnRpdGxlKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgY2xvc2VyID0gdm9pZCAwO1xuICAgICAgICAgICAgaWYgKGNsb3NhYmxlKSB7XG4gICAgICAgICAgICAgICAgY2xvc2VyID0gUmVhY3QuY3JlYXRlRWxlbWVudChcImJ1dHRvblwiLCB7IHR5cGU6IFwiYnV0dG9uXCIsIG9uQ2xpY2s6IF90aGlzLmNsb3NlLCBcImFyaWEtbGFiZWxcIjogXCJDbG9zZVwiLCBjbGFzc05hbWU6IHByZWZpeENscyArICctY2xvc2UnIH0sIHByb3BzLmNsb3NlSWNvbiB8fCBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7IGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy1jbG9zZS14JyB9KSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgc3R5bGUgPSBfZXh0ZW5kcyh7fSwgcHJvcHMuc3R5bGUsIGRlc3QpO1xuICAgICAgICAgICAgdmFyIHNlbnRpbmVsU3R5bGUgPSB7IHdpZHRoOiAwLCBoZWlnaHQ6IDAsIG92ZXJmbG93OiAnaGlkZGVuJyB9O1xuICAgICAgICAgICAgdmFyIHRyYW5zaXRpb25OYW1lID0gX3RoaXMuZ2V0VHJhbnNpdGlvbk5hbWUoKTtcbiAgICAgICAgICAgIHZhciBkaWFsb2dFbGVtZW50ID0gUmVhY3QuY3JlYXRlRWxlbWVudChMYXp5UmVuZGVyQm94LCB7IGtleTogXCJkaWFsb2ctZWxlbWVudFwiLCByb2xlOiBcImRvY3VtZW50XCIsIHJlZjogX3RoaXMuc2F2ZVJlZignZGlhbG9nJyksIHN0eWxlOiBzdHlsZSwgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnICcgKyAocHJvcHMuY2xhc3NOYW1lIHx8ICcnKSwgdmlzaWJsZTogcHJvcHMudmlzaWJsZSwgZm9yY2VSZW5kZXI6IHByb3BzLmZvcmNlUmVuZGVyLCBvbk1vdXNlRG93bjogX3RoaXMub25EaWFsb2dNb3VzZURvd24gfSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7IHRhYkluZGV4OiAwLCByZWY6IF90aGlzLnNhdmVSZWYoJ3NlbnRpbmVsU3RhcnQnKSwgc3R5bGU6IHNlbnRpbmVsU3R5bGUsIFwiYXJpYS1oaWRkZW5cIjogXCJ0cnVlXCIgfSksIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwgeyBjbGFzc05hbWU6IHByZWZpeENscyArICctY29udGVudCcgfSwgY2xvc2VyLCBoZWFkZXIsIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwgX2V4dGVuZHMoeyBjbGFzc05hbWU6IHByZWZpeENscyArICctYm9keScsIHN0eWxlOiBwcm9wcy5ib2R5U3R5bGUsIHJlZjogX3RoaXMuc2F2ZVJlZignYm9keScpIH0sIHByb3BzLmJvZHlQcm9wcyksIHByb3BzLmNoaWxkcmVuKSwgZm9vdGVyKSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7IHRhYkluZGV4OiAwLCByZWY6IF90aGlzLnNhdmVSZWYoJ3NlbnRpbmVsRW5kJyksIHN0eWxlOiBzZW50aW5lbFN0eWxlLCBcImFyaWEtaGlkZGVuXCI6IFwidHJ1ZVwiIH0pKTtcbiAgICAgICAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KEFuaW1hdGUsIHsga2V5OiBcImRpYWxvZ1wiLCBzaG93UHJvcDogXCJ2aXNpYmxlXCIsIG9uTGVhdmU6IF90aGlzLm9uQW5pbWF0ZUxlYXZlLCB0cmFuc2l0aW9uTmFtZTogdHJhbnNpdGlvbk5hbWUsIGNvbXBvbmVudDogXCJcIiwgdHJhbnNpdGlvbkFwcGVhcjogdHJ1ZSB9LCBwcm9wcy52aXNpYmxlIHx8ICFwcm9wcy5kZXN0cm95T25DbG9zZSA/IGRpYWxvZ0VsZW1lbnQgOiBudWxsKTtcbiAgICAgICAgfTtcbiAgICAgICAgX3RoaXMuZ2V0WkluZGV4U3R5bGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgc3R5bGUgPSB7fTtcbiAgICAgICAgICAgIHZhciBwcm9wcyA9IF90aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKHByb3BzLnpJbmRleCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgc3R5bGUuekluZGV4ID0gcHJvcHMuekluZGV4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHN0eWxlO1xuICAgICAgICB9O1xuICAgICAgICBfdGhpcy5nZXRXcmFwU3R5bGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gX2V4dGVuZHMoe30sIF90aGlzLmdldFpJbmRleFN0eWxlKCksIF90aGlzLnByb3BzLndyYXBTdHlsZSk7XG4gICAgICAgIH07XG4gICAgICAgIF90aGlzLmdldE1hc2tTdHlsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBfZXh0ZW5kcyh7fSwgX3RoaXMuZ2V0WkluZGV4U3R5bGUoKSwgX3RoaXMucHJvcHMubWFza1N0eWxlKTtcbiAgICAgICAgfTtcbiAgICAgICAgX3RoaXMuZ2V0TWFza0VsZW1lbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgcHJvcHMgPSBfdGhpcy5wcm9wcztcbiAgICAgICAgICAgIHZhciBtYXNrRWxlbWVudCA9IHZvaWQgMDtcbiAgICAgICAgICAgIGlmIChwcm9wcy5tYXNrKSB7XG4gICAgICAgICAgICAgICAgdmFyIG1hc2tUcmFuc2l0aW9uID0gX3RoaXMuZ2V0TWFza1RyYW5zaXRpb25OYW1lKCk7XG4gICAgICAgICAgICAgICAgbWFza0VsZW1lbnQgPSBSZWFjdC5jcmVhdGVFbGVtZW50KExhenlSZW5kZXJCb3gsIF9leHRlbmRzKHsgc3R5bGU6IF90aGlzLmdldE1hc2tTdHlsZSgpLCBrZXk6IFwibWFza1wiLCBjbGFzc05hbWU6IHByb3BzLnByZWZpeENscyArICctbWFzaycsIGhpZGRlbkNsYXNzTmFtZTogcHJvcHMucHJlZml4Q2xzICsgJy1tYXNrLWhpZGRlbicsIHZpc2libGU6IHByb3BzLnZpc2libGUgfSwgcHJvcHMubWFza1Byb3BzKSk7XG4gICAgICAgICAgICAgICAgaWYgKG1hc2tUcmFuc2l0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgIG1hc2tFbGVtZW50ID0gUmVhY3QuY3JlYXRlRWxlbWVudChBbmltYXRlLCB7IGtleTogXCJtYXNrXCIsIHNob3dQcm9wOiBcInZpc2libGVcIiwgdHJhbnNpdGlvbkFwcGVhcjogdHJ1ZSwgY29tcG9uZW50OiBcIlwiLCB0cmFuc2l0aW9uTmFtZTogbWFza1RyYW5zaXRpb24gfSwgbWFza0VsZW1lbnQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBtYXNrRWxlbWVudDtcbiAgICAgICAgfTtcbiAgICAgICAgX3RoaXMuZ2V0TWFza1RyYW5zaXRpb25OYW1lID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG4gICAgICAgICAgICB2YXIgdHJhbnNpdGlvbk5hbWUgPSBwcm9wcy5tYXNrVHJhbnNpdGlvbk5hbWU7XG4gICAgICAgICAgICB2YXIgYW5pbWF0aW9uID0gcHJvcHMubWFza0FuaW1hdGlvbjtcbiAgICAgICAgICAgIGlmICghdHJhbnNpdGlvbk5hbWUgJiYgYW5pbWF0aW9uKSB7XG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbk5hbWUgPSBwcm9wcy5wcmVmaXhDbHMgKyAnLScgKyBhbmltYXRpb247XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdHJhbnNpdGlvbk5hbWU7XG4gICAgICAgIH07XG4gICAgICAgIF90aGlzLmdldFRyYW5zaXRpb25OYW1lID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG4gICAgICAgICAgICB2YXIgdHJhbnNpdGlvbk5hbWUgPSBwcm9wcy50cmFuc2l0aW9uTmFtZTtcbiAgICAgICAgICAgIHZhciBhbmltYXRpb24gPSBwcm9wcy5hbmltYXRpb247XG4gICAgICAgICAgICBpZiAoIXRyYW5zaXRpb25OYW1lICYmIGFuaW1hdGlvbikge1xuICAgICAgICAgICAgICAgIHRyYW5zaXRpb25OYW1lID0gcHJvcHMucHJlZml4Q2xzICsgJy0nICsgYW5pbWF0aW9uO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHRyYW5zaXRpb25OYW1lO1xuICAgICAgICB9O1xuICAgICAgICBfdGhpcy5jbG9zZSA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICB2YXIgb25DbG9zZSA9IF90aGlzLnByb3BzLm9uQ2xvc2U7XG5cbiAgICAgICAgICAgIGlmIChvbkNsb3NlKSB7XG4gICAgICAgICAgICAgICAgb25DbG9zZShlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgX3RoaXMuc2F2ZVJlZiA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgICAgICAgICAgICBfdGhpc1tuYW1lXSA9IG5vZGU7XG4gICAgICAgICAgICB9O1xuICAgICAgICB9O1xuICAgICAgICBfdGhpcy50aXRsZUlkID0gJ3JjRGlhbG9nVGl0bGUnICsgdXVpZCsrO1xuICAgICAgICBfdGhpcy5zd2l0Y2hTY3JvbGxpbmdFZmZlY3QgPSBwcm9wcy5zd2l0Y2hTY3JvbGxpbmdFZmZlY3QgfHwgZnVuY3Rpb24gKCkge307XG4gICAgICAgIHJldHVybiBfdGhpcztcbiAgICB9XG5cbiAgICBEaWFsb2cucHJvdG90eXBlLmNvbXBvbmVudERpZE1vdW50ID0gZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIHRoaXMuY29tcG9uZW50RGlkVXBkYXRlKHt9KTtcbiAgICAgICAgLy8gaWYgZm9yY2VSZW5kZXIgaXMgdHJ1ZSwgc2V0IGVsZW1lbnQgc3R5bGUgZGlzcGxheSB0byBiZSBub25lO1xuICAgICAgICBpZiAoKHRoaXMucHJvcHMuZm9yY2VSZW5kZXIgfHwgdGhpcy5wcm9wcy5nZXRDb250YWluZXIgPT09IGZhbHNlICYmICF0aGlzLnByb3BzLnZpc2libGUpICYmIHRoaXMud3JhcCkge1xuICAgICAgICAgICAgdGhpcy53cmFwLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgRGlhbG9nLnByb3RvdHlwZS5jb21wb25lbnREaWRVcGRhdGUgPSBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzKSB7XG4gICAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgICAgdmlzaWJsZSA9IF9wcm9wcy52aXNpYmxlLFxuICAgICAgICAgICAgbWFzayA9IF9wcm9wcy5tYXNrLFxuICAgICAgICAgICAgZm9jdXNUcmlnZ2VyQWZ0ZXJDbG9zZSA9IF9wcm9wcy5mb2N1c1RyaWdnZXJBZnRlckNsb3NlO1xuXG4gICAgICAgIHZhciBtb3VzZVBvc2l0aW9uID0gdGhpcy5wcm9wcy5tb3VzZVBvc2l0aW9uO1xuICAgICAgICBpZiAodmlzaWJsZSkge1xuICAgICAgICAgICAgLy8gZmlyc3Qgc2hvd1xuICAgICAgICAgICAgaWYgKCFwcmV2UHJvcHMudmlzaWJsZSkge1xuICAgICAgICAgICAgICAgIHRoaXMub3BlblRpbWUgPSBEYXRlLm5vdygpO1xuICAgICAgICAgICAgICAgIHRoaXMuc3dpdGNoU2Nyb2xsaW5nRWZmZWN0KCk7XG4gICAgICAgICAgICAgICAgdGhpcy50cnlGb2N1cygpO1xuICAgICAgICAgICAgICAgIHZhciBkaWFsb2dOb2RlID0gUmVhY3RET00uZmluZERPTU5vZGUodGhpcy5kaWFsb2cpO1xuICAgICAgICAgICAgICAgIGlmIChtb3VzZVBvc2l0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBlbE9mZnNldCA9IG9mZnNldChkaWFsb2dOb2RlKTtcbiAgICAgICAgICAgICAgICAgICAgc2V0VHJhbnNmb3JtT3JpZ2luKGRpYWxvZ05vZGUsIG1vdXNlUG9zaXRpb24ueCAtIGVsT2Zmc2V0LmxlZnQgKyAncHggJyArIChtb3VzZVBvc2l0aW9uLnkgLSBlbE9mZnNldC50b3ApICsgJ3B4Jyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgc2V0VHJhbnNmb3JtT3JpZ2luKGRpYWxvZ05vZGUsICcnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAocHJldlByb3BzLnZpc2libGUpIHtcbiAgICAgICAgICAgIHRoaXMuaW5UcmFuc2l0aW9uID0gdHJ1ZTtcbiAgICAgICAgICAgIGlmIChtYXNrICYmIHRoaXMubGFzdE91dFNpZGVGb2N1c05vZGUgJiYgZm9jdXNUcmlnZ2VyQWZ0ZXJDbG9zZSkge1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGFzdE91dFNpZGVGb2N1c05vZGUuZm9jdXMoKTtcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGFzdE91dFNpZGVGb2N1c05vZGUgPSBudWxsO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLmxhc3RPdXRTaWRlRm9jdXNOb2RlID0gbnVsbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBEaWFsb2cucHJvdG90eXBlLmNvbXBvbmVudFdpbGxVbm1vdW50ID0gZnVuY3Rpb24gY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICAgIHZhciBfcHJvcHMyID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICAgIHZpc2libGUgPSBfcHJvcHMyLnZpc2libGUsXG4gICAgICAgICAgICBnZXRPcGVuQ291bnQgPSBfcHJvcHMyLmdldE9wZW5Db3VudDtcblxuICAgICAgICBpZiAoKHZpc2libGUgfHwgdGhpcy5pblRyYW5zaXRpb24pICYmICFnZXRPcGVuQ291bnQoKSkge1xuICAgICAgICAgICAgdGhpcy5zd2l0Y2hTY3JvbGxpbmdFZmZlY3QoKTtcbiAgICAgICAgfVxuICAgICAgICBjbGVhclRpbWVvdXQodGhpcy50aW1lb3V0SWQpO1xuICAgIH07XG5cbiAgICBEaWFsb2cucHJvdG90eXBlLnRyeUZvY3VzID0gZnVuY3Rpb24gdHJ5Rm9jdXMoKSB7XG4gICAgICAgIGlmICghY29udGFpbnModGhpcy53cmFwLCBkb2N1bWVudC5hY3RpdmVFbGVtZW50KSkge1xuICAgICAgICAgICAgdGhpcy5sYXN0T3V0U2lkZUZvY3VzTm9kZSA9IGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQ7XG4gICAgICAgICAgICB0aGlzLnNlbnRpbmVsU3RhcnQuZm9jdXMoKTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBEaWFsb2cucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgICAgdmFyIHByb3BzID0gdGhpcy5wcm9wcztcbiAgICAgICAgdmFyIHByZWZpeENscyA9IHByb3BzLnByZWZpeENscyxcbiAgICAgICAgICAgIG1hc2tDbG9zYWJsZSA9IHByb3BzLm1hc2tDbG9zYWJsZTtcblxuICAgICAgICB2YXIgc3R5bGUgPSB0aGlzLmdldFdyYXBTdHlsZSgpO1xuICAgICAgICAvLyBjbGVhciBoaWRlIGRpc3BsYXlcbiAgICAgICAgLy8gYW5kIG9ubHkgc2V0IGRpc3BsYXkgYWZ0ZXIgYXN5bmMgYW5pbSwgbm90IGhlcmUgZm9yIGhpZGVcbiAgICAgICAgaWYgKHByb3BzLnZpc2libGUpIHtcbiAgICAgICAgICAgIHN0eWxlLmRpc3BsYXkgPSBudWxsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHsgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLXJvb3QnIH0sIHRoaXMuZ2V0TWFza0VsZW1lbnQoKSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCBfZXh0ZW5kcyh7IHRhYkluZGV4OiAtMSwgb25LZXlEb3duOiB0aGlzLm9uS2V5RG93biwgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLXdyYXAgJyArIChwcm9wcy53cmFwQ2xhc3NOYW1lIHx8ICcnKSwgcmVmOiB0aGlzLnNhdmVSZWYoJ3dyYXAnKSwgb25DbGljazogbWFza0Nsb3NhYmxlID8gdGhpcy5vbk1hc2tDbGljayA6IG51bGwsIG9uTW91c2VVcDogbWFza0Nsb3NhYmxlID8gdGhpcy5vbk1hc2tNb3VzZVVwIDogbnVsbCwgcm9sZTogXCJkaWFsb2dcIiwgXCJhcmlhLWxhYmVsbGVkYnlcIjogcHJvcHMudGl0bGUgPyB0aGlzLnRpdGxlSWQgOiBudWxsLCBzdHlsZTogc3R5bGUgfSwgcHJvcHMud3JhcFByb3BzKSwgdGhpcy5nZXREaWFsb2dFbGVtZW50KCkpKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIERpYWxvZztcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxuZXhwb3J0IGRlZmF1bHQgRGlhbG9nO1xuXG5EaWFsb2cuZGVmYXVsdFByb3BzID0ge1xuICAgIGNsYXNzTmFtZTogJycsXG4gICAgbWFzazogdHJ1ZSxcbiAgICB2aXNpYmxlOiBmYWxzZSxcbiAgICBrZXlib2FyZDogdHJ1ZSxcbiAgICBjbG9zYWJsZTogdHJ1ZSxcbiAgICBtYXNrQ2xvc2FibGU6IHRydWUsXG4gICAgZGVzdHJveU9uQ2xvc2U6IGZhbHNlLFxuICAgIHByZWZpeENsczogJ3JjLWRpYWxvZycsXG4gICAgZm9jdXNUcmlnZ2VyQWZ0ZXJDbG9zZTogdHJ1ZVxufTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-dialog/es/Dialog.js
