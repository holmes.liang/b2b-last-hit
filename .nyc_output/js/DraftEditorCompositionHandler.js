/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEditorCompositionHandler
 * @format
 * 
 */


var DraftFeatureFlags = __webpack_require__(/*! ./DraftFeatureFlags */ "./node_modules/draft-js/lib/DraftFeatureFlags.js");

var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var Keys = __webpack_require__(/*! fbjs/lib/Keys */ "./node_modules/fbjs/lib/Keys.js");

var getEntityKeyForSelection = __webpack_require__(/*! ./getEntityKeyForSelection */ "./node_modules/draft-js/lib/getEntityKeyForSelection.js");

var isEventHandled = __webpack_require__(/*! ./isEventHandled */ "./node_modules/draft-js/lib/isEventHandled.js");

var isSelectionAtLeafStart = __webpack_require__(/*! ./isSelectionAtLeafStart */ "./node_modules/draft-js/lib/isSelectionAtLeafStart.js");
/**
 * Millisecond delay to allow `compositionstart` to fire again upon
 * `compositionend`.
 *
 * This is used for Korean input to ensure that typing can continue without
 * the editor trying to render too quickly. More specifically, Safari 7.1+
 * triggers `compositionstart` a little slower than Chrome/FF, which
 * leads to composed characters being resolved and re-render occurring
 * sooner than we want.
 */


var RESOLVE_DELAY = 20;
/**
 * A handful of variables used to track the current composition and its
 * resolution status. These exist at the module level because it is not
 * possible to have compositions occurring in multiple editors simultaneously,
 * and it simplifies state management with respect to the DraftEditor component.
 */

var resolved = false;
var stillComposing = false;
var textInputData = '';
var DraftEditorCompositionHandler = {
  onBeforeInput: function onBeforeInput(editor, e) {
    textInputData = (textInputData || '') + e.data;
  },

  /**
   * A `compositionstart` event has fired while we're still in composition
   * mode. Continue the current composition session to prevent a re-render.
   */
  onCompositionStart: function onCompositionStart(editor) {
    stillComposing = true;
  },

  /**
   * Attempt to end the current composition session.
   *
   * Defer handling because browser will still insert the chars into active
   * element after `compositionend`. If a `compositionstart` event fires
   * before `resolveComposition` executes, our composition session will
   * continue.
   *
   * The `resolved` flag is useful because certain IME interfaces fire the
   * `compositionend` event multiple times, thus queueing up multiple attempts
   * at handling the composition. Since handling the same composition event
   * twice could break the DOM, we only use the first event. Example: Arabic
   * Google Input Tools on Windows 8.1 fires `compositionend` three times.
   */
  onCompositionEnd: function onCompositionEnd(editor) {
    resolved = false;
    stillComposing = false;
    setTimeout(function () {
      if (!resolved) {
        DraftEditorCompositionHandler.resolveComposition(editor);
      }
    }, RESOLVE_DELAY);
  },

  /**
   * In Safari, keydown events may fire when committing compositions. If
   * the arrow keys are used to commit, prevent default so that the cursor
   * doesn't move, otherwise it will jump back noticeably on re-render.
   */
  onKeyDown: function onKeyDown(editor, e) {
    if (!stillComposing) {
      // If a keydown event is received after compositionend but before the
      // 20ms timer expires (ex: type option-E then backspace, or type A then
      // backspace in 2-Set Korean), we should immediately resolve the
      // composition and reinterpret the key press in edit mode.
      DraftEditorCompositionHandler.resolveComposition(editor);

      editor._onKeyDown(e);

      return;
    }

    if (e.which === Keys.RIGHT || e.which === Keys.LEFT) {
      e.preventDefault();
    }
  },

  /**
   * Keypress events may fire when committing compositions. In Firefox,
   * pressing RETURN commits the composition and inserts extra newline
   * characters that we do not want. `preventDefault` allows the composition
   * to be committed while preventing the extra characters.
   */
  onKeyPress: function onKeyPress(editor, e) {
    if (e.which === Keys.RETURN) {
      e.preventDefault();
    }
  },

  /**
   * Attempt to insert composed characters into the document.
   *
   * If we are still in a composition session, do nothing. Otherwise, insert
   * the characters into the document and terminate the composition session.
   *
   * If no characters were composed -- for instance, the user
   * deleted all composed characters and committed nothing new --
   * force a re-render. We also re-render when the composition occurs
   * at the beginning of a leaf, to ensure that if the browser has
   * created a new text node for the composition, we will discard it.
   *
   * Resetting innerHTML will move focus to the beginning of the editor,
   * so we update to force it back to the correct place.
   */
  resolveComposition: function resolveComposition(editor) {
    if (stillComposing) {
      return;
    }

    resolved = true;
    var composedChars = textInputData;
    textInputData = '';
    var editorState = EditorState.set(editor._latestEditorState, {
      inCompositionMode: false
    });
    var currentStyle = editorState.getCurrentInlineStyle();
    var entityKey = getEntityKeyForSelection(editorState.getCurrentContent(), editorState.getSelection());
    var mustReset = !composedChars || isSelectionAtLeafStart(editorState) || currentStyle.size > 0 || entityKey !== null;

    if (mustReset) {
      editor.restoreEditorDOM();
    }

    editor.exitCurrentMode();

    if (composedChars) {
      if (DraftFeatureFlags.draft_handlebeforeinput_composed_text && editor.props.handleBeforeInput && isEventHandled(editor.props.handleBeforeInput(composedChars, editorState))) {
        return;
      } // If characters have been composed, re-rendering with the update
      // is sufficient to reset the editor.


      var contentState = DraftModifier.replaceText(editorState.getCurrentContent(), editorState.getSelection(), composedChars, currentStyle, entityKey);
      editor.update(EditorState.push(editorState, contentState, 'insert-characters'));
      return;
    }

    if (mustReset) {
      editor.update(EditorState.set(editorState, {
        nativelyRenderedContent: null,
        forceSelection: true
      }));
    }
  }
};
module.exports = DraftEditorCompositionHandler;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RWRpdG9yQ29tcG9zaXRpb25IYW5kbGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RWRpdG9yQ29tcG9zaXRpb25IYW5kbGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgRHJhZnRFZGl0b3JDb21wb3NpdGlvbkhhbmRsZXJcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0RmVhdHVyZUZsYWdzID0gcmVxdWlyZSgnLi9EcmFmdEZlYXR1cmVGbGFncycpO1xudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcbnZhciBLZXlzID0gcmVxdWlyZSgnZmJqcy9saWIvS2V5cycpO1xuXG52YXIgZ2V0RW50aXR5S2V5Rm9yU2VsZWN0aW9uID0gcmVxdWlyZSgnLi9nZXRFbnRpdHlLZXlGb3JTZWxlY3Rpb24nKTtcbnZhciBpc0V2ZW50SGFuZGxlZCA9IHJlcXVpcmUoJy4vaXNFdmVudEhhbmRsZWQnKTtcbnZhciBpc1NlbGVjdGlvbkF0TGVhZlN0YXJ0ID0gcmVxdWlyZSgnLi9pc1NlbGVjdGlvbkF0TGVhZlN0YXJ0Jyk7XG5cbi8qKlxuICogTWlsbGlzZWNvbmQgZGVsYXkgdG8gYWxsb3cgYGNvbXBvc2l0aW9uc3RhcnRgIHRvIGZpcmUgYWdhaW4gdXBvblxuICogYGNvbXBvc2l0aW9uZW5kYC5cbiAqXG4gKiBUaGlzIGlzIHVzZWQgZm9yIEtvcmVhbiBpbnB1dCB0byBlbnN1cmUgdGhhdCB0eXBpbmcgY2FuIGNvbnRpbnVlIHdpdGhvdXRcbiAqIHRoZSBlZGl0b3IgdHJ5aW5nIHRvIHJlbmRlciB0b28gcXVpY2tseS4gTW9yZSBzcGVjaWZpY2FsbHksIFNhZmFyaSA3LjErXG4gKiB0cmlnZ2VycyBgY29tcG9zaXRpb25zdGFydGAgYSBsaXR0bGUgc2xvd2VyIHRoYW4gQ2hyb21lL0ZGLCB3aGljaFxuICogbGVhZHMgdG8gY29tcG9zZWQgY2hhcmFjdGVycyBiZWluZyByZXNvbHZlZCBhbmQgcmUtcmVuZGVyIG9jY3VycmluZ1xuICogc29vbmVyIHRoYW4gd2Ugd2FudC5cbiAqL1xudmFyIFJFU09MVkVfREVMQVkgPSAyMDtcblxuLyoqXG4gKiBBIGhhbmRmdWwgb2YgdmFyaWFibGVzIHVzZWQgdG8gdHJhY2sgdGhlIGN1cnJlbnQgY29tcG9zaXRpb24gYW5kIGl0c1xuICogcmVzb2x1dGlvbiBzdGF0dXMuIFRoZXNlIGV4aXN0IGF0IHRoZSBtb2R1bGUgbGV2ZWwgYmVjYXVzZSBpdCBpcyBub3RcbiAqIHBvc3NpYmxlIHRvIGhhdmUgY29tcG9zaXRpb25zIG9jY3VycmluZyBpbiBtdWx0aXBsZSBlZGl0b3JzIHNpbXVsdGFuZW91c2x5LFxuICogYW5kIGl0IHNpbXBsaWZpZXMgc3RhdGUgbWFuYWdlbWVudCB3aXRoIHJlc3BlY3QgdG8gdGhlIERyYWZ0RWRpdG9yIGNvbXBvbmVudC5cbiAqL1xudmFyIHJlc29sdmVkID0gZmFsc2U7XG52YXIgc3RpbGxDb21wb3NpbmcgPSBmYWxzZTtcbnZhciB0ZXh0SW5wdXREYXRhID0gJyc7XG5cbnZhciBEcmFmdEVkaXRvckNvbXBvc2l0aW9uSGFuZGxlciA9IHtcbiAgb25CZWZvcmVJbnB1dDogZnVuY3Rpb24gb25CZWZvcmVJbnB1dChlZGl0b3IsIGUpIHtcbiAgICB0ZXh0SW5wdXREYXRhID0gKHRleHRJbnB1dERhdGEgfHwgJycpICsgZS5kYXRhO1xuICB9LFxuXG4gIC8qKlxuICAgKiBBIGBjb21wb3NpdGlvbnN0YXJ0YCBldmVudCBoYXMgZmlyZWQgd2hpbGUgd2UncmUgc3RpbGwgaW4gY29tcG9zaXRpb25cbiAgICogbW9kZS4gQ29udGludWUgdGhlIGN1cnJlbnQgY29tcG9zaXRpb24gc2Vzc2lvbiB0byBwcmV2ZW50IGEgcmUtcmVuZGVyLlxuICAgKi9cbiAgb25Db21wb3NpdGlvblN0YXJ0OiBmdW5jdGlvbiBvbkNvbXBvc2l0aW9uU3RhcnQoZWRpdG9yKSB7XG4gICAgc3RpbGxDb21wb3NpbmcgPSB0cnVlO1xuICB9LFxuXG4gIC8qKlxuICAgKiBBdHRlbXB0IHRvIGVuZCB0aGUgY3VycmVudCBjb21wb3NpdGlvbiBzZXNzaW9uLlxuICAgKlxuICAgKiBEZWZlciBoYW5kbGluZyBiZWNhdXNlIGJyb3dzZXIgd2lsbCBzdGlsbCBpbnNlcnQgdGhlIGNoYXJzIGludG8gYWN0aXZlXG4gICAqIGVsZW1lbnQgYWZ0ZXIgYGNvbXBvc2l0aW9uZW5kYC4gSWYgYSBgY29tcG9zaXRpb25zdGFydGAgZXZlbnQgZmlyZXNcbiAgICogYmVmb3JlIGByZXNvbHZlQ29tcG9zaXRpb25gIGV4ZWN1dGVzLCBvdXIgY29tcG9zaXRpb24gc2Vzc2lvbiB3aWxsXG4gICAqIGNvbnRpbnVlLlxuICAgKlxuICAgKiBUaGUgYHJlc29sdmVkYCBmbGFnIGlzIHVzZWZ1bCBiZWNhdXNlIGNlcnRhaW4gSU1FIGludGVyZmFjZXMgZmlyZSB0aGVcbiAgICogYGNvbXBvc2l0aW9uZW5kYCBldmVudCBtdWx0aXBsZSB0aW1lcywgdGh1cyBxdWV1ZWluZyB1cCBtdWx0aXBsZSBhdHRlbXB0c1xuICAgKiBhdCBoYW5kbGluZyB0aGUgY29tcG9zaXRpb24uIFNpbmNlIGhhbmRsaW5nIHRoZSBzYW1lIGNvbXBvc2l0aW9uIGV2ZW50XG4gICAqIHR3aWNlIGNvdWxkIGJyZWFrIHRoZSBET00sIHdlIG9ubHkgdXNlIHRoZSBmaXJzdCBldmVudC4gRXhhbXBsZTogQXJhYmljXG4gICAqIEdvb2dsZSBJbnB1dCBUb29scyBvbiBXaW5kb3dzIDguMSBmaXJlcyBgY29tcG9zaXRpb25lbmRgIHRocmVlIHRpbWVzLlxuICAgKi9cbiAgb25Db21wb3NpdGlvbkVuZDogZnVuY3Rpb24gb25Db21wb3NpdGlvbkVuZChlZGl0b3IpIHtcbiAgICByZXNvbHZlZCA9IGZhbHNlO1xuICAgIHN0aWxsQ29tcG9zaW5nID0gZmFsc2U7XG4gICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoIXJlc29sdmVkKSB7XG4gICAgICAgIERyYWZ0RWRpdG9yQ29tcG9zaXRpb25IYW5kbGVyLnJlc29sdmVDb21wb3NpdGlvbihlZGl0b3IpO1xuICAgICAgfVxuICAgIH0sIFJFU09MVkVfREVMQVkpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBJbiBTYWZhcmksIGtleWRvd24gZXZlbnRzIG1heSBmaXJlIHdoZW4gY29tbWl0dGluZyBjb21wb3NpdGlvbnMuIElmXG4gICAqIHRoZSBhcnJvdyBrZXlzIGFyZSB1c2VkIHRvIGNvbW1pdCwgcHJldmVudCBkZWZhdWx0IHNvIHRoYXQgdGhlIGN1cnNvclxuICAgKiBkb2Vzbid0IG1vdmUsIG90aGVyd2lzZSBpdCB3aWxsIGp1bXAgYmFjayBub3RpY2VhYmx5IG9uIHJlLXJlbmRlci5cbiAgICovXG4gIG9uS2V5RG93bjogZnVuY3Rpb24gb25LZXlEb3duKGVkaXRvciwgZSkge1xuICAgIGlmICghc3RpbGxDb21wb3NpbmcpIHtcbiAgICAgIC8vIElmIGEga2V5ZG93biBldmVudCBpcyByZWNlaXZlZCBhZnRlciBjb21wb3NpdGlvbmVuZCBidXQgYmVmb3JlIHRoZVxuICAgICAgLy8gMjBtcyB0aW1lciBleHBpcmVzIChleDogdHlwZSBvcHRpb24tRSB0aGVuIGJhY2tzcGFjZSwgb3IgdHlwZSBBIHRoZW5cbiAgICAgIC8vIGJhY2tzcGFjZSBpbiAyLVNldCBLb3JlYW4pLCB3ZSBzaG91bGQgaW1tZWRpYXRlbHkgcmVzb2x2ZSB0aGVcbiAgICAgIC8vIGNvbXBvc2l0aW9uIGFuZCByZWludGVycHJldCB0aGUga2V5IHByZXNzIGluIGVkaXQgbW9kZS5cbiAgICAgIERyYWZ0RWRpdG9yQ29tcG9zaXRpb25IYW5kbGVyLnJlc29sdmVDb21wb3NpdGlvbihlZGl0b3IpO1xuICAgICAgZWRpdG9yLl9vbktleURvd24oZSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmIChlLndoaWNoID09PSBLZXlzLlJJR0hUIHx8IGUud2hpY2ggPT09IEtleXMuTEVGVCkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogS2V5cHJlc3MgZXZlbnRzIG1heSBmaXJlIHdoZW4gY29tbWl0dGluZyBjb21wb3NpdGlvbnMuIEluIEZpcmVmb3gsXG4gICAqIHByZXNzaW5nIFJFVFVSTiBjb21taXRzIHRoZSBjb21wb3NpdGlvbiBhbmQgaW5zZXJ0cyBleHRyYSBuZXdsaW5lXG4gICAqIGNoYXJhY3RlcnMgdGhhdCB3ZSBkbyBub3Qgd2FudC4gYHByZXZlbnREZWZhdWx0YCBhbGxvd3MgdGhlIGNvbXBvc2l0aW9uXG4gICAqIHRvIGJlIGNvbW1pdHRlZCB3aGlsZSBwcmV2ZW50aW5nIHRoZSBleHRyYSBjaGFyYWN0ZXJzLlxuICAgKi9cbiAgb25LZXlQcmVzczogZnVuY3Rpb24gb25LZXlQcmVzcyhlZGl0b3IsIGUpIHtcbiAgICBpZiAoZS53aGljaCA9PT0gS2V5cy5SRVRVUk4pIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEF0dGVtcHQgdG8gaW5zZXJ0IGNvbXBvc2VkIGNoYXJhY3RlcnMgaW50byB0aGUgZG9jdW1lbnQuXG4gICAqXG4gICAqIElmIHdlIGFyZSBzdGlsbCBpbiBhIGNvbXBvc2l0aW9uIHNlc3Npb24sIGRvIG5vdGhpbmcuIE90aGVyd2lzZSwgaW5zZXJ0XG4gICAqIHRoZSBjaGFyYWN0ZXJzIGludG8gdGhlIGRvY3VtZW50IGFuZCB0ZXJtaW5hdGUgdGhlIGNvbXBvc2l0aW9uIHNlc3Npb24uXG4gICAqXG4gICAqIElmIG5vIGNoYXJhY3RlcnMgd2VyZSBjb21wb3NlZCAtLSBmb3IgaW5zdGFuY2UsIHRoZSB1c2VyXG4gICAqIGRlbGV0ZWQgYWxsIGNvbXBvc2VkIGNoYXJhY3RlcnMgYW5kIGNvbW1pdHRlZCBub3RoaW5nIG5ldyAtLVxuICAgKiBmb3JjZSBhIHJlLXJlbmRlci4gV2UgYWxzbyByZS1yZW5kZXIgd2hlbiB0aGUgY29tcG9zaXRpb24gb2NjdXJzXG4gICAqIGF0IHRoZSBiZWdpbm5pbmcgb2YgYSBsZWFmLCB0byBlbnN1cmUgdGhhdCBpZiB0aGUgYnJvd3NlciBoYXNcbiAgICogY3JlYXRlZCBhIG5ldyB0ZXh0IG5vZGUgZm9yIHRoZSBjb21wb3NpdGlvbiwgd2Ugd2lsbCBkaXNjYXJkIGl0LlxuICAgKlxuICAgKiBSZXNldHRpbmcgaW5uZXJIVE1MIHdpbGwgbW92ZSBmb2N1cyB0byB0aGUgYmVnaW5uaW5nIG9mIHRoZSBlZGl0b3IsXG4gICAqIHNvIHdlIHVwZGF0ZSB0byBmb3JjZSBpdCBiYWNrIHRvIHRoZSBjb3JyZWN0IHBsYWNlLlxuICAgKi9cbiAgcmVzb2x2ZUNvbXBvc2l0aW9uOiBmdW5jdGlvbiByZXNvbHZlQ29tcG9zaXRpb24oZWRpdG9yKSB7XG4gICAgaWYgKHN0aWxsQ29tcG9zaW5nKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgcmVzb2x2ZWQgPSB0cnVlO1xuICAgIHZhciBjb21wb3NlZENoYXJzID0gdGV4dElucHV0RGF0YTtcbiAgICB0ZXh0SW5wdXREYXRhID0gJyc7XG5cbiAgICB2YXIgZWRpdG9yU3RhdGUgPSBFZGl0b3JTdGF0ZS5zZXQoZWRpdG9yLl9sYXRlc3RFZGl0b3JTdGF0ZSwge1xuICAgICAgaW5Db21wb3NpdGlvbk1vZGU6IGZhbHNlXG4gICAgfSk7XG5cbiAgICB2YXIgY3VycmVudFN0eWxlID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudElubGluZVN0eWxlKCk7XG4gICAgdmFyIGVudGl0eUtleSA9IGdldEVudGl0eUtleUZvclNlbGVjdGlvbihlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpLCBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKSk7XG5cbiAgICB2YXIgbXVzdFJlc2V0ID0gIWNvbXBvc2VkQ2hhcnMgfHwgaXNTZWxlY3Rpb25BdExlYWZTdGFydChlZGl0b3JTdGF0ZSkgfHwgY3VycmVudFN0eWxlLnNpemUgPiAwIHx8IGVudGl0eUtleSAhPT0gbnVsbDtcblxuICAgIGlmIChtdXN0UmVzZXQpIHtcbiAgICAgIGVkaXRvci5yZXN0b3JlRWRpdG9yRE9NKCk7XG4gICAgfVxuXG4gICAgZWRpdG9yLmV4aXRDdXJyZW50TW9kZSgpO1xuXG4gICAgaWYgKGNvbXBvc2VkQ2hhcnMpIHtcbiAgICAgIGlmIChEcmFmdEZlYXR1cmVGbGFncy5kcmFmdF9oYW5kbGViZWZvcmVpbnB1dF9jb21wb3NlZF90ZXh0ICYmIGVkaXRvci5wcm9wcy5oYW5kbGVCZWZvcmVJbnB1dCAmJiBpc0V2ZW50SGFuZGxlZChlZGl0b3IucHJvcHMuaGFuZGxlQmVmb3JlSW5wdXQoY29tcG9zZWRDaGFycywgZWRpdG9yU3RhdGUpKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICAvLyBJZiBjaGFyYWN0ZXJzIGhhdmUgYmVlbiBjb21wb3NlZCwgcmUtcmVuZGVyaW5nIHdpdGggdGhlIHVwZGF0ZVxuICAgICAgLy8gaXMgc3VmZmljaWVudCB0byByZXNldCB0aGUgZWRpdG9yLlxuICAgICAgdmFyIGNvbnRlbnRTdGF0ZSA9IERyYWZ0TW9kaWZpZXIucmVwbGFjZVRleHQoZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKSwgZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCksIGNvbXBvc2VkQ2hhcnMsIGN1cnJlbnRTdHlsZSwgZW50aXR5S2V5KTtcbiAgICAgIGVkaXRvci51cGRhdGUoRWRpdG9yU3RhdGUucHVzaChlZGl0b3JTdGF0ZSwgY29udGVudFN0YXRlLCAnaW5zZXJ0LWNoYXJhY3RlcnMnKSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKG11c3RSZXNldCkge1xuICAgICAgZWRpdG9yLnVwZGF0ZShFZGl0b3JTdGF0ZS5zZXQoZWRpdG9yU3RhdGUsIHtcbiAgICAgICAgbmF0aXZlbHlSZW5kZXJlZENvbnRlbnQ6IG51bGwsXG4gICAgICAgIGZvcmNlU2VsZWN0aW9uOiB0cnVlXG4gICAgICB9KSk7XG4gICAgfVxuICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IERyYWZ0RWRpdG9yQ29tcG9zaXRpb25IYW5kbGVyOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7Ozs7Ozs7QUFVQTtBQUVBOzs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7O0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUE3SEE7QUFnSUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEditorCompositionHandler.js
