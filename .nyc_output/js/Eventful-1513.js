/**
 * Event Mixin
 * @module zrender/mixin/Eventful
 * @author Kener (@Kener-林峰, kener.linfeng@gmail.com)
 *         pissang (https://www.github.com/pissang)
 */
var arrySlice = Array.prototype.slice;
/**
 * Event dispatcher.
 *
 * @alias module:zrender/mixin/Eventful
 * @constructor
 * @param {Object} [eventProcessor] The object eventProcessor is the scope when
 *        `eventProcessor.xxx` called.
 * @param {Function} [eventProcessor.normalizeQuery]
 *        param: {string|Object} Raw query.
 *        return: {string|Object} Normalized query.
 * @param {Function} [eventProcessor.filter] Event will be dispatched only
 *        if it returns `true`.
 *        param: {string} eventType
 *        param: {string|Object} query
 *        return: {boolean}
 * @param {Function} [eventProcessor.afterTrigger] Call after all handlers called.
 *        param: {string} eventType
 */

var Eventful = function Eventful(eventProcessor) {
  this._$handlers = {};
  this._$eventProcessor = eventProcessor;
};

Eventful.prototype = {
  constructor: Eventful,

  /**
   * The handler can only be triggered once, then removed.
   *
   * @param {string} event The event name.
   * @param {string|Object} [query] Condition used on event filter.
   * @param {Function} handler The event handler.
   * @param {Object} context
   */
  one: function one(event, query, handler, context) {
    return _on(this, event, query, handler, context, true);
  },

  /**
   * Bind a handler.
   *
   * @param {string} event The event name.
   * @param {string|Object} [query] Condition used on event filter.
   * @param {Function} handler The event handler.
   * @param {Object} [context]
   */
  on: function on(event, query, handler, context) {
    return _on(this, event, query, handler, context, false);
  },

  /**
   * Whether any handler has bound.
   *
   * @param  {string}  event
   * @return {boolean}
   */
  isSilent: function isSilent(event) {
    var _h = this._$handlers;
    return !_h[event] || !_h[event].length;
  },

  /**
   * Unbind a event.
   *
   * @param {string} event The event name.
   * @param {Function} [handler] The event handler.
   */
  off: function off(event, handler) {
    var _h = this._$handlers;

    if (!event) {
      this._$handlers = {};
      return this;
    }

    if (handler) {
      if (_h[event]) {
        var newList = [];

        for (var i = 0, l = _h[event].length; i < l; i++) {
          if (_h[event][i].h !== handler) {
            newList.push(_h[event][i]);
          }
        }

        _h[event] = newList;
      }

      if (_h[event] && _h[event].length === 0) {
        delete _h[event];
      }
    } else {
      delete _h[event];
    }

    return this;
  },

  /**
   * Dispatch a event.
   *
   * @param {string} type The event name.
   */
  trigger: function trigger(type) {
    var _h = this._$handlers[type];
    var eventProcessor = this._$eventProcessor;

    if (_h) {
      var args = arguments;
      var argLen = args.length;

      if (argLen > 3) {
        args = arrySlice.call(args, 1);
      }

      var len = _h.length;

      for (var i = 0; i < len;) {
        var hItem = _h[i];

        if (eventProcessor && eventProcessor.filter && hItem.query != null && !eventProcessor.filter(type, hItem.query)) {
          i++;
          continue;
        } // Optimize advise from backbone


        switch (argLen) {
          case 1:
            hItem.h.call(hItem.ctx);
            break;

          case 2:
            hItem.h.call(hItem.ctx, args[1]);
            break;

          case 3:
            hItem.h.call(hItem.ctx, args[1], args[2]);
            break;

          default:
            // have more than 2 given arguments
            hItem.h.apply(hItem.ctx, args);
            break;
        }

        if (hItem.one) {
          _h.splice(i, 1);

          len--;
        } else {
          i++;
        }
      }
    }

    eventProcessor && eventProcessor.afterTrigger && eventProcessor.afterTrigger(type);
    return this;
  },

  /**
   * Dispatch a event with context, which is specified at the last parameter.
   *
   * @param {string} type The event name.
   */
  triggerWithContext: function triggerWithContext(type) {
    var _h = this._$handlers[type];
    var eventProcessor = this._$eventProcessor;

    if (_h) {
      var args = arguments;
      var argLen = args.length;

      if (argLen > 4) {
        args = arrySlice.call(args, 1, args.length - 1);
      }

      var ctx = args[args.length - 1];
      var len = _h.length;

      for (var i = 0; i < len;) {
        var hItem = _h[i];

        if (eventProcessor && eventProcessor.filter && hItem.query != null && !eventProcessor.filter(type, hItem.query)) {
          i++;
          continue;
        } // Optimize advise from backbone


        switch (argLen) {
          case 1:
            hItem.h.call(ctx);
            break;

          case 2:
            hItem.h.call(ctx, args[1]);
            break;

          case 3:
            hItem.h.call(ctx, args[1], args[2]);
            break;

          default:
            // have more than 2 given arguments
            hItem.h.apply(ctx, args);
            break;
        }

        if (hItem.one) {
          _h.splice(i, 1);

          len--;
        } else {
          i++;
        }
      }
    }

    eventProcessor && eventProcessor.afterTrigger && eventProcessor.afterTrigger(type);
    return this;
  }
};

function normalizeQuery(host, query) {
  var eventProcessor = host._$eventProcessor;

  if (query != null && eventProcessor && eventProcessor.normalizeQuery) {
    query = eventProcessor.normalizeQuery(query);
  }

  return query;
}

function _on(eventful, event, query, handler, context, isOnce) {
  var _h = eventful._$handlers;

  if (typeof query === 'function') {
    context = handler;
    handler = query;
    query = null;
  }

  if (!handler || !event) {
    return eventful;
  }

  query = normalizeQuery(eventful, query);

  if (!_h[event]) {
    _h[event] = [];
  }

  for (var i = 0; i < _h[event].length; i++) {
    if (_h[event][i].h === handler) {
      return eventful;
    }
  }

  var wrap = {
    h: handler,
    one: isOnce,
    query: query,
    ctx: context || eventful,
    // FIXME
    // Do not publish this feature util it is proved that it makes sense.
    callAtLast: handler.zrEventfulCallAtLast
  };
  var lastIndex = _h[event].length - 1;
  var lastWrap = _h[event][lastIndex];
  lastWrap && lastWrap.callAtLast ? _h[event].splice(lastIndex, 0, wrap) : _h[event].push(wrap);
  return eventful;
} // ----------------------
// The events in zrender
// ----------------------

/**
 * @event module:zrender/mixin/Eventful#onclick
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#onmouseover
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#onmouseout
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#onmousemove
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#onmousewheel
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#onmousedown
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#onmouseup
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#ondrag
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#ondragstart
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#ondragend
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#ondragenter
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#ondragleave
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#ondragover
 * @type {Function}
 * @default null
 */

/**
 * @event module:zrender/mixin/Eventful#ondrop
 * @type {Function}
 * @default null
 */


var _default = Eventful;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvbWl4aW4vRXZlbnRmdWwuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9taXhpbi9FdmVudGZ1bC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEV2ZW50IE1peGluXG4gKiBAbW9kdWxlIHpyZW5kZXIvbWl4aW4vRXZlbnRmdWxcbiAqIEBhdXRob3IgS2VuZXIgKEBLZW5lci3mnpfls7AsIGtlbmVyLmxpbmZlbmdAZ21haWwuY29tKVxuICogICAgICAgICBwaXNzYW5nIChodHRwczovL3d3dy5naXRodWIuY29tL3Bpc3NhbmcpXG4gKi9cbnZhciBhcnJ5U2xpY2UgPSBBcnJheS5wcm90b3R5cGUuc2xpY2U7XG4vKipcbiAqIEV2ZW50IGRpc3BhdGNoZXIuXG4gKlxuICogQGFsaWFzIG1vZHVsZTp6cmVuZGVyL21peGluL0V2ZW50ZnVsXG4gKiBAY29uc3RydWN0b3JcbiAqIEBwYXJhbSB7T2JqZWN0fSBbZXZlbnRQcm9jZXNzb3JdIFRoZSBvYmplY3QgZXZlbnRQcm9jZXNzb3IgaXMgdGhlIHNjb3BlIHdoZW5cbiAqICAgICAgICBgZXZlbnRQcm9jZXNzb3IueHh4YCBjYWxsZWQuXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBbZXZlbnRQcm9jZXNzb3Iubm9ybWFsaXplUXVlcnldXG4gKiAgICAgICAgcGFyYW06IHtzdHJpbmd8T2JqZWN0fSBSYXcgcXVlcnkuXG4gKiAgICAgICAgcmV0dXJuOiB7c3RyaW5nfE9iamVjdH0gTm9ybWFsaXplZCBxdWVyeS5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IFtldmVudFByb2Nlc3Nvci5maWx0ZXJdIEV2ZW50IHdpbGwgYmUgZGlzcGF0Y2hlZCBvbmx5XG4gKiAgICAgICAgaWYgaXQgcmV0dXJucyBgdHJ1ZWAuXG4gKiAgICAgICAgcGFyYW06IHtzdHJpbmd9IGV2ZW50VHlwZVxuICogICAgICAgIHBhcmFtOiB7c3RyaW5nfE9iamVjdH0gcXVlcnlcbiAqICAgICAgICByZXR1cm46IHtib29sZWFufVxuICogQHBhcmFtIHtGdW5jdGlvbn0gW2V2ZW50UHJvY2Vzc29yLmFmdGVyVHJpZ2dlcl0gQ2FsbCBhZnRlciBhbGwgaGFuZGxlcnMgY2FsbGVkLlxuICogICAgICAgIHBhcmFtOiB7c3RyaW5nfSBldmVudFR5cGVcbiAqL1xuXG52YXIgRXZlbnRmdWwgPSBmdW5jdGlvbiAoZXZlbnRQcm9jZXNzb3IpIHtcbiAgdGhpcy5fJGhhbmRsZXJzID0ge307XG4gIHRoaXMuXyRldmVudFByb2Nlc3NvciA9IGV2ZW50UHJvY2Vzc29yO1xufTtcblxuRXZlbnRmdWwucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogRXZlbnRmdWwsXG5cbiAgLyoqXG4gICAqIFRoZSBoYW5kbGVyIGNhbiBvbmx5IGJlIHRyaWdnZXJlZCBvbmNlLCB0aGVuIHJlbW92ZWQuXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBldmVudCBUaGUgZXZlbnQgbmFtZS5cbiAgICogQHBhcmFtIHtzdHJpbmd8T2JqZWN0fSBbcXVlcnldIENvbmRpdGlvbiB1c2VkIG9uIGV2ZW50IGZpbHRlci5cbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gaGFuZGxlciBUaGUgZXZlbnQgaGFuZGxlci5cbiAgICogQHBhcmFtIHtPYmplY3R9IGNvbnRleHRcbiAgICovXG4gIG9uZTogZnVuY3Rpb24gKGV2ZW50LCBxdWVyeSwgaGFuZGxlciwgY29udGV4dCkge1xuICAgIHJldHVybiBvbih0aGlzLCBldmVudCwgcXVlcnksIGhhbmRsZXIsIGNvbnRleHQsIHRydWUpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBCaW5kIGEgaGFuZGxlci5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IGV2ZW50IFRoZSBldmVudCBuYW1lLlxuICAgKiBAcGFyYW0ge3N0cmluZ3xPYmplY3R9IFtxdWVyeV0gQ29uZGl0aW9uIHVzZWQgb24gZXZlbnQgZmlsdGVyLlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBoYW5kbGVyIFRoZSBldmVudCBoYW5kbGVyLlxuICAgKiBAcGFyYW0ge09iamVjdH0gW2NvbnRleHRdXG4gICAqL1xuICBvbjogZnVuY3Rpb24gKGV2ZW50LCBxdWVyeSwgaGFuZGxlciwgY29udGV4dCkge1xuICAgIHJldHVybiBvbih0aGlzLCBldmVudCwgcXVlcnksIGhhbmRsZXIsIGNvbnRleHQsIGZhbHNlKTtcbiAgfSxcblxuICAvKipcbiAgICogV2hldGhlciBhbnkgaGFuZGxlciBoYXMgYm91bmQuXG4gICAqXG4gICAqIEBwYXJhbSAge3N0cmluZ30gIGV2ZW50XG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuICBpc1NpbGVudDogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgdmFyIF9oID0gdGhpcy5fJGhhbmRsZXJzO1xuICAgIHJldHVybiAhX2hbZXZlbnRdIHx8ICFfaFtldmVudF0ubGVuZ3RoO1xuICB9LFxuXG4gIC8qKlxuICAgKiBVbmJpbmQgYSBldmVudC5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IGV2ZW50IFRoZSBldmVudCBuYW1lLlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBbaGFuZGxlcl0gVGhlIGV2ZW50IGhhbmRsZXIuXG4gICAqL1xuICBvZmY6IGZ1bmN0aW9uIChldmVudCwgaGFuZGxlcikge1xuICAgIHZhciBfaCA9IHRoaXMuXyRoYW5kbGVycztcblxuICAgIGlmICghZXZlbnQpIHtcbiAgICAgIHRoaXMuXyRoYW5kbGVycyA9IHt9O1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgaWYgKGhhbmRsZXIpIHtcbiAgICAgIGlmIChfaFtldmVudF0pIHtcbiAgICAgICAgdmFyIG5ld0xpc3QgPSBbXTtcblxuICAgICAgICBmb3IgKHZhciBpID0gMCwgbCA9IF9oW2V2ZW50XS5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgICAgICBpZiAoX2hbZXZlbnRdW2ldLmggIT09IGhhbmRsZXIpIHtcbiAgICAgICAgICAgIG5ld0xpc3QucHVzaChfaFtldmVudF1baV0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIF9oW2V2ZW50XSA9IG5ld0xpc3Q7XG4gICAgICB9XG5cbiAgICAgIGlmIChfaFtldmVudF0gJiYgX2hbZXZlbnRdLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICBkZWxldGUgX2hbZXZlbnRdO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBkZWxldGUgX2hbZXZlbnRdO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzO1xuICB9LFxuXG4gIC8qKlxuICAgKiBEaXNwYXRjaCBhIGV2ZW50LlxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gdHlwZSBUaGUgZXZlbnQgbmFtZS5cbiAgICovXG4gIHRyaWdnZXI6IGZ1bmN0aW9uICh0eXBlKSB7XG4gICAgdmFyIF9oID0gdGhpcy5fJGhhbmRsZXJzW3R5cGVdO1xuICAgIHZhciBldmVudFByb2Nlc3NvciA9IHRoaXMuXyRldmVudFByb2Nlc3NvcjtcblxuICAgIGlmIChfaCkge1xuICAgICAgdmFyIGFyZ3MgPSBhcmd1bWVudHM7XG4gICAgICB2YXIgYXJnTGVuID0gYXJncy5sZW5ndGg7XG5cbiAgICAgIGlmIChhcmdMZW4gPiAzKSB7XG4gICAgICAgIGFyZ3MgPSBhcnJ5U2xpY2UuY2FsbChhcmdzLCAxKTtcbiAgICAgIH1cblxuICAgICAgdmFyIGxlbiA9IF9oLmxlbmd0aDtcblxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47KSB7XG4gICAgICAgIHZhciBoSXRlbSA9IF9oW2ldO1xuXG4gICAgICAgIGlmIChldmVudFByb2Nlc3NvciAmJiBldmVudFByb2Nlc3Nvci5maWx0ZXIgJiYgaEl0ZW0ucXVlcnkgIT0gbnVsbCAmJiAhZXZlbnRQcm9jZXNzb3IuZmlsdGVyKHR5cGUsIGhJdGVtLnF1ZXJ5KSkge1xuICAgICAgICAgIGkrKztcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfSAvLyBPcHRpbWl6ZSBhZHZpc2UgZnJvbSBiYWNrYm9uZVxuXG5cbiAgICAgICAgc3dpdGNoIChhcmdMZW4pIHtcbiAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICBoSXRlbS5oLmNhbGwoaEl0ZW0uY3R4KTtcbiAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgaEl0ZW0uaC5jYWxsKGhJdGVtLmN0eCwgYXJnc1sxXSk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgIGNhc2UgMzpcbiAgICAgICAgICAgIGhJdGVtLmguY2FsbChoSXRlbS5jdHgsIGFyZ3NbMV0sIGFyZ3NbMl0pO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgLy8gaGF2ZSBtb3JlIHRoYW4gMiBnaXZlbiBhcmd1bWVudHNcbiAgICAgICAgICAgIGhJdGVtLmguYXBwbHkoaEl0ZW0uY3R4LCBhcmdzKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGhJdGVtLm9uZSkge1xuICAgICAgICAgIF9oLnNwbGljZShpLCAxKTtcblxuICAgICAgICAgIGxlbi0tO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGkrKztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGV2ZW50UHJvY2Vzc29yICYmIGV2ZW50UHJvY2Vzc29yLmFmdGVyVHJpZ2dlciAmJiBldmVudFByb2Nlc3Nvci5hZnRlclRyaWdnZXIodHlwZSk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqIERpc3BhdGNoIGEgZXZlbnQgd2l0aCBjb250ZXh0LCB3aGljaCBpcyBzcGVjaWZpZWQgYXQgdGhlIGxhc3QgcGFyYW1ldGVyLlxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gdHlwZSBUaGUgZXZlbnQgbmFtZS5cbiAgICovXG4gIHRyaWdnZXJXaXRoQ29udGV4dDogZnVuY3Rpb24gKHR5cGUpIHtcbiAgICB2YXIgX2ggPSB0aGlzLl8kaGFuZGxlcnNbdHlwZV07XG4gICAgdmFyIGV2ZW50UHJvY2Vzc29yID0gdGhpcy5fJGV2ZW50UHJvY2Vzc29yO1xuXG4gICAgaWYgKF9oKSB7XG4gICAgICB2YXIgYXJncyA9IGFyZ3VtZW50cztcbiAgICAgIHZhciBhcmdMZW4gPSBhcmdzLmxlbmd0aDtcblxuICAgICAgaWYgKGFyZ0xlbiA+IDQpIHtcbiAgICAgICAgYXJncyA9IGFycnlTbGljZS5jYWxsKGFyZ3MsIDEsIGFyZ3MubGVuZ3RoIC0gMSk7XG4gICAgICB9XG5cbiAgICAgIHZhciBjdHggPSBhcmdzW2FyZ3MubGVuZ3RoIC0gMV07XG4gICAgICB2YXIgbGVuID0gX2gubGVuZ3RoO1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjspIHtcbiAgICAgICAgdmFyIGhJdGVtID0gX2hbaV07XG5cbiAgICAgICAgaWYgKGV2ZW50UHJvY2Vzc29yICYmIGV2ZW50UHJvY2Vzc29yLmZpbHRlciAmJiBoSXRlbS5xdWVyeSAhPSBudWxsICYmICFldmVudFByb2Nlc3Nvci5maWx0ZXIodHlwZSwgaEl0ZW0ucXVlcnkpKSB7XG4gICAgICAgICAgaSsrO1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9IC8vIE9wdGltaXplIGFkdmlzZSBmcm9tIGJhY2tib25lXG5cblxuICAgICAgICBzd2l0Y2ggKGFyZ0xlbikge1xuICAgICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgIGhJdGVtLmguY2FsbChjdHgpO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgICBoSXRlbS5oLmNhbGwoY3R4LCBhcmdzWzFdKTtcbiAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgY2FzZSAzOlxuICAgICAgICAgICAgaEl0ZW0uaC5jYWxsKGN0eCwgYXJnc1sxXSwgYXJnc1syXSk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAvLyBoYXZlIG1vcmUgdGhhbiAyIGdpdmVuIGFyZ3VtZW50c1xuICAgICAgICAgICAgaEl0ZW0uaC5hcHBseShjdHgsIGFyZ3MpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaEl0ZW0ub25lKSB7XG4gICAgICAgICAgX2guc3BsaWNlKGksIDEpO1xuXG4gICAgICAgICAgbGVuLS07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaSsrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgZXZlbnRQcm9jZXNzb3IgJiYgZXZlbnRQcm9jZXNzb3IuYWZ0ZXJUcmlnZ2VyICYmIGV2ZW50UHJvY2Vzc29yLmFmdGVyVHJpZ2dlcih0eXBlKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxufTtcblxuZnVuY3Rpb24gbm9ybWFsaXplUXVlcnkoaG9zdCwgcXVlcnkpIHtcbiAgdmFyIGV2ZW50UHJvY2Vzc29yID0gaG9zdC5fJGV2ZW50UHJvY2Vzc29yO1xuXG4gIGlmIChxdWVyeSAhPSBudWxsICYmIGV2ZW50UHJvY2Vzc29yICYmIGV2ZW50UHJvY2Vzc29yLm5vcm1hbGl6ZVF1ZXJ5KSB7XG4gICAgcXVlcnkgPSBldmVudFByb2Nlc3Nvci5ub3JtYWxpemVRdWVyeShxdWVyeSk7XG4gIH1cblxuICByZXR1cm4gcXVlcnk7XG59XG5cbmZ1bmN0aW9uIG9uKGV2ZW50ZnVsLCBldmVudCwgcXVlcnksIGhhbmRsZXIsIGNvbnRleHQsIGlzT25jZSkge1xuICB2YXIgX2ggPSBldmVudGZ1bC5fJGhhbmRsZXJzO1xuXG4gIGlmICh0eXBlb2YgcXVlcnkgPT09ICdmdW5jdGlvbicpIHtcbiAgICBjb250ZXh0ID0gaGFuZGxlcjtcbiAgICBoYW5kbGVyID0gcXVlcnk7XG4gICAgcXVlcnkgPSBudWxsO1xuICB9XG5cbiAgaWYgKCFoYW5kbGVyIHx8ICFldmVudCkge1xuICAgIHJldHVybiBldmVudGZ1bDtcbiAgfVxuXG4gIHF1ZXJ5ID0gbm9ybWFsaXplUXVlcnkoZXZlbnRmdWwsIHF1ZXJ5KTtcblxuICBpZiAoIV9oW2V2ZW50XSkge1xuICAgIF9oW2V2ZW50XSA9IFtdO1xuICB9XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBfaFtldmVudF0ubGVuZ3RoOyBpKyspIHtcbiAgICBpZiAoX2hbZXZlbnRdW2ldLmggPT09IGhhbmRsZXIpIHtcbiAgICAgIHJldHVybiBldmVudGZ1bDtcbiAgICB9XG4gIH1cblxuICB2YXIgd3JhcCA9IHtcbiAgICBoOiBoYW5kbGVyLFxuICAgIG9uZTogaXNPbmNlLFxuICAgIHF1ZXJ5OiBxdWVyeSxcbiAgICBjdHg6IGNvbnRleHQgfHwgZXZlbnRmdWwsXG4gICAgLy8gRklYTUVcbiAgICAvLyBEbyBub3QgcHVibGlzaCB0aGlzIGZlYXR1cmUgdXRpbCBpdCBpcyBwcm92ZWQgdGhhdCBpdCBtYWtlcyBzZW5zZS5cbiAgICBjYWxsQXRMYXN0OiBoYW5kbGVyLnpyRXZlbnRmdWxDYWxsQXRMYXN0XG4gIH07XG4gIHZhciBsYXN0SW5kZXggPSBfaFtldmVudF0ubGVuZ3RoIC0gMTtcbiAgdmFyIGxhc3RXcmFwID0gX2hbZXZlbnRdW2xhc3RJbmRleF07XG4gIGxhc3RXcmFwICYmIGxhc3RXcmFwLmNhbGxBdExhc3QgPyBfaFtldmVudF0uc3BsaWNlKGxhc3RJbmRleCwgMCwgd3JhcCkgOiBfaFtldmVudF0ucHVzaCh3cmFwKTtcbiAgcmV0dXJuIGV2ZW50ZnVsO1xufSAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4vLyBUaGUgZXZlbnRzIGluIHpyZW5kZXJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblxuLyoqXG4gKiBAZXZlbnQgbW9kdWxlOnpyZW5kZXIvbWl4aW4vRXZlbnRmdWwjb25jbGlja1xuICogQHR5cGUge0Z1bmN0aW9ufVxuICogQGRlZmF1bHQgbnVsbFxuICovXG5cbi8qKlxuICogQGV2ZW50IG1vZHVsZTp6cmVuZGVyL21peGluL0V2ZW50ZnVsI29ubW91c2VvdmVyXG4gKiBAdHlwZSB7RnVuY3Rpb259XG4gKiBAZGVmYXVsdCBudWxsXG4gKi9cblxuLyoqXG4gKiBAZXZlbnQgbW9kdWxlOnpyZW5kZXIvbWl4aW4vRXZlbnRmdWwjb25tb3VzZW91dFxuICogQHR5cGUge0Z1bmN0aW9ufVxuICogQGRlZmF1bHQgbnVsbFxuICovXG5cbi8qKlxuICogQGV2ZW50IG1vZHVsZTp6cmVuZGVyL21peGluL0V2ZW50ZnVsI29ubW91c2Vtb3ZlXG4gKiBAdHlwZSB7RnVuY3Rpb259XG4gKiBAZGVmYXVsdCBudWxsXG4gKi9cblxuLyoqXG4gKiBAZXZlbnQgbW9kdWxlOnpyZW5kZXIvbWl4aW4vRXZlbnRmdWwjb25tb3VzZXdoZWVsXG4gKiBAdHlwZSB7RnVuY3Rpb259XG4gKiBAZGVmYXVsdCBudWxsXG4gKi9cblxuLyoqXG4gKiBAZXZlbnQgbW9kdWxlOnpyZW5kZXIvbWl4aW4vRXZlbnRmdWwjb25tb3VzZWRvd25cbiAqIEB0eXBlIHtGdW5jdGlvbn1cbiAqIEBkZWZhdWx0IG51bGxcbiAqL1xuXG4vKipcbiAqIEBldmVudCBtb2R1bGU6enJlbmRlci9taXhpbi9FdmVudGZ1bCNvbm1vdXNldXBcbiAqIEB0eXBlIHtGdW5jdGlvbn1cbiAqIEBkZWZhdWx0IG51bGxcbiAqL1xuXG4vKipcbiAqIEBldmVudCBtb2R1bGU6enJlbmRlci9taXhpbi9FdmVudGZ1bCNvbmRyYWdcbiAqIEB0eXBlIHtGdW5jdGlvbn1cbiAqIEBkZWZhdWx0IG51bGxcbiAqL1xuXG4vKipcbiAqIEBldmVudCBtb2R1bGU6enJlbmRlci9taXhpbi9FdmVudGZ1bCNvbmRyYWdzdGFydFxuICogQHR5cGUge0Z1bmN0aW9ufVxuICogQGRlZmF1bHQgbnVsbFxuICovXG5cbi8qKlxuICogQGV2ZW50IG1vZHVsZTp6cmVuZGVyL21peGluL0V2ZW50ZnVsI29uZHJhZ2VuZFxuICogQHR5cGUge0Z1bmN0aW9ufVxuICogQGRlZmF1bHQgbnVsbFxuICovXG5cbi8qKlxuICogQGV2ZW50IG1vZHVsZTp6cmVuZGVyL21peGluL0V2ZW50ZnVsI29uZHJhZ2VudGVyXG4gKiBAdHlwZSB7RnVuY3Rpb259XG4gKiBAZGVmYXVsdCBudWxsXG4gKi9cblxuLyoqXG4gKiBAZXZlbnQgbW9kdWxlOnpyZW5kZXIvbWl4aW4vRXZlbnRmdWwjb25kcmFnbGVhdmVcbiAqIEB0eXBlIHtGdW5jdGlvbn1cbiAqIEBkZWZhdWx0IG51bGxcbiAqL1xuXG4vKipcbiAqIEBldmVudCBtb2R1bGU6enJlbmRlci9taXhpbi9FdmVudGZ1bCNvbmRyYWdvdmVyXG4gKiBAdHlwZSB7RnVuY3Rpb259XG4gKiBAZGVmYXVsdCBudWxsXG4gKi9cblxuLyoqXG4gKiBAZXZlbnQgbW9kdWxlOnpyZW5kZXIvbWl4aW4vRXZlbnRmdWwjb25kcm9wXG4gKiBAdHlwZSB7RnVuY3Rpb259XG4gKiBAZGVmYXVsdCBudWxsXG4gKi9cblxuXG52YXIgX2RlZmF1bHQgPSBFdmVudGZ1bDtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7O0FBTUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFDQTtBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhCQTtBQUNBO0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBcE1BO0FBQ0E7QUFzTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7Ozs7OztBQU1BOzs7Ozs7QUFNQTs7Ozs7O0FBTUE7Ozs7OztBQU1BOzs7Ozs7QUFNQTs7Ozs7O0FBTUE7Ozs7OztBQU1BOzs7Ozs7QUFNQTs7Ozs7O0FBTUE7Ozs7OztBQU1BOzs7Ozs7QUFNQTs7Ozs7O0FBTUE7Ozs7Ozs7QUFPQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/mixin/Eventful.js
