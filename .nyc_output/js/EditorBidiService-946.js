/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule EditorBidiService
 * @format
 * 
 */


var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var UnicodeBidiService = __webpack_require__(/*! fbjs/lib/UnicodeBidiService */ "./node_modules/fbjs/lib/UnicodeBidiService.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");

var OrderedMap = Immutable.OrderedMap;
var bidiService;
var EditorBidiService = {
  getDirectionMap: function getDirectionMap(content, prevBidiMap) {
    if (!bidiService) {
      bidiService = new UnicodeBidiService();
    } else {
      bidiService.reset();
    }

    var blockMap = content.getBlockMap();
    var nextBidi = blockMap.valueSeq().map(function (block) {
      return nullthrows(bidiService).getDirection(block.getText());
    });
    var bidiMap = OrderedMap(blockMap.keySeq().zip(nextBidi));

    if (prevBidiMap != null && Immutable.is(prevBidiMap, bidiMap)) {
      return prevBidiMap;
    }

    return bidiMap;
  }
};
module.exports = EditorBidiService;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0VkaXRvckJpZGlTZXJ2aWNlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0VkaXRvckJpZGlTZXJ2aWNlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgRWRpdG9yQmlkaVNlcnZpY2VcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xudmFyIFVuaWNvZGVCaWRpU2VydmljZSA9IHJlcXVpcmUoJ2ZianMvbGliL1VuaWNvZGVCaWRpU2VydmljZScpO1xuXG52YXIgbnVsbHRocm93cyA9IHJlcXVpcmUoJ2ZianMvbGliL251bGx0aHJvd3MnKTtcblxudmFyIE9yZGVyZWRNYXAgPSBJbW11dGFibGUuT3JkZXJlZE1hcDtcblxuXG52YXIgYmlkaVNlcnZpY2U7XG5cbnZhciBFZGl0b3JCaWRpU2VydmljZSA9IHtcbiAgZ2V0RGlyZWN0aW9uTWFwOiBmdW5jdGlvbiBnZXREaXJlY3Rpb25NYXAoY29udGVudCwgcHJldkJpZGlNYXApIHtcbiAgICBpZiAoIWJpZGlTZXJ2aWNlKSB7XG4gICAgICBiaWRpU2VydmljZSA9IG5ldyBVbmljb2RlQmlkaVNlcnZpY2UoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgYmlkaVNlcnZpY2UucmVzZXQoKTtcbiAgICB9XG5cbiAgICB2YXIgYmxvY2tNYXAgPSBjb250ZW50LmdldEJsb2NrTWFwKCk7XG4gICAgdmFyIG5leHRCaWRpID0gYmxvY2tNYXAudmFsdWVTZXEoKS5tYXAoZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgICByZXR1cm4gbnVsbHRocm93cyhiaWRpU2VydmljZSkuZ2V0RGlyZWN0aW9uKGJsb2NrLmdldFRleHQoKSk7XG4gICAgfSk7XG4gICAgdmFyIGJpZGlNYXAgPSBPcmRlcmVkTWFwKGJsb2NrTWFwLmtleVNlcSgpLnppcChuZXh0QmlkaSkpO1xuXG4gICAgaWYgKHByZXZCaWRpTWFwICE9IG51bGwgJiYgSW1tdXRhYmxlLmlzKHByZXZCaWRpTWFwLCBiaWRpTWFwKSkge1xuICAgICAgcmV0dXJuIHByZXZCaWRpTWFwO1xuICAgIH1cblxuICAgIHJldHVybiBiaWRpTWFwO1xuICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IEVkaXRvckJpZGlTZXJ2aWNlOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQW5CQTtBQXNCQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/EditorBidiService.js
