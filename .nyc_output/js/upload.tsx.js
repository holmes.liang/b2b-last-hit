__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _desk_ekyc_identify__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk/ekyc/identify */ "./src/app/desk/ekyc/identify.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_utils_image_utils__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @common/utils/image-utils */ "./src/common/utils/image-utils.tsx");
/* harmony import */ var _desk_ekyc_manual_processing_share_share_ekyc__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk/ekyc/manual-processing/share/share-ekyc */ "./src/app/desk/ekyc/manual-processing/share/share-ekyc.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/ekyc/upload.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  .take-photo {\n    display: flex;\n    margin: 0 0 10px 0;\n    i.iconfont {\n      padding-right: 10px;\n      font-size: 18px;\n    }\n    .ant-upload-select {\n      &:first-child {\n        margin-right: 20px;\n      }\n    }\n  }\n  .show-image, .ant-spin-nested-loading {\n    width: 245px;\n    img {\n      width: 100%;\n    }\n  }\n  .show-image-before {\n    margin: 20px 0;\n  }\n  .show-image {\n    margin: 15px 0;\n    border: 1px solid #d9d9d9;\n    border-radius: 4px;\n    padding: 5px;\n  }\n  .bottom-btn {\n    display: flex;\n    margin-top: 20px;\n    justify-content: space-between;\n    .bottom-right {\n      .ant-btn {\n        margin-left: 10px;\n        &:first-child {\n          margin-left: 0;\n        }\n      }\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}









function getBase64(_x, _x2, _x3, _x4) {
  return _getBase.apply(this, arguments);
}

function _getBase() {
  _getBase = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_8__["default"])(
  /*#__PURE__*/
  _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(img, _this, type, callback) {
    var reader;
    return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            reader = new FileReader();
            reader.addEventListener("load", function (e) {
              _this.setState({
                uploadUrlBase64: e.target.result
              });

              if (type === "ID_CARD") {
                _this.uploadIdCard(e.target.result, callback, reader.result);
              } else {
                _this.uploadPassport(e.target.result, callback, reader.result);
              } // callback(reader.result);

            });
            reader.readAsDataURL(img);

          case 3:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _getBase.apply(this, arguments);
}

var UploadImg =
/*#__PURE__*/
function (_React$Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(UploadImg, _React$Component);

  function UploadImg() {
    var _getPrototypeOf2;

    var _this2;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, UploadImg);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(UploadImg)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this2.videoRef = {};
    _this2.state = {
      loading: false,
      imageUrl: "",
      uploadUrlBase64: "",
      isShowImage: false,
      infoData: {},
      isNextDisabled: true
    };

    _this2.handleChange =
    /*#__PURE__*/
    function () {
      var _ref = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_8__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(info, type) {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                // if (info.file.status === "uploading") {
                _this2.setState({
                  loading: true,
                  isShowImage: true
                }); //   return;
                // }
                // if (info.file.status === "done") {
                // Get this url from response in real world.


                _context.t0 = getBase64;
                _context.next = 4;
                return _common_utils_image_utils__WEBPACK_IMPORTED_MODULE_14__["default"].compress(info.file);

              case 4:
                _context.t1 = _context.sent;
                _context.t2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this2);
                _context.t3 = type;

                _context.t4 = function (imageUrl) {
                  return _this2.setState({
                    imageUrl: imageUrl,
                    loading: false
                  });
                };

                (0, _context.t0)(_context.t1, _context.t2, _context.t3, _context.t4);

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x5, _x6) {
        return _ref.apply(this, arguments);
      };
    }();

    _this2.handleCancel = function () {
      _this2.videoRef.stopTakePhoto();

      _this2.videoRef.recorder = "";

      _this2.props.onCancel(_this2.state.infoData);
    };

    _this2.getVideoRef = function (ref) {
      _this2.videoRef = ref;
    };

    return _this2;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(UploadImg, [{
    key: "beforeUpload",
    value: function beforeUpload(file) {
      var isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";

      if (!isJpgOrPng) {
        antd__WEBPACK_IMPORTED_MODULE_11__["message"].error("You can only upload JPG/PNG file!");
      }

      var isLt2M = file.size / 1024 / 1024 < 3;

      if (!isLt2M) {
        antd__WEBPACK_IMPORTED_MODULE_11__["message"].error("Image must smaller than 3MB!");
      }

      return isJpgOrPng && isLt2M;
    }
  }, {
    key: "postData",
    value: function postData(imageBase64) {
      var model = this.props.model;
      return {
        bizId: lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, "bizId"),
        bizType: lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, "bizType"),
        itntCode: lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, "itntCode"),
        imageBase64: imageBase64
      };
    }
  }, {
    key: "postHeader",
    value: function postHeader() {
      var authKey = _common__WEBPACK_IMPORTED_MODULE_13__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_13__["Consts"].AUTH_KEY);
      return {
        headers: {
          "recaptcha-token": authKey
        }
      };
    }
  }, {
    key: "setResponse",
    value: function setResponse(res) {
      var respData = (res.body || {}).respData || {};
      this.setState({
        infoData: respData
      });
      this.videoRef.setState({
        errMsg: ""
      });
    }
  }, {
    key: "uploadIdCard",
    value: function uploadIdCard(imageBase64, callBack, result) {
      var _this3 = this;

      _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].post("/kyc/idcard", this.postData(imageBase64), this.postHeader()).then(function (res) {
        callBack(result);

        _this3.setResponse(res);
      }).catch(function () {
        _this3.setState({
          loading: false,
          isShowImage: false
        });
      });
    }
  }, {
    key: "uploadPassport",
    value: function uploadPassport(imageBase64, callBack, result) {
      var _this4 = this;

      _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].post("/kyc/passport", this.postData(imageBase64), this.postHeader()).then(function (res) {
        callBack(result);

        _this4.setResponse(res);
      }).catch(function () {
        _this4.setState({
          loading: false,
          isShowImage: false
        });
      });
    }
  }, {
    key: "skip",
    value: function skip() {
      var _this5 = this;

      _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].post("/kyc/skip", this.props.model, {
        loading: true
      }).then(function (res) {
        var respData = (res.body || {}).respData || {};

        _this5.handleCancel();
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this6 = this;

      var props = {
        listType: "picture",
        defaultFileList: [],
        showUploadList: false,
        beforeUpload: this.beforeUpload,
        customRequest: function customRequest(info) {
          _this6.handleChange(info, "ID_CARD");
        } // onChange: (info: any) => this.handleChange(info, "ID_CARD"),

      };
      var props2 = {
        listType: "picture",
        defaultFileList: [],
        className: "upload-list-inline",
        showUploadList: false,
        customRequest: function customRequest(info) {
          _this6.handleChange(info, "PASSPORT");
        },
        beforeUpload: this.beforeUpload // onChange: (info: any) => this.handleChange(info, "PASSPORT"),

      };
      var antIcon = _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Icon"], {
        type: "loading",
        style: {
          fontSize: 24
        },
        spin: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 179
        },
        __self: this
      });
      var _this$state = this.state,
          imageUrl = _this$state.imageUrl,
          isShowImage = _this$state.isShowImage,
          loading = _this$state.loading,
          isNextDisabled = _this$state.isNextDisabled;
      var model = this.props.model;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Modal"], {
        title: "IDENTITY VALIDATION",
        width: _common__WEBPACK_IMPORTED_MODULE_13__["Utils"].getIsMobile() ? "100%" : "50%",
        visible: true,
        centered: true,
        onCancel: this.handleCancel,
        footer: null,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 183
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(Style, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Row"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 192
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 193
        },
        __self: this
      }, "Step 1"), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        span: 18,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 194
        },
        __self: this
      }, "Take photo of your ID or passport")), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Row"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 196
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 197
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        span: 18,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 198
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Spin"], {
        spinning: loading,
        indicator: antIcon,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 199
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: isShowImage ? "show-image" : "show-image-before",
        style: {
          height: loading ? "188px" : "auto"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 200
        },
        __self: this
      }, imageUrl && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("img", {
        src: imageUrl,
        alt: "avatar",
        style: {
          width: "100%"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 202
        },
        __self: this
      }))))), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Row"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 207
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 208
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        span: 18,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 209
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "take-photo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 210
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Upload"], Object.assign({}, props, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 211
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Button"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 212
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "iconfont icon-photo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 213
        },
        __self: this
      }), " ID Card")), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 216
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 217
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Upload"], Object.assign({}, props2, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 218
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Button"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 219
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "iconfont icon-photo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 220
        },
        __self: this
      }), " Passport"))))), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_ekyc_identify__WEBPACK_IMPORTED_MODULE_12__["default"], {
        model: this.props.model,
        ref: this.getVideoRef,
        successCallback: function successCallback() {
          _this6.setState({
            isNextDisabled: false
          });
        },
        uploadUrlBase64: this.state.uploadUrlBase64,
        getUrlBase64: function getUrlBase64(videoBase64) {},
        __source: {
          fileName: _jsxFileName,
          lineNumber: 226
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "bottom-btn",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 236
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_ekyc_manual_processing_share_share_ekyc__WEBPACK_IMPORTED_MODULE_15__["default"], {
        model: model,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 237
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "bottom-right",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 238
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Button"], {
        type: "primary",
        disabled: !isNextDisabled,
        onClick: function onClick() {
          _this6.skip();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 239
        },
        __self: this
      }, "Skip")))));
    }
  }]);

  return UploadImg;
}(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Component);

var Style = _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject());
/* harmony default export */ __webpack_exports__["default"] = (UploadImg);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svZWt5Yy91cGxvYWQudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svZWt5Yy91cGxvYWQudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IEJ1dHRvbiwgQ29sLCBJY29uLCBtZXNzYWdlLCBNb2RhbCwgUm93LCBTcGluLCBVcGxvYWQgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IElkZW50aWZ5IGZyb20gXCJAZGVzay9la3ljL2lkZW50aWZ5XCI7XG5pbXBvcnQgeyBBamF4LCBDb25zdHMsIFN0b3JhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCBJbWFnZVV0aWxzIGZyb20gXCJAY29tbW9uL3V0aWxzL2ltYWdlLXV0aWxzXCI7XG5pbXBvcnQgU2hhcmUgZnJvbSBcIkBkZXNrL2VreWMvbWFudWFsLXByb2Nlc3Npbmcvc2hhcmUvc2hhcmUtZWt5Y1wiO1xuXG5hc3luYyBmdW5jdGlvbiBnZXRCYXNlNjQoaW1nOiBhbnksIF90aGlzOiBhbnksIHR5cGU6IHN0cmluZywgY2FsbGJhY2s6IGFueSkge1xuICBjb25zdCByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xuXG4gIHJlYWRlci5hZGRFdmVudExpc3RlbmVyKFwibG9hZFwiLCAoZTogYW55KSA9PiB7XG4gICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHVwbG9hZFVybEJhc2U2NDogZS50YXJnZXQucmVzdWx0LFxuICAgICAgfSk7XG4gICAgICBpZiAodHlwZSA9PT0gXCJJRF9DQVJEXCIpIHtcbiAgICAgICAgX3RoaXMudXBsb2FkSWRDYXJkKGUudGFyZ2V0LnJlc3VsdCwgY2FsbGJhY2ssIHJlYWRlci5yZXN1bHQpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgX3RoaXMudXBsb2FkUGFzc3BvcnQoZS50YXJnZXQucmVzdWx0LCBjYWxsYmFjaywgcmVhZGVyLnJlc3VsdCk7XG4gICAgICB9XG4gICAgICAvLyBjYWxsYmFjayhyZWFkZXIucmVzdWx0KTtcbiAgICB9LFxuICApO1xuICByZWFkZXIucmVhZEFzRGF0YVVSTChpbWcpO1xufVxuXG5cbnR5cGUgSVByb3BzID0ge1xuICBvbkNhbmNlbDogYW55O1xuICBtb2RlbDogYW55O1xufTtcblxudHlwZSBJU3RhdGUgPSB7XG4gIGxvYWRpbmc6IGJvb2xlYW4sXG4gIGltYWdlVXJsOiBzdHJpbmcsXG4gIHVwbG9hZFVybEJhc2U2NDogc3RyaW5nLFxuICBpc1Nob3dJbWFnZTogYm9vbGVhbixcbiAgaW5mb0RhdGE6IGFueTtcbiAgaXNOZXh0RGlzYWJsZWQ6IGJvb2xlYW47XG59O1xuXG5jbGFzcyBVcGxvYWRJbWcgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQ8SVByb3BzLCBJU3RhdGU+IHtcbiAgdmlkZW9SZWY6IGFueSA9IHt9O1xuICBzdGF0ZSA9IHtcbiAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICBpbWFnZVVybDogXCJcIixcbiAgICB1cGxvYWRVcmxCYXNlNjQ6IFwiXCIsXG4gICAgaXNTaG93SW1hZ2U6IGZhbHNlLFxuICAgIGluZm9EYXRhOiB7fSxcbiAgICBpc05leHREaXNhYmxlZDogdHJ1ZSxcbiAgfTtcblxuICBiZWZvcmVVcGxvYWQoZmlsZTogYW55KSB7XG4gICAgY29uc3QgaXNKcGdPclBuZyA9IGZpbGUudHlwZSA9PT0gXCJpbWFnZS9qcGVnXCIgfHwgZmlsZS50eXBlID09PSBcImltYWdlL3BuZ1wiO1xuICAgIGlmICghaXNKcGdPclBuZykge1xuICAgICAgbWVzc2FnZS5lcnJvcihcIllvdSBjYW4gb25seSB1cGxvYWQgSlBHL1BORyBmaWxlIVwiKTtcbiAgICB9XG4gICAgY29uc3QgaXNMdDJNID0gZmlsZS5zaXplIC8gMTAyNCAvIDEwMjQgPCAzO1xuICAgIGlmICghaXNMdDJNKSB7XG4gICAgICBtZXNzYWdlLmVycm9yKFwiSW1hZ2UgbXVzdCBzbWFsbGVyIHRoYW4gM01CIVwiKTtcbiAgICB9XG4gICAgcmV0dXJuIGlzSnBnT3JQbmcgJiYgaXNMdDJNO1xuICB9XG5cbiAgaGFuZGxlQ2hhbmdlID0gYXN5bmMgKGluZm86IGFueSwgdHlwZTogc3RyaW5nKSA9PiB7XG4gICAgLy8gaWYgKGluZm8uZmlsZS5zdGF0dXMgPT09IFwidXBsb2FkaW5nXCIpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgbG9hZGluZzogdHJ1ZSwgaXNTaG93SW1hZ2U6IHRydWUgfSk7XG4gICAgLy8gICByZXR1cm47XG4gICAgLy8gfVxuICAgIC8vIGlmIChpbmZvLmZpbGUuc3RhdHVzID09PSBcImRvbmVcIikge1xuICAgIC8vIEdldCB0aGlzIHVybCBmcm9tIHJlc3BvbnNlIGluIHJlYWwgd29ybGQuXG4gICAgZ2V0QmFzZTY0KGF3YWl0IEltYWdlVXRpbHMuY29tcHJlc3MoaW5mby5maWxlKSwgdGhpcywgdHlwZSwgKGltYWdlVXJsOiBhbnkpID0+XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgaW1hZ2VVcmwsXG4gICAgICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgICAgfSksXG4gICAgKTtcbiAgICAvLyB9XG4gIH07XG5cbiAgaGFuZGxlQ2FuY2VsID0gKCkgPT4ge1xuICAgIHRoaXMudmlkZW9SZWYuc3RvcFRha2VQaG90bygpO1xuICAgIHRoaXMudmlkZW9SZWYucmVjb3JkZXIgPSBcIlwiO1xuICAgIHRoaXMucHJvcHMub25DYW5jZWwodGhpcy5zdGF0ZS5pbmZvRGF0YSk7XG4gIH07XG5cbiAgcG9zdERhdGEoaW1hZ2VCYXNlNjQ6IGFueSkge1xuICAgIGNvbnN0IHsgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIHtcbiAgICAgIGJpeklkOiBfLmdldChtb2RlbCwgXCJiaXpJZFwiKSxcbiAgICAgIGJpelR5cGU6IF8uZ2V0KG1vZGVsLCBcImJpelR5cGVcIiksXG4gICAgICBpdG50Q29kZTogXy5nZXQobW9kZWwsIFwiaXRudENvZGVcIiksXG4gICAgICBpbWFnZUJhc2U2NCxcbiAgICB9O1xuICB9XG5cbiAgcG9zdEhlYWRlcigpIHtcbiAgICBjb25zdCBhdXRoS2V5ID0gU3RvcmFnZS5BdXRoLnNlc3Npb24oKS5nZXQoQ29uc3RzLkFVVEhfS0VZKTtcbiAgICByZXR1cm4ge1xuICAgICAgaGVhZGVyczoge1xuICAgICAgICBcInJlY2FwdGNoYS10b2tlblwiOiBhdXRoS2V5LFxuICAgICAgfSxcbiAgICB9O1xuICB9XG5cbiAgc2V0UmVzcG9uc2UocmVzOiBhbnkpIHtcbiAgICBjb25zdCByZXNwRGF0YSA9IChyZXMuYm9keSB8fCB7fSkucmVzcERhdGEgfHwge307XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBpbmZvRGF0YTogcmVzcERhdGEsXG4gICAgfSk7XG4gICAgdGhpcy52aWRlb1JlZi5zZXRTdGF0ZSh7XG4gICAgICBlcnJNc2c6IFwiXCIsXG4gICAgfSk7XG4gIH1cblxuICB1cGxvYWRJZENhcmQoaW1hZ2VCYXNlNjQ6IGFueSwgY2FsbEJhY2s6IGFueSwgcmVzdWx0OiBhbnkpIHtcbiAgICBBamF4LnBvc3QoYC9reWMvaWRjYXJkYCxcbiAgICAgIHRoaXMucG9zdERhdGEoaW1hZ2VCYXNlNjQpLFxuICAgICAgdGhpcy5wb3N0SGVhZGVyKCkpLnRoZW4oKHJlczogYW55KSA9PiB7XG4gICAgICBjYWxsQmFjayhyZXN1bHQpO1xuICAgICAgdGhpcy5zZXRSZXNwb25zZShyZXMpO1xuICAgIH0pLmNhdGNoKCgpID0+IHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgICAgaXNTaG93SW1hZ2U6IGZhbHNlLFxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICB1cGxvYWRQYXNzcG9ydChpbWFnZUJhc2U2NDogYW55LCBjYWxsQmFjazogYW55LCByZXN1bHQ6IGFueSkge1xuICAgIEFqYXgucG9zdChgL2t5Yy9wYXNzcG9ydGAsXG4gICAgICB0aGlzLnBvc3REYXRhKGltYWdlQmFzZTY0KSxcbiAgICAgIHRoaXMucG9zdEhlYWRlcigpKS50aGVuKChyZXM6IGFueSkgPT4ge1xuICAgICAgY2FsbEJhY2socmVzdWx0KTtcbiAgICAgIHRoaXMuc2V0UmVzcG9uc2UocmVzKTtcbiAgICB9KS5jYXRjaCgoKSA9PiB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICAgIGlzU2hvd0ltYWdlOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgZ2V0VmlkZW9SZWYgPSAocmVmOiBhbnkpID0+IHtcbiAgICB0aGlzLnZpZGVvUmVmID0gcmVmO1xuICB9O1xuXG4gIHNraXAoKSB7XG4gICAgQWpheC5wb3N0KGAva3ljL3NraXBgLCB0aGlzLnByb3BzLm1vZGVsLCB7IGxvYWRpbmc6IHRydWUgfSlcbiAgICAgIC50aGVuKChyZXM6IGFueSkgPT4ge1xuICAgICAgICBjb25zdCByZXNwRGF0YSA9IChyZXMuYm9keSB8fCB7fSkucmVzcERhdGEgfHwge307XG4gICAgICAgIHRoaXMuaGFuZGxlQ2FuY2VsKCk7XG4gICAgICB9KTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBwcm9wczogYW55ID0ge1xuICAgICAgbGlzdFR5cGU6IFwicGljdHVyZVwiLFxuICAgICAgZGVmYXVsdEZpbGVMaXN0OiBbXSxcbiAgICAgIHNob3dVcGxvYWRMaXN0OiBmYWxzZSxcbiAgICAgIGJlZm9yZVVwbG9hZDogdGhpcy5iZWZvcmVVcGxvYWQsXG4gICAgICBjdXN0b21SZXF1ZXN0OiAoaW5mbzogYW55KSA9PiB7XG4gICAgICAgIHRoaXMuaGFuZGxlQ2hhbmdlKGluZm8sIFwiSURfQ0FSRFwiKTtcbiAgICAgIH0sXG4gICAgICAvLyBvbkNoYW5nZTogKGluZm86IGFueSkgPT4gdGhpcy5oYW5kbGVDaGFuZ2UoaW5mbywgXCJJRF9DQVJEXCIpLFxuICAgIH07XG5cbiAgICBjb25zdCBwcm9wczI6IGFueSA9IHtcbiAgICAgIGxpc3RUeXBlOiBcInBpY3R1cmVcIixcbiAgICAgIGRlZmF1bHRGaWxlTGlzdDogW10sXG4gICAgICBjbGFzc05hbWU6IFwidXBsb2FkLWxpc3QtaW5saW5lXCIsXG4gICAgICBzaG93VXBsb2FkTGlzdDogZmFsc2UsXG4gICAgICBjdXN0b21SZXF1ZXN0OiAoaW5mbzogYW55KSA9PiB7XG4gICAgICAgIHRoaXMuaGFuZGxlQ2hhbmdlKGluZm8sIFwiUEFTU1BPUlRcIik7XG4gICAgICB9LFxuICAgICAgYmVmb3JlVXBsb2FkOiB0aGlzLmJlZm9yZVVwbG9hZCxcbiAgICAgIC8vIG9uQ2hhbmdlOiAoaW5mbzogYW55KSA9PiB0aGlzLmhhbmRsZUNoYW5nZShpbmZvLCBcIlBBU1NQT1JUXCIpLFxuICAgIH07XG4gICAgY29uc3QgYW50SWNvbiA9IDxJY29uIHR5cGU9XCJsb2FkaW5nXCIgc3R5bGU9e3sgZm9udFNpemU6IDI0IH19IHNwaW4vPjtcbiAgICBjb25zdCB7IGltYWdlVXJsLCBpc1Nob3dJbWFnZSwgbG9hZGluZywgaXNOZXh0RGlzYWJsZWQgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gKFxuICAgICAgPE1vZGFsXG4gICAgICAgIHRpdGxlPVwiSURFTlRJVFkgVkFMSURBVElPTlwiXG4gICAgICAgIHdpZHRoPXtVdGlscy5nZXRJc01vYmlsZSgpID8gXCIxMDAlXCIgOiBcIjUwJVwifVxuICAgICAgICB2aXNpYmxlXG4gICAgICAgIGNlbnRlcmVkPXt0cnVlfVxuICAgICAgICBvbkNhbmNlbD17dGhpcy5oYW5kbGVDYW5jZWx9XG4gICAgICAgIGZvb3Rlcj17bnVsbH1cbiAgICAgID5cbiAgICAgICAgPFN0eWxlPlxuICAgICAgICAgIDxSb3c+XG4gICAgICAgICAgICA8Q29sIHNwYW49ezZ9PlN0ZXAgMTwvQ29sPlxuICAgICAgICAgICAgPENvbCBzcGFuPXsxOH0+VGFrZSBwaG90byBvZiB5b3VyIElEIG9yIHBhc3Nwb3J0PC9Db2w+XG4gICAgICAgICAgPC9Sb3c+XG4gICAgICAgICAgPFJvdz5cbiAgICAgICAgICAgIDxDb2wgc3Bhbj17Nn0vPlxuICAgICAgICAgICAgPENvbCBzcGFuPXsxOH0+XG4gICAgICAgICAgICAgIDxTcGluIHNwaW5uaW5nPXtsb2FkaW5nfSBpbmRpY2F0b3I9e2FudEljb259PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtpc1Nob3dJbWFnZSA/IFwic2hvdy1pbWFnZVwiIDogXCJzaG93LWltYWdlLWJlZm9yZVwifVxuICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgaGVpZ2h0OiBsb2FkaW5nID8gXCIxODhweFwiIDogXCJhdXRvXCIgfX0+XG4gICAgICAgICAgICAgICAgICB7aW1hZ2VVcmwgJiYgPGltZyBzcmM9e2ltYWdlVXJsfSBhbHQ9XCJhdmF0YXJcIiBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIgfX0vPn1cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9TcGluPlxuICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgPC9Sb3c+XG4gICAgICAgICAgPFJvdz5cbiAgICAgICAgICAgIDxDb2wgc3Bhbj17Nn0vPlxuICAgICAgICAgICAgPENvbCBzcGFuPXsxOH0+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtcInRha2UtcGhvdG9cIn0+XG4gICAgICAgICAgICAgICAgPFVwbG9hZCB7Li4ucHJvcHN9PlxuICAgICAgICAgICAgICAgICAgPEJ1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPXtcImljb25mb250IGljb24tcGhvdG9cIn0vPiBJRCBDYXJkXG4gICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L1VwbG9hZD5cbiAgICAgICAgICAgICAgICA8YnIvPlxuICAgICAgICAgICAgICAgIDxici8+XG4gICAgICAgICAgICAgICAgPFVwbG9hZCB7Li4ucHJvcHMyfT5cbiAgICAgICAgICAgICAgICAgIDxCdXR0b24+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT17XCJpY29uZm9udCBpY29uLXBob3RvXCJ9Lz4gUGFzc3BvcnRcbiAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvVXBsb2FkPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgIDwvUm93PlxuICAgICAgICAgIDxJZGVudGlmeSBtb2RlbD17dGhpcy5wcm9wcy5tb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgcmVmPXt0aGlzLmdldFZpZGVvUmVmfVxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzQ2FsbGJhY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzTmV4dERpc2FibGVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgdXBsb2FkVXJsQmFzZTY0PXt0aGlzLnN0YXRlLnVwbG9hZFVybEJhc2U2NH1cbiAgICAgICAgICAgICAgICAgICAgZ2V0VXJsQmFzZTY0PXsodmlkZW9CYXNlNjQ6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICB9fS8+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e1wiYm90dG9tLWJ0blwifT5cbiAgICAgICAgICAgIDxTaGFyZSBtb2RlbD17bW9kZWx9Lz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtcImJvdHRvbS1yaWdodFwifT5cbiAgICAgICAgICAgICAgPEJ1dHRvbiB0eXBlPXtcInByaW1hcnlcIn0gZGlzYWJsZWQ9eyFpc05leHREaXNhYmxlZH0gb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2tpcCgpO1xuICAgICAgICAgICAgICB9fT5cbiAgICAgICAgICAgICAgICBTa2lwXG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvU3R5bGU+XG4gICAgICA8L01vZGFsPlxuICAgICk7XG4gIH1cbn1cblxuY29uc3QgU3R5bGUgPSBTdHlsZWQuZGl2YFxuICAudGFrZS1waG90byB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IDAgMCAxMHB4IDA7XG4gICAgaS5pY29uZm9udCB7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgIH1cbiAgICAuYW50LXVwbG9hZC1zZWxlY3Qge1xuICAgICAgJjpmaXJzdC1jaGlsZCB7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLnNob3ctaW1hZ2UsIC5hbnQtc3Bpbi1uZXN0ZWQtbG9hZGluZyB7XG4gICAgd2lkdGg6IDI0NXB4O1xuICAgIGltZyB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG4gIH1cbiAgLnNob3ctaW1hZ2UtYmVmb3JlIHtcbiAgICBtYXJnaW46IDIwcHggMDtcbiAgfVxuICAuc2hvdy1pbWFnZSB7XG4gICAgbWFyZ2luOiAxNXB4IDA7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2Q5ZDlkOTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgcGFkZGluZzogNXB4O1xuICB9XG4gIC5ib3R0b20tYnRuIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIC5ib3R0b20tcmlnaHQge1xuICAgICAgLmFudC1idG4ge1xuICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgJjpmaXJzdC1jaGlsZCB7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbmA7XG5leHBvcnQgZGVmYXVsdCBVcGxvYWRJbWc7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFoQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFpQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQ0E7QUFvQkE7Ozs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFPQTtBQUVBO0FBQ0E7QUFGQTtBQURBO0FBQ0E7QUFSQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7Ozs7O0FBZUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQTBEQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBOUZBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBd0JBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOzs7QUFNQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF2QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBMEJBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTs7OztBQS9NQTtBQUNBO0FBaU5BO0FBMkNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/ekyc/upload.tsx
