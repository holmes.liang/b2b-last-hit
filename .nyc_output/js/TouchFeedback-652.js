__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_6__);








var TouchFeedback = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default()(TouchFeedback, _React$Component);

  function TouchFeedback() {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, TouchFeedback);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, (TouchFeedback.__proto__ || Object.getPrototypeOf(TouchFeedback)).apply(this, arguments));

    _this.state = {
      active: false
    };

    _this.onTouchStart = function (e) {
      _this.triggerEvent('TouchStart', true, e);
    };

    _this.onTouchMove = function (e) {
      _this.triggerEvent('TouchMove', false, e);
    };

    _this.onTouchEnd = function (e) {
      _this.triggerEvent('TouchEnd', false, e);
    };

    _this.onTouchCancel = function (e) {
      _this.triggerEvent('TouchCancel', false, e);
    };

    _this.onMouseDown = function (e) {
      // pc simulate mobile
      _this.triggerEvent('MouseDown', true, e);
    };

    _this.onMouseUp = function (e) {
      _this.triggerEvent('MouseUp', false, e);
    };

    _this.onMouseLeave = function (e) {
      _this.triggerEvent('MouseLeave', false, e);
    };

    return _this;
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(TouchFeedback, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.props.disabled && this.state.active) {
        this.setState({
          active: false
        });
      }
    }
  }, {
    key: 'triggerEvent',
    value: function triggerEvent(type, isActive, ev) {
      var eventType = 'on' + type;
      var children = this.props.children;

      if (children.props[eventType]) {
        children.props[eventType](ev);
      }

      if (isActive !== this.state.active) {
        this.setState({
          active: isActive
        });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          disabled = _props.disabled,
          activeClassName = _props.activeClassName,
          activeStyle = _props.activeStyle;
      var events = disabled ? undefined : {
        onTouchStart: this.onTouchStart,
        onTouchMove: this.onTouchMove,
        onTouchEnd: this.onTouchEnd,
        onTouchCancel: this.onTouchCancel,
        onMouseDown: this.onMouseDown,
        onMouseUp: this.onMouseUp,
        onMouseLeave: this.onMouseLeave
      };
      var child = react__WEBPACK_IMPORTED_MODULE_5___default.a.Children.only(children);

      if (!disabled && this.state.active) {
        var _child$props = child.props,
            style = _child$props.style,
            className = _child$props.className;

        if (activeStyle !== false) {
          if (activeStyle) {
            style = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, style, activeStyle);
          }

          className = classnames__WEBPACK_IMPORTED_MODULE_6___default()(className, activeClassName);
        }

        return react__WEBPACK_IMPORTED_MODULE_5___default.a.cloneElement(child, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
          className: className,
          style: style
        }, events));
      }

      return react__WEBPACK_IMPORTED_MODULE_5___default.a.cloneElement(child, events);
    }
  }]);

  return TouchFeedback;
}(react__WEBPACK_IMPORTED_MODULE_5___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (TouchFeedback);
TouchFeedback.defaultProps = {
  disabled: false
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcm1jLWZlZWRiYWNrL2VzL1RvdWNoRmVlZGJhY2suanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9ybWMtZmVlZGJhY2svZXMvVG91Y2hGZWVkYmFjay5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2V4dGVuZHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnO1xuaW1wb3J0IF9jbGFzc0NhbGxDaGVjayBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snO1xuaW1wb3J0IF9jcmVhdGVDbGFzcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnO1xuaW1wb3J0IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJztcbmltcG9ydCBfaW5oZXJpdHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJztcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcblxudmFyIFRvdWNoRmVlZGJhY2sgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAgIF9pbmhlcml0cyhUb3VjaEZlZWRiYWNrLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICAgIGZ1bmN0aW9uIFRvdWNoRmVlZGJhY2soKSB7XG4gICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBUb3VjaEZlZWRiYWNrKTtcblxuICAgICAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoVG91Y2hGZWVkYmFjay5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKFRvdWNoRmVlZGJhY2spKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcblxuICAgICAgICBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIGFjdGl2ZTogZmFsc2VcbiAgICAgICAgfTtcbiAgICAgICAgX3RoaXMub25Ub3VjaFN0YXJ0ID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIF90aGlzLnRyaWdnZXJFdmVudCgnVG91Y2hTdGFydCcsIHRydWUsIGUpO1xuICAgICAgICB9O1xuICAgICAgICBfdGhpcy5vblRvdWNoTW92ZSA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBfdGhpcy50cmlnZ2VyRXZlbnQoJ1RvdWNoTW92ZScsIGZhbHNlLCBlKTtcbiAgICAgICAgfTtcbiAgICAgICAgX3RoaXMub25Ub3VjaEVuZCA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBfdGhpcy50cmlnZ2VyRXZlbnQoJ1RvdWNoRW5kJywgZmFsc2UsIGUpO1xuICAgICAgICB9O1xuICAgICAgICBfdGhpcy5vblRvdWNoQ2FuY2VsID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIF90aGlzLnRyaWdnZXJFdmVudCgnVG91Y2hDYW5jZWwnLCBmYWxzZSwgZSk7XG4gICAgICAgIH07XG4gICAgICAgIF90aGlzLm9uTW91c2VEb3duID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIC8vIHBjIHNpbXVsYXRlIG1vYmlsZVxuICAgICAgICAgICAgX3RoaXMudHJpZ2dlckV2ZW50KCdNb3VzZURvd24nLCB0cnVlLCBlKTtcbiAgICAgICAgfTtcbiAgICAgICAgX3RoaXMub25Nb3VzZVVwID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIF90aGlzLnRyaWdnZXJFdmVudCgnTW91c2VVcCcsIGZhbHNlLCBlKTtcbiAgICAgICAgfTtcbiAgICAgICAgX3RoaXMub25Nb3VzZUxlYXZlID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIF90aGlzLnRyaWdnZXJFdmVudCgnTW91c2VMZWF2ZScsIGZhbHNlLCBlKTtcbiAgICAgICAgfTtcbiAgICAgICAgcmV0dXJuIF90aGlzO1xuICAgIH1cblxuICAgIF9jcmVhdGVDbGFzcyhUb3VjaEZlZWRiYWNrLCBbe1xuICAgICAgICBrZXk6ICdjb21wb25lbnREaWRVcGRhdGUnLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKCkge1xuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMuZGlzYWJsZWQgJiYgdGhpcy5zdGF0ZS5hY3RpdmUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgYWN0aXZlOiBmYWxzZVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSwge1xuICAgICAgICBrZXk6ICd0cmlnZ2VyRXZlbnQnLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gdHJpZ2dlckV2ZW50KHR5cGUsIGlzQWN0aXZlLCBldikge1xuICAgICAgICAgICAgdmFyIGV2ZW50VHlwZSA9ICdvbicgKyB0eXBlO1xuICAgICAgICAgICAgdmFyIGNoaWxkcmVuID0gdGhpcy5wcm9wcy5jaGlsZHJlbjtcblxuICAgICAgICAgICAgaWYgKGNoaWxkcmVuLnByb3BzW2V2ZW50VHlwZV0pIHtcbiAgICAgICAgICAgICAgICBjaGlsZHJlbi5wcm9wc1tldmVudFR5cGVdKGV2KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChpc0FjdGl2ZSAhPT0gdGhpcy5zdGF0ZS5hY3RpdmUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgYWN0aXZlOiBpc0FjdGl2ZVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSwge1xuICAgICAgICBrZXk6ICdyZW5kZXInLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQgPSBfcHJvcHMuZGlzYWJsZWQsXG4gICAgICAgICAgICAgICAgYWN0aXZlQ2xhc3NOYW1lID0gX3Byb3BzLmFjdGl2ZUNsYXNzTmFtZSxcbiAgICAgICAgICAgICAgICBhY3RpdmVTdHlsZSA9IF9wcm9wcy5hY3RpdmVTdHlsZTtcblxuICAgICAgICAgICAgdmFyIGV2ZW50cyA9IGRpc2FibGVkID8gdW5kZWZpbmVkIDoge1xuICAgICAgICAgICAgICAgIG9uVG91Y2hTdGFydDogdGhpcy5vblRvdWNoU3RhcnQsXG4gICAgICAgICAgICAgICAgb25Ub3VjaE1vdmU6IHRoaXMub25Ub3VjaE1vdmUsXG4gICAgICAgICAgICAgICAgb25Ub3VjaEVuZDogdGhpcy5vblRvdWNoRW5kLFxuICAgICAgICAgICAgICAgIG9uVG91Y2hDYW5jZWw6IHRoaXMub25Ub3VjaENhbmNlbCxcbiAgICAgICAgICAgICAgICBvbk1vdXNlRG93bjogdGhpcy5vbk1vdXNlRG93bixcbiAgICAgICAgICAgICAgICBvbk1vdXNlVXA6IHRoaXMub25Nb3VzZVVwLFxuICAgICAgICAgICAgICAgIG9uTW91c2VMZWF2ZTogdGhpcy5vbk1vdXNlTGVhdmVcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICB2YXIgY2hpbGQgPSBSZWFjdC5DaGlsZHJlbi5vbmx5KGNoaWxkcmVuKTtcbiAgICAgICAgICAgIGlmICghZGlzYWJsZWQgJiYgdGhpcy5zdGF0ZS5hY3RpdmUpIHtcbiAgICAgICAgICAgICAgICB2YXIgX2NoaWxkJHByb3BzID0gY2hpbGQucHJvcHMsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlID0gX2NoaWxkJHByb3BzLnN0eWxlLFxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWUgPSBfY2hpbGQkcHJvcHMuY2xhc3NOYW1lO1xuXG4gICAgICAgICAgICAgICAgaWYgKGFjdGl2ZVN0eWxlICE9PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoYWN0aXZlU3R5bGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlID0gX2V4dGVuZHMoe30sIHN0eWxlLCBhY3RpdmVTdHlsZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lID0gY2xhc3NOYW1lcyhjbGFzc05hbWUsIGFjdGl2ZUNsYXNzTmFtZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiBSZWFjdC5jbG9uZUVsZW1lbnQoY2hpbGQsIF9leHRlbmRzKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlOiBzdHlsZSB9LCBldmVudHMpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBSZWFjdC5jbG9uZUVsZW1lbnQoY2hpbGQsIGV2ZW50cyk7XG4gICAgICAgIH1cbiAgICB9XSk7XG5cbiAgICByZXR1cm4gVG91Y2hGZWVkYmFjaztcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxuZXhwb3J0IGRlZmF1bHQgVG91Y2hGZWVkYmFjaztcblxuVG91Y2hGZWVkYmFjay5kZWZhdWx0UHJvcHMgPSB7XG4gICAgZGlzYWJsZWQ6IGZhbHNlXG59OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFSQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQWRBO0FBZ0JBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQWxDQTtBQUNBO0FBb0NBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQURBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rmc-feedback/es/TouchFeedback.js
