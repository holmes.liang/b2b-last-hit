var Displayable = __webpack_require__(/*! ./Displayable */ "./node_modules/zrender/lib/graphic/Displayable.js");

var zrUtil = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var textContain = __webpack_require__(/*! ../contain/text */ "./node_modules/zrender/lib/contain/text.js");

var textHelper = __webpack_require__(/*! ./helper/text */ "./node_modules/zrender/lib/graphic/helper/text.js");

var _constant = __webpack_require__(/*! ./constant */ "./node_modules/zrender/lib/graphic/constant.js");

var ContextCachedBy = _constant.ContextCachedBy;
/**
 * @alias zrender/graphic/Text
 * @extends module:zrender/graphic/Displayable
 * @constructor
 * @param {Object} opts
 */

var Text = function Text(opts) {
  // jshint ignore:line
  Displayable.call(this, opts);
};

Text.prototype = {
  constructor: Text,
  type: 'text',
  brush: function brush(ctx, prevEl) {
    var style = this.style; // Optimize, avoid normalize every time.

    this.__dirty && textHelper.normalizeTextStyle(style, true); // Use props with prefix 'text'.

    style.fill = style.stroke = style.shadowBlur = style.shadowColor = style.shadowOffsetX = style.shadowOffsetY = null;
    var text = style.text; // Convert to string

    text != null && (text += ''); // Do not apply style.bind in Text node. Because the real bind job
    // is in textHelper.renderText, and performance of text render should
    // be considered.
    // style.bind(ctx, this, prevEl);

    if (!textHelper.needDrawText(text, style)) {
      // The current el.style is not applied
      // and should not be used as cache.
      ctx.__attrCachedBy = ContextCachedBy.NONE;
      return;
    }

    this.setTransform(ctx);
    textHelper.renderText(this, ctx, text, style, null, prevEl);
    this.restoreTransform(ctx);
  },
  getBoundingRect: function getBoundingRect() {
    var style = this.style; // Optimize, avoid normalize every time.

    this.__dirty && textHelper.normalizeTextStyle(style, true);

    if (!this._rect) {
      var text = style.text;
      text != null ? text += '' : text = '';
      var rect = textContain.getBoundingRect(style.text + '', style.font, style.textAlign, style.textVerticalAlign, style.textPadding, style.textLineHeight, style.rich);
      rect.x += style.x || 0;
      rect.y += style.y || 0;

      if (textHelper.getStroke(style.textStroke, style.textStrokeWidth)) {
        var w = style.textStrokeWidth;
        rect.x -= w / 2;
        rect.y -= w / 2;
        rect.width += w;
        rect.height += w;
      }

      this._rect = rect;
    }

    return this._rect;
  }
};
zrUtil.inherits(Text, Displayable);
var _default = Text;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9UZXh0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9UZXh0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBEaXNwbGF5YWJsZSA9IHJlcXVpcmUoXCIuL0Rpc3BsYXlhYmxlXCIpO1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcIi4uL2NvcmUvdXRpbFwiKTtcblxudmFyIHRleHRDb250YWluID0gcmVxdWlyZShcIi4uL2NvbnRhaW4vdGV4dFwiKTtcblxudmFyIHRleHRIZWxwZXIgPSByZXF1aXJlKFwiLi9oZWxwZXIvdGV4dFwiKTtcblxudmFyIF9jb25zdGFudCA9IHJlcXVpcmUoXCIuL2NvbnN0YW50XCIpO1xuXG52YXIgQ29udGV4dENhY2hlZEJ5ID0gX2NvbnN0YW50LkNvbnRleHRDYWNoZWRCeTtcblxuLyoqXG4gKiBAYWxpYXMgenJlbmRlci9ncmFwaGljL1RleHRcbiAqIEBleHRlbmRzIG1vZHVsZTp6cmVuZGVyL2dyYXBoaWMvRGlzcGxheWFibGVcbiAqIEBjb25zdHJ1Y3RvclxuICogQHBhcmFtIHtPYmplY3R9IG9wdHNcbiAqL1xudmFyIFRleHQgPSBmdW5jdGlvbiAob3B0cykge1xuICAvLyBqc2hpbnQgaWdub3JlOmxpbmVcbiAgRGlzcGxheWFibGUuY2FsbCh0aGlzLCBvcHRzKTtcbn07XG5cblRleHQucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogVGV4dCxcbiAgdHlwZTogJ3RleHQnLFxuICBicnVzaDogZnVuY3Rpb24gKGN0eCwgcHJldkVsKSB7XG4gICAgdmFyIHN0eWxlID0gdGhpcy5zdHlsZTsgLy8gT3B0aW1pemUsIGF2b2lkIG5vcm1hbGl6ZSBldmVyeSB0aW1lLlxuXG4gICAgdGhpcy5fX2RpcnR5ICYmIHRleHRIZWxwZXIubm9ybWFsaXplVGV4dFN0eWxlKHN0eWxlLCB0cnVlKTsgLy8gVXNlIHByb3BzIHdpdGggcHJlZml4ICd0ZXh0Jy5cblxuICAgIHN0eWxlLmZpbGwgPSBzdHlsZS5zdHJva2UgPSBzdHlsZS5zaGFkb3dCbHVyID0gc3R5bGUuc2hhZG93Q29sb3IgPSBzdHlsZS5zaGFkb3dPZmZzZXRYID0gc3R5bGUuc2hhZG93T2Zmc2V0WSA9IG51bGw7XG4gICAgdmFyIHRleHQgPSBzdHlsZS50ZXh0OyAvLyBDb252ZXJ0IHRvIHN0cmluZ1xuXG4gICAgdGV4dCAhPSBudWxsICYmICh0ZXh0ICs9ICcnKTsgLy8gRG8gbm90IGFwcGx5IHN0eWxlLmJpbmQgaW4gVGV4dCBub2RlLiBCZWNhdXNlIHRoZSByZWFsIGJpbmQgam9iXG4gICAgLy8gaXMgaW4gdGV4dEhlbHBlci5yZW5kZXJUZXh0LCBhbmQgcGVyZm9ybWFuY2Ugb2YgdGV4dCByZW5kZXIgc2hvdWxkXG4gICAgLy8gYmUgY29uc2lkZXJlZC5cbiAgICAvLyBzdHlsZS5iaW5kKGN0eCwgdGhpcywgcHJldkVsKTtcblxuICAgIGlmICghdGV4dEhlbHBlci5uZWVkRHJhd1RleHQodGV4dCwgc3R5bGUpKSB7XG4gICAgICAvLyBUaGUgY3VycmVudCBlbC5zdHlsZSBpcyBub3QgYXBwbGllZFxuICAgICAgLy8gYW5kIHNob3VsZCBub3QgYmUgdXNlZCBhcyBjYWNoZS5cbiAgICAgIGN0eC5fX2F0dHJDYWNoZWRCeSA9IENvbnRleHRDYWNoZWRCeS5OT05FO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuc2V0VHJhbnNmb3JtKGN0eCk7XG4gICAgdGV4dEhlbHBlci5yZW5kZXJUZXh0KHRoaXMsIGN0eCwgdGV4dCwgc3R5bGUsIG51bGwsIHByZXZFbCk7XG4gICAgdGhpcy5yZXN0b3JlVHJhbnNmb3JtKGN0eCk7XG4gIH0sXG4gIGdldEJvdW5kaW5nUmVjdDogZnVuY3Rpb24gKCkge1xuICAgIHZhciBzdHlsZSA9IHRoaXMuc3R5bGU7IC8vIE9wdGltaXplLCBhdm9pZCBub3JtYWxpemUgZXZlcnkgdGltZS5cblxuICAgIHRoaXMuX19kaXJ0eSAmJiB0ZXh0SGVscGVyLm5vcm1hbGl6ZVRleHRTdHlsZShzdHlsZSwgdHJ1ZSk7XG5cbiAgICBpZiAoIXRoaXMuX3JlY3QpIHtcbiAgICAgIHZhciB0ZXh0ID0gc3R5bGUudGV4dDtcbiAgICAgIHRleHQgIT0gbnVsbCA/IHRleHQgKz0gJycgOiB0ZXh0ID0gJyc7XG4gICAgICB2YXIgcmVjdCA9IHRleHRDb250YWluLmdldEJvdW5kaW5nUmVjdChzdHlsZS50ZXh0ICsgJycsIHN0eWxlLmZvbnQsIHN0eWxlLnRleHRBbGlnbiwgc3R5bGUudGV4dFZlcnRpY2FsQWxpZ24sIHN0eWxlLnRleHRQYWRkaW5nLCBzdHlsZS50ZXh0TGluZUhlaWdodCwgc3R5bGUucmljaCk7XG4gICAgICByZWN0LnggKz0gc3R5bGUueCB8fCAwO1xuICAgICAgcmVjdC55ICs9IHN0eWxlLnkgfHwgMDtcblxuICAgICAgaWYgKHRleHRIZWxwZXIuZ2V0U3Ryb2tlKHN0eWxlLnRleHRTdHJva2UsIHN0eWxlLnRleHRTdHJva2VXaWR0aCkpIHtcbiAgICAgICAgdmFyIHcgPSBzdHlsZS50ZXh0U3Ryb2tlV2lkdGg7XG4gICAgICAgIHJlY3QueCAtPSB3IC8gMjtcbiAgICAgICAgcmVjdC55IC09IHcgLyAyO1xuICAgICAgICByZWN0LndpZHRoICs9IHc7XG4gICAgICAgIHJlY3QuaGVpZ2h0ICs9IHc7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX3JlY3QgPSByZWN0O1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLl9yZWN0O1xuICB9XG59O1xuenJVdGlsLmluaGVyaXRzKFRleHQsIERpc3BsYXlhYmxlKTtcbnZhciBfZGVmYXVsdCA9IFRleHQ7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbkRBO0FBcURBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/Text.js
