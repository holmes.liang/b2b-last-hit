__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _component_coverage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./component/coverage */ "./src/app/desk/quote/SAIC/iar/component/coverage.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/coverage-entry.tsx";





var CoverageEntry =
/*#__PURE__*/
function (_Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(CoverageEntry, _Component);

  function CoverageEntry(props) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, CoverageEntry);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(CoverageEntry).call(this, props));
    _this.state = {
      model: _model__WEBPACK_IMPORTED_MODULE_6__["Modeller"].asProxied({})
    };
    _this.state = {
      model: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.cloneDeep(lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_this.props.model, "member.ext.coverages.".concat(_this.props.dataIndex), {}))
    };
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(CoverageEntry, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var dataIndex = this.props.dataIndex; // this.setState({
      //   model: _.cloneDeep(_.get(this.props.model, `member.ext.coverages.${dataIndex}`, {})),
      // });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          form = _this$props.form,
          dataIndex = _this$props.dataIndex,
          siCurrency = _this$props.siCurrency,
          premiumCurrency = _this$props.premiumCurrency,
          isEndo = _this$props.isEndo,
          effDate = _this$props.effDate,
          expDate = _this$props.expDate,
          breakDown = _this$props.breakDown;
      var model = this.state.model;
      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_component_coverage__WEBPACK_IMPORTED_MODULE_8__["default"], {
        form: form,
        model: model,
        siCurrency: siCurrency,
        isEndo: isEndo,
        effDate: effDate,
        expDate: expDate,
        breakDown: breakDown,
        premiumCurrency: premiumCurrency,
        dataIndex: dataIndex,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        },
        __self: this
      });
    }
  }]);

  return CoverageEntry;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (CoverageEntry);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY292ZXJhZ2UtZW50cnkudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY292ZXJhZ2UtZW50cnkudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IE1vZGVsbGVyIH0gZnJvbSBcIkBtb2RlbFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IENvdmVyYWdlIGZyb20gXCIuL2NvbXBvbmVudC9jb3ZlcmFnZVwiO1xuaW1wb3J0IHsgRm9ybSB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBGb3JtQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvZm9ybVwiO1xuXG5cbmludGVyZmFjZSBJU3RhdGUge1xuICBtb2RlbDogYW55LFxufVxuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgbW9kZWw6IGFueSxcbiAgZm9ybTogYW55LFxuICBkYXRhSW5kZXg6IGFueSxcbiAgcHJlbWl1bUN1cnJlbmN5OiBhbnksXG4gIHNpQ3VycmVuY3k6IGFueSxcbiAgaXNFbmRvPzogYm9vbGVhblxuICBleHBEYXRlOiBhbnksXG4gIGVmZkRhdGU6IGFueSxcbiAgYnJlYWtEb3duPzogYW55XG59XG5cbmNsYXNzIENvdmVyYWdlRW50cnkgZXh0ZW5kcyBDb21wb25lbnQ8SVByb3BzLCBJU3RhdGU+IHtcbiAgc3RhdGU6IElTdGF0ZSA9IHtcbiAgICBtb2RlbDogTW9kZWxsZXIuYXNQcm94aWVkKHt9KSxcbiAgfTtcblxuICBjb25zdHJ1Y3Rvcihwcm9wczogYW55KSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBtb2RlbDogXy5jbG9uZURlZXAoXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgYG1lbWJlci5leHQuY292ZXJhZ2VzLiR7dGhpcy5wcm9wcy5kYXRhSW5kZXh9YCwge30pKSxcbiAgICB9O1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgY29uc3QgeyBkYXRhSW5kZXggfSA9IHRoaXMucHJvcHM7XG4gICAgLy8gdGhpcy5zZXRTdGF0ZSh7XG4gICAgLy8gICBtb2RlbDogXy5jbG9uZURlZXAoXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgYG1lbWJlci5leHQuY292ZXJhZ2VzLiR7ZGF0YUluZGV4fWAsIHt9KSksXG4gICAgLy8gfSk7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBmb3JtLCBkYXRhSW5kZXgsIHNpQ3VycmVuY3ksIHByZW1pdW1DdXJyZW5jeSwgaXNFbmRvLCBlZmZEYXRlLCBleHBEYXRlLCBicmVha0Rvd24gfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5zdGF0ZTtcbiAgICByZXR1cm4gKFxuICAgICAgPENvdmVyYWdlIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgIHNpQ3VycmVuY3k9e3NpQ3VycmVuY3l9XG4gICAgICAgICAgICAgICAgaXNFbmRvPXtpc0VuZG99XG4gICAgICAgICAgICAgICAgZWZmRGF0ZT17ZWZmRGF0ZX1cbiAgICAgICAgICAgICAgICBleHBEYXRlPXtleHBEYXRlfVxuICAgICAgICAgICAgICAgIGJyZWFrRG93bj17YnJlYWtEb3dufVxuICAgICAgICAgICAgICAgIHByZW1pdW1DdXJyZW5jeT17cHJlbWl1bUN1cnJlbmN5fVxuICAgICAgICAgICAgICAgIGRhdGFJbmRleD17ZGF0YUluZGV4fS8+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBDb3ZlcmFnZUVudHJ5O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBb0JBOzs7OztBQUtBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQURBO0FBSEE7QUFEQTtBQU1BO0FBQ0E7QUFEQTtBQUZBO0FBS0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7Ozs7QUFqQ0E7QUFDQTtBQW1DQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/coverage-entry.tsx
