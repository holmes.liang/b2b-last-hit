__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TimePickerButton; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);


function TimePickerButton(_ref) {
  var _classnames;

  var prefixCls = _ref.prefixCls,
      locale = _ref.locale,
      showTimePicker = _ref.showTimePicker,
      onOpenTimePicker = _ref.onOpenTimePicker,
      onCloseTimePicker = _ref.onCloseTimePicker,
      timePickerDisabled = _ref.timePickerDisabled;
  var className = classnames__WEBPACK_IMPORTED_MODULE_1___default()((_classnames = {}, _classnames[prefixCls + '-time-picker-btn'] = true, _classnames[prefixCls + '-time-picker-btn-disabled'] = timePickerDisabled, _classnames));
  var onClick = null;

  if (!timePickerDisabled) {
    onClick = showTimePicker ? onCloseTimePicker : onOpenTimePicker;
  }

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('a', {
    className: className,
    role: 'button',
    onClick: onClick
  }, showTimePicker ? locale.dateSelect : locale.timeSelect);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtY2FsZW5kYXIvZXMvY2FsZW5kYXIvVGltZVBpY2tlckJ1dHRvbi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWNhbGVuZGFyL2VzL2NhbGVuZGFyL1RpbWVQaWNrZXJCdXR0b24uanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBjbGFzc25hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBUaW1lUGlja2VyQnV0dG9uKF9yZWYpIHtcbiAgdmFyIF9jbGFzc25hbWVzO1xuXG4gIHZhciBwcmVmaXhDbHMgPSBfcmVmLnByZWZpeENscyxcbiAgICAgIGxvY2FsZSA9IF9yZWYubG9jYWxlLFxuICAgICAgc2hvd1RpbWVQaWNrZXIgPSBfcmVmLnNob3dUaW1lUGlja2VyLFxuICAgICAgb25PcGVuVGltZVBpY2tlciA9IF9yZWYub25PcGVuVGltZVBpY2tlcixcbiAgICAgIG9uQ2xvc2VUaW1lUGlja2VyID0gX3JlZi5vbkNsb3NlVGltZVBpY2tlcixcbiAgICAgIHRpbWVQaWNrZXJEaXNhYmxlZCA9IF9yZWYudGltZVBpY2tlckRpc2FibGVkO1xuXG4gIHZhciBjbGFzc05hbWUgPSBjbGFzc25hbWVzKChfY2xhc3NuYW1lcyA9IHt9LCBfY2xhc3NuYW1lc1twcmVmaXhDbHMgKyAnLXRpbWUtcGlja2VyLWJ0biddID0gdHJ1ZSwgX2NsYXNzbmFtZXNbcHJlZml4Q2xzICsgJy10aW1lLXBpY2tlci1idG4tZGlzYWJsZWQnXSA9IHRpbWVQaWNrZXJEaXNhYmxlZCwgX2NsYXNzbmFtZXMpKTtcbiAgdmFyIG9uQ2xpY2sgPSBudWxsO1xuICBpZiAoIXRpbWVQaWNrZXJEaXNhYmxlZCkge1xuICAgIG9uQ2xpY2sgPSBzaG93VGltZVBpY2tlciA/IG9uQ2xvc2VUaW1lUGlja2VyIDogb25PcGVuVGltZVBpY2tlcjtcbiAgfVxuICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAnYScsXG4gICAge1xuICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICByb2xlOiAnYnV0dG9uJyxcbiAgICAgIG9uQ2xpY2s6IG9uQ2xpY2tcbiAgICB9LFxuICAgIHNob3dUaW1lUGlja2VyID8gbG9jYWxlLmRhdGVTZWxlY3QgOiBsb2NhbGUudGltZVNlbGVjdFxuICApO1xufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFIQTtBQU9BIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-calendar/es/calendar/TimePickerButton.js
