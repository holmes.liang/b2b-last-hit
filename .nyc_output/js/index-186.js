__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_notification__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-notification */ "./node_modules/rc-notification/es/index.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}




var defaultDuration = 3;
var defaultTop;
var messageInstance;
var key = 1;
var prefixCls = 'ant-message';
var transitionName = 'move-up';
var getContainer;
var maxCount;

function getMessageInstance(callback) {
  if (messageInstance) {
    callback(messageInstance);
    return;
  }

  rc_notification__WEBPACK_IMPORTED_MODULE_1__["default"].newInstance({
    prefixCls: prefixCls,
    transitionName: transitionName,
    style: {
      top: defaultTop
    },
    getContainer: getContainer,
    maxCount: maxCount
  }, function (instance) {
    if (messageInstance) {
      callback(messageInstance);
      return;
    }

    messageInstance = instance;
    callback(instance);
  });
}

function notice(args) {
  var duration = args.duration !== undefined ? args.duration : defaultDuration;
  var iconType = {
    info: 'info-circle',
    success: 'check-circle',
    error: 'close-circle',
    warning: 'exclamation-circle',
    loading: 'loading'
  }[args.type];
  var target = args.key || key++;
  var closePromise = new Promise(function (resolve) {
    var callback = function callback() {
      if (typeof args.onClose === 'function') {
        args.onClose();
      }

      return resolve(true);
    };

    getMessageInstance(function (instance) {
      var iconNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_2__["default"], {
        type: iconType,
        theme: iconType === 'loading' ? 'outlined' : 'filled'
      });
      var switchIconNode = iconType ? iconNode : '';
      instance.notice({
        key: target,
        duration: duration,
        style: {},
        content: react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
          className: "".concat(prefixCls, "-custom-content").concat(args.type ? " ".concat(prefixCls, "-").concat(args.type) : '')
        }, args.icon ? args.icon : switchIconNode, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null, args.content)),
        onClose: callback
      });
    });
  });

  var result = function result() {
    if (messageInstance) {
      messageInstance.removeNotice(target);
    }
  };

  result.then = function (filled, rejected) {
    return closePromise.then(filled, rejected);
  };

  result.promise = closePromise;
  return result;
}

function isArgsProps(content) {
  return Object.prototype.toString.call(content) === '[object Object]' && !!content.content;
}

var api = {
  open: notice,
  config: function config(options) {
    if (options.top !== undefined) {
      defaultTop = options.top;
      messageInstance = null; // delete messageInstance for new defaultTop
    }

    if (options.duration !== undefined) {
      defaultDuration = options.duration;
    }

    if (options.prefixCls !== undefined) {
      prefixCls = options.prefixCls;
    }

    if (options.getContainer !== undefined) {
      getContainer = options.getContainer;
    }

    if (options.transitionName !== undefined) {
      transitionName = options.transitionName;
      messageInstance = null; // delete messageInstance for new transitionName
    }

    if (options.maxCount !== undefined) {
      maxCount = options.maxCount;
      messageInstance = null;
    }
  },
  destroy: function destroy() {
    if (messageInstance) {
      messageInstance.destroy();
      messageInstance = null;
    }
  }
};
['success', 'info', 'warning', 'error', 'loading'].forEach(function (type) {
  api[type] = function (content, duration, onClose) {
    if (isArgsProps(content)) {
      return api.open(_extends(_extends({}, content), {
        type: type
      }));
    }

    if (typeof duration === 'function') {
      onClose = duration;
      duration = undefined;
    }

    return api.open({
      content: content,
      duration: duration,
      type: type,
      onClose: onClose
    });
  };
});
api.warn = api.warning;
/* harmony default export */ __webpack_exports__["default"] = (api);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9tZXNzYWdlL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9tZXNzYWdlL2luZGV4LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTm90aWZpY2F0aW9uIGZyb20gJ3JjLW5vdGlmaWNhdGlvbic7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmxldCBkZWZhdWx0RHVyYXRpb24gPSAzO1xubGV0IGRlZmF1bHRUb3A7XG5sZXQgbWVzc2FnZUluc3RhbmNlO1xubGV0IGtleSA9IDE7XG5sZXQgcHJlZml4Q2xzID0gJ2FudC1tZXNzYWdlJztcbmxldCB0cmFuc2l0aW9uTmFtZSA9ICdtb3ZlLXVwJztcbmxldCBnZXRDb250YWluZXI7XG5sZXQgbWF4Q291bnQ7XG5mdW5jdGlvbiBnZXRNZXNzYWdlSW5zdGFuY2UoY2FsbGJhY2spIHtcbiAgICBpZiAobWVzc2FnZUluc3RhbmNlKSB7XG4gICAgICAgIGNhbGxiYWNrKG1lc3NhZ2VJbnN0YW5jZSk7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgTm90aWZpY2F0aW9uLm5ld0luc3RhbmNlKHtcbiAgICAgICAgcHJlZml4Q2xzLFxuICAgICAgICB0cmFuc2l0aW9uTmFtZSxcbiAgICAgICAgc3R5bGU6IHsgdG9wOiBkZWZhdWx0VG9wIH0sXG4gICAgICAgIGdldENvbnRhaW5lcixcbiAgICAgICAgbWF4Q291bnQsXG4gICAgfSwgKGluc3RhbmNlKSA9PiB7XG4gICAgICAgIGlmIChtZXNzYWdlSW5zdGFuY2UpIHtcbiAgICAgICAgICAgIGNhbGxiYWNrKG1lc3NhZ2VJbnN0YW5jZSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgbWVzc2FnZUluc3RhbmNlID0gaW5zdGFuY2U7XG4gICAgICAgIGNhbGxiYWNrKGluc3RhbmNlKTtcbiAgICB9KTtcbn1cbmZ1bmN0aW9uIG5vdGljZShhcmdzKSB7XG4gICAgY29uc3QgZHVyYXRpb24gPSBhcmdzLmR1cmF0aW9uICE9PSB1bmRlZmluZWQgPyBhcmdzLmR1cmF0aW9uIDogZGVmYXVsdER1cmF0aW9uO1xuICAgIGNvbnN0IGljb25UeXBlID0ge1xuICAgICAgICBpbmZvOiAnaW5mby1jaXJjbGUnLFxuICAgICAgICBzdWNjZXNzOiAnY2hlY2stY2lyY2xlJyxcbiAgICAgICAgZXJyb3I6ICdjbG9zZS1jaXJjbGUnLFxuICAgICAgICB3YXJuaW5nOiAnZXhjbGFtYXRpb24tY2lyY2xlJyxcbiAgICAgICAgbG9hZGluZzogJ2xvYWRpbmcnLFxuICAgIH1bYXJncy50eXBlXTtcbiAgICBjb25zdCB0YXJnZXQgPSBhcmdzLmtleSB8fCBrZXkrKztcbiAgICBjb25zdCBjbG9zZVByb21pc2UgPSBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcbiAgICAgICAgY29uc3QgY2FsbGJhY2sgPSAoKSA9PiB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIGFyZ3Mub25DbG9zZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIGFyZ3Mub25DbG9zZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHJlc29sdmUodHJ1ZSk7XG4gICAgICAgIH07XG4gICAgICAgIGdldE1lc3NhZ2VJbnN0YW5jZShpbnN0YW5jZSA9PiB7XG4gICAgICAgICAgICBjb25zdCBpY29uTm9kZSA9ICg8SWNvbiB0eXBlPXtpY29uVHlwZX0gdGhlbWU9e2ljb25UeXBlID09PSAnbG9hZGluZycgPyAnb3V0bGluZWQnIDogJ2ZpbGxlZCd9Lz4pO1xuICAgICAgICAgICAgY29uc3Qgc3dpdGNoSWNvbk5vZGUgPSBpY29uVHlwZSA/IGljb25Ob2RlIDogJyc7XG4gICAgICAgICAgICBpbnN0YW5jZS5ub3RpY2Uoe1xuICAgICAgICAgICAgICAgIGtleTogdGFyZ2V0LFxuICAgICAgICAgICAgICAgIGR1cmF0aW9uLFxuICAgICAgICAgICAgICAgIHN0eWxlOiB7fSxcbiAgICAgICAgICAgICAgICBjb250ZW50OiAoPGRpdiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tY3VzdG9tLWNvbnRlbnQke2FyZ3MudHlwZSA/IGAgJHtwcmVmaXhDbHN9LSR7YXJncy50eXBlfWAgOiAnJ31gfT5cbiAgICAgICAgICAgIHthcmdzLmljb24gPyBhcmdzLmljb24gOiBzd2l0Y2hJY29uTm9kZX1cbiAgICAgICAgICAgIDxzcGFuPnthcmdzLmNvbnRlbnR9PC9zcGFuPlxuICAgICAgICAgIDwvZGl2PiksXG4gICAgICAgICAgICAgICAgb25DbG9zZTogY2FsbGJhY2ssXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG4gICAgY29uc3QgcmVzdWx0ID0gKCkgPT4ge1xuICAgICAgICBpZiAobWVzc2FnZUluc3RhbmNlKSB7XG4gICAgICAgICAgICBtZXNzYWdlSW5zdGFuY2UucmVtb3ZlTm90aWNlKHRhcmdldCk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIHJlc3VsdC50aGVuID0gKGZpbGxlZCwgcmVqZWN0ZWQpID0+IGNsb3NlUHJvbWlzZS50aGVuKGZpbGxlZCwgcmVqZWN0ZWQpO1xuICAgIHJlc3VsdC5wcm9taXNlID0gY2xvc2VQcm9taXNlO1xuICAgIHJldHVybiByZXN1bHQ7XG59XG5mdW5jdGlvbiBpc0FyZ3NQcm9wcyhjb250ZW50KSB7XG4gICAgcmV0dXJuIChPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoY29udGVudCkgPT09ICdbb2JqZWN0IE9iamVjdF0nICYmXG4gICAgICAgICEhY29udGVudC5jb250ZW50KTtcbn1cbmNvbnN0IGFwaSA9IHtcbiAgICBvcGVuOiBub3RpY2UsXG4gICAgY29uZmlnKG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKG9wdGlvbnMudG9wICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGRlZmF1bHRUb3AgPSBvcHRpb25zLnRvcDtcbiAgICAgICAgICAgIG1lc3NhZ2VJbnN0YW5jZSA9IG51bGw7IC8vIGRlbGV0ZSBtZXNzYWdlSW5zdGFuY2UgZm9yIG5ldyBkZWZhdWx0VG9wXG4gICAgICAgIH1cbiAgICAgICAgaWYgKG9wdGlvbnMuZHVyYXRpb24gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgZGVmYXVsdER1cmF0aW9uID0gb3B0aW9ucy5kdXJhdGlvbjtcbiAgICAgICAgfVxuICAgICAgICBpZiAob3B0aW9ucy5wcmVmaXhDbHMgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcHJlZml4Q2xzID0gb3B0aW9ucy5wcmVmaXhDbHM7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG9wdGlvbnMuZ2V0Q29udGFpbmVyICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGdldENvbnRhaW5lciA9IG9wdGlvbnMuZ2V0Q29udGFpbmVyO1xuICAgICAgICB9XG4gICAgICAgIGlmIChvcHRpb25zLnRyYW5zaXRpb25OYW1lICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRyYW5zaXRpb25OYW1lID0gb3B0aW9ucy50cmFuc2l0aW9uTmFtZTtcbiAgICAgICAgICAgIG1lc3NhZ2VJbnN0YW5jZSA9IG51bGw7IC8vIGRlbGV0ZSBtZXNzYWdlSW5zdGFuY2UgZm9yIG5ldyB0cmFuc2l0aW9uTmFtZVxuICAgICAgICB9XG4gICAgICAgIGlmIChvcHRpb25zLm1heENvdW50ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIG1heENvdW50ID0gb3B0aW9ucy5tYXhDb3VudDtcbiAgICAgICAgICAgIG1lc3NhZ2VJbnN0YW5jZSA9IG51bGw7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIGRlc3Ryb3koKSB7XG4gICAgICAgIGlmIChtZXNzYWdlSW5zdGFuY2UpIHtcbiAgICAgICAgICAgIG1lc3NhZ2VJbnN0YW5jZS5kZXN0cm95KCk7XG4gICAgICAgICAgICBtZXNzYWdlSW5zdGFuY2UgPSBudWxsO1xuICAgICAgICB9XG4gICAgfSxcbn07XG5bJ3N1Y2Nlc3MnLCAnaW5mbycsICd3YXJuaW5nJywgJ2Vycm9yJywgJ2xvYWRpbmcnXS5mb3JFYWNoKHR5cGUgPT4ge1xuICAgIGFwaVt0eXBlXSA9IChjb250ZW50LCBkdXJhdGlvbiwgb25DbG9zZSkgPT4ge1xuICAgICAgICBpZiAoaXNBcmdzUHJvcHMoY29udGVudCkpIHtcbiAgICAgICAgICAgIHJldHVybiBhcGkub3BlbihPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIGNvbnRlbnQpLCB7IHR5cGUgfSkpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0eXBlb2YgZHVyYXRpb24gPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIG9uQ2xvc2UgPSBkdXJhdGlvbjtcbiAgICAgICAgICAgIGR1cmF0aW9uID0gdW5kZWZpbmVkO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBhcGkub3Blbih7IGNvbnRlbnQsIGR1cmF0aW9uLCB0eXBlLCBvbkNsb3NlIH0pO1xuICAgIH07XG59KTtcbmFwaS53YXJuID0gYXBpLndhcm5pbmc7XG5leHBvcnQgZGVmYXVsdCBhcGk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFaQTtBQWNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBSkE7QUFDQTtBQUtBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSUE7QUFSQTtBQUhBO0FBUEE7QUFDQTtBQXFCQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFJQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBdkJBO0FBeUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTlCQTtBQWdDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVJBO0FBREE7QUFZQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/message/index.js
