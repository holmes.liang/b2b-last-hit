__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");


function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  .ant-radio-group {\n    .ant-radio-wrapper {\n      display: block;\n      margin-bottom: 15px;\n      &:last-child{ \n        margin-bottom: 0\n      }\n    }\n  }\n  .ant-row.ant-form-item {\n    margin-bottom: 24px;\n    margin-top: 0px;\n  }\n  "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}


/* harmony default export */ __webpack_exports__["default"] = ({
  Scope: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject())
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY2xhaW1zL2NsYWltLXR5cGUtc3R5bGUudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY2xhaW1zL2NsYWltLXR5cGUtc3R5bGUudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIFNjb3BlOiBTdHlsZWQuZGl2YFxuICAuYW50LXJhZGlvLWdyb3VwIHtcbiAgICAuYW50LXJhZGlvLXdyYXBwZXIge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICAgICAgJjpsYXN0LWNoaWxkeyBcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxuICAgICAgfVxuICAgIH1cbiAgfVxuICAuYW50LXJvdy5hbnQtZm9ybS1pdGVtIHtcbiAgICBtYXJnaW4tYm90dG9tOiAyNHB4O1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgfVxuICBgLFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFFQTtBQUNBO0FBREEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/claims/claim-type-style.tsx
