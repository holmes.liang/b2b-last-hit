/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule encodeEntityRanges
 * @format
 * 
 */


var DraftStringKey = __webpack_require__(/*! ./DraftStringKey */ "./node_modules/draft-js/lib/DraftStringKey.js");

var UnicodeUtils = __webpack_require__(/*! fbjs/lib/UnicodeUtils */ "./node_modules/fbjs/lib/UnicodeUtils.js");

var strlen = UnicodeUtils.strlen;
/**
 * Convert to UTF-8 character counts for storage.
 */

function encodeEntityRanges(block, storageMap) {
  var encoded = [];
  block.findEntityRanges(function (character) {
    return !!character.getEntity();
  }, function (
  /*number*/
  start,
  /*number*/
  end) {
    var text = block.getText();
    var key = block.getEntityAt(start);
    encoded.push({
      offset: strlen(text.slice(0, start)),
      length: strlen(text.slice(start, end)),
      // Encode the key as a number for range storage.
      key: Number(storageMap[DraftStringKey.stringify(key)])
    });
  });
  return encoded;
}

module.exports = encodeEntityRanges;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VuY29kZUVudGl0eVJhbmdlcy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9lbmNvZGVFbnRpdHlSYW5nZXMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBlbmNvZGVFbnRpdHlSYW5nZXNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0U3RyaW5nS2V5ID0gcmVxdWlyZSgnLi9EcmFmdFN0cmluZ0tleScpO1xudmFyIFVuaWNvZGVVdGlscyA9IHJlcXVpcmUoJ2ZianMvbGliL1VuaWNvZGVVdGlscycpO1xuXG52YXIgc3RybGVuID0gVW5pY29kZVV0aWxzLnN0cmxlbjtcblxuLyoqXG4gKiBDb252ZXJ0IHRvIFVURi04IGNoYXJhY3RlciBjb3VudHMgZm9yIHN0b3JhZ2UuXG4gKi9cblxuZnVuY3Rpb24gZW5jb2RlRW50aXR5UmFuZ2VzKGJsb2NrLCBzdG9yYWdlTWFwKSB7XG4gIHZhciBlbmNvZGVkID0gW107XG4gIGJsb2NrLmZpbmRFbnRpdHlSYW5nZXMoZnVuY3Rpb24gKGNoYXJhY3Rlcikge1xuICAgIHJldHVybiAhIWNoYXJhY3Rlci5nZXRFbnRpdHkoKTtcbiAgfSwgZnVuY3Rpb24gKCAvKm51bWJlciovc3RhcnQsIC8qbnVtYmVyKi9lbmQpIHtcbiAgICB2YXIgdGV4dCA9IGJsb2NrLmdldFRleHQoKTtcbiAgICB2YXIga2V5ID0gYmxvY2suZ2V0RW50aXR5QXQoc3RhcnQpO1xuICAgIGVuY29kZWQucHVzaCh7XG4gICAgICBvZmZzZXQ6IHN0cmxlbih0ZXh0LnNsaWNlKDAsIHN0YXJ0KSksXG4gICAgICBsZW5ndGg6IHN0cmxlbih0ZXh0LnNsaWNlKHN0YXJ0LCBlbmQpKSxcbiAgICAgIC8vIEVuY29kZSB0aGUga2V5IGFzIGEgbnVtYmVyIGZvciByYW5nZSBzdG9yYWdlLlxuICAgICAga2V5OiBOdW1iZXIoc3RvcmFnZU1hcFtEcmFmdFN0cmluZ0tleS5zdHJpbmdpZnkoa2V5KV0pXG4gICAgfSk7XG4gIH0pO1xuICByZXR1cm4gZW5jb2RlZDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBlbmNvZGVFbnRpdHlSYW5nZXM7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/encodeEntityRanges.js
