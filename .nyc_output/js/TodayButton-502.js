__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TodayButton; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../util/ */ "./node_modules/rc-calendar/es/util/index.js");


function TodayButton(_ref) {
  var prefixCls = _ref.prefixCls,
      locale = _ref.locale,
      value = _ref.value,
      timePicker = _ref.timePicker,
      disabled = _ref.disabled,
      disabledDate = _ref.disabledDate,
      onToday = _ref.onToday,
      text = _ref.text;
  var localeNow = (!text && timePicker ? locale.now : text) || locale.today;
  var disabledToday = disabledDate && !Object(_util___WEBPACK_IMPORTED_MODULE_1__["isAllowedDate"])(Object(_util___WEBPACK_IMPORTED_MODULE_1__["getTodayTime"])(value), disabledDate);
  var isDisabled = disabledToday || disabled;
  var disabledTodayClass = isDisabled ? prefixCls + '-today-btn-disabled' : '';
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('a', {
    className: prefixCls + '-today-btn ' + disabledTodayClass,
    role: 'button',
    onClick: isDisabled ? null : onToday,
    title: Object(_util___WEBPACK_IMPORTED_MODULE_1__["getTodayTimeStr"])(value)
  }, localeNow);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtY2FsZW5kYXIvZXMvY2FsZW5kYXIvVG9kYXlCdXR0b24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1jYWxlbmRhci9lcy9jYWxlbmRhci9Ub2RheUJ1dHRvbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgZ2V0VG9kYXlUaW1lU3RyLCBnZXRUb2RheVRpbWUsIGlzQWxsb3dlZERhdGUgfSBmcm9tICcuLi91dGlsLyc7XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFRvZGF5QnV0dG9uKF9yZWYpIHtcbiAgdmFyIHByZWZpeENscyA9IF9yZWYucHJlZml4Q2xzLFxuICAgICAgbG9jYWxlID0gX3JlZi5sb2NhbGUsXG4gICAgICB2YWx1ZSA9IF9yZWYudmFsdWUsXG4gICAgICB0aW1lUGlja2VyID0gX3JlZi50aW1lUGlja2VyLFxuICAgICAgZGlzYWJsZWQgPSBfcmVmLmRpc2FibGVkLFxuICAgICAgZGlzYWJsZWREYXRlID0gX3JlZi5kaXNhYmxlZERhdGUsXG4gICAgICBvblRvZGF5ID0gX3JlZi5vblRvZGF5LFxuICAgICAgdGV4dCA9IF9yZWYudGV4dDtcblxuICB2YXIgbG9jYWxlTm93ID0gKCF0ZXh0ICYmIHRpbWVQaWNrZXIgPyBsb2NhbGUubm93IDogdGV4dCkgfHwgbG9jYWxlLnRvZGF5O1xuICB2YXIgZGlzYWJsZWRUb2RheSA9IGRpc2FibGVkRGF0ZSAmJiAhaXNBbGxvd2VkRGF0ZShnZXRUb2RheVRpbWUodmFsdWUpLCBkaXNhYmxlZERhdGUpO1xuICB2YXIgaXNEaXNhYmxlZCA9IGRpc2FibGVkVG9kYXkgfHwgZGlzYWJsZWQ7XG4gIHZhciBkaXNhYmxlZFRvZGF5Q2xhc3MgPSBpc0Rpc2FibGVkID8gcHJlZml4Q2xzICsgJy10b2RheS1idG4tZGlzYWJsZWQnIDogJyc7XG4gIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICdhJyxcbiAgICB7XG4gICAgICBjbGFzc05hbWU6IHByZWZpeENscyArICctdG9kYXktYnRuICcgKyBkaXNhYmxlZFRvZGF5Q2xhc3MsXG4gICAgICByb2xlOiAnYnV0dG9uJyxcbiAgICAgIG9uQ2xpY2s6IGlzRGlzYWJsZWQgPyBudWxsIDogb25Ub2RheSxcbiAgICAgIHRpdGxlOiBnZXRUb2RheVRpbWVTdHIodmFsdWUpXG4gICAgfSxcbiAgICBsb2NhbGVOb3dcbiAgKTtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFRQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-calendar/es/calendar/TodayButton.js
