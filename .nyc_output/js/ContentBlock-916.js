/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule ContentBlock
 * @format
 * 
 */


function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var findRangesImmutable = __webpack_require__(/*! ./findRangesImmutable */ "./node_modules/draft-js/lib/findRangesImmutable.js");

var List = Immutable.List,
    Map = Immutable.Map,
    OrderedSet = Immutable.OrderedSet,
    Record = Immutable.Record,
    Repeat = Immutable.Repeat;
var EMPTY_SET = OrderedSet();
var defaultRecord = {
  key: '',
  type: 'unstyled',
  text: '',
  characterList: List(),
  depth: 0,
  data: Map()
};
var ContentBlockRecord = Record(defaultRecord);

var decorateCharacterList = function decorateCharacterList(config) {
  if (!config) {
    return config;
  }

  var characterList = config.characterList,
      text = config.text;

  if (text && !characterList) {
    config.characterList = List(Repeat(CharacterMetadata.EMPTY, text.length));
  }

  return config;
};

var ContentBlock = function (_ContentBlockRecord) {
  _inherits(ContentBlock, _ContentBlockRecord);

  function ContentBlock(config) {
    _classCallCheck(this, ContentBlock);

    return _possibleConstructorReturn(this, _ContentBlockRecord.call(this, decorateCharacterList(config)));
  }

  ContentBlock.prototype.getKey = function getKey() {
    return this.get('key');
  };

  ContentBlock.prototype.getType = function getType() {
    return this.get('type');
  };

  ContentBlock.prototype.getText = function getText() {
    return this.get('text');
  };

  ContentBlock.prototype.getCharacterList = function getCharacterList() {
    return this.get('characterList');
  };

  ContentBlock.prototype.getLength = function getLength() {
    return this.getText().length;
  };

  ContentBlock.prototype.getDepth = function getDepth() {
    return this.get('depth');
  };

  ContentBlock.prototype.getData = function getData() {
    return this.get('data');
  };

  ContentBlock.prototype.getInlineStyleAt = function getInlineStyleAt(offset) {
    var character = this.getCharacterList().get(offset);
    return character ? character.getStyle() : EMPTY_SET;
  };

  ContentBlock.prototype.getEntityAt = function getEntityAt(offset) {
    var character = this.getCharacterList().get(offset);
    return character ? character.getEntity() : null;
  };
  /**
   * Execute a callback for every contiguous range of styles within the block.
   */


  ContentBlock.prototype.findStyleRanges = function findStyleRanges(filterFn, callback) {
    findRangesImmutable(this.getCharacterList(), haveEqualStyle, filterFn, callback);
  };
  /**
   * Execute a callback for every contiguous range of entities within the block.
   */


  ContentBlock.prototype.findEntityRanges = function findEntityRanges(filterFn, callback) {
    findRangesImmutable(this.getCharacterList(), haveEqualEntity, filterFn, callback);
  };

  return ContentBlock;
}(ContentBlockRecord);

function haveEqualStyle(charA, charB) {
  return charA.getStyle() === charB.getStyle();
}

function haveEqualEntity(charA, charB) {
  return charA.getEntity() === charB.getEntity();
}

module.exports = ContentBlock;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0NvbnRlbnRCbG9jay5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9Db250ZW50QmxvY2suanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBDb250ZW50QmxvY2tcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgQ2hhcmFjdGVyTWV0YWRhdGEgPSByZXF1aXJlKCcuL0NoYXJhY3Rlck1ldGFkYXRhJyk7XG52YXIgSW1tdXRhYmxlID0gcmVxdWlyZSgnaW1tdXRhYmxlJyk7XG5cbnZhciBmaW5kUmFuZ2VzSW1tdXRhYmxlID0gcmVxdWlyZSgnLi9maW5kUmFuZ2VzSW1tdXRhYmxlJyk7XG5cbnZhciBMaXN0ID0gSW1tdXRhYmxlLkxpc3QsXG4gICAgTWFwID0gSW1tdXRhYmxlLk1hcCxcbiAgICBPcmRlcmVkU2V0ID0gSW1tdXRhYmxlLk9yZGVyZWRTZXQsXG4gICAgUmVjb3JkID0gSW1tdXRhYmxlLlJlY29yZCxcbiAgICBSZXBlYXQgPSBJbW11dGFibGUuUmVwZWF0O1xuXG5cbnZhciBFTVBUWV9TRVQgPSBPcmRlcmVkU2V0KCk7XG5cbnZhciBkZWZhdWx0UmVjb3JkID0ge1xuICBrZXk6ICcnLFxuICB0eXBlOiAndW5zdHlsZWQnLFxuICB0ZXh0OiAnJyxcbiAgY2hhcmFjdGVyTGlzdDogTGlzdCgpLFxuICBkZXB0aDogMCxcbiAgZGF0YTogTWFwKClcbn07XG5cbnZhciBDb250ZW50QmxvY2tSZWNvcmQgPSBSZWNvcmQoZGVmYXVsdFJlY29yZCk7XG5cbnZhciBkZWNvcmF0ZUNoYXJhY3Rlckxpc3QgPSBmdW5jdGlvbiBkZWNvcmF0ZUNoYXJhY3Rlckxpc3QoY29uZmlnKSB7XG4gIGlmICghY29uZmlnKSB7XG4gICAgcmV0dXJuIGNvbmZpZztcbiAgfVxuXG4gIHZhciBjaGFyYWN0ZXJMaXN0ID0gY29uZmlnLmNoYXJhY3Rlckxpc3QsXG4gICAgICB0ZXh0ID0gY29uZmlnLnRleHQ7XG5cblxuICBpZiAodGV4dCAmJiAhY2hhcmFjdGVyTGlzdCkge1xuICAgIGNvbmZpZy5jaGFyYWN0ZXJMaXN0ID0gTGlzdChSZXBlYXQoQ2hhcmFjdGVyTWV0YWRhdGEuRU1QVFksIHRleHQubGVuZ3RoKSk7XG4gIH1cblxuICByZXR1cm4gY29uZmlnO1xufTtcblxudmFyIENvbnRlbnRCbG9jayA9IGZ1bmN0aW9uIChfQ29udGVudEJsb2NrUmVjb3JkKSB7XG4gIF9pbmhlcml0cyhDb250ZW50QmxvY2ssIF9Db250ZW50QmxvY2tSZWNvcmQpO1xuXG4gIGZ1bmN0aW9uIENvbnRlbnRCbG9jayhjb25maWcpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQ29udGVudEJsb2NrKTtcblxuICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29udGVudEJsb2NrUmVjb3JkLmNhbGwodGhpcywgZGVjb3JhdGVDaGFyYWN0ZXJMaXN0KGNvbmZpZykpKTtcbiAgfVxuXG4gIENvbnRlbnRCbG9jay5wcm90b3R5cGUuZ2V0S2V5ID0gZnVuY3Rpb24gZ2V0S2V5KCkge1xuICAgIHJldHVybiB0aGlzLmdldCgna2V5Jyk7XG4gIH07XG5cbiAgQ29udGVudEJsb2NrLnByb3RvdHlwZS5nZXRUeXBlID0gZnVuY3Rpb24gZ2V0VHlwZSgpIHtcbiAgICByZXR1cm4gdGhpcy5nZXQoJ3R5cGUnKTtcbiAgfTtcblxuICBDb250ZW50QmxvY2sucHJvdG90eXBlLmdldFRleHQgPSBmdW5jdGlvbiBnZXRUZXh0KCkge1xuICAgIHJldHVybiB0aGlzLmdldCgndGV4dCcpO1xuICB9O1xuXG4gIENvbnRlbnRCbG9jay5wcm90b3R5cGUuZ2V0Q2hhcmFjdGVyTGlzdCA9IGZ1bmN0aW9uIGdldENoYXJhY3Rlckxpc3QoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdjaGFyYWN0ZXJMaXN0Jyk7XG4gIH07XG5cbiAgQ29udGVudEJsb2NrLnByb3RvdHlwZS5nZXRMZW5ndGggPSBmdW5jdGlvbiBnZXRMZW5ndGgoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0VGV4dCgpLmxlbmd0aDtcbiAgfTtcblxuICBDb250ZW50QmxvY2sucHJvdG90eXBlLmdldERlcHRoID0gZnVuY3Rpb24gZ2V0RGVwdGgoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdkZXB0aCcpO1xuICB9O1xuXG4gIENvbnRlbnRCbG9jay5wcm90b3R5cGUuZ2V0RGF0YSA9IGZ1bmN0aW9uIGdldERhdGEoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdkYXRhJyk7XG4gIH07XG5cbiAgQ29udGVudEJsb2NrLnByb3RvdHlwZS5nZXRJbmxpbmVTdHlsZUF0ID0gZnVuY3Rpb24gZ2V0SW5saW5lU3R5bGVBdChvZmZzZXQpIHtcbiAgICB2YXIgY2hhcmFjdGVyID0gdGhpcy5nZXRDaGFyYWN0ZXJMaXN0KCkuZ2V0KG9mZnNldCk7XG4gICAgcmV0dXJuIGNoYXJhY3RlciA/IGNoYXJhY3Rlci5nZXRTdHlsZSgpIDogRU1QVFlfU0VUO1xuICB9O1xuXG4gIENvbnRlbnRCbG9jay5wcm90b3R5cGUuZ2V0RW50aXR5QXQgPSBmdW5jdGlvbiBnZXRFbnRpdHlBdChvZmZzZXQpIHtcbiAgICB2YXIgY2hhcmFjdGVyID0gdGhpcy5nZXRDaGFyYWN0ZXJMaXN0KCkuZ2V0KG9mZnNldCk7XG4gICAgcmV0dXJuIGNoYXJhY3RlciA/IGNoYXJhY3Rlci5nZXRFbnRpdHkoKSA6IG51bGw7XG4gIH07XG5cbiAgLyoqXG4gICAqIEV4ZWN1dGUgYSBjYWxsYmFjayBmb3IgZXZlcnkgY29udGlndW91cyByYW5nZSBvZiBzdHlsZXMgd2l0aGluIHRoZSBibG9jay5cbiAgICovXG5cblxuICBDb250ZW50QmxvY2sucHJvdG90eXBlLmZpbmRTdHlsZVJhbmdlcyA9IGZ1bmN0aW9uIGZpbmRTdHlsZVJhbmdlcyhmaWx0ZXJGbiwgY2FsbGJhY2spIHtcbiAgICBmaW5kUmFuZ2VzSW1tdXRhYmxlKHRoaXMuZ2V0Q2hhcmFjdGVyTGlzdCgpLCBoYXZlRXF1YWxTdHlsZSwgZmlsdGVyRm4sIGNhbGxiYWNrKTtcbiAgfTtcblxuICAvKipcbiAgICogRXhlY3V0ZSBhIGNhbGxiYWNrIGZvciBldmVyeSBjb250aWd1b3VzIHJhbmdlIG9mIGVudGl0aWVzIHdpdGhpbiB0aGUgYmxvY2suXG4gICAqL1xuXG5cbiAgQ29udGVudEJsb2NrLnByb3RvdHlwZS5maW5kRW50aXR5UmFuZ2VzID0gZnVuY3Rpb24gZmluZEVudGl0eVJhbmdlcyhmaWx0ZXJGbiwgY2FsbGJhY2spIHtcbiAgICBmaW5kUmFuZ2VzSW1tdXRhYmxlKHRoaXMuZ2V0Q2hhcmFjdGVyTGlzdCgpLCBoYXZlRXF1YWxFbnRpdHksIGZpbHRlckZuLCBjYWxsYmFjayk7XG4gIH07XG5cbiAgcmV0dXJuIENvbnRlbnRCbG9jaztcbn0oQ29udGVudEJsb2NrUmVjb3JkKTtcblxuZnVuY3Rpb24gaGF2ZUVxdWFsU3R5bGUoY2hhckEsIGNoYXJCKSB7XG4gIHJldHVybiBjaGFyQS5nZXRTdHlsZSgpID09PSBjaGFyQi5nZXRTdHlsZSgpO1xufVxuXG5mdW5jdGlvbiBoYXZlRXF1YWxFbnRpdHkoY2hhckEsIGNoYXJCKSB7XG4gIHJldHVybiBjaGFyQS5nZXRFbnRpdHkoKSA9PT0gY2hhckIuZ2V0RW50aXR5KCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gQ29udGVudEJsb2NrOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/ContentBlock.js
