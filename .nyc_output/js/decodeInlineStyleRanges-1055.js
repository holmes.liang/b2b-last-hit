/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule decodeInlineStyleRanges
 * @format
 * 
 */


var _require = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js"),
    OrderedSet = _require.OrderedSet;

var UnicodeUtils = __webpack_require__(/*! fbjs/lib/UnicodeUtils */ "./node_modules/fbjs/lib/UnicodeUtils.js");

var substr = UnicodeUtils.substr;
var EMPTY_SET = OrderedSet();
/**
 * Convert to native JavaScript string lengths to determine ranges.
 */

function decodeInlineStyleRanges(text, ranges) {
  var styles = Array(text.length).fill(EMPTY_SET);

  if (ranges) {
    ranges.forEach(function (
    /*object*/
    range) {
      var cursor = substr(text, 0, range.offset).length;
      var end = cursor + substr(text, range.offset, range.length).length;

      while (cursor < end) {
        styles[cursor] = styles[cursor].add(range.style);
        cursor++;
      }
    });
  }

  return styles;
}

module.exports = decodeInlineStyleRanges;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2RlY29kZUlubGluZVN0eWxlUmFuZ2VzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2RlY29kZUlubGluZVN0eWxlUmFuZ2VzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZGVjb2RlSW5saW5lU3R5bGVSYW5nZXNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9yZXF1aXJlID0gcmVxdWlyZSgnaW1tdXRhYmxlJyksXG4gICAgT3JkZXJlZFNldCA9IF9yZXF1aXJlLk9yZGVyZWRTZXQ7XG5cbnZhciBVbmljb2RlVXRpbHMgPSByZXF1aXJlKCdmYmpzL2xpYi9Vbmljb2RlVXRpbHMnKTtcblxudmFyIHN1YnN0ciA9IFVuaWNvZGVVdGlscy5zdWJzdHI7XG5cblxudmFyIEVNUFRZX1NFVCA9IE9yZGVyZWRTZXQoKTtcblxuLyoqXG4gKiBDb252ZXJ0IHRvIG5hdGl2ZSBKYXZhU2NyaXB0IHN0cmluZyBsZW5ndGhzIHRvIGRldGVybWluZSByYW5nZXMuXG4gKi9cbmZ1bmN0aW9uIGRlY29kZUlubGluZVN0eWxlUmFuZ2VzKHRleHQsIHJhbmdlcykge1xuICB2YXIgc3R5bGVzID0gQXJyYXkodGV4dC5sZW5ndGgpLmZpbGwoRU1QVFlfU0VUKTtcbiAgaWYgKHJhbmdlcykge1xuICAgIHJhbmdlcy5mb3JFYWNoKGZ1bmN0aW9uICggLypvYmplY3QqL3JhbmdlKSB7XG4gICAgICB2YXIgY3Vyc29yID0gc3Vic3RyKHRleHQsIDAsIHJhbmdlLm9mZnNldCkubGVuZ3RoO1xuICAgICAgdmFyIGVuZCA9IGN1cnNvciArIHN1YnN0cih0ZXh0LCByYW5nZS5vZmZzZXQsIHJhbmdlLmxlbmd0aCkubGVuZ3RoO1xuICAgICAgd2hpbGUgKGN1cnNvciA8IGVuZCkge1xuICAgICAgICBzdHlsZXNbY3Vyc29yXSA9IHN0eWxlc1tjdXJzb3JdLmFkZChyYW5nZS5zdHlsZSk7XG4gICAgICAgIGN1cnNvcisrO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIHJldHVybiBzdHlsZXM7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZGVjb2RlSW5saW5lU3R5bGVSYW5nZXM7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUdBO0FBRUE7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/decodeInlineStyleRanges.js
