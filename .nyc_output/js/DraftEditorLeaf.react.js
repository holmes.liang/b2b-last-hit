/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEditorLeaf.react
 * @format
 * 
 */


var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var DraftEditorTextNode = __webpack_require__(/*! ./DraftEditorTextNode.react */ "./node_modules/draft-js/lib/DraftEditorTextNode.react.js");

var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var ReactDOM = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var setDraftEditorSelection = __webpack_require__(/*! ./setDraftEditorSelection */ "./node_modules/draft-js/lib/setDraftEditorSelection.js");
/**
 * All leaf nodes in the editor are spans with single text nodes. Leaf
 * elements are styled based on the merging of an optional custom style map
 * and a default style map.
 *
 * `DraftEditorLeaf` also provides a wrapper for calling into the imperative
 * DOM Selection API. In this way, top-level components can declaratively
 * maintain the selection state.
 */


var DraftEditorLeaf = function (_React$Component) {
  _inherits(DraftEditorLeaf, _React$Component);

  function DraftEditorLeaf() {
    _classCallCheck(this, DraftEditorLeaf);

    return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
  }

  DraftEditorLeaf.prototype._setSelection = function _setSelection() {
    var selection = this.props.selection; // If selection state is irrelevant to the parent block, no-op.

    if (selection == null || !selection.getHasFocus()) {
      return;
    }

    var _props = this.props,
        block = _props.block,
        start = _props.start,
        text = _props.text;
    var blockKey = block.getKey();
    var end = start + text.length;

    if (!selection.hasEdgeWithin(blockKey, start, end)) {
      return;
    } // Determine the appropriate target node for selection. If the child
    // is not a text node, it is a <br /> spacer. In this case, use the
    // <span> itself as the selection target.


    var node = ReactDOM.findDOMNode(this);
    !node ?  true ? invariant(false, 'Missing node') : undefined : void 0;
    var child = node.firstChild;
    !child ?  true ? invariant(false, 'Missing child') : undefined : void 0;
    var targetNode = void 0;

    if (child.nodeType === Node.TEXT_NODE) {
      targetNode = child;
    } else if (child.tagName === 'BR') {
      targetNode = node;
    } else {
      targetNode = child.firstChild;
      !targetNode ?  true ? invariant(false, 'Missing targetNode') : undefined : void 0;
    }

    setDraftEditorSelection(selection, targetNode, blockKey, start, end);
  };
  /**
   * By making individual leaf instances aware of their context within
   * the text of the editor, we can set our selection range more
   * easily than we could in the non-React world.
   *
   * Note that this depends on our maintaining tight control over the
   * DOM structure of the DraftEditor component. If leaves had multiple
   * text nodes, this would be harder.
   */


  DraftEditorLeaf.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
    var leafNode = ReactDOM.findDOMNode(this.leaf);
    !leafNode ?  true ? invariant(false, 'Missing leafNode') : undefined : void 0;
    return leafNode.textContent !== nextProps.text || nextProps.styleSet !== this.props.styleSet || nextProps.forceSelection;
  };

  DraftEditorLeaf.prototype.componentDidUpdate = function componentDidUpdate() {
    this._setSelection();
  };

  DraftEditorLeaf.prototype.componentDidMount = function componentDidMount() {
    this._setSelection();
  };

  DraftEditorLeaf.prototype.render = function render() {
    var _this2 = this;

    var block = this.props.block;
    var text = this.props.text; // If the leaf is at the end of its block and ends in a soft newline, append
    // an extra line feed character. Browsers collapse trailing newline
    // characters, which leaves the cursor in the wrong place after a
    // shift+enter. The extra character repairs this.

    if (text.endsWith('\n') && this.props.isLast) {
      text += '\n';
    }

    var _props2 = this.props,
        customStyleMap = _props2.customStyleMap,
        customStyleFn = _props2.customStyleFn,
        offsetKey = _props2.offsetKey,
        styleSet = _props2.styleSet;
    var styleObj = styleSet.reduce(function (map, styleName) {
      var mergedStyles = {};
      var style = customStyleMap[styleName];

      if (style !== undefined && map.textDecoration !== style.textDecoration) {
        // .trim() is necessary for IE9/10/11 and Edge
        mergedStyles.textDecoration = [map.textDecoration, style.textDecoration].join(' ').trim();
      }

      return _assign(map, style, mergedStyles);
    }, {});

    if (customStyleFn) {
      var newStyles = customStyleFn(styleSet, block);
      styleObj = _assign(styleObj, newStyles);
    }

    return React.createElement('span', {
      'data-offset-key': offsetKey,
      ref: function ref(_ref) {
        return _this2.leaf = _ref;
      },
      style: styleObj
    }, React.createElement(DraftEditorTextNode, null, text));
  };

  return DraftEditorLeaf;
}(React.Component);

module.exports = DraftEditorLeaf;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RWRpdG9yTGVhZi5yZWFjdC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9EcmFmdEVkaXRvckxlYWYucmVhY3QuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBEcmFmdEVkaXRvckxlYWYucmVhY3RcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9hc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIERyYWZ0RWRpdG9yVGV4dE5vZGUgPSByZXF1aXJlKCcuL0RyYWZ0RWRpdG9yVGV4dE5vZGUucmVhY3QnKTtcbnZhciBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG52YXIgUmVhY3RET00gPSByZXF1aXJlKCdyZWFjdC1kb20nKTtcblxudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJ2ZianMvbGliL2ludmFyaWFudCcpO1xudmFyIHNldERyYWZ0RWRpdG9yU2VsZWN0aW9uID0gcmVxdWlyZSgnLi9zZXREcmFmdEVkaXRvclNlbGVjdGlvbicpO1xuXG4vKipcbiAqIEFsbCBsZWFmIG5vZGVzIGluIHRoZSBlZGl0b3IgYXJlIHNwYW5zIHdpdGggc2luZ2xlIHRleHQgbm9kZXMuIExlYWZcbiAqIGVsZW1lbnRzIGFyZSBzdHlsZWQgYmFzZWQgb24gdGhlIG1lcmdpbmcgb2YgYW4gb3B0aW9uYWwgY3VzdG9tIHN0eWxlIG1hcFxuICogYW5kIGEgZGVmYXVsdCBzdHlsZSBtYXAuXG4gKlxuICogYERyYWZ0RWRpdG9yTGVhZmAgYWxzbyBwcm92aWRlcyBhIHdyYXBwZXIgZm9yIGNhbGxpbmcgaW50byB0aGUgaW1wZXJhdGl2ZVxuICogRE9NIFNlbGVjdGlvbiBBUEkuIEluIHRoaXMgd2F5LCB0b3AtbGV2ZWwgY29tcG9uZW50cyBjYW4gZGVjbGFyYXRpdmVseVxuICogbWFpbnRhaW4gdGhlIHNlbGVjdGlvbiBzdGF0ZS5cbiAqL1xudmFyIERyYWZ0RWRpdG9yTGVhZiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhEcmFmdEVkaXRvckxlYWYsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIERyYWZ0RWRpdG9yTGVhZigpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgRHJhZnRFZGl0b3JMZWFmKTtcblxuICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgRHJhZnRFZGl0b3JMZWFmLnByb3RvdHlwZS5fc2V0U2VsZWN0aW9uID0gZnVuY3Rpb24gX3NldFNlbGVjdGlvbigpIHtcbiAgICB2YXIgc2VsZWN0aW9uID0gdGhpcy5wcm9wcy5zZWxlY3Rpb247XG5cbiAgICAvLyBJZiBzZWxlY3Rpb24gc3RhdGUgaXMgaXJyZWxldmFudCB0byB0aGUgcGFyZW50IGJsb2NrLCBuby1vcC5cblxuICAgIGlmIChzZWxlY3Rpb24gPT0gbnVsbCB8fCAhc2VsZWN0aW9uLmdldEhhc0ZvY3VzKCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgYmxvY2sgPSBfcHJvcHMuYmxvY2ssXG4gICAgICAgIHN0YXJ0ID0gX3Byb3BzLnN0YXJ0LFxuICAgICAgICB0ZXh0ID0gX3Byb3BzLnRleHQ7XG5cbiAgICB2YXIgYmxvY2tLZXkgPSBibG9jay5nZXRLZXkoKTtcbiAgICB2YXIgZW5kID0gc3RhcnQgKyB0ZXh0Lmxlbmd0aDtcbiAgICBpZiAoIXNlbGVjdGlvbi5oYXNFZGdlV2l0aGluKGJsb2NrS2V5LCBzdGFydCwgZW5kKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIERldGVybWluZSB0aGUgYXBwcm9wcmlhdGUgdGFyZ2V0IG5vZGUgZm9yIHNlbGVjdGlvbi4gSWYgdGhlIGNoaWxkXG4gICAgLy8gaXMgbm90IGEgdGV4dCBub2RlLCBpdCBpcyBhIDxiciAvPiBzcGFjZXIuIEluIHRoaXMgY2FzZSwgdXNlIHRoZVxuICAgIC8vIDxzcGFuPiBpdHNlbGYgYXMgdGhlIHNlbGVjdGlvbiB0YXJnZXQuXG4gICAgdmFyIG5vZGUgPSBSZWFjdERPTS5maW5kRE9NTm9kZSh0aGlzKTtcbiAgICAhbm9kZSA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdNaXNzaW5nIG5vZGUnKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG4gICAgdmFyIGNoaWxkID0gbm9kZS5maXJzdENoaWxkO1xuICAgICFjaGlsZCA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdNaXNzaW5nIGNoaWxkJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuICAgIHZhciB0YXJnZXROb2RlID0gdm9pZCAwO1xuXG4gICAgaWYgKGNoaWxkLm5vZGVUeXBlID09PSBOb2RlLlRFWFRfTk9ERSkge1xuICAgICAgdGFyZ2V0Tm9kZSA9IGNoaWxkO1xuICAgIH0gZWxzZSBpZiAoY2hpbGQudGFnTmFtZSA9PT0gJ0JSJykge1xuICAgICAgdGFyZ2V0Tm9kZSA9IG5vZGU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRhcmdldE5vZGUgPSBjaGlsZC5maXJzdENoaWxkO1xuICAgICAgIXRhcmdldE5vZGUgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAnTWlzc2luZyB0YXJnZXROb2RlJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuICAgIH1cblxuICAgIHNldERyYWZ0RWRpdG9yU2VsZWN0aW9uKHNlbGVjdGlvbiwgdGFyZ2V0Tm9kZSwgYmxvY2tLZXksIHN0YXJ0LCBlbmQpO1xuICB9O1xuICAvKipcbiAgICogQnkgbWFraW5nIGluZGl2aWR1YWwgbGVhZiBpbnN0YW5jZXMgYXdhcmUgb2YgdGhlaXIgY29udGV4dCB3aXRoaW5cbiAgICogdGhlIHRleHQgb2YgdGhlIGVkaXRvciwgd2UgY2FuIHNldCBvdXIgc2VsZWN0aW9uIHJhbmdlIG1vcmVcbiAgICogZWFzaWx5IHRoYW4gd2UgY291bGQgaW4gdGhlIG5vbi1SZWFjdCB3b3JsZC5cbiAgICpcbiAgICogTm90ZSB0aGF0IHRoaXMgZGVwZW5kcyBvbiBvdXIgbWFpbnRhaW5pbmcgdGlnaHQgY29udHJvbCBvdmVyIHRoZVxuICAgKiBET00gc3RydWN0dXJlIG9mIHRoZSBEcmFmdEVkaXRvciBjb21wb25lbnQuIElmIGxlYXZlcyBoYWQgbXVsdGlwbGVcbiAgICogdGV4dCBub2RlcywgdGhpcyB3b3VsZCBiZSBoYXJkZXIuXG4gICAqL1xuXG4gIERyYWZ0RWRpdG9yTGVhZi5wcm90b3R5cGUuc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgIHZhciBsZWFmTm9kZSA9IFJlYWN0RE9NLmZpbmRET01Ob2RlKHRoaXMubGVhZik7XG4gICAgIWxlYWZOb2RlID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ01pc3NpbmcgbGVhZk5vZGUnKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG4gICAgcmV0dXJuIGxlYWZOb2RlLnRleHRDb250ZW50ICE9PSBuZXh0UHJvcHMudGV4dCB8fCBuZXh0UHJvcHMuc3R5bGVTZXQgIT09IHRoaXMucHJvcHMuc3R5bGVTZXQgfHwgbmV4dFByb3BzLmZvcmNlU2VsZWN0aW9uO1xuICB9O1xuXG4gIERyYWZ0RWRpdG9yTGVhZi5wcm90b3R5cGUuY29tcG9uZW50RGlkVXBkYXRlID0gZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKCkge1xuICAgIHRoaXMuX3NldFNlbGVjdGlvbigpO1xuICB9O1xuXG4gIERyYWZ0RWRpdG9yTGVhZi5wcm90b3R5cGUuY29tcG9uZW50RGlkTW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLl9zZXRTZWxlY3Rpb24oKTtcbiAgfTtcblxuICBEcmFmdEVkaXRvckxlYWYucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgIHZhciBibG9jayA9IHRoaXMucHJvcHMuYmxvY2s7XG4gICAgdmFyIHRleHQgPSB0aGlzLnByb3BzLnRleHQ7XG5cbiAgICAvLyBJZiB0aGUgbGVhZiBpcyBhdCB0aGUgZW5kIG9mIGl0cyBibG9jayBhbmQgZW5kcyBpbiBhIHNvZnQgbmV3bGluZSwgYXBwZW5kXG4gICAgLy8gYW4gZXh0cmEgbGluZSBmZWVkIGNoYXJhY3Rlci4gQnJvd3NlcnMgY29sbGFwc2UgdHJhaWxpbmcgbmV3bGluZVxuICAgIC8vIGNoYXJhY3RlcnMsIHdoaWNoIGxlYXZlcyB0aGUgY3Vyc29yIGluIHRoZSB3cm9uZyBwbGFjZSBhZnRlciBhXG4gICAgLy8gc2hpZnQrZW50ZXIuIFRoZSBleHRyYSBjaGFyYWN0ZXIgcmVwYWlycyB0aGlzLlxuXG4gICAgaWYgKHRleHQuZW5kc1dpdGgoJ1xcbicpICYmIHRoaXMucHJvcHMuaXNMYXN0KSB7XG4gICAgICB0ZXh0ICs9ICdcXG4nO1xuICAgIH1cblxuICAgIHZhciBfcHJvcHMyID0gdGhpcy5wcm9wcyxcbiAgICAgICAgY3VzdG9tU3R5bGVNYXAgPSBfcHJvcHMyLmN1c3RvbVN0eWxlTWFwLFxuICAgICAgICBjdXN0b21TdHlsZUZuID0gX3Byb3BzMi5jdXN0b21TdHlsZUZuLFxuICAgICAgICBvZmZzZXRLZXkgPSBfcHJvcHMyLm9mZnNldEtleSxcbiAgICAgICAgc3R5bGVTZXQgPSBfcHJvcHMyLnN0eWxlU2V0O1xuXG4gICAgdmFyIHN0eWxlT2JqID0gc3R5bGVTZXQucmVkdWNlKGZ1bmN0aW9uIChtYXAsIHN0eWxlTmFtZSkge1xuICAgICAgdmFyIG1lcmdlZFN0eWxlcyA9IHt9O1xuICAgICAgdmFyIHN0eWxlID0gY3VzdG9tU3R5bGVNYXBbc3R5bGVOYW1lXTtcblxuICAgICAgaWYgKHN0eWxlICE9PSB1bmRlZmluZWQgJiYgbWFwLnRleHREZWNvcmF0aW9uICE9PSBzdHlsZS50ZXh0RGVjb3JhdGlvbikge1xuICAgICAgICAvLyAudHJpbSgpIGlzIG5lY2Vzc2FyeSBmb3IgSUU5LzEwLzExIGFuZCBFZGdlXG4gICAgICAgIG1lcmdlZFN0eWxlcy50ZXh0RGVjb3JhdGlvbiA9IFttYXAudGV4dERlY29yYXRpb24sIHN0eWxlLnRleHREZWNvcmF0aW9uXS5qb2luKCcgJykudHJpbSgpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX2Fzc2lnbihtYXAsIHN0eWxlLCBtZXJnZWRTdHlsZXMpO1xuICAgIH0sIHt9KTtcblxuICAgIGlmIChjdXN0b21TdHlsZUZuKSB7XG4gICAgICB2YXIgbmV3U3R5bGVzID0gY3VzdG9tU3R5bGVGbihzdHlsZVNldCwgYmxvY2spO1xuICAgICAgc3R5bGVPYmogPSBfYXNzaWduKHN0eWxlT2JqLCBuZXdTdHlsZXMpO1xuICAgIH1cblxuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgJ3NwYW4nLFxuICAgICAge1xuICAgICAgICAnZGF0YS1vZmZzZXQta2V5Jzogb2Zmc2V0S2V5LFxuICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihfcmVmKSB7XG4gICAgICAgICAgcmV0dXJuIF90aGlzMi5sZWFmID0gX3JlZjtcbiAgICAgICAgfSxcbiAgICAgICAgc3R5bGU6IHN0eWxlT2JqIH0sXG4gICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBEcmFmdEVkaXRvclRleHROb2RlLFxuICAgICAgICBudWxsLFxuICAgICAgICB0ZXh0XG4gICAgICApXG4gICAgKTtcbiAgfTtcblxuICByZXR1cm4gRHJhZnRFZGl0b3JMZWFmO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IERyYWZ0RWRpdG9yTGVhZjsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEditorLeaf.react.js
