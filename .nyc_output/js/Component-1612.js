/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var Group = __webpack_require__(/*! zrender/lib/container/Group */ "./node_modules/zrender/lib/container/Group.js");

var componentUtil = __webpack_require__(/*! ../util/component */ "./node_modules/echarts/lib/util/component.js");

var clazzUtil = __webpack_require__(/*! ../util/clazz */ "./node_modules/echarts/lib/util/clazz.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var Component = function Component() {
  /**
   * @type {module:zrender/container/Group}
   * @readOnly
   */
  this.group = new Group();
  /**
   * @type {string}
   * @readOnly
   */

  this.uid = componentUtil.getUID('viewComponent');
};

Component.prototype = {
  constructor: Component,
  init: function init(ecModel, api) {},
  render: function render(componentModel, ecModel, api, payload) {},
  dispose: function dispose() {},

  /**
   * @param {string} eventType
   * @param {Object} query
   * @param {module:zrender/Element} targetEl
   * @param {Object} packedEvent
   * @return {boolen} Pass only when return `true`.
   */
  filterForExposedEvent: null
};
var componentProto = Component.prototype;

componentProto.updateView = componentProto.updateLayout = componentProto.updateVisual = function (seriesModel, ecModel, api, payload) {// Do nothing;
}; // Enable Component.extend.


clazzUtil.enableClassExtend(Component); // Enable capability of registerClass, getClass, hasClass, registerSubTypeDefaulter and so on.

clazzUtil.enableClassManagement(Component, {
  registerWhenExtend: true
});
var _default = Component;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdmlldy9Db21wb25lbnQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi92aWV3L0NvbXBvbmVudC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIEdyb3VwID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvbnRhaW5lci9Hcm91cFwiKTtcblxudmFyIGNvbXBvbmVudFV0aWwgPSByZXF1aXJlKFwiLi4vdXRpbC9jb21wb25lbnRcIik7XG5cbnZhciBjbGF6elV0aWwgPSByZXF1aXJlKFwiLi4vdXRpbC9jbGF6elwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIENvbXBvbmVudCA9IGZ1bmN0aW9uICgpIHtcbiAgLyoqXG4gICAqIEB0eXBlIHttb2R1bGU6enJlbmRlci9jb250YWluZXIvR3JvdXB9XG4gICAqIEByZWFkT25seVxuICAgKi9cbiAgdGhpcy5ncm91cCA9IG5ldyBHcm91cCgpO1xuICAvKipcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICogQHJlYWRPbmx5XG4gICAqL1xuXG4gIHRoaXMudWlkID0gY29tcG9uZW50VXRpbC5nZXRVSUQoJ3ZpZXdDb21wb25lbnQnKTtcbn07XG5cbkNvbXBvbmVudC5wcm90b3R5cGUgPSB7XG4gIGNvbnN0cnVjdG9yOiBDb21wb25lbnQsXG4gIGluaXQ6IGZ1bmN0aW9uIChlY01vZGVsLCBhcGkpIHt9LFxuICByZW5kZXI6IGZ1bmN0aW9uIChjb21wb25lbnRNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkKSB7fSxcbiAgZGlzcG9zZTogZnVuY3Rpb24gKCkge30sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBldmVudFR5cGVcbiAgICogQHBhcmFtIHtPYmplY3R9IHF1ZXJ5XG4gICAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvRWxlbWVudH0gdGFyZ2V0RWxcbiAgICogQHBhcmFtIHtPYmplY3R9IHBhY2tlZEV2ZW50XG4gICAqIEByZXR1cm4ge2Jvb2xlbn0gUGFzcyBvbmx5IHdoZW4gcmV0dXJuIGB0cnVlYC5cbiAgICovXG4gIGZpbHRlckZvckV4cG9zZWRFdmVudDogbnVsbFxufTtcbnZhciBjb21wb25lbnRQcm90byA9IENvbXBvbmVudC5wcm90b3R5cGU7XG5cbmNvbXBvbmVudFByb3RvLnVwZGF0ZVZpZXcgPSBjb21wb25lbnRQcm90by51cGRhdGVMYXlvdXQgPSBjb21wb25lbnRQcm90by51cGRhdGVWaXN1YWwgPSBmdW5jdGlvbiAoc2VyaWVzTW9kZWwsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCkgey8vIERvIG5vdGhpbmc7XG59OyAvLyBFbmFibGUgQ29tcG9uZW50LmV4dGVuZC5cblxuXG5jbGF6elV0aWwuZW5hYmxlQ2xhc3NFeHRlbmQoQ29tcG9uZW50KTsgLy8gRW5hYmxlIGNhcGFiaWxpdHkgb2YgcmVnaXN0ZXJDbGFzcywgZ2V0Q2xhc3MsIGhhc0NsYXNzLCByZWdpc3RlclN1YlR5cGVEZWZhdWx0ZXIgYW5kIHNvIG9uLlxuXG5jbGF6elV0aWwuZW5hYmxlQ2xhc3NNYW5hZ2VtZW50KENvbXBvbmVudCwge1xuICByZWdpc3RlcldoZW5FeHRlbmQ6IHRydWVcbn0pO1xudmFyIF9kZWZhdWx0ID0gQ29tcG9uZW50O1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBOzs7O0FBSUE7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBYkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/view/Component.js
