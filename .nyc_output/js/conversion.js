var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _regeneratorRuntime = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");

var _asyncToGenerator = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/asyncToGenerator.js");

var _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
  return typeof t;
} : function (t) {
  return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
};

!function (t) {
  "object" === ( false ? undefined : _typeof(exports)) && "undefined" != typeof module ? module.exports = t() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(function () {
  var t = {};

  function e(t) {
    return ["image/png", "image/jpeg", "image/gif"].some(function (e) {
      return e === t;
    });
  }

  return t.urltoImage = function (t) {
    return new Promise(function (e, a) {
      var n = new Image();
      n.onload = function () {
        return e(n);
      }, n.onerror = function () {
        return a(new Error("urltoImage(): Image failed to load, please check the image URL"));
      }, n.src = t;
    });
  }, t.urltoBlob = function (t) {
    return fetch(t).then(function (t) {
      return t.blob();
    });
  }, t.imagetoCanvas =
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    _regeneratorRuntime.mark(function _callee(t) {
      var e,
          a,
          n,
          i,
          r,
          o,
          c,
          _args = arguments;
      return _regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              e = _args.length > 1 && void 0 !== _args[1] ? _args[1] : {}, a = document.createElement("canvas"), n = a.getContext("2d"), i = void 0, r = void 0;

              for (o in e) {
                Object.prototype.hasOwnProperty.call(e, o) && (e[o] = Number(e[o]));
              }

              if (e.scale) {
                c = e.scale > 0 && e.scale < 10 ? e.scale : 1;
                r = t.width * c, i = t.height * c;
              } else r = e.width || e.height * t.width / t.height || t.width, i = e.height || e.width * t.height / t.width || t.height;

              _context.t0 = ([5, 6, 7, 8].some(function (t) {
                return t === e.orientation;
              }) ? (a.height = r, a.width = i) : (a.height = i, a.width = r), e.orientation);
              _context.next = _context.t0 === 3 ? 6 : _context.t0 === 6 ? 8 : _context.t0 === 8 ? 10 : _context.t0 === 2 ? 12 : _context.t0 === 4 ? 14 : _context.t0 === 5 ? 16 : _context.t0 === 7 ? 18 : 20;
              break;

            case 6:
              n.rotate(180 * Math.PI / 180), n.drawImage(t, -a.width, -a.height, a.width, a.height);
              return _context.abrupt("break", 21);

            case 8:
              n.rotate(90 * Math.PI / 180), n.drawImage(t, 0, -a.width, a.height, a.width);
              return _context.abrupt("break", 21);

            case 10:
              n.rotate(270 * Math.PI / 180), n.drawImage(t, -a.height, 0, a.height, a.width);
              return _context.abrupt("break", 21);

            case 12:
              n.translate(a.width, 0), n.scale(-1, 1), n.drawImage(t, 0, 0, a.width, a.height);
              return _context.abrupt("break", 21);

            case 14:
              n.translate(a.width, 0), n.scale(-1, 1), n.rotate(180 * Math.PI / 180), n.drawImage(t, -a.width, -a.height, a.width, a.height);
              return _context.abrupt("break", 21);

            case 16:
              n.translate(a.width, 0), n.scale(-1, 1), n.rotate(90 * Math.PI / 180), n.drawImage(t, 0, -a.width, a.height, a.width);
              return _context.abrupt("break", 21);

            case 18:
              n.translate(a.width, 0), n.scale(-1, 1), n.rotate(270 * Math.PI / 180), n.drawImage(t, -a.height, 0, a.height, a.width);
              return _context.abrupt("break", 21);

            case 20:
              n.drawImage(t, 0, 0, a.width, a.height);

            case 21:
              return _context.abrupt("return", a);

            case 22:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }(), t.canvastoFile = function (t, e) {
    var a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "image/jpeg";
    return new Promise(function (n) {
      return t.toBlob(function (t) {
        return n(t);
      }, a, e);
    });
  }, t.canvastoDataURL =
  /*#__PURE__*/
  function () {
    var _ref2 = _asyncToGenerator(
    /*#__PURE__*/
    _regeneratorRuntime.mark(function _callee2(t, a, n) {
      return _regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              return _context2.abrupt("return", (e(n) || (n = "image/jpeg"), t.toDataURL(n, a)));

            case 1:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function (_x2, _x3, _x4) {
      return _ref2.apply(this, arguments);
    };
  }(), t.filetoDataURL = function (t) {
    return new Promise(function (e) {
      var a = new FileReader();
      a.onloadend = function (t) {
        return e(t.target.result);
      }, a.readAsDataURL(t);
    });
  }, t.dataURLtoImage = function (t) {
    return new Promise(function (e, a) {
      var n = new Image();
      n.onload = function () {
        return e(n);
      }, n.onerror = function () {
        return a(new Error("dataURLtoImage(): dataURL is illegal"));
      }, n.src = t;
    });
  }, t.dataURLtoFile =
  /*#__PURE__*/
  function () {
    var _ref3 = _asyncToGenerator(
    /*#__PURE__*/
    _regeneratorRuntime.mark(function _callee3(t, a) {
      var n, i, r, o, c;
      return _regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              for (n = t.split(","), i = n[0].match(/:(.*?);/)[1], r = atob(n[1]), o = r.length, c = new Uint8Array(o); o--;) {
                c[o] = r.charCodeAt(o);
              }

              return _context3.abrupt("return", (e(a) && (i = a), new Blob([c], {
                type: i
              })));

            case 2:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    return function (_x5, _x6) {
      return _ref3.apply(this, arguments);
    };
  }(), t.downloadFile = function (t, e) {
    var a = document.createElement("a");
    a.href = window.URL.createObjectURL(t), a.download = e || Date.now().toString(36), document.body.appendChild(a);
    var n = document.createEvent("MouseEvents");
    n.initEvent("click", !1, !1), a.dispatchEvent(n), document.body.removeChild(a);
  }, t.compress =
  /*#__PURE__*/
  function () {
    var _ref4 = _asyncToGenerator(
    /*#__PURE__*/
    _regeneratorRuntime.mark(function _callee4(a) {
      var n,
          i,
          r,
          o,
          c,
          u,
          s,
          _args4 = arguments;
      return _regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              n = _args4.length > 1 && void 0 !== _args4[1] ? _args4[1] : {};

              if (a instanceof Blob) {
                _context4.next = 3;
                break;
              }

              throw new Error("compress(): First arg must be a Blob object or a File object.");

            case 3:
              if (!("object" !== (void 0 === n ? "undefined" : _typeof(n)) && (n = Object.assign({
                quality: n
              })), n.quality = Number(n.quality), Number.isNaN(n.quality))) {
                _context4.next = 5;
                break;
              }

              return _context4.abrupt("return", a);

            case 5:
              _context4.next = 7;
              return t.filetoDataURL(a);

            case 7:
              i = _context4.sent;
              r = i.split(",")[0].match(/:(.*?);/)[1];
              o = "image/jpeg";
              e(n.type) && (o = n.type, r = n.type);
              _context4.next = 13;
              return t.dataURLtoImage(i);

            case 13:
              c = _context4.sent;
              _context4.next = 16;
              return t.imagetoCanvas(c, Object.assign({}, n));

            case 16:
              u = _context4.sent;
              _context4.next = 19;
              return t.canvastoDataURL(u, n.quality, o);

            case 19:
              s = _context4.sent;
              _context4.next = 22;
              return t.dataURLtoFile(s, r);

            case 22:
              return _context4.abrupt("return", _context4.sent);

            case 23:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4);
    }));

    return function (_x7) {
      return _ref4.apply(this, arguments);
    };
  }(), t.compressAccurately =
  /*#__PURE__*/
  function () {
    var _ref5 = _asyncToGenerator(
    /*#__PURE__*/
    _regeneratorRuntime.mark(function _callee5(a) {
      var n,
          i,
          r,
          o,
          c,
          u,
          s,
          h,
          d,
          l,
          f,
          g,
          m,
          w,
          y,
          _args5 = arguments;
      return _regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              n = _args5.length > 1 && void 0 !== _args5[1] ? _args5[1] : {};

              if (a instanceof Blob) {
                _context5.next = 3;
                break;
              }

              throw new Error("compressAccurately(): First arg must be a Blob object or a File object.");

            case 3:
              if (!("object" !== (void 0 === n ? "undefined" : _typeof(n)) && (n = Object.assign({
                size: n
              })), n.size = Number(n.size), Number.isNaN(n.size))) {
                _context5.next = 5;
                break;
              }

              return _context5.abrupt("return", a);

            case 5:
              if (!(1024 * n.size > a.size)) {
                _context5.next = 7;
                break;
              }

              return _context5.abrupt("return", a);

            case 7:
              n.accuracy = Number(n.accuracy), (!n.accuracy || n.accuracy < .8 || n.accuracy > .99) && (n.accuracy = .95);
              i = n.size * (2 - n.accuracy) * 1024;
              r = 1024 * n.size;
              o = n.size * n.accuracy * 1024;
              _context5.next = 13;
              return t.filetoDataURL(a);

            case 13:
              c = _context5.sent;
              u = c.split(",")[0].match(/:(.*?);/)[1];
              s = "image/jpeg";
              e(n.type) && (s = n.type, u = n.type);
              _context5.next = 19;
              return t.dataURLtoImage(c);

            case 19:
              h = _context5.sent;
              _context5.next = 22;
              return t.imagetoCanvas(h, Object.assign({}, n));

            case 22:
              d = _context5.sent;
              l = .5;
              f = void 0;
              g = [null, null];
              m = 1;

            case 27:
              if (!(m <= 7)) {
                _context5.next = 45;
                break;
              }

              _context5.next = 30;
              return t.canvastoDataURL(d, l, s);

            case 30:
              _context5.t0 = (f = _context5.sent).length;
              w = .75 * _context5.t0;

              if (!(7 === m)) {
                _context5.next = 35;
                break;
              }

              (i < w || o > w) && (f = [f].concat(g).filter(function (t) {
                return t;
              }).sort(function (t, e) {
                return Math.abs(.75 * t.length - r) - Math.abs(.75 * e.length - r);
              })[0]);
              return _context5.abrupt("break", 45);

            case 35:
              if (!(i < w)) {
                _context5.next = 39;
                break;
              }

              g[1] = f, l -= Math.pow(.5, m + 1);
              _context5.next = 42;
              break;

            case 39:
              if (o > w) {
                _context5.next = 41;
                break;
              }

              return _context5.abrupt("break", 45);

            case 41:
              g[0] = f, l += Math.pow(.5, m + 1);

            case 42:
              m++;
              _context5.next = 27;
              break;

            case 45:
              _context5.next = 47;
              return t.dataURLtoFile(f, u);

            case 47:
              y = _context5.sent;
              return _context5.abrupt("return", y.size > a.size ? a : y);

            case 49:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5);
    }));

    return function (_x8) {
      return _ref5.apply(this, arguments);
    };
  }(), t;
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvaW1hZ2UtY29udmVyc2lvbi9idWlsZC9jb252ZXJzaW9uLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvaW1hZ2UtY29udmVyc2lvbi9idWlsZC9jb252ZXJzaW9uLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO3ZhciBfdHlwZW9mPVwiZnVuY3Rpb25cIj09dHlwZW9mIFN5bWJvbCYmXCJzeW1ib2xcIj09dHlwZW9mIFN5bWJvbC5pdGVyYXRvcj9mdW5jdGlvbih0KXtyZXR1cm4gdHlwZW9mIHR9OmZ1bmN0aW9uKHQpe3JldHVybiB0JiZcImZ1bmN0aW9uXCI9PXR5cGVvZiBTeW1ib2wmJnQuY29uc3RydWN0b3I9PT1TeW1ib2wmJnQhPT1TeW1ib2wucHJvdG90eXBlP1wic3ltYm9sXCI6dHlwZW9mIHR9OyFmdW5jdGlvbih0KXtcIm9iamVjdFwiPT09KFwidW5kZWZpbmVkXCI9PXR5cGVvZiBleHBvcnRzP1widW5kZWZpbmVkXCI6X3R5cGVvZihleHBvcnRzKSkmJlwidW5kZWZpbmVkXCIhPXR5cGVvZiBtb2R1bGU/bW9kdWxlLmV4cG9ydHM9dCgpOlwiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUodCk6d2luZG93LmltYWdlQ29udmVyc2lvbj10KCl9KGZ1bmN0aW9uKCl7dmFyIHQ9e307ZnVuY3Rpb24gZSh0KXtyZXR1cm5bXCJpbWFnZS9wbmdcIixcImltYWdlL2pwZWdcIixcImltYWdlL2dpZlwiXS5zb21lKGZ1bmN0aW9uKGUpe3JldHVybiBlPT09dH0pfXJldHVybiB0LnVybHRvSW1hZ2U9ZnVuY3Rpb24odCl7cmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uKGUsYSl7dmFyIG49bmV3IEltYWdlO24ub25sb2FkPWZ1bmN0aW9uKCl7cmV0dXJuIGUobil9LG4ub25lcnJvcj1mdW5jdGlvbigpe3JldHVybiBhKG5ldyBFcnJvcihcInVybHRvSW1hZ2UoKTogSW1hZ2UgZmFpbGVkIHRvIGxvYWQsIHBsZWFzZSBjaGVjayB0aGUgaW1hZ2UgVVJMXCIpKX0sbi5zcmM9dH0pfSx0LnVybHRvQmxvYj1mdW5jdGlvbih0KXtyZXR1cm4gZmV0Y2godCkudGhlbihmdW5jdGlvbih0KXtyZXR1cm4gdC5ibG9iKCl9KX0sdC5pbWFnZXRvQ2FudmFzPWFzeW5jIGZ1bmN0aW9uKHQpe3ZhciBlPWFyZ3VtZW50cy5sZW5ndGg+MSYmdm9pZCAwIT09YXJndW1lbnRzWzFdP2FyZ3VtZW50c1sxXTp7fSxhPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJjYW52YXNcIiksbj1hLmdldENvbnRleHQoXCIyZFwiKSxpPXZvaWQgMCxyPXZvaWQgMDtmb3IodmFyIG8gaW4gZSlPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoZSxvKSYmKGVbb109TnVtYmVyKGVbb10pKTtpZihlLnNjYWxlKXt2YXIgYz1lLnNjYWxlPjAmJmUuc2NhbGU8MTA/ZS5zY2FsZToxO3I9dC53aWR0aCpjLGk9dC5oZWlnaHQqY31lbHNlIHI9ZS53aWR0aHx8ZS5oZWlnaHQqdC53aWR0aC90LmhlaWdodHx8dC53aWR0aCxpPWUuaGVpZ2h0fHxlLndpZHRoKnQuaGVpZ2h0L3Qud2lkdGh8fHQuaGVpZ2h0O3N3aXRjaChbNSw2LDcsOF0uc29tZShmdW5jdGlvbih0KXtyZXR1cm4gdD09PWUub3JpZW50YXRpb259KT8oYS5oZWlnaHQ9cixhLndpZHRoPWkpOihhLmhlaWdodD1pLGEud2lkdGg9ciksZS5vcmllbnRhdGlvbil7Y2FzZSAzOm4ucm90YXRlKDE4MCpNYXRoLlBJLzE4MCksbi5kcmF3SW1hZ2UodCwtYS53aWR0aCwtYS5oZWlnaHQsYS53aWR0aCxhLmhlaWdodCk7YnJlYWs7Y2FzZSA2Om4ucm90YXRlKDkwKk1hdGguUEkvMTgwKSxuLmRyYXdJbWFnZSh0LDAsLWEud2lkdGgsYS5oZWlnaHQsYS53aWR0aCk7YnJlYWs7Y2FzZSA4Om4ucm90YXRlKDI3MCpNYXRoLlBJLzE4MCksbi5kcmF3SW1hZ2UodCwtYS5oZWlnaHQsMCxhLmhlaWdodCxhLndpZHRoKTticmVhaztjYXNlIDI6bi50cmFuc2xhdGUoYS53aWR0aCwwKSxuLnNjYWxlKC0xLDEpLG4uZHJhd0ltYWdlKHQsMCwwLGEud2lkdGgsYS5oZWlnaHQpO2JyZWFrO2Nhc2UgNDpuLnRyYW5zbGF0ZShhLndpZHRoLDApLG4uc2NhbGUoLTEsMSksbi5yb3RhdGUoMTgwKk1hdGguUEkvMTgwKSxuLmRyYXdJbWFnZSh0LC1hLndpZHRoLC1hLmhlaWdodCxhLndpZHRoLGEuaGVpZ2h0KTticmVhaztjYXNlIDU6bi50cmFuc2xhdGUoYS53aWR0aCwwKSxuLnNjYWxlKC0xLDEpLG4ucm90YXRlKDkwKk1hdGguUEkvMTgwKSxuLmRyYXdJbWFnZSh0LDAsLWEud2lkdGgsYS5oZWlnaHQsYS53aWR0aCk7YnJlYWs7Y2FzZSA3Om4udHJhbnNsYXRlKGEud2lkdGgsMCksbi5zY2FsZSgtMSwxKSxuLnJvdGF0ZSgyNzAqTWF0aC5QSS8xODApLG4uZHJhd0ltYWdlKHQsLWEuaGVpZ2h0LDAsYS5oZWlnaHQsYS53aWR0aCk7YnJlYWs7ZGVmYXVsdDpuLmRyYXdJbWFnZSh0LDAsMCxhLndpZHRoLGEuaGVpZ2h0KX1yZXR1cm4gYX0sdC5jYW52YXN0b0ZpbGU9ZnVuY3Rpb24odCxlKXt2YXIgYT1hcmd1bWVudHMubGVuZ3RoPjImJnZvaWQgMCE9PWFyZ3VtZW50c1syXT9hcmd1bWVudHNbMl06XCJpbWFnZS9qcGVnXCI7cmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uKG4pe3JldHVybiB0LnRvQmxvYihmdW5jdGlvbih0KXtyZXR1cm4gbih0KX0sYSxlKX0pfSx0LmNhbnZhc3RvRGF0YVVSTD1hc3luYyBmdW5jdGlvbih0LGEsbil7cmV0dXJuIGUobil8fChuPVwiaW1hZ2UvanBlZ1wiKSx0LnRvRGF0YVVSTChuLGEpfSx0LmZpbGV0b0RhdGFVUkw9ZnVuY3Rpb24odCl7cmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uKGUpe3ZhciBhPW5ldyBGaWxlUmVhZGVyO2Eub25sb2FkZW5kPWZ1bmN0aW9uKHQpe3JldHVybiBlKHQudGFyZ2V0LnJlc3VsdCl9LGEucmVhZEFzRGF0YVVSTCh0KX0pfSx0LmRhdGFVUkx0b0ltYWdlPWZ1bmN0aW9uKHQpe3JldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbihlLGEpe3ZhciBuPW5ldyBJbWFnZTtuLm9ubG9hZD1mdW5jdGlvbigpe3JldHVybiBlKG4pfSxuLm9uZXJyb3I9ZnVuY3Rpb24oKXtyZXR1cm4gYShuZXcgRXJyb3IoXCJkYXRhVVJMdG9JbWFnZSgpOiBkYXRhVVJMIGlzIGlsbGVnYWxcIikpfSxuLnNyYz10fSl9LHQuZGF0YVVSTHRvRmlsZT1hc3luYyBmdW5jdGlvbih0LGEpe2Zvcih2YXIgbj10LnNwbGl0KFwiLFwiKSxpPW5bMF0ubWF0Y2goLzooLio/KTsvKVsxXSxyPWF0b2IoblsxXSksbz1yLmxlbmd0aCxjPW5ldyBVaW50OEFycmF5KG8pO28tLTspY1tvXT1yLmNoYXJDb2RlQXQobyk7cmV0dXJuIGUoYSkmJihpPWEpLG5ldyBCbG9iKFtjXSx7dHlwZTppfSl9LHQuZG93bmxvYWRGaWxlPWZ1bmN0aW9uKHQsZSl7dmFyIGE9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImFcIik7YS5ocmVmPXdpbmRvdy5VUkwuY3JlYXRlT2JqZWN0VVJMKHQpLGEuZG93bmxvYWQ9ZXx8RGF0ZS5ub3coKS50b1N0cmluZygzNiksZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChhKTt2YXIgbj1kb2N1bWVudC5jcmVhdGVFdmVudChcIk1vdXNlRXZlbnRzXCIpO24uaW5pdEV2ZW50KFwiY2xpY2tcIiwhMSwhMSksYS5kaXNwYXRjaEV2ZW50KG4pLGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQoYSl9LHQuY29tcHJlc3M9YXN5bmMgZnVuY3Rpb24oYSl7dmFyIG49YXJndW1lbnRzLmxlbmd0aD4xJiZ2b2lkIDAhPT1hcmd1bWVudHNbMV0/YXJndW1lbnRzWzFdOnt9O2lmKCEoYSBpbnN0YW5jZW9mIEJsb2IpKXRocm93IG5ldyBFcnJvcihcImNvbXByZXNzKCk6IEZpcnN0IGFyZyBtdXN0IGJlIGEgQmxvYiBvYmplY3Qgb3IgYSBGaWxlIG9iamVjdC5cIik7aWYoXCJvYmplY3RcIiE9PSh2b2lkIDA9PT1uP1widW5kZWZpbmVkXCI6X3R5cGVvZihuKSkmJihuPU9iamVjdC5hc3NpZ24oe3F1YWxpdHk6bn0pKSxuLnF1YWxpdHk9TnVtYmVyKG4ucXVhbGl0eSksTnVtYmVyLmlzTmFOKG4ucXVhbGl0eSkpcmV0dXJuIGE7dmFyIGk9YXdhaXQgdC5maWxldG9EYXRhVVJMKGEpLHI9aS5zcGxpdChcIixcIilbMF0ubWF0Y2goLzooLio/KTsvKVsxXSxvPVwiaW1hZ2UvanBlZ1wiO2Uobi50eXBlKSYmKG89bi50eXBlLHI9bi50eXBlKTt2YXIgYz1hd2FpdCB0LmRhdGFVUkx0b0ltYWdlKGkpLHU9YXdhaXQgdC5pbWFnZXRvQ2FudmFzKGMsT2JqZWN0LmFzc2lnbih7fSxuKSkscz1hd2FpdCB0LmNhbnZhc3RvRGF0YVVSTCh1LG4ucXVhbGl0eSxvKTtyZXR1cm4gYXdhaXQgdC5kYXRhVVJMdG9GaWxlKHMscil9LHQuY29tcHJlc3NBY2N1cmF0ZWx5PWFzeW5jIGZ1bmN0aW9uKGEpe3ZhciBuPWFyZ3VtZW50cy5sZW5ndGg+MSYmdm9pZCAwIT09YXJndW1lbnRzWzFdP2FyZ3VtZW50c1sxXTp7fTtpZighKGEgaW5zdGFuY2VvZiBCbG9iKSl0aHJvdyBuZXcgRXJyb3IoXCJjb21wcmVzc0FjY3VyYXRlbHkoKTogRmlyc3QgYXJnIG11c3QgYmUgYSBCbG9iIG9iamVjdCBvciBhIEZpbGUgb2JqZWN0LlwiKTtpZihcIm9iamVjdFwiIT09KHZvaWQgMD09PW4/XCJ1bmRlZmluZWRcIjpfdHlwZW9mKG4pKSYmKG49T2JqZWN0LmFzc2lnbih7c2l6ZTpufSkpLG4uc2l6ZT1OdW1iZXIobi5zaXplKSxOdW1iZXIuaXNOYU4obi5zaXplKSlyZXR1cm4gYTtpZigxMDI0Km4uc2l6ZT5hLnNpemUpcmV0dXJuIGE7bi5hY2N1cmFjeT1OdW1iZXIobi5hY2N1cmFjeSksKCFuLmFjY3VyYWN5fHxuLmFjY3VyYWN5PC44fHxuLmFjY3VyYWN5Pi45OSkmJihuLmFjY3VyYWN5PS45NSk7dmFyIGk9bi5zaXplKigyLW4uYWNjdXJhY3kpKjEwMjQscj0xMDI0Km4uc2l6ZSxvPW4uc2l6ZSpuLmFjY3VyYWN5KjEwMjQsYz1hd2FpdCB0LmZpbGV0b0RhdGFVUkwoYSksdT1jLnNwbGl0KFwiLFwiKVswXS5tYXRjaCgvOiguKj8pOy8pWzFdLHM9XCJpbWFnZS9qcGVnXCI7ZShuLnR5cGUpJiYocz1uLnR5cGUsdT1uLnR5cGUpO2Zvcih2YXIgaD1hd2FpdCB0LmRhdGFVUkx0b0ltYWdlKGMpLGQ9YXdhaXQgdC5pbWFnZXRvQ2FudmFzKGgsT2JqZWN0LmFzc2lnbih7fSxuKSksbD0uNSxmPXZvaWQgMCxnPVtudWxsLG51bGxdLG09MTttPD03O20rKyl7dmFyIHc9Ljc1KihmPWF3YWl0IHQuY2FudmFzdG9EYXRhVVJMKGQsbCxzKSkubGVuZ3RoO2lmKDc9PT1tKXsoaTx3fHxvPncpJiYoZj1bZl0uY29uY2F0KGcpLmZpbHRlcihmdW5jdGlvbih0KXtyZXR1cm4gdH0pLnNvcnQoZnVuY3Rpb24odCxlKXtyZXR1cm4gTWF0aC5hYnMoLjc1KnQubGVuZ3RoLXIpLU1hdGguYWJzKC43NSplLmxlbmd0aC1yKX0pWzBdKTticmVha31pZihpPHcpZ1sxXT1mLGwtPS41KioobSsxKTtlbHNle2lmKCEobz53KSlicmVhaztnWzBdPWYsbCs9LjUqKihtKzEpfX12YXIgeT1hd2FpdCB0LmRhdGFVUkx0b0ZpbGUoZix1KTtyZXR1cm4geS5zaXplPmEuc2l6ZT9hOnl9LHR9KTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7Ozs7O0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/image-conversion/build/conversion.js
