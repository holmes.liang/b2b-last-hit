var Pattern = function Pattern(image, repeat) {
  // Should do nothing more in this constructor. Because gradient can be
  // declard by `color: {image: ...}`, where this constructor will not be called.
  this.image = image;
  this.repeat = repeat; // Can be cloned

  this.type = 'pattern';
};

Pattern.prototype.getCanvasPattern = function (ctx) {
  return ctx.createPattern(this.image, this.repeat || 'repeat');
};

var _default = Pattern;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9QYXR0ZXJuLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9QYXR0ZXJuLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBQYXR0ZXJuID0gZnVuY3Rpb24gKGltYWdlLCByZXBlYXQpIHtcbiAgLy8gU2hvdWxkIGRvIG5vdGhpbmcgbW9yZSBpbiB0aGlzIGNvbnN0cnVjdG9yLiBCZWNhdXNlIGdyYWRpZW50IGNhbiBiZVxuICAvLyBkZWNsYXJkIGJ5IGBjb2xvcjoge2ltYWdlOiAuLi59YCwgd2hlcmUgdGhpcyBjb25zdHJ1Y3RvciB3aWxsIG5vdCBiZSBjYWxsZWQuXG4gIHRoaXMuaW1hZ2UgPSBpbWFnZTtcbiAgdGhpcy5yZXBlYXQgPSByZXBlYXQ7IC8vIENhbiBiZSBjbG9uZWRcblxuICB0aGlzLnR5cGUgPSAncGF0dGVybic7XG59O1xuXG5QYXR0ZXJuLnByb3RvdHlwZS5nZXRDYW52YXNQYXR0ZXJuID0gZnVuY3Rpb24gKGN0eCkge1xuICByZXR1cm4gY3R4LmNyZWF0ZVBhdHRlcm4odGhpcy5pbWFnZSwgdGhpcy5yZXBlYXQgfHwgJ3JlcGVhdCcpO1xufTtcblxudmFyIF9kZWZhdWx0ID0gUGF0dGVybjtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/Pattern.js
