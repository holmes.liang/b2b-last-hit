__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  // Options.jsx
  items_per_page: '条/页',
  jump_to: '跳至',
  jump_to_confirm: '确定',
  page: '页',
  // Pagination.jsx
  prev_page: '上一页',
  next_page: '下一页',
  prev_5: '向前 5 页',
  next_5: '向后 5 页',
  prev_3: '向前 3 页',
  next_3: '向后 3 页'
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtcGFnaW5hdGlvbi9lcy9sb2NhbGUvemhfQ04uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1wYWdpbmF0aW9uL2VzL2xvY2FsZS96aF9DTi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCB7XG4gIC8vIE9wdGlvbnMuanN4XG4gIGl0ZW1zX3Blcl9wYWdlOiAn5p2hL+mhtScsXG4gIGp1bXBfdG86ICfot7Poh7MnLFxuICBqdW1wX3RvX2NvbmZpcm06ICfnoa7lrponLFxuICBwYWdlOiAn6aG1JyxcblxuICAvLyBQYWdpbmF0aW9uLmpzeFxuICBwcmV2X3BhZ2U6ICfkuIrkuIDpobUnLFxuICBuZXh0X3BhZ2U6ICfkuIvkuIDpobUnLFxuICBwcmV2XzU6ICflkJHliY0gNSDpobUnLFxuICBuZXh0XzU6ICflkJHlkI4gNSDpobUnLFxuICBwcmV2XzM6ICflkJHliY0gMyDpobUnLFxuICBuZXh0XzM6ICflkJHlkI4gMyDpobUnXG59OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-pagination/es/locale/zh_CN.js
