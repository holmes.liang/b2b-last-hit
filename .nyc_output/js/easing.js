/**
 * 缓动代码来自 https://github.com/sole/tween.js/blob/master/src/Tween.js
 * @see http://sole.github.io/tween.js/examples/03_graphs.html
 * @exports zrender/animation/easing
 */
var easing = {
  /**
  * @param {number} k
  * @return {number}
  */
  linear: function linear(k) {
    return k;
  },

  /**
  * @param {number} k
  * @return {number}
  */
  quadraticIn: function quadraticIn(k) {
    return k * k;
  },

  /**
  * @param {number} k
  * @return {number}
  */
  quadraticOut: function quadraticOut(k) {
    return k * (2 - k);
  },

  /**
  * @param {number} k
  * @return {number}
  */
  quadraticInOut: function quadraticInOut(k) {
    if ((k *= 2) < 1) {
      return 0.5 * k * k;
    }

    return -0.5 * (--k * (k - 2) - 1);
  },
  // 三次方的缓动（t^3）

  /**
  * @param {number} k
  * @return {number}
  */
  cubicIn: function cubicIn(k) {
    return k * k * k;
  },

  /**
  * @param {number} k
  * @return {number}
  */
  cubicOut: function cubicOut(k) {
    return --k * k * k + 1;
  },

  /**
  * @param {number} k
  * @return {number}
  */
  cubicInOut: function cubicInOut(k) {
    if ((k *= 2) < 1) {
      return 0.5 * k * k * k;
    }

    return 0.5 * ((k -= 2) * k * k + 2);
  },
  // 四次方的缓动（t^4）

  /**
  * @param {number} k
  * @return {number}
  */
  quarticIn: function quarticIn(k) {
    return k * k * k * k;
  },

  /**
  * @param {number} k
  * @return {number}
  */
  quarticOut: function quarticOut(k) {
    return 1 - --k * k * k * k;
  },

  /**
  * @param {number} k
  * @return {number}
  */
  quarticInOut: function quarticInOut(k) {
    if ((k *= 2) < 1) {
      return 0.5 * k * k * k * k;
    }

    return -0.5 * ((k -= 2) * k * k * k - 2);
  },
  // 五次方的缓动（t^5）

  /**
  * @param {number} k
  * @return {number}
  */
  quinticIn: function quinticIn(k) {
    return k * k * k * k * k;
  },

  /**
  * @param {number} k
  * @return {number}
  */
  quinticOut: function quinticOut(k) {
    return --k * k * k * k * k + 1;
  },

  /**
  * @param {number} k
  * @return {number}
  */
  quinticInOut: function quinticInOut(k) {
    if ((k *= 2) < 1) {
      return 0.5 * k * k * k * k * k;
    }

    return 0.5 * ((k -= 2) * k * k * k * k + 2);
  },
  // 正弦曲线的缓动（sin(t)）

  /**
  * @param {number} k
  * @return {number}
  */
  sinusoidalIn: function sinusoidalIn(k) {
    return 1 - Math.cos(k * Math.PI / 2);
  },

  /**
  * @param {number} k
  * @return {number}
  */
  sinusoidalOut: function sinusoidalOut(k) {
    return Math.sin(k * Math.PI / 2);
  },

  /**
  * @param {number} k
  * @return {number}
  */
  sinusoidalInOut: function sinusoidalInOut(k) {
    return 0.5 * (1 - Math.cos(Math.PI * k));
  },
  // 指数曲线的缓动（2^t）

  /**
  * @param {number} k
  * @return {number}
  */
  exponentialIn: function exponentialIn(k) {
    return k === 0 ? 0 : Math.pow(1024, k - 1);
  },

  /**
  * @param {number} k
  * @return {number}
  */
  exponentialOut: function exponentialOut(k) {
    return k === 1 ? 1 : 1 - Math.pow(2, -10 * k);
  },

  /**
  * @param {number} k
  * @return {number}
  */
  exponentialInOut: function exponentialInOut(k) {
    if (k === 0) {
      return 0;
    }

    if (k === 1) {
      return 1;
    }

    if ((k *= 2) < 1) {
      return 0.5 * Math.pow(1024, k - 1);
    }

    return 0.5 * (-Math.pow(2, -10 * (k - 1)) + 2);
  },
  // 圆形曲线的缓动（sqrt(1-t^2)）

  /**
  * @param {number} k
  * @return {number}
  */
  circularIn: function circularIn(k) {
    return 1 - Math.sqrt(1 - k * k);
  },

  /**
  * @param {number} k
  * @return {number}
  */
  circularOut: function circularOut(k) {
    return Math.sqrt(1 - --k * k);
  },

  /**
  * @param {number} k
  * @return {number}
  */
  circularInOut: function circularInOut(k) {
    if ((k *= 2) < 1) {
      return -0.5 * (Math.sqrt(1 - k * k) - 1);
    }

    return 0.5 * (Math.sqrt(1 - (k -= 2) * k) + 1);
  },
  // 创建类似于弹簧在停止前来回振荡的动画

  /**
  * @param {number} k
  * @return {number}
  */
  elasticIn: function elasticIn(k) {
    var s;
    var a = 0.1;
    var p = 0.4;

    if (k === 0) {
      return 0;
    }

    if (k === 1) {
      return 1;
    }

    if (!a || a < 1) {
      a = 1;
      s = p / 4;
    } else {
      s = p * Math.asin(1 / a) / (2 * Math.PI);
    }

    return -(a * Math.pow(2, 10 * (k -= 1)) * Math.sin((k - s) * (2 * Math.PI) / p));
  },

  /**
  * @param {number} k
  * @return {number}
  */
  elasticOut: function elasticOut(k) {
    var s;
    var a = 0.1;
    var p = 0.4;

    if (k === 0) {
      return 0;
    }

    if (k === 1) {
      return 1;
    }

    if (!a || a < 1) {
      a = 1;
      s = p / 4;
    } else {
      s = p * Math.asin(1 / a) / (2 * Math.PI);
    }

    return a * Math.pow(2, -10 * k) * Math.sin((k - s) * (2 * Math.PI) / p) + 1;
  },

  /**
  * @param {number} k
  * @return {number}
  */
  elasticInOut: function elasticInOut(k) {
    var s;
    var a = 0.1;
    var p = 0.4;

    if (k === 0) {
      return 0;
    }

    if (k === 1) {
      return 1;
    }

    if (!a || a < 1) {
      a = 1;
      s = p / 4;
    } else {
      s = p * Math.asin(1 / a) / (2 * Math.PI);
    }

    if ((k *= 2) < 1) {
      return -0.5 * (a * Math.pow(2, 10 * (k -= 1)) * Math.sin((k - s) * (2 * Math.PI) / p));
    }

    return a * Math.pow(2, -10 * (k -= 1)) * Math.sin((k - s) * (2 * Math.PI) / p) * 0.5 + 1;
  },
  // 在某一动画开始沿指示的路径进行动画处理前稍稍收回该动画的移动

  /**
  * @param {number} k
  * @return {number}
  */
  backIn: function backIn(k) {
    var s = 1.70158;
    return k * k * ((s + 1) * k - s);
  },

  /**
  * @param {number} k
  * @return {number}
  */
  backOut: function backOut(k) {
    var s = 1.70158;
    return --k * k * ((s + 1) * k + s) + 1;
  },

  /**
  * @param {number} k
  * @return {number}
  */
  backInOut: function backInOut(k) {
    var s = 1.70158 * 1.525;

    if ((k *= 2) < 1) {
      return 0.5 * (k * k * ((s + 1) * k - s));
    }

    return 0.5 * ((k -= 2) * k * ((s + 1) * k + s) + 2);
  },
  // 创建弹跳效果

  /**
  * @param {number} k
  * @return {number}
  */
  bounceIn: function bounceIn(k) {
    return 1 - easing.bounceOut(1 - k);
  },

  /**
  * @param {number} k
  * @return {number}
  */
  bounceOut: function bounceOut(k) {
    if (k < 1 / 2.75) {
      return 7.5625 * k * k;
    } else if (k < 2 / 2.75) {
      return 7.5625 * (k -= 1.5 / 2.75) * k + 0.75;
    } else if (k < 2.5 / 2.75) {
      return 7.5625 * (k -= 2.25 / 2.75) * k + 0.9375;
    } else {
      return 7.5625 * (k -= 2.625 / 2.75) * k + 0.984375;
    }
  },

  /**
  * @param {number} k
  * @return {number}
  */
  bounceInOut: function bounceInOut(k) {
    if (k < 0.5) {
      return easing.bounceIn(k * 2) * 0.5;
    }

    return easing.bounceOut(k * 2 - 1) * 0.5 + 0.5;
  }
};
var _default = easing;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvYW5pbWF0aW9uL2Vhc2luZy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2FuaW1hdGlvbi9lYXNpbmcuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiDnvJPliqjku6PnoIHmnaXoh6ogaHR0cHM6Ly9naXRodWIuY29tL3NvbGUvdHdlZW4uanMvYmxvYi9tYXN0ZXIvc3JjL1R3ZWVuLmpzXG4gKiBAc2VlIGh0dHA6Ly9zb2xlLmdpdGh1Yi5pby90d2Vlbi5qcy9leGFtcGxlcy8wM19ncmFwaHMuaHRtbFxuICogQGV4cG9ydHMgenJlbmRlci9hbmltYXRpb24vZWFzaW5nXG4gKi9cbnZhciBlYXNpbmcgPSB7XG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBsaW5lYXI6IGZ1bmN0aW9uIChrKSB7XG4gICAgcmV0dXJuIGs7XG4gIH0sXG5cbiAgLyoqXG4gICogQHBhcmFtIHtudW1iZXJ9IGtcbiAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICovXG4gIHF1YWRyYXRpY0luOiBmdW5jdGlvbiAoaykge1xuICAgIHJldHVybiBrICogaztcbiAgfSxcblxuICAvKipcbiAgKiBAcGFyYW0ge251bWJlcn0ga1xuICAqIEByZXR1cm4ge251bWJlcn1cbiAgKi9cbiAgcXVhZHJhdGljT3V0OiBmdW5jdGlvbiAoaykge1xuICAgIHJldHVybiBrICogKDIgLSBrKTtcbiAgfSxcblxuICAvKipcbiAgKiBAcGFyYW0ge251bWJlcn0ga1xuICAqIEByZXR1cm4ge251bWJlcn1cbiAgKi9cbiAgcXVhZHJhdGljSW5PdXQ6IGZ1bmN0aW9uIChrKSB7XG4gICAgaWYgKChrICo9IDIpIDwgMSkge1xuICAgICAgcmV0dXJuIDAuNSAqIGsgKiBrO1xuICAgIH1cblxuICAgIHJldHVybiAtMC41ICogKC0tayAqIChrIC0gMikgLSAxKTtcbiAgfSxcbiAgLy8g5LiJ5qyh5pa555qE57yT5Yqo77yIdF4z77yJXG5cbiAgLyoqXG4gICogQHBhcmFtIHtudW1iZXJ9IGtcbiAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICovXG4gIGN1YmljSW46IGZ1bmN0aW9uIChrKSB7XG4gICAgcmV0dXJuIGsgKiBrICogaztcbiAgfSxcblxuICAvKipcbiAgKiBAcGFyYW0ge251bWJlcn0ga1xuICAqIEByZXR1cm4ge251bWJlcn1cbiAgKi9cbiAgY3ViaWNPdXQ6IGZ1bmN0aW9uIChrKSB7XG4gICAgcmV0dXJuIC0tayAqIGsgKiBrICsgMTtcbiAgfSxcblxuICAvKipcbiAgKiBAcGFyYW0ge251bWJlcn0ga1xuICAqIEByZXR1cm4ge251bWJlcn1cbiAgKi9cbiAgY3ViaWNJbk91dDogZnVuY3Rpb24gKGspIHtcbiAgICBpZiAoKGsgKj0gMikgPCAxKSB7XG4gICAgICByZXR1cm4gMC41ICogayAqIGsgKiBrO1xuICAgIH1cblxuICAgIHJldHVybiAwLjUgKiAoKGsgLT0gMikgKiBrICogayArIDIpO1xuICB9LFxuICAvLyDlm5vmrKHmlrnnmoTnvJPliqjvvIh0XjTvvIlcblxuICAvKipcbiAgKiBAcGFyYW0ge251bWJlcn0ga1xuICAqIEByZXR1cm4ge251bWJlcn1cbiAgKi9cbiAgcXVhcnRpY0luOiBmdW5jdGlvbiAoaykge1xuICAgIHJldHVybiBrICogayAqIGsgKiBrO1xuICB9LFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBxdWFydGljT3V0OiBmdW5jdGlvbiAoaykge1xuICAgIHJldHVybiAxIC0gLS1rICogayAqIGsgKiBrO1xuICB9LFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBxdWFydGljSW5PdXQ6IGZ1bmN0aW9uIChrKSB7XG4gICAgaWYgKChrICo9IDIpIDwgMSkge1xuICAgICAgcmV0dXJuIDAuNSAqIGsgKiBrICogayAqIGs7XG4gICAgfVxuXG4gICAgcmV0dXJuIC0wLjUgKiAoKGsgLT0gMikgKiBrICogayAqIGsgLSAyKTtcbiAgfSxcbiAgLy8g5LqU5qyh5pa555qE57yT5Yqo77yIdF4177yJXG5cbiAgLyoqXG4gICogQHBhcmFtIHtudW1iZXJ9IGtcbiAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICovXG4gIHF1aW50aWNJbjogZnVuY3Rpb24gKGspIHtcbiAgICByZXR1cm4gayAqIGsgKiBrICogayAqIGs7XG4gIH0sXG5cbiAgLyoqXG4gICogQHBhcmFtIHtudW1iZXJ9IGtcbiAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICovXG4gIHF1aW50aWNPdXQ6IGZ1bmN0aW9uIChrKSB7XG4gICAgcmV0dXJuIC0tayAqIGsgKiBrICogayAqIGsgKyAxO1xuICB9LFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBxdWludGljSW5PdXQ6IGZ1bmN0aW9uIChrKSB7XG4gICAgaWYgKChrICo9IDIpIDwgMSkge1xuICAgICAgcmV0dXJuIDAuNSAqIGsgKiBrICogayAqIGsgKiBrO1xuICAgIH1cblxuICAgIHJldHVybiAwLjUgKiAoKGsgLT0gMikgKiBrICogayAqIGsgKiBrICsgMik7XG4gIH0sXG4gIC8vIOato+W8puabsue6v+eahOe8k+WKqO+8iHNpbih0Ke+8iVxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBzaW51c29pZGFsSW46IGZ1bmN0aW9uIChrKSB7XG4gICAgcmV0dXJuIDEgLSBNYXRoLmNvcyhrICogTWF0aC5QSSAvIDIpO1xuICB9LFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBzaW51c29pZGFsT3V0OiBmdW5jdGlvbiAoaykge1xuICAgIHJldHVybiBNYXRoLnNpbihrICogTWF0aC5QSSAvIDIpO1xuICB9LFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBzaW51c29pZGFsSW5PdXQ6IGZ1bmN0aW9uIChrKSB7XG4gICAgcmV0dXJuIDAuNSAqICgxIC0gTWF0aC5jb3MoTWF0aC5QSSAqIGspKTtcbiAgfSxcbiAgLy8g5oyH5pWw5puy57q/55qE57yT5Yqo77yIMl5077yJXG5cbiAgLyoqXG4gICogQHBhcmFtIHtudW1iZXJ9IGtcbiAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICovXG4gIGV4cG9uZW50aWFsSW46IGZ1bmN0aW9uIChrKSB7XG4gICAgcmV0dXJuIGsgPT09IDAgPyAwIDogTWF0aC5wb3coMTAyNCwgayAtIDEpO1xuICB9LFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBleHBvbmVudGlhbE91dDogZnVuY3Rpb24gKGspIHtcbiAgICByZXR1cm4gayA9PT0gMSA/IDEgOiAxIC0gTWF0aC5wb3coMiwgLTEwICogayk7XG4gIH0sXG5cbiAgLyoqXG4gICogQHBhcmFtIHtudW1iZXJ9IGtcbiAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICovXG4gIGV4cG9uZW50aWFsSW5PdXQ6IGZ1bmN0aW9uIChrKSB7XG4gICAgaWYgKGsgPT09IDApIHtcbiAgICAgIHJldHVybiAwO1xuICAgIH1cblxuICAgIGlmIChrID09PSAxKSB7XG4gICAgICByZXR1cm4gMTtcbiAgICB9XG5cbiAgICBpZiAoKGsgKj0gMikgPCAxKSB7XG4gICAgICByZXR1cm4gMC41ICogTWF0aC5wb3coMTAyNCwgayAtIDEpO1xuICAgIH1cblxuICAgIHJldHVybiAwLjUgKiAoLU1hdGgucG93KDIsIC0xMCAqIChrIC0gMSkpICsgMik7XG4gIH0sXG4gIC8vIOWchuW9ouabsue6v+eahOe8k+WKqO+8iHNxcnQoMS10XjIp77yJXG5cbiAgLyoqXG4gICogQHBhcmFtIHtudW1iZXJ9IGtcbiAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICovXG4gIGNpcmN1bGFySW46IGZ1bmN0aW9uIChrKSB7XG4gICAgcmV0dXJuIDEgLSBNYXRoLnNxcnQoMSAtIGsgKiBrKTtcbiAgfSxcblxuICAvKipcbiAgKiBAcGFyYW0ge251bWJlcn0ga1xuICAqIEByZXR1cm4ge251bWJlcn1cbiAgKi9cbiAgY2lyY3VsYXJPdXQ6IGZ1bmN0aW9uIChrKSB7XG4gICAgcmV0dXJuIE1hdGguc3FydCgxIC0gLS1rICogayk7XG4gIH0sXG5cbiAgLyoqXG4gICogQHBhcmFtIHtudW1iZXJ9IGtcbiAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICovXG4gIGNpcmN1bGFySW5PdXQ6IGZ1bmN0aW9uIChrKSB7XG4gICAgaWYgKChrICo9IDIpIDwgMSkge1xuICAgICAgcmV0dXJuIC0wLjUgKiAoTWF0aC5zcXJ0KDEgLSBrICogaykgLSAxKTtcbiAgICB9XG5cbiAgICByZXR1cm4gMC41ICogKE1hdGguc3FydCgxIC0gKGsgLT0gMikgKiBrKSArIDEpO1xuICB9LFxuICAvLyDliJvlu7rnsbvkvLzkuo7lvLnnsKflnKjlgZzmraLliY3mnaXlm57mjK/ojaHnmoTliqjnlLtcblxuICAvKipcbiAgKiBAcGFyYW0ge251bWJlcn0ga1xuICAqIEByZXR1cm4ge251bWJlcn1cbiAgKi9cbiAgZWxhc3RpY0luOiBmdW5jdGlvbiAoaykge1xuICAgIHZhciBzO1xuICAgIHZhciBhID0gMC4xO1xuICAgIHZhciBwID0gMC40O1xuXG4gICAgaWYgKGsgPT09IDApIHtcbiAgICAgIHJldHVybiAwO1xuICAgIH1cblxuICAgIGlmIChrID09PSAxKSB7XG4gICAgICByZXR1cm4gMTtcbiAgICB9XG5cbiAgICBpZiAoIWEgfHwgYSA8IDEpIHtcbiAgICAgIGEgPSAxO1xuICAgICAgcyA9IHAgLyA0O1xuICAgIH0gZWxzZSB7XG4gICAgICBzID0gcCAqIE1hdGguYXNpbigxIC8gYSkgLyAoMiAqIE1hdGguUEkpO1xuICAgIH1cblxuICAgIHJldHVybiAtKGEgKiBNYXRoLnBvdygyLCAxMCAqIChrIC09IDEpKSAqIE1hdGguc2luKChrIC0gcykgKiAoMiAqIE1hdGguUEkpIC8gcCkpO1xuICB9LFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBlbGFzdGljT3V0OiBmdW5jdGlvbiAoaykge1xuICAgIHZhciBzO1xuICAgIHZhciBhID0gMC4xO1xuICAgIHZhciBwID0gMC40O1xuXG4gICAgaWYgKGsgPT09IDApIHtcbiAgICAgIHJldHVybiAwO1xuICAgIH1cblxuICAgIGlmIChrID09PSAxKSB7XG4gICAgICByZXR1cm4gMTtcbiAgICB9XG5cbiAgICBpZiAoIWEgfHwgYSA8IDEpIHtcbiAgICAgIGEgPSAxO1xuICAgICAgcyA9IHAgLyA0O1xuICAgIH0gZWxzZSB7XG4gICAgICBzID0gcCAqIE1hdGguYXNpbigxIC8gYSkgLyAoMiAqIE1hdGguUEkpO1xuICAgIH1cblxuICAgIHJldHVybiBhICogTWF0aC5wb3coMiwgLTEwICogaykgKiBNYXRoLnNpbigoayAtIHMpICogKDIgKiBNYXRoLlBJKSAvIHApICsgMTtcbiAgfSxcblxuICAvKipcbiAgKiBAcGFyYW0ge251bWJlcn0ga1xuICAqIEByZXR1cm4ge251bWJlcn1cbiAgKi9cbiAgZWxhc3RpY0luT3V0OiBmdW5jdGlvbiAoaykge1xuICAgIHZhciBzO1xuICAgIHZhciBhID0gMC4xO1xuICAgIHZhciBwID0gMC40O1xuXG4gICAgaWYgKGsgPT09IDApIHtcbiAgICAgIHJldHVybiAwO1xuICAgIH1cblxuICAgIGlmIChrID09PSAxKSB7XG4gICAgICByZXR1cm4gMTtcbiAgICB9XG5cbiAgICBpZiAoIWEgfHwgYSA8IDEpIHtcbiAgICAgIGEgPSAxO1xuICAgICAgcyA9IHAgLyA0O1xuICAgIH0gZWxzZSB7XG4gICAgICBzID0gcCAqIE1hdGguYXNpbigxIC8gYSkgLyAoMiAqIE1hdGguUEkpO1xuICAgIH1cblxuICAgIGlmICgoayAqPSAyKSA8IDEpIHtcbiAgICAgIHJldHVybiAtMC41ICogKGEgKiBNYXRoLnBvdygyLCAxMCAqIChrIC09IDEpKSAqIE1hdGguc2luKChrIC0gcykgKiAoMiAqIE1hdGguUEkpIC8gcCkpO1xuICAgIH1cblxuICAgIHJldHVybiBhICogTWF0aC5wb3coMiwgLTEwICogKGsgLT0gMSkpICogTWF0aC5zaW4oKGsgLSBzKSAqICgyICogTWF0aC5QSSkgLyBwKSAqIDAuNSArIDE7XG4gIH0sXG4gIC8vIOWcqOafkOS4gOWKqOeUu+W8gOWni+ayv+aMh+ekuueahOi3r+W+hOi/m+ihjOWKqOeUu+WkhOeQhuWJjeeojeeojeaUtuWbnuivpeWKqOeUu+eahOenu+WKqFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBiYWNrSW46IGZ1bmN0aW9uIChrKSB7XG4gICAgdmFyIHMgPSAxLjcwMTU4O1xuICAgIHJldHVybiBrICogayAqICgocyArIDEpICogayAtIHMpO1xuICB9LFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBiYWNrT3V0OiBmdW5jdGlvbiAoaykge1xuICAgIHZhciBzID0gMS43MDE1ODtcbiAgICByZXR1cm4gLS1rICogayAqICgocyArIDEpICogayArIHMpICsgMTtcbiAgfSxcblxuICAvKipcbiAgKiBAcGFyYW0ge251bWJlcn0ga1xuICAqIEByZXR1cm4ge251bWJlcn1cbiAgKi9cbiAgYmFja0luT3V0OiBmdW5jdGlvbiAoaykge1xuICAgIHZhciBzID0gMS43MDE1OCAqIDEuNTI1O1xuXG4gICAgaWYgKChrICo9IDIpIDwgMSkge1xuICAgICAgcmV0dXJuIDAuNSAqIChrICogayAqICgocyArIDEpICogayAtIHMpKTtcbiAgICB9XG5cbiAgICByZXR1cm4gMC41ICogKChrIC09IDIpICogayAqICgocyArIDEpICogayArIHMpICsgMik7XG4gIH0sXG4gIC8vIOWIm+W7uuW8uei3s+aViOaenFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBib3VuY2VJbjogZnVuY3Rpb24gKGspIHtcbiAgICByZXR1cm4gMSAtIGVhc2luZy5ib3VuY2VPdXQoMSAtIGspO1xuICB9LFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBib3VuY2VPdXQ6IGZ1bmN0aW9uIChrKSB7XG4gICAgaWYgKGsgPCAxIC8gMi43NSkge1xuICAgICAgcmV0dXJuIDcuNTYyNSAqIGsgKiBrO1xuICAgIH0gZWxzZSBpZiAoayA8IDIgLyAyLjc1KSB7XG4gICAgICByZXR1cm4gNy41NjI1ICogKGsgLT0gMS41IC8gMi43NSkgKiBrICsgMC43NTtcbiAgICB9IGVsc2UgaWYgKGsgPCAyLjUgLyAyLjc1KSB7XG4gICAgICByZXR1cm4gNy41NjI1ICogKGsgLT0gMi4yNSAvIDIuNzUpICogayArIDAuOTM3NTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIDcuNTYyNSAqIChrIC09IDIuNjI1IC8gMi43NSkgKiBrICsgMC45ODQzNzU7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAqIEBwYXJhbSB7bnVtYmVyfSBrXG4gICogQHJldHVybiB7bnVtYmVyfVxuICAqL1xuICBib3VuY2VJbk91dDogZnVuY3Rpb24gKGspIHtcbiAgICBpZiAoayA8IDAuNSkge1xuICAgICAgcmV0dXJuIGVhc2luZy5ib3VuY2VJbihrICogMikgKiAwLjU7XG4gICAgfVxuXG4gICAgcmV0dXJuIGVhc2luZy5ib3VuY2VPdXQoayAqIDIgLSAxKSAqIDAuNSArIDAuNTtcbiAgfVxufTtcbnZhciBfZGVmYXVsdCA9IGVhc2luZztcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7QUFLQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpYQTtBQW1YQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/animation/easing.js
