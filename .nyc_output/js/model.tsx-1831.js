__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModelSevice", function() { return ModelSevice; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");


var ModelSevice =
/*#__PURE__*/
function () {
  function ModelSevice() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, ModelSevice);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(ModelSevice, null, [{
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix, rootPrefix) {
      var result = [rootPrefix, dataIdPrefix, propName].filter(function (item) {
        return item;
      });
      return result.join(".");
    }
  }, {
    key: "setValueIfUndefined",
    value: function setValueIfUndefined(_ref) {
      var propsName = _ref.propsName,
          value = _ref.value,
          that = _ref.that;
      if (!that) return;

      if (that.getValueFromModel(propsName) === undefined || that.getValueFromModel(propsName) === null) {
        that.setValueToModel(value, propsName);
      }
    }
  }]);

  return ModelSevice;
}();//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svc2VydmljZXMvbW9kZWwudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svc2VydmljZXMvbW9kZWwudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBNb2RlbFNldmljZSB7XG4gIHN0YXRpYyBnZW5lcmF0ZVByb3BOYW1lKHByb3BOYW1lOiBzdHJpbmcsIGRhdGFJZFByZWZpeD86IHN0cmluZywgcm9vdFByZWZpeD86IHN0cmluZyk6IHN0cmluZyB7XG4gICAgbGV0IHJlc3VsdCA9IFtyb290UHJlZml4LCBkYXRhSWRQcmVmaXgsIHByb3BOYW1lXS5maWx0ZXIoKGl0ZW06IGFueSkgPT4gaXRlbSk7XG4gICAgcmV0dXJuIHJlc3VsdC5qb2luKFwiLlwiKTtcbiAgfVxuXG4gIHN0YXRpYyBzZXRWYWx1ZUlmVW5kZWZpbmVkKHsgcHJvcHNOYW1lLCB2YWx1ZSwgdGhhdCB9OiB7IHByb3BzTmFtZTogc3RyaW5nLCB2YWx1ZTogYW55LCB0aGF0OiBhbnkgfSkge1xuICAgIGlmICghdGhhdCkgcmV0dXJuO1xuICAgIGlmICh0aGF0LmdldFZhbHVlRnJvbU1vZGVsKHByb3BzTmFtZSkgPT09IHVuZGVmaW5lZCB8fCB0aGF0LmdldFZhbHVlRnJvbU1vZGVsKHByb3BzTmFtZSkgPT09IG51bGwpIHtcbiAgICAgIHRoYXQuc2V0VmFsdWVUb01vZGVsKHZhbHVlLCBwcm9wc05hbWUpO1xuICAgIH1cbiAgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFYQTtBQUNBO0FBREE7QUFBQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/services/model.tsx
