/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule editOnCopy
 * @format
 * 
 */


var getFragmentFromSelection = __webpack_require__(/*! ./getFragmentFromSelection */ "./node_modules/draft-js/lib/getFragmentFromSelection.js");
/**
 * If we have a selection, create a ContentState fragment and store
 * it in our internal clipboard. Subsequent paste events will use this
 * fragment if no external clipboard data is supplied.
 */


function editOnCopy(editor, e) {
  var editorState = editor._latestEditorState;
  var selection = editorState.getSelection(); // No selection, so there's nothing to copy.

  if (selection.isCollapsed()) {
    e.preventDefault();
    return;
  }

  editor.setClipboard(getFragmentFromSelection(editor._latestEditorState));
}

module.exports = editOnCopy;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbkNvcHkuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvZWRpdE9uQ29weS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGVkaXRPbkNvcHlcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGdldEZyYWdtZW50RnJvbVNlbGVjdGlvbiA9IHJlcXVpcmUoJy4vZ2V0RnJhZ21lbnRGcm9tU2VsZWN0aW9uJyk7XG5cbi8qKlxuICogSWYgd2UgaGF2ZSBhIHNlbGVjdGlvbiwgY3JlYXRlIGEgQ29udGVudFN0YXRlIGZyYWdtZW50IGFuZCBzdG9yZVxuICogaXQgaW4gb3VyIGludGVybmFsIGNsaXBib2FyZC4gU3Vic2VxdWVudCBwYXN0ZSBldmVudHMgd2lsbCB1c2UgdGhpc1xuICogZnJhZ21lbnQgaWYgbm8gZXh0ZXJuYWwgY2xpcGJvYXJkIGRhdGEgaXMgc3VwcGxpZWQuXG4gKi9cbmZ1bmN0aW9uIGVkaXRPbkNvcHkoZWRpdG9yLCBlKSB7XG4gIHZhciBlZGl0b3JTdGF0ZSA9IGVkaXRvci5fbGF0ZXN0RWRpdG9yU3RhdGU7XG4gIHZhciBzZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcblxuICAvLyBObyBzZWxlY3Rpb24sIHNvIHRoZXJlJ3Mgbm90aGluZyB0byBjb3B5LlxuICBpZiAoc2VsZWN0aW9uLmlzQ29sbGFwc2VkKCkpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgZWRpdG9yLnNldENsaXBib2FyZChnZXRGcmFnbWVudEZyb21TZWxlY3Rpb24oZWRpdG9yLl9sYXRlc3RFZGl0b3JTdGF0ZSkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGVkaXRPbkNvcHk7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/editOnCopy.js
