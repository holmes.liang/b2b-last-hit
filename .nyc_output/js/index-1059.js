__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetText", function() { return GetText; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetHTML", function() { return GetHTML; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toEditorState", function() { return toEditorState; });
/* harmony import */ var _EditorCore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditorCore */ "./node_modules/rc-editor-core/es/EditorCore/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EditorCore", function() { return _EditorCore__WEBPACK_IMPORTED_MODULE_0__["default"]; });


var GetText = _EditorCore__WEBPACK_IMPORTED_MODULE_0__["default"].GetText,
    GetHTML = _EditorCore__WEBPACK_IMPORTED_MODULE_0__["default"].GetHTML;
var toEditorState = _EditorCore__WEBPACK_IMPORTED_MODULE_0__["default"].ToEditorState;
var EditorCorePublic = {
  EditorCore: _EditorCore__WEBPACK_IMPORTED_MODULE_0__["default"],
  GetText: GetText,
  GetHTML: GetHTML,
  toEditorState: toEditorState
};

/* harmony default export */ __webpack_exports__["default"] = (EditorCorePublic);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLWNvcmUvZXMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1lZGl0b3ItY29yZS9lcy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgRWRpdG9yQ29yZSBmcm9tICcuL0VkaXRvckNvcmUnO1xudmFyIEdldFRleHQgPSBFZGl0b3JDb3JlLkdldFRleHQsXG4gICAgR2V0SFRNTCA9IEVkaXRvckNvcmUuR2V0SFRNTDtcblxudmFyIHRvRWRpdG9yU3RhdGUgPSBFZGl0b3JDb3JlLlRvRWRpdG9yU3RhdGU7XG52YXIgRWRpdG9yQ29yZVB1YmxpYyA9IHtcbiAgICBFZGl0b3JDb3JlOiBFZGl0b3JDb3JlLFxuICAgIEdldFRleHQ6IEdldFRleHQsXG4gICAgR2V0SFRNTDogR2V0SFRNTCxcbiAgICB0b0VkaXRvclN0YXRlOiB0b0VkaXRvclN0YXRlXG59O1xuZXhwb3J0IHsgRWRpdG9yQ29yZSwgR2V0VGV4dCwgR2V0SFRNTCwgdG9FZGl0b3JTdGF0ZSB9O1xuZXhwb3J0IGRlZmF1bHQgRWRpdG9yQ29yZVB1YmxpYzsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-editor-core/es/index.js
