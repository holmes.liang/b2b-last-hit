/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftRemovableWord
 * @format
 * 
 */


var TokenizeUtil = __webpack_require__(/*! fbjs/lib/TokenizeUtil */ "./node_modules/fbjs/lib/TokenizeUtil.js");

var punctuation = TokenizeUtil.getPunctuation(); // The apostrophe and curly single quotes behave in a curious way: when
// surrounded on both sides by word characters, they behave as word chars; when
// either neighbor is punctuation or an end of the string, they behave as
// punctuation.

var CHAMELEON_CHARS = "['\u2018\u2019]"; // Remove the underscore, which should count as part of the removable word. The
// "chameleon chars" also count as punctuation in this regex.

var WHITESPACE_AND_PUNCTUATION = '\\s|(?![_])' + punctuation;
var DELETE_STRING = '^' + '(?:' + WHITESPACE_AND_PUNCTUATION + ')*' + '(?:' + CHAMELEON_CHARS + '|(?!' + WHITESPACE_AND_PUNCTUATION + ').)*' + '(?:(?!' + WHITESPACE_AND_PUNCTUATION + ').)';
var DELETE_REGEX = new RegExp(DELETE_STRING);
var BACKSPACE_STRING = '(?:(?!' + WHITESPACE_AND_PUNCTUATION + ').)' + '(?:' + CHAMELEON_CHARS + '|(?!' + WHITESPACE_AND_PUNCTUATION + ').)*' + '(?:' + WHITESPACE_AND_PUNCTUATION + ')*' + '$';
var BACKSPACE_REGEX = new RegExp(BACKSPACE_STRING);

function getRemovableWord(text, isBackward) {
  var matches = isBackward ? BACKSPACE_REGEX.exec(text) : DELETE_REGEX.exec(text);
  return matches ? matches[0] : text;
}

var DraftRemovableWord = {
  getBackward: function getBackward(text) {
    return getRemovableWord(text, true);
  },
  getForward: function getForward(text) {
    return getRemovableWord(text, false);
  }
};
module.exports = DraftRemovableWord;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0UmVtb3ZhYmxlV29yZC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9EcmFmdFJlbW92YWJsZVdvcmQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBEcmFmdFJlbW92YWJsZVdvcmRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIFRva2VuaXplVXRpbCA9IHJlcXVpcmUoJ2ZianMvbGliL1Rva2VuaXplVXRpbCcpO1xuXG52YXIgcHVuY3R1YXRpb24gPSBUb2tlbml6ZVV0aWwuZ2V0UHVuY3R1YXRpb24oKTtcblxuLy8gVGhlIGFwb3N0cm9waGUgYW5kIGN1cmx5IHNpbmdsZSBxdW90ZXMgYmVoYXZlIGluIGEgY3VyaW91cyB3YXk6IHdoZW5cbi8vIHN1cnJvdW5kZWQgb24gYm90aCBzaWRlcyBieSB3b3JkIGNoYXJhY3RlcnMsIHRoZXkgYmVoYXZlIGFzIHdvcmQgY2hhcnM7IHdoZW5cbi8vIGVpdGhlciBuZWlnaGJvciBpcyBwdW5jdHVhdGlvbiBvciBhbiBlbmQgb2YgdGhlIHN0cmluZywgdGhleSBiZWhhdmUgYXNcbi8vIHB1bmN0dWF0aW9uLlxudmFyIENIQU1FTEVPTl9DSEFSUyA9ICdbXFwnXFx1MjAxOFxcdTIwMTldJztcblxuLy8gUmVtb3ZlIHRoZSB1bmRlcnNjb3JlLCB3aGljaCBzaG91bGQgY291bnQgYXMgcGFydCBvZiB0aGUgcmVtb3ZhYmxlIHdvcmQuIFRoZVxuLy8gXCJjaGFtZWxlb24gY2hhcnNcIiBhbHNvIGNvdW50IGFzIHB1bmN0dWF0aW9uIGluIHRoaXMgcmVnZXguXG52YXIgV0hJVEVTUEFDRV9BTkRfUFVOQ1RVQVRJT04gPSAnXFxcXHN8KD8hW19dKScgKyBwdW5jdHVhdGlvbjtcblxudmFyIERFTEVURV9TVFJJTkcgPSAnXicgKyAnKD86JyArIFdISVRFU1BBQ0VfQU5EX1BVTkNUVUFUSU9OICsgJykqJyArICcoPzonICsgQ0hBTUVMRU9OX0NIQVJTICsgJ3woPyEnICsgV0hJVEVTUEFDRV9BTkRfUFVOQ1RVQVRJT04gKyAnKS4pKicgKyAnKD86KD8hJyArIFdISVRFU1BBQ0VfQU5EX1BVTkNUVUFUSU9OICsgJykuKSc7XG52YXIgREVMRVRFX1JFR0VYID0gbmV3IFJlZ0V4cChERUxFVEVfU1RSSU5HKTtcblxudmFyIEJBQ0tTUEFDRV9TVFJJTkcgPSAnKD86KD8hJyArIFdISVRFU1BBQ0VfQU5EX1BVTkNUVUFUSU9OICsgJykuKScgKyAnKD86JyArIENIQU1FTEVPTl9DSEFSUyArICd8KD8hJyArIFdISVRFU1BBQ0VfQU5EX1BVTkNUVUFUSU9OICsgJykuKSonICsgJyg/OicgKyBXSElURVNQQUNFX0FORF9QVU5DVFVBVElPTiArICcpKicgKyAnJCc7XG52YXIgQkFDS1NQQUNFX1JFR0VYID0gbmV3IFJlZ0V4cChCQUNLU1BBQ0VfU1RSSU5HKTtcblxuZnVuY3Rpb24gZ2V0UmVtb3ZhYmxlV29yZCh0ZXh0LCBpc0JhY2t3YXJkKSB7XG4gIHZhciBtYXRjaGVzID0gaXNCYWNrd2FyZCA/IEJBQ0tTUEFDRV9SRUdFWC5leGVjKHRleHQpIDogREVMRVRFX1JFR0VYLmV4ZWModGV4dCk7XG4gIHJldHVybiBtYXRjaGVzID8gbWF0Y2hlc1swXSA6IHRleHQ7XG59XG5cbnZhciBEcmFmdFJlbW92YWJsZVdvcmQgPSB7XG4gIGdldEJhY2t3YXJkOiBmdW5jdGlvbiBnZXRCYWNrd2FyZCh0ZXh0KSB7XG4gICAgcmV0dXJuIGdldFJlbW92YWJsZVdvcmQodGV4dCwgdHJ1ZSk7XG4gIH0sXG5cbiAgZ2V0Rm9yd2FyZDogZnVuY3Rpb24gZ2V0Rm9yd2FyZCh0ZXh0KSB7XG4gICAgcmV0dXJuIGdldFJlbW92YWJsZVdvcmQodGV4dCwgZmFsc2UpO1xuICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IERyYWZ0UmVtb3ZhYmxlV29yZDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFQQTtBQVVBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftRemovableWord.js
