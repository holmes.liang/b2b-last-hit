__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rc_trigger__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-trigger */ "./node_modules/rc-trigger/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _placements__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./placements */ "./node_modules/rc-dropdown/es/placements.js");
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

function _objectWithoutProperties(obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}









var Dropdown = function (_Component) {
  _inherits(Dropdown, _Component);

  function Dropdown(props) {
    _classCallCheck(this, Dropdown);

    var _this = _possibleConstructorReturn(this, _Component.call(this, props));

    _initialiseProps.call(_this);

    if ('visible' in props) {
      _this.state = {
        visible: props.visible
      };
    } else {
      _this.state = {
        visible: props.defaultVisible
      };
    }

    return _this;
  }

  Dropdown.getDerivedStateFromProps = function getDerivedStateFromProps(nextProps) {
    if ('visible' in nextProps) {
      return {
        visible: nextProps.visible
      };
    }

    return null;
  };

  Dropdown.prototype.getOverlayElement = function getOverlayElement() {
    var overlay = this.props.overlay;
    var overlayElement = void 0;

    if (typeof overlay === 'function') {
      overlayElement = overlay();
    } else {
      overlayElement = overlay;
    }

    return overlayElement;
  };

  Dropdown.prototype.getMenuElementOrLambda = function getMenuElementOrLambda() {
    var overlay = this.props.overlay;

    if (typeof overlay === 'function') {
      return this.getMenuElement;
    }

    return this.getMenuElement();
  };

  Dropdown.prototype.getPopupDomNode = function getPopupDomNode() {
    return this.trigger.getPopupDomNode();
  };

  Dropdown.prototype.getOpenClassName = function getOpenClassName() {
    var _props = this.props,
        openClassName = _props.openClassName,
        prefixCls = _props.prefixCls;

    if (openClassName !== undefined) {
      return openClassName;
    }

    return prefixCls + '-open';
  };

  Dropdown.prototype.renderChildren = function renderChildren() {
    var children = this.props.children;
    var visible = this.state.visible;
    var childrenProps = children.props ? children.props : {};
    var childClassName = classnames__WEBPACK_IMPORTED_MODULE_4___default()(childrenProps.className, this.getOpenClassName());
    return visible && children ? Object(react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"])(children, {
      className: childClassName
    }) : children;
  };

  Dropdown.prototype.render = function render() {
    var _props2 = this.props,
        prefixCls = _props2.prefixCls,
        transitionName = _props2.transitionName,
        animation = _props2.animation,
        align = _props2.align,
        placement = _props2.placement,
        getPopupContainer = _props2.getPopupContainer,
        showAction = _props2.showAction,
        hideAction = _props2.hideAction,
        overlayClassName = _props2.overlayClassName,
        overlayStyle = _props2.overlayStyle,
        trigger = _props2.trigger,
        otherProps = _objectWithoutProperties(_props2, ['prefixCls', 'transitionName', 'animation', 'align', 'placement', 'getPopupContainer', 'showAction', 'hideAction', 'overlayClassName', 'overlayStyle', 'trigger']);

    var triggerHideAction = hideAction;

    if (!triggerHideAction && trigger.indexOf('contextMenu') !== -1) {
      triggerHideAction = ['click'];
    }

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(rc_trigger__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({}, otherProps, {
      prefixCls: prefixCls,
      ref: this.saveTrigger,
      popupClassName: overlayClassName,
      popupStyle: overlayStyle,
      builtinPlacements: _placements__WEBPACK_IMPORTED_MODULE_5__["default"],
      action: trigger,
      showAction: showAction,
      hideAction: triggerHideAction || [],
      popupPlacement: placement,
      popupAlign: align,
      popupTransitionName: transitionName,
      popupAnimation: animation,
      popupVisible: this.state.visible,
      afterPopupVisibleChange: this.afterVisibleChange,
      popup: this.getMenuElementOrLambda(),
      onPopupVisibleChange: this.onVisibleChange,
      getPopupContainer: getPopupContainer
    }), this.renderChildren());
  };

  return Dropdown;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Dropdown.propTypes = {
  minOverlayWidthMatchTrigger: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  onVisibleChange: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onOverlayClick: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.any,
  transitionName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  overlayClassName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  openClassName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  animation: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.any,
  align: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  overlayStyle: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  placement: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  overlay: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func]),
  trigger: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  alignPoint: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  showAction: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  hideAction: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  getPopupContainer: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  visible: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  defaultVisible: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool
};
Dropdown.defaultProps = {
  prefixCls: 'rc-dropdown',
  trigger: ['hover'],
  showAction: [],
  overlayClassName: '',
  overlayStyle: {},
  defaultVisible: false,
  onVisibleChange: function onVisibleChange() {},
  placement: 'bottomLeft'
};

var _initialiseProps = function _initialiseProps() {
  var _this2 = this;

  this.onClick = function (e) {
    var props = _this2.props;

    var overlayProps = _this2.getOverlayElement().props; // do no call onVisibleChange, if you need click to hide, use onClick and control visible


    if (!('visible' in props)) {
      _this2.setState({
        visible: false
      });
    }

    if (props.onOverlayClick) {
      props.onOverlayClick(e);
    }

    if (overlayProps.onClick) {
      overlayProps.onClick(e);
    }
  };

  this.onVisibleChange = function (visible) {
    var props = _this2.props;

    if (!('visible' in props)) {
      _this2.setState({
        visible: visible
      });
    }

    props.onVisibleChange(visible);
  };

  this.getMinOverlayWidthMatchTrigger = function () {
    var _props3 = _this2.props,
        minOverlayWidthMatchTrigger = _props3.minOverlayWidthMatchTrigger,
        alignPoint = _props3.alignPoint;

    if ('minOverlayWidthMatchTrigger' in _this2.props) {
      return minOverlayWidthMatchTrigger;
    }

    return !alignPoint;
  };

  this.getMenuElement = function () {
    var prefixCls = _this2.props.prefixCls;

    var overlayElement = _this2.getOverlayElement();

    var extraOverlayProps = {
      prefixCls: prefixCls + '-menu',
      onClick: _this2.onClick
    };

    if (typeof overlayElement.type === 'string') {
      delete extraOverlayProps.prefixCls;
    }

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.cloneElement(overlayElement, extraOverlayProps);
  };

  this.afterVisibleChange = function (visible) {
    if (visible && _this2.getMinOverlayWidthMatchTrigger()) {
      var overlayNode = _this2.getPopupDomNode();

      var rootNode = react_dom__WEBPACK_IMPORTED_MODULE_2___default.a.findDOMNode(_this2);

      if (rootNode && overlayNode && rootNode.offsetWidth > overlayNode.offsetWidth) {
        overlayNode.style.minWidth = rootNode.offsetWidth + 'px';

        if (_this2.trigger && _this2.trigger._component && _this2.trigger._component.alignInstance) {
          _this2.trigger._component.alignInstance.forceAlign();
        }
      }
    }
  };

  this.saveTrigger = function (node) {
    _this2.trigger = node;
  };
};

Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_6__["polyfill"])(Dropdown);
/* harmony default export */ __webpack_exports__["default"] = (Dropdown);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZHJvcGRvd24vZXMvRHJvcGRvd24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1kcm9wZG93bi9lcy9Ecm9wZG93bi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG5mdW5jdGlvbiBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMob2JqLCBrZXlzKSB7IHZhciB0YXJnZXQgPSB7fTsgZm9yICh2YXIgaSBpbiBvYmopIHsgaWYgKGtleXMuaW5kZXhPZihpKSA+PSAwKSBjb250aW51ZTsgaWYgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBpKSkgY29udGludWU7IHRhcmdldFtpXSA9IG9ialtpXTsgfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCwgY2xvbmVFbGVtZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xuaW1wb3J0IFRyaWdnZXIgZnJvbSAncmMtdHJpZ2dlcic7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBwbGFjZW1lbnRzIGZyb20gJy4vcGxhY2VtZW50cyc7XG5pbXBvcnQgeyBwb2x5ZmlsbCB9IGZyb20gJ3JlYWN0LWxpZmVjeWNsZXMtY29tcGF0JztcblxudmFyIERyb3Bkb3duID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgX2luaGVyaXRzKERyb3Bkb3duLCBfQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBEcm9wZG93bihwcm9wcykge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBEcm9wZG93bik7XG5cbiAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29tcG9uZW50LmNhbGwodGhpcywgcHJvcHMpKTtcblxuICAgIF9pbml0aWFsaXNlUHJvcHMuY2FsbChfdGhpcyk7XG5cbiAgICBpZiAoJ3Zpc2libGUnIGluIHByb3BzKSB7XG4gICAgICBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgdmlzaWJsZTogcHJvcHMudmlzaWJsZVxuICAgICAgfTtcbiAgICB9IGVsc2Uge1xuICAgICAgX3RoaXMuc3RhdGUgPSB7XG4gICAgICAgIHZpc2libGU6IHByb3BzLmRlZmF1bHRWaXNpYmxlXG4gICAgICB9O1xuICAgIH1cbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBEcm9wZG93bi5nZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMgPSBmdW5jdGlvbiBnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMobmV4dFByb3BzKSB7XG4gICAgaWYgKCd2aXNpYmxlJyBpbiBuZXh0UHJvcHMpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHZpc2libGU6IG5leHRQcm9wcy52aXNpYmxlXG4gICAgICB9O1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbiAgfTtcblxuICBEcm9wZG93bi5wcm90b3R5cGUuZ2V0T3ZlcmxheUVsZW1lbnQgPSBmdW5jdGlvbiBnZXRPdmVybGF5RWxlbWVudCgpIHtcbiAgICB2YXIgb3ZlcmxheSA9IHRoaXMucHJvcHMub3ZlcmxheTtcblxuICAgIHZhciBvdmVybGF5RWxlbWVudCA9IHZvaWQgMDtcbiAgICBpZiAodHlwZW9mIG92ZXJsYXkgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIG92ZXJsYXlFbGVtZW50ID0gb3ZlcmxheSgpO1xuICAgIH0gZWxzZSB7XG4gICAgICBvdmVybGF5RWxlbWVudCA9IG92ZXJsYXk7XG4gICAgfVxuICAgIHJldHVybiBvdmVybGF5RWxlbWVudDtcbiAgfTtcblxuICBEcm9wZG93bi5wcm90b3R5cGUuZ2V0TWVudUVsZW1lbnRPckxhbWJkYSA9IGZ1bmN0aW9uIGdldE1lbnVFbGVtZW50T3JMYW1iZGEoKSB7XG4gICAgdmFyIG92ZXJsYXkgPSB0aGlzLnByb3BzLm92ZXJsYXk7XG5cbiAgICBpZiAodHlwZW9mIG92ZXJsYXkgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHJldHVybiB0aGlzLmdldE1lbnVFbGVtZW50O1xuICAgIH1cbiAgICByZXR1cm4gdGhpcy5nZXRNZW51RWxlbWVudCgpO1xuICB9O1xuXG4gIERyb3Bkb3duLnByb3RvdHlwZS5nZXRQb3B1cERvbU5vZGUgPSBmdW5jdGlvbiBnZXRQb3B1cERvbU5vZGUoKSB7XG4gICAgcmV0dXJuIHRoaXMudHJpZ2dlci5nZXRQb3B1cERvbU5vZGUoKTtcbiAgfTtcblxuICBEcm9wZG93bi5wcm90b3R5cGUuZ2V0T3BlbkNsYXNzTmFtZSA9IGZ1bmN0aW9uIGdldE9wZW5DbGFzc05hbWUoKSB7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIG9wZW5DbGFzc05hbWUgPSBfcHJvcHMub3BlbkNsYXNzTmFtZSxcbiAgICAgICAgcHJlZml4Q2xzID0gX3Byb3BzLnByZWZpeENscztcblxuICAgIGlmIChvcGVuQ2xhc3NOYW1lICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHJldHVybiBvcGVuQ2xhc3NOYW1lO1xuICAgIH1cbiAgICByZXR1cm4gcHJlZml4Q2xzICsgJy1vcGVuJztcbiAgfTtcblxuICBEcm9wZG93bi5wcm90b3R5cGUucmVuZGVyQ2hpbGRyZW4gPSBmdW5jdGlvbiByZW5kZXJDaGlsZHJlbigpIHtcbiAgICB2YXIgY2hpbGRyZW4gPSB0aGlzLnByb3BzLmNoaWxkcmVuO1xuICAgIHZhciB2aXNpYmxlID0gdGhpcy5zdGF0ZS52aXNpYmxlO1xuXG4gICAgdmFyIGNoaWxkcmVuUHJvcHMgPSBjaGlsZHJlbi5wcm9wcyA/IGNoaWxkcmVuLnByb3BzIDoge307XG4gICAgdmFyIGNoaWxkQ2xhc3NOYW1lID0gY2xhc3NOYW1lcyhjaGlsZHJlblByb3BzLmNsYXNzTmFtZSwgdGhpcy5nZXRPcGVuQ2xhc3NOYW1lKCkpO1xuICAgIHJldHVybiB2aXNpYmxlICYmIGNoaWxkcmVuID8gY2xvbmVFbGVtZW50KGNoaWxkcmVuLCB7IGNsYXNzTmFtZTogY2hpbGRDbGFzc05hbWUgfSkgOiBjaGlsZHJlbjtcbiAgfTtcblxuICBEcm9wZG93bi5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgIHZhciBfcHJvcHMyID0gdGhpcy5wcm9wcyxcbiAgICAgICAgcHJlZml4Q2xzID0gX3Byb3BzMi5wcmVmaXhDbHMsXG4gICAgICAgIHRyYW5zaXRpb25OYW1lID0gX3Byb3BzMi50cmFuc2l0aW9uTmFtZSxcbiAgICAgICAgYW5pbWF0aW9uID0gX3Byb3BzMi5hbmltYXRpb24sXG4gICAgICAgIGFsaWduID0gX3Byb3BzMi5hbGlnbixcbiAgICAgICAgcGxhY2VtZW50ID0gX3Byb3BzMi5wbGFjZW1lbnQsXG4gICAgICAgIGdldFBvcHVwQ29udGFpbmVyID0gX3Byb3BzMi5nZXRQb3B1cENvbnRhaW5lcixcbiAgICAgICAgc2hvd0FjdGlvbiA9IF9wcm9wczIuc2hvd0FjdGlvbixcbiAgICAgICAgaGlkZUFjdGlvbiA9IF9wcm9wczIuaGlkZUFjdGlvbixcbiAgICAgICAgb3ZlcmxheUNsYXNzTmFtZSA9IF9wcm9wczIub3ZlcmxheUNsYXNzTmFtZSxcbiAgICAgICAgb3ZlcmxheVN0eWxlID0gX3Byb3BzMi5vdmVybGF5U3R5bGUsXG4gICAgICAgIHRyaWdnZXIgPSBfcHJvcHMyLnRyaWdnZXIsXG4gICAgICAgIG90aGVyUHJvcHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3Byb3BzMiwgWydwcmVmaXhDbHMnLCAndHJhbnNpdGlvbk5hbWUnLCAnYW5pbWF0aW9uJywgJ2FsaWduJywgJ3BsYWNlbWVudCcsICdnZXRQb3B1cENvbnRhaW5lcicsICdzaG93QWN0aW9uJywgJ2hpZGVBY3Rpb24nLCAnb3ZlcmxheUNsYXNzTmFtZScsICdvdmVybGF5U3R5bGUnLCAndHJpZ2dlciddKTtcblxuICAgIHZhciB0cmlnZ2VySGlkZUFjdGlvbiA9IGhpZGVBY3Rpb247XG4gICAgaWYgKCF0cmlnZ2VySGlkZUFjdGlvbiAmJiB0cmlnZ2VyLmluZGV4T2YoJ2NvbnRleHRNZW51JykgIT09IC0xKSB7XG4gICAgICB0cmlnZ2VySGlkZUFjdGlvbiA9IFsnY2xpY2snXTtcbiAgICB9XG5cbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgIFRyaWdnZXIsXG4gICAgICBfZXh0ZW5kcyh7fSwgb3RoZXJQcm9wcywge1xuICAgICAgICBwcmVmaXhDbHM6IHByZWZpeENscyxcbiAgICAgICAgcmVmOiB0aGlzLnNhdmVUcmlnZ2VyLFxuICAgICAgICBwb3B1cENsYXNzTmFtZTogb3ZlcmxheUNsYXNzTmFtZSxcbiAgICAgICAgcG9wdXBTdHlsZTogb3ZlcmxheVN0eWxlLFxuICAgICAgICBidWlsdGluUGxhY2VtZW50czogcGxhY2VtZW50cyxcbiAgICAgICAgYWN0aW9uOiB0cmlnZ2VyLFxuICAgICAgICBzaG93QWN0aW9uOiBzaG93QWN0aW9uLFxuICAgICAgICBoaWRlQWN0aW9uOiB0cmlnZ2VySGlkZUFjdGlvbiB8fCBbXSxcbiAgICAgICAgcG9wdXBQbGFjZW1lbnQ6IHBsYWNlbWVudCxcbiAgICAgICAgcG9wdXBBbGlnbjogYWxpZ24sXG4gICAgICAgIHBvcHVwVHJhbnNpdGlvbk5hbWU6IHRyYW5zaXRpb25OYW1lLFxuICAgICAgICBwb3B1cEFuaW1hdGlvbjogYW5pbWF0aW9uLFxuICAgICAgICBwb3B1cFZpc2libGU6IHRoaXMuc3RhdGUudmlzaWJsZSxcbiAgICAgICAgYWZ0ZXJQb3B1cFZpc2libGVDaGFuZ2U6IHRoaXMuYWZ0ZXJWaXNpYmxlQ2hhbmdlLFxuICAgICAgICBwb3B1cDogdGhpcy5nZXRNZW51RWxlbWVudE9yTGFtYmRhKCksXG4gICAgICAgIG9uUG9wdXBWaXNpYmxlQ2hhbmdlOiB0aGlzLm9uVmlzaWJsZUNoYW5nZSxcbiAgICAgICAgZ2V0UG9wdXBDb250YWluZXI6IGdldFBvcHVwQ29udGFpbmVyXG4gICAgICB9KSxcbiAgICAgIHRoaXMucmVuZGVyQ2hpbGRyZW4oKVxuICAgICk7XG4gIH07XG5cbiAgcmV0dXJuIERyb3Bkb3duO1xufShDb21wb25lbnQpO1xuXG5Ecm9wZG93bi5wcm9wVHlwZXMgPSB7XG4gIG1pbk92ZXJsYXlXaWR0aE1hdGNoVHJpZ2dlcjogUHJvcFR5cGVzLmJvb2wsXG4gIG9uVmlzaWJsZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uT3ZlcmxheUNsaWNrOiBQcm9wVHlwZXMuZnVuYyxcbiAgcHJlZml4Q2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBjaGlsZHJlbjogUHJvcFR5cGVzLmFueSxcbiAgdHJhbnNpdGlvbk5hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG92ZXJsYXlDbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG9wZW5DbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGFuaW1hdGlvbjogUHJvcFR5cGVzLmFueSxcbiAgYWxpZ246IFByb3BUeXBlcy5vYmplY3QsXG4gIG92ZXJsYXlTdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgcGxhY2VtZW50OiBQcm9wVHlwZXMuc3RyaW5nLFxuICBvdmVybGF5OiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMubm9kZSwgUHJvcFR5cGVzLmZ1bmNdKSxcbiAgdHJpZ2dlcjogUHJvcFR5cGVzLmFycmF5LFxuICBhbGlnblBvaW50OiBQcm9wVHlwZXMuYm9vbCxcbiAgc2hvd0FjdGlvbjogUHJvcFR5cGVzLmFycmF5LFxuICBoaWRlQWN0aW9uOiBQcm9wVHlwZXMuYXJyYXksXG4gIGdldFBvcHVwQ29udGFpbmVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgdmlzaWJsZTogUHJvcFR5cGVzLmJvb2wsXG4gIGRlZmF1bHRWaXNpYmxlOiBQcm9wVHlwZXMuYm9vbFxufTtcbkRyb3Bkb3duLmRlZmF1bHRQcm9wcyA9IHtcbiAgcHJlZml4Q2xzOiAncmMtZHJvcGRvd24nLFxuICB0cmlnZ2VyOiBbJ2hvdmVyJ10sXG4gIHNob3dBY3Rpb246IFtdLFxuICBvdmVybGF5Q2xhc3NOYW1lOiAnJyxcbiAgb3ZlcmxheVN0eWxlOiB7fSxcbiAgZGVmYXVsdFZpc2libGU6IGZhbHNlLFxuICBvblZpc2libGVDaGFuZ2U6IGZ1bmN0aW9uIG9uVmlzaWJsZUNoYW5nZSgpIHt9LFxuXG4gIHBsYWNlbWVudDogJ2JvdHRvbUxlZnQnXG59O1xuXG52YXIgX2luaXRpYWxpc2VQcm9wcyA9IGZ1bmN0aW9uIF9pbml0aWFsaXNlUHJvcHMoKSB7XG4gIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gIHRoaXMub25DbGljayA9IGZ1bmN0aW9uIChlKSB7XG4gICAgdmFyIHByb3BzID0gX3RoaXMyLnByb3BzO1xuICAgIHZhciBvdmVybGF5UHJvcHMgPSBfdGhpczIuZ2V0T3ZlcmxheUVsZW1lbnQoKS5wcm9wcztcbiAgICAvLyBkbyBubyBjYWxsIG9uVmlzaWJsZUNoYW5nZSwgaWYgeW91IG5lZWQgY2xpY2sgdG8gaGlkZSwgdXNlIG9uQ2xpY2sgYW5kIGNvbnRyb2wgdmlzaWJsZVxuICAgIGlmICghKCd2aXNpYmxlJyBpbiBwcm9wcykpIHtcbiAgICAgIF90aGlzMi5zZXRTdGF0ZSh7XG4gICAgICAgIHZpc2libGU6IGZhbHNlXG4gICAgICB9KTtcbiAgICB9XG4gICAgaWYgKHByb3BzLm9uT3ZlcmxheUNsaWNrKSB7XG4gICAgICBwcm9wcy5vbk92ZXJsYXlDbGljayhlKTtcbiAgICB9XG4gICAgaWYgKG92ZXJsYXlQcm9wcy5vbkNsaWNrKSB7XG4gICAgICBvdmVybGF5UHJvcHMub25DbGljayhlKTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5vblZpc2libGVDaGFuZ2UgPSBmdW5jdGlvbiAodmlzaWJsZSkge1xuICAgIHZhciBwcm9wcyA9IF90aGlzMi5wcm9wcztcbiAgICBpZiAoISgndmlzaWJsZScgaW4gcHJvcHMpKSB7XG4gICAgICBfdGhpczIuc2V0U3RhdGUoe1xuICAgICAgICB2aXNpYmxlOiB2aXNpYmxlXG4gICAgICB9KTtcbiAgICB9XG4gICAgcHJvcHMub25WaXNpYmxlQ2hhbmdlKHZpc2libGUpO1xuICB9O1xuXG4gIHRoaXMuZ2V0TWluT3ZlcmxheVdpZHRoTWF0Y2hUcmlnZ2VyID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfcHJvcHMzID0gX3RoaXMyLnByb3BzLFxuICAgICAgICBtaW5PdmVybGF5V2lkdGhNYXRjaFRyaWdnZXIgPSBfcHJvcHMzLm1pbk92ZXJsYXlXaWR0aE1hdGNoVHJpZ2dlcixcbiAgICAgICAgYWxpZ25Qb2ludCA9IF9wcm9wczMuYWxpZ25Qb2ludDtcblxuICAgIGlmICgnbWluT3ZlcmxheVdpZHRoTWF0Y2hUcmlnZ2VyJyBpbiBfdGhpczIucHJvcHMpIHtcbiAgICAgIHJldHVybiBtaW5PdmVybGF5V2lkdGhNYXRjaFRyaWdnZXI7XG4gICAgfVxuXG4gICAgcmV0dXJuICFhbGlnblBvaW50O1xuICB9O1xuXG4gIHRoaXMuZ2V0TWVudUVsZW1lbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHByZWZpeENscyA9IF90aGlzMi5wcm9wcy5wcmVmaXhDbHM7XG5cbiAgICB2YXIgb3ZlcmxheUVsZW1lbnQgPSBfdGhpczIuZ2V0T3ZlcmxheUVsZW1lbnQoKTtcbiAgICB2YXIgZXh0cmFPdmVybGF5UHJvcHMgPSB7XG4gICAgICBwcmVmaXhDbHM6IHByZWZpeENscyArICctbWVudScsXG4gICAgICBvbkNsaWNrOiBfdGhpczIub25DbGlja1xuICAgIH07XG4gICAgaWYgKHR5cGVvZiBvdmVybGF5RWxlbWVudC50eXBlID09PSAnc3RyaW5nJykge1xuICAgICAgZGVsZXRlIGV4dHJhT3ZlcmxheVByb3BzLnByZWZpeENscztcbiAgICB9XG4gICAgcmV0dXJuIFJlYWN0LmNsb25lRWxlbWVudChvdmVybGF5RWxlbWVudCwgZXh0cmFPdmVybGF5UHJvcHMpO1xuICB9O1xuXG4gIHRoaXMuYWZ0ZXJWaXNpYmxlQ2hhbmdlID0gZnVuY3Rpb24gKHZpc2libGUpIHtcbiAgICBpZiAodmlzaWJsZSAmJiBfdGhpczIuZ2V0TWluT3ZlcmxheVdpZHRoTWF0Y2hUcmlnZ2VyKCkpIHtcbiAgICAgIHZhciBvdmVybGF5Tm9kZSA9IF90aGlzMi5nZXRQb3B1cERvbU5vZGUoKTtcbiAgICAgIHZhciByb290Tm9kZSA9IFJlYWN0RE9NLmZpbmRET01Ob2RlKF90aGlzMik7XG4gICAgICBpZiAocm9vdE5vZGUgJiYgb3ZlcmxheU5vZGUgJiYgcm9vdE5vZGUub2Zmc2V0V2lkdGggPiBvdmVybGF5Tm9kZS5vZmZzZXRXaWR0aCkge1xuICAgICAgICBvdmVybGF5Tm9kZS5zdHlsZS5taW5XaWR0aCA9IHJvb3ROb2RlLm9mZnNldFdpZHRoICsgJ3B4JztcbiAgICAgICAgaWYgKF90aGlzMi50cmlnZ2VyICYmIF90aGlzMi50cmlnZ2VyLl9jb21wb25lbnQgJiYgX3RoaXMyLnRyaWdnZXIuX2NvbXBvbmVudC5hbGlnbkluc3RhbmNlKSB7XG4gICAgICAgICAgX3RoaXMyLnRyaWdnZXIuX2NvbXBvbmVudC5hbGlnbkluc3RhbmNlLmZvcmNlQWxpZ24oKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICB0aGlzLnNhdmVUcmlnZ2VyID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICBfdGhpczIudHJpZ2dlciA9IG5vZGU7XG4gIH07XG59O1xuXG5wb2x5ZmlsbChEcm9wZG93bik7XG5cbmV4cG9ydCBkZWZhdWx0IERyb3Bkb3duOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQWFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpCQTtBQXFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBcEJBO0FBc0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQVRBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-dropdown/es/Dropdown.js
