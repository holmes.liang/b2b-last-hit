__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isUrl", function() { return isUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatter", function() { return formatter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMenuData", function() { return getMenuData; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");

var menuData = [{
  name: "Home",
  icon: "home",
  path: "home",
  itemCode: "MI01",
  children: [{
    path: "dashboard",
    itemCode: "MI0101",
    name: "Dashboard"
  }]
}, {
  name: "Administration",
  icon: "rocket",
  path: "admin",
  itemCode: "MI03",
  children: [{
    path: "home",
    itemCode: "MI0301",
    name: "Admin"
  }]
}, {
  name: "Transactions",
  icon: "table",
  path: "transactions",
  itemCode: "MI04",
  children: [{
    name: "New",
    path: "new/quote/list",
    itemCode: "MI0400"
  }, {
    name: "Policies",
    path: "policyList",
    itemCode: "MI0401"
  }, {
    name: "Endorsements",
    path: "endorsements",
    itemCode: "MI0402"
  }, {
    name: "Underwriting",
    path: "uw",
    itemCode: "MI0404"
  }, {
    name: "Master Agreement",
    path: "master/query",
    itemCode: "MI0407"
  }, {
    name: "Inspection Approval",
    path: "inspection",
    itemCode: "MI0405"
  }, {
    name: "Blacklist Approval",
    path: "blacklist",
    itemCode: "MI0406"
  }, {
    name: "Claims",
    path: "claims",
    itemCode: "MI0403"
  }]
}, {
  name: "Claims",
  icon: "solution",
  path: "claims",
  itemCode: "MI07",
  children: [{
    name: "FNOL",
    path: "callcenter",
    itemCode: "MI0701"
  }, {
    name: "Claim Handling",
    path: "handling",
    itemCode: "MI0702"
  }, {
    name: "Settlement Approval",
    path: "settlement-approval",
    itemCode: "MI0703"
  }, {
    name: "Cash Call",
    itemCode: "MI0704",
    path: "cash-call",
    children: [{
      name: "To Be Confirmed",
      itemCode: "MI070401",
      path: "confirmed"
    }, {
      name: "Cash Call Handling",
      itemCode: "MI070402",
      path: "handling"
    }]
  }]
}, {
  name: "Finance",
  icon: "pay-circle",
  path: "bcp",
  itemCode: "MI06",
  children: [{
    name: "Collection",
    path: "collection",
    itemCode: "MI0601",
    children: [{
      name: "Collection",
      path: "index",
      itemCode: "MI060101"
    }, {
      name: "Balance Collection",
      path: "balance",
      itemCode: "MI060102"
    }, {
      name: "Bulk Collection",
      path: "bulk",
      itemCode: "MI060103"
    }, {
      name: "Electronic Collection",
      path: "billPayment",
      itemCode: "MI060104"
    }, {
      name: "Direct Debit",
      path: "autoDebit",
      itemCode: "MI060105"
    }, {
      name: "Collection Enquiry",
      path: "enquiry",
      itemCode: "MI060120"
    }]
  }, {
    path: "myAccounts",
    itemCode: "MI0607",
    name: "Payment"
  }, {
    name: "Payment",
    path: "payment",
    itemCode: "MI0604",
    children: [{
      name: "Payment",
      path: "index",
      itemCode: "MI060401"
    }, {
      name: "Balance Refund",
      path: "balance",
      itemCode: "MI060402"
    }, {
      name: "Bulk Payment",
      path: "bulk",
      itemCode: "MI060403"
    }, {
      name: "Payment Approval",
      path: "approval",
      itemCode: "MI060404"
    }, {
      name: "Direct Credit",
      path: "direct",
      itemCode: "MI060405"
    }, {
      name: "Payment Enquiry",
      path: "enquiry",
      itemCode: "MI060420"
    }]
  }, {
    name: "Referral Fee Approval",
    path: "referralFee",
    itemCode: "MI0609"
  }, {
    name: "Amend Direct Debit Bank Account",
    path: "bankAccount",
    itemCode: "MI0606"
  }]
}, {
  name: "Customers",
  icon: "usergroup-add",
  path: "customers",
  itemCode: "MI09",
  children: [{
    name: "Customers",
    itemCode: "MI0901",
    // icon: 'profile',
    path: "list"
  }]
}, {
  name: "eKYC",
  icon: "kyc",
  path: "ekyc",
  itemCode: "MI40",
  children: [{
    name: "eKYC Queries",
    itemCode: "MI4001",
    path: "queries"
  }]
}, {
  name: "Reinsurance",
  icon: "insurance",
  path: "reinsurance",
  itemCode: "MI20",
  children: [{
    name: "Risk Accumulation",
    itemCode: "MI2003",
    path: "riskAccumulation"
  }, {
    name: "Cash Call",
    itemCode: "MI2005",
    path: "cash-call",
    children: [{
      name: "To Be Confirmed",
      itemCode: "MI200501",
      path: "confirmed"
    }, {
      name: "Cash Call Handling",
      itemCode: "MI200502",
      path: "handling"
    }]
  }, {
    name: "RI Statement",
    itemCode: "MI2004",
    path: "statement"
  }, {
    name: "Treaty Definition",
    itemCode: "MI2002",
    path: "treatyDefinition"
  }]
}, {
  name: "Campaigns",
  icon: "rocket",
  path: "campaigns",
  itemCode: "MI10",
  children: [{
    path: "query",
    itemCode: "MI1001",
    name: "Campaigns"
  }, {
    path: "voucher",
    itemCode: "MI1003",
    name: "Vouchers",
    children: [{
      name: "Vouchers",
      itemCode: "MI100301",
      path: "query"
    }, {
      name: "Vouchers Setup",
      itemCode: "MI100302",
      path: "type/query"
    }]
  }, {
    path: "redemption",
    itemCode: "MI1004",
    name: "Redemption"
  }, {
    path: "monitor",
    itemCode: "MI1002",
    name: "Campaign Monitoring",
    children: [{
      name: "Campaign Monitoring",
      itemCode: "MI100201",
      path: "campaign"
    }, {
      name: "Voucher Monitoring",
      itemCode: "MI100202",
      path: "voucher"
    }]
  }]
}, {
  name: "Configurations",
  icon: "configuration",
  path: "configuration",
  itemCode: "MI30",
  children: [{
    name: "White-Labeling",
    itemCode: "MI3001",
    path: "white-labeling"
  }, {
    name: "RI Stakeholders",
    itemCode: "MI3002",
    path: "ri-stakeholder"
  }, {
    name: "Claim Stakeholders",
    itemCode: "MI3003",
    path: "claim-stakeholder"
  }, {
    name: "Letter Templates",
    itemCode: "MI3005",
    path: "letter-templates"
  }, {
    name: "Extended Clauses",
    itemCode: "MI3004",
    path: "extended-clauses"
  }]
}, {
  name: "Report",
  icon: "project",
  path: "reports",
  itemCode: "MI05",
  children: [{
    name: "Sales Report",
    itemCode: "MI0501",
    path: "sales"
  }, {
    name: "Commission Report",
    itemCode: "MI0502",
    path: "commission"
  }, {
    name: "More",
    itemCode: "MI0503",
    path: "more"
  }]
}];
var reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/g;
function isUrl(path) {
  return reg.test(path);
}
function formatter(data) {
  var parentPath = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "/";
  return data.map(function (item) {
    var path = item.path;

    if (!isUrl(path)) {
      path = parentPath + item.path;
    }

    var result = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {
      path: path
    });

    if (item.children) {
      result.children = formatter(item.children, "".concat(parentPath).concat(item.path, "/"));
    }

    return result;
  });
}
var getMenuData = function getMenuData() {
  return formatter(menuData);
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL21lbnUudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tbW9uL21lbnUudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImNvbnN0IG1lbnVEYXRhID0gW1xuICB7XG4gICAgbmFtZTogXCJIb21lXCIsXG4gICAgaWNvbjogXCJob21lXCIsXG4gICAgcGF0aDogXCJob21lXCIsXG4gICAgaXRlbUNvZGU6IFwiTUkwMVwiLFxuICAgIGNoaWxkcmVuOiBbXG4gICAgICB7XG4gICAgICAgIHBhdGg6IFwiZGFzaGJvYXJkXCIsXG4gICAgICAgIGl0ZW1Db2RlOiBcIk1JMDEwMVwiLFxuICAgICAgICBuYW1lOiBcIkRhc2hib2FyZFwiLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgbmFtZTogXCJBZG1pbmlzdHJhdGlvblwiLFxuICAgIGljb246IFwicm9ja2V0XCIsXG4gICAgcGF0aDogXCJhZG1pblwiLFxuICAgIGl0ZW1Db2RlOiBcIk1JMDNcIixcbiAgICBjaGlsZHJlbjogW1xuICAgICAge1xuICAgICAgICBwYXRoOiBcImhvbWVcIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkwMzAxXCIsXG4gICAgICAgIG5hbWU6IFwiQWRtaW5cIixcbiAgICAgIH0sXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiVHJhbnNhY3Rpb25zXCIsXG4gICAgaWNvbjogXCJ0YWJsZVwiLFxuICAgIHBhdGg6IFwidHJhbnNhY3Rpb25zXCIsXG4gICAgaXRlbUNvZGU6IFwiTUkwNFwiLFxuICAgIGNoaWxkcmVuOiBbXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiTmV3XCIsXG4gICAgICAgIHBhdGg6IFwibmV3L3F1b3RlL2xpc3RcIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkwNDAwXCIsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiBcIlBvbGljaWVzXCIsXG4gICAgICAgIHBhdGg6IFwicG9saWN5TGlzdFwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA0MDFcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiRW5kb3JzZW1lbnRzXCIsXG4gICAgICAgIHBhdGg6IFwiZW5kb3JzZW1lbnRzXCIsXG4gICAgICAgIGl0ZW1Db2RlOiBcIk1JMDQwMlwiLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogXCJVbmRlcndyaXRpbmdcIixcbiAgICAgICAgcGF0aDogXCJ1d1wiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA0MDRcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiTWFzdGVyIEFncmVlbWVudFwiLFxuICAgICAgICBwYXRoOiBcIm1hc3Rlci9xdWVyeVwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA0MDdcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiSW5zcGVjdGlvbiBBcHByb3ZhbFwiLFxuICAgICAgICBwYXRoOiBcImluc3BlY3Rpb25cIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkwNDA1XCIsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiBcIkJsYWNrbGlzdCBBcHByb3ZhbFwiLFxuICAgICAgICBwYXRoOiBcImJsYWNrbGlzdFwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA0MDZcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiQ2xhaW1zXCIsXG4gICAgICAgIHBhdGg6IFwiY2xhaW1zXCIsXG4gICAgICAgIGl0ZW1Db2RlOiBcIk1JMDQwM1wiLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgbmFtZTogXCJDbGFpbXNcIixcbiAgICBpY29uOiBcInNvbHV0aW9uXCIsXG4gICAgcGF0aDogXCJjbGFpbXNcIixcbiAgICBpdGVtQ29kZTogXCJNSTA3XCIsXG4gICAgY2hpbGRyZW46IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogXCJGTk9MXCIsXG4gICAgICAgIHBhdGg6IFwiY2FsbGNlbnRlclwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA3MDFcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiQ2xhaW0gSGFuZGxpbmdcIixcbiAgICAgICAgcGF0aDogXCJoYW5kbGluZ1wiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA3MDJcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiU2V0dGxlbWVudCBBcHByb3ZhbFwiLFxuICAgICAgICBwYXRoOiBcInNldHRsZW1lbnQtYXBwcm92YWxcIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkwNzAzXCIsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiBcIkNhc2ggQ2FsbFwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA3MDRcIixcbiAgICAgICAgcGF0aDogXCJjYXNoLWNhbGxcIixcbiAgICAgICAgY2hpbGRyZW46IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIlRvIEJlIENvbmZpcm1lZFwiLFxuICAgICAgICAgICAgaXRlbUNvZGU6IFwiTUkwNzA0MDFcIixcbiAgICAgICAgICAgIHBhdGg6IFwiY29uZmlybWVkXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIkNhc2ggQ2FsbCBIYW5kbGluZ1wiLFxuICAgICAgICAgICAgaXRlbUNvZGU6IFwiTUkwNzA0MDJcIixcbiAgICAgICAgICAgIHBhdGg6IFwiaGFuZGxpbmdcIixcbiAgICAgICAgICB9LFxuICAgICAgICBdLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICB7XG4gICAgbmFtZTogXCJGaW5hbmNlXCIsXG4gICAgaWNvbjogXCJwYXktY2lyY2xlXCIsXG4gICAgcGF0aDogXCJiY3BcIixcbiAgICBpdGVtQ29kZTogXCJNSTA2XCIsXG4gICAgY2hpbGRyZW46IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogXCJDb2xsZWN0aW9uXCIsXG4gICAgICAgIHBhdGg6IFwiY29sbGVjdGlvblwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA2MDFcIixcbiAgICAgICAgY2hpbGRyZW46IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIkNvbGxlY3Rpb25cIixcbiAgICAgICAgICAgIHBhdGg6IFwiaW5kZXhcIixcbiAgICAgICAgICAgIGl0ZW1Db2RlOiBcIk1JMDYwMTAxXCIsXG4gICAgICAgICAgfSwge1xuICAgICAgICAgICAgbmFtZTogXCJCYWxhbmNlIENvbGxlY3Rpb25cIixcbiAgICAgICAgICAgIHBhdGg6IFwiYmFsYW5jZVwiLFxuICAgICAgICAgICAgaXRlbUNvZGU6IFwiTUkwNjAxMDJcIixcbiAgICAgICAgICB9LCB7XG4gICAgICAgICAgICBuYW1lOiBcIkJ1bGsgQ29sbGVjdGlvblwiLFxuICAgICAgICAgICAgcGF0aDogXCJidWxrXCIsXG4gICAgICAgICAgICBpdGVtQ29kZTogXCJNSTA2MDEwM1wiLFxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogXCJFbGVjdHJvbmljIENvbGxlY3Rpb25cIixcbiAgICAgICAgICAgIHBhdGg6IFwiYmlsbFBheW1lbnRcIixcbiAgICAgICAgICAgIGl0ZW1Db2RlOiBcIk1JMDYwMTA0XCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIkRpcmVjdCBEZWJpdFwiLFxuICAgICAgICAgICAgcGF0aDogXCJhdXRvRGViaXRcIixcbiAgICAgICAgICAgIGl0ZW1Db2RlOiBcIk1JMDYwMTA1XCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIkNvbGxlY3Rpb24gRW5xdWlyeVwiLFxuICAgICAgICAgICAgcGF0aDogXCJlbnF1aXJ5XCIsXG4gICAgICAgICAgICBpdGVtQ29kZTogXCJNSTA2MDEyMFwiLFxuICAgICAgICAgIH0sXG4gICAgICAgIF0sXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBwYXRoOiBcIm15QWNjb3VudHNcIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkwNjA3XCIsXG4gICAgICAgIG5hbWU6IFwiUGF5bWVudFwiLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogXCJQYXltZW50XCIsXG4gICAgICAgIHBhdGg6IFwicGF5bWVudFwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA2MDRcIixcbiAgICAgICAgY2hpbGRyZW46IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIlBheW1lbnRcIixcbiAgICAgICAgICAgIHBhdGg6IFwiaW5kZXhcIixcbiAgICAgICAgICAgIGl0ZW1Db2RlOiBcIk1JMDYwNDAxXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIkJhbGFuY2UgUmVmdW5kXCIsXG4gICAgICAgICAgICBwYXRoOiBcImJhbGFuY2VcIixcbiAgICAgICAgICAgIGl0ZW1Db2RlOiBcIk1JMDYwNDAyXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIkJ1bGsgUGF5bWVudFwiLFxuICAgICAgICAgICAgcGF0aDogXCJidWxrXCIsXG4gICAgICAgICAgICBpdGVtQ29kZTogXCJNSTA2MDQwM1wiLFxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogXCJQYXltZW50IEFwcHJvdmFsXCIsXG4gICAgICAgICAgICBwYXRoOiBcImFwcHJvdmFsXCIsXG4gICAgICAgICAgICBpdGVtQ29kZTogXCJNSTA2MDQwNFwiLFxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogXCJEaXJlY3QgQ3JlZGl0XCIsXG4gICAgICAgICAgICBwYXRoOiBcImRpcmVjdFwiLFxuICAgICAgICAgICAgaXRlbUNvZGU6IFwiTUkwNjA0MDVcIixcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6IFwiUGF5bWVudCBFbnF1aXJ5XCIsXG4gICAgICAgICAgICBwYXRoOiBcImVucXVpcnlcIixcbiAgICAgICAgICAgIGl0ZW1Db2RlOiBcIk1JMDYwNDIwXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgXSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiUmVmZXJyYWwgRmVlIEFwcHJvdmFsXCIsXG4gICAgICAgIHBhdGg6IFwicmVmZXJyYWxGZWVcIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkwNjA5XCIsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiBcIkFtZW5kIERpcmVjdCBEZWJpdCBCYW5rIEFjY291bnRcIixcbiAgICAgICAgcGF0aDogXCJiYW5rQWNjb3VudFwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA2MDZcIixcbiAgICAgIH0sXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiQ3VzdG9tZXJzXCIsXG4gICAgaWNvbjogXCJ1c2VyZ3JvdXAtYWRkXCIsXG4gICAgcGF0aDogXCJjdXN0b21lcnNcIixcbiAgICBpdGVtQ29kZTogXCJNSTA5XCIsXG4gICAgY2hpbGRyZW46IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogXCJDdXN0b21lcnNcIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkwOTAxXCIsXG4gICAgICAgIC8vIGljb246ICdwcm9maWxlJyxcbiAgICAgICAgcGF0aDogXCJsaXN0XCIsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcImVLWUNcIixcbiAgICBpY29uOiBcImt5Y1wiLFxuICAgIHBhdGg6IFwiZWt5Y1wiLFxuICAgIGl0ZW1Db2RlOiBcIk1JNDBcIixcbiAgICBjaGlsZHJlbjogW1xuICAgICAge1xuICAgICAgICBuYW1lOiBcImVLWUMgUXVlcmllc1wiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTQwMDFcIixcbiAgICAgICAgcGF0aDogXCJxdWVyaWVzXCIsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcIlJlaW5zdXJhbmNlXCIsXG4gICAgaWNvbjogXCJpbnN1cmFuY2VcIixcbiAgICBwYXRoOiBcInJlaW5zdXJhbmNlXCIsXG4gICAgaXRlbUNvZGU6IFwiTUkyMFwiLFxuICAgIGNoaWxkcmVuOiBbXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiUmlzayBBY2N1bXVsYXRpb25cIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkyMDAzXCIsXG4gICAgICAgIHBhdGg6IFwicmlza0FjY3VtdWxhdGlvblwiLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogXCJDYXNoIENhbGxcIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkyMDA1XCIsXG4gICAgICAgIHBhdGg6IFwiY2FzaC1jYWxsXCIsXG4gICAgICAgIGNoaWxkcmVuOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogXCJUbyBCZSBDb25maXJtZWRcIixcbiAgICAgICAgICAgIGl0ZW1Db2RlOiBcIk1JMjAwNTAxXCIsXG4gICAgICAgICAgICBwYXRoOiBcImNvbmZpcm1lZFwiLFxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogXCJDYXNoIENhbGwgSGFuZGxpbmdcIixcbiAgICAgICAgICAgIGl0ZW1Db2RlOiBcIk1JMjAwNTAyXCIsXG4gICAgICAgICAgICBwYXRoOiBcImhhbmRsaW5nXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgXSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiUkkgU3RhdGVtZW50XCIsXG4gICAgICAgIGl0ZW1Db2RlOiBcIk1JMjAwNFwiLFxuICAgICAgICBwYXRoOiBcInN0YXRlbWVudFwiLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogXCJUcmVhdHkgRGVmaW5pdGlvblwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTIwMDJcIixcbiAgICAgICAgcGF0aDogXCJ0cmVhdHlEZWZpbml0aW9uXCIsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcIkNhbXBhaWduc1wiLFxuICAgIGljb246IFwicm9ja2V0XCIsXG4gICAgcGF0aDogXCJjYW1wYWlnbnNcIixcbiAgICBpdGVtQ29kZTogXCJNSTEwXCIsXG4gICAgY2hpbGRyZW46IFtcbiAgICAgIHtcbiAgICAgICAgcGF0aDogXCJxdWVyeVwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTEwMDFcIixcbiAgICAgICAgbmFtZTogXCJDYW1wYWlnbnNcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHBhdGg6IFwidm91Y2hlclwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTEwMDNcIixcbiAgICAgICAgbmFtZTogXCJWb3VjaGVyc1wiLFxuICAgICAgICBjaGlsZHJlbjogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6IFwiVm91Y2hlcnNcIixcbiAgICAgICAgICAgIGl0ZW1Db2RlOiBcIk1JMTAwMzAxXCIsXG4gICAgICAgICAgICBwYXRoOiBcInF1ZXJ5XCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIlZvdWNoZXJzIFNldHVwXCIsXG4gICAgICAgICAgICBpdGVtQ29kZTogXCJNSTEwMDMwMlwiLFxuICAgICAgICAgICAgcGF0aDogXCJ0eXBlL3F1ZXJ5XCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgXSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHBhdGg6IFwicmVkZW1wdGlvblwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTEwMDRcIixcbiAgICAgICAgbmFtZTogXCJSZWRlbXB0aW9uXCIsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBwYXRoOiBcIm1vbml0b3JcIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkxMDAyXCIsXG4gICAgICAgIG5hbWU6IFwiQ2FtcGFpZ24gTW9uaXRvcmluZ1wiLFxuICAgICAgICBjaGlsZHJlbjogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6IFwiQ2FtcGFpZ24gTW9uaXRvcmluZ1wiLFxuICAgICAgICAgICAgaXRlbUNvZGU6IFwiTUkxMDAyMDFcIixcbiAgICAgICAgICAgIHBhdGg6IFwiY2FtcGFpZ25cIixcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6IFwiVm91Y2hlciBNb25pdG9yaW5nXCIsXG4gICAgICAgICAgICBpdGVtQ29kZTogXCJNSTEwMDIwMlwiLFxuICAgICAgICAgICAgcGF0aDogXCJ2b3VjaGVyXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgXSxcbiAgICAgIH0sXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiQ29uZmlndXJhdGlvbnNcIixcbiAgICBpY29uOiBcImNvbmZpZ3VyYXRpb25cIixcbiAgICBwYXRoOiBcImNvbmZpZ3VyYXRpb25cIixcbiAgICBpdGVtQ29kZTogXCJNSTMwXCIsXG4gICAgY2hpbGRyZW46IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogXCJXaGl0ZS1MYWJlbGluZ1wiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTMwMDFcIixcbiAgICAgICAgcGF0aDogXCJ3aGl0ZS1sYWJlbGluZ1wiLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogXCJSSSBTdGFrZWhvbGRlcnNcIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkzMDAyXCIsXG4gICAgICAgIHBhdGg6IFwicmktc3Rha2Vob2xkZXJcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiQ2xhaW0gU3Rha2Vob2xkZXJzXCIsXG4gICAgICAgIGl0ZW1Db2RlOiBcIk1JMzAwM1wiLFxuICAgICAgICBwYXRoOiBcImNsYWltLXN0YWtlaG9sZGVyXCIsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiBcIkxldHRlciBUZW1wbGF0ZXNcIixcbiAgICAgICAgaXRlbUNvZGU6IFwiTUkzMDA1XCIsXG4gICAgICAgIHBhdGg6IFwibGV0dGVyLXRlbXBsYXRlc1wiLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogXCJFeHRlbmRlZCBDbGF1c2VzXCIsXG4gICAgICAgIGl0ZW1Db2RlOiBcIk1JMzAwNFwiLFxuICAgICAgICBwYXRoOiBcImV4dGVuZGVkLWNsYXVzZXNcIixcbiAgICAgIH0sXG4gICAgXSxcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiUmVwb3J0XCIsXG4gICAgaWNvbjogXCJwcm9qZWN0XCIsXG4gICAgcGF0aDogXCJyZXBvcnRzXCIsXG4gICAgaXRlbUNvZGU6IFwiTUkwNVwiLFxuICAgIGNoaWxkcmVuOiBbXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiU2FsZXMgUmVwb3J0XCIsXG4gICAgICAgIGl0ZW1Db2RlOiBcIk1JMDUwMVwiLFxuICAgICAgICBwYXRoOiBcInNhbGVzXCIsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiBcIkNvbW1pc3Npb24gUmVwb3J0XCIsXG4gICAgICAgIGl0ZW1Db2RlOiBcIk1JMDUwMlwiLFxuICAgICAgICBwYXRoOiBcImNvbW1pc3Npb25cIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6IFwiTW9yZVwiLFxuICAgICAgICBpdGVtQ29kZTogXCJNSTA1MDNcIixcbiAgICAgICAgcGF0aDogXCJtb3JlXCIsXG4gICAgICB9LFxuICAgIF0sXG4gIH0sXG5dO1xuY29uc3QgcmVnID0gLygoKF5odHRwcz86KD86XFwvXFwvKT8pKD86Wy07OiY9XFwrXFwkLFxcd10rQCk/W0EtWmEtejAtOS4tXSt8KD86d3d3LnxbLTs6Jj1cXCtcXCQsXFx3XStAKVtBLVphLXowLTkuLV0rKSgoPzpcXC9bXFwrfiVcXC8uXFx3LV9dKik/XFw/Pyg/OlstXFwrPSY7JUAuXFx3X10qKSM/KD86W1xcd10qKSk/KSQvZztcblxuZXhwb3J0IGZ1bmN0aW9uIGlzVXJsKHBhdGg6IGFueSkge1xuICByZXR1cm4gcmVnLnRlc3QocGF0aCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBmb3JtYXR0ZXIoZGF0YTogYW55LCBwYXJlbnRQYXRoID0gXCIvXCIpIHtcbiAgcmV0dXJuIGRhdGEubWFwKChpdGVtOiBhbnkpID0+IHtcbiAgICBsZXQgeyBwYXRoIH0gPSBpdGVtO1xuICAgIGlmICghaXNVcmwocGF0aCkpIHtcbiAgICAgIHBhdGggPSBwYXJlbnRQYXRoICsgaXRlbS5wYXRoO1xuICAgIH1cbiAgICBjb25zdCByZXN1bHQgPSB7XG4gICAgICAuLi5pdGVtLFxuICAgICAgcGF0aCxcbiAgICB9O1xuICAgIGlmIChpdGVtLmNoaWxkcmVuKSB7XG4gICAgICByZXN1bHQuY2hpbGRyZW4gPSBmb3JtYXR0ZXIoaXRlbS5jaGlsZHJlbiwgYCR7cGFyZW50UGF0aH0ke2l0ZW0ucGF0aH0vYCk7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHQ7XG4gIH0pO1xufVxuXG5leHBvcnQgY29uc3QgZ2V0TWVudURhdGEgPSAoKSA9PiBmb3JtYXR0ZXIobWVudURhdGEpO1xuXG4iXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTkE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFOQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBekNBO0FBaURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFWQTtBQXJCQTtBQXlDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUE1QkE7QUFvQ0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQTlCQTtBQXNDQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBeEZBO0FBZ0dBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFOQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQVZBO0FBa0JBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFqQ0E7QUF5Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBVkE7QUFrQkE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFWQTtBQWpDQTtBQXFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQTFCQTtBQWtDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBaEJBO0FBd0JBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/common/menu.tsx
