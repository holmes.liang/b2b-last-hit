__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SideInfo; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _desk_master_line_list__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk/master/line-list */ "./src/app/desk/master/line-list.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/component/side-info.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_5__["default"])(["\n  padding-right: 20px;\n  .info-title {\n    font-size: 14px;\n    color: gray;\n    text-transform: uppercase;\n   }\n   .info-content {\n    font-size: 16px;\n    color: rgb(0, 0, .85);\n   }\n   .info-bottom {\n    .info-icon { \n      border: 1px solid;\n      border-radius: 50%;\n      padding: 3px;\n      cursor: pointer;\n      &:first-child {\n        margin-right:10px;\n      }\n      &:hover {\n        color: ", ";\n      }\n    }\n   }\n  .side-info-header {\n     padding: 20px 0;\n     text-align: center;\n     border-bottom: 1px dashed #e8e8e8;\n  }\n  .side-info-time {\n    padding: 30px 0 0 40px;\n  }\n  .ant-timeline-item-content {\n    font-size: 18px;\n    color: rgb(0, 0, .85);\n\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}









var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_11__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var SideInfoStyle = _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject(), COLOR_A);

var SideInfo =
/*#__PURE__*/
function (_Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(SideInfo, _Component);

  function SideInfo() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, SideInfo);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(SideInfo)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.getPremium = function () {
      var _this$props = _this.props,
          premiumId = _this$props.premiumId,
          model = _this$props.model;

      var currencyCode = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "premCurrencyCode");

      var policy = {
        openEnd: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "openEnd"),
        recurringPayment: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "recurringPayment"),
        policyPremium: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, premiumId || "cartPremium"),
        currencyCode: currencyCode,
        premiumDesc: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "premiumDesc")
      };
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        style: {
          marginRight: "5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].renderPolicyPremium(policy)));
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(SideInfo, [{
    key: "getProp",
    value: function getProp(propName) {
      var productCate = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.props.model, "productCate", "");

      var productCode = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.props.model, "productCode", "");

      var infoPropName;

      if (["LIFE"].includes(productCate)) {
        infoPropName = "insured";
      } else if (["PH", "PA"].includes(productCode)) {
        infoPropName = "payer";
      } else {
        infoPropName = "policyholder";
      }

      var endoFixed = this.props.endoFixed;

      if (endoFixed) {
        return "".concat(endoFixed, ".").concat(infoPropName, ".").concat(propName);
      }

      return "ext.".concat(infoPropName, ".").concat(propName);
    }
  }, {
    key: "renderPlan",
    value: function renderPlan() {
      var model = this.props.model;
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 103
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "planName"));
    }
  }, {
    key: "render",
    value: function render() {
      if (_common__WEBPACK_IMPORTED_MODULE_12__["Utils"].getIsInApp()) return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112
        },
        __self: this
      });
      var list = this.props.lineList || _desk_master_line_list__WEBPACK_IMPORTED_MODULE_10__["LineList"];
      var _this$props2 = this.props,
          activeStep = _this$props2.activeStep,
          model = _this$props2.model,
          showList = _this$props2.showList;
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Col"], {
        sm: 5,
        xs: 24,
        style: {
          borderRight: "1px solid #e8e8e8"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 116
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(SideInfoStyle, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "quoteNo") ? react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "side-info-header",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 119
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "quoteNo")), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "productCode"), " - ", lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "productName")), this.renderPlan(), this.getPremium()) : lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "endoNo") && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "side-info-header",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "endoNo")), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 128
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "productCode"), " - ", lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "productName")), this.renderPlan(), this.getPremium()), lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("name")) && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "side-info-header",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 136
        },
        __self: this
      }, "Customer"), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("name"))), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-bottom",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 138
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Tooltip"], {
        placement: "top",
        title: "".concat(lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("mobileNationCode"), "") || "", " ").concat(lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("mobile"), "") || ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        className: "info-icon",
        type: "phone",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 142
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Tooltip"], {
        placement: "top",
        title: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("email"), "") || "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        className: "info-icon",
        type: "mail",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 145
        },
        __self: this
      })))), lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "scName") && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "side-info-header",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 150
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 151
        },
        __self: this
      }, "CHANNEL"), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 152
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "scCode"), " - ", lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "scName")), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-bottom",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 153
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Tooltip"], {
        placement: "top",
        title: "".concat(lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("mobileNationCode"), "") || "", " ").concat(lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("mobile"), "") || ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 154
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        className: "info-icon",
        type: "phone",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 157
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Tooltip"], {
        placement: "top",
        title: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("email"), "") || "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 159
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        className: "info-icon",
        type: "mail",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 160
        },
        __self: this
      })))), showList && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "side-info-time",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 165
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Timeline"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166
        },
        __self: this
      }, list.map(function (item, i) {
        return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Timeline"].Item, {
          dot: react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
            type: "minus-circle",
            style: {
              fontSize: 16
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 171
            },
            __self: this
          }),
          key: i,
          color: activeStep === item ? "green" : "gray",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 170
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
          style: {
            color: activeStep === item ? "green" : "",
            borderBottom: activeStep === item ? "1px solid green" : "",
            fontWeight: activeStep === item ? 600 : 300
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 173
          },
          __self: this
        }, item));
      })))));
    }
  }]);

  return SideInfo;
}(react__WEBPACK_IMPORTED_MODULE_6__["Component"]);

SideInfo.defaultProps = {
  showList: true
};

;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3NpZGUtaW5mby50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9TQUlDL2lhci9jb21wb25lbnQvc2lkZS1pbmZvLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBDb2wsIEljb24sIFRpbWVsaW5lLCBUb29sdGlwIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgTGluZUxpc3QgfSBmcm9tIFwiQGRlc2svbWFzdGVyL2xpbmUtbGlzdFwiO1xuaW1wb3J0IFRoZW1lIGZyb20gXCJAc3R5bGVzXCI7XG5pbXBvcnQgeyBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5cbmNvbnN0IHsgQ09MT1JfQSB9ID0gVGhlbWUuZ2V0VGhlbWUoKTtcblxuaW50ZXJmYWNlIElQcm9wcyB7XG4gIGxpbmVMaXN0PzogYW55LFxuICBtb2RlbDogYW55LFxuICBhY3RpdmVTdGVwOiBhbnksXG4gIHNob3dMaXN0PzogYm9vbGVhbixcbiAgZW5kb0ZpeGVkPzogYW55LFxuICBwcmVtaXVtSWQ6IGFueVxufVxuXG5jb25zdCBTaWRlSW5mb1N0eWxlID0gU3R5bGVkLmRpdmBcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgLmluZm8tdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBjb2xvcjogZ3JheTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgfVxuICAgLmluZm8tY29udGVudCB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGNvbG9yOiByZ2IoMCwgMCwgLjg1KTtcbiAgIH1cbiAgIC5pbmZvLWJvdHRvbSB7XG4gICAgLmluZm8taWNvbiB7IFxuICAgICAgYm9yZGVyOiAxcHggc29saWQ7XG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICBwYWRkaW5nOiAzcHg7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OjEwcHg7XG4gICAgICB9XG4gICAgICAmOmhvdmVyIHtcbiAgICAgICAgY29sb3I6ICR7Q09MT1JfQX07XG4gICAgICB9XG4gICAgfVxuICAgfVxuICAuc2lkZS1pbmZvLWhlYWRlciB7XG4gICAgIHBhZGRpbmc6IDIwcHggMDtcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICBib3JkZXItYm90dG9tOiAxcHggZGFzaGVkICNlOGU4ZTg7XG4gIH1cbiAgLnNpZGUtaW5mby10aW1lIHtcbiAgICBwYWRkaW5nOiAzMHB4IDAgMCA0MHB4O1xuICB9XG4gIC5hbnQtdGltZWxpbmUtaXRlbS1jb250ZW50IHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgY29sb3I6IHJnYigwLCAwLCAuODUpO1xuXG4gIH1cbmA7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpZGVJbmZvIGV4dGVuZHMgQ29tcG9uZW50PElQcm9wcywgYW55PiB7XG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgc2hvd0xpc3Q6IHRydWUsXG4gIH07XG5cbiAgZ2V0UHJvcChwcm9wTmFtZTogYW55KSB7XG4gICAgY29uc3QgcHJvZHVjdENhdGUgPSBfLmdldCh0aGlzLnByb3BzLm1vZGVsLCBcInByb2R1Y3RDYXRlXCIsIFwiXCIpO1xuICAgIGNvbnN0IHByb2R1Y3RDb2RlID0gXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgXCJwcm9kdWN0Q29kZVwiLCBcIlwiKTtcbiAgICBsZXQgaW5mb1Byb3BOYW1lOiBhbnk7XG4gICAgaWYgKFtcIkxJRkVcIl0uaW5jbHVkZXMocHJvZHVjdENhdGUpKSB7XG4gICAgICBpbmZvUHJvcE5hbWUgPSBcImluc3VyZWRcIjtcbiAgICB9IGVsc2UgaWYgKFtcIlBIXCIsIFwiUEFcIl0uaW5jbHVkZXMocHJvZHVjdENvZGUpKSB7XG4gICAgICBpbmZvUHJvcE5hbWUgPSBcInBheWVyXCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIGluZm9Qcm9wTmFtZSA9IFwicG9saWN5aG9sZGVyXCI7XG4gICAgfVxuICAgIGNvbnN0IHsgZW5kb0ZpeGVkIH0gPSB0aGlzLnByb3BzO1xuICAgIGlmIChlbmRvRml4ZWQpIHtcbiAgICAgIHJldHVybiBgJHtlbmRvRml4ZWR9LiR7aW5mb1Byb3BOYW1lfS4ke3Byb3BOYW1lfWA7XG4gICAgfVxuICAgIHJldHVybiBgZXh0LiR7aW5mb1Byb3BOYW1lfS4ke3Byb3BOYW1lfWA7XG4gIH1cblxuICBnZXRQcmVtaXVtID0gKCkgPT4ge1xuICAgIGNvbnN0IHsgcHJlbWl1bUlkLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBjdXJyZW5jeUNvZGUgPSBfLmdldChtb2RlbCwgXCJwcmVtQ3VycmVuY3lDb2RlXCIpO1xuICAgIGxldCBwb2xpY3kgPSB7XG4gICAgICBvcGVuRW5kOiBfLmdldChtb2RlbCwgXCJvcGVuRW5kXCIpLFxuICAgICAgcmVjdXJyaW5nUGF5bWVudDogXy5nZXQobW9kZWwsIFwicmVjdXJyaW5nUGF5bWVudFwiKSxcbiAgICAgIHBvbGljeVByZW1pdW06IF8uZ2V0KG1vZGVsLCAocHJlbWl1bUlkIHx8IFwiY2FydFByZW1pdW1cIikpLFxuICAgICAgY3VycmVuY3lDb2RlLFxuICAgICAgcHJlbWl1bURlc2M6IF8uZ2V0KG1vZGVsLCBcInByZW1pdW1EZXNjXCIpLFxuICAgIH07XG4gICAgcmV0dXJuIChcbiAgICAgIDxzcGFuPlxuICAgICAgICA8c3BhbiBzdHlsZT17eyBtYXJnaW5SaWdodDogXCI1cHhcIiB9fT57VXRpbHMucmVuZGVyUG9saWN5UHJlbWl1bShwb2xpY3kpfTwvc3Bhbj5cbiAgICAgIDwvc3Bhbj5cbiAgICApO1xuICB9O1xuXG4gIHJlbmRlclBsYW4oKSB7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gKFxuICAgICAgPHAgY2xhc3NOYW1lPXtcImluZm8tY29udGVudFwifT5cbiAgICAgICAge1xuICAgICAgICAgIF8uZ2V0KG1vZGVsLCBcInBsYW5OYW1lXCIpXG4gICAgICAgIH1cbiAgICAgIDwvcD5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGlmIChVdGlscy5nZXRJc0luQXBwKCkpIHJldHVybiA8ZGl2PjwvZGl2PjtcbiAgICBjb25zdCBsaXN0ID0gdGhpcy5wcm9wcy5saW5lTGlzdCB8fCBMaW5lTGlzdDtcbiAgICBjb25zdCB7IGFjdGl2ZVN0ZXAsIG1vZGVsLCBzaG93TGlzdCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gKFxuICAgICAgPENvbCBzbT17NX0geHM9ezI0fSBzdHlsZT17eyBib3JkZXJSaWdodDogXCIxcHggc29saWQgI2U4ZThlOFwiIH19PlxuICAgICAgICA8U2lkZUluZm9TdHlsZT5cbiAgICAgICAgICB7Xy5nZXQobW9kZWwsIFwicXVvdGVOb1wiKSA/IDxkaXYgY2xhc3NOYW1lPXtcInNpZGUtaW5mby1oZWFkZXJcIn0+XG4gICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLWNvbnRlbnRcIn0+e18uZ2V0KG1vZGVsLCBcInF1b3RlTm9cIil9PC9wPlxuICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9e1wiaW5mby1jb250ZW50XCJ9PlxuICAgICAgICAgICAgICAgIHtfLmdldChtb2RlbCwgXCJwcm9kdWN0Q29kZVwiKX0gLSB7Xy5nZXQobW9kZWwsIFwicHJvZHVjdE5hbWVcIil9XG4gICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAge3RoaXMucmVuZGVyUGxhbigpfVxuICAgICAgICAgICAgICB7dGhpcy5nZXRQcmVtaXVtKCl9XG4gICAgICAgICAgICA8L2Rpdj4gOlxuICAgICAgICAgICAgKF8uZ2V0KG1vZGVsLCBcImVuZG9Ob1wiKSAmJiA8ZGl2IGNsYXNzTmFtZT17XCJzaWRlLWluZm8taGVhZGVyXCJ9PlxuICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9e1wiaW5mby1jb250ZW50XCJ9PntfLmdldChtb2RlbCwgXCJlbmRvTm9cIil9PC9wPlxuICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9e1wiaW5mby1jb250ZW50XCJ9PlxuICAgICAgICAgICAgICAgIHtfLmdldChtb2RlbCwgXCJwcm9kdWN0Q29kZVwiKX0gLSB7Xy5nZXQobW9kZWwsIFwicHJvZHVjdE5hbWVcIil9XG4gICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAge3RoaXMucmVuZGVyUGxhbigpfVxuICAgICAgICAgICAgICB7dGhpcy5nZXRQcmVtaXVtKCl9XG4gICAgICAgICAgICA8L2Rpdj4pXG4gICAgICAgICAgfVxuICAgICAgICAgIHtfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwibmFtZVwiKSkgJiYgPGRpdiBjbGFzc05hbWU9e1wic2lkZS1pbmZvLWhlYWRlclwifT5cbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLXRpdGxlXCJ9PkN1c3RvbWVyPC9wPlxuICAgICAgICAgICAgPHAgY2xhc3NOYW1lPXtcImluZm8tY29udGVudFwifT57Xy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcIm5hbWVcIikpfTwvcD5cbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLWJvdHRvbVwifT5cbiAgICAgICAgICAgICAgPFRvb2x0aXAgcGxhY2VtZW50PVwidG9wXCIgdGl0bGU9e1xuICAgICAgICAgICAgICAgIGAke18uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJtb2JpbGVOYXRpb25Db2RlXCIpLCBcIlwiKSB8fCBcIlwifSAke18uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJtb2JpbGVcIiksIFwiXCIpIHx8IFwiXCJ9YFxuICAgICAgICAgICAgICB9PlxuICAgICAgICAgICAgICAgIDxJY29uIGNsYXNzTmFtZT17XCJpbmZvLWljb25cIn0gdHlwZT1cInBob25lXCIvPlxuICAgICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICAgIDxUb29sdGlwIHBsYWNlbWVudD1cInRvcFwiIHRpdGxlPXtfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwiZW1haWxcIiksIFwiXCIpIHx8IFwiXCJ9PlxuICAgICAgICAgICAgICAgIDxJY29uIGNsYXNzTmFtZT17XCJpbmZvLWljb25cIn0gdHlwZT1cIm1haWxcIi8+XG4gICAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICB9XG4gICAgICAgICAge18uZ2V0KG1vZGVsLCBcInNjTmFtZVwiKSAmJiA8ZGl2IGNsYXNzTmFtZT17XCJzaWRlLWluZm8taGVhZGVyXCJ9PlxuICAgICAgICAgICAgPHAgY2xhc3NOYW1lPXtcImluZm8tdGl0bGVcIn0+Q0hBTk5FTDwvcD5cbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLWNvbnRlbnRcIn0+e18uZ2V0KG1vZGVsLCBcInNjQ29kZVwiKX0gLSB7Xy5nZXQobW9kZWwsIFwic2NOYW1lXCIpfTwvcD5cbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLWJvdHRvbVwifT5cbiAgICAgICAgICAgICAgPFRvb2x0aXAgcGxhY2VtZW50PVwidG9wXCIgdGl0bGU9e1xuICAgICAgICAgICAgICAgIGAke18uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJtb2JpbGVOYXRpb25Db2RlXCIpLCBcIlwiKSB8fCBcIlwifSAke18uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJtb2JpbGVcIiksIFwiXCIpIHx8IFwiXCJ9YFxuICAgICAgICAgICAgICB9PlxuICAgICAgICAgICAgICAgIDxJY29uIGNsYXNzTmFtZT17XCJpbmZvLWljb25cIn0gdHlwZT1cInBob25lXCIvPlxuICAgICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICAgIDxUb29sdGlwIHBsYWNlbWVudD1cInRvcFwiIHRpdGxlPXtfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwiZW1haWxcIiksIFwiXCIpIHx8IFwiXCJ9PlxuICAgICAgICAgICAgICAgIDxJY29uIGNsYXNzTmFtZT17XCJpbmZvLWljb25cIn0gdHlwZT1cIm1haWxcIi8+XG4gICAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICB9XG4gICAgICAgICAge3Nob3dMaXN0ICYmIDxkaXYgY2xhc3NOYW1lPXtcInNpZGUtaW5mby10aW1lXCJ9PlxuICAgICAgICAgICAgPFRpbWVsaW5lPlxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGlzdC5tYXAoKGl0ZW06IGFueSwgaTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICA8VGltZWxpbmUuSXRlbVxuICAgICAgICAgICAgICAgICAgICAgIGRvdD17PEljb24gdHlwZT1cIm1pbnVzLWNpcmNsZVwiIHN0eWxlPXt7IGZvbnRTaXplOiAxNiB9fS8+fVxuICAgICAgICAgICAgICAgICAgICAgIGtleT17aX0gY29sb3I9e2FjdGl2ZVN0ZXAgPT09IGl0ZW0gPyBcImdyZWVuXCIgOiBcImdyYXlcIn0+XG4gICAgICAgICAgICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiBhY3RpdmVTdGVwID09PSBpdGVtID8gXCJncmVlblwiIDogXCJcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlckJvdHRvbTogYWN0aXZlU3RlcCA9PT0gaXRlbSA/IFwiMXB4IHNvbGlkIGdyZWVuXCIgOiBcIlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgZm9udFdlaWdodDogYWN0aXZlU3RlcCA9PT0gaXRlbSA/IDYwMCA6IDMwMCxcbiAgICAgICAgICAgICAgICAgICAgICB9fT57aXRlbX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDwvVGltZWxpbmUuSXRlbT5cbiAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgPC9UaW1lbGluZT5cbiAgICAgICAgICA8L2Rpdj59XG4gICAgICAgIDwvU2lkZUluZm9TdHlsZT5cbiAgICAgIDwvQ29sPlxuICAgICk7XG4gIH07XG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFVQTtBQUNBO0FBdUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1QkE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTs7Ozs7O0FBbENBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVhBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQW1CQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQU9BOzs7O0FBL0hBO0FBQ0E7QUFEQTtBQUVBO0FBREE7QUFEQTtBQWdJQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/component/side-info.tsx
