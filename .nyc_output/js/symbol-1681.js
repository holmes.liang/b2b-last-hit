/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
function _default(seriesType, defaultSymbolType, legendSymbol) {
  // Encoding visual for all series include which is filtered for legend drawing
  return {
    seriesType: seriesType,
    // For legend.
    performRawSeries: true,
    reset: function reset(seriesModel, ecModel, api) {
      var data = seriesModel.getData();
      var symbolType = seriesModel.get('symbol') || defaultSymbolType;
      var symbolSize = seriesModel.get('symbolSize');
      var keepAspect = seriesModel.get('symbolKeepAspect');
      data.setVisual({
        legendSymbol: legendSymbol || symbolType,
        symbol: symbolType,
        symbolSize: symbolSize,
        symbolKeepAspect: keepAspect
      }); // Only visible series has each data be visual encoded

      if (ecModel.isSeriesFiltered(seriesModel)) {
        return;
      }

      var hasCallback = typeof symbolSize === 'function';

      function dataEach(data, idx) {
        if (typeof symbolSize === 'function') {
          var rawValue = seriesModel.getRawValue(idx); // FIXME

          var params = seriesModel.getDataParams(idx);
          data.setItemVisual(idx, 'symbolSize', symbolSize(rawValue, params));
        }

        if (data.hasItemOption) {
          var itemModel = data.getItemModel(idx);
          var itemSymbolType = itemModel.getShallow('symbol', true);
          var itemSymbolSize = itemModel.getShallow('symbolSize', true);
          var itemSymbolKeepAspect = itemModel.getShallow('symbolKeepAspect', true); // If has item symbol

          if (itemSymbolType != null) {
            data.setItemVisual(idx, 'symbol', itemSymbolType);
          }

          if (itemSymbolSize != null) {
            // PENDING Transform symbolSize ?
            data.setItemVisual(idx, 'symbolSize', itemSymbolSize);
          }

          if (itemSymbolKeepAspect != null) {
            data.setItemVisual(idx, 'symbolKeepAspect', itemSymbolKeepAspect);
          }
        }
      }

      return {
        dataEach: data.hasItemOption || hasCallback ? dataEach : null
      };
    }
  };
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdmlzdWFsL3N5bWJvbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3Zpc3VhbC9zeW1ib2wuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbmZ1bmN0aW9uIF9kZWZhdWx0KHNlcmllc1R5cGUsIGRlZmF1bHRTeW1ib2xUeXBlLCBsZWdlbmRTeW1ib2wpIHtcbiAgLy8gRW5jb2RpbmcgdmlzdWFsIGZvciBhbGwgc2VyaWVzIGluY2x1ZGUgd2hpY2ggaXMgZmlsdGVyZWQgZm9yIGxlZ2VuZCBkcmF3aW5nXG4gIHJldHVybiB7XG4gICAgc2VyaWVzVHlwZTogc2VyaWVzVHlwZSxcbiAgICAvLyBGb3IgbGVnZW5kLlxuICAgIHBlcmZvcm1SYXdTZXJpZXM6IHRydWUsXG4gICAgcmVzZXQ6IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCwgZWNNb2RlbCwgYXBpKSB7XG4gICAgICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgICAgIHZhciBzeW1ib2xUeXBlID0gc2VyaWVzTW9kZWwuZ2V0KCdzeW1ib2wnKSB8fCBkZWZhdWx0U3ltYm9sVHlwZTtcbiAgICAgIHZhciBzeW1ib2xTaXplID0gc2VyaWVzTW9kZWwuZ2V0KCdzeW1ib2xTaXplJyk7XG4gICAgICB2YXIga2VlcEFzcGVjdCA9IHNlcmllc01vZGVsLmdldCgnc3ltYm9sS2VlcEFzcGVjdCcpO1xuICAgICAgZGF0YS5zZXRWaXN1YWwoe1xuICAgICAgICBsZWdlbmRTeW1ib2w6IGxlZ2VuZFN5bWJvbCB8fCBzeW1ib2xUeXBlLFxuICAgICAgICBzeW1ib2w6IHN5bWJvbFR5cGUsXG4gICAgICAgIHN5bWJvbFNpemU6IHN5bWJvbFNpemUsXG4gICAgICAgIHN5bWJvbEtlZXBBc3BlY3Q6IGtlZXBBc3BlY3RcbiAgICAgIH0pOyAvLyBPbmx5IHZpc2libGUgc2VyaWVzIGhhcyBlYWNoIGRhdGEgYmUgdmlzdWFsIGVuY29kZWRcblxuICAgICAgaWYgKGVjTW9kZWwuaXNTZXJpZXNGaWx0ZXJlZChzZXJpZXNNb2RlbCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgaGFzQ2FsbGJhY2sgPSB0eXBlb2Ygc3ltYm9sU2l6ZSA9PT0gJ2Z1bmN0aW9uJztcblxuICAgICAgZnVuY3Rpb24gZGF0YUVhY2goZGF0YSwgaWR4KSB7XG4gICAgICAgIGlmICh0eXBlb2Ygc3ltYm9sU2l6ZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgIHZhciByYXdWYWx1ZSA9IHNlcmllc01vZGVsLmdldFJhd1ZhbHVlKGlkeCk7IC8vIEZJWE1FXG5cbiAgICAgICAgICB2YXIgcGFyYW1zID0gc2VyaWVzTW9kZWwuZ2V0RGF0YVBhcmFtcyhpZHgpO1xuICAgICAgICAgIGRhdGEuc2V0SXRlbVZpc3VhbChpZHgsICdzeW1ib2xTaXplJywgc3ltYm9sU2l6ZShyYXdWYWx1ZSwgcGFyYW1zKSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZGF0YS5oYXNJdGVtT3B0aW9uKSB7XG4gICAgICAgICAgdmFyIGl0ZW1Nb2RlbCA9IGRhdGEuZ2V0SXRlbU1vZGVsKGlkeCk7XG4gICAgICAgICAgdmFyIGl0ZW1TeW1ib2xUeXBlID0gaXRlbU1vZGVsLmdldFNoYWxsb3coJ3N5bWJvbCcsIHRydWUpO1xuICAgICAgICAgIHZhciBpdGVtU3ltYm9sU2l6ZSA9IGl0ZW1Nb2RlbC5nZXRTaGFsbG93KCdzeW1ib2xTaXplJywgdHJ1ZSk7XG4gICAgICAgICAgdmFyIGl0ZW1TeW1ib2xLZWVwQXNwZWN0ID0gaXRlbU1vZGVsLmdldFNoYWxsb3coJ3N5bWJvbEtlZXBBc3BlY3QnLCB0cnVlKTsgLy8gSWYgaGFzIGl0ZW0gc3ltYm9sXG5cbiAgICAgICAgICBpZiAoaXRlbVN5bWJvbFR5cGUgIT0gbnVsbCkge1xuICAgICAgICAgICAgZGF0YS5zZXRJdGVtVmlzdWFsKGlkeCwgJ3N5bWJvbCcsIGl0ZW1TeW1ib2xUeXBlKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAoaXRlbVN5bWJvbFNpemUgIT0gbnVsbCkge1xuICAgICAgICAgICAgLy8gUEVORElORyBUcmFuc2Zvcm0gc3ltYm9sU2l6ZSA/XG4gICAgICAgICAgICBkYXRhLnNldEl0ZW1WaXN1YWwoaWR4LCAnc3ltYm9sU2l6ZScsIGl0ZW1TeW1ib2xTaXplKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAoaXRlbVN5bWJvbEtlZXBBc3BlY3QgIT0gbnVsbCkge1xuICAgICAgICAgICAgZGF0YS5zZXRJdGVtVmlzdWFsKGlkeCwgJ3N5bWJvbEtlZXBBc3BlY3QnLCBpdGVtU3ltYm9sS2VlcEFzcGVjdCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIGRhdGFFYWNoOiBkYXRhLmhhc0l0ZW1PcHRpb24gfHwgaGFzQ2FsbGJhY2sgPyBkYXRhRWFjaCA6IG51bGxcbiAgICAgIH07XG4gICAgfVxuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUF0REE7QUF3REE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/visual/symbol.js
