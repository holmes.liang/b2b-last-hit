__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NFormItem", function() { return NFormItem; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/component/NFormItem.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_5__["default"])(["\n    .ant-form-item-label > label {\n      color: #9e9e9e;\n    }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}





var LabelInput = _common_3rd__WEBPACK_IMPORTED_MODULE_6__["Styled"].div(_templateObject());

var NFormItem =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(NFormItem, _ModelWidget);

  function NFormItem(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, NFormItem);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(NFormItem).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(NFormItem, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        LabelInput: LabelInput
      };
    }
  }, {
    key: "getValue",
    value: function getValue(event) {
      //if the env is undefined or null, return directly
      if (event === undefined || event === null) return; //for input

      var value = event;

      if (event.target) {
        var target = event.target;
        value = target.type === "checkbox" ? target.checked : target.value;
      }

      if (typeof value === "boolean") {
        value = value ? "Y" : "N";
      }

      return value;
    }
  }, {
    key: "getRules",
    value: function getRules() {
      return this.props.rules || [];
    }
  }, {
    key: "renderFormValue",
    value: function renderFormValue() {
      var _this = this;

      var _this$props = this.props,
          form = _this$props.form,
          label = _this$props.label,
          propName = _this$props.propName,
          _onChange = _this$props.onChange,
          required = _this$props.required,
          transform = _this$props.transform,
          _this$props$showPlace = _this$props.showPlaceholder,
          showPlaceholder = _this$props$showPlace === void 0 ? false : _this$props$showPlace;
      var getFieldDecorator = form.getFieldDecorator;
      return getFieldDecorator(propName.toString(), {
        initialValue: showPlaceholder ? _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isNull(this.getValueFromModel(propName)) || _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isUndefined(this.getValueFromModel(propName)) ? undefined : this.getValueFromModel(propName) : _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isNull(this.getValueFromModel(propName)) || _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isUndefined(this.getValueFromModel(propName)) ? "" : this.getValueFromModel(propName),
        getValueFromEvent: function getValueFromEvent(event) {
          if (transform) return transform();
          return _this.getValue(event);
        },
        rules: [{
          required: required,
          message: _common__WEBPACK_IMPORTED_MODULE_8__["Language"].en("".concat(label === " " || !label ? propName : label, " is required")).thai("".concat(label === " " || !label ? propName : label, " is required")).getMessage()
        }].concat(this.getRules()),
        onChange: function onChange(event) {
          var value = _this.getValue(event);

          _this.setValueToModel(value);

          if (_onChange) {
            _onChange(value);
          }
        },
        validateFirst: true
      })(this.props.children);
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      var _this$props2 = this.props,
          label = _this$props2.label,
          layoutCol = _this$props2.layoutCol;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Form"].Item, Object.assign({
        label: label,
        colon: false
      }, layoutCol || _common__WEBPACK_IMPORTED_MODULE_8__["Consts"].FORM_ITEM_LAYOUT, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }), this.props.renderBox ? this.props.renderBox(this.renderFormValue()) : this.renderFormValue());
    }
  }]);

  return NFormItem;
}(_component__WEBPACK_IMPORTED_MODULE_7__["ModelWidget"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tcG9uZW50L05Gb3JtSXRlbS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21wb25lbnQvTkZvcm1JdGVtLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBDb25zdHMsIExhbmd1YWdlLCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzLCBQYWdlQ29tcG9uZW50cyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IEZvcm0gfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgRm9ybUNvbXBvbmVudFByb3BzIH0gZnJvbSBcImFudGQvbGliL2Zvcm0vRm9ybVwiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuXG5cbmV4cG9ydCB0eXBlIFN0eWxlZElOUFVUID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImlucHV0XCIsIGFueSwgeyByZWFkT25seT86IGJvb2xlYW4gfSwgbmV2ZXI+O1xuZXhwb3J0IHR5cGUgTkZvcm1JdGVtUGFnZUNvbXBvbmVudHMgPSB7XG4gIExhYmVsSW5wdXQ6IFN0eWxlZElOUFVUXG59ICYgUGFnZUNvbXBvbmVudHM7XG5jb25zdCBMYWJlbElucHV0ID0gU3R5bGVkLmRpdmBcbiAgICAuYW50LWZvcm0taXRlbS1sYWJlbCA+IGxhYmVsIHtcbiAgICAgIGNvbG9yOiAjOWU5ZTllO1xuICAgIH1cbmA7XG5cbmV4cG9ydCB0eXBlIE5Gb3JtSXRlbVByb3BzID0ge1xuICBsYWJlbD86IFJlYWN0LlJlYWN0Tm9kZTtcbiAgbGF5b3V0Q29sPzogb2JqZWN0O1xuICByZXF1aXJlZD86IGJvb2xlYW47XG4gIGZvcm06IGFueTtcbiAgc2hvd1BsYWNlaG9sZGVyPzogYm9vbGVhblxuICBvbkNoYW5nZT86ICh2YWx1ZTogYW55KSA9PiB2b2lkO1xuICBwcm9wTmFtZTogc3RyaW5nO1xuICBydWxlcz86IGFueTtcbiAgdHJhbnNmb3JtPzogKCkgPT4gYW55O1xuICByZW5kZXJCb3g/OiBhbnk7XG59ICYgRm9ybUNvbXBvbmVudFByb3BzICYgTW9kZWxXaWRnZXRQcm9wc1xuXG5jbGFzcyBORm9ybUl0ZW08UCBleHRlbmRzIE5Gb3JtSXRlbVByb3BzLCBTLCBDIGV4dGVuZHMgTkZvcm1JdGVtUGFnZUNvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogTkZvcm1JdGVtUHJvcHMsIGNvbnRleHQ/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHsgTGFiZWxJbnB1dCB9IGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0VmFsdWUoZXZlbnQ6IGFueSk6IGFueSB7XG4gICAgLy9pZiB0aGUgZW52IGlzIHVuZGVmaW5lZCBvciBudWxsLCByZXR1cm4gZGlyZWN0bHlcbiAgICBpZiAoZXZlbnQgPT09IHVuZGVmaW5lZCB8fCBldmVudCA9PT0gbnVsbCkgcmV0dXJuO1xuXG4gICAgLy9mb3IgaW5wdXRcbiAgICBsZXQgdmFsdWUgPSBldmVudDtcblxuICAgIGlmIChldmVudC50YXJnZXQpIHtcbiAgICAgIGNvbnN0IHsgdGFyZ2V0IH0gPSBldmVudDtcbiAgICAgIHZhbHVlID0gKHRhcmdldC50eXBlID09PSBcImNoZWNrYm94XCIgPyB0YXJnZXQuY2hlY2tlZCA6IHRhcmdldC52YWx1ZSk7XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gXCJib29sZWFuXCIpIHtcbiAgICAgIHZhbHVlID0gdmFsdWUgPyBcIllcIiA6IFwiTlwiO1xuICAgIH1cbiAgICByZXR1cm4gdmFsdWU7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0UnVsZXMoKTogYW55IHtcbiAgICByZXR1cm4gKHRoaXMucHJvcHMucnVsZXMgfHwgW10pIGFzIGFueVtdO1xuICB9XG5cbiAgcmVuZGVyRm9ybVZhbHVlKCkge1xuICAgIGNvbnN0IHsgZm9ybSwgbGFiZWwsIHByb3BOYW1lLCBvbkNoYW5nZSwgcmVxdWlyZWQsIHRyYW5zZm9ybSwgc2hvd1BsYWNlaG9sZGVyID0gZmFsc2UgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBnZXRGaWVsZERlY29yYXRvciB9ID0gZm9ybTtcbiAgICByZXR1cm4gZ2V0RmllbGREZWNvcmF0b3IocHJvcE5hbWUudG9TdHJpbmcoKSwge1xuICAgICAgaW5pdGlhbFZhbHVlOiBzaG93UGxhY2Vob2xkZXIgP1xuICAgICAgICAoKFV0aWxzLmlzTnVsbCh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHByb3BOYW1lKSkgfHwgVXRpbHMuaXNVbmRlZmluZWQodGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChwcm9wTmFtZSkpKSA/IHVuZGVmaW5lZCA6IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwocHJvcE5hbWUpKVxuICAgICAgICA6XG4gICAgICAgICgoVXRpbHMuaXNOdWxsKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwocHJvcE5hbWUpKSB8fCBVdGlscy5pc1VuZGVmaW5lZCh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHByb3BOYW1lKSkpID8gXCJcIiA6IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwocHJvcE5hbWUpKSxcbiAgICAgIGdldFZhbHVlRnJvbUV2ZW50OiAoZXZlbnQ6IGFueSkgPT4ge1xuICAgICAgICBpZiAodHJhbnNmb3JtKSByZXR1cm4gdHJhbnNmb3JtKCk7XG4gICAgICAgIHJldHVybiB0aGlzLmdldFZhbHVlKGV2ZW50KTtcbiAgICAgIH0sXG4gICAgICBydWxlczogW3tcbiAgICAgICAgcmVxdWlyZWQ6IHJlcXVpcmVkLFxuICAgICAgICBtZXNzYWdlOiBMYW5ndWFnZS5lbihgJHtsYWJlbCA9PT0gXCIgXCIgfHwgIWxhYmVsID8gcHJvcE5hbWUgOiBsYWJlbH0gaXMgcmVxdWlyZWRgKVxuICAgICAgICAgIC50aGFpKGAke2xhYmVsID09PSBcIiBcIiB8fCAhbGFiZWwgPyBwcm9wTmFtZSA6IGxhYmVsfSBpcyByZXF1aXJlZGApLmdldE1lc3NhZ2UoKSxcbiAgICAgIH1dLmNvbmNhdCh0aGlzLmdldFJ1bGVzKCkpLFxuICAgICAgb25DaGFuZ2U6IChldmVudDogYW55KSA9PiB7XG4gICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5nZXRWYWx1ZShldmVudCk7XG4gICAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKHZhbHVlKTtcbiAgICAgICAgaWYgKG9uQ2hhbmdlKSB7XG4gICAgICAgICAgb25DaGFuZ2UodmFsdWUpO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgdmFsaWRhdGVGaXJzdDogdHJ1ZSxcbiAgICB9IGFzIGFueSkodGhpcy5wcm9wcy5jaGlsZHJlbik7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIGNvbnN0IHsgbGFiZWwsIGxheW91dENvbCB9ID0gdGhpcy5wcm9wcztcblxuICAgIHJldHVybiAoXG4gICAgICA8Rm9ybS5JdGVtIGxhYmVsPXtsYWJlbH0gY29sb249e2ZhbHNlfSB7Li4uKGxheW91dENvbCB8fCBDb25zdHMuRk9STV9JVEVNX0xBWU9VVCl9PlxuICAgICAgICB7dGhpcy5wcm9wcy5yZW5kZXJCb3ggPyB0aGlzLnByb3BzLnJlbmRlckJveCh0aGlzLnJlbmRlckZvcm1WYWx1ZSgpKSA6IHRoaXMucmVuZGVyRm9ybVZhbHVlKCl9XG4gICAgICA8L0Zvcm0uSXRlbT5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCB7IE5Gb3JtSXRlbSB9O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQVNBO0FBQ0E7QUFrQkE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJCQTtBQXVCQTs7O0FBRUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7Ozs7QUFwRUE7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/component/NFormItem.tsx
