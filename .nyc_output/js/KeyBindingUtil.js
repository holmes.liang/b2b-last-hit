/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule KeyBindingUtil
 * @format
 * 
 */


var UserAgent = __webpack_require__(/*! fbjs/lib/UserAgent */ "./node_modules/fbjs/lib/UserAgent.js");

var isOSX = UserAgent.isPlatform('Mac OS X');
var KeyBindingUtil = {
  /**
   * Check whether the ctrlKey modifier is *not* being used in conjunction with
   * the altKey modifier. If they are combined, the result is an `altGraph`
   * key modifier, which should not be handled by this set of key bindings.
   */
  isCtrlKeyCommand: function isCtrlKeyCommand(e) {
    return !!e.ctrlKey && !e.altKey;
  },
  isOptionKeyCommand: function isOptionKeyCommand(e) {
    return isOSX && e.altKey;
  },
  hasCommandModifier: function hasCommandModifier(e) {
    return isOSX ? !!e.metaKey && !e.altKey : KeyBindingUtil.isCtrlKeyCommand(e);
  }
};
module.exports = KeyBindingUtil;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0tleUJpbmRpbmdVdGlsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0tleUJpbmRpbmdVdGlsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgS2V5QmluZGluZ1V0aWxcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIFVzZXJBZ2VudCA9IHJlcXVpcmUoJ2ZianMvbGliL1VzZXJBZ2VudCcpO1xuXG52YXIgaXNPU1ggPSBVc2VyQWdlbnQuaXNQbGF0Zm9ybSgnTWFjIE9TIFgnKTtcblxudmFyIEtleUJpbmRpbmdVdGlsID0ge1xuICAvKipcbiAgICogQ2hlY2sgd2hldGhlciB0aGUgY3RybEtleSBtb2RpZmllciBpcyAqbm90KiBiZWluZyB1c2VkIGluIGNvbmp1bmN0aW9uIHdpdGhcbiAgICogdGhlIGFsdEtleSBtb2RpZmllci4gSWYgdGhleSBhcmUgY29tYmluZWQsIHRoZSByZXN1bHQgaXMgYW4gYGFsdEdyYXBoYFxuICAgKiBrZXkgbW9kaWZpZXIsIHdoaWNoIHNob3VsZCBub3QgYmUgaGFuZGxlZCBieSB0aGlzIHNldCBvZiBrZXkgYmluZGluZ3MuXG4gICAqL1xuICBpc0N0cmxLZXlDb21tYW5kOiBmdW5jdGlvbiBpc0N0cmxLZXlDb21tYW5kKGUpIHtcbiAgICByZXR1cm4gISFlLmN0cmxLZXkgJiYgIWUuYWx0S2V5O1xuICB9LFxuXG4gIGlzT3B0aW9uS2V5Q29tbWFuZDogZnVuY3Rpb24gaXNPcHRpb25LZXlDb21tYW5kKGUpIHtcbiAgICByZXR1cm4gaXNPU1ggJiYgZS5hbHRLZXk7XG4gIH0sXG5cbiAgaGFzQ29tbWFuZE1vZGlmaWVyOiBmdW5jdGlvbiBoYXNDb21tYW5kTW9kaWZpZXIoZSkge1xuICAgIHJldHVybiBpc09TWCA/ICEhZS5tZXRhS2V5ICYmICFlLmFsdEtleSA6IEtleUJpbmRpbmdVdGlsLmlzQ3RybEtleUNvbW1hbmQoZSk7XG4gIH1cbn07XG5cbm1vZHVsZS5leHBvcnRzID0gS2V5QmluZGluZ1V0aWw7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBaEJBO0FBbUJBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/KeyBindingUtil.js
