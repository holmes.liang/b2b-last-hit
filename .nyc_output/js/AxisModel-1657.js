/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var ComponentModel = __webpack_require__(/*! ../../model/Component */ "./node_modules/echarts/lib/model/Component.js");

var axisModelCreator = __webpack_require__(/*! ../axisModelCreator */ "./node_modules/echarts/lib/coord/axisModelCreator.js");

var axisModelCommonMixin = __webpack_require__(/*! ../axisModelCommonMixin */ "./node_modules/echarts/lib/coord/axisModelCommonMixin.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var AxisModel = ComponentModel.extend({
  type: 'cartesian2dAxis',

  /**
   * @type {module:echarts/coord/cartesian/Axis2D}
   */
  axis: null,

  /**
   * @override
   */
  init: function init() {
    AxisModel.superApply(this, 'init', arguments);
    this.resetRange();
  },

  /**
   * @override
   */
  mergeOption: function mergeOption() {
    AxisModel.superApply(this, 'mergeOption', arguments);
    this.resetRange();
  },

  /**
   * @override
   */
  restoreData: function restoreData() {
    AxisModel.superApply(this, 'restoreData', arguments);
    this.resetRange();
  },

  /**
   * @override
   * @return {module:echarts/model/Component}
   */
  getCoordSysModel: function getCoordSysModel() {
    return this.ecModel.queryComponents({
      mainType: 'grid',
      index: this.option.gridIndex,
      id: this.option.gridId
    })[0];
  }
});

function getAxisType(axisDim, option) {
  // Default axis with data is category axis
  return option.type || (option.data ? 'category' : 'value');
}

zrUtil.merge(AxisModel.prototype, axisModelCommonMixin);
var extraOption = {
  // gridIndex: 0,
  // gridId: '',
  // Offset is for multiple axis on the same position
  offset: 0
};
axisModelCreator('x', AxisModel, getAxisType, extraOption);
axisModelCreator('y', AxisModel, getAxisType, extraOption);
var _default = AxisModel;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvY2FydGVzaWFuL0F4aXNNb2RlbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2Nvb3JkL2NhcnRlc2lhbi9BeGlzTW9kZWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgQ29tcG9uZW50TW9kZWwgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvQ29tcG9uZW50XCIpO1xuXG52YXIgYXhpc01vZGVsQ3JlYXRvciA9IHJlcXVpcmUoXCIuLi9heGlzTW9kZWxDcmVhdG9yXCIpO1xuXG52YXIgYXhpc01vZGVsQ29tbW9uTWl4aW4gPSByZXF1aXJlKFwiLi4vYXhpc01vZGVsQ29tbW9uTWl4aW5cIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBBeGlzTW9kZWwgPSBDb21wb25lbnRNb2RlbC5leHRlbmQoe1xuICB0eXBlOiAnY2FydGVzaWFuMmRBeGlzJyxcblxuICAvKipcbiAgICogQHR5cGUge21vZHVsZTplY2hhcnRzL2Nvb3JkL2NhcnRlc2lhbi9BeGlzMkR9XG4gICAqL1xuICBheGlzOiBudWxsLFxuXG4gIC8qKlxuICAgKiBAb3ZlcnJpZGVcbiAgICovXG4gIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICBBeGlzTW9kZWwuc3VwZXJBcHBseSh0aGlzLCAnaW5pdCcsIGFyZ3VtZW50cyk7XG4gICAgdGhpcy5yZXNldFJhbmdlKCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBvdmVycmlkZVxuICAgKi9cbiAgbWVyZ2VPcHRpb246IGZ1bmN0aW9uICgpIHtcbiAgICBBeGlzTW9kZWwuc3VwZXJBcHBseSh0aGlzLCAnbWVyZ2VPcHRpb24nLCBhcmd1bWVudHMpO1xuICAgIHRoaXMucmVzZXRSYW5nZSgpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAb3ZlcnJpZGVcbiAgICovXG4gIHJlc3RvcmVEYXRhOiBmdW5jdGlvbiAoKSB7XG4gICAgQXhpc01vZGVsLnN1cGVyQXBwbHkodGhpcywgJ3Jlc3RvcmVEYXRhJywgYXJndW1lbnRzKTtcbiAgICB0aGlzLnJlc2V0UmFuZ2UoKTtcbiAgfSxcblxuICAvKipcbiAgICogQG92ZXJyaWRlXG4gICAqIEByZXR1cm4ge21vZHVsZTplY2hhcnRzL21vZGVsL0NvbXBvbmVudH1cbiAgICovXG4gIGdldENvb3JkU3lzTW9kZWw6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5lY01vZGVsLnF1ZXJ5Q29tcG9uZW50cyh7XG4gICAgICBtYWluVHlwZTogJ2dyaWQnLFxuICAgICAgaW5kZXg6IHRoaXMub3B0aW9uLmdyaWRJbmRleCxcbiAgICAgIGlkOiB0aGlzLm9wdGlvbi5ncmlkSWRcbiAgICB9KVswXTtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIGdldEF4aXNUeXBlKGF4aXNEaW0sIG9wdGlvbikge1xuICAvLyBEZWZhdWx0IGF4aXMgd2l0aCBkYXRhIGlzIGNhdGVnb3J5IGF4aXNcbiAgcmV0dXJuIG9wdGlvbi50eXBlIHx8IChvcHRpb24uZGF0YSA/ICdjYXRlZ29yeScgOiAndmFsdWUnKTtcbn1cblxuenJVdGlsLm1lcmdlKEF4aXNNb2RlbC5wcm90b3R5cGUsIGF4aXNNb2RlbENvbW1vbk1peGluKTtcbnZhciBleHRyYU9wdGlvbiA9IHtcbiAgLy8gZ3JpZEluZGV4OiAwLFxuICAvLyBncmlkSWQ6ICcnLFxuICAvLyBPZmZzZXQgaXMgZm9yIG11bHRpcGxlIGF4aXMgb24gdGhlIHNhbWUgcG9zaXRpb25cbiAgb2Zmc2V0OiAwXG59O1xuYXhpc01vZGVsQ3JlYXRvcigneCcsIEF4aXNNb2RlbCwgZ2V0QXhpc1R5cGUsIGV4dHJhT3B0aW9uKTtcbmF4aXNNb2RlbENyZWF0b3IoJ3knLCBBeGlzTW9kZWwsIGdldEF4aXNUeXBlLCBleHRyYU9wdGlvbik7XG52YXIgX2RlZmF1bHQgPSBBeGlzTW9kZWw7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUExQ0E7QUFDQTtBQTRDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/cartesian/AxisModel.js
