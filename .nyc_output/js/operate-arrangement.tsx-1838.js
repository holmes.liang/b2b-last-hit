__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formItemLayout", function() { return formItemLayout; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_claims_handing_details_SAIC_components_settlement_components_style_table__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk/claims/handing/details/SAIC/components/settlement/components/style/table */ "./src/app/desk/claims/handing/details/SAIC/components/settlement/components/style/table.tsx");
/* harmony import */ var antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! antd/lib/table/Column */ "./node_modules/antd/lib/table/Column.js");
/* harmony import */ var antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _desk_styles_global__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @desk-styles/global */ "./src/app/desk/styles/global.tsx");
/* harmony import */ var _desk_component_table_action_bars__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @desk-component/table/action-bars */ "./src/app/desk/component/table/action-bars.tsx");
/* harmony import */ var _desk_component_table_actions_under_table__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @desk-component/table/actions-under-table */ "./src/app/desk/component/table/actions-under-table.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/components/panyment-arramgement/component/operate-arrangement.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        .total-premium {\n          margin-bottom: 20px\n        }  \n        .ant-table-thead {\n         text-transform: uppercase;\n        }\n        .total-content {\n          display: flex;\n          .total-split-content {\n            margin-left: 20px;\n            a {\n              margin-right: 10px;\n            }\n          }\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}













var formItemLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};
var itemLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 24
    }
  }
};

var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_8__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();

var OperateArrangement =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(OperateArrangement, _ModelWidget);

  function OperateArrangement(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, OperateArrangement);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(OperateArrangement).call(this, props, context));

    _this.toggleEdit = function (index) {
      _this.setState({
        editIndex: index
      }, function () {});
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(OperateArrangement, [{
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(OperateArrangement.prototype), "initState", this).call(this), {
        editIndex: 0
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_12__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {} // render

  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          onCancel = _this$props.onCancel,
          onOK = _this$props.onOK;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Modal"], {
        width: "60%",
        maskClosable: false,
        visible: true,
        centered: true,
        title: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Non-regular Instalments").thai("Non-regular Instalments").getMessage(),
        onCancel: onCancel,
        onOk: onOK,
        footer: [_common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Button"], {
          key: "cancel",
          onClick: onCancel,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 112
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Cancel").thai("ยกเลิก").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Button"], {
          key: "save",
          type: "primary",
          onClick: this.handleSave.bind(this),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 115
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Save").thai("Save").getMessage())],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 103
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(C.BoxContent, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(_desk_claims_handing_details_SAIC_components_settlement_components_style_table__WEBPACK_IMPORTED_MODULE_15__["TableStyleForDefaultSize"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 125
        },
        __self: this
      }, this.renderTotal(), this.renderTable(), this.renderFormAction())));
    }
  }, {
    key: "renderTotal",
    value: function renderTotal() {
      var _this2 = this;

      var oldModel = this.props.oldModel;
      var premCurrency = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(oldModel, "policy.premCurrencyCode") || "SGD";
      var currencySymbol = _common__WEBPACK_IMPORTED_MODULE_10__["Consts"].CURRENCY_SYMBOL_TABLE[premCurrency];

      var premium = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(oldModel, this.getProp("policy.policyPremium.lumpsum.ar"));

      return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("div", {
        className: "total-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("div", {
        className: "total-premium",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 142
        },
        __self: this
      }, "Total premium ", _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("a", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 143
        },
        __self: this
      }, currencySymbol, this.formmatCurrency(premium)), ", to be arranged premium ", _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("a", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }, currencySymbol, this.calcArrangedPremium())), _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("div", {
        className: "total-split-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 147
        },
        __self: this
      }, "Split by: ", _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("a", {
        onClick: function onClick() {
          _this2.handleSplit(_common__WEBPACK_IMPORTED_MODULE_10__["Consts"].PAYMENT_CUSTOMIZATION.MONTHLY);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 148
        },
        __self: this
      }, "Monthly"), _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("a", {
        onClick: function onClick() {
          _this2.handleSplit(_common__WEBPACK_IMPORTED_MODULE_10__["Consts"].PAYMENT_CUSTOMIZATION.QUARTERLY);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 151
        },
        __self: this
      }, "Quarterly"), _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("a", {
        onClick: function onClick() {
          _this2.handleSplit(_common__WEBPACK_IMPORTED_MODULE_10__["Consts"].PAYMENT_CUSTOMIZATION.HALF_YEARLY);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 154
        },
        __self: this
      }, "Half-yearly")));
    }
  }, {
    key: "renderTable",
    value: function renderTable() {
      var _this3 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Table"], {
        dataSource: this.tableData,
        pagination: false,
        bordered: true,
        onRow: function onRow(record, index) {
          return {
            onClick: function onClick(event) {
              _this3.toggleEdit(index);
            } // 点击行

          };
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 164
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_16___default.a, {
        title: "Installment Seq.",
        key: "seqNo",
        width: 200,
        className: "amount-right",
        render: function render(text, record, dataIndex) {
          return dataIndex + 1;
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 175
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_16___default.a, {
        title: "Amount",
        key: "amount",
        dataIndex: "amount",
        className: "amount-right",
        render: function render(text, record, dataIndex) {
          return _this3.renderAmount(text, dataIndex);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 185
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_16___default.a, {
        title: "Payment Date",
        key: "paymentDate",
        dataIndex: "paymentDate",
        render: function render(text, record, dataIndex) {
          return _this3.renderDate(text, dataIndex);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 194
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_16___default.a, {
        title: "ACTION",
        key: "action",
        className: "amount-center",
        render: function render(text, record, dataIndex) {
          var actions = [{
            actionName: "Delete",
            handel: function handel() {
              // todo 验证
              var _list = _this3.tableData;
              if (lodash__WEBPACK_IMPORTED_MODULE_9___default.a.isEmpty(_list)) return;

              var _newList = _list.filter(function (item, index) {
                return index != dataIndex;
              });

              _this3.setValueToModel([], _this3.getProp("details"));

              _newList.forEach(function (item, index) {
                _this3.setValueToModel(item, _this3.getProp("details.".concat(index)));
              });
            }
          }];
          return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(_desk_component_table_action_bars__WEBPACK_IMPORTED_MODULE_18__["TableActionBars"], {
            actions: actions,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 223
            },
            __self: this
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 203
        },
        __self: this
      }));
    }
  }, {
    key: "renderFormAction",
    value: function renderFormAction() {
      var _this4 = this;

      var max = this.props.max;
      var length = (this.getValueFromModel(this.getProp("details")) || []).length;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(_desk_component_table_actions_under_table__WEBPACK_IMPORTED_MODULE_19__["TableUnderActions"], {
        actions: [{
          actionName: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Add").thai("Add").getMessage(),
          isShow: max > length,
          handel: function handel() {
            _this4.handleAdd();
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 234
        },
        __self: this
      });
    }
  }, {
    key: "renderAmount",
    value: function renderAmount(text, dataIndex) {
      var _this$props2 = this.props,
          form = _this$props2.form,
          model = _this$props2.model,
          oldModel = _this$props2.oldModel;
      var premCurrency = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(oldModel, "policy.premCurrencyCode") || "SGD";
      var currencySymbol = _common__WEBPACK_IMPORTED_MODULE_10__["Consts"].CURRENCY_SYMBOL_TABLE[premCurrency];

      if (this.state.editIndex != dataIndex) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("div", {
          style: {
            paddingRight: 4
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 251
          },
          __self: this
        }, currencySymbol, " ", this.formmatCurrency(text));
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_17__["StyleWithoutMarginBottom"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 256
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_14__["NPrice"], {
        style: {
          width: "100%"
        },
        precision: 2,
        form: form,
        label: "",
        model: model,
        addonBefore: currencySymbol,
        size: "default",
        required: true,
        propName: this.getProp("details.".concat(dataIndex, ".amount")),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 257
        },
        __self: this
      }));
    }
  }, {
    key: "renderDate",
    value: function renderDate(text, dataIndex) {
      var newArr = this.tableData.slice(0, dataIndex);

      var findIndex = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.findLastIndex(newArr, function (o) {
        return !lodash__WEBPACK_IMPORTED_MODULE_9___default.a.isEmpty(lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(o, "paymentDate"));
      });

      var effDate = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(this.props.oldModel, this.getProp("policy.effDate"));

      var expDate = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(this.props.oldModel, this.getProp("policy.expDate"));

      if (this.state.editIndex != dataIndex) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("div", {
          style: {
            paddingRight: 4
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 280
          },
          __self: this
        }, text ? _common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].toDate(text)) : "");
      }

      var _this$props3 = this.props,
          form = _this$props3.form,
          model = _this$props3.model,
          oldModel = _this$props3.oldModel;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_17__["StyleWithoutMarginBottom"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 286
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("div", {
        onClick: function onClick(e) {
          // 阻止冒泡导致的因为行点击事件导致日期选择上一月下一月无法点击的问题
          e.stopPropagation();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 287
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_14__["NDate"], {
        style: {
          width: "100%"
        },
        form: form,
        label: "",
        model: model,
        format: _common__WEBPACK_IMPORTED_MODULE_10__["Consts"].DATE_FORMAT.DATE_FORMAT,
        size: "default",
        required: true,
        disabledDate: function disabledDate(current) {
          if (findIndex === -1) {
            return current.isBefore(_common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].toDate(effDate), "day") || current.isAfter(_common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].toDate(expDate), "day");
          } else {
            var endDate = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(newArr, "".concat(findIndex, ".paymentDate"));

            return current.isSameOrBefore(_common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].toDate(endDate), "day") || current.isAfter(_common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].toDate(expDate), "day");
          }
        },
        propName: this.getProp("details.".concat(dataIndex, ".paymentDate")),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 291
        },
        __self: this
      })));
    } // actions

  }, {
    key: "handleAdd",
    value: function handleAdd() {
      var _tableData = [].concat(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(this.tableData), [{
        amount: 0
      }]);

      this.toggleEdit(_tableData.length - 1);
      this.setValueToModel(_tableData, this.getProp("details"));
    }
  }, {
    key: "handleSave",
    value: function handleSave() {
      var _this5 = this;

      this.props.form.validateFields(function (err, fieldsValue) {
        if (err) {
          return;
        }

        var newDetails = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.cloneDeep(lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this5.props.model, "details"));

        newDetails.forEach(function (item, index) {
          item.seqNo = index + 1;
        });

        _this5.props.onSave(newDetails);

        _this5.props.onCancel();
      });
    }
  }, {
    key: "handleSplit",
    value: function handleSplit(plan) {
      var _this6 = this;

      var policy = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.cloneDeep(lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(this.props.oldModel, "policy", {}));

      var policyId = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(policy, "policyId", "");

      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_10__["Apis"].PAYMENT_PLAN_CUSTOMIZATION.replace(":policyId", policyId), {
        policy: policy,
        paymentPlan: plan
      }, {
        loading: true
      }).then(function (res) {
        var data = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(res, "body.respData") || [];

        _this6.toggleEdit(-1);

        _this6.setValueToModel(data, _this6.getProp("details"));
      });
    } // methods

  }, {
    key: "getProp",
    value: function getProp(propName) {
      return propName;
    }
  }, {
    key: "formmatCurrency",
    value: function formmatCurrency(money) {
      var _money = parseFloat(money || 0).toFixed(2);

      return "".concat(_common__WEBPACK_IMPORTED_MODULE_10__["Utils"].toThousands(_money || 0));
    }
  }, {
    key: "calcArrangedPremium",
    value: function calcArrangedPremium() {
      var oldModel = this.props.oldModel;
      var premium = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(oldModel, this.getProp("policy.policyPremium.lumpsum.ar")) || 0;
      this.tableData.forEach(function (item) {
        if (_common__WEBPACK_IMPORTED_MODULE_10__["Rules"].isNum(item.amount)) {
          premium = premium - item.amount;
        }
      });
      return premium.toFixed(2);
    } // getters

  }, {
    key: "tableData",
    get: function get() {
      return this.getValueFromModel(this.getProp("details")) || [];
    }
  }]);

  return OperateArrangement;
}(_component__WEBPACK_IMPORTED_MODULE_11__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (antd__WEBPACK_IMPORTED_MODULE_13__["Form"].create()(OperateArrangement));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcG9uZW50cy9wYW55bWVudC1hcnJhbWdlbWVudC9jb21wb25lbnQvb3BlcmF0ZS1hcnJhbmdlbWVudC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9jb21wb25lbnRzL3BhbnltZW50LWFycmFtZ2VtZW50L2NvbXBvbmVudC9vcGVyYXRlLWFycmFuZ2VtZW50LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXNcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IEFqYXgsIEFwaXMsIENvbnN0cywgRGF0ZVV0aWxzLCBMYW5ndWFnZSwgUnVsZXMsIFN0b3JhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCB7IEFqYXhSZXNwb25zZSwgTW9kZWxXaWRnZXRQcm9wcyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IEZvcm1Db21wb25lbnRQcm9wcyB9IGZyb20gXCJhbnRkL2xpYi9mb3JtXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCwgTkZvcm1JdGVtIH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IEJ1dHRvbiwgQ29sLCBGb3JtLCBJbnB1dCwgTW9kYWwsIFJvdywgU3BpbiwgVGFibGUsIFVwbG9hZCB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBORGF0ZSwgTk51bWJlciwgTlByaWNlLCBOU2VsZWN0LCBOVGV4dCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgVGFibGVTdHlsZUZvckRlZmF1bHRTaXplIH0gZnJvbSBcIkBkZXNrL2NsYWltcy9oYW5kaW5nL2RldGFpbHMvU0FJQy9jb21wb25lbnRzL3NldHRsZW1lbnQvY29tcG9uZW50cy9zdHlsZS90YWJsZVwiO1xuaW1wb3J0IENvbHVtbiBmcm9tIFwiYW50ZC9saWIvdGFibGUvQ29sdW1uXCI7XG5pbXBvcnQgeyBTdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20gfSBmcm9tIFwiQGRlc2stc3R5bGVzL2dsb2JhbFwiO1xuaW1wb3J0IHsgSUFjdGlvbiwgVGFibGVBY3Rpb25CYXJzIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC90YWJsZS9hY3Rpb24tYmFyc1wiO1xuaW1wb3J0IHsgVGFibGVVbmRlckFjdGlvbnMgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3RhYmxlL2FjdGlvbnMtdW5kZXItdGFibGVcIjtcbmltcG9ydCBtb21lbnQgZnJvbSBcIm1vbWVudFwiO1xuXG5leHBvcnQgY29uc3QgZm9ybUl0ZW1MYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogOCB9LFxuICAgIHNtOiB7IHNwYW46IDYgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDE2IH0sXG4gICAgc206IHsgc3BhbjogMTMgfSxcbiAgfSxcbn07XG5cbmNvbnN0IGl0ZW1MYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogOCB9LFxuICAgIHNtOiB7IHNwYW46IDYgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDE2IH0sXG4gICAgc206IHsgc3BhbjogMjQgfSxcbiAgfSxcbn07XG5jb25zdCB7IENPTE9SX0EgfSA9IFRoZW1lLmdldFRoZW1lKCk7XG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG50eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xudHlwZSBJQ29tcG9uZW50cyA9IHtcbiAgQm94Q29udGVudDogU3R5bGVkRElWO1xufTtcblxudHlwZSBhcnJhbmdlbWVudFN0YXRlID0ge1xuICBlZGl0SW5kZXg6IG51bWJlcjtcbn07XG5cbmV4cG9ydCB0eXBlIGFycmFuZ2VtZW50UHJvcHMgPSB7XG4gIG9uQ2FuY2VsOiBhbnk7XG4gIG9uT0s6IGFueTtcbiAgbW9kZWw6IGFueTtcbiAgb2xkTW9kZWw6IGFueTtcbiAgbWluOiBudW1iZXJcbiAgbWF4OiBudW1iZXI7XG4gIG9uU2F2ZTogYW55O1xufSAmIE1vZGVsV2lkZ2V0UHJvcHMgJiBGb3JtQ29tcG9uZW50UHJvcHM7XG5cbmNsYXNzIE9wZXJhdGVBcnJhbmdlbWVudDxQIGV4dGVuZHMgYXJyYW5nZW1lbnRQcm9wcywgUyBleHRlbmRzIGFycmFuZ2VtZW50U3RhdGUsIEMgZXh0ZW5kcyBJQ29tcG9uZW50cz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIGNvbnN0cnVjdG9yKHByb3BzOiBhcnJhbmdlbWVudFByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgZWRpdEluZGV4OiAwLFxuICAgIH0pIGFzIFM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIEJveENvbnRlbnQ6IFN0eWxlZC5kaXZgXG4gICAgICAgIC50b3RhbC1wcmVtaXVtIHtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4XG4gICAgICAgIH0gIFxuICAgICAgICAuYW50LXRhYmxlLXRoZWFkIHtcbiAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgIH1cbiAgICAgICAgLnRvdGFsLWNvbnRlbnQge1xuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgLnRvdGFsLXNwbGl0LWNvbnRlbnQge1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gICAgICAgICAgICBhIHtcbiAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcblxuICB9XG5cbiAgLy8gcmVuZGVyXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgeyBtb2RlbCwgZm9ybSwgb25DYW5jZWwsIG9uT0sgfSA9IHRoaXMucHJvcHM7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPE1vZGFsXG4gICAgICAgIHdpZHRoPXtcIjYwJVwifVxuICAgICAgICBtYXNrQ2xvc2FibGU9e2ZhbHNlfVxuICAgICAgICB2aXNpYmxlPXt0cnVlfVxuICAgICAgICBjZW50ZXJlZD17dHJ1ZX1cbiAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiTm9uLXJlZ3VsYXIgSW5zdGFsbWVudHNcIikudGhhaShcIk5vbi1yZWd1bGFyIEluc3RhbG1lbnRzXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgb25DYW5jZWw9e29uQ2FuY2VsfVxuICAgICAgICBvbk9rPXtvbk9LfVxuICAgICAgICBmb290ZXI9e1tcbiAgICAgICAgICA8QnV0dG9uIGtleT1cImNhbmNlbFwiIG9uQ2xpY2s9e29uQ2FuY2VsfT5cbiAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIkNhbmNlbFwiKS50aGFpKFwi4Lii4LiB4LmA4Lil4Li04LiBXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA8L0J1dHRvbj4sXG4gICAgICAgICAgPEJ1dHRvblxuICAgICAgICAgICAga2V5PVwic2F2ZVwiXG4gICAgICAgICAgICB0eXBlPVwicHJpbWFyeVwiXG4gICAgICAgICAgICBvbkNsaWNrPXt0aGlzLmhhbmRsZVNhdmUuYmluZCh0aGlzKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJTYXZlXCIpLnRoYWkoXCJTYXZlXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA8L0J1dHRvbj4sXG4gICAgICAgIF19XG4gICAgICA+XG4gICAgICAgIDxDLkJveENvbnRlbnQ+XG4gICAgICAgICAgPFRhYmxlU3R5bGVGb3JEZWZhdWx0U2l6ZT5cbiAgICAgICAgICAgIHt0aGlzLnJlbmRlclRvdGFsKCl9XG4gICAgICAgICAgICB7dGhpcy5yZW5kZXJUYWJsZSgpfVxuICAgICAgICAgICAge3RoaXMucmVuZGVyRm9ybUFjdGlvbigpfVxuICAgICAgICAgIDwvVGFibGVTdHlsZUZvckRlZmF1bHRTaXplPlxuICAgICAgICA8L0MuQm94Q29udGVudD5cbiAgICAgIDwvTW9kYWw+XG4gICAgKTtcbiAgfVxuXG4gIHJlbmRlclRvdGFsKCkge1xuICAgIGNvbnN0IHsgb2xkTW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgcHJlbUN1cnJlbmN5ID0gXy5nZXQob2xkTW9kZWwsIFwicG9saWN5LnByZW1DdXJyZW5jeUNvZGVcIikgfHwgXCJTR0RcIjtcbiAgICBjb25zdCBjdXJyZW5jeVN5bWJvbCA9IENvbnN0cy5DVVJSRU5DWV9TWU1CT0xfVEFCTEVbcHJlbUN1cnJlbmN5XTtcbiAgICBjb25zdCBwcmVtaXVtID0gXy5nZXQob2xkTW9kZWwsIHRoaXMuZ2V0UHJvcChcInBvbGljeS5wb2xpY3lQcmVtaXVtLmx1bXBzdW0uYXJcIikpO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17XCJ0b3RhbC1jb250ZW50XCJ9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17XCJ0b3RhbC1wcmVtaXVtXCJ9PlxuICAgICAgICAgIFRvdGFsIHByZW1pdW0gPGE+e2N1cnJlbmN5U3ltYm9sfXt0aGlzLmZvcm1tYXRDdXJyZW5jeShwcmVtaXVtKX08L2E+XG4gICAgICAgICAgLCB0byBiZSBhcnJhbmdlZCBwcmVtaXVtIDxhPntjdXJyZW5jeVN5bWJvbH17dGhpcy5jYWxjQXJyYW5nZWRQcmVtaXVtKCl9PC9hPlxuXG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17XCJ0b3RhbC1zcGxpdC1jb250ZW50XCJ9PlxuICAgICAgICAgIFNwbGl0IGJ5OiA8YSBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgdGhpcy5oYW5kbGVTcGxpdChDb25zdHMuUEFZTUVOVF9DVVNUT01JWkFUSU9OLk1PTlRITFkpO1xuICAgICAgICB9fT5Nb250aGx5PC9hPlxuICAgICAgICAgIDxhIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaGFuZGxlU3BsaXQoQ29uc3RzLlBBWU1FTlRfQ1VTVE9NSVpBVElPTi5RVUFSVEVSTFkpO1xuICAgICAgICAgIH19PlF1YXJ0ZXJseTwvYT5cbiAgICAgICAgICA8YSBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmhhbmRsZVNwbGl0KENvbnN0cy5QQVlNRU5UX0NVU1RPTUlaQVRJT04uSEFMRl9ZRUFSTFkpO1xuICAgICAgICAgIH19PkhhbGYteWVhcmx5PC9hPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICByZW5kZXJUYWJsZSgpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPFRhYmxlIGRhdGFTb3VyY2U9e3RoaXMudGFibGVEYXRhfVxuICAgICAgICAgICAgIHBhZ2luYXRpb249e2ZhbHNlfVxuICAgICAgICAgICAgIGJvcmRlcmVkPXt0cnVlfVxuICAgICAgICAgICAgIG9uUm93PXsocmVjb3JkLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgb25DbGljazogZXZlbnQgPT4ge1xuICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlRWRpdChpbmRleCk7XG4gICAgICAgICAgICAgICAgIH0sIC8vIOeCueWHu+ihjFxuICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICB9fVxuICAgICAgPlxuICAgICAgICA8Q29sdW1uXG4gICAgICAgICAgdGl0bGU9XCJJbnN0YWxsbWVudCBTZXEuXCJcbiAgICAgICAgICBrZXk9XCJzZXFOb1wiXG4gICAgICAgICAgd2lkdGg9ezIwMH1cbiAgICAgICAgICBjbGFzc05hbWU9XCJhbW91bnQtcmlnaHRcIlxuICAgICAgICAgIHJlbmRlcj17KHRleHQsIHJlY29yZDogYW55LCBkYXRhSW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIGRhdGFJbmRleCArIDE7XG4gICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgLz5cbiAgICAgICAgPENvbHVtblxuICAgICAgICAgIHRpdGxlPVwiQW1vdW50XCJcbiAgICAgICAgICBrZXk9XCJhbW91bnRcIlxuICAgICAgICAgIGRhdGFJbmRleD17XCJhbW91bnRcIn1cbiAgICAgICAgICBjbGFzc05hbWU9XCJhbW91bnQtcmlnaHRcIlxuICAgICAgICAgIHJlbmRlcj17KHRleHQ6IGFueSwgcmVjb3JkOiBhbnksIGRhdGFJbmRleCkgPT5cbiAgICAgICAgICAgIHRoaXMucmVuZGVyQW1vdW50KHRleHQsIGRhdGFJbmRleClcbiAgICAgICAgICB9XG4gICAgICAgIC8+XG4gICAgICAgIDxDb2x1bW5cbiAgICAgICAgICB0aXRsZT1cIlBheW1lbnQgRGF0ZVwiXG4gICAgICAgICAga2V5PVwicGF5bWVudERhdGVcIlxuICAgICAgICAgIGRhdGFJbmRleD17XCJwYXltZW50RGF0ZVwifVxuICAgICAgICAgIHJlbmRlcj17KHRleHQ6IGFueSwgcmVjb3JkOiBhbnksIGRhdGFJbmRleCkgPT5cbiAgICAgICAgICAgIHRoaXMucmVuZGVyRGF0ZSh0ZXh0LCBkYXRhSW5kZXgpXG4gICAgICAgICAgfVxuICAgICAgICAvPlxuXG4gICAgICAgIDxDb2x1bW5cbiAgICAgICAgICB0aXRsZT1cIkFDVElPTlwiXG4gICAgICAgICAga2V5PVwiYWN0aW9uXCJcbiAgICAgICAgICBjbGFzc05hbWU9XCJhbW91bnQtY2VudGVyXCJcbiAgICAgICAgICByZW5kZXI9eyh0ZXh0LCByZWNvcmQ6IGFueSwgZGF0YUluZGV4KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBhY3Rpb25zOiBJQWN0aW9uW10gPSBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBhY3Rpb25OYW1lOiBcIkRlbGV0ZVwiLFxuICAgICAgICAgICAgICAgIGhhbmRlbDogKCkgPT4geyAvLyB0b2RvIOmqjOivgVxuICAgICAgICAgICAgICAgICAgY29uc3QgX2xpc3QgPSB0aGlzLnRhYmxlRGF0YTtcblxuICAgICAgICAgICAgICAgICAgaWYgKF8uaXNFbXB0eShfbGlzdCkpIHJldHVybjtcbiAgICAgICAgICAgICAgICAgIGNvbnN0IF9uZXdMaXN0ID0gX2xpc3QuZmlsdGVyKChpdGVtOiBhbnksIGluZGV4OiBudW1iZXIpID0+IGluZGV4ICE9IGRhdGFJbmRleCk7XG4gICAgICAgICAgICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChbXSwgdGhpcy5nZXRQcm9wKFwiZGV0YWlsc1wiKSk7XG4gICAgICAgICAgICAgICAgICBfbmV3TGlzdC5mb3JFYWNoKChpdGVtOiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoaXRlbSwgdGhpcy5nZXRQcm9wKGBkZXRhaWxzLiR7aW5kZXh9YCkpO1xuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF07XG4gICAgICAgICAgICByZXR1cm4gPFRhYmxlQWN0aW9uQmFycyBhY3Rpb25zPXthY3Rpb25zfS8+O1xuICAgICAgICAgIH19XG4gICAgICAgIC8+XG4gICAgICA8L1RhYmxlPlxuICAgICk7XG4gIH1cblxuICByZW5kZXJGb3JtQWN0aW9uKCkge1xuICAgIGNvbnN0IHsgbWF4IH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IGxlbmd0aCA9ICh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcChcImRldGFpbHNcIikpIHx8IFtdKS5sZW5ndGg7XG4gICAgcmV0dXJuIChcbiAgICAgIDxUYWJsZVVuZGVyQWN0aW9ucyBhY3Rpb25zPXtbXG4gICAgICAgIHtcbiAgICAgICAgICBhY3Rpb25OYW1lOiBMYW5ndWFnZS5lbihcIkFkZFwiKS50aGFpKFwiQWRkXCIpLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICBpc1Nob3c6IG1heCA+IGxlbmd0aCxcbiAgICAgICAgICBoYW5kZWw6ICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaGFuZGxlQWRkKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgIF19Lz5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyQW1vdW50KHRleHQ6IGFueSwgZGF0YUluZGV4OiBudW1iZXIpIHtcbiAgICBjb25zdCB7IGZvcm0sIG1vZGVsLCBvbGRNb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBwcmVtQ3VycmVuY3kgPSBfLmdldChvbGRNb2RlbCwgXCJwb2xpY3kucHJlbUN1cnJlbmN5Q29kZVwiKSB8fCBcIlNHRFwiO1xuICAgIGNvbnN0IGN1cnJlbmN5U3ltYm9sID0gQ29uc3RzLkNVUlJFTkNZX1NZTUJPTF9UQUJMRVtwcmVtQ3VycmVuY3ldO1xuICAgIGlmICh0aGlzLnN0YXRlLmVkaXRJbmRleCAhPSBkYXRhSW5kZXgpIHtcbiAgICAgIHJldHVybiA8ZGl2XG4gICAgICAgIHN0eWxlPXt7IHBhZGRpbmdSaWdodDogNCB9fT57Y3VycmVuY3lTeW1ib2x9IHt0aGlzLmZvcm1tYXRDdXJyZW5jeSh0ZXh0KX08L2Rpdj47XG4gICAgfVxuXG4gICAgcmV0dXJuIChcbiAgICAgIDxTdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgICAgIDxOUHJpY2VcbiAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIgfX1cbiAgICAgICAgICBwcmVjaXNpb249ezJ9XG4gICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICBsYWJlbD17XCJcIn1cbiAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgYWRkb25CZWZvcmU9e2N1cnJlbmN5U3ltYm9sfVxuICAgICAgICAgIHNpemU9XCJkZWZhdWx0XCJcbiAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZXRQcm9wKGBkZXRhaWxzLiR7ZGF0YUluZGV4fS5hbW91bnRgKX1cbiAgICAgICAgLz5cbiAgICAgIDwvU3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPlxuICAgICk7XG4gIH1cblxuICByZW5kZXJEYXRlKHRleHQ6IGFueSwgZGF0YUluZGV4OiBudW1iZXIpIHtcbiAgICBjb25zdCBuZXdBcnIgPSB0aGlzLnRhYmxlRGF0YS5zbGljZSgwLCBkYXRhSW5kZXgpO1xuICAgIGNvbnN0IGZpbmRJbmRleCA9IF8uZmluZExhc3RJbmRleChuZXdBcnIsIGZ1bmN0aW9uKG86IGFueSkge1xuICAgICAgcmV0dXJuICFfLmlzRW1wdHkoXy5nZXQobywgXCJwYXltZW50RGF0ZVwiKSk7XG4gICAgfSk7XG4gICAgY29uc3QgZWZmRGF0ZSA9IF8uZ2V0KHRoaXMucHJvcHMub2xkTW9kZWwsIHRoaXMuZ2V0UHJvcChcInBvbGljeS5lZmZEYXRlXCIpKTtcbiAgICBjb25zdCBleHBEYXRlID0gXy5nZXQodGhpcy5wcm9wcy5vbGRNb2RlbCwgdGhpcy5nZXRQcm9wKFwicG9saWN5LmV4cERhdGVcIikpO1xuICAgIGlmICh0aGlzLnN0YXRlLmVkaXRJbmRleCAhPSBkYXRhSW5kZXgpIHtcbiAgICAgIHJldHVybiA8ZGl2XG4gICAgICAgIHN0eWxlPXt7IHBhZGRpbmdSaWdodDogNCB9fT57dGV4dCA/IERhdGVVdGlscy5mb3JtYXREYXRlKERhdGVVdGlscy50b0RhdGUodGV4dCkpIDogXCJcIn1cbiAgICAgIDwvZGl2PjtcbiAgICB9XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCwgb2xkTW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxTdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgICAgIDxkaXYgb25DbGljaz17KGUpID0+IHtcbiAgICAgICAgICAvLyDpmLvmraLlhpLms6Hlr7zoh7TnmoTlm6DkuLrooYzngrnlh7vkuovku7blr7zoh7Tml6XmnJ/pgInmi6nkuIrkuIDmnIjkuIvkuIDmnIjml6Dms5Xngrnlh7vnmoTpl67pophcbiAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICB9fT5cbiAgICAgICAgICA8TkRhdGVcbiAgICAgICAgICAgIHN0eWxlPXt7IHdpZHRoOiBcIjEwMCVcIiB9fVxuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIGxhYmVsPXtcIlwifVxuICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgZm9ybWF0PXtDb25zdHMuREFURV9GT1JNQVQuREFURV9GT1JNQVR9XG4gICAgICAgICAgICBzaXplPVwiZGVmYXVsdFwiXG4gICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgIGRpc2FibGVkRGF0ZT17KGN1cnJlbnQ6IGFueSkgPT4ge1xuICAgICAgICAgICAgICBpZiAoZmluZEluZGV4ID09PSAtMSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBjdXJyZW50LmlzQmVmb3JlKERhdGVVdGlscy50b0RhdGUoZWZmRGF0ZSksIFwiZGF5XCIpIHx8XG4gICAgICAgICAgICAgICAgICBjdXJyZW50LmlzQWZ0ZXIoRGF0ZVV0aWxzLnRvRGF0ZShleHBEYXRlKSwgXCJkYXlcIik7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc3QgZW5kRGF0ZSA9IF8uZ2V0KG5ld0FyciwgYCR7ZmluZEluZGV4fS5wYXltZW50RGF0ZWApO1xuICAgICAgICAgICAgICAgIHJldHVybiBjdXJyZW50LmlzU2FtZU9yQmVmb3JlKERhdGVVdGlscy50b0RhdGUoZW5kRGF0ZSksIFwiZGF5XCIpIHx8XG4gICAgICAgICAgICAgICAgICBjdXJyZW50LmlzQWZ0ZXIoRGF0ZVV0aWxzLnRvRGF0ZShleHBEYXRlKSwgXCJkYXlcIik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH19XG4gICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZXRQcm9wKGBkZXRhaWxzLiR7ZGF0YUluZGV4fS5wYXltZW50RGF0ZWApfVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9TdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgKTtcbiAgfVxuXG4gIC8vIGFjdGlvbnNcbiAgdG9nZ2xlRWRpdCA9IChpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGVkaXRJbmRleDogaW5kZXggfSwgKCkgPT4ge1xuICAgIH0pO1xuICB9O1xuXG4gIGhhbmRsZUFkZCgpIHtcbiAgICBjb25zdCBfdGFibGVEYXRhID0gWy4uLnRoaXMudGFibGVEYXRhLCB7IGFtb3VudDogMCB9XTtcbiAgICB0aGlzLnRvZ2dsZUVkaXQoX3RhYmxlRGF0YS5sZW5ndGggLSAxKTtcbiAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChfdGFibGVEYXRhLCB0aGlzLmdldFByb3AoXCJkZXRhaWxzXCIpKTtcbiAgfVxuXG4gIGhhbmRsZVNhdmUoKSB7XG4gICAgdGhpcy5wcm9wcy5mb3JtLnZhbGlkYXRlRmllbGRzKChlcnI6IGFueSwgZmllbGRzVmFsdWU6IGFueSkgPT4ge1xuICAgICAgaWYgKGVycikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBsZXQgbmV3RGV0YWlscyA9IF8uY2xvbmVEZWVwKF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIFwiZGV0YWlsc1wiKSk7XG4gICAgICBuZXdEZXRhaWxzLmZvckVhY2goKGl0ZW06IGFueSwgaW5kZXg6IGFueSkgPT4ge1xuICAgICAgICBpdGVtLnNlcU5vID0gaW5kZXggKyAxO1xuICAgICAgfSk7XG4gICAgICB0aGlzLnByb3BzLm9uU2F2ZShuZXdEZXRhaWxzKTtcbiAgICAgIHRoaXMucHJvcHMub25DYW5jZWwoKTtcbiAgICB9KTtcbiAgfVxuXG4gIGhhbmRsZVNwbGl0KHBsYW46IHN0cmluZykge1xuICAgIGNvbnN0IHBvbGljeSA9IF8uY2xvbmVEZWVwKF8uZ2V0KHRoaXMucHJvcHMub2xkTW9kZWwsIFwicG9saWN5XCIsIHt9KSk7XG4gICAgY29uc3QgcG9saWN5SWQgPSBfLmdldChwb2xpY3ksIFwicG9saWN5SWRcIiwgXCJcIik7XG4gICAgQWpheC5wb3N0KEFwaXMuUEFZTUVOVF9QTEFOX0NVU1RPTUlaQVRJT04ucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBwb2xpY3lJZCksXG4gICAgICB7IHBvbGljeSwgcGF5bWVudFBsYW46IHBsYW4gfSwgeyBsb2FkaW5nOiB0cnVlIH0pXG4gICAgICAudGhlbigocmVzOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgY29uc3QgZGF0YSA9IF8uZ2V0KHJlcywgXCJib2R5LnJlc3BEYXRhXCIpIHx8IFtdO1xuICAgICAgICB0aGlzLnRvZ2dsZUVkaXQoLTEpO1xuICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChkYXRhLCB0aGlzLmdldFByb3AoXCJkZXRhaWxzXCIpKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgLy8gbWV0aG9kc1xuICBnZXRQcm9wKHByb3BOYW1lOiBhbnkpIHtcbiAgICByZXR1cm4gcHJvcE5hbWU7XG4gIH1cblxuICBmb3JtbWF0Q3VycmVuY3kobW9uZXk6IGFueSkge1xuICAgIGNvbnN0IF9tb25leSA9IHBhcnNlRmxvYXQobW9uZXkgfHwgMCkudG9GaXhlZCgyKTtcbiAgICByZXR1cm4gYCR7VXRpbHMudG9UaG91c2FuZHMoX21vbmV5IHx8IDApfWA7XG4gIH1cblxuICBjYWxjQXJyYW5nZWRQcmVtaXVtKCkge1xuICAgIGNvbnN0IHsgb2xkTW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgbGV0IHByZW1pdW0gPSBfLmdldChvbGRNb2RlbCwgdGhpcy5nZXRQcm9wKFwicG9saWN5LnBvbGljeVByZW1pdW0ubHVtcHN1bS5hclwiKSkgfHwgMDtcbiAgICB0aGlzLnRhYmxlRGF0YS5mb3JFYWNoKChpdGVtOiBhbnkpID0+IHtcbiAgICAgIGlmIChSdWxlcy5pc051bShpdGVtLmFtb3VudCkpIHtcbiAgICAgICAgcHJlbWl1bSA9IHByZW1pdW0gLSBpdGVtLmFtb3VudDtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gcHJlbWl1bS50b0ZpeGVkKDIpO1xuICB9XG5cbiAgLy8gZ2V0dGVyc1xuICBnZXQgdGFibGVEYXRhKCkge1xuICAgIHJldHVybiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcChcImRldGFpbHNcIikpIHx8IFtdO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEZvcm0uY3JlYXRlPGFycmFuZ2VtZW50UHJvcHM+KCkoT3BlcmF0ZUFycmFuZ2VtZW50KTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFXQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQVNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFtQkE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQWlRQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBcFFBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTs7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFtQkE7OztBQUVBO0FBQ0E7OztBQUlBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXFCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUtBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQVhBO0FBY0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBckJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXlCQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUE7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBYUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBbEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXVCQTtBQUNBOzs7QUFPQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7OztBQTlUQTtBQUNBO0FBZ1VBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/components/panyment-arramgement/component/operate-arrangement.tsx
