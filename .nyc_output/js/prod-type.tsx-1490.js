__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProdTypeImage", function() { return ProdTypeImage; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _assets_car_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../assets/car.svg */ "./src/assets/car.svg");
/* harmony import */ var _assets_car_svg__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_assets_car_svg__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _assets_cmi_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../assets/cmi.svg */ "./src/assets/cmi.svg");
/* harmony import */ var _assets_cmi_svg__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_assets_cmi_svg__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _assets_life_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../assets/life.svg */ "./src/assets/life.svg");
/* harmony import */ var _assets_life_svg__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_life_svg__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _assets_annuity_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../assets/annuity.svg */ "./src/assets/annuity.svg");
/* harmony import */ var _assets_annuity_svg__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_assets_annuity_svg__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/query/prod-type.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["        \n    display: inline-block;\n    width: 30px;\n    height: 30px;\n    margin-right: 1em;\n    vertical-align: ", ";\n    span{\n      width: 30px;\n      height: 30px;\n      display: flex;\n      align-items: center;\n      &::before {\n          display: inline-block;\n          width: 100%;\n          height: 100%;\n      }\n      \n      i {\n        font-size: 30px !important;\n        color: rgba(0,0,0,.65);\n        &.icon-cmi {\n          font-size: 23px !important;\n        }\n        &.icon-grpcancer {\n          font-size: 25px !important;\n        }\n      }\n      &[data-type=CMI] {\n          &::before {\n              background-image: ", ";\n          }\n      }\n\n      &[data-type=VMI] {\n          &::before {\n              background-image: ", ";\n          }\n      }\n      &[data-type=ENDOWMENT] {\n          &::before {\n              background-image: ", ";\n          }\n      }\n      &[data-type=WLF] {\n          &::before {\n              background-image: ", ";\n          }\n      }\n      &[data-type=ANNUITY] {\n          &::before {\n              background-image: ", ";\n          }\n      }\n    }\n  "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var ProdTypeImage = function ProdTypeImage(props) {
  var productCate = lodash__WEBPACK_IMPORTED_MODULE_6___default.a.get(props, "productCate", "");

  var Div = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject(), productCate === "TC" ? "middle" : "sub", "url('".concat(_assets_cmi_svg__WEBPACK_IMPORTED_MODULE_3___default.a, "')"), "url('".concat(_assets_car_svg__WEBPACK_IMPORTED_MODULE_2___default.a, "')"), "url('".concat(_assets_life_svg__WEBPACK_IMPORTED_MODULE_4___default.a, "')"), "url('".concat(_assets_life_svg__WEBPACK_IMPORTED_MODULE_4___default.a, "')"), "url('".concat(_assets_annuity_svg__WEBPACK_IMPORTED_MODULE_5___default.a, "')"));
  return _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement(Div, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement("span", {
    className: "product  pro-image-front",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67
    },
    __self: this
  }, productCate === "TC" ? _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement("img", {
    style: {
      width: "40px",
      marginTop: "4px",
      marginBottom: "4px",
      marginRight: "0"
    },
    src: __webpack_require__(/*! @assets/icon-font/tc.svg */ "./src/assets/icon-font/tc.svg"),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69
    },
    __self: this
  }) : productCate ? _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement("i", {
    className: "iconfont icon-".concat(productCate.toLowerCase()),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: this
  }) : _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement("i", {
    className: "iconfont icon-product-common",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: this
  })));
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3F1ZXJ5L3Byb2QtdHlwZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcXVlcnkvcHJvZC10eXBlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgVm1pSW1hZ2UgZnJvbSBcIi4uLy4uLy4uLy4uL2Fzc2V0cy9jYXIuc3ZnXCI7XG5pbXBvcnQgQ21pSW1hZ2UgZnJvbSBcIi4uLy4uLy4uLy4uL2Fzc2V0cy9jbWkuc3ZnXCI7XG5pbXBvcnQgTGlmZUltYWdlIGZyb20gXCIuLi8uLi8uLi8uLi9hc3NldHMvbGlmZS5zdmdcIjtcbmltcG9ydCBBbm51aXR5SW1hZ2UgZnJvbSBcIi4uLy4uLy4uLy4uL2Fzc2V0cy9hbm51aXR5LnN2Z1wiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuXG5leHBvcnQgY29uc3QgUHJvZFR5cGVJbWFnZSA9IChwcm9wczogeyBwcm9kdWN0Q2F0ZTogc3RyaW5nIH0pID0+IHtcbiAgY29uc3QgcHJvZHVjdENhdGUgPSBfLmdldChwcm9wcywgXCJwcm9kdWN0Q2F0ZVwiLCBcIlwiKTtcbiAgY29uc3QgRGl2ID0gU3R5bGVkLmRpdmAgICAgICAgIFxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMzBweDtcbiAgICBoZWlnaHQ6IDMwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAxZW07XG4gICAgdmVydGljYWwtYWxpZ246ICR7cHJvZHVjdENhdGUgPT09IFwiVENcIiA/IFwibWlkZGxlXCIgOiBcInN1YlwifTtcbiAgICBzcGFue1xuICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICY6OmJlZm9yZSB7XG4gICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgIH1cbiAgICAgIFxuICAgICAgaSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMzBweCAhaW1wb3J0YW50O1xuICAgICAgICBjb2xvcjogcmdiYSgwLDAsMCwuNjUpO1xuICAgICAgICAmLmljb24tY21pIHtcbiAgICAgICAgICBmb250LXNpemU6IDIzcHggIWltcG9ydGFudDtcbiAgICAgICAgfVxuICAgICAgICAmLmljb24tZ3JwY2FuY2VyIHtcbiAgICAgICAgICBmb250LXNpemU6IDI1cHggIWltcG9ydGFudDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgJltkYXRhLXR5cGU9Q01JXSB7XG4gICAgICAgICAgJjo6YmVmb3JlIHtcbiAgICAgICAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogJHtgdXJsKCcke0NtaUltYWdlfScpYH07XG4gICAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAmW2RhdGEtdHlwZT1WTUldIHtcbiAgICAgICAgICAmOjpiZWZvcmUge1xuICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiAke2B1cmwoJyR7Vm1pSW1hZ2V9JylgfTtcbiAgICAgICAgICB9XG4gICAgICB9XG4gICAgICAmW2RhdGEtdHlwZT1FTkRPV01FTlRdIHtcbiAgICAgICAgICAmOjpiZWZvcmUge1xuICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiAke2B1cmwoJyR7TGlmZUltYWdlfScpYH07XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgICAgJltkYXRhLXR5cGU9V0xGXSB7XG4gICAgICAgICAgJjo6YmVmb3JlIHtcbiAgICAgICAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogJHtgdXJsKCcke0xpZmVJbWFnZX0nKWB9O1xuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICAgICZbZGF0YS10eXBlPUFOTlVJVFldIHtcbiAgICAgICAgICAmOjpiZWZvcmUge1xuICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiAke2B1cmwoJyR7QW5udWl0eUltYWdlfScpYH07XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgYDtcbiAgcmV0dXJuIChcbiAgICA8RGl2PlxuICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicHJvZHVjdCAgcHJvLWltYWdlLWZyb250XCI+XG4gICAgICAgIHtwcm9kdWN0Q2F0ZSA9PT0gXCJUQ1wiID9cbiAgICAgICAgICA8aW1nIHN0eWxlPXt7XG4gICAgICAgICAgICB3aWR0aDogXCI0MHB4XCIsXG4gICAgICAgICAgICBtYXJnaW5Ub3A6IFwiNHB4XCIsXG4gICAgICAgICAgICBtYXJnaW5Cb3R0b206IFwiNHB4XCIsXG4gICAgICAgICAgICBtYXJnaW5SaWdodDogXCIwXCIsXG4gICAgICAgICAgfX0gc3JjPXtyZXF1aXJlKFwiQGFzc2V0cy9pY29uLWZvbnQvdGMuc3ZnXCIpfS8+XG4gICAgICAgICAgOiAocHJvZHVjdENhdGUgP1xuICAgICAgICAgICAgPGkgY2xhc3NOYW1lPXtgaWNvbmZvbnQgaWNvbi0ke3Byb2R1Y3RDYXRlLnRvTG93ZXJDYXNlKCl9YH0vPlxuICAgICAgICAgICAgOiA8aSBjbGFzc05hbWU9e2BpY29uZm9udCBpY29uLXByb2R1Y3QtY29tbW9uYH0vPil9XG4gICAgICA8L3NwYW4+XG4gICAgPC9EaXY+XG4gICk7XG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7Ozs7QUFLQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBdURBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFLQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/query/prod-type.tsx
