/**
 * Sub-pixel optimize for canvas rendering, prevent from blur
 * when rendering a thin vertical/horizontal line.
 */
var round = Math.round;
/**
 * Sub pixel optimize line for canvas
 *
 * @param {Object} outputShape The modification will be performed on `outputShape`.
 *                 `outputShape` and `inputShape` can be the same object.
 *                 `outputShape` object can be used repeatly, because all of
 *                 the `x1`, `x2`, `y1`, `y2` will be assigned in this method.
 * @param {Object} [inputShape]
 * @param {number} [inputShape.x1]
 * @param {number} [inputShape.y1]
 * @param {number} [inputShape.x2]
 * @param {number} [inputShape.y2]
 * @param {Object} [style]
 * @param {number} [style.lineWidth]
 */

function subPixelOptimizeLine(outputShape, inputShape, style) {
  var lineWidth = style && style.lineWidth;

  if (!inputShape || !lineWidth) {
    return;
  }

  var x1 = inputShape.x1;
  var x2 = inputShape.x2;
  var y1 = inputShape.y1;
  var y2 = inputShape.y2;

  if (round(x1 * 2) === round(x2 * 2)) {
    outputShape.x1 = outputShape.x2 = subPixelOptimize(x1, lineWidth, true);
  } else {
    outputShape.x1 = x1;
    outputShape.x2 = x2;
  }

  if (round(y1 * 2) === round(y2 * 2)) {
    outputShape.y1 = outputShape.y2 = subPixelOptimize(y1, lineWidth, true);
  } else {
    outputShape.y1 = y1;
    outputShape.y2 = y2;
  }
}
/**
 * Sub pixel optimize rect for canvas
 *
 * @param {Object} outputShape The modification will be performed on `outputShape`.
 *                 `outputShape` and `inputShape` can be the same object.
 *                 `outputShape` object can be used repeatly, because all of
 *                 the `x`, `y`, `width`, `height` will be assigned in this method.
 * @param {Object} [inputShape]
 * @param {number} [inputShape.x]
 * @param {number} [inputShape.y]
 * @param {number} [inputShape.width]
 * @param {number} [inputShape.height]
 * @param {Object} [style]
 * @param {number} [style.lineWidth]
 */


function subPixelOptimizeRect(outputShape, inputShape, style) {
  var lineWidth = style && style.lineWidth;

  if (!inputShape || !lineWidth) {
    return;
  }

  var originX = inputShape.x;
  var originY = inputShape.y;
  var originWidth = inputShape.width;
  var originHeight = inputShape.height;
  outputShape.x = subPixelOptimize(originX, lineWidth, true);
  outputShape.y = subPixelOptimize(originY, lineWidth, true);
  outputShape.width = Math.max(subPixelOptimize(originX + originWidth, lineWidth, false) - outputShape.x, originWidth === 0 ? 0 : 1);
  outputShape.height = Math.max(subPixelOptimize(originY + originHeight, lineWidth, false) - outputShape.y, originHeight === 0 ? 0 : 1);
}
/**
 * Sub pixel optimize for canvas
 *
 * @param {number} position Coordinate, such as x, y
 * @param {number} lineWidth Should be nonnegative integer.
 * @param {boolean=} positiveOrNegative Default false (negative).
 * @return {number} Optimized position.
 */


function subPixelOptimize(position, lineWidth, positiveOrNegative) {
  // Assure that (position + lineWidth / 2) is near integer edge,
  // otherwise line will be fuzzy in canvas.
  var doubledPosition = round(position * 2);
  return (doubledPosition + round(lineWidth)) % 2 === 0 ? doubledPosition / 2 : (doubledPosition + (positiveOrNegative ? 1 : -1)) / 2;
}

exports.subPixelOptimizeLine = subPixelOptimizeLine;
exports.subPixelOptimizeRect = subPixelOptimizeRect;
exports.subPixelOptimize = subPixelOptimize;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvc3ViUGl4ZWxPcHRpbWl6ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvaGVscGVyL3N1YlBpeGVsT3B0aW1pemUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBTdWItcGl4ZWwgb3B0aW1pemUgZm9yIGNhbnZhcyByZW5kZXJpbmcsIHByZXZlbnQgZnJvbSBibHVyXG4gKiB3aGVuIHJlbmRlcmluZyBhIHRoaW4gdmVydGljYWwvaG9yaXpvbnRhbCBsaW5lLlxuICovXG52YXIgcm91bmQgPSBNYXRoLnJvdW5kO1xuLyoqXG4gKiBTdWIgcGl4ZWwgb3B0aW1pemUgbGluZSBmb3IgY2FudmFzXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG91dHB1dFNoYXBlIFRoZSBtb2RpZmljYXRpb24gd2lsbCBiZSBwZXJmb3JtZWQgb24gYG91dHB1dFNoYXBlYC5cbiAqICAgICAgICAgICAgICAgICBgb3V0cHV0U2hhcGVgIGFuZCBgaW5wdXRTaGFwZWAgY2FuIGJlIHRoZSBzYW1lIG9iamVjdC5cbiAqICAgICAgICAgICAgICAgICBgb3V0cHV0U2hhcGVgIG9iamVjdCBjYW4gYmUgdXNlZCByZXBlYXRseSwgYmVjYXVzZSBhbGwgb2ZcbiAqICAgICAgICAgICAgICAgICB0aGUgYHgxYCwgYHgyYCwgYHkxYCwgYHkyYCB3aWxsIGJlIGFzc2lnbmVkIGluIHRoaXMgbWV0aG9kLlxuICogQHBhcmFtIHtPYmplY3R9IFtpbnB1dFNoYXBlXVxuICogQHBhcmFtIHtudW1iZXJ9IFtpbnB1dFNoYXBlLngxXVxuICogQHBhcmFtIHtudW1iZXJ9IFtpbnB1dFNoYXBlLnkxXVxuICogQHBhcmFtIHtudW1iZXJ9IFtpbnB1dFNoYXBlLngyXVxuICogQHBhcmFtIHtudW1iZXJ9IFtpbnB1dFNoYXBlLnkyXVxuICogQHBhcmFtIHtPYmplY3R9IFtzdHlsZV1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbc3R5bGUubGluZVdpZHRoXVxuICovXG5cbmZ1bmN0aW9uIHN1YlBpeGVsT3B0aW1pemVMaW5lKG91dHB1dFNoYXBlLCBpbnB1dFNoYXBlLCBzdHlsZSkge1xuICB2YXIgbGluZVdpZHRoID0gc3R5bGUgJiYgc3R5bGUubGluZVdpZHRoO1xuXG4gIGlmICghaW5wdXRTaGFwZSB8fCAhbGluZVdpZHRoKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHgxID0gaW5wdXRTaGFwZS54MTtcbiAgdmFyIHgyID0gaW5wdXRTaGFwZS54MjtcbiAgdmFyIHkxID0gaW5wdXRTaGFwZS55MTtcbiAgdmFyIHkyID0gaW5wdXRTaGFwZS55MjtcblxuICBpZiAocm91bmQoeDEgKiAyKSA9PT0gcm91bmQoeDIgKiAyKSkge1xuICAgIG91dHB1dFNoYXBlLngxID0gb3V0cHV0U2hhcGUueDIgPSBzdWJQaXhlbE9wdGltaXplKHgxLCBsaW5lV2lkdGgsIHRydWUpO1xuICB9IGVsc2Uge1xuICAgIG91dHB1dFNoYXBlLngxID0geDE7XG4gICAgb3V0cHV0U2hhcGUueDIgPSB4MjtcbiAgfVxuXG4gIGlmIChyb3VuZCh5MSAqIDIpID09PSByb3VuZCh5MiAqIDIpKSB7XG4gICAgb3V0cHV0U2hhcGUueTEgPSBvdXRwdXRTaGFwZS55MiA9IHN1YlBpeGVsT3B0aW1pemUoeTEsIGxpbmVXaWR0aCwgdHJ1ZSk7XG4gIH0gZWxzZSB7XG4gICAgb3V0cHV0U2hhcGUueTEgPSB5MTtcbiAgICBvdXRwdXRTaGFwZS55MiA9IHkyO1xuICB9XG59XG4vKipcbiAqIFN1YiBwaXhlbCBvcHRpbWl6ZSByZWN0IGZvciBjYW52YXNcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb3V0cHV0U2hhcGUgVGhlIG1vZGlmaWNhdGlvbiB3aWxsIGJlIHBlcmZvcm1lZCBvbiBgb3V0cHV0U2hhcGVgLlxuICogICAgICAgICAgICAgICAgIGBvdXRwdXRTaGFwZWAgYW5kIGBpbnB1dFNoYXBlYCBjYW4gYmUgdGhlIHNhbWUgb2JqZWN0LlxuICogICAgICAgICAgICAgICAgIGBvdXRwdXRTaGFwZWAgb2JqZWN0IGNhbiBiZSB1c2VkIHJlcGVhdGx5LCBiZWNhdXNlIGFsbCBvZlxuICogICAgICAgICAgICAgICAgIHRoZSBgeGAsIGB5YCwgYHdpZHRoYCwgYGhlaWdodGAgd2lsbCBiZSBhc3NpZ25lZCBpbiB0aGlzIG1ldGhvZC5cbiAqIEBwYXJhbSB7T2JqZWN0fSBbaW5wdXRTaGFwZV1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbaW5wdXRTaGFwZS54XVxuICogQHBhcmFtIHtudW1iZXJ9IFtpbnB1dFNoYXBlLnldXG4gKiBAcGFyYW0ge251bWJlcn0gW2lucHV0U2hhcGUud2lkdGhdXG4gKiBAcGFyYW0ge251bWJlcn0gW2lucHV0U2hhcGUuaGVpZ2h0XVxuICogQHBhcmFtIHtPYmplY3R9IFtzdHlsZV1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbc3R5bGUubGluZVdpZHRoXVxuICovXG5cblxuZnVuY3Rpb24gc3ViUGl4ZWxPcHRpbWl6ZVJlY3Qob3V0cHV0U2hhcGUsIGlucHV0U2hhcGUsIHN0eWxlKSB7XG4gIHZhciBsaW5lV2lkdGggPSBzdHlsZSAmJiBzdHlsZS5saW5lV2lkdGg7XG5cbiAgaWYgKCFpbnB1dFNoYXBlIHx8ICFsaW5lV2lkdGgpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgb3JpZ2luWCA9IGlucHV0U2hhcGUueDtcbiAgdmFyIG9yaWdpblkgPSBpbnB1dFNoYXBlLnk7XG4gIHZhciBvcmlnaW5XaWR0aCA9IGlucHV0U2hhcGUud2lkdGg7XG4gIHZhciBvcmlnaW5IZWlnaHQgPSBpbnB1dFNoYXBlLmhlaWdodDtcbiAgb3V0cHV0U2hhcGUueCA9IHN1YlBpeGVsT3B0aW1pemUob3JpZ2luWCwgbGluZVdpZHRoLCB0cnVlKTtcbiAgb3V0cHV0U2hhcGUueSA9IHN1YlBpeGVsT3B0aW1pemUob3JpZ2luWSwgbGluZVdpZHRoLCB0cnVlKTtcbiAgb3V0cHV0U2hhcGUud2lkdGggPSBNYXRoLm1heChzdWJQaXhlbE9wdGltaXplKG9yaWdpblggKyBvcmlnaW5XaWR0aCwgbGluZVdpZHRoLCBmYWxzZSkgLSBvdXRwdXRTaGFwZS54LCBvcmlnaW5XaWR0aCA9PT0gMCA/IDAgOiAxKTtcbiAgb3V0cHV0U2hhcGUuaGVpZ2h0ID0gTWF0aC5tYXgoc3ViUGl4ZWxPcHRpbWl6ZShvcmlnaW5ZICsgb3JpZ2luSGVpZ2h0LCBsaW5lV2lkdGgsIGZhbHNlKSAtIG91dHB1dFNoYXBlLnksIG9yaWdpbkhlaWdodCA9PT0gMCA/IDAgOiAxKTtcbn1cbi8qKlxuICogU3ViIHBpeGVsIG9wdGltaXplIGZvciBjYW52YXNcbiAqXG4gKiBAcGFyYW0ge251bWJlcn0gcG9zaXRpb24gQ29vcmRpbmF0ZSwgc3VjaCBhcyB4LCB5XG4gKiBAcGFyYW0ge251bWJlcn0gbGluZVdpZHRoIFNob3VsZCBiZSBub25uZWdhdGl2ZSBpbnRlZ2VyLlxuICogQHBhcmFtIHtib29sZWFuPX0gcG9zaXRpdmVPck5lZ2F0aXZlIERlZmF1bHQgZmFsc2UgKG5lZ2F0aXZlKS5cbiAqIEByZXR1cm4ge251bWJlcn0gT3B0aW1pemVkIHBvc2l0aW9uLlxuICovXG5cblxuZnVuY3Rpb24gc3ViUGl4ZWxPcHRpbWl6ZShwb3NpdGlvbiwgbGluZVdpZHRoLCBwb3NpdGl2ZU9yTmVnYXRpdmUpIHtcbiAgLy8gQXNzdXJlIHRoYXQgKHBvc2l0aW9uICsgbGluZVdpZHRoIC8gMikgaXMgbmVhciBpbnRlZ2VyIGVkZ2UsXG4gIC8vIG90aGVyd2lzZSBsaW5lIHdpbGwgYmUgZnV6enkgaW4gY2FudmFzLlxuICB2YXIgZG91YmxlZFBvc2l0aW9uID0gcm91bmQocG9zaXRpb24gKiAyKTtcbiAgcmV0dXJuIChkb3VibGVkUG9zaXRpb24gKyByb3VuZChsaW5lV2lkdGgpKSAlIDIgPT09IDAgPyBkb3VibGVkUG9zaXRpb24gLyAyIDogKGRvdWJsZWRQb3NpdGlvbiArIChwb3NpdGl2ZU9yTmVnYXRpdmUgPyAxIDogLTEpKSAvIDI7XG59XG5cbmV4cG9ydHMuc3ViUGl4ZWxPcHRpbWl6ZUxpbmUgPSBzdWJQaXhlbE9wdGltaXplTGluZTtcbmV4cG9ydHMuc3ViUGl4ZWxPcHRpbWl6ZVJlY3QgPSBzdWJQaXhlbE9wdGltaXplUmVjdDtcbmV4cG9ydHMuc3ViUGl4ZWxPcHRpbWl6ZSA9IHN1YlBpeGVsT3B0aW1pemU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7OztBQUlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7O0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/helper/subPixelOptimize.js
