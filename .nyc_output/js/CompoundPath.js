var Path = __webpack_require__(/*! ./Path */ "./node_modules/zrender/lib/graphic/Path.js"); // CompoundPath to improve performance


var _default = Path.extend({
  type: 'compound',
  shape: {
    paths: null
  },
  _updatePathDirty: function _updatePathDirty() {
    var dirtyPath = this.__dirtyPath;
    var paths = this.shape.paths;

    for (var i = 0; i < paths.length; i++) {
      // Mark as dirty if any subpath is dirty
      dirtyPath = dirtyPath || paths[i].__dirtyPath;
    }

    this.__dirtyPath = dirtyPath;
    this.__dirty = this.__dirty || dirtyPath;
  },
  beforeBrush: function beforeBrush() {
    this._updatePathDirty();

    var paths = this.shape.paths || [];
    var scale = this.getGlobalScale(); // Update path scale

    for (var i = 0; i < paths.length; i++) {
      if (!paths[i].path) {
        paths[i].createPathProxy();
      }

      paths[i].path.setScale(scale[0], scale[1]);
    }
  },
  buildPath: function buildPath(ctx, shape) {
    var paths = shape.paths || [];

    for (var i = 0; i < paths.length; i++) {
      paths[i].buildPath(ctx, paths[i].shape, true);
    }
  },
  afterBrush: function afterBrush() {
    var paths = this.shape.paths || [];

    for (var i = 0; i < paths.length; i++) {
      paths[i].__dirtyPath = false;
    }
  },
  getBoundingRect: function getBoundingRect() {
    this._updatePathDirty();

    return Path.prototype.getBoundingRect.call(this);
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9Db21wb3VuZFBhdGguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9ncmFwaGljL0NvbXBvdW5kUGF0aC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgUGF0aCA9IHJlcXVpcmUoXCIuL1BhdGhcIik7XG5cbi8vIENvbXBvdW5kUGF0aCB0byBpbXByb3ZlIHBlcmZvcm1hbmNlXG52YXIgX2RlZmF1bHQgPSBQYXRoLmV4dGVuZCh7XG4gIHR5cGU6ICdjb21wb3VuZCcsXG4gIHNoYXBlOiB7XG4gICAgcGF0aHM6IG51bGxcbiAgfSxcbiAgX3VwZGF0ZVBhdGhEaXJ0eTogZnVuY3Rpb24gKCkge1xuICAgIHZhciBkaXJ0eVBhdGggPSB0aGlzLl9fZGlydHlQYXRoO1xuICAgIHZhciBwYXRocyA9IHRoaXMuc2hhcGUucGF0aHM7XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHBhdGhzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAvLyBNYXJrIGFzIGRpcnR5IGlmIGFueSBzdWJwYXRoIGlzIGRpcnR5XG4gICAgICBkaXJ0eVBhdGggPSBkaXJ0eVBhdGggfHwgcGF0aHNbaV0uX19kaXJ0eVBhdGg7XG4gICAgfVxuXG4gICAgdGhpcy5fX2RpcnR5UGF0aCA9IGRpcnR5UGF0aDtcbiAgICB0aGlzLl9fZGlydHkgPSB0aGlzLl9fZGlydHkgfHwgZGlydHlQYXRoO1xuICB9LFxuICBiZWZvcmVCcnVzaDogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX3VwZGF0ZVBhdGhEaXJ0eSgpO1xuXG4gICAgdmFyIHBhdGhzID0gdGhpcy5zaGFwZS5wYXRocyB8fCBbXTtcbiAgICB2YXIgc2NhbGUgPSB0aGlzLmdldEdsb2JhbFNjYWxlKCk7IC8vIFVwZGF0ZSBwYXRoIHNjYWxlXG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHBhdGhzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBpZiAoIXBhdGhzW2ldLnBhdGgpIHtcbiAgICAgICAgcGF0aHNbaV0uY3JlYXRlUGF0aFByb3h5KCk7XG4gICAgICB9XG5cbiAgICAgIHBhdGhzW2ldLnBhdGguc2V0U2NhbGUoc2NhbGVbMF0sIHNjYWxlWzFdKTtcbiAgICB9XG4gIH0sXG4gIGJ1aWxkUGF0aDogZnVuY3Rpb24gKGN0eCwgc2hhcGUpIHtcbiAgICB2YXIgcGF0aHMgPSBzaGFwZS5wYXRocyB8fCBbXTtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcGF0aHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHBhdGhzW2ldLmJ1aWxkUGF0aChjdHgsIHBhdGhzW2ldLnNoYXBlLCB0cnVlKTtcbiAgICB9XG4gIH0sXG4gIGFmdGVyQnJ1c2g6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgcGF0aHMgPSB0aGlzLnNoYXBlLnBhdGhzIHx8IFtdO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYXRocy5sZW5ndGg7IGkrKykge1xuICAgICAgcGF0aHNbaV0uX19kaXJ0eVBhdGggPSBmYWxzZTtcbiAgICB9XG4gIH0sXG4gIGdldEJvdW5kaW5nUmVjdDogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX3VwZGF0ZVBhdGhEaXJ0eSgpO1xuXG4gICAgcmV0dXJuIFBhdGgucHJvdG90eXBlLmdldEJvdW5kaW5nUmVjdC5jYWxsKHRoaXMpO1xuICB9XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFqREE7QUFDQTtBQW1EQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/CompoundPath.js
