

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = inViewport;

var _getElementPosition = __webpack_require__(/*! ./getElementPosition */ "./node_modules/react-lazy-load/lib/utils/getElementPosition.js");

var _getElementPosition2 = _interopRequireDefault(_getElementPosition);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

var isHidden = function isHidden(element) {
  return element.offsetParent === null;
};

function inViewport(element, container, customOffset) {
  if (isHidden(element)) {
    return false;
  }

  var top = void 0;
  var bottom = void 0;
  var left = void 0;
  var right = void 0;

  if (typeof container === 'undefined' || container === window) {
    top = window.pageYOffset;
    left = window.pageXOffset;
    bottom = top + window.innerHeight;
    right = left + window.innerWidth;
  } else {
    var containerPosition = (0, _getElementPosition2.default)(container);
    top = containerPosition.top;
    left = containerPosition.left;
    bottom = top + container.offsetHeight;
    right = left + container.offsetWidth;
  }

  var elementPosition = (0, _getElementPosition2.default)(element);
  return top <= elementPosition.top + element.offsetHeight + customOffset.top && bottom >= elementPosition.top - customOffset.bottom && left <= elementPosition.left + element.offsetWidth + customOffset.left && right >= elementPosition.left - customOffset.right;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtbGF6eS1sb2FkL2xpYi91dGlscy9pblZpZXdwb3J0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmVhY3QtbGF6eS1sb2FkL2xpYi91dGlscy9pblZpZXdwb3J0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuZGVmYXVsdCA9IGluVmlld3BvcnQ7XG5cbnZhciBfZ2V0RWxlbWVudFBvc2l0aW9uID0gcmVxdWlyZSgnLi9nZXRFbGVtZW50UG9zaXRpb24nKTtcblxudmFyIF9nZXRFbGVtZW50UG9zaXRpb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0RWxlbWVudFBvc2l0aW9uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGlzSGlkZGVuID0gZnVuY3Rpb24gaXNIaWRkZW4oZWxlbWVudCkge1xuICByZXR1cm4gZWxlbWVudC5vZmZzZXRQYXJlbnQgPT09IG51bGw7XG59O1xuXG5mdW5jdGlvbiBpblZpZXdwb3J0KGVsZW1lbnQsIGNvbnRhaW5lciwgY3VzdG9tT2Zmc2V0KSB7XG4gIGlmIChpc0hpZGRlbihlbGVtZW50KSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIHZhciB0b3AgPSB2b2lkIDA7XG4gIHZhciBib3R0b20gPSB2b2lkIDA7XG4gIHZhciBsZWZ0ID0gdm9pZCAwO1xuICB2YXIgcmlnaHQgPSB2b2lkIDA7XG5cbiAgaWYgKHR5cGVvZiBjb250YWluZXIgPT09ICd1bmRlZmluZWQnIHx8IGNvbnRhaW5lciA9PT0gd2luZG93KSB7XG4gICAgdG9wID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xuICAgIGxlZnQgPSB3aW5kb3cucGFnZVhPZmZzZXQ7XG4gICAgYm90dG9tID0gdG9wICsgd2luZG93LmlubmVySGVpZ2h0O1xuICAgIHJpZ2h0ID0gbGVmdCArIHdpbmRvdy5pbm5lcldpZHRoO1xuICB9IGVsc2Uge1xuICAgIHZhciBjb250YWluZXJQb3NpdGlvbiA9ICgwLCBfZ2V0RWxlbWVudFBvc2l0aW9uMi5kZWZhdWx0KShjb250YWluZXIpO1xuXG4gICAgdG9wID0gY29udGFpbmVyUG9zaXRpb24udG9wO1xuICAgIGxlZnQgPSBjb250YWluZXJQb3NpdGlvbi5sZWZ0O1xuICAgIGJvdHRvbSA9IHRvcCArIGNvbnRhaW5lci5vZmZzZXRIZWlnaHQ7XG4gICAgcmlnaHQgPSBsZWZ0ICsgY29udGFpbmVyLm9mZnNldFdpZHRoO1xuICB9XG5cbiAgdmFyIGVsZW1lbnRQb3NpdGlvbiA9ICgwLCBfZ2V0RWxlbWVudFBvc2l0aW9uMi5kZWZhdWx0KShlbGVtZW50KTtcblxuICByZXR1cm4gdG9wIDw9IGVsZW1lbnRQb3NpdGlvbi50b3AgKyBlbGVtZW50Lm9mZnNldEhlaWdodCArIGN1c3RvbU9mZnNldC50b3AgJiYgYm90dG9tID49IGVsZW1lbnRQb3NpdGlvbi50b3AgLSBjdXN0b21PZmZzZXQuYm90dG9tICYmIGxlZnQgPD0gZWxlbWVudFBvc2l0aW9uLmxlZnQgKyBlbGVtZW50Lm9mZnNldFdpZHRoICsgY3VzdG9tT2Zmc2V0LmxlZnQgJiYgcmlnaHQgPj0gZWxlbWVudFBvc2l0aW9uLmxlZnQgLSBjdXN0b21PZmZzZXQucmlnaHQ7XG59Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/react-lazy-load/lib/utils/inViewport.js
