/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var numberUtil = __webpack_require__(/*! ../util/number */ "./node_modules/echarts/lib/util/number.js");

var formatUtil = __webpack_require__(/*! ../util/format */ "./node_modules/echarts/lib/util/format.js");

var Scale = __webpack_require__(/*! ./Scale */ "./node_modules/echarts/lib/scale/Scale.js");

var helper = __webpack_require__(/*! ./helper */ "./node_modules/echarts/lib/scale/helper.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Interval scale
 * @module echarts/scale/Interval
 */


var roundNumber = numberUtil.round;
/**
 * @alias module:echarts/coord/scale/Interval
 * @constructor
 */

var IntervalScale = Scale.extend({
  type: 'interval',
  _interval: 0,
  _intervalPrecision: 2,
  setExtent: function setExtent(start, end) {
    var thisExtent = this._extent; //start,end may be a Number like '25',so...

    if (!isNaN(start)) {
      thisExtent[0] = parseFloat(start);
    }

    if (!isNaN(end)) {
      thisExtent[1] = parseFloat(end);
    }
  },
  unionExtent: function unionExtent(other) {
    var extent = this._extent;
    other[0] < extent[0] && (extent[0] = other[0]);
    other[1] > extent[1] && (extent[1] = other[1]); // unionExtent may called by it's sub classes

    IntervalScale.prototype.setExtent.call(this, extent[0], extent[1]);
  },

  /**
   * Get interval
   */
  getInterval: function getInterval() {
    return this._interval;
  },

  /**
   * Set interval
   */
  setInterval: function setInterval(interval) {
    this._interval = interval; // Dropped auto calculated niceExtent and use user setted extent
    // We assume user wan't to set both interval, min, max to get a better result

    this._niceExtent = this._extent.slice();
    this._intervalPrecision = helper.getIntervalPrecision(interval);
  },

  /**
   * @return {Array.<number>}
   */
  getTicks: function getTicks() {
    return helper.intervalScaleGetTicks(this._interval, this._extent, this._niceExtent, this._intervalPrecision);
  },

  /**
   * @param {number} data
   * @param {Object} [opt]
   * @param {number|string} [opt.precision] If 'auto', use nice presision.
   * @param {boolean} [opt.pad] returns 1.50 but not 1.5 if precision is 2.
   * @return {string}
   */
  getLabel: function getLabel(data, opt) {
    if (data == null) {
      return '';
    }

    var precision = opt && opt.precision;

    if (precision == null) {
      precision = numberUtil.getPrecisionSafe(data) || 0;
    } else if (precision === 'auto') {
      // Should be more precise then tick.
      precision = this._intervalPrecision;
    } // (1) If `precision` is set, 12.005 should be display as '12.00500'.
    // (2) Use roundNumber (toFixed) to avoid scientific notation like '3.5e-7'.


    data = roundNumber(data, precision, true);
    return formatUtil.addCommas(data);
  },

  /**
   * Update interval and extent of intervals for nice ticks
   *
   * @param {number} [splitNumber = 5] Desired number of ticks
   * @param {number} [minInterval]
   * @param {number} [maxInterval]
   */
  niceTicks: function niceTicks(splitNumber, minInterval, maxInterval) {
    splitNumber = splitNumber || 5;
    var extent = this._extent;
    var span = extent[1] - extent[0];

    if (!isFinite(span)) {
      return;
    } // User may set axis min 0 and data are all negative
    // FIXME If it needs to reverse ?


    if (span < 0) {
      span = -span;
      extent.reverse();
    }

    var result = helper.intervalScaleNiceTicks(extent, splitNumber, minInterval, maxInterval);
    this._intervalPrecision = result.intervalPrecision;
    this._interval = result.interval;
    this._niceExtent = result.niceTickExtent;
  },

  /**
   * Nice extent.
   * @param {Object} opt
   * @param {number} [opt.splitNumber = 5] Given approx tick number
   * @param {boolean} [opt.fixMin=false]
   * @param {boolean} [opt.fixMax=false]
   * @param {boolean} [opt.minInterval]
   * @param {boolean} [opt.maxInterval]
   */
  niceExtent: function niceExtent(opt) {
    var extent = this._extent; // If extent start and end are same, expand them

    if (extent[0] === extent[1]) {
      if (extent[0] !== 0) {
        // Expand extent
        var expandSize = extent[0]; // In the fowllowing case
        //      Axis has been fixed max 100
        //      Plus data are all 100 and axis extent are [100, 100].
        // Extend to the both side will cause expanded max is larger than fixed max.
        // So only expand to the smaller side.

        if (!opt.fixMax) {
          extent[1] += expandSize / 2;
          extent[0] -= expandSize / 2;
        } else {
          extent[0] -= expandSize / 2;
        }
      } else {
        extent[1] = 1;
      }
    }

    var span = extent[1] - extent[0]; // If there are no data and extent are [Infinity, -Infinity]

    if (!isFinite(span)) {
      extent[0] = 0;
      extent[1] = 1;
    }

    this.niceTicks(opt.splitNumber, opt.minInterval, opt.maxInterval); // var extent = this._extent;

    var interval = this._interval;

    if (!opt.fixMin) {
      extent[0] = roundNumber(Math.floor(extent[0] / interval) * interval);
    }

    if (!opt.fixMax) {
      extent[1] = roundNumber(Math.ceil(extent[1] / interval) * interval);
    }
  }
});
/**
 * @return {module:echarts/scale/Time}
 */

IntervalScale.create = function () {
  return new IntervalScale();
};

var _default = IntervalScale;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvc2NhbGUvSW50ZXJ2YWwuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9zY2FsZS9JbnRlcnZhbC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIG51bWJlclV0aWwgPSByZXF1aXJlKFwiLi4vdXRpbC9udW1iZXJcIik7XG5cbnZhciBmb3JtYXRVdGlsID0gcmVxdWlyZShcIi4uL3V0aWwvZm9ybWF0XCIpO1xuXG52YXIgU2NhbGUgPSByZXF1aXJlKFwiLi9TY2FsZVwiKTtcblxudmFyIGhlbHBlciA9IHJlcXVpcmUoXCIuL2hlbHBlclwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEludGVydmFsIHNjYWxlXG4gKiBAbW9kdWxlIGVjaGFydHMvc2NhbGUvSW50ZXJ2YWxcbiAqL1xudmFyIHJvdW5kTnVtYmVyID0gbnVtYmVyVXRpbC5yb3VuZDtcbi8qKlxuICogQGFsaWFzIG1vZHVsZTplY2hhcnRzL2Nvb3JkL3NjYWxlL0ludGVydmFsXG4gKiBAY29uc3RydWN0b3JcbiAqL1xuXG52YXIgSW50ZXJ2YWxTY2FsZSA9IFNjYWxlLmV4dGVuZCh7XG4gIHR5cGU6ICdpbnRlcnZhbCcsXG4gIF9pbnRlcnZhbDogMCxcbiAgX2ludGVydmFsUHJlY2lzaW9uOiAyLFxuICBzZXRFeHRlbnQ6IGZ1bmN0aW9uIChzdGFydCwgZW5kKSB7XG4gICAgdmFyIHRoaXNFeHRlbnQgPSB0aGlzLl9leHRlbnQ7IC8vc3RhcnQsZW5kIG1heSBiZSBhIE51bWJlciBsaWtlICcyNScsc28uLi5cblxuICAgIGlmICghaXNOYU4oc3RhcnQpKSB7XG4gICAgICB0aGlzRXh0ZW50WzBdID0gcGFyc2VGbG9hdChzdGFydCk7XG4gICAgfVxuXG4gICAgaWYgKCFpc05hTihlbmQpKSB7XG4gICAgICB0aGlzRXh0ZW50WzFdID0gcGFyc2VGbG9hdChlbmQpO1xuICAgIH1cbiAgfSxcbiAgdW5pb25FeHRlbnQ6IGZ1bmN0aW9uIChvdGhlcikge1xuICAgIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7XG4gICAgb3RoZXJbMF0gPCBleHRlbnRbMF0gJiYgKGV4dGVudFswXSA9IG90aGVyWzBdKTtcbiAgICBvdGhlclsxXSA+IGV4dGVudFsxXSAmJiAoZXh0ZW50WzFdID0gb3RoZXJbMV0pOyAvLyB1bmlvbkV4dGVudCBtYXkgY2FsbGVkIGJ5IGl0J3Mgc3ViIGNsYXNzZXNcblxuICAgIEludGVydmFsU2NhbGUucHJvdG90eXBlLnNldEV4dGVudC5jYWxsKHRoaXMsIGV4dGVudFswXSwgZXh0ZW50WzFdKTtcbiAgfSxcblxuICAvKipcbiAgICogR2V0IGludGVydmFsXG4gICAqL1xuICBnZXRJbnRlcnZhbDogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLl9pbnRlcnZhbDtcbiAgfSxcblxuICAvKipcbiAgICogU2V0IGludGVydmFsXG4gICAqL1xuICBzZXRJbnRlcnZhbDogZnVuY3Rpb24gKGludGVydmFsKSB7XG4gICAgdGhpcy5faW50ZXJ2YWwgPSBpbnRlcnZhbDsgLy8gRHJvcHBlZCBhdXRvIGNhbGN1bGF0ZWQgbmljZUV4dGVudCBhbmQgdXNlIHVzZXIgc2V0dGVkIGV4dGVudFxuICAgIC8vIFdlIGFzc3VtZSB1c2VyIHdhbid0IHRvIHNldCBib3RoIGludGVydmFsLCBtaW4sIG1heCB0byBnZXQgYSBiZXR0ZXIgcmVzdWx0XG5cbiAgICB0aGlzLl9uaWNlRXh0ZW50ID0gdGhpcy5fZXh0ZW50LnNsaWNlKCk7XG4gICAgdGhpcy5faW50ZXJ2YWxQcmVjaXNpb24gPSBoZWxwZXIuZ2V0SW50ZXJ2YWxQcmVjaXNpb24oaW50ZXJ2YWwpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPn1cbiAgICovXG4gIGdldFRpY2tzOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGhlbHBlci5pbnRlcnZhbFNjYWxlR2V0VGlja3ModGhpcy5faW50ZXJ2YWwsIHRoaXMuX2V4dGVudCwgdGhpcy5fbmljZUV4dGVudCwgdGhpcy5faW50ZXJ2YWxQcmVjaXNpb24pO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge251bWJlcn0gZGF0YVxuICAgKiBAcGFyYW0ge09iamVjdH0gW29wdF1cbiAgICogQHBhcmFtIHtudW1iZXJ8c3RyaW5nfSBbb3B0LnByZWNpc2lvbl0gSWYgJ2F1dG8nLCB1c2UgbmljZSBwcmVzaXNpb24uXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdC5wYWRdIHJldHVybnMgMS41MCBidXQgbm90IDEuNSBpZiBwcmVjaXNpb24gaXMgMi5cbiAgICogQHJldHVybiB7c3RyaW5nfVxuICAgKi9cbiAgZ2V0TGFiZWw6IGZ1bmN0aW9uIChkYXRhLCBvcHQpIHtcbiAgICBpZiAoZGF0YSA9PSBudWxsKSB7XG4gICAgICByZXR1cm4gJyc7XG4gICAgfVxuXG4gICAgdmFyIHByZWNpc2lvbiA9IG9wdCAmJiBvcHQucHJlY2lzaW9uO1xuXG4gICAgaWYgKHByZWNpc2lvbiA9PSBudWxsKSB7XG4gICAgICBwcmVjaXNpb24gPSBudW1iZXJVdGlsLmdldFByZWNpc2lvblNhZmUoZGF0YSkgfHwgMDtcbiAgICB9IGVsc2UgaWYgKHByZWNpc2lvbiA9PT0gJ2F1dG8nKSB7XG4gICAgICAvLyBTaG91bGQgYmUgbW9yZSBwcmVjaXNlIHRoZW4gdGljay5cbiAgICAgIHByZWNpc2lvbiA9IHRoaXMuX2ludGVydmFsUHJlY2lzaW9uO1xuICAgIH0gLy8gKDEpIElmIGBwcmVjaXNpb25gIGlzIHNldCwgMTIuMDA1IHNob3VsZCBiZSBkaXNwbGF5IGFzICcxMi4wMDUwMCcuXG4gICAgLy8gKDIpIFVzZSByb3VuZE51bWJlciAodG9GaXhlZCkgdG8gYXZvaWQgc2NpZW50aWZpYyBub3RhdGlvbiBsaWtlICczLjVlLTcnLlxuXG5cbiAgICBkYXRhID0gcm91bmROdW1iZXIoZGF0YSwgcHJlY2lzaW9uLCB0cnVlKTtcbiAgICByZXR1cm4gZm9ybWF0VXRpbC5hZGRDb21tYXMoZGF0YSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBpbnRlcnZhbCBhbmQgZXh0ZW50IG9mIGludGVydmFscyBmb3IgbmljZSB0aWNrc1xuICAgKlxuICAgKiBAcGFyYW0ge251bWJlcn0gW3NwbGl0TnVtYmVyID0gNV0gRGVzaXJlZCBudW1iZXIgb2YgdGlja3NcbiAgICogQHBhcmFtIHtudW1iZXJ9IFttaW5JbnRlcnZhbF1cbiAgICogQHBhcmFtIHtudW1iZXJ9IFttYXhJbnRlcnZhbF1cbiAgICovXG4gIG5pY2VUaWNrczogZnVuY3Rpb24gKHNwbGl0TnVtYmVyLCBtaW5JbnRlcnZhbCwgbWF4SW50ZXJ2YWwpIHtcbiAgICBzcGxpdE51bWJlciA9IHNwbGl0TnVtYmVyIHx8IDU7XG4gICAgdmFyIGV4dGVudCA9IHRoaXMuX2V4dGVudDtcbiAgICB2YXIgc3BhbiA9IGV4dGVudFsxXSAtIGV4dGVudFswXTtcblxuICAgIGlmICghaXNGaW5pdGUoc3BhbikpIHtcbiAgICAgIHJldHVybjtcbiAgICB9IC8vIFVzZXIgbWF5IHNldCBheGlzIG1pbiAwIGFuZCBkYXRhIGFyZSBhbGwgbmVnYXRpdmVcbiAgICAvLyBGSVhNRSBJZiBpdCBuZWVkcyB0byByZXZlcnNlID9cblxuXG4gICAgaWYgKHNwYW4gPCAwKSB7XG4gICAgICBzcGFuID0gLXNwYW47XG4gICAgICBleHRlbnQucmV2ZXJzZSgpO1xuICAgIH1cblxuICAgIHZhciByZXN1bHQgPSBoZWxwZXIuaW50ZXJ2YWxTY2FsZU5pY2VUaWNrcyhleHRlbnQsIHNwbGl0TnVtYmVyLCBtaW5JbnRlcnZhbCwgbWF4SW50ZXJ2YWwpO1xuICAgIHRoaXMuX2ludGVydmFsUHJlY2lzaW9uID0gcmVzdWx0LmludGVydmFsUHJlY2lzaW9uO1xuICAgIHRoaXMuX2ludGVydmFsID0gcmVzdWx0LmludGVydmFsO1xuICAgIHRoaXMuX25pY2VFeHRlbnQgPSByZXN1bHQubmljZVRpY2tFeHRlbnQ7XG4gIH0sXG5cbiAgLyoqXG4gICAqIE5pY2UgZXh0ZW50LlxuICAgKiBAcGFyYW0ge09iamVjdH0gb3B0XG4gICAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0LnNwbGl0TnVtYmVyID0gNV0gR2l2ZW4gYXBwcm94IHRpY2sgbnVtYmVyXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdC5maXhNaW49ZmFsc2VdXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdC5maXhNYXg9ZmFsc2VdXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdC5taW5JbnRlcnZhbF1cbiAgICogQHBhcmFtIHtib29sZWFufSBbb3B0Lm1heEludGVydmFsXVxuICAgKi9cbiAgbmljZUV4dGVudDogZnVuY3Rpb24gKG9wdCkge1xuICAgIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7IC8vIElmIGV4dGVudCBzdGFydCBhbmQgZW5kIGFyZSBzYW1lLCBleHBhbmQgdGhlbVxuXG4gICAgaWYgKGV4dGVudFswXSA9PT0gZXh0ZW50WzFdKSB7XG4gICAgICBpZiAoZXh0ZW50WzBdICE9PSAwKSB7XG4gICAgICAgIC8vIEV4cGFuZCBleHRlbnRcbiAgICAgICAgdmFyIGV4cGFuZFNpemUgPSBleHRlbnRbMF07IC8vIEluIHRoZSBmb3dsbG93aW5nIGNhc2VcbiAgICAgICAgLy8gICAgICBBeGlzIGhhcyBiZWVuIGZpeGVkIG1heCAxMDBcbiAgICAgICAgLy8gICAgICBQbHVzIGRhdGEgYXJlIGFsbCAxMDAgYW5kIGF4aXMgZXh0ZW50IGFyZSBbMTAwLCAxMDBdLlxuICAgICAgICAvLyBFeHRlbmQgdG8gdGhlIGJvdGggc2lkZSB3aWxsIGNhdXNlIGV4cGFuZGVkIG1heCBpcyBsYXJnZXIgdGhhbiBmaXhlZCBtYXguXG4gICAgICAgIC8vIFNvIG9ubHkgZXhwYW5kIHRvIHRoZSBzbWFsbGVyIHNpZGUuXG5cbiAgICAgICAgaWYgKCFvcHQuZml4TWF4KSB7XG4gICAgICAgICAgZXh0ZW50WzFdICs9IGV4cGFuZFNpemUgLyAyO1xuICAgICAgICAgIGV4dGVudFswXSAtPSBleHBhbmRTaXplIC8gMjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBleHRlbnRbMF0gLT0gZXhwYW5kU2l6ZSAvIDI7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGV4dGVudFsxXSA9IDE7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIHNwYW4gPSBleHRlbnRbMV0gLSBleHRlbnRbMF07IC8vIElmIHRoZXJlIGFyZSBubyBkYXRhIGFuZCBleHRlbnQgYXJlIFtJbmZpbml0eSwgLUluZmluaXR5XVxuXG4gICAgaWYgKCFpc0Zpbml0ZShzcGFuKSkge1xuICAgICAgZXh0ZW50WzBdID0gMDtcbiAgICAgIGV4dGVudFsxXSA9IDE7XG4gICAgfVxuXG4gICAgdGhpcy5uaWNlVGlja3Mob3B0LnNwbGl0TnVtYmVyLCBvcHQubWluSW50ZXJ2YWwsIG9wdC5tYXhJbnRlcnZhbCk7IC8vIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7XG5cbiAgICB2YXIgaW50ZXJ2YWwgPSB0aGlzLl9pbnRlcnZhbDtcblxuICAgIGlmICghb3B0LmZpeE1pbikge1xuICAgICAgZXh0ZW50WzBdID0gcm91bmROdW1iZXIoTWF0aC5mbG9vcihleHRlbnRbMF0gLyBpbnRlcnZhbCkgKiBpbnRlcnZhbCk7XG4gICAgfVxuXG4gICAgaWYgKCFvcHQuZml4TWF4KSB7XG4gICAgICBleHRlbnRbMV0gPSByb3VuZE51bWJlcihNYXRoLmNlaWwoZXh0ZW50WzFdIC8gaW50ZXJ2YWwpICogaW50ZXJ2YWwpO1xuICAgIH1cbiAgfVxufSk7XG4vKipcbiAqIEByZXR1cm4ge21vZHVsZTplY2hhcnRzL3NjYWxlL1RpbWV9XG4gKi9cblxuSW50ZXJ2YWxTY2FsZS5jcmVhdGUgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiBuZXcgSW50ZXJ2YWxTY2FsZSgpO1xufTtcblxudmFyIF9kZWZhdWx0ID0gSW50ZXJ2YWxTY2FsZTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7OztBQUlBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUExSkE7QUE0SkE7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/scale/Interval.js
