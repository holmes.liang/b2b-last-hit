__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Submit", function() { return Submit; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _desk_quote_SAIC_iar_basic_submit__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @desk/quote/SAIC/iar/basic-submit */ "./src/app/desk/quote/SAIC/iar/basic-submit.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk/quote/SAIC/iar/index */ "./src/app/desk/quote/SAIC/iar/index.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_view_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk/quote/SAIC/iar/view-component */ "./src/app/desk/quote/SAIC/iar/view-component.tsx");
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/spin */ "./src/app/desk/component/spin.tsx");
/* harmony import */ var _desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk/quote/SAIC/phs/components/side-info */ "./src/app/desk/quote/SAIC/phs/components/side-info.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/submit.tsx";












var Submit =
/*#__PURE__*/
function (_BasicSubmit) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Submit, _BasicSubmit);

  function Submit() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Submit);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Submit)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.handleConfirm = function () {
      _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__["default"].spinShow();

      var newPolicy = _this.props.mergePolicyToServiceModel();

      _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].post("/cart/bind", newPolicy).then(function (response) {
        var _ref = response.body || {
          respData: {}
        },
            respData = _ref.respData;

        var cart = respData.cart,
            suspended = respData.suspended,
            paymentStepRequired = respData.paymentStepRequired;

        _this.props.mergePolicyToUIModel(respData.cart || {});

        if (suspended) {
          _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__["default"].spinClose();

          _this.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_9__["PATH"].QUOTE_RESULT.replace(":transId", lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(cart, "transId")).replace(":type", "suspended").replace("/:policyId?", ""));
        } else {
          if (paymentStepRequired) {
            _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__["default"].spinClose();

            _this.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_9__["PATH"].QUOTE_PAYMENT_FOR_CART.replace(":transId", _this.getValueFromModel("transId")));
          } else {
            var transId = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(cart, "transId");

            if (!transId) {
              _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__["default"].spinClose();
              return;
            }

            _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].post("/cart/issue?autoUW=N", {
              transId: transId,
              paymentMethod: lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(cart, "paymentMethod")
            }).then(function (res) {
              _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__["default"].spinClose();

              _this.handelResponse(res, transId);
            }).catch(function () {
              return _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__["default"].spinClose();
            }).finally(function () {});
          }
        }
      }).catch(function () {
        _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__["default"].spinClose();
      });
    };

    _this.handelResponse =
    /*#__PURE__*/
    function () {
      var _ref2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(response, transId) {
        var respData, policy, suspended, success, message;
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                respData = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(response, "body.respData", {});

                if (respData) {
                  _context.next = 3;
                  break;
                }

                return _context.abrupt("return");

              case 3:
                policy = respData.policy, suspended = respData.suspended, success = respData.success, message = respData.message;

                if (success) {
                  _context.next = 7;
                  break;
                }

                _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_14__["default"].create({
                  Component: function Component(_ref3) {
                    var onCancel = _ref3.onCancel,
                        onOk = _ref3.onOk;
                    return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Modal"], {
                      width: "30%",
                      maskClosable: false,
                      visible: true,
                      centered: true,
                      title: null,
                      onCancel: onCancel,
                      onOk: onOk,
                      footer: null,
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 70
                      },
                      __self: this
                    }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
                      style: {
                        fontSize: "16px"
                      },
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 80
                      },
                      __self: this
                    }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Icon"], {
                      style: {
                        color: "red",
                        verticalAlign: "middle",
                        fontSize: "18px",
                        paddingRight: "10px"
                      },
                      type: "close-circle",
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 80
                      },
                      __self: this
                    }), message));
                  },
                  onCancel: function onCancel() {}
                });
                return _context.abrupt("return");

              case 7:
                if (!success) {
                  _context.next = 15;
                  break;
                }

                if (!["PA", "PH"].includes(lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(policy, "productCode", ""))) {
                  _context.next = 13;
                  break;
                }

                _this.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_9__["PATH"].QUOTE_RESULT.replace(":transId", transId).replace(":type", "success").replace(":policyId", lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(policy, "policyId", "")));

                return _context.abrupt("return");

              case 13:
                _this.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_9__["PATH"].QUOTE_RESULT.replace(":transId", transId).replace(":type", "success").replace("/:policyId", ""));

                return _context.abrupt("return");

              case 15:
                if (!suspended) {
                  _context.next = 18;
                  break;
                }

                _this.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_9__["PATH"].QUOTE_RESULT.replace(":transId", transId).replace(":type", "suspended").replace("/:policyId?", ""));

                return _context.abrupt("return");

              case 18:
                antd__WEBPACK_IMPORTED_MODULE_11__["notification"].error({
                  message: message
                });

              case 19:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x, _x2) {
        return _ref2.apply(this, arguments);
      };
    }();

    _this.afterUploadPolicyDoc = function () {
      var newCart = _this.props.mergePolicyToServiceModel();

      _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_9__["Apis"].CART_MERGE, newCart).then(function (response) {
        _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__["default"].spinClose();

        _this.props.mergePolicyToUIModel(response.body.respData || {});

        if (response.body.respCode !== "0000") return;
      }).catch(function (error) {
        console.error(error);
        _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__["default"].spinClose();
      });
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Submit, [{
    key: "renderSideInfo",
    value: function renderSideInfo() {
      var model = this.props.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_16__["default"], {
        lineList: _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_12__["list"],
        premiumId: "cartPremium.totalPremium",
        model: model,
        dataFixed: "policy",
        activeStep: "Submit",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 140
        },
        __self: this
      });
    }
  }, {
    key: "renderViewComponent",
    value: function renderViewComponent() {
      var model = this.props.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_quote_SAIC_iar_view_component__WEBPACK_IMPORTED_MODULE_13__["default"], {
        afterUploadPolicyDoc: this.afterUploadPolicyDoc,
        premiumId: "policyPremium",
        dataFixed: "policy",
        isConfirm: true,
        model: model,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 150
        },
        __self: this
      });
    }
  }]);

  return Submit;
}(_desk_quote_SAIC_iar_basic_submit__WEBPACK_IMPORTED_MODULE_8__["BasicSubmit"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvc3VibWl0LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL1NBSUMvaWFyL3N1Ym1pdC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgQmFzaWNTdWJtaXQgfSBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtc3VibWl0XCI7XG5pbXBvcnQgeyBBamF4UmVzcG9uc2UgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBBamF4LCBBcGlzLCBMYW5ndWFnZSwgUEFUSCB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBJY29uLCBNb2RhbCwgbm90aWZpY2F0aW9uIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IGxpc3QgfSBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9pYXIvaW5kZXhcIjtcbmltcG9ydCBWaWV3Q29tcG9uZW50IGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL2lhci92aWV3LWNvbXBvbmVudFwiO1xuaW1wb3J0IE1hc2sgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9jcmVhdGUtZGlhbG9nXCI7XG5pbXBvcnQgU3BpbiBmcm9tIFwiQGRlc2stY29tcG9uZW50L3NwaW5cIjtcbmltcG9ydCBTaWRlSW5mb1BocyBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9waHMvY29tcG9uZW50cy9zaWRlLWluZm9cIjtcbmltcG9ydCBzcGluIGZyb20gXCJAZGVzay1jb21wb25lbnQvc3BpblwiO1xuXG5jbGFzcyBTdWJtaXQgZXh0ZW5kcyBCYXNpY1N1Ym1pdDxhbnksIGFueSwgYW55PiB7XG5cbiAgaGFuZGxlQ29uZmlybSA9ICgpOiB2b2lkID0+IHtcbiAgICBTcGluLnNwaW5TaG93KCk7XG4gICAgY29uc3QgbmV3UG9saWN5ID0gdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvU2VydmljZU1vZGVsKCk7XG4gICAgQWpheC5wb3N0KFwiL2NhcnQvYmluZFwiLCBuZXdQb2xpY3kpXG4gICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5IHx8IHsgcmVzcERhdGE6IHt9IH07XG4gICAgICAgIGNvbnN0IHsgY2FydCwgc3VzcGVuZGVkLCBwYXltZW50U3RlcFJlcXVpcmVkIH0gPSByZXNwRGF0YTtcbiAgICAgICAgdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvVUlNb2RlbChyZXNwRGF0YS5jYXJ0IHx8IHt9KTtcbiAgICAgICAgaWYgKHN1c3BlbmRlZCkge1xuICAgICAgICAgIFNwaW4uc3BpbkNsb3NlKCk7XG4gICAgICAgICAgdGhpcy5nZXRIZWxwZXJzKCkuZ2V0Um91dGVyKCkucHVzaFJlZGlyZWN0KFxuICAgICAgICAgICAgUEFUSC5RVU9URV9SRVNVTFQucmVwbGFjZShcIjp0cmFuc0lkXCIsIF8uZ2V0KGNhcnQsIFwidHJhbnNJZFwiKSlcbiAgICAgICAgICAgICAgLnJlcGxhY2UoXCI6dHlwZVwiLCBcInN1c3BlbmRlZFwiKVxuICAgICAgICAgICAgICAucmVwbGFjZShcIi86cG9saWN5SWQ/XCIsIFwiXCIpLFxuICAgICAgICAgICk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaWYgKHBheW1lbnRTdGVwUmVxdWlyZWQpIHtcbiAgICAgICAgICAgIFNwaW4uc3BpbkNsb3NlKCk7XG4gICAgICAgICAgICB0aGlzLmdldEhlbHBlcnMoKS5nZXRSb3V0ZXIoKS5wdXNoUmVkaXJlY3QoXG4gICAgICAgICAgICAgIFBBVEguUVVPVEVfUEFZTUVOVF9GT1JfQ0FSVC5yZXBsYWNlKFwiOnRyYW5zSWRcIixcbiAgICAgICAgICAgICAgICB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwidHJhbnNJZFwiKSxcbiAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbnN0IHRyYW5zSWQgPSBfLmdldChjYXJ0LCBcInRyYW5zSWRcIik7XG4gICAgICAgICAgICBpZiAoIXRyYW5zSWQpIHtcbiAgICAgICAgICAgICAgU3Bpbi5zcGluQ2xvc2UoKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgQWpheC5wb3N0KFwiL2NhcnQvaXNzdWU/YXV0b1VXPU5cIiwge1xuICAgICAgICAgICAgICB0cmFuc0lkLFxuICAgICAgICAgICAgICBwYXltZW50TWV0aG9kOiBfLmdldChjYXJ0LCBcInBheW1lbnRNZXRob2RcIiksXG4gICAgICAgICAgICB9KS50aGVuKChyZXM6IGFueSkgPT4ge1xuICAgICAgICAgICAgICBTcGluLnNwaW5DbG9zZSgpO1xuICAgICAgICAgICAgICB0aGlzLmhhbmRlbFJlc3BvbnNlKHJlcywgdHJhbnNJZCk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAuY2F0Y2goKCkgPT4gU3Bpbi5zcGluQ2xvc2UoKSlcbiAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgICAuY2F0Y2goKCkgPT4ge1xuICAgICAgICBTcGluLnNwaW5DbG9zZSgpO1xuICAgICAgfSk7XG4gIH07XG5cbiAgaGFuZGVsUmVzcG9uc2UgPSBhc3luYyAocmVzcG9uc2U6IGFueSwgdHJhbnNJZDogYW55KSA9PiB7XG4gICAgY29uc3QgcmVzcERhdGEgPSBfLmdldChyZXNwb25zZSwgXCJib2R5LnJlc3BEYXRhXCIsIHt9KTtcbiAgICBpZiAoIXJlc3BEYXRhKSByZXR1cm47XG4gICAgY29uc3QgeyBwb2xpY3ksIHN1c3BlbmRlZCwgc3VjY2VzcywgbWVzc2FnZSB9ID0gcmVzcERhdGE7XG4gICAgaWYgKCFzdWNjZXNzKSB7XG4gICAgICBNYXNrLmNyZWF0ZSh7XG4gICAgICAgIENvbXBvbmVudDogKHsgb25DYW5jZWwsIG9uT2sgfTogYW55KSA9PiAoXG4gICAgICAgICAgPE1vZGFsXG4gICAgICAgICAgICB3aWR0aD17XCIzMCVcIn1cbiAgICAgICAgICAgIG1hc2tDbG9zYWJsZT17ZmFsc2V9XG4gICAgICAgICAgICB2aXNpYmxlPXt0cnVlfVxuICAgICAgICAgICAgY2VudGVyZWQ9e3RydWV9XG4gICAgICAgICAgICB0aXRsZT17bnVsbH1cbiAgICAgICAgICAgIG9uQ2FuY2VsPXtvbkNhbmNlbH1cbiAgICAgICAgICAgIG9uT2s9e29uT2t9XG4gICAgICAgICAgICBmb290ZXI9e251bGx9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBmb250U2l6ZTogXCIxNnB4XCIgfX0+PEljb24gc3R5bGU9e3tcbiAgICAgICAgICAgICAgY29sb3I6IFwicmVkXCIsXG4gICAgICAgICAgICAgIHZlcnRpY2FsQWxpZ246IFwibWlkZGxlXCIsXG4gICAgICAgICAgICAgIGZvbnRTaXplOiBcIjE4cHhcIixcbiAgICAgICAgICAgICAgcGFkZGluZ1JpZ2h0OiBcIjEwcHhcIixcbiAgICAgICAgICAgIH19IHR5cGU9e1wiY2xvc2UtY2lyY2xlXCJ9Lz57bWVzc2FnZX08L2Rpdj5cbiAgICAgICAgICA8L01vZGFsPlxuICAgICAgICApLFxuICAgICAgICBvbkNhbmNlbDogKCkgPT4ge1xuICAgICAgICB9LFxuICAgICAgfSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmIChzdWNjZXNzKSB7XG4gICAgICBpZiAoW1wiUEFcIiwgXCJQSFwiXS5pbmNsdWRlcyhfLmdldChwb2xpY3ksIFwicHJvZHVjdENvZGVcIiwgXCJcIikpKSB7XG4gICAgICAgIHRoaXMuZ2V0SGVscGVycygpLmdldFJvdXRlcigpLnB1c2hSZWRpcmVjdChcbiAgICAgICAgICBQQVRILlFVT1RFX1JFU1VMVC5yZXBsYWNlKFwiOnRyYW5zSWRcIiwgdHJhbnNJZClcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOnR5cGVcIiwgXCJzdWNjZXNzXCIpXG4gICAgICAgICAgICAucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBfLmdldChwb2xpY3ksIFwicG9saWN5SWRcIiwgXCJcIikpLFxuICAgICAgICApO1xuICAgICAgICByZXR1cm47XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmdldEhlbHBlcnMoKS5nZXRSb3V0ZXIoKS5wdXNoUmVkaXJlY3QoXG4gICAgICAgICAgUEFUSC5RVU9URV9SRVNVTFQucmVwbGFjZShcIjp0cmFuc0lkXCIsIHRyYW5zSWQpXG4gICAgICAgICAgICAucmVwbGFjZShcIjp0eXBlXCIsIFwic3VjY2Vzc1wiKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCIvOnBvbGljeUlkXCIsIFwiXCIpLFxuICAgICAgICApO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgfVxuICAgIGlmIChzdXNwZW5kZWQpIHtcbiAgICAgIHRoaXMuZ2V0SGVscGVycygpLmdldFJvdXRlcigpLnB1c2hSZWRpcmVjdChcbiAgICAgICAgUEFUSC5RVU9URV9SRVNVTFQucmVwbGFjZShcIjp0cmFuc0lkXCIsIHRyYW5zSWQpXG4gICAgICAgICAgLnJlcGxhY2UoXCI6dHlwZVwiLCBcInN1c3BlbmRlZFwiKVxuICAgICAgICAgIC5yZXBsYWNlKFwiLzpwb2xpY3lJZD9cIiwgXCJcIiksXG4gICAgICApO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIG5vdGlmaWNhdGlvbi5lcnJvcih7IG1lc3NhZ2U6IG1lc3NhZ2UgfSk7XG4gIH07XG5cbiAgYWZ0ZXJVcGxvYWRQb2xpY3lEb2MgPSAoKSA9PiB7XG4gICAgY29uc3QgbmV3Q2FydCA9IHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbCgpO1xuXG4gICAgQWpheC5wb3N0KEFwaXMuQ0FSVF9NRVJHRSwgbmV3Q2FydClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgc3Bpbi5zcGluQ2xvc2UoKTtcbiAgICAgICAgdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvVUlNb2RlbChyZXNwb25zZS5ib2R5LnJlc3BEYXRhIHx8IHt9KTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkucmVzcENvZGUgIT09IFwiMDAwMFwiKSByZXR1cm47XG4gICAgICB9KVxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICAgIHNwaW4uc3BpbkNsb3NlKCk7XG4gICAgICB9KTtcbiAgfTtcblxuXG4gIHJlbmRlclNpZGVJbmZvKCk6IGFueSB7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPFNpZGVJbmZvUGhzXG4gICAgICBsaW5lTGlzdD17bGlzdH1cbiAgICAgIHByZW1pdW1JZD17XCJjYXJ0UHJlbWl1bS50b3RhbFByZW1pdW1cIn1cbiAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgIGRhdGFGaXhlZD17XCJwb2xpY3lcIn1cbiAgICAgIGFjdGl2ZVN0ZXA9e1wiU3VibWl0XCJ9Lz47XG4gIH1cblxuICByZW5kZXJWaWV3Q29tcG9uZW50KCk6IGFueSB7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPFZpZXdDb21wb25lbnQgYWZ0ZXJVcGxvYWRQb2xpY3lEb2M9e3RoaXMuYWZ0ZXJVcGxvYWRQb2xpY3lEb2N9IHByZW1pdW1JZD17XCJwb2xpY3lQcmVtaXVtXCJ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFGaXhlZD17XCJwb2xpY3lcIn0gaXNDb25maXJtPXt0cnVlfSBtb2RlbD17bW9kZWx9Lz47XG4gIH1cbn1cblxuZXhwb3J0IHsgU3VibWl0IH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFEQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFLQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVhBO0FBbUJBO0FBcEJBO0FBTEE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUErQkE7QUFDQTtBQWpDQTtBQUNBO0FBREE7QUF1Q0E7QUFDQTtBQXhDQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBK0NBO0FBQ0E7QUFqREE7QUFDQTtBQURBO0FBd0RBO0FBQUE7QUFBQTtBQUNBO0FBekRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7Ozs7OztBQTBEQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTs7O0FBRUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBOzs7O0FBMUlBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/submit.tsx
