/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule isSelectionAtLeafStart
 * @format
 * 
 */


function isSelectionAtLeafStart(editorState) {
  var selection = editorState.getSelection();
  var anchorKey = selection.getAnchorKey();
  var blockTree = editorState.getBlockTree(anchorKey);
  var offset = selection.getStartOffset();
  var isAtStart = false;
  blockTree.some(function (leafSet) {
    if (offset === leafSet.get('start')) {
      isAtStart = true;
      return true;
    }

    if (offset < leafSet.get('end')) {
      return leafSet.get('leaves').some(function (leaf) {
        var leafStart = leaf.get('start');

        if (offset === leafStart) {
          isAtStart = true;
          return true;
        }

        return false;
      });
    }

    return false;
  });
  return isAtStart;
}

module.exports = isSelectionAtLeafStart;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2lzU2VsZWN0aW9uQXRMZWFmU3RhcnQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvaXNTZWxlY3Rpb25BdExlYWZTdGFydC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGlzU2VsZWN0aW9uQXRMZWFmU3RhcnRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gaXNTZWxlY3Rpb25BdExlYWZTdGFydChlZGl0b3JTdGF0ZSkge1xuICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gIHZhciBhbmNob3JLZXkgPSBzZWxlY3Rpb24uZ2V0QW5jaG9yS2V5KCk7XG4gIHZhciBibG9ja1RyZWUgPSBlZGl0b3JTdGF0ZS5nZXRCbG9ja1RyZWUoYW5jaG9yS2V5KTtcbiAgdmFyIG9mZnNldCA9IHNlbGVjdGlvbi5nZXRTdGFydE9mZnNldCgpO1xuXG4gIHZhciBpc0F0U3RhcnQgPSBmYWxzZTtcblxuICBibG9ja1RyZWUuc29tZShmdW5jdGlvbiAobGVhZlNldCkge1xuICAgIGlmIChvZmZzZXQgPT09IGxlYWZTZXQuZ2V0KCdzdGFydCcpKSB7XG4gICAgICBpc0F0U3RhcnQgPSB0cnVlO1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgaWYgKG9mZnNldCA8IGxlYWZTZXQuZ2V0KCdlbmQnKSkge1xuICAgICAgcmV0dXJuIGxlYWZTZXQuZ2V0KCdsZWF2ZXMnKS5zb21lKGZ1bmN0aW9uIChsZWFmKSB7XG4gICAgICAgIHZhciBsZWFmU3RhcnQgPSBsZWFmLmdldCgnc3RhcnQnKTtcbiAgICAgICAgaWYgKG9mZnNldCA9PT0gbGVhZlN0YXJ0KSB7XG4gICAgICAgICAgaXNBdFN0YXJ0ID0gdHJ1ZTtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIHJldHVybiBmYWxzZTtcbiAgfSk7XG5cbiAgcmV0dXJuIGlzQXRTdGFydDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc1NlbGVjdGlvbkF0TGVhZlN0YXJ0OyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/isSelectionAtLeafStart.js
