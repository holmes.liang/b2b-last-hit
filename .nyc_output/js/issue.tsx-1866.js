__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formDateLayout", function() { return formDateLayout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Issue", function() { return Issue; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_consts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/consts */ "./src/common/consts.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/issue.tsx";





var formDateLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 9
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 6
    }
  }
};

var Issue =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(Issue, _QuoteStep);

  function Issue() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Issue);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Issue)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.policy = void 0;
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Issue, [{
    key: "initState",
    value: function initState() {
      return {
        pdfList: [],
        checked: false
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getPdf(this.getValueFromModel("policy.policyId"));
    }
  }, {
    key: "getModel",
    value: function getModel() {
      return this.policy || this.props.model || {};
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      this.policy = nextProps.model;
      this.getPdf(this.getValueFromModel("policy.policyId"));
    }
  }, {
    key: "getPdf",
    value: function getPdf(policyId) {
      var _this2 = this;

      if (!policyId) return;
      this.getHelpers().getAjax().get(_common__WEBPACK_IMPORTED_MODULE_8__["Apis"].POLICY_OUTPUTS.replace(":policyId", policyId), {}, {}).then(function (response) {
        var respData = response.body.respData;

        _this2.setState({
          pdfList: respData
        });
      }).catch(function (error) {});
    }
  }, {
    key: "renderActions",
    value: function renderActions() {
      return null;
    }
  }, {
    key: "renderPdfList",
    value: function renderPdfList() {
      var pdfList = this.state.pdfList;
      var policyId = this.getValueFromModel("policy.policyId");
      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        style: {
          display: "inline-flex"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, (pdfList || []).map(function (pdf) {
        var displayName = pdf.displayName,
            name = pdf.name,
            fileName = pdf.fileName;
        var pdfUrl = _common__WEBPACK_IMPORTED_MODULE_8__["Ajax"].appendAuthToUrl("/policies/".concat(policyId, "/pdf/").concat(name));
        return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("a", {
          href: pdfUrl,
          className: "pdf-link-item",
          key: fileName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 75
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Icon"], {
          type: "file-pdf",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 76
          },
          __self: this
        }), "".concat(displayName));
      }));
    }
  }, {
    key: "renderIssueSuccess",
    value: function renderIssueSuccess() {
      var _this3 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        className: "congratulation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Icon"], {
        type: "check-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        className: "paragraph paragraph--main",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_8__["Language"].en("Thank you for insuring with us.").thai("ขอบคุณที่ทำประกันกับเรา.").my("ကြှနျုပျတို့နှငျ့အတူ insuring အတွက်ကျေးဇူးတင်ပါသည်.").getMessage(), _common__WEBPACK_IMPORTED_MODULE_8__["Language"].en("Your policy has been issued, policy number").thai("กรมธรรม์ของท่านได้รับการอนุมัติเรียบร้อยแล้ว หมายเลขกรมธรรมของท่านคือ ").my("သင့်ရဲ့မူဝါဒ, မူဝါဒသည်အရေအတွက်ကိုထုတ်ပေးလျက်ရှိသည် ").getMessage(), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("a", {
        className: "policy-no-link",
        onClick: function onClick() {
          _this3.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_8__["PATH"].POLICIES_QUERY);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, this.getValueFromModel("policyNo"))), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        className: "paragraph",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        className: "pdf-link-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112
        },
        __self: this
      }, this.renderPdfList())));
    }
  }, {
    key: "renderIssueFail",
    value: function renderIssueFail() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        className: "congratulation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 121
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Icon"], {
        type: "close-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        className: "paragraph paragraph--main",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124
        },
        __self: this
      }, "We confirmed that your payment was received, your policy is processing.You will be informed once policy is issued."));
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var quoteStatus = this.getValueFromModel("policy.status");
      var endoIssued = this.getValueFromModel("policy.endoIssued");
      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        className: "result-page",
        style: {
          paddingBottom: "20px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137
        },
        __self: this
      }, quoteStatus === _common_consts__WEBPACK_IMPORTED_MODULE_9__["default"].QUOTE_STATUS.ISSUED || endoIssued ? this.renderIssueSuccess() : this.renderIssueFail());
    }
  }, {
    key: "renderTitle",
    value: function renderTitle() {
      return null;
    }
  }]);

  return Issue;
}(_quote_step__WEBPACK_IMPORTED_MODULE_6__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvaXNzdWUudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvaXNzdWUudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgUXVvdGVTdGVwLCB7IFN0ZXBDb21wb25lbnRzLCBTdGVwUHJvcHMgfSBmcm9tIFwiLi4vLi4vcXVvdGUtc3RlcFwiO1xuaW1wb3J0IHsgSWNvbiB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBBamF4LCBBcGlzLCBMYW5ndWFnZSwgUEFUSCwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IENvbnN0cyBmcm9tIFwiQGNvbW1vbi9jb25zdHNcIjtcblxudHlwZSBJc3N1ZVN0YXRlID0ge1xuICBwZGZMaXN0OiBhbnk7XG4gIGNoZWNrZWQ6IGJvb2xlYW47XG59O1xuZXhwb3J0IGNvbnN0IGZvcm1EYXRlTGF5b3V0ID0ge1xuICBsYWJlbENvbDoge1xuICAgIHhzOiB7IHNwYW46IDggfSxcbiAgICBzbTogeyBzcGFuOiA5IH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxNiB9LFxuICAgIHNtOiB7IHNwYW46IDYgfSxcbiAgfSxcbn07XG5cbmNsYXNzIElzc3VlPFAgZXh0ZW5kcyBTdGVwUHJvcHMsIFMgZXh0ZW5kcyBJc3N1ZVN0YXRlLCBDIGV4dGVuZHMgU3RlcENvbXBvbmVudHM+IGV4dGVuZHMgUXVvdGVTdGVwPFAsIFMsIEM+IHtcbiAgcHJvdGVjdGVkIHBvbGljeTogYW55O1xuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHBkZkxpc3Q6IFtdLFxuICAgICAgY2hlY2tlZDogZmFsc2UsXG4gICAgfSBhcyBTO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdGhpcy5nZXRQZGYodGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeS5wb2xpY3lJZFwiKSk7XG4gIH1cblxuICBnZXRNb2RlbCgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLnBvbGljeSB8fCB0aGlzLnByb3BzLm1vZGVsIHx8IHt9O1xuICB9XG5cbiAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHM6IGFueSkge1xuICAgIHRoaXMucG9saWN5ID0gbmV4dFByb3BzLm1vZGVsO1xuXG4gICAgdGhpcy5nZXRQZGYodGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeS5wb2xpY3lJZFwiKSk7XG4gIH1cblxuICBwcml2YXRlIGdldFBkZihwb2xpY3lJZDogc3RyaW5nKSB7XG4gICAgaWYgKCFwb2xpY3lJZCkgcmV0dXJuO1xuICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAuZ2V0QWpheCgpXG4gICAgICAuZ2V0KEFwaXMuUE9MSUNZX09VVFBVVFMucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBwb2xpY3lJZCksIHt9LCB7fSlcbiAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGNvbnN0IHsgcmVzcERhdGEgfSA9IHJlc3BvbnNlLmJvZHk7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIHBkZkxpc3Q6IHJlc3BEYXRhLFxuICAgICAgICB9KTtcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgfSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyQWN0aW9ucygpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVyUGRmTGlzdCgpIHtcbiAgICBjb25zdCB7IHBkZkxpc3QgfSA9IHRoaXMuc3RhdGU7XG4gICAgbGV0IHBvbGljeUlkID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeS5wb2xpY3lJZFwiKTtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBzdHlsZT17eyBkaXNwbGF5OiBcImlubGluZS1mbGV4XCIgfX0+XG4gICAgICAgIHsocGRmTGlzdCB8fCBbXSkubWFwKChwZGY6IGFueSkgPT4ge1xuICAgICAgICAgIGNvbnN0IHsgZGlzcGxheU5hbWUsIG5hbWUsIGZpbGVOYW1lIH0gPSBwZGY7XG4gICAgICAgICAgY29uc3QgcGRmVXJsID0gQWpheC5hcHBlbmRBdXRoVG9VcmwoYC9wb2xpY2llcy8ke3BvbGljeUlkfS9wZGYvJHtuYW1lfWApO1xuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8YSBocmVmPXtwZGZVcmx9IGNsYXNzTmFtZT1cInBkZi1saW5rLWl0ZW1cIiBrZXk9e2ZpbGVOYW1lfT5cbiAgICAgICAgICAgICAgPEljb24gdHlwZT1cImZpbGUtcGRmXCIvPlxuICAgICAgICAgICAgICB7YCR7ZGlzcGxheU5hbWV9YH1cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICApO1xuICAgICAgICB9KX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICBwcml2YXRlIHJlbmRlcklzc3VlU3VjY2VzcygpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb25ncmF0dWxhdGlvblwiPlxuICAgICAgICAgIDxJY29uIHR5cGU9XCJjaGVjay1jaXJjbGVcIi8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBhcmFncmFwaCBwYXJhZ3JhcGgtLW1haW5cIj5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJUaGFuayB5b3UgZm9yIGluc3VyaW5nIHdpdGggdXMuXCIpXG4gICAgICAgICAgICAudGhhaShcIuC4guC4reC4muC4hOC4uOC4k+C4l+C4teC5iOC4l+C4s+C4m+C4o+C4sOC4geC4seC4meC4geC4seC4muC5gOC4o+C4si5cIilcbiAgICAgICAgICAgIC5teShcIuGAgOGAvOGAvuGAlOGAu+GAr+GAleGAu+GAkOGAreGAr+GAt+GAlOGAvuGAhOGAu+GAt+GAoeGAkOGAsCBpbnN1cmluZyDhgKHhgJDhgL3hgIDhgLrhgIDhgLvhgLHhgLjhgIfhgLDhgLjhgJDhgIThgLrhgJXhgKvhgJ7hgIrhgLouXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIllvdXIgcG9saWN5IGhhcyBiZWVuIGlzc3VlZCwgcG9saWN5IG51bWJlclwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguIHguKPguKHguJjguKPguKPguKHguYzguILguK3guIfguJfguYjguLLguJnguYTguJTguYnguKPguLHguJrguIHguLLguKPguK3guJnguLjguKHguLHguJXguLTguYDguKPguLXguKLguJrguKPguYnguK3guKLguYHguKXguYnguKcg4Lir4Lih4Liy4Lii4LmA4Lil4LiC4LiB4Lij4Lih4LiY4Lij4Lij4Lih4LiC4Lit4LiH4LiX4LmI4Liy4LiZ4LiE4Li34LitIFwiKVxuICAgICAgICAgICAgLm15KFwi4YCe4YCE4YC64YC34YCb4YCy4YC34YCZ4YCw4YCd4YCr4YCSLCDhgJnhgLDhgJ3hgKvhgJLhgJ7hgIrhgLrhgKHhgJvhgLHhgKHhgJDhgL3hgIDhgLrhgIDhgK3hgK/hgJHhgK/hgJDhgLrhgJXhgLHhgLjhgJzhgLvhgIDhgLrhgJvhgL7hgK3hgJ7hgIrhgLogXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIDxhXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJwb2xpY3ktbm8tbGlua1wiXG4gICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAgICAgLmdldFJvdXRlcigpXG4gICAgICAgICAgICAgICAgLnB1c2hSZWRpcmVjdChQQVRILlBPTElDSUVTX1FVRVJZKTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgPlxuICAgICAgICAgICAge3RoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJwb2xpY3lOb1wiKX1cbiAgICAgICAgICA8L2E+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBhcmFncmFwaFwiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGRmLWxpbmstbGlzdFwiPnt0aGlzLnJlbmRlclBkZkxpc3QoKX08L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcHJpdmF0ZSByZW5kZXJJc3N1ZUZhaWwoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDw+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29uZ3JhdHVsYXRpb25cIj5cbiAgICAgICAgICA8SWNvbiB0eXBlPVwiY2xvc2UtY2lyY2xlXCIvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwYXJhZ3JhcGggcGFyYWdyYXBoLS1tYWluXCI+XG4gICAgICAgICAgV2UgY29uZmlybWVkIHRoYXQgeW91ciBwYXltZW50IHdhcyByZWNlaXZlZCwgeW91ciBwb2xpY3kgaXMgcHJvY2Vzc2luZy5Zb3Ugd2lsbCBiZSBpbmZvcm1lZCBvbmNlIHBvbGljeSBpc1xuICAgICAgICAgIGlzc3VlZC5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8Lz5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlckNvbnRlbnQoKSB7XG4gICAgY29uc3QgcXVvdGVTdGF0dXMgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicG9saWN5LnN0YXR1c1wiKTtcbiAgICBjb25zdCBlbmRvSXNzdWVkID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeS5lbmRvSXNzdWVkXCIpO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmVzdWx0LXBhZ2VcIiBzdHlsZT17eyBwYWRkaW5nQm90dG9tOiBcIjIwcHhcIiB9fT5cbiAgICAgICAge3F1b3RlU3RhdHVzID09PSBDb25zdHMuUVVPVEVfU1RBVFVTLklTU1VFRCB8fCBlbmRvSXNzdWVkID8gdGhpcy5yZW5kZXJJc3N1ZVN1Y2Nlc3MoKSA6IHRoaXMucmVuZGVySXNzdWVGYWlsKCl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlclRpdGxlKCkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG59XG5cbmV4cG9ydCB7IElzc3VlIH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBTUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBQ0E7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTs7Ozs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUVBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFHQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBR0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQUNBO0FBR0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7OztBQUVBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTs7O0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7Ozs7QUExSEE7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/issue.tsx
