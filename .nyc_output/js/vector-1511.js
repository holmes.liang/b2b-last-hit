var ArrayCtor = typeof Float32Array === 'undefined' ? Array : Float32Array;
/**
 * 创建一个向量
 * @param {number} [x=0]
 * @param {number} [y=0]
 * @return {Vector2}
 */

function create(x, y) {
  var out = new ArrayCtor(2);

  if (x == null) {
    x = 0;
  }

  if (y == null) {
    y = 0;
  }

  out[0] = x;
  out[1] = y;
  return out;
}
/**
 * 复制向量数据
 * @param {Vector2} out
 * @param {Vector2} v
 * @return {Vector2}
 */


function copy(out, v) {
  out[0] = v[0];
  out[1] = v[1];
  return out;
}
/**
 * 克隆一个向量
 * @param {Vector2} v
 * @return {Vector2}
 */


function clone(v) {
  var out = new ArrayCtor(2);
  out[0] = v[0];
  out[1] = v[1];
  return out;
}
/**
 * 设置向量的两个项
 * @param {Vector2} out
 * @param {number} a
 * @param {number} b
 * @return {Vector2} 结果
 */


function set(out, a, b) {
  out[0] = a;
  out[1] = b;
  return out;
}
/**
 * 向量相加
 * @param {Vector2} out
 * @param {Vector2} v1
 * @param {Vector2} v2
 */


function add(out, v1, v2) {
  out[0] = v1[0] + v2[0];
  out[1] = v1[1] + v2[1];
  return out;
}
/**
 * 向量缩放后相加
 * @param {Vector2} out
 * @param {Vector2} v1
 * @param {Vector2} v2
 * @param {number} a
 */


function scaleAndAdd(out, v1, v2, a) {
  out[0] = v1[0] + v2[0] * a;
  out[1] = v1[1] + v2[1] * a;
  return out;
}
/**
 * 向量相减
 * @param {Vector2} out
 * @param {Vector2} v1
 * @param {Vector2} v2
 */


function sub(out, v1, v2) {
  out[0] = v1[0] - v2[0];
  out[1] = v1[1] - v2[1];
  return out;
}
/**
 * 向量长度
 * @param {Vector2} v
 * @return {number}
 */


function len(v) {
  return Math.sqrt(lenSquare(v));
}

var length = len; // jshint ignore:line

/**
 * 向量长度平方
 * @param {Vector2} v
 * @return {number}
 */

function lenSquare(v) {
  return v[0] * v[0] + v[1] * v[1];
}

var lengthSquare = lenSquare;
/**
 * 向量乘法
 * @param {Vector2} out
 * @param {Vector2} v1
 * @param {Vector2} v2
 */

function mul(out, v1, v2) {
  out[0] = v1[0] * v2[0];
  out[1] = v1[1] * v2[1];
  return out;
}
/**
 * 向量除法
 * @param {Vector2} out
 * @param {Vector2} v1
 * @param {Vector2} v2
 */


function div(out, v1, v2) {
  out[0] = v1[0] / v2[0];
  out[1] = v1[1] / v2[1];
  return out;
}
/**
 * 向量点乘
 * @param {Vector2} v1
 * @param {Vector2} v2
 * @return {number}
 */


function dot(v1, v2) {
  return v1[0] * v2[0] + v1[1] * v2[1];
}
/**
 * 向量缩放
 * @param {Vector2} out
 * @param {Vector2} v
 * @param {number} s
 */


function scale(out, v, s) {
  out[0] = v[0] * s;
  out[1] = v[1] * s;
  return out;
}
/**
 * 向量归一化
 * @param {Vector2} out
 * @param {Vector2} v
 */


function normalize(out, v) {
  var d = len(v);

  if (d === 0) {
    out[0] = 0;
    out[1] = 0;
  } else {
    out[0] = v[0] / d;
    out[1] = v[1] / d;
  }

  return out;
}
/**
 * 计算向量间距离
 * @param {Vector2} v1
 * @param {Vector2} v2
 * @return {number}
 */


function distance(v1, v2) {
  return Math.sqrt((v1[0] - v2[0]) * (v1[0] - v2[0]) + (v1[1] - v2[1]) * (v1[1] - v2[1]));
}

var dist = distance;
/**
 * 向量距离平方
 * @param {Vector2} v1
 * @param {Vector2} v2
 * @return {number}
 */

function distanceSquare(v1, v2) {
  return (v1[0] - v2[0]) * (v1[0] - v2[0]) + (v1[1] - v2[1]) * (v1[1] - v2[1]);
}

var distSquare = distanceSquare;
/**
 * 求负向量
 * @param {Vector2} out
 * @param {Vector2} v
 */

function negate(out, v) {
  out[0] = -v[0];
  out[1] = -v[1];
  return out;
}
/**
 * 插值两个点
 * @param {Vector2} out
 * @param {Vector2} v1
 * @param {Vector2} v2
 * @param {number} t
 */


function lerp(out, v1, v2, t) {
  out[0] = v1[0] + t * (v2[0] - v1[0]);
  out[1] = v1[1] + t * (v2[1] - v1[1]);
  return out;
}
/**
 * 矩阵左乘向量
 * @param {Vector2} out
 * @param {Vector2} v
 * @param {Vector2} m
 */


function applyTransform(out, v, m) {
  var x = v[0];
  var y = v[1];
  out[0] = m[0] * x + m[2] * y + m[4];
  out[1] = m[1] * x + m[3] * y + m[5];
  return out;
}
/**
 * 求两个向量最小值
 * @param  {Vector2} out
 * @param  {Vector2} v1
 * @param  {Vector2} v2
 */


function min(out, v1, v2) {
  out[0] = Math.min(v1[0], v2[0]);
  out[1] = Math.min(v1[1], v2[1]);
  return out;
}
/**
 * 求两个向量最大值
 * @param  {Vector2} out
 * @param  {Vector2} v1
 * @param  {Vector2} v2
 */


function max(out, v1, v2) {
  out[0] = Math.max(v1[0], v2[0]);
  out[1] = Math.max(v1[1], v2[1]);
  return out;
}

exports.create = create;
exports.copy = copy;
exports.clone = clone;
exports.set = set;
exports.add = add;
exports.scaleAndAdd = scaleAndAdd;
exports.sub = sub;
exports.len = len;
exports.length = length;
exports.lenSquare = lenSquare;
exports.lengthSquare = lengthSquare;
exports.mul = mul;
exports.div = div;
exports.dot = dot;
exports.scale = scale;
exports.normalize = normalize;
exports.distance = distance;
exports.dist = dist;
exports.distanceSquare = distanceSquare;
exports.distSquare = distSquare;
exports.negate = negate;
exports.lerp = lerp;
exports.applyTransform = applyTransform;
exports.min = min;
exports.max = max;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS92ZWN0b3IuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9jb3JlL3ZlY3Rvci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgQXJyYXlDdG9yID0gdHlwZW9mIEZsb2F0MzJBcnJheSA9PT0gJ3VuZGVmaW5lZCcgPyBBcnJheSA6IEZsb2F0MzJBcnJheTtcbi8qKlxuICog5Yib5bu65LiA5Liq5ZCR6YePXG4gKiBAcGFyYW0ge251bWJlcn0gW3g9MF1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbeT0wXVxuICogQHJldHVybiB7VmVjdG9yMn1cbiAqL1xuXG5mdW5jdGlvbiBjcmVhdGUoeCwgeSkge1xuICB2YXIgb3V0ID0gbmV3IEFycmF5Q3RvcigyKTtcblxuICBpZiAoeCA9PSBudWxsKSB7XG4gICAgeCA9IDA7XG4gIH1cblxuICBpZiAoeSA9PSBudWxsKSB7XG4gICAgeSA9IDA7XG4gIH1cblxuICBvdXRbMF0gPSB4O1xuICBvdXRbMV0gPSB5O1xuICByZXR1cm4gb3V0O1xufVxuLyoqXG4gKiDlpI3liLblkJHph4/mlbDmja5cbiAqIEBwYXJhbSB7VmVjdG9yMn0gb3V0XG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHZcbiAqIEByZXR1cm4ge1ZlY3RvcjJ9XG4gKi9cblxuXG5mdW5jdGlvbiBjb3B5KG91dCwgdikge1xuICBvdXRbMF0gPSB2WzBdO1xuICBvdXRbMV0gPSB2WzFdO1xuICByZXR1cm4gb3V0O1xufVxuLyoqXG4gKiDlhYvpmobkuIDkuKrlkJHph49cbiAqIEBwYXJhbSB7VmVjdG9yMn0gdlxuICogQHJldHVybiB7VmVjdG9yMn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGNsb25lKHYpIHtcbiAgdmFyIG91dCA9IG5ldyBBcnJheUN0b3IoMik7XG4gIG91dFswXSA9IHZbMF07XG4gIG91dFsxXSA9IHZbMV07XG4gIHJldHVybiBvdXQ7XG59XG4vKipcbiAqIOiuvue9ruWQkemHj+eahOS4pOS4qumhuVxuICogQHBhcmFtIHtWZWN0b3IyfSBvdXRcbiAqIEBwYXJhbSB7bnVtYmVyfSBhXG4gKiBAcGFyYW0ge251bWJlcn0gYlxuICogQHJldHVybiB7VmVjdG9yMn0g57uT5p6cXG4gKi9cblxuXG5mdW5jdGlvbiBzZXQob3V0LCBhLCBiKSB7XG4gIG91dFswXSA9IGE7XG4gIG91dFsxXSA9IGI7XG4gIHJldHVybiBvdXQ7XG59XG4vKipcbiAqIOWQkemHj+ebuOWKoFxuICogQHBhcmFtIHtWZWN0b3IyfSBvdXRcbiAqIEBwYXJhbSB7VmVjdG9yMn0gdjFcbiAqIEBwYXJhbSB7VmVjdG9yMn0gdjJcbiAqL1xuXG5cbmZ1bmN0aW9uIGFkZChvdXQsIHYxLCB2Mikge1xuICBvdXRbMF0gPSB2MVswXSArIHYyWzBdO1xuICBvdXRbMV0gPSB2MVsxXSArIHYyWzFdO1xuICByZXR1cm4gb3V0O1xufVxuLyoqXG4gKiDlkJHph4/nvKnmlL7lkI7nm7jliqBcbiAqIEBwYXJhbSB7VmVjdG9yMn0gb3V0XG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHYxXG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHYyXG4gKiBAcGFyYW0ge251bWJlcn0gYVxuICovXG5cblxuZnVuY3Rpb24gc2NhbGVBbmRBZGQob3V0LCB2MSwgdjIsIGEpIHtcbiAgb3V0WzBdID0gdjFbMF0gKyB2MlswXSAqIGE7XG4gIG91dFsxXSA9IHYxWzFdICsgdjJbMV0gKiBhO1xuICByZXR1cm4gb3V0O1xufVxuLyoqXG4gKiDlkJHph4/nm7jlh49cbiAqIEBwYXJhbSB7VmVjdG9yMn0gb3V0XG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHYxXG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHYyXG4gKi9cblxuXG5mdW5jdGlvbiBzdWIob3V0LCB2MSwgdjIpIHtcbiAgb3V0WzBdID0gdjFbMF0gLSB2MlswXTtcbiAgb3V0WzFdID0gdjFbMV0gLSB2MlsxXTtcbiAgcmV0dXJuIG91dDtcbn1cbi8qKlxuICog5ZCR6YeP6ZW/5bqmXG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHZcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGxlbih2KSB7XG4gIHJldHVybiBNYXRoLnNxcnQobGVuU3F1YXJlKHYpKTtcbn1cblxudmFyIGxlbmd0aCA9IGxlbjsgLy8ganNoaW50IGlnbm9yZTpsaW5lXG5cbi8qKlxuICog5ZCR6YeP6ZW/5bqm5bmz5pa5XG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHZcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5mdW5jdGlvbiBsZW5TcXVhcmUodikge1xuICByZXR1cm4gdlswXSAqIHZbMF0gKyB2WzFdICogdlsxXTtcbn1cblxudmFyIGxlbmd0aFNxdWFyZSA9IGxlblNxdWFyZTtcbi8qKlxuICog5ZCR6YeP5LmY5rOVXG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IG91dFxuICogQHBhcmFtIHtWZWN0b3IyfSB2MVxuICogQHBhcmFtIHtWZWN0b3IyfSB2MlxuICovXG5cbmZ1bmN0aW9uIG11bChvdXQsIHYxLCB2Mikge1xuICBvdXRbMF0gPSB2MVswXSAqIHYyWzBdO1xuICBvdXRbMV0gPSB2MVsxXSAqIHYyWzFdO1xuICByZXR1cm4gb3V0O1xufVxuLyoqXG4gKiDlkJHph4/pmaTms5VcbiAqIEBwYXJhbSB7VmVjdG9yMn0gb3V0XG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHYxXG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHYyXG4gKi9cblxuXG5mdW5jdGlvbiBkaXYob3V0LCB2MSwgdjIpIHtcbiAgb3V0WzBdID0gdjFbMF0gLyB2MlswXTtcbiAgb3V0WzFdID0gdjFbMV0gLyB2MlsxXTtcbiAgcmV0dXJuIG91dDtcbn1cbi8qKlxuICog5ZCR6YeP54K55LmYXG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHYxXG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHYyXG4gKiBAcmV0dXJuIHtudW1iZXJ9XG4gKi9cblxuXG5mdW5jdGlvbiBkb3QodjEsIHYyKSB7XG4gIHJldHVybiB2MVswXSAqIHYyWzBdICsgdjFbMV0gKiB2MlsxXTtcbn1cbi8qKlxuICog5ZCR6YeP57yp5pS+XG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IG91dFxuICogQHBhcmFtIHtWZWN0b3IyfSB2XG4gKiBAcGFyYW0ge251bWJlcn0gc1xuICovXG5cblxuZnVuY3Rpb24gc2NhbGUob3V0LCB2LCBzKSB7XG4gIG91dFswXSA9IHZbMF0gKiBzO1xuICBvdXRbMV0gPSB2WzFdICogcztcbiAgcmV0dXJuIG91dDtcbn1cbi8qKlxuICog5ZCR6YeP5b2S5LiA5YyWXG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IG91dFxuICogQHBhcmFtIHtWZWN0b3IyfSB2XG4gKi9cblxuXG5mdW5jdGlvbiBub3JtYWxpemUob3V0LCB2KSB7XG4gIHZhciBkID0gbGVuKHYpO1xuXG4gIGlmIChkID09PSAwKSB7XG4gICAgb3V0WzBdID0gMDtcbiAgICBvdXRbMV0gPSAwO1xuICB9IGVsc2Uge1xuICAgIG91dFswXSA9IHZbMF0gLyBkO1xuICAgIG91dFsxXSA9IHZbMV0gLyBkO1xuICB9XG5cbiAgcmV0dXJuIG91dDtcbn1cbi8qKlxuICog6K6h566X5ZCR6YeP6Ze06Led56a7XG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHYxXG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHYyXG4gKiBAcmV0dXJuIHtudW1iZXJ9XG4gKi9cblxuXG5mdW5jdGlvbiBkaXN0YW5jZSh2MSwgdjIpIHtcbiAgcmV0dXJuIE1hdGguc3FydCgodjFbMF0gLSB2MlswXSkgKiAodjFbMF0gLSB2MlswXSkgKyAodjFbMV0gLSB2MlsxXSkgKiAodjFbMV0gLSB2MlsxXSkpO1xufVxuXG52YXIgZGlzdCA9IGRpc3RhbmNlO1xuLyoqXG4gKiDlkJHph4/ot53nprvlubPmlrlcbiAqIEBwYXJhbSB7VmVjdG9yMn0gdjFcbiAqIEBwYXJhbSB7VmVjdG9yMn0gdjJcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5mdW5jdGlvbiBkaXN0YW5jZVNxdWFyZSh2MSwgdjIpIHtcbiAgcmV0dXJuICh2MVswXSAtIHYyWzBdKSAqICh2MVswXSAtIHYyWzBdKSArICh2MVsxXSAtIHYyWzFdKSAqICh2MVsxXSAtIHYyWzFdKTtcbn1cblxudmFyIGRpc3RTcXVhcmUgPSBkaXN0YW5jZVNxdWFyZTtcbi8qKlxuICog5rGC6LSf5ZCR6YePXG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IG91dFxuICogQHBhcmFtIHtWZWN0b3IyfSB2XG4gKi9cblxuZnVuY3Rpb24gbmVnYXRlKG91dCwgdikge1xuICBvdXRbMF0gPSAtdlswXTtcbiAgb3V0WzFdID0gLXZbMV07XG4gIHJldHVybiBvdXQ7XG59XG4vKipcbiAqIOaPkuWAvOS4pOS4queCuVxuICogQHBhcmFtIHtWZWN0b3IyfSBvdXRcbiAqIEBwYXJhbSB7VmVjdG9yMn0gdjFcbiAqIEBwYXJhbSB7VmVjdG9yMn0gdjJcbiAqIEBwYXJhbSB7bnVtYmVyfSB0XG4gKi9cblxuXG5mdW5jdGlvbiBsZXJwKG91dCwgdjEsIHYyLCB0KSB7XG4gIG91dFswXSA9IHYxWzBdICsgdCAqICh2MlswXSAtIHYxWzBdKTtcbiAgb3V0WzFdID0gdjFbMV0gKyB0ICogKHYyWzFdIC0gdjFbMV0pO1xuICByZXR1cm4gb3V0O1xufVxuLyoqXG4gKiDnn6npmLXlt6bkuZjlkJHph49cbiAqIEBwYXJhbSB7VmVjdG9yMn0gb3V0XG4gKiBAcGFyYW0ge1ZlY3RvcjJ9IHZcbiAqIEBwYXJhbSB7VmVjdG9yMn0gbVxuICovXG5cblxuZnVuY3Rpb24gYXBwbHlUcmFuc2Zvcm0ob3V0LCB2LCBtKSB7XG4gIHZhciB4ID0gdlswXTtcbiAgdmFyIHkgPSB2WzFdO1xuICBvdXRbMF0gPSBtWzBdICogeCArIG1bMl0gKiB5ICsgbVs0XTtcbiAgb3V0WzFdID0gbVsxXSAqIHggKyBtWzNdICogeSArIG1bNV07XG4gIHJldHVybiBvdXQ7XG59XG4vKipcbiAqIOaxguS4pOS4quWQkemHj+acgOWwj+WAvFxuICogQHBhcmFtICB7VmVjdG9yMn0gb3V0XG4gKiBAcGFyYW0gIHtWZWN0b3IyfSB2MVxuICogQHBhcmFtICB7VmVjdG9yMn0gdjJcbiAqL1xuXG5cbmZ1bmN0aW9uIG1pbihvdXQsIHYxLCB2Mikge1xuICBvdXRbMF0gPSBNYXRoLm1pbih2MVswXSwgdjJbMF0pO1xuICBvdXRbMV0gPSBNYXRoLm1pbih2MVsxXSwgdjJbMV0pO1xuICByZXR1cm4gb3V0O1xufVxuLyoqXG4gKiDmsYLkuKTkuKrlkJHph4/mnIDlpKflgLxcbiAqIEBwYXJhbSAge1ZlY3RvcjJ9IG91dFxuICogQHBhcmFtICB7VmVjdG9yMn0gdjFcbiAqIEBwYXJhbSAge1ZlY3RvcjJ9IHYyXG4gKi9cblxuXG5mdW5jdGlvbiBtYXgob3V0LCB2MSwgdjIpIHtcbiAgb3V0WzBdID0gTWF0aC5tYXgodjFbMF0sIHYyWzBdKTtcbiAgb3V0WzFdID0gTWF0aC5tYXgodjFbMV0sIHYyWzFdKTtcbiAgcmV0dXJuIG91dDtcbn1cblxuZXhwb3J0cy5jcmVhdGUgPSBjcmVhdGU7XG5leHBvcnRzLmNvcHkgPSBjb3B5O1xuZXhwb3J0cy5jbG9uZSA9IGNsb25lO1xuZXhwb3J0cy5zZXQgPSBzZXQ7XG5leHBvcnRzLmFkZCA9IGFkZDtcbmV4cG9ydHMuc2NhbGVBbmRBZGQgPSBzY2FsZUFuZEFkZDtcbmV4cG9ydHMuc3ViID0gc3ViO1xuZXhwb3J0cy5sZW4gPSBsZW47XG5leHBvcnRzLmxlbmd0aCA9IGxlbmd0aDtcbmV4cG9ydHMubGVuU3F1YXJlID0gbGVuU3F1YXJlO1xuZXhwb3J0cy5sZW5ndGhTcXVhcmUgPSBsZW5ndGhTcXVhcmU7XG5leHBvcnRzLm11bCA9IG11bDtcbmV4cG9ydHMuZGl2ID0gZGl2O1xuZXhwb3J0cy5kb3QgPSBkb3Q7XG5leHBvcnRzLnNjYWxlID0gc2NhbGU7XG5leHBvcnRzLm5vcm1hbGl6ZSA9IG5vcm1hbGl6ZTtcbmV4cG9ydHMuZGlzdGFuY2UgPSBkaXN0YW5jZTtcbmV4cG9ydHMuZGlzdCA9IGRpc3Q7XG5leHBvcnRzLmRpc3RhbmNlU3F1YXJlID0gZGlzdGFuY2VTcXVhcmU7XG5leHBvcnRzLmRpc3RTcXVhcmUgPSBkaXN0U3F1YXJlO1xuZXhwb3J0cy5uZWdhdGUgPSBuZWdhdGU7XG5leHBvcnRzLmxlcnAgPSBsZXJwO1xuZXhwb3J0cy5hcHBseVRyYW5zZm9ybSA9IGFwcGx5VHJhbnNmb3JtO1xuZXhwb3J0cy5taW4gPSBtaW47XG5leHBvcnRzLm1heCA9IG1heDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/vector.js
