__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var unstated_next__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! unstated-next */ "./node_modules/unstated-next/dist/unstated-next.mjs");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);






var countState = function countState() {
  var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(initialState),
      _useState2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useState, 2),
      count = _useState2[0],
      setCount = _useState2[1];

  var getCount = function getCount() {
    _common__WEBPACK_IMPORTED_MODULE_3__["Ajax"].get("/consume/imsgs/newcount", {}, {
      silent: true
    }).then(function (res) {
      setCount(lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(res, "body.respData", 0));
    });
  };

  return {
    count: count,
    setCount: setCount,
    getCount: getCount
  };
};

var CountContainer = Object(unstated_next__WEBPACK_IMPORTED_MODULE_2__["createContainer"])(countState);
/* harmony default export */ __webpack_exports__["default"] = (CountContainer);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BhZ2UvbWVzc2FnZS1jb3VudC1zdGF0ZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcGFnZS9tZXNzYWdlLWNvdW50LXN0YXRlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgY3JlYXRlQ29udGFpbmVyIH0gZnJvbSBcInVuc3RhdGVkLW5leHRcIjtcbmltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBBamF4IH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcblxuY29uc3QgY291bnRTdGF0ZSA9IChpbml0aWFsU3RhdGUgPSAwKSA9PiB7XG4gIGxldCBbY291bnQsIHNldENvdW50XSA9IHVzZVN0YXRlKGluaXRpYWxTdGF0ZSk7XG4gIGNvbnN0IGdldENvdW50ID0gKCkgPT4ge1xuICAgIEFqYXguZ2V0KFwiL2NvbnN1bWUvaW1zZ3MvbmV3Y291bnRcIiwge30sIHsgc2lsZW50OiB0cnVlIH0pLnRoZW4ocmVzID0+IHtcbiAgICAgIHNldENvdW50KF8uZ2V0KHJlcywgXCJib2R5LnJlc3BEYXRhXCIsIDApKTtcbiAgICB9KTtcbiAgfTtcbiAgcmV0dXJuIHsgY291bnQsIHNldENvdW50LCBnZXRDb3VudCB9O1xufTtcbmxldCBDb3VudENvbnRhaW5lciA9IGNyZWF0ZUNvbnRhaW5lcihjb3VudFN0YXRlKTtcbmV4cG9ydCBkZWZhdWx0IENvdW50Q29udGFpbmVyO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/page/message-count-state.tsx
