/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var Scale = __webpack_require__(/*! ./Scale */ "./node_modules/echarts/lib/scale/Scale.js");

var OrdinalMeta = __webpack_require__(/*! ../data/OrdinalMeta */ "./node_modules/echarts/lib/data/OrdinalMeta.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Linear continuous scale
 * @module echarts/coord/scale/Ordinal
 *
 * http://en.wikipedia.org/wiki/Level_of_measurement
 */
// FIXME only one data


var scaleProto = Scale.prototype;
var OrdinalScale = Scale.extend({
  type: 'ordinal',

  /**
   * @param {module:echarts/data/OrdianlMeta|Array.<string>} ordinalMeta
   */
  init: function init(ordinalMeta, extent) {
    // Caution: Should not use instanceof, consider ec-extensions using
    // import approach to get OrdinalMeta class.
    if (!ordinalMeta || zrUtil.isArray(ordinalMeta)) {
      ordinalMeta = new OrdinalMeta({
        categories: ordinalMeta
      });
    }

    this._ordinalMeta = ordinalMeta;
    this._extent = extent || [0, ordinalMeta.categories.length - 1];
  },
  parse: function parse(val) {
    return typeof val === 'string' ? this._ordinalMeta.getOrdinal(val) // val might be float.
    : Math.round(val);
  },
  contain: function contain(rank) {
    rank = this.parse(rank);
    return scaleProto.contain.call(this, rank) && this._ordinalMeta.categories[rank] != null;
  },

  /**
   * Normalize given rank or name to linear [0, 1]
   * @param {number|string} [val]
   * @return {number}
   */
  normalize: function normalize(val) {
    return scaleProto.normalize.call(this, this.parse(val));
  },
  scale: function scale(val) {
    return Math.round(scaleProto.scale.call(this, val));
  },

  /**
   * @return {Array}
   */
  getTicks: function getTicks() {
    var ticks = [];
    var extent = this._extent;
    var rank = extent[0];

    while (rank <= extent[1]) {
      ticks.push(rank);
      rank++;
    }

    return ticks;
  },

  /**
   * Get item on rank n
   * @param {number} n
   * @return {string}
   */
  getLabel: function getLabel(n) {
    if (!this.isBlank()) {
      // Note that if no data, ordinalMeta.categories is an empty array.
      return this._ordinalMeta.categories[n];
    }
  },

  /**
   * @return {number}
   */
  count: function count() {
    return this._extent[1] - this._extent[0] + 1;
  },

  /**
   * @override
   */
  unionExtentFromData: function unionExtentFromData(data, dim) {
    this.unionExtent(data.getApproximateExtent(dim));
  },
  getOrdinalMeta: function getOrdinalMeta() {
    return this._ordinalMeta;
  },
  niceTicks: zrUtil.noop,
  niceExtent: zrUtil.noop
});
/**
 * @return {module:echarts/scale/Time}
 */

OrdinalScale.create = function () {
  return new OrdinalScale();
};

var _default = OrdinalScale;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvc2NhbGUvT3JkaW5hbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3NjYWxlL09yZGluYWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgU2NhbGUgPSByZXF1aXJlKFwiLi9TY2FsZVwiKTtcblxudmFyIE9yZGluYWxNZXRhID0gcmVxdWlyZShcIi4uL2RhdGEvT3JkaW5hbE1ldGFcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBMaW5lYXIgY29udGludW91cyBzY2FsZVxuICogQG1vZHVsZSBlY2hhcnRzL2Nvb3JkL3NjYWxlL09yZGluYWxcbiAqXG4gKiBodHRwOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0xldmVsX29mX21lYXN1cmVtZW50XG4gKi9cbi8vIEZJWE1FIG9ubHkgb25lIGRhdGFcbnZhciBzY2FsZVByb3RvID0gU2NhbGUucHJvdG90eXBlO1xudmFyIE9yZGluYWxTY2FsZSA9IFNjYWxlLmV4dGVuZCh7XG4gIHR5cGU6ICdvcmRpbmFsJyxcblxuICAvKipcbiAgICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9kYXRhL09yZGlhbmxNZXRhfEFycmF5LjxzdHJpbmc+fSBvcmRpbmFsTWV0YVxuICAgKi9cbiAgaW5pdDogZnVuY3Rpb24gKG9yZGluYWxNZXRhLCBleHRlbnQpIHtcbiAgICAvLyBDYXV0aW9uOiBTaG91bGQgbm90IHVzZSBpbnN0YW5jZW9mLCBjb25zaWRlciBlYy1leHRlbnNpb25zIHVzaW5nXG4gICAgLy8gaW1wb3J0IGFwcHJvYWNoIHRvIGdldCBPcmRpbmFsTWV0YSBjbGFzcy5cbiAgICBpZiAoIW9yZGluYWxNZXRhIHx8IHpyVXRpbC5pc0FycmF5KG9yZGluYWxNZXRhKSkge1xuICAgICAgb3JkaW5hbE1ldGEgPSBuZXcgT3JkaW5hbE1ldGEoe1xuICAgICAgICBjYXRlZ29yaWVzOiBvcmRpbmFsTWV0YVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgdGhpcy5fb3JkaW5hbE1ldGEgPSBvcmRpbmFsTWV0YTtcbiAgICB0aGlzLl9leHRlbnQgPSBleHRlbnQgfHwgWzAsIG9yZGluYWxNZXRhLmNhdGVnb3JpZXMubGVuZ3RoIC0gMV07XG4gIH0sXG4gIHBhcnNlOiBmdW5jdGlvbiAodmFsKSB7XG4gICAgcmV0dXJuIHR5cGVvZiB2YWwgPT09ICdzdHJpbmcnID8gdGhpcy5fb3JkaW5hbE1ldGEuZ2V0T3JkaW5hbCh2YWwpIC8vIHZhbCBtaWdodCBiZSBmbG9hdC5cbiAgICA6IE1hdGgucm91bmQodmFsKTtcbiAgfSxcbiAgY29udGFpbjogZnVuY3Rpb24gKHJhbmspIHtcbiAgICByYW5rID0gdGhpcy5wYXJzZShyYW5rKTtcbiAgICByZXR1cm4gc2NhbGVQcm90by5jb250YWluLmNhbGwodGhpcywgcmFuaykgJiYgdGhpcy5fb3JkaW5hbE1ldGEuY2F0ZWdvcmllc1tyYW5rXSAhPSBudWxsO1xuICB9LFxuXG4gIC8qKlxuICAgKiBOb3JtYWxpemUgZ2l2ZW4gcmFuayBvciBuYW1lIHRvIGxpbmVhciBbMCwgMV1cbiAgICogQHBhcmFtIHtudW1iZXJ8c3RyaW5nfSBbdmFsXVxuICAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICAqL1xuICBub3JtYWxpemU6IGZ1bmN0aW9uICh2YWwpIHtcbiAgICByZXR1cm4gc2NhbGVQcm90by5ub3JtYWxpemUuY2FsbCh0aGlzLCB0aGlzLnBhcnNlKHZhbCkpO1xuICB9LFxuICBzY2FsZTogZnVuY3Rpb24gKHZhbCkge1xuICAgIHJldHVybiBNYXRoLnJvdW5kKHNjYWxlUHJvdG8uc2NhbGUuY2FsbCh0aGlzLCB2YWwpKTtcbiAgfSxcblxuICAvKipcbiAgICogQHJldHVybiB7QXJyYXl9XG4gICAqL1xuICBnZXRUaWNrczogZnVuY3Rpb24gKCkge1xuICAgIHZhciB0aWNrcyA9IFtdO1xuICAgIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7XG4gICAgdmFyIHJhbmsgPSBleHRlbnRbMF07XG5cbiAgICB3aGlsZSAocmFuayA8PSBleHRlbnRbMV0pIHtcbiAgICAgIHRpY2tzLnB1c2gocmFuayk7XG4gICAgICByYW5rKys7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRpY2tzO1xuICB9LFxuXG4gIC8qKlxuICAgKiBHZXQgaXRlbSBvbiByYW5rIG5cbiAgICogQHBhcmFtIHtudW1iZXJ9IG5cbiAgICogQHJldHVybiB7c3RyaW5nfVxuICAgKi9cbiAgZ2V0TGFiZWw6IGZ1bmN0aW9uIChuKSB7XG4gICAgaWYgKCF0aGlzLmlzQmxhbmsoKSkge1xuICAgICAgLy8gTm90ZSB0aGF0IGlmIG5vIGRhdGEsIG9yZGluYWxNZXRhLmNhdGVnb3JpZXMgaXMgYW4gZW1wdHkgYXJyYXkuXG4gICAgICByZXR1cm4gdGhpcy5fb3JkaW5hbE1ldGEuY2F0ZWdvcmllc1tuXTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEByZXR1cm4ge251bWJlcn1cbiAgICovXG4gIGNvdW50OiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2V4dGVudFsxXSAtIHRoaXMuX2V4dGVudFswXSArIDE7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBvdmVycmlkZVxuICAgKi9cbiAgdW5pb25FeHRlbnRGcm9tRGF0YTogZnVuY3Rpb24gKGRhdGEsIGRpbSkge1xuICAgIHRoaXMudW5pb25FeHRlbnQoZGF0YS5nZXRBcHByb3hpbWF0ZUV4dGVudChkaW0pKTtcbiAgfSxcbiAgZ2V0T3JkaW5hbE1ldGE6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5fb3JkaW5hbE1ldGE7XG4gIH0sXG4gIG5pY2VUaWNrczogenJVdGlsLm5vb3AsXG4gIG5pY2VFeHRlbnQ6IHpyVXRpbC5ub29wXG59KTtcbi8qKlxuICogQHJldHVybiB7bW9kdWxlOmVjaGFydHMvc2NhbGUvVGltZX1cbiAqL1xuXG5PcmRpbmFsU2NhbGUuY3JlYXRlID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gbmV3IE9yZGluYWxTY2FsZSgpO1xufTtcblxudmFyIF9kZWZhdWx0ID0gT3JkaW5hbFNjYWxlO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFwRkE7QUFzRkE7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/scale/Ordinal.js
