__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/endoOutputRender.tsx";

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__["default"])(["  \n      "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__["default"])(["\n  .paragraph{\n    font-size: 16px;\n    margin: 0 0 15px 0;\n    text-align: center;\n    color: rgba(0, 0, 0, 0.85);\n    .pdf-link-list {\n        font-size: 14px;\n        margin: 10px 0;\n        .pdf-link-item {\n            display: block;\n            margin: 0 15px 10px 0;\n            line-height: 20px;\n        }\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_11__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var ECardDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject());

var EndoOutputRender =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(EndoOutputRender, _ModelWidget);

  function EndoOutputRender(props, state) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, EndoOutputRender);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(EndoOutputRender).call(this, props, state));
    _this.getOutputs =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var dataFixed, endoId;
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              dataFixed = _this.props.dataFixed;
              endoId = _this.getValueFromModel(dataFixed ? "".concat(dataFixed, ".endoId") : "endoId");

              _this.getHelpers().getAjax().get(_common__WEBPACK_IMPORTED_MODULE_12__["Apis"].ENDOREEMENT_OUTPUTS.replace(":endoId", endoId), {}, {}).then(function (response) {
                var _ref2 = response.body || [],
                    respData = _ref2.respData;

                if (_this.props.afterGetData) {
                  _this.props.afterGetData(respData);
                }

                _this.setState({
                  pdfList: respData
                });
              }).catch(function (error) {});

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(EndoOutputRender, [{
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(EndoOutputRender.prototype), "initState", this).call(this), {
        pdfList: []
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Box: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject2())
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getOutputs();
    }
  }, {
    key: "renderPdfList",
    value: function renderPdfList() {
      var pdfList = this.state.pdfList;
      var endoId = this.getValueFromModel("endoId");
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        style: {
          display: "inline-flex"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }, (pdfList || []).map(function (pdf) {
        var displayName = pdf.displayName,
            name = pdf.name,
            fileName = pdf.fileName;
        var pdfUrl = _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].appendAuthToUrl("/endo/".concat(endoId, "/pdf/").concat(name));
        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          href: pdfUrl,
          className: "pdf-link-item",
          key: fileName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 94
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
          type: "file-pdf",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 95
          },
          __self: this
        }), "".concat(displayName));
      }));
    }
  }, {
    key: "outPutsRender",
    value: function outPutsRender() {
      var dataFixed = this.props.dataFixed;
      var pdfList = this.state.pdfList;
      var endoId = this.getValueFromModel(dataFixed ? "".concat(dataFixed, ".endoId") : "endoId");
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        style: {
          display: "flex",
          justifyContent: "space-evenly"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 110
        },
        __self: this
      }, (pdfList || []).map(function (pdf) {
        var displayName = pdf.displayName,
            name = pdf.name,
            fileName = pdf.fileName;
        var pdfUrl = _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].getServiceLocation("/endo/".concat(endoId, "/pdf/").concat(name, "?access_token=").concat(_common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].AUTH_KEY)));
        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          style: {
            color: COLOR_A
          },
          target: "_blank",
          href: pdfUrl,
          key: fileName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 118
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
          type: "file-pdf",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 119
          },
          __self: this
        }), "".concat(displayName));
      }), this.props.isECard && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ECardDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        className: "paragraph",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 125
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        className: "pdf-link-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, this.renderPdfList()))));
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.Box, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 136
        },
        __self: this
      }, this.outPutsRender());
    }
  }]);

  return EndoOutputRender;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (EndoOutputRender);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2VuZG9PdXRwdXRSZW5kZXIudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2VuZG9PdXRwdXRSZW5kZXIudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0IH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IEFqYXhSZXNwb25zZSwgU3R5bGVkRElWLCBXaWRnZXRQcm9wcyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlcy9pbmRleFwiO1xuaW1wb3J0IHsgQWpheCwgQXBpcywgQ29uc3RzLCBTdG9yYWdlIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IEljb24gfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgRGVza1BhZ2VCb3ggfSBmcm9tIFwiQGRlc2stc3R5bGVzL2dsb2JhbFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuXG5jb25zdCB7IENPTE9SX0EgfSA9IFRoZW1lLmdldFRoZW1lKCk7XG5leHBvcnQgdHlwZSBPdXRwdXRSZW5kZXJQcm9wcyA9IHtcbiAgbW9kZWw6IGFueTtcbiAgYWZ0ZXJHZXREYXRhPzogYW55LFxuICBkYXRhRml4ZWQ/OiBhbnk7XG4gIGlzRUNhcmQ/OiBib29sZWFuXG59ICYgV2lkZ2V0UHJvcHM7XG5cbmV4cG9ydCB0eXBlIE91dHB1dFJlbmRlclN0YXRlID0ge1xuICBwZGZMaXN0OiBhbnk7XG59O1xuXG5leHBvcnQgdHlwZSBPdXRwdXRSZW5kZXJDb21wb25lbnRzID0ge1xuICBCb3g6IFN0eWxlZERJVjtcbn07XG5cbmNvbnN0IEVDYXJkRGl2ID0gU3R5bGVkLmRpdmBcbiAgLnBhcmFncmFwaHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbWFyZ2luOiAwIDAgMTVweCAwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjg1KTtcbiAgICAucGRmLWxpbmstbGlzdCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgbWFyZ2luOiAxMHB4IDA7XG4gICAgICAgIC5wZGYtbGluay1pdGVtIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgbWFyZ2luOiAwIDE1cHggMTBweCAwO1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgIH1cbiAgICB9XG4gIH1cbmA7XG5cbmNsYXNzIEVuZG9PdXRwdXRSZW5kZXI8UCBleHRlbmRzIE91dHB1dFJlbmRlclByb3BzLCBTIGV4dGVuZHMgT3V0cHV0UmVuZGVyU3RhdGUsIEMgZXh0ZW5kcyBPdXRwdXRSZW5kZXJDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IGFueSwgc3RhdGU/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgc3RhdGUpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgcGRmTGlzdDogW10sXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94OiBTdHlsZWQuZGl2YCAgXG4gICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuZ2V0T3V0cHV0cygpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRPdXRwdXRzID0gYXN5bmMgKCkgPT4ge1xuICAgIGNvbnN0IHsgZGF0YUZpeGVkIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IGVuZG9JZCA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoZGF0YUZpeGVkID8gYCR7ZGF0YUZpeGVkfS5lbmRvSWRgIDogXCJlbmRvSWRcIik7XG4gICAgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgIC5nZXRBamF4KClcbiAgICAgIC5nZXQoQXBpcy5FTkRPUkVFTUVOVF9PVVRQVVRTLnJlcGxhY2UoXCI6ZW5kb0lkXCIsIGVuZG9JZCksIHt9LCB7fSlcbiAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGNvbnN0IHsgcmVzcERhdGEgfSA9IHJlc3BvbnNlLmJvZHkgfHwgW107XG4gICAgICAgIGlmICh0aGlzLnByb3BzLmFmdGVyR2V0RGF0YSkge1xuICAgICAgICAgIHRoaXMucHJvcHMuYWZ0ZXJHZXREYXRhKHJlc3BEYXRhKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBwZGZMaXN0OiByZXNwRGF0YSxcbiAgICAgICAgfSk7XG4gICAgICB9KVxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgIH0pO1xuICB9O1xuXG4gIHByaXZhdGUgcmVuZGVyUGRmTGlzdCgpIHtcbiAgICBjb25zdCB7IHBkZkxpc3QgfSA9IHRoaXMuc3RhdGU7XG4gICAgbGV0IGVuZG9JZCA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJlbmRvSWRcIik7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgc3R5bGU9e3sgZGlzcGxheTogXCJpbmxpbmUtZmxleFwiIH19PlxuICAgICAgICB7KHBkZkxpc3QgfHwgW10pLm1hcCgocGRmOiBhbnkpID0+IHtcbiAgICAgICAgICBjb25zdCB7IGRpc3BsYXlOYW1lLCBuYW1lLCBmaWxlTmFtZSB9ID0gcGRmO1xuICAgICAgICAgIGNvbnN0IHBkZlVybCA9IEFqYXguYXBwZW5kQXV0aFRvVXJsKGAvZW5kby8ke2VuZG9JZH0vcGRmLyR7bmFtZX1gKTtcbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPGEgaHJlZj17cGRmVXJsfSBjbGFzc05hbWU9XCJwZGYtbGluay1pdGVtXCIga2V5PXtmaWxlTmFtZX0+XG4gICAgICAgICAgICAgIDxJY29uIHR5cGU9XCJmaWxlLXBkZlwiLz5cbiAgICAgICAgICAgICAge2Ake2Rpc3BsYXlOYW1lfWB9XG4gICAgICAgICAgICA8L2E+XG4gICAgICAgICAgKTtcbiAgICAgICAgfSl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcHJpdmF0ZSBvdXRQdXRzUmVuZGVyKCkge1xuICAgIGNvbnN0IHsgZGF0YUZpeGVkIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgcGRmTGlzdCB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBlbmRvSWQgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGRhdGFGaXhlZCA/IGAke2RhdGFGaXhlZH0uZW5kb0lkYCA6IFwiZW5kb0lkXCIpO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgc3R5bGU9e3sgZGlzcGxheTogXCJmbGV4XCIsIGp1c3RpZnlDb250ZW50OiBcInNwYWNlLWV2ZW5seVwiIH19PlxuICAgICAgICB7KHBkZkxpc3QgfHwgW10pLm1hcCgocGRmOiBhbnkpID0+IHtcbiAgICAgICAgICBjb25zdCB7IGRpc3BsYXlOYW1lLCBuYW1lLCBmaWxlTmFtZSB9ID0gcGRmO1xuICAgICAgICAgIGNvbnN0IHBkZlVybCA9IEFqYXguZ2V0U2VydmljZUxvY2F0aW9uKFxuICAgICAgICAgICAgYC9lbmRvLyR7ZW5kb0lkfS9wZGYvJHtuYW1lfT9hY2Nlc3NfdG9rZW49JHtTdG9yYWdlLkF1dGguc2Vzc2lvbigpLmdldChDb25zdHMuQVVUSF9LRVkpfWAsXG4gICAgICAgICAgKTtcblxuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8YSBzdHlsZT17eyBjb2xvcjogQ09MT1JfQSB9fSB0YXJnZXQ9XCJfYmxhbmtcIiBocmVmPXtwZGZVcmx9IGtleT17ZmlsZU5hbWV9PlxuICAgICAgICAgICAgICA8SWNvbiB0eXBlPVwiZmlsZS1wZGZcIi8+XG4gICAgICAgICAgICAgIHtgJHtkaXNwbGF5TmFtZX1gfVxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICk7XG4gICAgICAgIH0pfVxuICAgICAgICB7dGhpcy5wcm9wcy5pc0VDYXJkICYmIDxFQ2FyZERpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBhcmFncmFwaFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwZGYtbGluay1saXN0XCI+e3RoaXMucmVuZGVyUGRmTGlzdCgpfTwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L0VDYXJkRGl2Pn1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3g+XG4gICAgICAgIHt0aGlzLm91dFB1dHNSZW5kZXIoKX1cbiAgICAgIDwvQy5Cb3g+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBFbmRvT3V0cHV0UmVuZGVyO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQUE7QUFDQTtBQWVBO0FBQ0E7QUFpQkE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXFCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFmQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXJCQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBSUE7OztBQUVBO0FBQ0E7QUFDQTs7O0FBcUJBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFHQTs7O0FBRUE7QUFBQTtBQUFBO0FBR0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBOzs7O0FBaEdBO0FBQ0E7QUFrR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/endoOutputRender.tsx
