__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toEditorState", function() { return toEditorState; });
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _component_Mention_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./component/Mention.react */ "./node_modules/rc-editor-mention/es/component/Mention.react.js");
/* harmony import */ var _utils_exportContent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils/exportContent */ "./node_modules/rc-editor-mention/es/utils/exportContent.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "toString", function() { return _utils_exportContent__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _utils_getMentions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utils/getMentions */ "./node_modules/rc-editor-mention/es/utils/getMentions.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getMentions", function() { return _utils_getMentions__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _component_Nav_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./component/Nav.react */ "./node_modules/rc-editor-mention/es/component/Nav.react.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Nav", function() { return _component_Nav_react__WEBPACK_IMPORTED_MODULE_4__["default"]; });

// export this package's api






function toEditorState(text) {
  return draft_js__WEBPACK_IMPORTED_MODULE_0__["ContentState"].createFromText(text);
}

_component_Mention_react__WEBPACK_IMPORTED_MODULE_1__["default"].Nav = _component_Nav_react__WEBPACK_IMPORTED_MODULE_4__["default"];
_component_Mention_react__WEBPACK_IMPORTED_MODULE_1__["default"].toString = _utils_exportContent__WEBPACK_IMPORTED_MODULE_2__["default"];
_component_Mention_react__WEBPACK_IMPORTED_MODULE_1__["default"].toEditorState = toEditorState;
_component_Mention_react__WEBPACK_IMPORTED_MODULE_1__["default"].getMentions = _utils_getMentions__WEBPACK_IMPORTED_MODULE_3__["default"];

/* harmony default export */ __webpack_exports__["default"] = (_component_Mention_react__WEBPACK_IMPORTED_MODULE_1__["default"]);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1lZGl0b3ItbWVudGlvbi9lcy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBleHBvcnQgdGhpcyBwYWNrYWdlJ3MgYXBpXG5pbXBvcnQgeyBDb250ZW50U3RhdGUgfSBmcm9tICdkcmFmdC1qcyc7XG5pbXBvcnQgTWVudGlvbiBmcm9tICcuL2NvbXBvbmVudC9NZW50aW9uLnJlYWN0JztcbmltcG9ydCB0b1N0cmluZyBmcm9tICcuL3V0aWxzL2V4cG9ydENvbnRlbnQnO1xuaW1wb3J0IGdldE1lbnRpb25zIGZyb20gJy4vdXRpbHMvZ2V0TWVudGlvbnMnO1xuaW1wb3J0IE5hdiBmcm9tICcuL2NvbXBvbmVudC9OYXYucmVhY3QnO1xuXG5mdW5jdGlvbiB0b0VkaXRvclN0YXRlKHRleHQpIHtcbiAgcmV0dXJuIENvbnRlbnRTdGF0ZS5jcmVhdGVGcm9tVGV4dCh0ZXh0KTtcbn1cblxuTWVudGlvbi5OYXYgPSBOYXY7XG5NZW50aW9uLnRvU3RyaW5nID0gdG9TdHJpbmc7XG5NZW50aW9uLnRvRWRpdG9yU3RhdGUgPSB0b0VkaXRvclN0YXRlO1xuTWVudGlvbi5nZXRNZW50aW9ucyA9IGdldE1lbnRpb25zO1xuXG5leHBvcnQgeyBOYXYsIHRvU3RyaW5nLCB0b0VkaXRvclN0YXRlLCBnZXRNZW50aW9ucyB9O1xuXG5leHBvcnQgZGVmYXVsdCBNZW50aW9uOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/index.js
