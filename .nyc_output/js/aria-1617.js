/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var lang = __webpack_require__(/*! ../lang */ "./node_modules/echarts/lib/lang.js");

var _dataProvider = __webpack_require__(/*! ../data/helper/dataProvider */ "./node_modules/echarts/lib/data/helper/dataProvider.js");

var retrieveRawValue = _dataProvider.retrieveRawValue;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

function _default(dom, ecModel) {
  var ariaModel = ecModel.getModel('aria');

  if (!ariaModel.get('show')) {
    return;
  } else if (ariaModel.get('description')) {
    dom.setAttribute('aria-label', ariaModel.get('description'));
    return;
  }

  var seriesCnt = 0;
  ecModel.eachSeries(function (seriesModel, idx) {
    ++seriesCnt;
  }, this);
  var maxDataCnt = ariaModel.get('data.maxCount') || 10;
  var maxSeriesCnt = ariaModel.get('series.maxCount') || 10;
  var displaySeriesCnt = Math.min(seriesCnt, maxSeriesCnt);
  var ariaLabel;

  if (seriesCnt < 1) {
    // No series, no aria label
    return;
  } else {
    var title = getTitle();

    if (title) {
      ariaLabel = replace(getConfig('general.withTitle'), {
        title: title
      });
    } else {
      ariaLabel = getConfig('general.withoutTitle');
    }

    var seriesLabels = [];
    var prefix = seriesCnt > 1 ? 'series.multiple.prefix' : 'series.single.prefix';
    ariaLabel += replace(getConfig(prefix), {
      seriesCount: seriesCnt
    });
    ecModel.eachSeries(function (seriesModel, idx) {
      if (idx < displaySeriesCnt) {
        var seriesLabel;
        var seriesName = seriesModel.get('name');
        var seriesTpl = 'series.' + (seriesCnt > 1 ? 'multiple' : 'single') + '.';
        seriesLabel = getConfig(seriesName ? seriesTpl + 'withName' : seriesTpl + 'withoutName');
        seriesLabel = replace(seriesLabel, {
          seriesId: seriesModel.seriesIndex,
          seriesName: seriesModel.get('name'),
          seriesType: getSeriesTypeName(seriesModel.subType)
        });
        var data = seriesModel.getData();
        window.data = data;

        if (data.count() > maxDataCnt) {
          // Show part of data
          seriesLabel += replace(getConfig('data.partialData'), {
            displayCnt: maxDataCnt
          });
        } else {
          seriesLabel += getConfig('data.allData');
        }

        var dataLabels = [];

        for (var i = 0; i < data.count(); i++) {
          if (i < maxDataCnt) {
            var name = data.getName(i);
            var value = retrieveRawValue(data, i);
            dataLabels.push(replace(name ? getConfig('data.withName') : getConfig('data.withoutName'), {
              name: name,
              value: value
            }));
          }
        }

        seriesLabel += dataLabels.join(getConfig('data.separator.middle')) + getConfig('data.separator.end');
        seriesLabels.push(seriesLabel);
      }
    });
    ariaLabel += seriesLabels.join(getConfig('series.multiple.separator.middle')) + getConfig('series.multiple.separator.end');
    dom.setAttribute('aria-label', ariaLabel);
  }

  function replace(str, keyValues) {
    if (typeof str !== 'string') {
      return str;
    }

    var result = str;
    zrUtil.each(keyValues, function (value, key) {
      result = result.replace(new RegExp('\\{\\s*' + key + '\\s*\\}', 'g'), value);
    });
    return result;
  }

  function getConfig(path) {
    var userConfig = ariaModel.get(path);

    if (userConfig == null) {
      var pathArr = path.split('.');
      var result = lang.aria;

      for (var i = 0; i < pathArr.length; ++i) {
        result = result[pathArr[i]];
      }

      return result;
    } else {
      return userConfig;
    }
  }

  function getTitle() {
    var title = ecModel.getModel('title').option;

    if (title && title.length) {
      title = title[0];
    }

    return title && title.text;
  }

  function getSeriesTypeName(type) {
    return lang.series.typeNames[type] || '自定义图';
  }
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdmlzdWFsL2FyaWEuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi92aXN1YWwvYXJpYS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBsYW5nID0gcmVxdWlyZShcIi4uL2xhbmdcIik7XG5cbnZhciBfZGF0YVByb3ZpZGVyID0gcmVxdWlyZShcIi4uL2RhdGEvaGVscGVyL2RhdGFQcm92aWRlclwiKTtcblxudmFyIHJldHJpZXZlUmF3VmFsdWUgPSBfZGF0YVByb3ZpZGVyLnJldHJpZXZlUmF3VmFsdWU7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbmZ1bmN0aW9uIF9kZWZhdWx0KGRvbSwgZWNNb2RlbCkge1xuICB2YXIgYXJpYU1vZGVsID0gZWNNb2RlbC5nZXRNb2RlbCgnYXJpYScpO1xuXG4gIGlmICghYXJpYU1vZGVsLmdldCgnc2hvdycpKSB7XG4gICAgcmV0dXJuO1xuICB9IGVsc2UgaWYgKGFyaWFNb2RlbC5nZXQoJ2Rlc2NyaXB0aW9uJykpIHtcbiAgICBkb20uc2V0QXR0cmlidXRlKCdhcmlhLWxhYmVsJywgYXJpYU1vZGVsLmdldCgnZGVzY3JpcHRpb24nKSk7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHNlcmllc0NudCA9IDA7XG4gIGVjTW9kZWwuZWFjaFNlcmllcyhmdW5jdGlvbiAoc2VyaWVzTW9kZWwsIGlkeCkge1xuICAgICsrc2VyaWVzQ250O1xuICB9LCB0aGlzKTtcbiAgdmFyIG1heERhdGFDbnQgPSBhcmlhTW9kZWwuZ2V0KCdkYXRhLm1heENvdW50JykgfHwgMTA7XG4gIHZhciBtYXhTZXJpZXNDbnQgPSBhcmlhTW9kZWwuZ2V0KCdzZXJpZXMubWF4Q291bnQnKSB8fCAxMDtcbiAgdmFyIGRpc3BsYXlTZXJpZXNDbnQgPSBNYXRoLm1pbihzZXJpZXNDbnQsIG1heFNlcmllc0NudCk7XG4gIHZhciBhcmlhTGFiZWw7XG5cbiAgaWYgKHNlcmllc0NudCA8IDEpIHtcbiAgICAvLyBObyBzZXJpZXMsIG5vIGFyaWEgbGFiZWxcbiAgICByZXR1cm47XG4gIH0gZWxzZSB7XG4gICAgdmFyIHRpdGxlID0gZ2V0VGl0bGUoKTtcblxuICAgIGlmICh0aXRsZSkge1xuICAgICAgYXJpYUxhYmVsID0gcmVwbGFjZShnZXRDb25maWcoJ2dlbmVyYWwud2l0aFRpdGxlJyksIHtcbiAgICAgICAgdGl0bGU6IHRpdGxlXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgYXJpYUxhYmVsID0gZ2V0Q29uZmlnKCdnZW5lcmFsLndpdGhvdXRUaXRsZScpO1xuICAgIH1cblxuICAgIHZhciBzZXJpZXNMYWJlbHMgPSBbXTtcbiAgICB2YXIgcHJlZml4ID0gc2VyaWVzQ250ID4gMSA/ICdzZXJpZXMubXVsdGlwbGUucHJlZml4JyA6ICdzZXJpZXMuc2luZ2xlLnByZWZpeCc7XG4gICAgYXJpYUxhYmVsICs9IHJlcGxhY2UoZ2V0Q29uZmlnKHByZWZpeCksIHtcbiAgICAgIHNlcmllc0NvdW50OiBzZXJpZXNDbnRcbiAgICB9KTtcbiAgICBlY01vZGVsLmVhY2hTZXJpZXMoZnVuY3Rpb24gKHNlcmllc01vZGVsLCBpZHgpIHtcbiAgICAgIGlmIChpZHggPCBkaXNwbGF5U2VyaWVzQ250KSB7XG4gICAgICAgIHZhciBzZXJpZXNMYWJlbDtcbiAgICAgICAgdmFyIHNlcmllc05hbWUgPSBzZXJpZXNNb2RlbC5nZXQoJ25hbWUnKTtcbiAgICAgICAgdmFyIHNlcmllc1RwbCA9ICdzZXJpZXMuJyArIChzZXJpZXNDbnQgPiAxID8gJ211bHRpcGxlJyA6ICdzaW5nbGUnKSArICcuJztcbiAgICAgICAgc2VyaWVzTGFiZWwgPSBnZXRDb25maWcoc2VyaWVzTmFtZSA/IHNlcmllc1RwbCArICd3aXRoTmFtZScgOiBzZXJpZXNUcGwgKyAnd2l0aG91dE5hbWUnKTtcbiAgICAgICAgc2VyaWVzTGFiZWwgPSByZXBsYWNlKHNlcmllc0xhYmVsLCB7XG4gICAgICAgICAgc2VyaWVzSWQ6IHNlcmllc01vZGVsLnNlcmllc0luZGV4LFxuICAgICAgICAgIHNlcmllc05hbWU6IHNlcmllc01vZGVsLmdldCgnbmFtZScpLFxuICAgICAgICAgIHNlcmllc1R5cGU6IGdldFNlcmllc1R5cGVOYW1lKHNlcmllc01vZGVsLnN1YlR5cGUpXG4gICAgICAgIH0pO1xuICAgICAgICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgICAgICAgd2luZG93LmRhdGEgPSBkYXRhO1xuXG4gICAgICAgIGlmIChkYXRhLmNvdW50KCkgPiBtYXhEYXRhQ250KSB7XG4gICAgICAgICAgLy8gU2hvdyBwYXJ0IG9mIGRhdGFcbiAgICAgICAgICBzZXJpZXNMYWJlbCArPSByZXBsYWNlKGdldENvbmZpZygnZGF0YS5wYXJ0aWFsRGF0YScpLCB7XG4gICAgICAgICAgICBkaXNwbGF5Q250OiBtYXhEYXRhQ250XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgc2VyaWVzTGFiZWwgKz0gZ2V0Q29uZmlnKCdkYXRhLmFsbERhdGEnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBkYXRhTGFiZWxzID0gW107XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBkYXRhLmNvdW50KCk7IGkrKykge1xuICAgICAgICAgIGlmIChpIDwgbWF4RGF0YUNudCkge1xuICAgICAgICAgICAgdmFyIG5hbWUgPSBkYXRhLmdldE5hbWUoaSk7XG4gICAgICAgICAgICB2YXIgdmFsdWUgPSByZXRyaWV2ZVJhd1ZhbHVlKGRhdGEsIGkpO1xuICAgICAgICAgICAgZGF0YUxhYmVscy5wdXNoKHJlcGxhY2UobmFtZSA/IGdldENvbmZpZygnZGF0YS53aXRoTmFtZScpIDogZ2V0Q29uZmlnKCdkYXRhLndpdGhvdXROYW1lJyksIHtcbiAgICAgICAgICAgICAgbmFtZTogbmFtZSxcbiAgICAgICAgICAgICAgdmFsdWU6IHZhbHVlXG4gICAgICAgICAgICB9KSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgc2VyaWVzTGFiZWwgKz0gZGF0YUxhYmVscy5qb2luKGdldENvbmZpZygnZGF0YS5zZXBhcmF0b3IubWlkZGxlJykpICsgZ2V0Q29uZmlnKCdkYXRhLnNlcGFyYXRvci5lbmQnKTtcbiAgICAgICAgc2VyaWVzTGFiZWxzLnB1c2goc2VyaWVzTGFiZWwpO1xuICAgICAgfVxuICAgIH0pO1xuICAgIGFyaWFMYWJlbCArPSBzZXJpZXNMYWJlbHMuam9pbihnZXRDb25maWcoJ3Nlcmllcy5tdWx0aXBsZS5zZXBhcmF0b3IubWlkZGxlJykpICsgZ2V0Q29uZmlnKCdzZXJpZXMubXVsdGlwbGUuc2VwYXJhdG9yLmVuZCcpO1xuICAgIGRvbS5zZXRBdHRyaWJ1dGUoJ2FyaWEtbGFiZWwnLCBhcmlhTGFiZWwpO1xuICB9XG5cbiAgZnVuY3Rpb24gcmVwbGFjZShzdHIsIGtleVZhbHVlcykge1xuICAgIGlmICh0eXBlb2Ygc3RyICE9PSAnc3RyaW5nJykge1xuICAgICAgcmV0dXJuIHN0cjtcbiAgICB9XG5cbiAgICB2YXIgcmVzdWx0ID0gc3RyO1xuICAgIHpyVXRpbC5lYWNoKGtleVZhbHVlcywgZnVuY3Rpb24gKHZhbHVlLCBrZXkpIHtcbiAgICAgIHJlc3VsdCA9IHJlc3VsdC5yZXBsYWNlKG5ldyBSZWdFeHAoJ1xcXFx7XFxcXHMqJyArIGtleSArICdcXFxccypcXFxcfScsICdnJyksIHZhbHVlKTtcbiAgICB9KTtcbiAgICByZXR1cm4gcmVzdWx0O1xuICB9XG5cbiAgZnVuY3Rpb24gZ2V0Q29uZmlnKHBhdGgpIHtcbiAgICB2YXIgdXNlckNvbmZpZyA9IGFyaWFNb2RlbC5nZXQocGF0aCk7XG5cbiAgICBpZiAodXNlckNvbmZpZyA9PSBudWxsKSB7XG4gICAgICB2YXIgcGF0aEFyciA9IHBhdGguc3BsaXQoJy4nKTtcbiAgICAgIHZhciByZXN1bHQgPSBsYW5nLmFyaWE7XG5cbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcGF0aEFyci5sZW5ndGg7ICsraSkge1xuICAgICAgICByZXN1bHQgPSByZXN1bHRbcGF0aEFycltpXV07XG4gICAgICB9XG5cbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB1c2VyQ29uZmlnO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIGdldFRpdGxlKCkge1xuICAgIHZhciB0aXRsZSA9IGVjTW9kZWwuZ2V0TW9kZWwoJ3RpdGxlJykub3B0aW9uO1xuXG4gICAgaWYgKHRpdGxlICYmIHRpdGxlLmxlbmd0aCkge1xuICAgICAgdGl0bGUgPSB0aXRsZVswXTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGl0bGUgJiYgdGl0bGUudGV4dDtcbiAgfVxuXG4gIGZ1bmN0aW9uIGdldFNlcmllc1R5cGVOYW1lKHR5cGUpIHtcbiAgICByZXR1cm4gbGFuZy5zZXJpZXMudHlwZU5hbWVzW3R5cGVdIHx8ICfoh6rlrprkuYnlm74nO1xuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/visual/aria.js
