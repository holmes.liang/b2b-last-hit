__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getClientInfo", function() { return getClientInfo; });
/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uuid/v4 */ "./node_modules/uuid/v4.js");
/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(uuid_v4__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");


function getClientInfo(isfile) {
  var os;
  if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].os.macos) os = "MACOS";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].os.windows) os = "WIND";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].os.linux) os = "LINUX";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].os.unix) os = "UNIX";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].os.ios) os = "IOS";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].os.android) os = "ANDROID";else os = "OTH";
  var browser;
  if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].browser.chrome) browser = "CHROME";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].browser.safari) browser = "SAFARI";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].browser.opera) browser = "OPERA";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].browser.firefox) browser = "FIREFOX";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].browser.edge) browser = "MSEDGE";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].browser.ie) browser = "IE";else browser = "OTH";
  var type;
  if (isfile) type = "FUP";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].os.phone) type = "MBR";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].os.desk) type = "PCBR";else if (_common__WEBPACK_IMPORTED_MODULE_1__["Envs"].os.phone) type = "MAPP"; // todo
  else type = "OTH";
  var result = {
    "x-insmate-client-os": os,
    "x-insmate-client-type": type,
    "x-insmate-client-browser": browser,
    "x-insmate-traceid": uuid_v4__WEBPACK_IMPORTED_MODULE_0___default()().split("-").join("")
  };
  return result;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL2FqYXgtYmVmb3JlLnRzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tbW9uL2FqYXgtYmVmb3JlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB1dWlkdjQgZnJvbSBcInV1aWQvdjRcIjtcbmltcG9ydCB7IEVudnMgfSBmcm9tIFwiQGNvbW1vblwiO1xuXG5pbnRlcmZhY2UgSUFwaUJlZm9yZSB7XG4gIFwieC1pbnNtYXRlLWNsaWVudC1vc1wiOiBzdHJpbmc7XG4gIFwieC1pbnNtYXRlLWNsaWVudC10eXBlXCI6IHN0cmluZztcbiAgXCJ4LWluc21hdGUtY2xpZW50LWJyb3dzZXJcIjogc3RyaW5nO1xuICBcIngtaW5zbWF0ZS10cmFjZWlkXCI6IGFueTsgIC8vIOivt+axguWUr+S4gOagh+ivhlxufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q2xpZW50SW5mbyhpc2ZpbGU/OiBib29sZWFuKTogSUFwaUJlZm9yZSB7XG4gIGxldCBvcztcbiAgaWYgKEVudnMub3MubWFjb3MpIG9zID0gXCJNQUNPU1wiO1xuICBlbHNlIGlmIChFbnZzLm9zLndpbmRvd3MpIG9zID0gXCJXSU5EXCI7XG4gIGVsc2UgaWYgKEVudnMub3MubGludXgpIG9zID0gXCJMSU5VWFwiO1xuICBlbHNlIGlmIChFbnZzLm9zLnVuaXgpIG9zID0gXCJVTklYXCI7XG4gIGVsc2UgaWYgKEVudnMub3MuaW9zKSBvcyA9IFwiSU9TXCI7XG4gIGVsc2UgaWYgKEVudnMub3MuYW5kcm9pZCkgb3MgPSBcIkFORFJPSURcIjtcbiAgZWxzZSBvcyA9IFwiT1RIXCI7XG5cbiAgbGV0IGJyb3dzZXI7XG4gIGlmIChFbnZzLmJyb3dzZXIuY2hyb21lKSBicm93c2VyID0gXCJDSFJPTUVcIjtcbiAgZWxzZSBpZiAoRW52cy5icm93c2VyLnNhZmFyaSkgYnJvd3NlciA9IFwiU0FGQVJJXCI7XG4gIGVsc2UgaWYgKEVudnMuYnJvd3Nlci5vcGVyYSkgYnJvd3NlciA9IFwiT1BFUkFcIjtcbiAgZWxzZSBpZiAoRW52cy5icm93c2VyLmZpcmVmb3gpIGJyb3dzZXIgPSBcIkZJUkVGT1hcIjtcbiAgZWxzZSBpZiAoRW52cy5icm93c2VyLmVkZ2UpIGJyb3dzZXIgPSBcIk1TRURHRVwiO1xuICBlbHNlIGlmIChFbnZzLmJyb3dzZXIuaWUpIGJyb3dzZXIgPSBcIklFXCI7XG4gIGVsc2UgYnJvd3NlciA9IFwiT1RIXCI7XG5cbiAgbGV0IHR5cGU7XG4gIGlmIChpc2ZpbGUpIHR5cGUgPSBcIkZVUFwiO1xuICBlbHNlIGlmIChFbnZzLm9zLnBob25lKSB0eXBlID0gXCJNQlJcIjtcbiAgZWxzZSBpZiAoRW52cy5vcy5kZXNrKSB0eXBlID0gXCJQQ0JSXCI7XG4gIGVsc2UgaWYgKEVudnMub3MucGhvbmUpIHR5cGUgPSBcIk1BUFBcIjsgLy8gdG9kb1xuICBlbHNlIHR5cGUgPSBcIk9USFwiO1xuXG5cbiAgY29uc3QgcmVzdWx0OiBJQXBpQmVmb3JlID0ge1xuICAgICAgXCJ4LWluc21hdGUtY2xpZW50LW9zXCI6IG9zLFxuICAgICAgXCJ4LWluc21hdGUtY2xpZW50LXR5cGVcIjogdHlwZSxcbiAgICAgIFwieC1pbnNtYXRlLWNsaWVudC1icm93c2VyXCI6IGJyb3dzZXIsXG4gICAgICBcIngtaW5zbWF0ZS10cmFjZWlkXCI6IHV1aWR2NCgpLnNwbGl0KFwiLVwiKS5qb2luKFwiXCIpLFxuICAgIH1cbiAgO1xuXG4gIHJldHVybiByZXN1bHQ7XG59XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBUUE7QUFDQTtBQVFBO0FBQ0E7QUFHQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/ajax-before.ts
