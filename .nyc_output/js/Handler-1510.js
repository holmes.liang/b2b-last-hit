var util = __webpack_require__(/*! ./core/util */ "./node_modules/zrender/lib/core/util.js");

var vec2 = __webpack_require__(/*! ./core/vector */ "./node_modules/zrender/lib/core/vector.js");

var Draggable = __webpack_require__(/*! ./mixin/Draggable */ "./node_modules/zrender/lib/mixin/Draggable.js");

var Eventful = __webpack_require__(/*! ./mixin/Eventful */ "./node_modules/zrender/lib/mixin/Eventful.js");

var eventTool = __webpack_require__(/*! ./core/event */ "./node_modules/zrender/lib/core/event.js");

var GestureMgr = __webpack_require__(/*! ./core/GestureMgr */ "./node_modules/zrender/lib/core/GestureMgr.js");

var SILENT = 'silent';

function makeEventPacket(eveType, targetInfo, event) {
  return {
    type: eveType,
    event: event,
    // target can only be an element that is not silent.
    target: targetInfo.target,
    // topTarget can be a silent element.
    topTarget: targetInfo.topTarget,
    cancelBubble: false,
    offsetX: event.zrX,
    offsetY: event.zrY,
    gestureEvent: event.gestureEvent,
    pinchX: event.pinchX,
    pinchY: event.pinchY,
    pinchScale: event.pinchScale,
    wheelDelta: event.zrDelta,
    zrByTouch: event.zrByTouch,
    which: event.which,
    stop: stopEvent
  };
}

function stopEvent(event) {
  eventTool.stop(this.event);
}

function EmptyProxy() {}

EmptyProxy.prototype.dispose = function () {};

var handlerNames = ['click', 'dblclick', 'mousewheel', 'mouseout', 'mouseup', 'mousedown', 'mousemove', 'contextmenu'];
/**
 * @alias module:zrender/Handler
 * @constructor
 * @extends module:zrender/mixin/Eventful
 * @param {module:zrender/Storage} storage Storage instance.
 * @param {module:zrender/Painter} painter Painter instance.
 * @param {module:zrender/dom/HandlerProxy} proxy HandlerProxy instance.
 * @param {HTMLElement} painterRoot painter.root (not painter.getViewportRoot()).
 */

var Handler = function Handler(storage, painter, proxy, painterRoot) {
  Eventful.call(this);
  this.storage = storage;
  this.painter = painter;
  this.painterRoot = painterRoot;
  proxy = proxy || new EmptyProxy();
  /**
   * Proxy of event. can be Dom, WebGLSurface, etc.
   */

  this.proxy = null;
  /**
   * {target, topTarget, x, y}
   * @private
   * @type {Object}
   */

  this._hovered = {};
  /**
   * @private
   * @type {Date}
   */

  this._lastTouchMoment;
  /**
   * @private
   * @type {number}
   */

  this._lastX;
  /**
   * @private
   * @type {number}
   */

  this._lastY;
  /**
   * @private
   * @type {module:zrender/core/GestureMgr}
   */

  this._gestureMgr;
  Draggable.call(this);
  this.setHandlerProxy(proxy);
};

Handler.prototype = {
  constructor: Handler,
  setHandlerProxy: function setHandlerProxy(proxy) {
    if (this.proxy) {
      this.proxy.dispose();
    }

    if (proxy) {
      util.each(handlerNames, function (name) {
        proxy.on && proxy.on(name, this[name], this);
      }, this); // Attach handler

      proxy.handler = this;
    }

    this.proxy = proxy;
  },
  mousemove: function mousemove(event) {
    var x = event.zrX;
    var y = event.zrY;
    var lastHovered = this._hovered;
    var lastHoveredTarget = lastHovered.target; // If lastHoveredTarget is removed from zr (detected by '__zr') by some API call
    // (like 'setOption' or 'dispatchAction') in event handlers, we should find
    // lastHovered again here. Otherwise 'mouseout' can not be triggered normally.
    // See #6198.

    if (lastHoveredTarget && !lastHoveredTarget.__zr) {
      lastHovered = this.findHover(lastHovered.x, lastHovered.y);
      lastHoveredTarget = lastHovered.target;
    }

    var hovered = this._hovered = this.findHover(x, y);
    var hoveredTarget = hovered.target;
    var proxy = this.proxy;
    proxy.setCursor && proxy.setCursor(hoveredTarget ? hoveredTarget.cursor : 'default'); // Mouse out on previous hovered element

    if (lastHoveredTarget && hoveredTarget !== lastHoveredTarget) {
      this.dispatchToElement(lastHovered, 'mouseout', event);
    } // Mouse moving on one element


    this.dispatchToElement(hovered, 'mousemove', event); // Mouse over on a new element

    if (hoveredTarget && hoveredTarget !== lastHoveredTarget) {
      this.dispatchToElement(hovered, 'mouseover', event);
    }
  },
  mouseout: function mouseout(event) {
    this.dispatchToElement(this._hovered, 'mouseout', event); // There might be some doms created by upper layer application
    // at the same level of painter.getViewportRoot() (e.g., tooltip
    // dom created by echarts), where 'globalout' event should not
    // be triggered when mouse enters these doms. (But 'mouseout'
    // should be triggered at the original hovered element as usual).

    var element = event.toElement || event.relatedTarget;
    var innerDom;

    do {
      element = element && element.parentNode;
    } while (element && element.nodeType !== 9 && !(innerDom = element === this.painterRoot));

    !innerDom && this.trigger('globalout', {
      event: event
    });
  },

  /**
   * Resize
   */
  resize: function resize(event) {
    this._hovered = {};
  },

  /**
   * Dispatch event
   * @param {string} eventName
   * @param {event=} eventArgs
   */
  dispatch: function dispatch(eventName, eventArgs) {
    var handler = this[eventName];
    handler && handler.call(this, eventArgs);
  },

  /**
   * Dispose
   */
  dispose: function dispose() {
    this.proxy.dispose();
    this.storage = this.proxy = this.painter = null;
  },

  /**
   * 设置默认的cursor style
   * @param {string} [cursorStyle='default'] 例如 crosshair
   */
  setCursorStyle: function setCursorStyle(cursorStyle) {
    var proxy = this.proxy;
    proxy.setCursor && proxy.setCursor(cursorStyle);
  },

  /**
   * 事件分发代理
   *
   * @private
   * @param {Object} targetInfo {target, topTarget} 目标图形元素
   * @param {string} eventName 事件名称
   * @param {Object} event 事件对象
   */
  dispatchToElement: function dispatchToElement(targetInfo, eventName, event) {
    targetInfo = targetInfo || {};
    var el = targetInfo.target;

    if (el && el.silent) {
      return;
    }

    var eventHandler = 'on' + eventName;
    var eventPacket = makeEventPacket(eventName, targetInfo, event);

    while (el) {
      el[eventHandler] && (eventPacket.cancelBubble = el[eventHandler].call(el, eventPacket));
      el.trigger(eventName, eventPacket);
      el = el.parent;

      if (eventPacket.cancelBubble) {
        break;
      }
    }

    if (!eventPacket.cancelBubble) {
      // 冒泡到顶级 zrender 对象
      this.trigger(eventName, eventPacket); // 分发事件到用户自定义层
      // 用户有可能在全局 click 事件中 dispose，所以需要判断下 painter 是否存在

      this.painter && this.painter.eachOtherLayer(function (layer) {
        if (typeof layer[eventHandler] === 'function') {
          layer[eventHandler].call(layer, eventPacket);
        }

        if (layer.trigger) {
          layer.trigger(eventName, eventPacket);
        }
      });
    }
  },

  /**
   * @private
   * @param {number} x
   * @param {number} y
   * @param {module:zrender/graphic/Displayable} exclude
   * @return {model:zrender/Element}
   * @method
   */
  findHover: function findHover(x, y, exclude) {
    var list = this.storage.getDisplayList();
    var out = {
      x: x,
      y: y
    };

    for (var i = list.length - 1; i >= 0; i--) {
      var hoverCheckResult;

      if (list[i] !== exclude // getDisplayList may include ignored item in VML mode
      && !list[i].ignore && (hoverCheckResult = isHover(list[i], x, y))) {
        !out.topTarget && (out.topTarget = list[i]);

        if (hoverCheckResult !== SILENT) {
          out.target = list[i];
          break;
        }
      }
    }

    return out;
  },
  processGesture: function processGesture(event, stage) {
    if (!this._gestureMgr) {
      this._gestureMgr = new GestureMgr();
    }

    var gestureMgr = this._gestureMgr;
    stage === 'start' && gestureMgr.clear();
    var gestureInfo = gestureMgr.recognize(event, this.findHover(event.zrX, event.zrY, null).target, this.proxy.dom);
    stage === 'end' && gestureMgr.clear(); // Do not do any preventDefault here. Upper application do that if necessary.

    if (gestureInfo) {
      var type = gestureInfo.type;
      event.gestureEvent = type;
      this.dispatchToElement({
        target: gestureInfo.target
      }, type, gestureInfo.event);
    }
  }
}; // Common handlers

util.each(['click', 'mousedown', 'mouseup', 'mousewheel', 'dblclick', 'contextmenu'], function (name) {
  Handler.prototype[name] = function (event) {
    // Find hover again to avoid click event is dispatched manually. Or click is triggered without mouseover
    var hovered = this.findHover(event.zrX, event.zrY);
    var hoveredTarget = hovered.target;

    if (name === 'mousedown') {
      this._downEl = hoveredTarget;
      this._downPoint = [event.zrX, event.zrY]; // In case click triggered before mouseup

      this._upEl = hoveredTarget;
    } else if (name === 'mouseup') {
      this._upEl = hoveredTarget;
    } else if (name === 'click') {
      if (this._downEl !== this._upEl // Original click event is triggered on the whole canvas element,
      // including the case that `mousedown` - `mousemove` - `mouseup`,
      // which should be filtered, otherwise it will bring trouble to
      // pan and zoom.
      || !this._downPoint // Arbitrary value
      || vec2.dist(this._downPoint, [event.zrX, event.zrY]) > 4) {
        return;
      }

      this._downPoint = null;
    }

    this.dispatchToElement(hovered, name, event);
  };
});

function isHover(displayable, x, y) {
  if (displayable[displayable.rectHover ? 'rectContain' : 'contain'](x, y)) {
    var el = displayable;
    var isSilent;

    while (el) {
      // If clipped by ancestor.
      // FIXME: If clipPath has neither stroke nor fill,
      // el.clipPath.contain(x, y) will always return false.
      if (el.clipPath && !el.clipPath.contain(x, y)) {
        return false;
      }

      if (el.silent) {
        isSilent = true;
      }

      el = el.parent;
    }

    return isSilent ? SILENT : true;
  }

  return false;
}

util.mixin(Handler, Eventful);
util.mixin(Handler, Draggable);
var _default = Handler;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvSGFuZGxlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL0hhbmRsZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIHV0aWwgPSByZXF1aXJlKFwiLi9jb3JlL3V0aWxcIik7XG5cbnZhciB2ZWMyID0gcmVxdWlyZShcIi4vY29yZS92ZWN0b3JcIik7XG5cbnZhciBEcmFnZ2FibGUgPSByZXF1aXJlKFwiLi9taXhpbi9EcmFnZ2FibGVcIik7XG5cbnZhciBFdmVudGZ1bCA9IHJlcXVpcmUoXCIuL21peGluL0V2ZW50ZnVsXCIpO1xuXG52YXIgZXZlbnRUb29sID0gcmVxdWlyZShcIi4vY29yZS9ldmVudFwiKTtcblxudmFyIEdlc3R1cmVNZ3IgPSByZXF1aXJlKFwiLi9jb3JlL0dlc3R1cmVNZ3JcIik7XG5cbnZhciBTSUxFTlQgPSAnc2lsZW50JztcblxuZnVuY3Rpb24gbWFrZUV2ZW50UGFja2V0KGV2ZVR5cGUsIHRhcmdldEluZm8sIGV2ZW50KSB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogZXZlVHlwZSxcbiAgICBldmVudDogZXZlbnQsXG4gICAgLy8gdGFyZ2V0IGNhbiBvbmx5IGJlIGFuIGVsZW1lbnQgdGhhdCBpcyBub3Qgc2lsZW50LlxuICAgIHRhcmdldDogdGFyZ2V0SW5mby50YXJnZXQsXG4gICAgLy8gdG9wVGFyZ2V0IGNhbiBiZSBhIHNpbGVudCBlbGVtZW50LlxuICAgIHRvcFRhcmdldDogdGFyZ2V0SW5mby50b3BUYXJnZXQsXG4gICAgY2FuY2VsQnViYmxlOiBmYWxzZSxcbiAgICBvZmZzZXRYOiBldmVudC56clgsXG4gICAgb2Zmc2V0WTogZXZlbnQuenJZLFxuICAgIGdlc3R1cmVFdmVudDogZXZlbnQuZ2VzdHVyZUV2ZW50LFxuICAgIHBpbmNoWDogZXZlbnQucGluY2hYLFxuICAgIHBpbmNoWTogZXZlbnQucGluY2hZLFxuICAgIHBpbmNoU2NhbGU6IGV2ZW50LnBpbmNoU2NhbGUsXG4gICAgd2hlZWxEZWx0YTogZXZlbnQuenJEZWx0YSxcbiAgICB6ckJ5VG91Y2g6IGV2ZW50LnpyQnlUb3VjaCxcbiAgICB3aGljaDogZXZlbnQud2hpY2gsXG4gICAgc3RvcDogc3RvcEV2ZW50XG4gIH07XG59XG5cbmZ1bmN0aW9uIHN0b3BFdmVudChldmVudCkge1xuICBldmVudFRvb2wuc3RvcCh0aGlzLmV2ZW50KTtcbn1cblxuZnVuY3Rpb24gRW1wdHlQcm94eSgpIHt9XG5cbkVtcHR5UHJveHkucHJvdG90eXBlLmRpc3Bvc2UgPSBmdW5jdGlvbiAoKSB7fTtcblxudmFyIGhhbmRsZXJOYW1lcyA9IFsnY2xpY2snLCAnZGJsY2xpY2snLCAnbW91c2V3aGVlbCcsICdtb3VzZW91dCcsICdtb3VzZXVwJywgJ21vdXNlZG93bicsICdtb3VzZW1vdmUnLCAnY29udGV4dG1lbnUnXTtcbi8qKlxuICogQGFsaWFzIG1vZHVsZTp6cmVuZGVyL0hhbmRsZXJcbiAqIEBjb25zdHJ1Y3RvclxuICogQGV4dGVuZHMgbW9kdWxlOnpyZW5kZXIvbWl4aW4vRXZlbnRmdWxcbiAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvU3RvcmFnZX0gc3RvcmFnZSBTdG9yYWdlIGluc3RhbmNlLlxuICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9QYWludGVyfSBwYWludGVyIFBhaW50ZXIgaW5zdGFuY2UuXG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL2RvbS9IYW5kbGVyUHJveHl9IHByb3h5IEhhbmRsZXJQcm94eSBpbnN0YW5jZS5cbiAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IHBhaW50ZXJSb290IHBhaW50ZXIucm9vdCAobm90IHBhaW50ZXIuZ2V0Vmlld3BvcnRSb290KCkpLlxuICovXG5cbnZhciBIYW5kbGVyID0gZnVuY3Rpb24gKHN0b3JhZ2UsIHBhaW50ZXIsIHByb3h5LCBwYWludGVyUm9vdCkge1xuICBFdmVudGZ1bC5jYWxsKHRoaXMpO1xuICB0aGlzLnN0b3JhZ2UgPSBzdG9yYWdlO1xuICB0aGlzLnBhaW50ZXIgPSBwYWludGVyO1xuICB0aGlzLnBhaW50ZXJSb290ID0gcGFpbnRlclJvb3Q7XG4gIHByb3h5ID0gcHJveHkgfHwgbmV3IEVtcHR5UHJveHkoKTtcbiAgLyoqXG4gICAqIFByb3h5IG9mIGV2ZW50LiBjYW4gYmUgRG9tLCBXZWJHTFN1cmZhY2UsIGV0Yy5cbiAgICovXG5cbiAgdGhpcy5wcm94eSA9IG51bGw7XG4gIC8qKlxuICAgKiB7dGFyZ2V0LCB0b3BUYXJnZXQsIHgsIHl9XG4gICAqIEBwcml2YXRlXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuXG4gIHRoaXMuX2hvdmVyZWQgPSB7fTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEB0eXBlIHtEYXRlfVxuICAgKi9cblxuICB0aGlzLl9sYXN0VG91Y2hNb21lbnQ7XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cblxuICB0aGlzLl9sYXN0WDtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuXG4gIHRoaXMuX2xhc3RZO1xuICAvKipcbiAgICogQHByaXZhdGVcbiAgICogQHR5cGUge21vZHVsZTp6cmVuZGVyL2NvcmUvR2VzdHVyZU1ncn1cbiAgICovXG5cbiAgdGhpcy5fZ2VzdHVyZU1ncjtcbiAgRHJhZ2dhYmxlLmNhbGwodGhpcyk7XG4gIHRoaXMuc2V0SGFuZGxlclByb3h5KHByb3h5KTtcbn07XG5cbkhhbmRsZXIucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogSGFuZGxlcixcbiAgc2V0SGFuZGxlclByb3h5OiBmdW5jdGlvbiAocHJveHkpIHtcbiAgICBpZiAodGhpcy5wcm94eSkge1xuICAgICAgdGhpcy5wcm94eS5kaXNwb3NlKCk7XG4gICAgfVxuXG4gICAgaWYgKHByb3h5KSB7XG4gICAgICB1dGlsLmVhY2goaGFuZGxlck5hbWVzLCBmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICBwcm94eS5vbiAmJiBwcm94eS5vbihuYW1lLCB0aGlzW25hbWVdLCB0aGlzKTtcbiAgICAgIH0sIHRoaXMpOyAvLyBBdHRhY2ggaGFuZGxlclxuXG4gICAgICBwcm94eS5oYW5kbGVyID0gdGhpcztcbiAgICB9XG5cbiAgICB0aGlzLnByb3h5ID0gcHJveHk7XG4gIH0sXG4gIG1vdXNlbW92ZTogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgdmFyIHggPSBldmVudC56clg7XG4gICAgdmFyIHkgPSBldmVudC56clk7XG4gICAgdmFyIGxhc3RIb3ZlcmVkID0gdGhpcy5faG92ZXJlZDtcbiAgICB2YXIgbGFzdEhvdmVyZWRUYXJnZXQgPSBsYXN0SG92ZXJlZC50YXJnZXQ7IC8vIElmIGxhc3RIb3ZlcmVkVGFyZ2V0IGlzIHJlbW92ZWQgZnJvbSB6ciAoZGV0ZWN0ZWQgYnkgJ19fenInKSBieSBzb21lIEFQSSBjYWxsXG4gICAgLy8gKGxpa2UgJ3NldE9wdGlvbicgb3IgJ2Rpc3BhdGNoQWN0aW9uJykgaW4gZXZlbnQgaGFuZGxlcnMsIHdlIHNob3VsZCBmaW5kXG4gICAgLy8gbGFzdEhvdmVyZWQgYWdhaW4gaGVyZS4gT3RoZXJ3aXNlICdtb3VzZW91dCcgY2FuIG5vdCBiZSB0cmlnZ2VyZWQgbm9ybWFsbHkuXG4gICAgLy8gU2VlICM2MTk4LlxuXG4gICAgaWYgKGxhc3RIb3ZlcmVkVGFyZ2V0ICYmICFsYXN0SG92ZXJlZFRhcmdldC5fX3pyKSB7XG4gICAgICBsYXN0SG92ZXJlZCA9IHRoaXMuZmluZEhvdmVyKGxhc3RIb3ZlcmVkLngsIGxhc3RIb3ZlcmVkLnkpO1xuICAgICAgbGFzdEhvdmVyZWRUYXJnZXQgPSBsYXN0SG92ZXJlZC50YXJnZXQ7XG4gICAgfVxuXG4gICAgdmFyIGhvdmVyZWQgPSB0aGlzLl9ob3ZlcmVkID0gdGhpcy5maW5kSG92ZXIoeCwgeSk7XG4gICAgdmFyIGhvdmVyZWRUYXJnZXQgPSBob3ZlcmVkLnRhcmdldDtcbiAgICB2YXIgcHJveHkgPSB0aGlzLnByb3h5O1xuICAgIHByb3h5LnNldEN1cnNvciAmJiBwcm94eS5zZXRDdXJzb3IoaG92ZXJlZFRhcmdldCA/IGhvdmVyZWRUYXJnZXQuY3Vyc29yIDogJ2RlZmF1bHQnKTsgLy8gTW91c2Ugb3V0IG9uIHByZXZpb3VzIGhvdmVyZWQgZWxlbWVudFxuXG4gICAgaWYgKGxhc3RIb3ZlcmVkVGFyZ2V0ICYmIGhvdmVyZWRUYXJnZXQgIT09IGxhc3RIb3ZlcmVkVGFyZ2V0KSB7XG4gICAgICB0aGlzLmRpc3BhdGNoVG9FbGVtZW50KGxhc3RIb3ZlcmVkLCAnbW91c2VvdXQnLCBldmVudCk7XG4gICAgfSAvLyBNb3VzZSBtb3Zpbmcgb24gb25lIGVsZW1lbnRcblxuXG4gICAgdGhpcy5kaXNwYXRjaFRvRWxlbWVudChob3ZlcmVkLCAnbW91c2Vtb3ZlJywgZXZlbnQpOyAvLyBNb3VzZSBvdmVyIG9uIGEgbmV3IGVsZW1lbnRcblxuICAgIGlmIChob3ZlcmVkVGFyZ2V0ICYmIGhvdmVyZWRUYXJnZXQgIT09IGxhc3RIb3ZlcmVkVGFyZ2V0KSB7XG4gICAgICB0aGlzLmRpc3BhdGNoVG9FbGVtZW50KGhvdmVyZWQsICdtb3VzZW92ZXInLCBldmVudCk7XG4gICAgfVxuICB9LFxuICBtb3VzZW91dDogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgdGhpcy5kaXNwYXRjaFRvRWxlbWVudCh0aGlzLl9ob3ZlcmVkLCAnbW91c2VvdXQnLCBldmVudCk7IC8vIFRoZXJlIG1pZ2h0IGJlIHNvbWUgZG9tcyBjcmVhdGVkIGJ5IHVwcGVyIGxheWVyIGFwcGxpY2F0aW9uXG4gICAgLy8gYXQgdGhlIHNhbWUgbGV2ZWwgb2YgcGFpbnRlci5nZXRWaWV3cG9ydFJvb3QoKSAoZS5nLiwgdG9vbHRpcFxuICAgIC8vIGRvbSBjcmVhdGVkIGJ5IGVjaGFydHMpLCB3aGVyZSAnZ2xvYmFsb3V0JyBldmVudCBzaG91bGQgbm90XG4gICAgLy8gYmUgdHJpZ2dlcmVkIHdoZW4gbW91c2UgZW50ZXJzIHRoZXNlIGRvbXMuIChCdXQgJ21vdXNlb3V0J1xuICAgIC8vIHNob3VsZCBiZSB0cmlnZ2VyZWQgYXQgdGhlIG9yaWdpbmFsIGhvdmVyZWQgZWxlbWVudCBhcyB1c3VhbCkuXG5cbiAgICB2YXIgZWxlbWVudCA9IGV2ZW50LnRvRWxlbWVudCB8fCBldmVudC5yZWxhdGVkVGFyZ2V0O1xuICAgIHZhciBpbm5lckRvbTtcblxuICAgIGRvIHtcbiAgICAgIGVsZW1lbnQgPSBlbGVtZW50ICYmIGVsZW1lbnQucGFyZW50Tm9kZTtcbiAgICB9IHdoaWxlIChlbGVtZW50ICYmIGVsZW1lbnQubm9kZVR5cGUgIT09IDkgJiYgIShpbm5lckRvbSA9IGVsZW1lbnQgPT09IHRoaXMucGFpbnRlclJvb3QpKTtcblxuICAgICFpbm5lckRvbSAmJiB0aGlzLnRyaWdnZXIoJ2dsb2JhbG91dCcsIHtcbiAgICAgIGV2ZW50OiBldmVudFxuICAgIH0pO1xuICB9LFxuXG4gIC8qKlxuICAgKiBSZXNpemVcbiAgICovXG4gIHJlc2l6ZTogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgdGhpcy5faG92ZXJlZCA9IHt9O1xuICB9LFxuXG4gIC8qKlxuICAgKiBEaXNwYXRjaCBldmVudFxuICAgKiBAcGFyYW0ge3N0cmluZ30gZXZlbnROYW1lXG4gICAqIEBwYXJhbSB7ZXZlbnQ9fSBldmVudEFyZ3NcbiAgICovXG4gIGRpc3BhdGNoOiBmdW5jdGlvbiAoZXZlbnROYW1lLCBldmVudEFyZ3MpIHtcbiAgICB2YXIgaGFuZGxlciA9IHRoaXNbZXZlbnROYW1lXTtcbiAgICBoYW5kbGVyICYmIGhhbmRsZXIuY2FsbCh0aGlzLCBldmVudEFyZ3MpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBEaXNwb3NlXG4gICAqL1xuICBkaXNwb3NlOiBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5wcm94eS5kaXNwb3NlKCk7XG4gICAgdGhpcy5zdG9yYWdlID0gdGhpcy5wcm94eSA9IHRoaXMucGFpbnRlciA9IG51bGw7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOiuvue9rum7mOiupOeahGN1cnNvciBzdHlsZVxuICAgKiBAcGFyYW0ge3N0cmluZ30gW2N1cnNvclN0eWxlPSdkZWZhdWx0J10g5L6L5aaCIGNyb3NzaGFpclxuICAgKi9cbiAgc2V0Q3Vyc29yU3R5bGU6IGZ1bmN0aW9uIChjdXJzb3JTdHlsZSkge1xuICAgIHZhciBwcm94eSA9IHRoaXMucHJveHk7XG4gICAgcHJveHkuc2V0Q3Vyc29yICYmIHByb3h5LnNldEN1cnNvcihjdXJzb3JTdHlsZSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOS6i+S7tuWIhuWPkeS7o+eQhlxuICAgKlxuICAgKiBAcHJpdmF0ZVxuICAgKiBAcGFyYW0ge09iamVjdH0gdGFyZ2V0SW5mbyB7dGFyZ2V0LCB0b3BUYXJnZXR9IOebruagh+WbvuW9ouWFg+e0oFxuICAgKiBAcGFyYW0ge3N0cmluZ30gZXZlbnROYW1lIOS6i+S7tuWQjeensFxuICAgKiBAcGFyYW0ge09iamVjdH0gZXZlbnQg5LqL5Lu25a+56LGhXG4gICAqL1xuICBkaXNwYXRjaFRvRWxlbWVudDogZnVuY3Rpb24gKHRhcmdldEluZm8sIGV2ZW50TmFtZSwgZXZlbnQpIHtcbiAgICB0YXJnZXRJbmZvID0gdGFyZ2V0SW5mbyB8fCB7fTtcbiAgICB2YXIgZWwgPSB0YXJnZXRJbmZvLnRhcmdldDtcblxuICAgIGlmIChlbCAmJiBlbC5zaWxlbnQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgZXZlbnRIYW5kbGVyID0gJ29uJyArIGV2ZW50TmFtZTtcbiAgICB2YXIgZXZlbnRQYWNrZXQgPSBtYWtlRXZlbnRQYWNrZXQoZXZlbnROYW1lLCB0YXJnZXRJbmZvLCBldmVudCk7XG5cbiAgICB3aGlsZSAoZWwpIHtcbiAgICAgIGVsW2V2ZW50SGFuZGxlcl0gJiYgKGV2ZW50UGFja2V0LmNhbmNlbEJ1YmJsZSA9IGVsW2V2ZW50SGFuZGxlcl0uY2FsbChlbCwgZXZlbnRQYWNrZXQpKTtcbiAgICAgIGVsLnRyaWdnZXIoZXZlbnROYW1lLCBldmVudFBhY2tldCk7XG4gICAgICBlbCA9IGVsLnBhcmVudDtcblxuICAgICAgaWYgKGV2ZW50UGFja2V0LmNhbmNlbEJ1YmJsZSkge1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoIWV2ZW50UGFja2V0LmNhbmNlbEJ1YmJsZSkge1xuICAgICAgLy8g5YaS5rOh5Yiw6aG257qnIHpyZW5kZXIg5a+56LGhXG4gICAgICB0aGlzLnRyaWdnZXIoZXZlbnROYW1lLCBldmVudFBhY2tldCk7IC8vIOWIhuWPkeS6i+S7tuWIsOeUqOaIt+iHquWumuS5ieWxglxuICAgICAgLy8g55So5oi35pyJ5Y+v6IO95Zyo5YWo5bGAIGNsaWNrIOS6i+S7tuS4rSBkaXNwb3Nl77yM5omA5Lul6ZyA6KaB5Yik5pat5LiLIHBhaW50ZXIg5piv5ZCm5a2Y5ZyoXG5cbiAgICAgIHRoaXMucGFpbnRlciAmJiB0aGlzLnBhaW50ZXIuZWFjaE90aGVyTGF5ZXIoZnVuY3Rpb24gKGxheWVyKSB7XG4gICAgICAgIGlmICh0eXBlb2YgbGF5ZXJbZXZlbnRIYW5kbGVyXSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgIGxheWVyW2V2ZW50SGFuZGxlcl0uY2FsbChsYXllciwgZXZlbnRQYWNrZXQpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGxheWVyLnRyaWdnZXIpIHtcbiAgICAgICAgICBsYXllci50cmlnZ2VyKGV2ZW50TmFtZSwgZXZlbnRQYWNrZXQpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEBwYXJhbSB7bnVtYmVyfSB4XG4gICAqIEBwYXJhbSB7bnVtYmVyfSB5XG4gICAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvZ3JhcGhpYy9EaXNwbGF5YWJsZX0gZXhjbHVkZVxuICAgKiBAcmV0dXJuIHttb2RlbDp6cmVuZGVyL0VsZW1lbnR9XG4gICAqIEBtZXRob2RcbiAgICovXG4gIGZpbmRIb3ZlcjogZnVuY3Rpb24gKHgsIHksIGV4Y2x1ZGUpIHtcbiAgICB2YXIgbGlzdCA9IHRoaXMuc3RvcmFnZS5nZXREaXNwbGF5TGlzdCgpO1xuICAgIHZhciBvdXQgPSB7XG4gICAgICB4OiB4LFxuICAgICAgeTogeVxuICAgIH07XG5cbiAgICBmb3IgKHZhciBpID0gbGlzdC5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgICAgdmFyIGhvdmVyQ2hlY2tSZXN1bHQ7XG5cbiAgICAgIGlmIChsaXN0W2ldICE9PSBleGNsdWRlIC8vIGdldERpc3BsYXlMaXN0IG1heSBpbmNsdWRlIGlnbm9yZWQgaXRlbSBpbiBWTUwgbW9kZVxuICAgICAgJiYgIWxpc3RbaV0uaWdub3JlICYmIChob3ZlckNoZWNrUmVzdWx0ID0gaXNIb3ZlcihsaXN0W2ldLCB4LCB5KSkpIHtcbiAgICAgICAgIW91dC50b3BUYXJnZXQgJiYgKG91dC50b3BUYXJnZXQgPSBsaXN0W2ldKTtcblxuICAgICAgICBpZiAoaG92ZXJDaGVja1Jlc3VsdCAhPT0gU0lMRU5UKSB7XG4gICAgICAgICAgb3V0LnRhcmdldCA9IGxpc3RbaV07XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gb3V0O1xuICB9LFxuICBwcm9jZXNzR2VzdHVyZTogZnVuY3Rpb24gKGV2ZW50LCBzdGFnZSkge1xuICAgIGlmICghdGhpcy5fZ2VzdHVyZU1ncikge1xuICAgICAgdGhpcy5fZ2VzdHVyZU1nciA9IG5ldyBHZXN0dXJlTWdyKCk7XG4gICAgfVxuXG4gICAgdmFyIGdlc3R1cmVNZ3IgPSB0aGlzLl9nZXN0dXJlTWdyO1xuICAgIHN0YWdlID09PSAnc3RhcnQnICYmIGdlc3R1cmVNZ3IuY2xlYXIoKTtcbiAgICB2YXIgZ2VzdHVyZUluZm8gPSBnZXN0dXJlTWdyLnJlY29nbml6ZShldmVudCwgdGhpcy5maW5kSG92ZXIoZXZlbnQuenJYLCBldmVudC56clksIG51bGwpLnRhcmdldCwgdGhpcy5wcm94eS5kb20pO1xuICAgIHN0YWdlID09PSAnZW5kJyAmJiBnZXN0dXJlTWdyLmNsZWFyKCk7IC8vIERvIG5vdCBkbyBhbnkgcHJldmVudERlZmF1bHQgaGVyZS4gVXBwZXIgYXBwbGljYXRpb24gZG8gdGhhdCBpZiBuZWNlc3NhcnkuXG5cbiAgICBpZiAoZ2VzdHVyZUluZm8pIHtcbiAgICAgIHZhciB0eXBlID0gZ2VzdHVyZUluZm8udHlwZTtcbiAgICAgIGV2ZW50Lmdlc3R1cmVFdmVudCA9IHR5cGU7XG4gICAgICB0aGlzLmRpc3BhdGNoVG9FbGVtZW50KHtcbiAgICAgICAgdGFyZ2V0OiBnZXN0dXJlSW5mby50YXJnZXRcbiAgICAgIH0sIHR5cGUsIGdlc3R1cmVJbmZvLmV2ZW50KTtcbiAgICB9XG4gIH1cbn07IC8vIENvbW1vbiBoYW5kbGVyc1xuXG51dGlsLmVhY2goWydjbGljaycsICdtb3VzZWRvd24nLCAnbW91c2V1cCcsICdtb3VzZXdoZWVsJywgJ2RibGNsaWNrJywgJ2NvbnRleHRtZW51J10sIGZ1bmN0aW9uIChuYW1lKSB7XG4gIEhhbmRsZXIucHJvdG90eXBlW25hbWVdID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgLy8gRmluZCBob3ZlciBhZ2FpbiB0byBhdm9pZCBjbGljayBldmVudCBpcyBkaXNwYXRjaGVkIG1hbnVhbGx5LiBPciBjbGljayBpcyB0cmlnZ2VyZWQgd2l0aG91dCBtb3VzZW92ZXJcbiAgICB2YXIgaG92ZXJlZCA9IHRoaXMuZmluZEhvdmVyKGV2ZW50LnpyWCwgZXZlbnQuenJZKTtcbiAgICB2YXIgaG92ZXJlZFRhcmdldCA9IGhvdmVyZWQudGFyZ2V0O1xuXG4gICAgaWYgKG5hbWUgPT09ICdtb3VzZWRvd24nKSB7XG4gICAgICB0aGlzLl9kb3duRWwgPSBob3ZlcmVkVGFyZ2V0O1xuICAgICAgdGhpcy5fZG93blBvaW50ID0gW2V2ZW50LnpyWCwgZXZlbnQuenJZXTsgLy8gSW4gY2FzZSBjbGljayB0cmlnZ2VyZWQgYmVmb3JlIG1vdXNldXBcblxuICAgICAgdGhpcy5fdXBFbCA9IGhvdmVyZWRUYXJnZXQ7XG4gICAgfSBlbHNlIGlmIChuYW1lID09PSAnbW91c2V1cCcpIHtcbiAgICAgIHRoaXMuX3VwRWwgPSBob3ZlcmVkVGFyZ2V0O1xuICAgIH0gZWxzZSBpZiAobmFtZSA9PT0gJ2NsaWNrJykge1xuICAgICAgaWYgKHRoaXMuX2Rvd25FbCAhPT0gdGhpcy5fdXBFbCAvLyBPcmlnaW5hbCBjbGljayBldmVudCBpcyB0cmlnZ2VyZWQgb24gdGhlIHdob2xlIGNhbnZhcyBlbGVtZW50LFxuICAgICAgLy8gaW5jbHVkaW5nIHRoZSBjYXNlIHRoYXQgYG1vdXNlZG93bmAgLSBgbW91c2Vtb3ZlYCAtIGBtb3VzZXVwYCxcbiAgICAgIC8vIHdoaWNoIHNob3VsZCBiZSBmaWx0ZXJlZCwgb3RoZXJ3aXNlIGl0IHdpbGwgYnJpbmcgdHJvdWJsZSB0b1xuICAgICAgLy8gcGFuIGFuZCB6b29tLlxuICAgICAgfHwgIXRoaXMuX2Rvd25Qb2ludCAvLyBBcmJpdHJhcnkgdmFsdWVcbiAgICAgIHx8IHZlYzIuZGlzdCh0aGlzLl9kb3duUG9pbnQsIFtldmVudC56clgsIGV2ZW50LnpyWV0pID4gNCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX2Rvd25Qb2ludCA9IG51bGw7XG4gICAgfVxuXG4gICAgdGhpcy5kaXNwYXRjaFRvRWxlbWVudChob3ZlcmVkLCBuYW1lLCBldmVudCk7XG4gIH07XG59KTtcblxuZnVuY3Rpb24gaXNIb3ZlcihkaXNwbGF5YWJsZSwgeCwgeSkge1xuICBpZiAoZGlzcGxheWFibGVbZGlzcGxheWFibGUucmVjdEhvdmVyID8gJ3JlY3RDb250YWluJyA6ICdjb250YWluJ10oeCwgeSkpIHtcbiAgICB2YXIgZWwgPSBkaXNwbGF5YWJsZTtcbiAgICB2YXIgaXNTaWxlbnQ7XG5cbiAgICB3aGlsZSAoZWwpIHtcbiAgICAgIC8vIElmIGNsaXBwZWQgYnkgYW5jZXN0b3IuXG4gICAgICAvLyBGSVhNRTogSWYgY2xpcFBhdGggaGFzIG5laXRoZXIgc3Ryb2tlIG5vciBmaWxsLFxuICAgICAgLy8gZWwuY2xpcFBhdGguY29udGFpbih4LCB5KSB3aWxsIGFsd2F5cyByZXR1cm4gZmFsc2UuXG4gICAgICBpZiAoZWwuY2xpcFBhdGggJiYgIWVsLmNsaXBQYXRoLmNvbnRhaW4oeCwgeSkpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICBpZiAoZWwuc2lsZW50KSB7XG4gICAgICAgIGlzU2lsZW50ID0gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgZWwgPSBlbC5wYXJlbnQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIGlzU2lsZW50ID8gU0lMRU5UIDogdHJ1ZTtcbiAgfVxuXG4gIHJldHVybiBmYWxzZTtcbn1cblxudXRpbC5taXhpbihIYW5kbGVyLCBFdmVudGZ1bCk7XG51dGlsLm1peGluKEhhbmRsZXIsIERyYWdnYWJsZSk7XG52YXIgX2RlZmF1bHQgPSBIYW5kbGVyO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpCQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBbE1BO0FBQ0E7QUFvTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/Handler.js
