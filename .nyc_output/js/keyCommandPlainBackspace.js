/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule keyCommandPlainBackspace
 * @format
 * 
 */


var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var UnicodeUtils = __webpack_require__(/*! fbjs/lib/UnicodeUtils */ "./node_modules/fbjs/lib/UnicodeUtils.js");

var moveSelectionBackward = __webpack_require__(/*! ./moveSelectionBackward */ "./node_modules/draft-js/lib/moveSelectionBackward.js");

var removeTextWithStrategy = __webpack_require__(/*! ./removeTextWithStrategy */ "./node_modules/draft-js/lib/removeTextWithStrategy.js");
/**
 * Remove the selected range. If the cursor is collapsed, remove the preceding
 * character. This operation is Unicode-aware, so removing a single character
 * will remove a surrogate pair properly as well.
 */


function keyCommandPlainBackspace(editorState) {
  var afterRemoval = removeTextWithStrategy(editorState, function (strategyState) {
    var selection = strategyState.getSelection();
    var content = strategyState.getCurrentContent();
    var key = selection.getAnchorKey();
    var offset = selection.getAnchorOffset();
    var charBehind = content.getBlockForKey(key).getText()[offset - 1];
    return moveSelectionBackward(strategyState, charBehind ? UnicodeUtils.getUTF16Length(charBehind, 0) : 1);
  }, 'backward');

  if (afterRemoval === editorState.getCurrentContent()) {
    return editorState;
  }

  var selection = editorState.getSelection();
  return EditorState.push(editorState, afterRemoval.set('selectionBefore', selection), selection.isCollapsed() ? 'backspace-character' : 'remove-range');
}

module.exports = keyCommandPlainBackspace;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRQbGFpbkJhY2tzcGFjZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9rZXlDb21tYW5kUGxhaW5CYWNrc3BhY2UuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBrZXlDb21tYW5kUGxhaW5CYWNrc3BhY2VcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xudmFyIFVuaWNvZGVVdGlscyA9IHJlcXVpcmUoJ2ZianMvbGliL1VuaWNvZGVVdGlscycpO1xuXG52YXIgbW92ZVNlbGVjdGlvbkJhY2t3YXJkID0gcmVxdWlyZSgnLi9tb3ZlU2VsZWN0aW9uQmFja3dhcmQnKTtcbnZhciByZW1vdmVUZXh0V2l0aFN0cmF0ZWd5ID0gcmVxdWlyZSgnLi9yZW1vdmVUZXh0V2l0aFN0cmF0ZWd5Jyk7XG5cbi8qKlxuICogUmVtb3ZlIHRoZSBzZWxlY3RlZCByYW5nZS4gSWYgdGhlIGN1cnNvciBpcyBjb2xsYXBzZWQsIHJlbW92ZSB0aGUgcHJlY2VkaW5nXG4gKiBjaGFyYWN0ZXIuIFRoaXMgb3BlcmF0aW9uIGlzIFVuaWNvZGUtYXdhcmUsIHNvIHJlbW92aW5nIGEgc2luZ2xlIGNoYXJhY3RlclxuICogd2lsbCByZW1vdmUgYSBzdXJyb2dhdGUgcGFpciBwcm9wZXJseSBhcyB3ZWxsLlxuICovXG5mdW5jdGlvbiBrZXlDb21tYW5kUGxhaW5CYWNrc3BhY2UoZWRpdG9yU3RhdGUpIHtcbiAgdmFyIGFmdGVyUmVtb3ZhbCA9IHJlbW92ZVRleHRXaXRoU3RyYXRlZ3koZWRpdG9yU3RhdGUsIGZ1bmN0aW9uIChzdHJhdGVneVN0YXRlKSB7XG4gICAgdmFyIHNlbGVjdGlvbiA9IHN0cmF0ZWd5U3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gICAgdmFyIGNvbnRlbnQgPSBzdHJhdGVneVN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdmFyIGtleSA9IHNlbGVjdGlvbi5nZXRBbmNob3JLZXkoKTtcbiAgICB2YXIgb2Zmc2V0ID0gc2VsZWN0aW9uLmdldEFuY2hvck9mZnNldCgpO1xuICAgIHZhciBjaGFyQmVoaW5kID0gY29udGVudC5nZXRCbG9ja0ZvcktleShrZXkpLmdldFRleHQoKVtvZmZzZXQgLSAxXTtcbiAgICByZXR1cm4gbW92ZVNlbGVjdGlvbkJhY2t3YXJkKHN0cmF0ZWd5U3RhdGUsIGNoYXJCZWhpbmQgPyBVbmljb2RlVXRpbHMuZ2V0VVRGMTZMZW5ndGgoY2hhckJlaGluZCwgMCkgOiAxKTtcbiAgfSwgJ2JhY2t3YXJkJyk7XG5cbiAgaWYgKGFmdGVyUmVtb3ZhbCA9PT0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKSkge1xuICAgIHJldHVybiBlZGl0b3JTdGF0ZTtcbiAgfVxuXG4gIHZhciBzZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcbiAgcmV0dXJuIEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIGFmdGVyUmVtb3ZhbC5zZXQoJ3NlbGVjdGlvbkJlZm9yZScsIHNlbGVjdGlvbiksIHNlbGVjdGlvbi5pc0NvbGxhcHNlZCgpID8gJ2JhY2tzcGFjZS1jaGFyYWN0ZXInIDogJ3JlbW92ZS1yYW5nZScpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGtleUNvbW1hbmRQbGFpbkJhY2tzcGFjZTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTs7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/keyCommandPlainBackspace.js
