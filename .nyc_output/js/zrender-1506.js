var guid = __webpack_require__(/*! ./core/guid */ "./node_modules/zrender/lib/core/guid.js");

var env = __webpack_require__(/*! ./core/env */ "./node_modules/zrender/lib/core/env.js");

var zrUtil = __webpack_require__(/*! ./core/util */ "./node_modules/zrender/lib/core/util.js");

var Handler = __webpack_require__(/*! ./Handler */ "./node_modules/zrender/lib/Handler.js");

var Storage = __webpack_require__(/*! ./Storage */ "./node_modules/zrender/lib/Storage.js");

var Painter = __webpack_require__(/*! ./Painter */ "./node_modules/zrender/lib/Painter.js");

var Animation = __webpack_require__(/*! ./animation/Animation */ "./node_modules/zrender/lib/animation/Animation.js");

var HandlerProxy = __webpack_require__(/*! ./dom/HandlerProxy */ "./node_modules/zrender/lib/dom/HandlerProxy.js");
/*!
* ZRender, a high performance 2d drawing library.
*
* Copyright (c) 2013, Baidu Inc.
* All rights reserved.
*
* LICENSE
* https://github.com/ecomfe/zrender/blob/master/LICENSE.txt
*/


var useVML = !env.canvasSupported;
var painterCtors = {
  canvas: Painter
};
var instances = {}; // ZRender实例map索引

/**
 * @type {string}
 */

var version = '4.0.7';
/**
 * Initializing a zrender instance
 * @param {HTMLElement} dom
 * @param {Object} [opts]
 * @param {string} [opts.renderer='canvas'] 'canvas' or 'svg'
 * @param {number} [opts.devicePixelRatio]
 * @param {number|string} [opts.width] Can be 'auto' (the same as null/undefined)
 * @param {number|string} [opts.height] Can be 'auto' (the same as null/undefined)
 * @return {module:zrender/ZRender}
 */

function init(dom, opts) {
  var zr = new ZRender(guid(), dom, opts);
  instances[zr.id] = zr;
  return zr;
}
/**
 * Dispose zrender instance
 * @param {module:zrender/ZRender} zr
 */


function dispose(zr) {
  if (zr) {
    zr.dispose();
  } else {
    for (var key in instances) {
      if (instances.hasOwnProperty(key)) {
        instances[key].dispose();
      }
    }

    instances = {};
  }

  return this;
}
/**
 * Get zrender instance by id
 * @param {string} id zrender instance id
 * @return {module:zrender/ZRender}
 */


function getInstance(id) {
  return instances[id];
}

function registerPainter(name, Ctor) {
  painterCtors[name] = Ctor;
}

function delInstance(id) {
  delete instances[id];
}
/**
 * @module zrender/ZRender
 */

/**
 * @constructor
 * @alias module:zrender/ZRender
 * @param {string} id
 * @param {HTMLElement} dom
 * @param {Object} opts
 * @param {string} [opts.renderer='canvas'] 'canvas' or 'svg'
 * @param {number} [opts.devicePixelRatio]
 * @param {number} [opts.width] Can be 'auto' (the same as null/undefined)
 * @param {number} [opts.height] Can be 'auto' (the same as null/undefined)
 */


var ZRender = function ZRender(id, dom, opts) {
  opts = opts || {};
  /**
   * @type {HTMLDomElement}
   */

  this.dom = dom;
  /**
   * @type {string}
   */

  this.id = id;
  var self = this;
  var storage = new Storage();
  var rendererType = opts.renderer; // TODO WebGL

  if (useVML) {
    if (!painterCtors.vml) {
      throw new Error('You need to require \'zrender/vml/vml\' to support IE8');
    }

    rendererType = 'vml';
  } else if (!rendererType || !painterCtors[rendererType]) {
    rendererType = 'canvas';
  }

  var painter = new painterCtors[rendererType](dom, storage, opts, id);
  this.storage = storage;
  this.painter = painter;
  var handerProxy = !env.node && !env.worker ? new HandlerProxy(painter.getViewportRoot()) : null;
  this.handler = new Handler(storage, painter, handerProxy, painter.root);
  /**
   * @type {module:zrender/animation/Animation}
   */

  this.animation = new Animation({
    stage: {
      update: zrUtil.bind(this.flush, this)
    }
  });
  this.animation.start();
  /**
   * @type {boolean}
   * @private
   */

  this._needsRefresh; // 修改 storage.delFromStorage, 每次删除元素之前删除动画
  // FIXME 有点ugly

  var oldDelFromStorage = storage.delFromStorage;
  var oldAddToStorage = storage.addToStorage;

  storage.delFromStorage = function (el) {
    oldDelFromStorage.call(storage, el);
    el && el.removeSelfFromZr(self);
  };

  storage.addToStorage = function (el) {
    oldAddToStorage.call(storage, el);
    el.addSelfToZr(self);
  };
};

ZRender.prototype = {
  constructor: ZRender,

  /**
   * 获取实例唯一标识
   * @return {string}
   */
  getId: function getId() {
    return this.id;
  },

  /**
   * 添加元素
   * @param  {module:zrender/Element} el
   */
  add: function add(el) {
    this.storage.addRoot(el);
    this._needsRefresh = true;
  },

  /**
   * 删除元素
   * @param  {module:zrender/Element} el
   */
  remove: function remove(el) {
    this.storage.delRoot(el);
    this._needsRefresh = true;
  },

  /**
   * Change configuration of layer
   * @param {string} zLevel
   * @param {Object} config
   * @param {string} [config.clearColor=0] Clear color
   * @param {string} [config.motionBlur=false] If enable motion blur
   * @param {number} [config.lastFrameAlpha=0.7] Motion blur factor. Larger value cause longer trailer
  */
  configLayer: function configLayer(zLevel, config) {
    if (this.painter.configLayer) {
      this.painter.configLayer(zLevel, config);
    }

    this._needsRefresh = true;
  },

  /**
   * Set background color
   * @param {string} backgroundColor
   */
  setBackgroundColor: function setBackgroundColor(backgroundColor) {
    if (this.painter.setBackgroundColor) {
      this.painter.setBackgroundColor(backgroundColor);
    }

    this._needsRefresh = true;
  },

  /**
   * Repaint the canvas immediately
   */
  refreshImmediately: function refreshImmediately() {
    // var start = new Date();
    // Clear needsRefresh ahead to avoid something wrong happens in refresh
    // Or it will cause zrender refreshes again and again.
    this._needsRefresh = false;
    this.painter.refresh();
    /**
     * Avoid trigger zr.refresh in Element#beforeUpdate hook
     */

    this._needsRefresh = false; // var end = new Date();
    // var log = document.getElementById('log');
    // if (log) {
    //     log.innerHTML = log.innerHTML + '<br>' + (end - start);
    // }
  },

  /**
   * Mark and repaint the canvas in the next frame of browser
   */
  refresh: function refresh() {
    this._needsRefresh = true;
  },

  /**
   * Perform all refresh
   */
  flush: function flush() {
    var triggerRendered;

    if (this._needsRefresh) {
      triggerRendered = true;
      this.refreshImmediately();
    }

    if (this._needsRefreshHover) {
      triggerRendered = true;
      this.refreshHoverImmediately();
    }

    triggerRendered && this.trigger('rendered');
  },

  /**
   * Add element to hover layer
   * @param  {module:zrender/Element} el
   * @param {Object} style
   */
  addHover: function addHover(el, style) {
    if (this.painter.addHover) {
      var elMirror = this.painter.addHover(el, style);
      this.refreshHover();
      return elMirror;
    }
  },

  /**
   * Add element from hover layer
   * @param  {module:zrender/Element} el
   */
  removeHover: function removeHover(el) {
    if (this.painter.removeHover) {
      this.painter.removeHover(el);
      this.refreshHover();
    }
  },

  /**
   * Clear all hover elements in hover layer
   * @param  {module:zrender/Element} el
   */
  clearHover: function clearHover() {
    if (this.painter.clearHover) {
      this.painter.clearHover();
      this.refreshHover();
    }
  },

  /**
   * Refresh hover in next frame
   */
  refreshHover: function refreshHover() {
    this._needsRefreshHover = true;
  },

  /**
   * Refresh hover immediately
   */
  refreshHoverImmediately: function refreshHoverImmediately() {
    this._needsRefreshHover = false;
    this.painter.refreshHover && this.painter.refreshHover();
  },

  /**
   * Resize the canvas.
   * Should be invoked when container size is changed
   * @param {Object} [opts]
   * @param {number|string} [opts.width] Can be 'auto' (the same as null/undefined)
   * @param {number|string} [opts.height] Can be 'auto' (the same as null/undefined)
   */
  resize: function resize(opts) {
    opts = opts || {};
    this.painter.resize(opts.width, opts.height);
    this.handler.resize();
  },

  /**
   * Stop and clear all animation immediately
   */
  clearAnimation: function clearAnimation() {
    this.animation.clear();
  },

  /**
   * Get container width
   */
  getWidth: function getWidth() {
    return this.painter.getWidth();
  },

  /**
   * Get container height
   */
  getHeight: function getHeight() {
    return this.painter.getHeight();
  },

  /**
   * Export the canvas as Base64 URL
   * @param {string} type
   * @param {string} [backgroundColor='#fff']
   * @return {string} Base64 URL
   */
  // toDataURL: function(type, backgroundColor) {
  //     return this.painter.getRenderedCanvas({
  //         backgroundColor: backgroundColor
  //     }).toDataURL(type);
  // },

  /**
   * Converting a path to image.
   * It has much better performance of drawing image rather than drawing a vector path.
   * @param {module:zrender/graphic/Path} e
   * @param {number} width
   * @param {number} height
   */
  pathToImage: function pathToImage(e, dpr) {
    return this.painter.pathToImage(e, dpr);
  },

  /**
   * Set default cursor
   * @param {string} [cursorStyle='default'] 例如 crosshair
   */
  setCursorStyle: function setCursorStyle(cursorStyle) {
    this.handler.setCursorStyle(cursorStyle);
  },

  /**
   * Find hovered element
   * @param {number} x
   * @param {number} y
   * @return {Object} {target, topTarget}
   */
  findHover: function findHover(x, y) {
    return this.handler.findHover(x, y);
  },

  /**
   * Bind event
   *
   * @param {string} eventName Event name
   * @param {Function} eventHandler Handler function
   * @param {Object} [context] Context object
   */
  on: function on(eventName, eventHandler, context) {
    this.handler.on(eventName, eventHandler, context);
  },

  /**
   * Unbind event
   * @param {string} eventName Event name
   * @param {Function} [eventHandler] Handler function
   */
  off: function off(eventName, eventHandler) {
    this.handler.off(eventName, eventHandler);
  },

  /**
   * Trigger event manually
   *
   * @param {string} eventName Event name
   * @param {event=} event Event object
   */
  trigger: function trigger(eventName, event) {
    this.handler.trigger(eventName, event);
  },

  /**
   * Clear all objects and the canvas.
   */
  clear: function clear() {
    this.storage.delRoot();
    this.painter.clear();
  },

  /**
   * Dispose self.
   */
  dispose: function dispose() {
    this.animation.stop();
    this.clear();
    this.storage.dispose();
    this.painter.dispose();
    this.handler.dispose();
    this.animation = this.storage = this.painter = this.handler = null;
    delInstance(this.id);
  }
};
exports.version = version;
exports.init = init;
exports.dispose = dispose;
exports.getInstance = getInstance;
exports.registerPainter = registerPainter;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvenJlbmRlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL3pyZW5kZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIGd1aWQgPSByZXF1aXJlKFwiLi9jb3JlL2d1aWRcIik7XG5cbnZhciBlbnYgPSByZXF1aXJlKFwiLi9jb3JlL2VudlwiKTtcblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCIuL2NvcmUvdXRpbFwiKTtcblxudmFyIEhhbmRsZXIgPSByZXF1aXJlKFwiLi9IYW5kbGVyXCIpO1xuXG52YXIgU3RvcmFnZSA9IHJlcXVpcmUoXCIuL1N0b3JhZ2VcIik7XG5cbnZhciBQYWludGVyID0gcmVxdWlyZShcIi4vUGFpbnRlclwiKTtcblxudmFyIEFuaW1hdGlvbiA9IHJlcXVpcmUoXCIuL2FuaW1hdGlvbi9BbmltYXRpb25cIik7XG5cbnZhciBIYW5kbGVyUHJveHkgPSByZXF1aXJlKFwiLi9kb20vSGFuZGxlclByb3h5XCIpO1xuXG4vKiFcbiogWlJlbmRlciwgYSBoaWdoIHBlcmZvcm1hbmNlIDJkIGRyYXdpbmcgbGlicmFyeS5cbipcbiogQ29weXJpZ2h0IChjKSAyMDEzLCBCYWlkdSBJbmMuXG4qIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4qXG4qIExJQ0VOU0VcbiogaHR0cHM6Ly9naXRodWIuY29tL2Vjb21mZS96cmVuZGVyL2Jsb2IvbWFzdGVyL0xJQ0VOU0UudHh0XG4qL1xudmFyIHVzZVZNTCA9ICFlbnYuY2FudmFzU3VwcG9ydGVkO1xudmFyIHBhaW50ZXJDdG9ycyA9IHtcbiAgY2FudmFzOiBQYWludGVyXG59O1xudmFyIGluc3RhbmNlcyA9IHt9OyAvLyBaUmVuZGVy5a6e5L6LbWFw57Si5byVXG5cbi8qKlxuICogQHR5cGUge3N0cmluZ31cbiAqL1xuXG52YXIgdmVyc2lvbiA9ICc0LjAuNyc7XG4vKipcbiAqIEluaXRpYWxpemluZyBhIHpyZW5kZXIgaW5zdGFuY2VcbiAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGRvbVxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRzXVxuICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLnJlbmRlcmVyPSdjYW52YXMnXSAnY2FudmFzJyBvciAnc3ZnJ1xuICogQHBhcmFtIHtudW1iZXJ9IFtvcHRzLmRldmljZVBpeGVsUmF0aW9dXG4gKiBAcGFyYW0ge251bWJlcnxzdHJpbmd9IFtvcHRzLndpZHRoXSBDYW4gYmUgJ2F1dG8nICh0aGUgc2FtZSBhcyBudWxsL3VuZGVmaW5lZClcbiAqIEBwYXJhbSB7bnVtYmVyfHN0cmluZ30gW29wdHMuaGVpZ2h0XSBDYW4gYmUgJ2F1dG8nICh0aGUgc2FtZSBhcyBudWxsL3VuZGVmaW5lZClcbiAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyL1pSZW5kZXJ9XG4gKi9cblxuZnVuY3Rpb24gaW5pdChkb20sIG9wdHMpIHtcbiAgdmFyIHpyID0gbmV3IFpSZW5kZXIoZ3VpZCgpLCBkb20sIG9wdHMpO1xuICBpbnN0YW5jZXNbenIuaWRdID0genI7XG4gIHJldHVybiB6cjtcbn1cbi8qKlxuICogRGlzcG9zZSB6cmVuZGVyIGluc3RhbmNlXG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL1pSZW5kZXJ9IHpyXG4gKi9cblxuXG5mdW5jdGlvbiBkaXNwb3NlKHpyKSB7XG4gIGlmICh6cikge1xuICAgIHpyLmRpc3Bvc2UoKTtcbiAgfSBlbHNlIHtcbiAgICBmb3IgKHZhciBrZXkgaW4gaW5zdGFuY2VzKSB7XG4gICAgICBpZiAoaW5zdGFuY2VzLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgaW5zdGFuY2VzW2tleV0uZGlzcG9zZSgpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGluc3RhbmNlcyA9IHt9O1xuICB9XG5cbiAgcmV0dXJuIHRoaXM7XG59XG4vKipcbiAqIEdldCB6cmVuZGVyIGluc3RhbmNlIGJ5IGlkXG4gKiBAcGFyYW0ge3N0cmluZ30gaWQgenJlbmRlciBpbnN0YW5jZSBpZFxuICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvWlJlbmRlcn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGdldEluc3RhbmNlKGlkKSB7XG4gIHJldHVybiBpbnN0YW5jZXNbaWRdO1xufVxuXG5mdW5jdGlvbiByZWdpc3RlclBhaW50ZXIobmFtZSwgQ3Rvcikge1xuICBwYWludGVyQ3RvcnNbbmFtZV0gPSBDdG9yO1xufVxuXG5mdW5jdGlvbiBkZWxJbnN0YW5jZShpZCkge1xuICBkZWxldGUgaW5zdGFuY2VzW2lkXTtcbn1cbi8qKlxuICogQG1vZHVsZSB6cmVuZGVyL1pSZW5kZXJcbiAqL1xuXG4vKipcbiAqIEBjb25zdHJ1Y3RvclxuICogQGFsaWFzIG1vZHVsZTp6cmVuZGVyL1pSZW5kZXJcbiAqIEBwYXJhbSB7c3RyaW5nfSBpZFxuICogQHBhcmFtIHtIVE1MRWxlbWVudH0gZG9tXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0c1xuICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLnJlbmRlcmVyPSdjYW52YXMnXSAnY2FudmFzJyBvciAnc3ZnJ1xuICogQHBhcmFtIHtudW1iZXJ9IFtvcHRzLmRldmljZVBpeGVsUmF0aW9dXG4gKiBAcGFyYW0ge251bWJlcn0gW29wdHMud2lkdGhdIENhbiBiZSAnYXV0bycgKHRoZSBzYW1lIGFzIG51bGwvdW5kZWZpbmVkKVxuICogQHBhcmFtIHtudW1iZXJ9IFtvcHRzLmhlaWdodF0gQ2FuIGJlICdhdXRvJyAodGhlIHNhbWUgYXMgbnVsbC91bmRlZmluZWQpXG4gKi9cblxuXG52YXIgWlJlbmRlciA9IGZ1bmN0aW9uIChpZCwgZG9tLCBvcHRzKSB7XG4gIG9wdHMgPSBvcHRzIHx8IHt9O1xuICAvKipcbiAgICogQHR5cGUge0hUTUxEb21FbGVtZW50fVxuICAgKi9cblxuICB0aGlzLmRvbSA9IGRvbTtcbiAgLyoqXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuXG4gIHRoaXMuaWQgPSBpZDtcbiAgdmFyIHNlbGYgPSB0aGlzO1xuICB2YXIgc3RvcmFnZSA9IG5ldyBTdG9yYWdlKCk7XG4gIHZhciByZW5kZXJlclR5cGUgPSBvcHRzLnJlbmRlcmVyOyAvLyBUT0RPIFdlYkdMXG5cbiAgaWYgKHVzZVZNTCkge1xuICAgIGlmICghcGFpbnRlckN0b3JzLnZtbCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdZb3UgbmVlZCB0byByZXF1aXJlIFxcJ3pyZW5kZXIvdm1sL3ZtbFxcJyB0byBzdXBwb3J0IElFOCcpO1xuICAgIH1cblxuICAgIHJlbmRlcmVyVHlwZSA9ICd2bWwnO1xuICB9IGVsc2UgaWYgKCFyZW5kZXJlclR5cGUgfHwgIXBhaW50ZXJDdG9yc1tyZW5kZXJlclR5cGVdKSB7XG4gICAgcmVuZGVyZXJUeXBlID0gJ2NhbnZhcyc7XG4gIH1cblxuICB2YXIgcGFpbnRlciA9IG5ldyBwYWludGVyQ3RvcnNbcmVuZGVyZXJUeXBlXShkb20sIHN0b3JhZ2UsIG9wdHMsIGlkKTtcbiAgdGhpcy5zdG9yYWdlID0gc3RvcmFnZTtcbiAgdGhpcy5wYWludGVyID0gcGFpbnRlcjtcbiAgdmFyIGhhbmRlclByb3h5ID0gIWVudi5ub2RlICYmICFlbnYud29ya2VyID8gbmV3IEhhbmRsZXJQcm94eShwYWludGVyLmdldFZpZXdwb3J0Um9vdCgpKSA6IG51bGw7XG4gIHRoaXMuaGFuZGxlciA9IG5ldyBIYW5kbGVyKHN0b3JhZ2UsIHBhaW50ZXIsIGhhbmRlclByb3h5LCBwYWludGVyLnJvb3QpO1xuICAvKipcbiAgICogQHR5cGUge21vZHVsZTp6cmVuZGVyL2FuaW1hdGlvbi9BbmltYXRpb259XG4gICAqL1xuXG4gIHRoaXMuYW5pbWF0aW9uID0gbmV3IEFuaW1hdGlvbih7XG4gICAgc3RhZ2U6IHtcbiAgICAgIHVwZGF0ZTogenJVdGlsLmJpbmQodGhpcy5mbHVzaCwgdGhpcylcbiAgICB9XG4gIH0pO1xuICB0aGlzLmFuaW1hdGlvbi5zdGFydCgpO1xuICAvKipcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX25lZWRzUmVmcmVzaDsgLy8g5L+u5pS5IHN0b3JhZ2UuZGVsRnJvbVN0b3JhZ2UsIOavj+asoeWIoOmZpOWFg+e0oOS5i+WJjeWIoOmZpOWKqOeUu1xuICAvLyBGSVhNRSDmnInngrl1Z2x5XG5cbiAgdmFyIG9sZERlbEZyb21TdG9yYWdlID0gc3RvcmFnZS5kZWxGcm9tU3RvcmFnZTtcbiAgdmFyIG9sZEFkZFRvU3RvcmFnZSA9IHN0b3JhZ2UuYWRkVG9TdG9yYWdlO1xuXG4gIHN0b3JhZ2UuZGVsRnJvbVN0b3JhZ2UgPSBmdW5jdGlvbiAoZWwpIHtcbiAgICBvbGREZWxGcm9tU3RvcmFnZS5jYWxsKHN0b3JhZ2UsIGVsKTtcbiAgICBlbCAmJiBlbC5yZW1vdmVTZWxmRnJvbVpyKHNlbGYpO1xuICB9O1xuXG4gIHN0b3JhZ2UuYWRkVG9TdG9yYWdlID0gZnVuY3Rpb24gKGVsKSB7XG4gICAgb2xkQWRkVG9TdG9yYWdlLmNhbGwoc3RvcmFnZSwgZWwpO1xuICAgIGVsLmFkZFNlbGZUb1pyKHNlbGYpO1xuICB9O1xufTtcblxuWlJlbmRlci5wcm90b3R5cGUgPSB7XG4gIGNvbnN0cnVjdG9yOiBaUmVuZGVyLFxuXG4gIC8qKlxuICAgKiDojrflj5blrp7kvovllK/kuIDmoIfor4ZcbiAgICogQHJldHVybiB7c3RyaW5nfVxuICAgKi9cbiAgZ2V0SWQ6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5pZDtcbiAgfSxcblxuICAvKipcbiAgICog5re75Yqg5YWD57SgXG4gICAqIEBwYXJhbSAge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9IGVsXG4gICAqL1xuICBhZGQ6IGZ1bmN0aW9uIChlbCkge1xuICAgIHRoaXMuc3RvcmFnZS5hZGRSb290KGVsKTtcbiAgICB0aGlzLl9uZWVkc1JlZnJlc2ggPSB0cnVlO1xuICB9LFxuXG4gIC8qKlxuICAgKiDliKDpmaTlhYPntKBcbiAgICogQHBhcmFtICB7bW9kdWxlOnpyZW5kZXIvRWxlbWVudH0gZWxcbiAgICovXG4gIHJlbW92ZTogZnVuY3Rpb24gKGVsKSB7XG4gICAgdGhpcy5zdG9yYWdlLmRlbFJvb3QoZWwpO1xuICAgIHRoaXMuX25lZWRzUmVmcmVzaCA9IHRydWU7XG4gIH0sXG5cbiAgLyoqXG4gICAqIENoYW5nZSBjb25maWd1cmF0aW9uIG9mIGxheWVyXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB6TGV2ZWxcbiAgICogQHBhcmFtIHtPYmplY3R9IGNvbmZpZ1xuICAgKiBAcGFyYW0ge3N0cmluZ30gW2NvbmZpZy5jbGVhckNvbG9yPTBdIENsZWFyIGNvbG9yXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBbY29uZmlnLm1vdGlvbkJsdXI9ZmFsc2VdIElmIGVuYWJsZSBtb3Rpb24gYmx1clxuICAgKiBAcGFyYW0ge251bWJlcn0gW2NvbmZpZy5sYXN0RnJhbWVBbHBoYT0wLjddIE1vdGlvbiBibHVyIGZhY3Rvci4gTGFyZ2VyIHZhbHVlIGNhdXNlIGxvbmdlciB0cmFpbGVyXG4gICovXG4gIGNvbmZpZ0xheWVyOiBmdW5jdGlvbiAoekxldmVsLCBjb25maWcpIHtcbiAgICBpZiAodGhpcy5wYWludGVyLmNvbmZpZ0xheWVyKSB7XG4gICAgICB0aGlzLnBhaW50ZXIuY29uZmlnTGF5ZXIoekxldmVsLCBjb25maWcpO1xuICAgIH1cblxuICAgIHRoaXMuX25lZWRzUmVmcmVzaCA9IHRydWU7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFNldCBiYWNrZ3JvdW5kIGNvbG9yXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBiYWNrZ3JvdW5kQ29sb3JcbiAgICovXG4gIHNldEJhY2tncm91bmRDb2xvcjogZnVuY3Rpb24gKGJhY2tncm91bmRDb2xvcikge1xuICAgIGlmICh0aGlzLnBhaW50ZXIuc2V0QmFja2dyb3VuZENvbG9yKSB7XG4gICAgICB0aGlzLnBhaW50ZXIuc2V0QmFja2dyb3VuZENvbG9yKGJhY2tncm91bmRDb2xvcik7XG4gICAgfVxuXG4gICAgdGhpcy5fbmVlZHNSZWZyZXNoID0gdHJ1ZTtcbiAgfSxcblxuICAvKipcbiAgICogUmVwYWludCB0aGUgY2FudmFzIGltbWVkaWF0ZWx5XG4gICAqL1xuICByZWZyZXNoSW1tZWRpYXRlbHk6IGZ1bmN0aW9uICgpIHtcbiAgICAvLyB2YXIgc3RhcnQgPSBuZXcgRGF0ZSgpO1xuICAgIC8vIENsZWFyIG5lZWRzUmVmcmVzaCBhaGVhZCB0byBhdm9pZCBzb21ldGhpbmcgd3JvbmcgaGFwcGVucyBpbiByZWZyZXNoXG4gICAgLy8gT3IgaXQgd2lsbCBjYXVzZSB6cmVuZGVyIHJlZnJlc2hlcyBhZ2FpbiBhbmQgYWdhaW4uXG4gICAgdGhpcy5fbmVlZHNSZWZyZXNoID0gZmFsc2U7XG4gICAgdGhpcy5wYWludGVyLnJlZnJlc2goKTtcbiAgICAvKipcbiAgICAgKiBBdm9pZCB0cmlnZ2VyIHpyLnJlZnJlc2ggaW4gRWxlbWVudCNiZWZvcmVVcGRhdGUgaG9va1xuICAgICAqL1xuXG4gICAgdGhpcy5fbmVlZHNSZWZyZXNoID0gZmFsc2U7IC8vIHZhciBlbmQgPSBuZXcgRGF0ZSgpO1xuICAgIC8vIHZhciBsb2cgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbG9nJyk7XG4gICAgLy8gaWYgKGxvZykge1xuICAgIC8vICAgICBsb2cuaW5uZXJIVE1MID0gbG9nLmlubmVySFRNTCArICc8YnI+JyArIChlbmQgLSBzdGFydCk7XG4gICAgLy8gfVxuICB9LFxuXG4gIC8qKlxuICAgKiBNYXJrIGFuZCByZXBhaW50IHRoZSBjYW52YXMgaW4gdGhlIG5leHQgZnJhbWUgb2YgYnJvd3NlclxuICAgKi9cbiAgcmVmcmVzaDogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX25lZWRzUmVmcmVzaCA9IHRydWU7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFBlcmZvcm0gYWxsIHJlZnJlc2hcbiAgICovXG4gIGZsdXNoOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHRyaWdnZXJSZW5kZXJlZDtcblxuICAgIGlmICh0aGlzLl9uZWVkc1JlZnJlc2gpIHtcbiAgICAgIHRyaWdnZXJSZW5kZXJlZCA9IHRydWU7XG4gICAgICB0aGlzLnJlZnJlc2hJbW1lZGlhdGVseSgpO1xuICAgIH1cblxuICAgIGlmICh0aGlzLl9uZWVkc1JlZnJlc2hIb3Zlcikge1xuICAgICAgdHJpZ2dlclJlbmRlcmVkID0gdHJ1ZTtcbiAgICAgIHRoaXMucmVmcmVzaEhvdmVySW1tZWRpYXRlbHkoKTtcbiAgICB9XG5cbiAgICB0cmlnZ2VyUmVuZGVyZWQgJiYgdGhpcy50cmlnZ2VyKCdyZW5kZXJlZCcpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBBZGQgZWxlbWVudCB0byBob3ZlciBsYXllclxuICAgKiBAcGFyYW0gIHttb2R1bGU6enJlbmRlci9FbGVtZW50fSBlbFxuICAgKiBAcGFyYW0ge09iamVjdH0gc3R5bGVcbiAgICovXG4gIGFkZEhvdmVyOiBmdW5jdGlvbiAoZWwsIHN0eWxlKSB7XG4gICAgaWYgKHRoaXMucGFpbnRlci5hZGRIb3Zlcikge1xuICAgICAgdmFyIGVsTWlycm9yID0gdGhpcy5wYWludGVyLmFkZEhvdmVyKGVsLCBzdHlsZSk7XG4gICAgICB0aGlzLnJlZnJlc2hIb3ZlcigpO1xuICAgICAgcmV0dXJuIGVsTWlycm9yO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQWRkIGVsZW1lbnQgZnJvbSBob3ZlciBsYXllclxuICAgKiBAcGFyYW0gIHttb2R1bGU6enJlbmRlci9FbGVtZW50fSBlbFxuICAgKi9cbiAgcmVtb3ZlSG92ZXI6IGZ1bmN0aW9uIChlbCkge1xuICAgIGlmICh0aGlzLnBhaW50ZXIucmVtb3ZlSG92ZXIpIHtcbiAgICAgIHRoaXMucGFpbnRlci5yZW1vdmVIb3ZlcihlbCk7XG4gICAgICB0aGlzLnJlZnJlc2hIb3ZlcigpO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQ2xlYXIgYWxsIGhvdmVyIGVsZW1lbnRzIGluIGhvdmVyIGxheWVyXG4gICAqIEBwYXJhbSAge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9IGVsXG4gICAqL1xuICBjbGVhckhvdmVyOiBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHRoaXMucGFpbnRlci5jbGVhckhvdmVyKSB7XG4gICAgICB0aGlzLnBhaW50ZXIuY2xlYXJIb3ZlcigpO1xuICAgICAgdGhpcy5yZWZyZXNoSG92ZXIoKTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIFJlZnJlc2ggaG92ZXIgaW4gbmV4dCBmcmFtZVxuICAgKi9cbiAgcmVmcmVzaEhvdmVyOiBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5fbmVlZHNSZWZyZXNoSG92ZXIgPSB0cnVlO1xuICB9LFxuXG4gIC8qKlxuICAgKiBSZWZyZXNoIGhvdmVyIGltbWVkaWF0ZWx5XG4gICAqL1xuICByZWZyZXNoSG92ZXJJbW1lZGlhdGVseTogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX25lZWRzUmVmcmVzaEhvdmVyID0gZmFsc2U7XG4gICAgdGhpcy5wYWludGVyLnJlZnJlc2hIb3ZlciAmJiB0aGlzLnBhaW50ZXIucmVmcmVzaEhvdmVyKCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFJlc2l6ZSB0aGUgY2FudmFzLlxuICAgKiBTaG91bGQgYmUgaW52b2tlZCB3aGVuIGNvbnRhaW5lciBzaXplIGlzIGNoYW5nZWRcbiAgICogQHBhcmFtIHtPYmplY3R9IFtvcHRzXVxuICAgKiBAcGFyYW0ge251bWJlcnxzdHJpbmd9IFtvcHRzLndpZHRoXSBDYW4gYmUgJ2F1dG8nICh0aGUgc2FtZSBhcyBudWxsL3VuZGVmaW5lZClcbiAgICogQHBhcmFtIHtudW1iZXJ8c3RyaW5nfSBbb3B0cy5oZWlnaHRdIENhbiBiZSAnYXV0bycgKHRoZSBzYW1lIGFzIG51bGwvdW5kZWZpbmVkKVxuICAgKi9cbiAgcmVzaXplOiBmdW5jdGlvbiAob3B0cykge1xuICAgIG9wdHMgPSBvcHRzIHx8IHt9O1xuICAgIHRoaXMucGFpbnRlci5yZXNpemUob3B0cy53aWR0aCwgb3B0cy5oZWlnaHQpO1xuICAgIHRoaXMuaGFuZGxlci5yZXNpemUoKTtcbiAgfSxcblxuICAvKipcbiAgICogU3RvcCBhbmQgY2xlYXIgYWxsIGFuaW1hdGlvbiBpbW1lZGlhdGVseVxuICAgKi9cbiAgY2xlYXJBbmltYXRpb246IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmFuaW1hdGlvbi5jbGVhcigpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBHZXQgY29udGFpbmVyIHdpZHRoXG4gICAqL1xuICBnZXRXaWR0aDogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLnBhaW50ZXIuZ2V0V2lkdGgoKTtcbiAgfSxcblxuICAvKipcbiAgICogR2V0IGNvbnRhaW5lciBoZWlnaHRcbiAgICovXG4gIGdldEhlaWdodDogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLnBhaW50ZXIuZ2V0SGVpZ2h0KCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEV4cG9ydCB0aGUgY2FudmFzIGFzIEJhc2U2NCBVUkxcbiAgICogQHBhcmFtIHtzdHJpbmd9IHR5cGVcbiAgICogQHBhcmFtIHtzdHJpbmd9IFtiYWNrZ3JvdW5kQ29sb3I9JyNmZmYnXVxuICAgKiBAcmV0dXJuIHtzdHJpbmd9IEJhc2U2NCBVUkxcbiAgICovXG4gIC8vIHRvRGF0YVVSTDogZnVuY3Rpb24odHlwZSwgYmFja2dyb3VuZENvbG9yKSB7XG4gIC8vICAgICByZXR1cm4gdGhpcy5wYWludGVyLmdldFJlbmRlcmVkQ2FudmFzKHtcbiAgLy8gICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IGJhY2tncm91bmRDb2xvclxuICAvLyAgICAgfSkudG9EYXRhVVJMKHR5cGUpO1xuICAvLyB9LFxuXG4gIC8qKlxuICAgKiBDb252ZXJ0aW5nIGEgcGF0aCB0byBpbWFnZS5cbiAgICogSXQgaGFzIG11Y2ggYmV0dGVyIHBlcmZvcm1hbmNlIG9mIGRyYXdpbmcgaW1hZ2UgcmF0aGVyIHRoYW4gZHJhd2luZyBhIHZlY3RvciBwYXRoLlxuICAgKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL2dyYXBoaWMvUGF0aH0gZVxuICAgKiBAcGFyYW0ge251bWJlcn0gd2lkdGhcbiAgICogQHBhcmFtIHtudW1iZXJ9IGhlaWdodFxuICAgKi9cbiAgcGF0aFRvSW1hZ2U6IGZ1bmN0aW9uIChlLCBkcHIpIHtcbiAgICByZXR1cm4gdGhpcy5wYWludGVyLnBhdGhUb0ltYWdlKGUsIGRwcik7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFNldCBkZWZhdWx0IGN1cnNvclxuICAgKiBAcGFyYW0ge3N0cmluZ30gW2N1cnNvclN0eWxlPSdkZWZhdWx0J10g5L6L5aaCIGNyb3NzaGFpclxuICAgKi9cbiAgc2V0Q3Vyc29yU3R5bGU6IGZ1bmN0aW9uIChjdXJzb3JTdHlsZSkge1xuICAgIHRoaXMuaGFuZGxlci5zZXRDdXJzb3JTdHlsZShjdXJzb3JTdHlsZSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEZpbmQgaG92ZXJlZCBlbGVtZW50XG4gICAqIEBwYXJhbSB7bnVtYmVyfSB4XG4gICAqIEBwYXJhbSB7bnVtYmVyfSB5XG4gICAqIEByZXR1cm4ge09iamVjdH0ge3RhcmdldCwgdG9wVGFyZ2V0fVxuICAgKi9cbiAgZmluZEhvdmVyOiBmdW5jdGlvbiAoeCwgeSkge1xuICAgIHJldHVybiB0aGlzLmhhbmRsZXIuZmluZEhvdmVyKHgsIHkpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBCaW5kIGV2ZW50XG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBldmVudE5hbWUgRXZlbnQgbmFtZVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBldmVudEhhbmRsZXIgSGFuZGxlciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge09iamVjdH0gW2NvbnRleHRdIENvbnRleHQgb2JqZWN0XG4gICAqL1xuICBvbjogZnVuY3Rpb24gKGV2ZW50TmFtZSwgZXZlbnRIYW5kbGVyLCBjb250ZXh0KSB7XG4gICAgdGhpcy5oYW5kbGVyLm9uKGV2ZW50TmFtZSwgZXZlbnRIYW5kbGVyLCBjb250ZXh0KTtcbiAgfSxcblxuICAvKipcbiAgICogVW5iaW5kIGV2ZW50XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBldmVudE5hbWUgRXZlbnQgbmFtZVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBbZXZlbnRIYW5kbGVyXSBIYW5kbGVyIGZ1bmN0aW9uXG4gICAqL1xuICBvZmY6IGZ1bmN0aW9uIChldmVudE5hbWUsIGV2ZW50SGFuZGxlcikge1xuICAgIHRoaXMuaGFuZGxlci5vZmYoZXZlbnROYW1lLCBldmVudEhhbmRsZXIpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBUcmlnZ2VyIGV2ZW50IG1hbnVhbGx5XG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBldmVudE5hbWUgRXZlbnQgbmFtZVxuICAgKiBAcGFyYW0ge2V2ZW50PX0gZXZlbnQgRXZlbnQgb2JqZWN0XG4gICAqL1xuICB0cmlnZ2VyOiBmdW5jdGlvbiAoZXZlbnROYW1lLCBldmVudCkge1xuICAgIHRoaXMuaGFuZGxlci50cmlnZ2VyKGV2ZW50TmFtZSwgZXZlbnQpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBDbGVhciBhbGwgb2JqZWN0cyBhbmQgdGhlIGNhbnZhcy5cbiAgICovXG4gIGNsZWFyOiBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5zdG9yYWdlLmRlbFJvb3QoKTtcbiAgICB0aGlzLnBhaW50ZXIuY2xlYXIoKTtcbiAgfSxcblxuICAvKipcbiAgICogRGlzcG9zZSBzZWxmLlxuICAgKi9cbiAgZGlzcG9zZTogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuYW5pbWF0aW9uLnN0b3AoKTtcbiAgICB0aGlzLmNsZWFyKCk7XG4gICAgdGhpcy5zdG9yYWdlLmRpc3Bvc2UoKTtcbiAgICB0aGlzLnBhaW50ZXIuZGlzcG9zZSgpO1xuICAgIHRoaXMuaGFuZGxlci5kaXNwb3NlKCk7XG4gICAgdGhpcy5hbmltYXRpb24gPSB0aGlzLnN0b3JhZ2UgPSB0aGlzLnBhaW50ZXIgPSB0aGlzLmhhbmRsZXIgPSBudWxsO1xuICAgIGRlbEluc3RhbmNlKHRoaXMuaWQpO1xuICB9XG59O1xuZXhwb3J0cy52ZXJzaW9uID0gdmVyc2lvbjtcbmV4cG9ydHMuaW5pdCA9IGluaXQ7XG5leHBvcnRzLmRpc3Bvc2UgPSBkaXNwb3NlO1xuZXhwb3J0cy5nZXRJbnN0YW5jZSA9IGdldEluc3RhbmNlO1xuZXhwb3J0cy5yZWdpc3RlclBhaW50ZXIgPSByZWdpc3RlclBhaW50ZXI7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7Ozs7Ozs7Ozs7O0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBOzs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJSQTtBQXVSQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/zrender.js
