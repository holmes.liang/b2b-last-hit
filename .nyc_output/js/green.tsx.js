__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _standard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./standard */ "./src/styles/themes/standard.tsx");

var green = Object.assign({}, _standard__WEBPACK_IMPORTED_MODULE_0__["default"], {
  COLOR_PRIMARY: "#52c41a",
  COLOR_PRIMARY_LIGHT_O75: "rgba(226, 98, 39, 0.75)",
  COLOR_PRIMARY_LIGHT_010: "rgba(226, 98, 39, 0.1)",
  COLOR_PRIMARY_LIGHT: "#e88354",
  INPUT_FOCUS_BORDER_COLOR: "rgba(226, 98, 39, 0.8)",
  TOGGLE_TRUE_BORDER_COLOR: "#e26227",
  TOGGLE_TRUE_BACKGROUND_COLOR: "#e26227"
});
/* harmony default export */ __webpack_exports__["default"] = (green);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc3R5bGVzL3RoZW1lcy9ncmVlbi50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9zdHlsZXMvdGhlbWVzL2dyZWVuLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgcGFyZW50IGZyb20gXCIuL3N0YW5kYXJkXCI7XG5pbXBvcnQge1N0eWxlZFByb3BzVGhlbWV9IGZyb20gXCJAbXktdHlwZXNcIjtcblxuY29uc3QgZ3JlZW4gPSBPYmplY3QuYXNzaWduKHt9LCBwYXJlbnQsIHtcbiAgICBDT0xPUl9QUklNQVJZOiBcIiM1MmM0MWFcIixcbiAgICBDT0xPUl9QUklNQVJZX0xJR0hUX083NTogXCJyZ2JhKDIyNiwgOTgsIDM5LCAwLjc1KVwiLFxuICAgIENPTE9SX1BSSU1BUllfTElHSFRfMDEwOiBcInJnYmEoMjI2LCA5OCwgMzksIDAuMSlcIixcbiAgICBDT0xPUl9QUklNQVJZX0xJR0hUOiBcIiNlODgzNTRcIixcbiAgICBJTlBVVF9GT0NVU19CT1JERVJfQ09MT1I6IFwicmdiYSgyMjYsIDk4LCAzOSwgMC44KVwiLFxuICAgIFRPR0dMRV9UUlVFX0JPUkRFUl9DT0xPUjogXCIjZTI2MjI3XCIsXG4gICAgVE9HR0xFX1RSVUVfQkFDS0dST1VORF9DT0xPUjogXCIjZTI2MjI3XCJcbn0pIGFzIFN0eWxlZFByb3BzVGhlbWU7XG5cbmV4cG9ydCBkZWZhdWx0IGdyZWVuO1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFVQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/styles/themes/green.tsx
