__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_notification__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-notification */ "./node_modules/rc-notification/es/index.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}




var notificationInstance = {};
var defaultDuration = 4.5;
var defaultTop = 24;
var defaultBottom = 24;
var defaultPlacement = 'topRight';
var defaultGetContainer;
var defaultCloseIcon;

function setNotificationConfig(options) {
  var duration = options.duration,
      placement = options.placement,
      bottom = options.bottom,
      top = options.top,
      getContainer = options.getContainer,
      closeIcon = options.closeIcon;

  if (duration !== undefined) {
    defaultDuration = duration;
  }

  if (placement !== undefined) {
    defaultPlacement = placement;
  }

  if (bottom !== undefined) {
    defaultBottom = bottom;
  }

  if (top !== undefined) {
    defaultTop = top;
  }

  if (getContainer !== undefined) {
    defaultGetContainer = getContainer;
  }

  if (closeIcon !== undefined) {
    defaultCloseIcon = closeIcon;
  }
}

function getPlacementStyle(placement) {
  var top = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultTop;
  var bottom = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : defaultBottom;
  var style;

  switch (placement) {
    case 'topLeft':
      style = {
        left: 0,
        top: top,
        bottom: 'auto'
      };
      break;

    case 'topRight':
      style = {
        right: 0,
        top: top,
        bottom: 'auto'
      };
      break;

    case 'bottomLeft':
      style = {
        left: 0,
        top: 'auto',
        bottom: bottom
      };
      break;

    default:
      style = {
        right: 0,
        top: 'auto',
        bottom: bottom
      };
      break;
  }

  return style;
}

function getNotificationInstance(_ref, callback) {
  var prefixCls = _ref.prefixCls,
      _ref$placement = _ref.placement,
      placement = _ref$placement === void 0 ? defaultPlacement : _ref$placement,
      _ref$getContainer = _ref.getContainer,
      getContainer = _ref$getContainer === void 0 ? defaultGetContainer : _ref$getContainer,
      top = _ref.top,
      bottom = _ref.bottom,
      _ref$closeIcon = _ref.closeIcon,
      closeIcon = _ref$closeIcon === void 0 ? defaultCloseIcon : _ref$closeIcon;
  var cacheKey = "".concat(prefixCls, "-").concat(placement);

  if (notificationInstance[cacheKey]) {
    callback(notificationInstance[cacheKey]);
    return;
  }

  var closeIconToRender = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
    className: "".concat(prefixCls, "-close-x")
  }, closeIcon || react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_2__["default"], {
    className: "".concat(prefixCls, "-close-icon"),
    type: "close"
  }));
  rc_notification__WEBPACK_IMPORTED_MODULE_1__["default"].newInstance({
    prefixCls: prefixCls,
    className: "".concat(prefixCls, "-").concat(placement),
    style: getPlacementStyle(placement, top, bottom),
    getContainer: getContainer,
    closeIcon: closeIconToRender
  }, function (notification) {
    notificationInstance[cacheKey] = notification;
    callback(notification);
  });
}

var typeToIcon = {
  success: 'check-circle-o',
  info: 'info-circle-o',
  error: 'close-circle-o',
  warning: 'exclamation-circle-o'
};

function notice(args) {
  var outerPrefixCls = args.prefixCls || 'ant-notification';
  var prefixCls = "".concat(outerPrefixCls, "-notice");
  var duration = args.duration === undefined ? defaultDuration : args.duration;
  var iconNode = null;

  if (args.icon) {
    iconNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
      className: "".concat(prefixCls, "-icon")
    }, args.icon);
  } else if (args.type) {
    var iconType = typeToIcon[args.type];
    iconNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_2__["default"], {
      className: "".concat(prefixCls, "-icon ").concat(prefixCls, "-icon-").concat(args.type),
      type: iconType
    });
  }

  var autoMarginTag = !args.description && iconNode ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
    className: "".concat(prefixCls, "-message-single-line-auto-margin")
  }) : null;
  var placement = args.placement,
      top = args.top,
      bottom = args.bottom,
      getContainer = args.getContainer,
      closeIcon = args.closeIcon;
  getNotificationInstance({
    prefixCls: outerPrefixCls,
    placement: placement,
    top: top,
    bottom: bottom,
    getContainer: getContainer,
    closeIcon: closeIcon
  }, function (notification) {
    notification.notice({
      content: react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: iconNode ? "".concat(prefixCls, "-with-icon") : ''
      }, iconNode, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-message")
      }, autoMarginTag, args.message), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-description")
      }, args.description), args.btn ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-btn")
      }, args.btn) : null),
      duration: duration,
      closable: true,
      onClose: args.onClose,
      onClick: args.onClick,
      key: args.key,
      style: args.style || {},
      className: args.className
    });
  });
}

var api = {
  open: notice,
  close: function close(key) {
    Object.keys(notificationInstance).forEach(function (cacheKey) {
      return notificationInstance[cacheKey].removeNotice(key);
    });
  },
  config: setNotificationConfig,
  destroy: function destroy() {
    Object.keys(notificationInstance).forEach(function (cacheKey) {
      notificationInstance[cacheKey].destroy();
      delete notificationInstance[cacheKey];
    });
  }
};
['success', 'info', 'warning', 'error'].forEach(function (type) {
  api[type] = function (args) {
    return api.open(_extends(_extends({}, args), {
      type: type
    }));
  };
});
api.warn = api.warning;
/* harmony default export */ __webpack_exports__["default"] = (api);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9ub3RpZmljYXRpb24vaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vdGlmaWNhdGlvbi9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IE5vdGlmaWNhdGlvbiBmcm9tICdyYy1ub3RpZmljYXRpb24nO1xuaW1wb3J0IEljb24gZnJvbSAnLi4vaWNvbic7XG5jb25zdCBub3RpZmljYXRpb25JbnN0YW5jZSA9IHt9O1xubGV0IGRlZmF1bHREdXJhdGlvbiA9IDQuNTtcbmxldCBkZWZhdWx0VG9wID0gMjQ7XG5sZXQgZGVmYXVsdEJvdHRvbSA9IDI0O1xubGV0IGRlZmF1bHRQbGFjZW1lbnQgPSAndG9wUmlnaHQnO1xubGV0IGRlZmF1bHRHZXRDb250YWluZXI7XG5sZXQgZGVmYXVsdENsb3NlSWNvbjtcbmZ1bmN0aW9uIHNldE5vdGlmaWNhdGlvbkNvbmZpZyhvcHRpb25zKSB7XG4gICAgY29uc3QgeyBkdXJhdGlvbiwgcGxhY2VtZW50LCBib3R0b20sIHRvcCwgZ2V0Q29udGFpbmVyLCBjbG9zZUljb24gfSA9IG9wdGlvbnM7XG4gICAgaWYgKGR1cmF0aW9uICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgZGVmYXVsdER1cmF0aW9uID0gZHVyYXRpb247XG4gICAgfVxuICAgIGlmIChwbGFjZW1lbnQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBkZWZhdWx0UGxhY2VtZW50ID0gcGxhY2VtZW50O1xuICAgIH1cbiAgICBpZiAoYm90dG9tICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgZGVmYXVsdEJvdHRvbSA9IGJvdHRvbTtcbiAgICB9XG4gICAgaWYgKHRvcCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGRlZmF1bHRUb3AgPSB0b3A7XG4gICAgfVxuICAgIGlmIChnZXRDb250YWluZXIgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBkZWZhdWx0R2V0Q29udGFpbmVyID0gZ2V0Q29udGFpbmVyO1xuICAgIH1cbiAgICBpZiAoY2xvc2VJY29uICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgZGVmYXVsdENsb3NlSWNvbiA9IGNsb3NlSWNvbjtcbiAgICB9XG59XG5mdW5jdGlvbiBnZXRQbGFjZW1lbnRTdHlsZShwbGFjZW1lbnQsIHRvcCA9IGRlZmF1bHRUb3AsIGJvdHRvbSA9IGRlZmF1bHRCb3R0b20pIHtcbiAgICBsZXQgc3R5bGU7XG4gICAgc3dpdGNoIChwbGFjZW1lbnQpIHtcbiAgICAgICAgY2FzZSAndG9wTGVmdCc6XG4gICAgICAgICAgICBzdHlsZSA9IHtcbiAgICAgICAgICAgICAgICBsZWZ0OiAwLFxuICAgICAgICAgICAgICAgIHRvcCxcbiAgICAgICAgICAgICAgICBib3R0b206ICdhdXRvJyxcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAndG9wUmlnaHQnOlxuICAgICAgICAgICAgc3R5bGUgPSB7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IDAsXG4gICAgICAgICAgICAgICAgdG9wLFxuICAgICAgICAgICAgICAgIGJvdHRvbTogJ2F1dG8nLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdib3R0b21MZWZ0JzpcbiAgICAgICAgICAgIHN0eWxlID0ge1xuICAgICAgICAgICAgICAgIGxlZnQ6IDAsXG4gICAgICAgICAgICAgICAgdG9wOiAnYXV0bycsXG4gICAgICAgICAgICAgICAgYm90dG9tLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgc3R5bGUgPSB7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IDAsXG4gICAgICAgICAgICAgICAgdG9wOiAnYXV0bycsXG4gICAgICAgICAgICAgICAgYm90dG9tLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgIH1cbiAgICByZXR1cm4gc3R5bGU7XG59XG5mdW5jdGlvbiBnZXROb3RpZmljYXRpb25JbnN0YW5jZSh7IHByZWZpeENscywgcGxhY2VtZW50ID0gZGVmYXVsdFBsYWNlbWVudCwgZ2V0Q29udGFpbmVyID0gZGVmYXVsdEdldENvbnRhaW5lciwgdG9wLCBib3R0b20sIGNsb3NlSWNvbiA9IGRlZmF1bHRDbG9zZUljb24sIH0sIGNhbGxiYWNrKSB7XG4gICAgY29uc3QgY2FjaGVLZXkgPSBgJHtwcmVmaXhDbHN9LSR7cGxhY2VtZW50fWA7XG4gICAgaWYgKG5vdGlmaWNhdGlvbkluc3RhbmNlW2NhY2hlS2V5XSkge1xuICAgICAgICBjYWxsYmFjayhub3RpZmljYXRpb25JbnN0YW5jZVtjYWNoZUtleV0pO1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGNvbnN0IGNsb3NlSWNvblRvUmVuZGVyID0gKDxzcGFuIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1jbG9zZS14YH0+XG4gICAgICB7Y2xvc2VJY29uIHx8IDxJY29uIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1jbG9zZS1pY29uYH0gdHlwZT1cImNsb3NlXCIvPn1cbiAgICA8L3NwYW4+KTtcbiAgICBOb3RpZmljYXRpb24ubmV3SW5zdGFuY2Uoe1xuICAgICAgICBwcmVmaXhDbHMsXG4gICAgICAgIGNsYXNzTmFtZTogYCR7cHJlZml4Q2xzfS0ke3BsYWNlbWVudH1gLFxuICAgICAgICBzdHlsZTogZ2V0UGxhY2VtZW50U3R5bGUocGxhY2VtZW50LCB0b3AsIGJvdHRvbSksXG4gICAgICAgIGdldENvbnRhaW5lcixcbiAgICAgICAgY2xvc2VJY29uOiBjbG9zZUljb25Ub1JlbmRlcixcbiAgICB9LCAobm90aWZpY2F0aW9uKSA9PiB7XG4gICAgICAgIG5vdGlmaWNhdGlvbkluc3RhbmNlW2NhY2hlS2V5XSA9IG5vdGlmaWNhdGlvbjtcbiAgICAgICAgY2FsbGJhY2sobm90aWZpY2F0aW9uKTtcbiAgICB9KTtcbn1cbmNvbnN0IHR5cGVUb0ljb24gPSB7XG4gICAgc3VjY2VzczogJ2NoZWNrLWNpcmNsZS1vJyxcbiAgICBpbmZvOiAnaW5mby1jaXJjbGUtbycsXG4gICAgZXJyb3I6ICdjbG9zZS1jaXJjbGUtbycsXG4gICAgd2FybmluZzogJ2V4Y2xhbWF0aW9uLWNpcmNsZS1vJyxcbn07XG5mdW5jdGlvbiBub3RpY2UoYXJncykge1xuICAgIGNvbnN0IG91dGVyUHJlZml4Q2xzID0gYXJncy5wcmVmaXhDbHMgfHwgJ2FudC1ub3RpZmljYXRpb24nO1xuICAgIGNvbnN0IHByZWZpeENscyA9IGAke291dGVyUHJlZml4Q2xzfS1ub3RpY2VgO1xuICAgIGNvbnN0IGR1cmF0aW9uID0gYXJncy5kdXJhdGlvbiA9PT0gdW5kZWZpbmVkID8gZGVmYXVsdER1cmF0aW9uIDogYXJncy5kdXJhdGlvbjtcbiAgICBsZXQgaWNvbk5vZGUgPSBudWxsO1xuICAgIGlmIChhcmdzLmljb24pIHtcbiAgICAgICAgaWNvbk5vZGUgPSA8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taWNvbmB9PnthcmdzLmljb259PC9zcGFuPjtcbiAgICB9XG4gICAgZWxzZSBpZiAoYXJncy50eXBlKSB7XG4gICAgICAgIGNvbnN0IGljb25UeXBlID0gdHlwZVRvSWNvblthcmdzLnR5cGVdO1xuICAgICAgICBpY29uTm9kZSA9ICg8SWNvbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taWNvbiAke3ByZWZpeENsc30taWNvbi0ke2FyZ3MudHlwZX1gfSB0eXBlPXtpY29uVHlwZX0vPik7XG4gICAgfVxuICAgIGNvbnN0IGF1dG9NYXJnaW5UYWcgPSAhYXJncy5kZXNjcmlwdGlvbiAmJiBpY29uTm9kZSA/ICg8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tbWVzc2FnZS1zaW5nbGUtbGluZS1hdXRvLW1hcmdpbmB9Lz4pIDogbnVsbDtcbiAgICBjb25zdCB7IHBsYWNlbWVudCwgdG9wLCBib3R0b20sIGdldENvbnRhaW5lciwgY2xvc2VJY29uIH0gPSBhcmdzO1xuICAgIGdldE5vdGlmaWNhdGlvbkluc3RhbmNlKHtcbiAgICAgICAgcHJlZml4Q2xzOiBvdXRlclByZWZpeENscyxcbiAgICAgICAgcGxhY2VtZW50LFxuICAgICAgICB0b3AsXG4gICAgICAgIGJvdHRvbSxcbiAgICAgICAgZ2V0Q29udGFpbmVyLFxuICAgICAgICBjbG9zZUljb24sXG4gICAgfSwgKG5vdGlmaWNhdGlvbikgPT4ge1xuICAgICAgICBub3RpZmljYXRpb24ubm90aWNlKHtcbiAgICAgICAgICAgIGNvbnRlbnQ6ICg8ZGl2IGNsYXNzTmFtZT17aWNvbk5vZGUgPyBgJHtwcmVmaXhDbHN9LXdpdGgtaWNvbmAgOiAnJ30+XG4gICAgICAgICAgICB7aWNvbk5vZGV9XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1tZXNzYWdlYH0+XG4gICAgICAgICAgICAgIHthdXRvTWFyZ2luVGFnfVxuICAgICAgICAgICAgICB7YXJncy5tZXNzYWdlfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1kZXNjcmlwdGlvbmB9PnthcmdzLmRlc2NyaXB0aW9ufTwvZGl2PlxuICAgICAgICAgICAge2FyZ3MuYnRuID8gPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWJ0bmB9PnthcmdzLmJ0bn08L3NwYW4+IDogbnVsbH1cbiAgICAgICAgICA8L2Rpdj4pLFxuICAgICAgICAgICAgZHVyYXRpb24sXG4gICAgICAgICAgICBjbG9zYWJsZTogdHJ1ZSxcbiAgICAgICAgICAgIG9uQ2xvc2U6IGFyZ3Mub25DbG9zZSxcbiAgICAgICAgICAgIG9uQ2xpY2s6IGFyZ3Mub25DbGljayxcbiAgICAgICAgICAgIGtleTogYXJncy5rZXksXG4gICAgICAgICAgICBzdHlsZTogYXJncy5zdHlsZSB8fCB7fSxcbiAgICAgICAgICAgIGNsYXNzTmFtZTogYXJncy5jbGFzc05hbWUsXG4gICAgICAgIH0pO1xuICAgIH0pO1xufVxuY29uc3QgYXBpID0ge1xuICAgIG9wZW46IG5vdGljZSxcbiAgICBjbG9zZShrZXkpIHtcbiAgICAgICAgT2JqZWN0LmtleXMobm90aWZpY2F0aW9uSW5zdGFuY2UpLmZvckVhY2goY2FjaGVLZXkgPT4gbm90aWZpY2F0aW9uSW5zdGFuY2VbY2FjaGVLZXldLnJlbW92ZU5vdGljZShrZXkpKTtcbiAgICB9LFxuICAgIGNvbmZpZzogc2V0Tm90aWZpY2F0aW9uQ29uZmlnLFxuICAgIGRlc3Ryb3koKSB7XG4gICAgICAgIE9iamVjdC5rZXlzKG5vdGlmaWNhdGlvbkluc3RhbmNlKS5mb3JFYWNoKGNhY2hlS2V5ID0+IHtcbiAgICAgICAgICAgIG5vdGlmaWNhdGlvbkluc3RhbmNlW2NhY2hlS2V5XS5kZXN0cm95KCk7XG4gICAgICAgICAgICBkZWxldGUgbm90aWZpY2F0aW9uSW5zdGFuY2VbY2FjaGVLZXldO1xuICAgICAgICB9KTtcbiAgICB9LFxufTtcblsnc3VjY2VzcycsICdpbmZvJywgJ3dhcm5pbmcnLCAnZXJyb3InXS5mb3JFYWNoKHR5cGUgPT4ge1xuICAgIGFwaVt0eXBlXSA9IChhcmdzKSA9PiBhcGkub3BlbihPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIGFyZ3MpLCB7IHR5cGUgfSkpO1xufSk7XG5hcGkud2FybiA9IGFwaS53YXJuaW5nO1xuZXhwb3J0IGRlZmF1bHQgYXBpO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQTVCQTtBQUNBO0FBNkJBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFSQTtBQVVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBSUE7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhCQTtBQVJBO0FBMkJBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBWEE7QUFhQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUdBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/notification/index.js
