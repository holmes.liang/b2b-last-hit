/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var pathTool = __webpack_require__(/*! zrender/lib/tool/path */ "./node_modules/zrender/lib/tool/path.js");

var colorTool = __webpack_require__(/*! zrender/lib/tool/color */ "./node_modules/zrender/lib/tool/color.js");

var matrix = __webpack_require__(/*! zrender/lib/core/matrix */ "./node_modules/zrender/lib/core/matrix.js");

var vector = __webpack_require__(/*! zrender/lib/core/vector */ "./node_modules/zrender/lib/core/vector.js");

var Path = __webpack_require__(/*! zrender/lib/graphic/Path */ "./node_modules/zrender/lib/graphic/Path.js");

var Transformable = __webpack_require__(/*! zrender/lib/mixin/Transformable */ "./node_modules/zrender/lib/mixin/Transformable.js");

var ZImage = __webpack_require__(/*! zrender/lib/graphic/Image */ "./node_modules/zrender/lib/graphic/Image.js");

exports.Image = ZImage;

var Group = __webpack_require__(/*! zrender/lib/container/Group */ "./node_modules/zrender/lib/container/Group.js");

exports.Group = Group;

var Text = __webpack_require__(/*! zrender/lib/graphic/Text */ "./node_modules/zrender/lib/graphic/Text.js");

exports.Text = Text;

var Circle = __webpack_require__(/*! zrender/lib/graphic/shape/Circle */ "./node_modules/zrender/lib/graphic/shape/Circle.js");

exports.Circle = Circle;

var Sector = __webpack_require__(/*! zrender/lib/graphic/shape/Sector */ "./node_modules/zrender/lib/graphic/shape/Sector.js");

exports.Sector = Sector;

var Ring = __webpack_require__(/*! zrender/lib/graphic/shape/Ring */ "./node_modules/zrender/lib/graphic/shape/Ring.js");

exports.Ring = Ring;

var Polygon = __webpack_require__(/*! zrender/lib/graphic/shape/Polygon */ "./node_modules/zrender/lib/graphic/shape/Polygon.js");

exports.Polygon = Polygon;

var Polyline = __webpack_require__(/*! zrender/lib/graphic/shape/Polyline */ "./node_modules/zrender/lib/graphic/shape/Polyline.js");

exports.Polyline = Polyline;

var Rect = __webpack_require__(/*! zrender/lib/graphic/shape/Rect */ "./node_modules/zrender/lib/graphic/shape/Rect.js");

exports.Rect = Rect;

var Line = __webpack_require__(/*! zrender/lib/graphic/shape/Line */ "./node_modules/zrender/lib/graphic/shape/Line.js");

exports.Line = Line;

var BezierCurve = __webpack_require__(/*! zrender/lib/graphic/shape/BezierCurve */ "./node_modules/zrender/lib/graphic/shape/BezierCurve.js");

exports.BezierCurve = BezierCurve;

var Arc = __webpack_require__(/*! zrender/lib/graphic/shape/Arc */ "./node_modules/zrender/lib/graphic/shape/Arc.js");

exports.Arc = Arc;

var CompoundPath = __webpack_require__(/*! zrender/lib/graphic/CompoundPath */ "./node_modules/zrender/lib/graphic/CompoundPath.js");

exports.CompoundPath = CompoundPath;

var LinearGradient = __webpack_require__(/*! zrender/lib/graphic/LinearGradient */ "./node_modules/zrender/lib/graphic/LinearGradient.js");

exports.LinearGradient = LinearGradient;

var RadialGradient = __webpack_require__(/*! zrender/lib/graphic/RadialGradient */ "./node_modules/zrender/lib/graphic/RadialGradient.js");

exports.RadialGradient = RadialGradient;

var BoundingRect = __webpack_require__(/*! zrender/lib/core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");

exports.BoundingRect = BoundingRect;

var IncrementalDisplayable = __webpack_require__(/*! zrender/lib/graphic/IncrementalDisplayable */ "./node_modules/zrender/lib/graphic/IncrementalDisplayable.js");

exports.IncrementalDisplayable = IncrementalDisplayable;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var round = Math.round;
var mathMax = Math.max;
var mathMin = Math.min;
var EMPTY_OBJ = {};
var Z2_EMPHASIS_LIFT = 1;
/**
 * Extend shape with parameters
 */

function extendShape(opts) {
  return Path.extend(opts);
}
/**
 * Extend path
 */


function extendPath(pathData, opts) {
  return pathTool.extendFromString(pathData, opts);
}
/**
 * Create a path element from path data string
 * @param {string} pathData
 * @param {Object} opts
 * @param {module:zrender/core/BoundingRect} rect
 * @param {string} [layout=cover] 'center' or 'cover'
 */


function makePath(pathData, opts, rect, layout) {
  var path = pathTool.createFromString(pathData, opts);

  if (rect) {
    if (layout === 'center') {
      rect = centerGraphic(rect, path.getBoundingRect());
    }

    resizePath(path, rect);
  }

  return path;
}
/**
 * Create a image element from image url
 * @param {string} imageUrl image url
 * @param {Object} opts options
 * @param {module:zrender/core/BoundingRect} rect constrain rect
 * @param {string} [layout=cover] 'center' or 'cover'
 */


function makeImage(imageUrl, rect, layout) {
  var path = new ZImage({
    style: {
      image: imageUrl,
      x: rect.x,
      y: rect.y,
      width: rect.width,
      height: rect.height
    },
    onload: function onload(img) {
      if (layout === 'center') {
        var boundingRect = {
          width: img.width,
          height: img.height
        };
        path.setStyle(centerGraphic(rect, boundingRect));
      }
    }
  });
  return path;
}
/**
 * Get position of centered element in bounding box.
 *
 * @param  {Object} rect         element local bounding box
 * @param  {Object} boundingRect constraint bounding box
 * @return {Object} element position containing x, y, width, and height
 */


function centerGraphic(rect, boundingRect) {
  // Set rect to center, keep width / height ratio.
  var aspect = boundingRect.width / boundingRect.height;
  var width = rect.height * aspect;
  var height;

  if (width <= rect.width) {
    height = rect.height;
  } else {
    width = rect.width;
    height = width / aspect;
  }

  var cx = rect.x + rect.width / 2;
  var cy = rect.y + rect.height / 2;
  return {
    x: cx - width / 2,
    y: cy - height / 2,
    width: width,
    height: height
  };
}

var mergePath = pathTool.mergePath;
/**
 * Resize a path to fit the rect
 * @param {module:zrender/graphic/Path} path
 * @param {Object} rect
 */

function resizePath(path, rect) {
  if (!path.applyTransform) {
    return;
  }

  var pathRect = path.getBoundingRect();
  var m = pathRect.calculateTransform(rect);
  path.applyTransform(m);
}
/**
 * Sub pixel optimize line for canvas
 *
 * @param {Object} param
 * @param {Object} [param.shape]
 * @param {number} [param.shape.x1]
 * @param {number} [param.shape.y1]
 * @param {number} [param.shape.x2]
 * @param {number} [param.shape.y2]
 * @param {Object} [param.style]
 * @param {number} [param.style.lineWidth]
 * @return {Object} Modified param
 */


function subPixelOptimizeLine(param) {
  var shape = param.shape;
  var lineWidth = param.style.lineWidth;

  if (round(shape.x1 * 2) === round(shape.x2 * 2)) {
    shape.x1 = shape.x2 = subPixelOptimize(shape.x1, lineWidth, true);
  }

  if (round(shape.y1 * 2) === round(shape.y2 * 2)) {
    shape.y1 = shape.y2 = subPixelOptimize(shape.y1, lineWidth, true);
  }

  return param;
}
/**
 * Sub pixel optimize rect for canvas
 *
 * @param {Object} param
 * @param {Object} [param.shape]
 * @param {number} [param.shape.x]
 * @param {number} [param.shape.y]
 * @param {number} [param.shape.width]
 * @param {number} [param.shape.height]
 * @param {Object} [param.style]
 * @param {number} [param.style.lineWidth]
 * @return {Object} Modified param
 */


function subPixelOptimizeRect(param) {
  var shape = param.shape;
  var lineWidth = param.style.lineWidth;
  var originX = shape.x;
  var originY = shape.y;
  var originWidth = shape.width;
  var originHeight = shape.height;
  shape.x = subPixelOptimize(shape.x, lineWidth, true);
  shape.y = subPixelOptimize(shape.y, lineWidth, true);
  shape.width = Math.max(subPixelOptimize(originX + originWidth, lineWidth, false) - shape.x, originWidth === 0 ? 0 : 1);
  shape.height = Math.max(subPixelOptimize(originY + originHeight, lineWidth, false) - shape.y, originHeight === 0 ? 0 : 1);
  return param;
}
/**
 * Sub pixel optimize for canvas
 *
 * @param {number} position Coordinate, such as x, y
 * @param {number} lineWidth Should be nonnegative integer.
 * @param {boolean=} positiveOrNegative Default false (negative).
 * @return {number} Optimized position.
 */


function subPixelOptimize(position, lineWidth, positiveOrNegative) {
  // Assure that (position + lineWidth / 2) is near integer edge,
  // otherwise line will be fuzzy in canvas.
  var doubledPosition = round(position * 2);
  return (doubledPosition + round(lineWidth)) % 2 === 0 ? doubledPosition / 2 : (doubledPosition + (positiveOrNegative ? 1 : -1)) / 2;
}

function hasFillOrStroke(fillOrStroke) {
  return fillOrStroke != null && fillOrStroke !== 'none';
} // Most lifted color are duplicated.


var liftedColorMap = zrUtil.createHashMap();
var liftedColorCount = 0;

function liftColor(color) {
  if (typeof color !== 'string') {
    return color;
  }

  var liftedColor = liftedColorMap.get(color);

  if (!liftedColor) {
    liftedColor = colorTool.lift(color, -0.1);

    if (liftedColorCount < 10000) {
      liftedColorMap.set(color, liftedColor);
      liftedColorCount++;
    }
  }

  return liftedColor;
}

function cacheElementStl(el) {
  if (!el.__hoverStlDirty) {
    return;
  }

  el.__hoverStlDirty = false;
  var hoverStyle = el.__hoverStl;

  if (!hoverStyle) {
    el.__cachedNormalStl = el.__cachedNormalZ2 = null;
    return;
  }

  var normalStyle = el.__cachedNormalStl = {};
  el.__cachedNormalZ2 = el.z2;
  var elStyle = el.style;

  for (var name in hoverStyle) {
    // See comment in `doSingleEnterHover`.
    if (hoverStyle[name] != null) {
      normalStyle[name] = elStyle[name];
    }
  } // Always cache fill and stroke to normalStyle for lifting color.


  normalStyle.fill = elStyle.fill;
  normalStyle.stroke = elStyle.stroke;
}

function doSingleEnterHover(el) {
  var hoverStl = el.__hoverStl;

  if (!hoverStl || el.__highlighted) {
    return;
  }

  var useHoverLayer = el.useHoverLayer;
  el.__highlighted = useHoverLayer ? 'layer' : 'plain';
  var zr = el.__zr;

  if (!zr && useHoverLayer) {
    return;
  }

  var elTarget = el;
  var targetStyle = el.style;

  if (useHoverLayer) {
    elTarget = zr.addHover(el);
    targetStyle = elTarget.style;
  }

  rollbackDefaultTextStyle(targetStyle);

  if (!useHoverLayer) {
    cacheElementStl(elTarget);
  } // styles can be:
  // {
  //    label: {
  //        show: false,
  //        position: 'outside',
  //        fontSize: 18
  //    },
  //    emphasis: {
  //        label: {
  //            show: true
  //        }
  //    }
  // },
  // where properties of `emphasis` may not appear in `normal`. We previously use
  // module:echarts/util/model#defaultEmphasis to merge `normal` to `emphasis`.
  // But consider rich text and setOption in merge mode, it is impossible to cover
  // all properties in merge. So we use merge mode when setting style here, where
  // only properties that is not `null/undefined` can be set. The disadventage:
  // null/undefined can not be used to remove style any more in `emphasis`.


  targetStyle.extendFrom(hoverStl);
  setDefaultHoverFillStroke(targetStyle, hoverStl, 'fill');
  setDefaultHoverFillStroke(targetStyle, hoverStl, 'stroke');
  applyDefaultTextStyle(targetStyle);

  if (!useHoverLayer) {
    el.dirty(false);
    el.z2 += Z2_EMPHASIS_LIFT;
  }
}

function setDefaultHoverFillStroke(targetStyle, hoverStyle, prop) {
  if (!hasFillOrStroke(hoverStyle[prop]) && hasFillOrStroke(targetStyle[prop])) {
    targetStyle[prop] = liftColor(targetStyle[prop]);
  }
}

function doSingleLeaveHover(el) {
  var highlighted = el.__highlighted;

  if (!highlighted) {
    return;
  }

  el.__highlighted = false;

  if (highlighted === 'layer') {
    el.__zr && el.__zr.removeHover(el);
  } else if (highlighted) {
    var style = el.style;
    var normalStl = el.__cachedNormalStl;

    if (normalStl) {
      rollbackDefaultTextStyle(style); // Consider null/undefined value, should use
      // `setStyle` but not `extendFrom(stl, true)`.

      el.setStyle(normalStl);
      applyDefaultTextStyle(style);
    } // `__cachedNormalZ2` will not be reset if calling `setElementHoverStyle`
    // when `el` is on emphasis state. So here by comparing with 1, we try
    // hard to make the bug case rare.


    var normalZ2 = el.__cachedNormalZ2;

    if (normalZ2 != null && el.z2 - normalZ2 === Z2_EMPHASIS_LIFT) {
      el.z2 = normalZ2;
    }
  }
}

function traverseCall(el, method) {
  el.isGroup ? el.traverse(function (child) {
    !child.isGroup && method(child);
  }) : method(el);
}
/**
 * Set hover style (namely "emphasis style") of element, based on the current
 * style of the given `el`.
 * This method should be called after all of the normal styles have been adopted
 * to the `el`. See the reason on `setHoverStyle`.
 *
 * @param {module:zrender/Element} el Should not be `zrender/container/Group`.
 * @param {Object|boolean} [hoverStl] The specified hover style.
 *        If set as `false`, disable the hover style.
 *        Similarly, The `el.hoverStyle` can alse be set
 *        as `false` to disable the hover style.
 *        Otherwise, use the default hover style if not provided.
 * @param {Object} [opt]
 * @param {boolean} [opt.hoverSilentOnTouch=false] See `graphic.setAsHoverStyleTrigger`
 */


function setElementHoverStyle(el, hoverStl) {
  // For performance consideration, it might be better to make the "hover style" only the
  // difference properties from the "normal style", but not a entire copy of all styles.
  hoverStl = el.__hoverStl = hoverStl !== false && (hoverStl || {});
  el.__hoverStlDirty = true; // FIXME
  // It is not completely right to save "normal"/"emphasis" flag on elements.
  // It probably should be saved on `data` of series. Consider the cases:
  // (1) A highlighted elements are moved out of the view port and re-enter
  // again by dataZoom.
  // (2) call `setOption` and replace elements totally when they are highlighted.

  if (el.__highlighted) {
    // Consider the case:
    // The styles of a highlighted `el` is being updated. The new "emphasis style"
    // should be adapted to the `el`. Notice here new "normal styles" should have
    // been set outside and the cached "normal style" is out of date.
    el.__cachedNormalStl = null; // Do not clear `__cachedNormalZ2` here, because setting `z2` is not a constraint
    // of this method. In most cases, `z2` is not set and hover style should be able
    // to rollback. Of course, that would bring bug, but only in a rare case, see
    // `doSingleLeaveHover` for details.

    doSingleLeaveHover(el);
    doSingleEnterHover(el);
  }
}
/**
 * Emphasis (called by API) has higher priority than `mouseover`.
 * When element has been called to be entered emphasis, mouse over
 * should not trigger the highlight effect (for example, animation
 * scale) again, and `mouseout` should not downplay the highlight
 * effect. So the listener of `mouseover` and `mouseout` should
 * check `isInEmphasis`.
 *
 * @param {module:zrender/Element} el
 * @return {boolean}
 */


function isInEmphasis(el) {
  return el && el.__isEmphasisEntered;
}

function onElementMouseOver(e) {
  if (this.__hoverSilentOnTouch && e.zrByTouch) {
    return;
  } // Only if element is not in emphasis status


  !this.__isEmphasisEntered && traverseCall(this, doSingleEnterHover);
}

function onElementMouseOut(e) {
  if (this.__hoverSilentOnTouch && e.zrByTouch) {
    return;
  } // Only if element is not in emphasis status


  !this.__isEmphasisEntered && traverseCall(this, doSingleLeaveHover);
}

function enterEmphasis() {
  this.__isEmphasisEntered = true;
  traverseCall(this, doSingleEnterHover);
}

function leaveEmphasis() {
  this.__isEmphasisEntered = false;
  traverseCall(this, doSingleLeaveHover);
}
/**
 * Set hover style (namely "emphasis style") of element,
 * based on the current style of the given `el`.
 *
 * (1)
 * **CONSTRAINTS** for this method:
 * <A> This method MUST be called after all of the normal styles having been adopted
 * to the `el`.
 * <B> The input `hoverStyle` (that is, "emphasis style") MUST be the subset of the
 * "normal style" having been set to the el.
 * <C> `color` MUST be one of the "normal styles" (because color might be lifted as
 * a default hover style).
 *
 * The reason: this method treat the current style of the `el` as the "normal style"
 * and cache them when enter/update the "emphasis style". Consider the case: the `el`
 * is in "emphasis" state and `setOption`/`dispatchAction` trigger the style updating
 * logic, where the el should shift from the original emphasis style to the new
 * "emphasis style" and should be able to "downplay" back to the new "normal style".
 *
 * Indeed, it is error-prone to make a interface has so many constraints, but I have
 * not found a better solution yet to fit the backward compatibility, performance and
 * the current programming style.
 *
 * (2)
 * Call the method for a "root" element once. Do not call it for each descendants.
 * If the descendants elemenets of a group has itself hover style different from the
 * root group, we can simply mount the style on `el.hoverStyle` for them, but should
 * not call this method for them.
 *
 * @param {module:zrender/Element} el
 * @param {Object|boolean} [hoverStyle] See `graphic.setElementHoverStyle`.
 * @param {Object} [opt]
 * @param {boolean} [opt.hoverSilentOnTouch=false] See `graphic.setAsHoverStyleTrigger`.
 */


function setHoverStyle(el, hoverStyle, opt) {
  el.isGroup ? el.traverse(function (child) {
    // If element has sepcified hoverStyle, then use it instead of given hoverStyle
    // Often used when item group has a label element and it's hoverStyle is different
    !child.isGroup && setElementHoverStyle(child, child.hoverStyle || hoverStyle);
  }) : setElementHoverStyle(el, el.hoverStyle || hoverStyle);
  setAsHoverStyleTrigger(el, opt);
}
/**
 * @param {Object|boolean} [opt] If `false`, means disable trigger.
 * @param {boolean} [opt.hoverSilentOnTouch=false]
 *        In touch device, mouseover event will be trigger on touchstart event
 *        (see module:zrender/dom/HandlerProxy). By this mechanism, we can
 *        conveniently use hoverStyle when tap on touch screen without additional
 *        code for compatibility.
 *        But if the chart/component has select feature, which usually also use
 *        hoverStyle, there might be conflict between 'select-highlight' and
 *        'hover-highlight' especially when roam is enabled (see geo for example).
 *        In this case, hoverSilentOnTouch should be used to disable hover-highlight
 *        on touch device.
 */


function setAsHoverStyleTrigger(el, opt) {
  var disable = opt === false;
  el.__hoverSilentOnTouch = opt != null && opt.hoverSilentOnTouch; // Simple optimize, since this method might be
  // called for each elements of a group in some cases.

  if (!disable || el.__hoverStyleTrigger) {
    var method = disable ? 'off' : 'on'; // Duplicated function will be auto-ignored, see Eventful.js.

    el[method]('mouseover', onElementMouseOver)[method]('mouseout', onElementMouseOut); // Emphasis, normal can be triggered manually

    el[method]('emphasis', enterEmphasis)[method]('normal', leaveEmphasis);
    el.__hoverStyleTrigger = !disable;
  }
}
/**
 * See more info in `setTextStyleCommon`.
 * @param {Object|module:zrender/graphic/Style} normalStyle
 * @param {Object} emphasisStyle
 * @param {module:echarts/model/Model} normalModel
 * @param {module:echarts/model/Model} emphasisModel
 * @param {Object} opt Check `opt` of `setTextStyleCommon` to find other props.
 * @param {string|Function} [opt.defaultText]
 * @param {module:echarts/model/Model} [opt.labelFetcher] Fetch text by
 *      `opt.labelFetcher.getFormattedLabel(opt.labelDataIndex, 'normal'/'emphasis', null, opt.labelDimIndex)`
 * @param {module:echarts/model/Model} [opt.labelDataIndex] Fetch text by
 *      `opt.textFetcher.getFormattedLabel(opt.labelDataIndex, 'normal'/'emphasis', null, opt.labelDimIndex)`
 * @param {module:echarts/model/Model} [opt.labelDimIndex] Fetch text by
 *      `opt.textFetcher.getFormattedLabel(opt.labelDataIndex, 'normal'/'emphasis', null, opt.labelDimIndex)`
 * @param {Object} [normalSpecified]
 * @param {Object} [emphasisSpecified]
 */


function setLabelStyle(normalStyle, emphasisStyle, normalModel, emphasisModel, opt, normalSpecified, emphasisSpecified) {
  opt = opt || EMPTY_OBJ;
  var labelFetcher = opt.labelFetcher;
  var labelDataIndex = opt.labelDataIndex;
  var labelDimIndex = opt.labelDimIndex; // This scenario, `label.normal.show = true; label.emphasis.show = false`,
  // is not supported util someone requests.

  var showNormal = normalModel.getShallow('show');
  var showEmphasis = emphasisModel.getShallow('show'); // Consider performance, only fetch label when necessary.
  // If `normal.show` is `false` and `emphasis.show` is `true` and `emphasis.formatter` is not set,
  // label should be displayed, where text is fetched by `normal.formatter` or `opt.defaultText`.

  var baseText;

  if (showNormal || showEmphasis) {
    if (labelFetcher) {
      baseText = labelFetcher.getFormattedLabel(labelDataIndex, 'normal', null, labelDimIndex);
    }

    if (baseText == null) {
      baseText = zrUtil.isFunction(opt.defaultText) ? opt.defaultText(labelDataIndex, opt) : opt.defaultText;
    }
  }

  var normalStyleText = showNormal ? baseText : null;
  var emphasisStyleText = showEmphasis ? zrUtil.retrieve2(labelFetcher ? labelFetcher.getFormattedLabel(labelDataIndex, 'emphasis', null, labelDimIndex) : null, baseText) : null; // Optimize: If style.text is null, text will not be drawn.

  if (normalStyleText != null || emphasisStyleText != null) {
    // Always set `textStyle` even if `normalStyle.text` is null, because default
    // values have to be set on `normalStyle`.
    // If we set default values on `emphasisStyle`, consider case:
    // Firstly, `setOption(... label: {normal: {text: null}, emphasis: {show: true}} ...);`
    // Secondly, `setOption(... label: {noraml: {show: true, text: 'abc', color: 'red'} ...);`
    // Then the 'red' will not work on emphasis.
    setTextStyle(normalStyle, normalModel, normalSpecified, opt);
    setTextStyle(emphasisStyle, emphasisModel, emphasisSpecified, opt, true);
  }

  normalStyle.text = normalStyleText;
  emphasisStyle.text = emphasisStyleText;
}
/**
 * Set basic textStyle properties.
 * See more info in `setTextStyleCommon`.
 * @param {Object|module:zrender/graphic/Style} textStyle
 * @param {module:echarts/model/Model} model
 * @param {Object} [specifiedTextStyle] Can be overrided by settings in model.
 * @param {Object} [opt] See `opt` of `setTextStyleCommon`.
 * @param {boolean} [isEmphasis]
 */


function setTextStyle(textStyle, textStyleModel, specifiedTextStyle, opt, isEmphasis) {
  setTextStyleCommon(textStyle, textStyleModel, opt, isEmphasis);
  specifiedTextStyle && zrUtil.extend(textStyle, specifiedTextStyle); // textStyle.host && textStyle.host.dirty && textStyle.host.dirty(false);

  return textStyle;
}
/**
 * Set text option in the style.
 * See more info in `setTextStyleCommon`.
 * @deprecated
 * @param {Object} textStyle
 * @param {module:echarts/model/Model} labelModel
 * @param {string|boolean} defaultColor Default text color.
 *        If set as false, it will be processed as a emphasis style.
 */


function setText(textStyle, labelModel, defaultColor) {
  var opt = {
    isRectText: true
  };
  var isEmphasis;

  if (defaultColor === false) {
    isEmphasis = true;
  } else {
    // Support setting color as 'auto' to get visual color.
    opt.autoColor = defaultColor;
  }

  setTextStyleCommon(textStyle, labelModel, opt, isEmphasis); // textStyle.host && textStyle.host.dirty && textStyle.host.dirty(false);
}
/**
 * The uniform entry of set text style, that is, retrieve style definitions
 * from `model` and set to `textStyle` object.
 *
 * Never in merge mode, but in overwrite mode, that is, all of the text style
 * properties will be set. (Consider the states of normal and emphasis and
 * default value can be adopted, merge would make the logic too complicated
 * to manage.)
 *
 * The `textStyle` object can either be a plain object or an instance of
 * `zrender/src/graphic/Style`, and either be the style of normal or emphasis.
 * After this mothod called, the `textStyle` object can then be used in
 * `el.setStyle(textStyle)` or `el.hoverStyle = textStyle`.
 *
 * Default value will be adopted and `insideRollbackOpt` will be created.
 * See `applyDefaultTextStyle` `rollbackDefaultTextStyle` for more details.
 *
 * opt: {
 *      disableBox: boolean, Whether diable drawing box of block (outer most).
 *      isRectText: boolean,
 *      autoColor: string, specify a color when color is 'auto',
 *              for textFill, textStroke, textBackgroundColor, and textBorderColor.
 *              If autoColor specified, it is used as default textFill.
 *      useInsideStyle:
 *              `true`: Use inside style (textFill, textStroke, textStrokeWidth)
 *                  if `textFill` is not specified.
 *              `false`: Do not use inside style.
 *              `null/undefined`: use inside style if `isRectText` is true and
 *                  `textFill` is not specified and textPosition contains `'inside'`.
 *      forceRich: boolean
 * }
 */


function setTextStyleCommon(textStyle, textStyleModel, opt, isEmphasis) {
  // Consider there will be abnormal when merge hover style to normal style if given default value.
  opt = opt || EMPTY_OBJ;

  if (opt.isRectText) {
    var textPosition = textStyleModel.getShallow('position') || (isEmphasis ? null : 'inside'); // 'outside' is not a valid zr textPostion value, but used
    // in bar series, and magric type should be considered.

    textPosition === 'outside' && (textPosition = 'top');
    textStyle.textPosition = textPosition;
    textStyle.textOffset = textStyleModel.getShallow('offset');
    var labelRotate = textStyleModel.getShallow('rotate');
    labelRotate != null && (labelRotate *= Math.PI / 180);
    textStyle.textRotation = labelRotate;
    textStyle.textDistance = zrUtil.retrieve2(textStyleModel.getShallow('distance'), isEmphasis ? null : 5);
  }

  var ecModel = textStyleModel.ecModel;
  var globalTextStyle = ecModel && ecModel.option.textStyle; // Consider case:
  // {
  //     data: [{
  //         value: 12,
  //         label: {
  //             rich: {
  //                 // no 'a' here but using parent 'a'.
  //             }
  //         }
  //     }],
  //     rich: {
  //         a: { ... }
  //     }
  // }

  var richItemNames = getRichItemNames(textStyleModel);
  var richResult;

  if (richItemNames) {
    richResult = {};

    for (var name in richItemNames) {
      if (richItemNames.hasOwnProperty(name)) {
        // Cascade is supported in rich.
        var richTextStyle = textStyleModel.getModel(['rich', name]); // In rich, never `disableBox`.

        setTokenTextStyle(richResult[name] = {}, richTextStyle, globalTextStyle, opt, isEmphasis);
      }
    }
  }

  textStyle.rich = richResult;
  setTokenTextStyle(textStyle, textStyleModel, globalTextStyle, opt, isEmphasis, true);

  if (opt.forceRich && !opt.textStyle) {
    opt.textStyle = {};
  }

  return textStyle;
} // Consider case:
// {
//     data: [{
//         value: 12,
//         label: {
//             rich: {
//                 // no 'a' here but using parent 'a'.
//             }
//         }
//     }],
//     rich: {
//         a: { ... }
//     }
// }


function getRichItemNames(textStyleModel) {
  // Use object to remove duplicated names.
  var richItemNameMap;

  while (textStyleModel && textStyleModel !== textStyleModel.ecModel) {
    var rich = (textStyleModel.option || EMPTY_OBJ).rich;

    if (rich) {
      richItemNameMap = richItemNameMap || {};

      for (var name in rich) {
        if (rich.hasOwnProperty(name)) {
          richItemNameMap[name] = 1;
        }
      }
    }

    textStyleModel = textStyleModel.parentModel;
  }

  return richItemNameMap;
}

function setTokenTextStyle(textStyle, textStyleModel, globalTextStyle, opt, isEmphasis, isBlock) {
  // In merge mode, default value should not be given.
  globalTextStyle = !isEmphasis && globalTextStyle || EMPTY_OBJ;
  textStyle.textFill = getAutoColor(textStyleModel.getShallow('color'), opt) || globalTextStyle.color;
  textStyle.textStroke = getAutoColor(textStyleModel.getShallow('textBorderColor'), opt) || globalTextStyle.textBorderColor;
  textStyle.textStrokeWidth = zrUtil.retrieve2(textStyleModel.getShallow('textBorderWidth'), globalTextStyle.textBorderWidth); // Save original textPosition, because style.textPosition will be repalced by
  // real location (like [10, 30]) in zrender.

  textStyle.insideRawTextPosition = textStyle.textPosition;

  if (!isEmphasis) {
    if (isBlock) {
      textStyle.insideRollbackOpt = opt;
      applyDefaultTextStyle(textStyle);
    } // Set default finally.


    if (textStyle.textFill == null) {
      textStyle.textFill = opt.autoColor;
    }
  } // Do not use `getFont` here, because merge should be supported, where
  // part of these properties may be changed in emphasis style, and the
  // others should remain their original value got from normal style.


  textStyle.fontStyle = textStyleModel.getShallow('fontStyle') || globalTextStyle.fontStyle;
  textStyle.fontWeight = textStyleModel.getShallow('fontWeight') || globalTextStyle.fontWeight;
  textStyle.fontSize = textStyleModel.getShallow('fontSize') || globalTextStyle.fontSize;
  textStyle.fontFamily = textStyleModel.getShallow('fontFamily') || globalTextStyle.fontFamily;
  textStyle.textAlign = textStyleModel.getShallow('align');
  textStyle.textVerticalAlign = textStyleModel.getShallow('verticalAlign') || textStyleModel.getShallow('baseline');
  textStyle.textLineHeight = textStyleModel.getShallow('lineHeight');
  textStyle.textWidth = textStyleModel.getShallow('width');
  textStyle.textHeight = textStyleModel.getShallow('height');
  textStyle.textTag = textStyleModel.getShallow('tag');

  if (!isBlock || !opt.disableBox) {
    textStyle.textBackgroundColor = getAutoColor(textStyleModel.getShallow('backgroundColor'), opt);
    textStyle.textPadding = textStyleModel.getShallow('padding');
    textStyle.textBorderColor = getAutoColor(textStyleModel.getShallow('borderColor'), opt);
    textStyle.textBorderWidth = textStyleModel.getShallow('borderWidth');
    textStyle.textBorderRadius = textStyleModel.getShallow('borderRadius');
    textStyle.textBoxShadowColor = textStyleModel.getShallow('shadowColor');
    textStyle.textBoxShadowBlur = textStyleModel.getShallow('shadowBlur');
    textStyle.textBoxShadowOffsetX = textStyleModel.getShallow('shadowOffsetX');
    textStyle.textBoxShadowOffsetY = textStyleModel.getShallow('shadowOffsetY');
  }

  textStyle.textShadowColor = textStyleModel.getShallow('textShadowColor') || globalTextStyle.textShadowColor;
  textStyle.textShadowBlur = textStyleModel.getShallow('textShadowBlur') || globalTextStyle.textShadowBlur;
  textStyle.textShadowOffsetX = textStyleModel.getShallow('textShadowOffsetX') || globalTextStyle.textShadowOffsetX;
  textStyle.textShadowOffsetY = textStyleModel.getShallow('textShadowOffsetY') || globalTextStyle.textShadowOffsetY;
}

function getAutoColor(color, opt) {
  return color !== 'auto' ? color : opt && opt.autoColor ? opt.autoColor : null;
}
/**
 * Give some default value to the input `textStyle` object, based on the current settings
 * in this `textStyle` object.
 *
 * The Scenario:
 * when text position is `inside` and `textFill` is not specified, we show
 * text border by default for better view. But it should be considered that text position
 * might be changed when hovering or being emphasis, where the `insideRollback` is used to
 * restore the style.
 *
 * Usage (& NOTICE):
 * When a style object (eithor plain object or instance of `zrender/src/graphic/Style`) is
 * about to be modified on its text related properties, `rollbackDefaultTextStyle` should
 * be called before the modification and `applyDefaultTextStyle` should be called after that.
 * (For the case that all of the text related properties is reset, like `setTextStyleCommon`
 * does, `rollbackDefaultTextStyle` is not needed to be called).
 */


function applyDefaultTextStyle(textStyle) {
  var opt = textStyle.insideRollbackOpt; // Only `insideRollbackOpt` created (in `setTextStyleCommon`),
  // applyDefaultTextStyle works.

  if (!opt || textStyle.textFill != null) {
    return;
  }

  var useInsideStyle = opt.useInsideStyle;
  var textPosition = textStyle.insideRawTextPosition;
  var insideRollback;
  var autoColor = opt.autoColor;

  if (useInsideStyle !== false && (useInsideStyle === true || opt.isRectText && textPosition // textPosition can be [10, 30]
  && typeof textPosition === 'string' && textPosition.indexOf('inside') >= 0)) {
    insideRollback = {
      textFill: null,
      textStroke: textStyle.textStroke,
      textStrokeWidth: textStyle.textStrokeWidth
    };
    textStyle.textFill = '#fff'; // Consider text with #fff overflow its container.

    if (textStyle.textStroke == null) {
      textStyle.textStroke = autoColor;
      textStyle.textStrokeWidth == null && (textStyle.textStrokeWidth = 2);
    }
  } else if (autoColor != null) {
    insideRollback = {
      textFill: null
    };
    textStyle.textFill = autoColor;
  } // Always set `insideRollback`, for clearing previous.


  if (insideRollback) {
    textStyle.insideRollback = insideRollback;
  }
}
/**
 * Consider the case: in a scatter,
 * label: {
 *     normal: {position: 'inside'},
 *     emphasis: {position: 'top'}
 * }
 * In the normal state, the `textFill` will be set as '#fff' for pretty view (see
 * `applyDefaultTextStyle`), but when switching to emphasis state, the `textFill`
 * should be retured to 'autoColor', but not keep '#fff'.
 */


function rollbackDefaultTextStyle(style) {
  var insideRollback = style.insideRollback;

  if (insideRollback) {
    style.textFill = insideRollback.textFill;
    style.textStroke = insideRollback.textStroke;
    style.textStrokeWidth = insideRollback.textStrokeWidth;
    style.insideRollback = null;
  }
}

function getFont(opt, ecModel) {
  // ecModel or default text style model.
  var gTextStyleModel = ecModel || ecModel.getModel('textStyle');
  return zrUtil.trim([// FIXME in node-canvas fontWeight is before fontStyle
  opt.fontStyle || gTextStyleModel && gTextStyleModel.getShallow('fontStyle') || '', opt.fontWeight || gTextStyleModel && gTextStyleModel.getShallow('fontWeight') || '', (opt.fontSize || gTextStyleModel && gTextStyleModel.getShallow('fontSize') || 12) + 'px', opt.fontFamily || gTextStyleModel && gTextStyleModel.getShallow('fontFamily') || 'sans-serif'].join(' '));
}

function animateOrSetProps(isUpdate, el, props, animatableModel, dataIndex, cb) {
  if (typeof dataIndex === 'function') {
    cb = dataIndex;
    dataIndex = null;
  } // Do not check 'animation' property directly here. Consider this case:
  // animation model is an `itemModel`, whose does not have `isAnimationEnabled`
  // but its parent model (`seriesModel`) does.


  var animationEnabled = animatableModel && animatableModel.isAnimationEnabled();

  if (animationEnabled) {
    var postfix = isUpdate ? 'Update' : '';
    var duration = animatableModel.getShallow('animationDuration' + postfix);
    var animationEasing = animatableModel.getShallow('animationEasing' + postfix);
    var animationDelay = animatableModel.getShallow('animationDelay' + postfix);

    if (typeof animationDelay === 'function') {
      animationDelay = animationDelay(dataIndex, animatableModel.getAnimationDelayParams ? animatableModel.getAnimationDelayParams(el, dataIndex) : null);
    }

    if (typeof duration === 'function') {
      duration = duration(dataIndex);
    }

    duration > 0 ? el.animateTo(props, duration, animationDelay || 0, animationEasing, cb, !!cb) : (el.stopAnimation(), el.attr(props), cb && cb());
  } else {
    el.stopAnimation();
    el.attr(props);
    cb && cb();
  }
}
/**
 * Update graphic element properties with or without animation according to the
 * configuration in series.
 *
 * Caution: this method will stop previous animation.
 * So if do not use this method to one element twice before
 * animation starts, unless you know what you are doing.
 *
 * @param {module:zrender/Element} el
 * @param {Object} props
 * @param {module:echarts/model/Model} [animatableModel]
 * @param {number} [dataIndex]
 * @param {Function} [cb]
 * @example
 *     graphic.updateProps(el, {
 *         position: [100, 100]
 *     }, seriesModel, dataIndex, function () { console.log('Animation done!'); });
 *     // Or
 *     graphic.updateProps(el, {
 *         position: [100, 100]
 *     }, seriesModel, function () { console.log('Animation done!'); });
 */


function updateProps(el, props, animatableModel, dataIndex, cb) {
  animateOrSetProps(true, el, props, animatableModel, dataIndex, cb);
}
/**
 * Init graphic element properties with or without animation according to the
 * configuration in series.
 *
 * Caution: this method will stop previous animation.
 * So if do not use this method to one element twice before
 * animation starts, unless you know what you are doing.
 *
 * @param {module:zrender/Element} el
 * @param {Object} props
 * @param {module:echarts/model/Model} [animatableModel]
 * @param {number} [dataIndex]
 * @param {Function} cb
 */


function initProps(el, props, animatableModel, dataIndex, cb) {
  animateOrSetProps(false, el, props, animatableModel, dataIndex, cb);
}
/**
 * Get transform matrix of target (param target),
 * in coordinate of its ancestor (param ancestor)
 *
 * @param {module:zrender/mixin/Transformable} target
 * @param {module:zrender/mixin/Transformable} [ancestor]
 */


function getTransform(target, ancestor) {
  var mat = matrix.identity([]);

  while (target && target !== ancestor) {
    matrix.mul(mat, target.getLocalTransform(), mat);
    target = target.parent;
  }

  return mat;
}
/**
 * Apply transform to an vertex.
 * @param {Array.<number>} target [x, y]
 * @param {Array.<number>|TypedArray.<number>|Object} transform Can be:
 *      + Transform matrix: like [1, 0, 0, 1, 0, 0]
 *      + {position, rotation, scale}, the same as `zrender/Transformable`.
 * @param {boolean=} invert Whether use invert matrix.
 * @return {Array.<number>} [x, y]
 */


function applyTransform(target, transform, invert) {
  if (transform && !zrUtil.isArrayLike(transform)) {
    transform = Transformable.getLocalTransform(transform);
  }

  if (invert) {
    transform = matrix.invert([], transform);
  }

  return vector.applyTransform([], target, transform);
}
/**
 * @param {string} direction 'left' 'right' 'top' 'bottom'
 * @param {Array.<number>} transform Transform matrix: like [1, 0, 0, 1, 0, 0]
 * @param {boolean=} invert Whether use invert matrix.
 * @return {string} Transformed direction. 'left' 'right' 'top' 'bottom'
 */


function transformDirection(direction, transform, invert) {
  // Pick a base, ensure that transform result will not be (0, 0).
  var hBase = transform[4] === 0 || transform[5] === 0 || transform[0] === 0 ? 1 : Math.abs(2 * transform[4] / transform[0]);
  var vBase = transform[4] === 0 || transform[5] === 0 || transform[2] === 0 ? 1 : Math.abs(2 * transform[4] / transform[2]);
  var vertex = [direction === 'left' ? -hBase : direction === 'right' ? hBase : 0, direction === 'top' ? -vBase : direction === 'bottom' ? vBase : 0];
  vertex = applyTransform(vertex, transform, invert);
  return Math.abs(vertex[0]) > Math.abs(vertex[1]) ? vertex[0] > 0 ? 'right' : 'left' : vertex[1] > 0 ? 'bottom' : 'top';
}
/**
 * Apply group transition animation from g1 to g2.
 * If no animatableModel, no animation.
 */


function groupTransition(g1, g2, animatableModel, cb) {
  if (!g1 || !g2) {
    return;
  }

  function getElMap(g) {
    var elMap = {};
    g.traverse(function (el) {
      if (!el.isGroup && el.anid) {
        elMap[el.anid] = el;
      }
    });
    return elMap;
  }

  function getAnimatableProps(el) {
    var obj = {
      position: vector.clone(el.position),
      rotation: el.rotation
    };

    if (el.shape) {
      obj.shape = zrUtil.extend({}, el.shape);
    }

    return obj;
  }

  var elMap1 = getElMap(g1);
  g2.traverse(function (el) {
    if (!el.isGroup && el.anid) {
      var oldEl = elMap1[el.anid];

      if (oldEl) {
        var newProp = getAnimatableProps(el);
        el.attr(getAnimatableProps(oldEl));
        updateProps(el, newProp, animatableModel, el.dataIndex);
      } // else {
      //     if (el.previousProps) {
      //         graphic.updateProps
      //     }
      // }

    }
  });
}
/**
 * @param {Array.<Array.<number>>} points Like: [[23, 44], [53, 66], ...]
 * @param {Object} rect {x, y, width, height}
 * @return {Array.<Array.<number>>} A new clipped points.
 */


function clipPointsByRect(points, rect) {
  // FIXME: this way migth be incorrect when grpahic clipped by a corner.
  // and when element have border.
  return zrUtil.map(points, function (point) {
    var x = point[0];
    x = mathMax(x, rect.x);
    x = mathMin(x, rect.x + rect.width);
    var y = point[1];
    y = mathMax(y, rect.y);
    y = mathMin(y, rect.y + rect.height);
    return [x, y];
  });
}
/**
 * @param {Object} targetRect {x, y, width, height}
 * @param {Object} rect {x, y, width, height}
 * @return {Object} A new clipped rect. If rect size are negative, return undefined.
 */


function clipRectByRect(targetRect, rect) {
  var x = mathMax(targetRect.x, rect.x);
  var x2 = mathMin(targetRect.x + targetRect.width, rect.x + rect.width);
  var y = mathMax(targetRect.y, rect.y);
  var y2 = mathMin(targetRect.y + targetRect.height, rect.y + rect.height); // If the total rect is cliped, nothing, including the border,
  // should be painted. So return undefined.

  if (x2 >= x && y2 >= y) {
    return {
      x: x,
      y: y,
      width: x2 - x,
      height: y2 - y
    };
  }
}
/**
 * @param {string} iconStr Support 'image://' or 'path://' or direct svg path.
 * @param {Object} [opt] Properties of `module:zrender/Element`, except `style`.
 * @param {Object} [rect] {x, y, width, height}
 * @return {module:zrender/Element} Icon path or image element.
 */


function createIcon(iconStr, opt, rect) {
  opt = zrUtil.extend({
    rectHover: true
  }, opt);
  var style = opt.style = {
    strokeNoScale: true
  };
  rect = rect || {
    x: -1,
    y: -1,
    width: 2,
    height: 2
  };

  if (iconStr) {
    return iconStr.indexOf('image://') === 0 ? (style.image = iconStr.slice(8), zrUtil.defaults(style, rect), new ZImage(opt)) : makePath(iconStr.replace('path://', ''), opt, rect, 'center');
  }
}

exports.Z2_EMPHASIS_LIFT = Z2_EMPHASIS_LIFT;
exports.extendShape = extendShape;
exports.extendPath = extendPath;
exports.makePath = makePath;
exports.makeImage = makeImage;
exports.mergePath = mergePath;
exports.resizePath = resizePath;
exports.subPixelOptimizeLine = subPixelOptimizeLine;
exports.subPixelOptimizeRect = subPixelOptimizeRect;
exports.subPixelOptimize = subPixelOptimize;
exports.setElementHoverStyle = setElementHoverStyle;
exports.isInEmphasis = isInEmphasis;
exports.setHoverStyle = setHoverStyle;
exports.setAsHoverStyleTrigger = setAsHoverStyleTrigger;
exports.setLabelStyle = setLabelStyle;
exports.setTextStyle = setTextStyle;
exports.setText = setText;
exports.getFont = getFont;
exports.updateProps = updateProps;
exports.initProps = initProps;
exports.getTransform = getTransform;
exports.applyTransform = applyTransform;
exports.transformDirection = transformDirection;
exports.groupTransition = groupTransition;
exports.clipPointsByRect = clipPointsByRect;
exports.clipRectByRect = clipRectByRect;
exports.createIcon = createIcon;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdXRpbC9ncmFwaGljLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdXRpbC9ncmFwaGljLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIHBhdGhUb29sID0gcmVxdWlyZShcInpyZW5kZXIvbGliL3Rvb2wvcGF0aFwiKTtcblxudmFyIGNvbG9yVG9vbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi90b29sL2NvbG9yXCIpO1xuXG52YXIgbWF0cml4ID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvbWF0cml4XCIpO1xuXG52YXIgdmVjdG9yID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdmVjdG9yXCIpO1xuXG52YXIgUGF0aCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9ncmFwaGljL1BhdGhcIik7XG5cbnZhciBUcmFuc2Zvcm1hYmxlID0gcmVxdWlyZShcInpyZW5kZXIvbGliL21peGluL1RyYW5zZm9ybWFibGVcIik7XG5cbnZhciBaSW1hZ2UgPSByZXF1aXJlKFwienJlbmRlci9saWIvZ3JhcGhpYy9JbWFnZVwiKTtcblxuZXhwb3J0cy5JbWFnZSA9IFpJbWFnZTtcblxudmFyIEdyb3VwID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvbnRhaW5lci9Hcm91cFwiKTtcblxuZXhwb3J0cy5Hcm91cCA9IEdyb3VwO1xuXG52YXIgVGV4dCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9ncmFwaGljL1RleHRcIik7XG5cbmV4cG9ydHMuVGV4dCA9IFRleHQ7XG5cbnZhciBDaXJjbGUgPSByZXF1aXJlKFwienJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9DaXJjbGVcIik7XG5cbmV4cG9ydHMuQ2lyY2xlID0gQ2lyY2xlO1xuXG52YXIgU2VjdG9yID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2dyYXBoaWMvc2hhcGUvU2VjdG9yXCIpO1xuXG5leHBvcnRzLlNlY3RvciA9IFNlY3RvcjtcblxudmFyIFJpbmcgPSByZXF1aXJlKFwienJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9SaW5nXCIpO1xuXG5leHBvcnRzLlJpbmcgPSBSaW5nO1xuXG52YXIgUG9seWdvbiA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9ncmFwaGljL3NoYXBlL1BvbHlnb25cIik7XG5cbmV4cG9ydHMuUG9seWdvbiA9IFBvbHlnb247XG5cbnZhciBQb2x5bGluZSA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9ncmFwaGljL3NoYXBlL1BvbHlsaW5lXCIpO1xuXG5leHBvcnRzLlBvbHlsaW5lID0gUG9seWxpbmU7XG5cbnZhciBSZWN0ID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2dyYXBoaWMvc2hhcGUvUmVjdFwiKTtcblxuZXhwb3J0cy5SZWN0ID0gUmVjdDtcblxudmFyIExpbmUgPSByZXF1aXJlKFwienJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9MaW5lXCIpO1xuXG5leHBvcnRzLkxpbmUgPSBMaW5lO1xuXG52YXIgQmV6aWVyQ3VydmUgPSByZXF1aXJlKFwienJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9CZXppZXJDdXJ2ZVwiKTtcblxuZXhwb3J0cy5CZXppZXJDdXJ2ZSA9IEJlemllckN1cnZlO1xuXG52YXIgQXJjID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2dyYXBoaWMvc2hhcGUvQXJjXCIpO1xuXG5leHBvcnRzLkFyYyA9IEFyYztcblxudmFyIENvbXBvdW5kUGF0aCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9ncmFwaGljL0NvbXBvdW5kUGF0aFwiKTtcblxuZXhwb3J0cy5Db21wb3VuZFBhdGggPSBDb21wb3VuZFBhdGg7XG5cbnZhciBMaW5lYXJHcmFkaWVudCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9ncmFwaGljL0xpbmVhckdyYWRpZW50XCIpO1xuXG5leHBvcnRzLkxpbmVhckdyYWRpZW50ID0gTGluZWFyR3JhZGllbnQ7XG5cbnZhciBSYWRpYWxHcmFkaWVudCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9ncmFwaGljL1JhZGlhbEdyYWRpZW50XCIpO1xuXG5leHBvcnRzLlJhZGlhbEdyYWRpZW50ID0gUmFkaWFsR3JhZGllbnQ7XG5cbnZhciBCb3VuZGluZ1JlY3QgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS9Cb3VuZGluZ1JlY3RcIik7XG5cbmV4cG9ydHMuQm91bmRpbmdSZWN0ID0gQm91bmRpbmdSZWN0O1xuXG52YXIgSW5jcmVtZW50YWxEaXNwbGF5YWJsZSA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9ncmFwaGljL0luY3JlbWVudGFsRGlzcGxheWFibGVcIik7XG5cbmV4cG9ydHMuSW5jcmVtZW50YWxEaXNwbGF5YWJsZSA9IEluY3JlbWVudGFsRGlzcGxheWFibGU7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciByb3VuZCA9IE1hdGgucm91bmQ7XG52YXIgbWF0aE1heCA9IE1hdGgubWF4O1xudmFyIG1hdGhNaW4gPSBNYXRoLm1pbjtcbnZhciBFTVBUWV9PQkogPSB7fTtcbnZhciBaMl9FTVBIQVNJU19MSUZUID0gMTtcbi8qKlxuICogRXh0ZW5kIHNoYXBlIHdpdGggcGFyYW1ldGVyc1xuICovXG5cbmZ1bmN0aW9uIGV4dGVuZFNoYXBlKG9wdHMpIHtcbiAgcmV0dXJuIFBhdGguZXh0ZW5kKG9wdHMpO1xufVxuLyoqXG4gKiBFeHRlbmQgcGF0aFxuICovXG5cblxuZnVuY3Rpb24gZXh0ZW5kUGF0aChwYXRoRGF0YSwgb3B0cykge1xuICByZXR1cm4gcGF0aFRvb2wuZXh0ZW5kRnJvbVN0cmluZyhwYXRoRGF0YSwgb3B0cyk7XG59XG4vKipcbiAqIENyZWF0ZSBhIHBhdGggZWxlbWVudCBmcm9tIHBhdGggZGF0YSBzdHJpbmdcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXRoRGF0YVxuICogQHBhcmFtIHtPYmplY3R9IG9wdHNcbiAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvY29yZS9Cb3VuZGluZ1JlY3R9IHJlY3RcbiAqIEBwYXJhbSB7c3RyaW5nfSBbbGF5b3V0PWNvdmVyXSAnY2VudGVyJyBvciAnY292ZXInXG4gKi9cblxuXG5mdW5jdGlvbiBtYWtlUGF0aChwYXRoRGF0YSwgb3B0cywgcmVjdCwgbGF5b3V0KSB7XG4gIHZhciBwYXRoID0gcGF0aFRvb2wuY3JlYXRlRnJvbVN0cmluZyhwYXRoRGF0YSwgb3B0cyk7XG5cbiAgaWYgKHJlY3QpIHtcbiAgICBpZiAobGF5b3V0ID09PSAnY2VudGVyJykge1xuICAgICAgcmVjdCA9IGNlbnRlckdyYXBoaWMocmVjdCwgcGF0aC5nZXRCb3VuZGluZ1JlY3QoKSk7XG4gICAgfVxuXG4gICAgcmVzaXplUGF0aChwYXRoLCByZWN0KTtcbiAgfVxuXG4gIHJldHVybiBwYXRoO1xufVxuLyoqXG4gKiBDcmVhdGUgYSBpbWFnZSBlbGVtZW50IGZyb20gaW1hZ2UgdXJsXG4gKiBAcGFyYW0ge3N0cmluZ30gaW1hZ2VVcmwgaW1hZ2UgdXJsXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0cyBvcHRpb25zXG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL2NvcmUvQm91bmRpbmdSZWN0fSByZWN0IGNvbnN0cmFpbiByZWN0XG4gKiBAcGFyYW0ge3N0cmluZ30gW2xheW91dD1jb3Zlcl0gJ2NlbnRlcicgb3IgJ2NvdmVyJ1xuICovXG5cblxuZnVuY3Rpb24gbWFrZUltYWdlKGltYWdlVXJsLCByZWN0LCBsYXlvdXQpIHtcbiAgdmFyIHBhdGggPSBuZXcgWkltYWdlKHtcbiAgICBzdHlsZToge1xuICAgICAgaW1hZ2U6IGltYWdlVXJsLFxuICAgICAgeDogcmVjdC54LFxuICAgICAgeTogcmVjdC55LFxuICAgICAgd2lkdGg6IHJlY3Qud2lkdGgsXG4gICAgICBoZWlnaHQ6IHJlY3QuaGVpZ2h0XG4gICAgfSxcbiAgICBvbmxvYWQ6IGZ1bmN0aW9uIChpbWcpIHtcbiAgICAgIGlmIChsYXlvdXQgPT09ICdjZW50ZXInKSB7XG4gICAgICAgIHZhciBib3VuZGluZ1JlY3QgPSB7XG4gICAgICAgICAgd2lkdGg6IGltZy53aWR0aCxcbiAgICAgICAgICBoZWlnaHQ6IGltZy5oZWlnaHRcbiAgICAgICAgfTtcbiAgICAgICAgcGF0aC5zZXRTdHlsZShjZW50ZXJHcmFwaGljKHJlY3QsIGJvdW5kaW5nUmVjdCkpO1xuICAgICAgfVxuICAgIH1cbiAgfSk7XG4gIHJldHVybiBwYXRoO1xufVxuLyoqXG4gKiBHZXQgcG9zaXRpb24gb2YgY2VudGVyZWQgZWxlbWVudCBpbiBib3VuZGluZyBib3guXG4gKlxuICogQHBhcmFtICB7T2JqZWN0fSByZWN0ICAgICAgICAgZWxlbWVudCBsb2NhbCBib3VuZGluZyBib3hcbiAqIEBwYXJhbSAge09iamVjdH0gYm91bmRpbmdSZWN0IGNvbnN0cmFpbnQgYm91bmRpbmcgYm94XG4gKiBAcmV0dXJuIHtPYmplY3R9IGVsZW1lbnQgcG9zaXRpb24gY29udGFpbmluZyB4LCB5LCB3aWR0aCwgYW5kIGhlaWdodFxuICovXG5cblxuZnVuY3Rpb24gY2VudGVyR3JhcGhpYyhyZWN0LCBib3VuZGluZ1JlY3QpIHtcbiAgLy8gU2V0IHJlY3QgdG8gY2VudGVyLCBrZWVwIHdpZHRoIC8gaGVpZ2h0IHJhdGlvLlxuICB2YXIgYXNwZWN0ID0gYm91bmRpbmdSZWN0LndpZHRoIC8gYm91bmRpbmdSZWN0LmhlaWdodDtcbiAgdmFyIHdpZHRoID0gcmVjdC5oZWlnaHQgKiBhc3BlY3Q7XG4gIHZhciBoZWlnaHQ7XG5cbiAgaWYgKHdpZHRoIDw9IHJlY3Qud2lkdGgpIHtcbiAgICBoZWlnaHQgPSByZWN0LmhlaWdodDtcbiAgfSBlbHNlIHtcbiAgICB3aWR0aCA9IHJlY3Qud2lkdGg7XG4gICAgaGVpZ2h0ID0gd2lkdGggLyBhc3BlY3Q7XG4gIH1cblxuICB2YXIgY3ggPSByZWN0LnggKyByZWN0LndpZHRoIC8gMjtcbiAgdmFyIGN5ID0gcmVjdC55ICsgcmVjdC5oZWlnaHQgLyAyO1xuICByZXR1cm4ge1xuICAgIHg6IGN4IC0gd2lkdGggLyAyLFxuICAgIHk6IGN5IC0gaGVpZ2h0IC8gMixcbiAgICB3aWR0aDogd2lkdGgsXG4gICAgaGVpZ2h0OiBoZWlnaHRcbiAgfTtcbn1cblxudmFyIG1lcmdlUGF0aCA9IHBhdGhUb29sLm1lcmdlUGF0aDtcbi8qKlxuICogUmVzaXplIGEgcGF0aCB0byBmaXQgdGhlIHJlY3RcbiAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvZ3JhcGhpYy9QYXRofSBwYXRoXG4gKiBAcGFyYW0ge09iamVjdH0gcmVjdFxuICovXG5cbmZ1bmN0aW9uIHJlc2l6ZVBhdGgocGF0aCwgcmVjdCkge1xuICBpZiAoIXBhdGguYXBwbHlUcmFuc2Zvcm0pIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgcGF0aFJlY3QgPSBwYXRoLmdldEJvdW5kaW5nUmVjdCgpO1xuICB2YXIgbSA9IHBhdGhSZWN0LmNhbGN1bGF0ZVRyYW5zZm9ybShyZWN0KTtcbiAgcGF0aC5hcHBseVRyYW5zZm9ybShtKTtcbn1cbi8qKlxuICogU3ViIHBpeGVsIG9wdGltaXplIGxpbmUgZm9yIGNhbnZhc1xuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXJhbVxuICogQHBhcmFtIHtPYmplY3R9IFtwYXJhbS5zaGFwZV1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbcGFyYW0uc2hhcGUueDFdXG4gKiBAcGFyYW0ge251bWJlcn0gW3BhcmFtLnNoYXBlLnkxXVxuICogQHBhcmFtIHtudW1iZXJ9IFtwYXJhbS5zaGFwZS54Ml1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbcGFyYW0uc2hhcGUueTJdXG4gKiBAcGFyYW0ge09iamVjdH0gW3BhcmFtLnN0eWxlXVxuICogQHBhcmFtIHtudW1iZXJ9IFtwYXJhbS5zdHlsZS5saW5lV2lkdGhdXG4gKiBAcmV0dXJuIHtPYmplY3R9IE1vZGlmaWVkIHBhcmFtXG4gKi9cblxuXG5mdW5jdGlvbiBzdWJQaXhlbE9wdGltaXplTGluZShwYXJhbSkge1xuICB2YXIgc2hhcGUgPSBwYXJhbS5zaGFwZTtcbiAgdmFyIGxpbmVXaWR0aCA9IHBhcmFtLnN0eWxlLmxpbmVXaWR0aDtcblxuICBpZiAocm91bmQoc2hhcGUueDEgKiAyKSA9PT0gcm91bmQoc2hhcGUueDIgKiAyKSkge1xuICAgIHNoYXBlLngxID0gc2hhcGUueDIgPSBzdWJQaXhlbE9wdGltaXplKHNoYXBlLngxLCBsaW5lV2lkdGgsIHRydWUpO1xuICB9XG5cbiAgaWYgKHJvdW5kKHNoYXBlLnkxICogMikgPT09IHJvdW5kKHNoYXBlLnkyICogMikpIHtcbiAgICBzaGFwZS55MSA9IHNoYXBlLnkyID0gc3ViUGl4ZWxPcHRpbWl6ZShzaGFwZS55MSwgbGluZVdpZHRoLCB0cnVlKTtcbiAgfVxuXG4gIHJldHVybiBwYXJhbTtcbn1cbi8qKlxuICogU3ViIHBpeGVsIG9wdGltaXplIHJlY3QgZm9yIGNhbnZhc1xuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXJhbVxuICogQHBhcmFtIHtPYmplY3R9IFtwYXJhbS5zaGFwZV1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbcGFyYW0uc2hhcGUueF1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbcGFyYW0uc2hhcGUueV1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbcGFyYW0uc2hhcGUud2lkdGhdXG4gKiBAcGFyYW0ge251bWJlcn0gW3BhcmFtLnNoYXBlLmhlaWdodF1cbiAqIEBwYXJhbSB7T2JqZWN0fSBbcGFyYW0uc3R5bGVdXG4gKiBAcGFyYW0ge251bWJlcn0gW3BhcmFtLnN0eWxlLmxpbmVXaWR0aF1cbiAqIEByZXR1cm4ge09iamVjdH0gTW9kaWZpZWQgcGFyYW1cbiAqL1xuXG5cbmZ1bmN0aW9uIHN1YlBpeGVsT3B0aW1pemVSZWN0KHBhcmFtKSB7XG4gIHZhciBzaGFwZSA9IHBhcmFtLnNoYXBlO1xuICB2YXIgbGluZVdpZHRoID0gcGFyYW0uc3R5bGUubGluZVdpZHRoO1xuICB2YXIgb3JpZ2luWCA9IHNoYXBlLng7XG4gIHZhciBvcmlnaW5ZID0gc2hhcGUueTtcbiAgdmFyIG9yaWdpbldpZHRoID0gc2hhcGUud2lkdGg7XG4gIHZhciBvcmlnaW5IZWlnaHQgPSBzaGFwZS5oZWlnaHQ7XG4gIHNoYXBlLnggPSBzdWJQaXhlbE9wdGltaXplKHNoYXBlLngsIGxpbmVXaWR0aCwgdHJ1ZSk7XG4gIHNoYXBlLnkgPSBzdWJQaXhlbE9wdGltaXplKHNoYXBlLnksIGxpbmVXaWR0aCwgdHJ1ZSk7XG4gIHNoYXBlLndpZHRoID0gTWF0aC5tYXgoc3ViUGl4ZWxPcHRpbWl6ZShvcmlnaW5YICsgb3JpZ2luV2lkdGgsIGxpbmVXaWR0aCwgZmFsc2UpIC0gc2hhcGUueCwgb3JpZ2luV2lkdGggPT09IDAgPyAwIDogMSk7XG4gIHNoYXBlLmhlaWdodCA9IE1hdGgubWF4KHN1YlBpeGVsT3B0aW1pemUob3JpZ2luWSArIG9yaWdpbkhlaWdodCwgbGluZVdpZHRoLCBmYWxzZSkgLSBzaGFwZS55LCBvcmlnaW5IZWlnaHQgPT09IDAgPyAwIDogMSk7XG4gIHJldHVybiBwYXJhbTtcbn1cbi8qKlxuICogU3ViIHBpeGVsIG9wdGltaXplIGZvciBjYW52YXNcbiAqXG4gKiBAcGFyYW0ge251bWJlcn0gcG9zaXRpb24gQ29vcmRpbmF0ZSwgc3VjaCBhcyB4LCB5XG4gKiBAcGFyYW0ge251bWJlcn0gbGluZVdpZHRoIFNob3VsZCBiZSBub25uZWdhdGl2ZSBpbnRlZ2VyLlxuICogQHBhcmFtIHtib29sZWFuPX0gcG9zaXRpdmVPck5lZ2F0aXZlIERlZmF1bHQgZmFsc2UgKG5lZ2F0aXZlKS5cbiAqIEByZXR1cm4ge251bWJlcn0gT3B0aW1pemVkIHBvc2l0aW9uLlxuICovXG5cblxuZnVuY3Rpb24gc3ViUGl4ZWxPcHRpbWl6ZShwb3NpdGlvbiwgbGluZVdpZHRoLCBwb3NpdGl2ZU9yTmVnYXRpdmUpIHtcbiAgLy8gQXNzdXJlIHRoYXQgKHBvc2l0aW9uICsgbGluZVdpZHRoIC8gMikgaXMgbmVhciBpbnRlZ2VyIGVkZ2UsXG4gIC8vIG90aGVyd2lzZSBsaW5lIHdpbGwgYmUgZnV6enkgaW4gY2FudmFzLlxuICB2YXIgZG91YmxlZFBvc2l0aW9uID0gcm91bmQocG9zaXRpb24gKiAyKTtcbiAgcmV0dXJuIChkb3VibGVkUG9zaXRpb24gKyByb3VuZChsaW5lV2lkdGgpKSAlIDIgPT09IDAgPyBkb3VibGVkUG9zaXRpb24gLyAyIDogKGRvdWJsZWRQb3NpdGlvbiArIChwb3NpdGl2ZU9yTmVnYXRpdmUgPyAxIDogLTEpKSAvIDI7XG59XG5cbmZ1bmN0aW9uIGhhc0ZpbGxPclN0cm9rZShmaWxsT3JTdHJva2UpIHtcbiAgcmV0dXJuIGZpbGxPclN0cm9rZSAhPSBudWxsICYmIGZpbGxPclN0cm9rZSAhPT0gJ25vbmUnO1xufSAvLyBNb3N0IGxpZnRlZCBjb2xvciBhcmUgZHVwbGljYXRlZC5cblxuXG52YXIgbGlmdGVkQ29sb3JNYXAgPSB6clV0aWwuY3JlYXRlSGFzaE1hcCgpO1xudmFyIGxpZnRlZENvbG9yQ291bnQgPSAwO1xuXG5mdW5jdGlvbiBsaWZ0Q29sb3IoY29sb3IpIHtcbiAgaWYgKHR5cGVvZiBjb2xvciAhPT0gJ3N0cmluZycpIHtcbiAgICByZXR1cm4gY29sb3I7XG4gIH1cblxuICB2YXIgbGlmdGVkQ29sb3IgPSBsaWZ0ZWRDb2xvck1hcC5nZXQoY29sb3IpO1xuXG4gIGlmICghbGlmdGVkQ29sb3IpIHtcbiAgICBsaWZ0ZWRDb2xvciA9IGNvbG9yVG9vbC5saWZ0KGNvbG9yLCAtMC4xKTtcblxuICAgIGlmIChsaWZ0ZWRDb2xvckNvdW50IDwgMTAwMDApIHtcbiAgICAgIGxpZnRlZENvbG9yTWFwLnNldChjb2xvciwgbGlmdGVkQ29sb3IpO1xuICAgICAgbGlmdGVkQ29sb3JDb3VudCsrO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBsaWZ0ZWRDb2xvcjtcbn1cblxuZnVuY3Rpb24gY2FjaGVFbGVtZW50U3RsKGVsKSB7XG4gIGlmICghZWwuX19ob3ZlclN0bERpcnR5KSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgZWwuX19ob3ZlclN0bERpcnR5ID0gZmFsc2U7XG4gIHZhciBob3ZlclN0eWxlID0gZWwuX19ob3ZlclN0bDtcblxuICBpZiAoIWhvdmVyU3R5bGUpIHtcbiAgICBlbC5fX2NhY2hlZE5vcm1hbFN0bCA9IGVsLl9fY2FjaGVkTm9ybWFsWjIgPSBudWxsO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBub3JtYWxTdHlsZSA9IGVsLl9fY2FjaGVkTm9ybWFsU3RsID0ge307XG4gIGVsLl9fY2FjaGVkTm9ybWFsWjIgPSBlbC56MjtcbiAgdmFyIGVsU3R5bGUgPSBlbC5zdHlsZTtcblxuICBmb3IgKHZhciBuYW1lIGluIGhvdmVyU3R5bGUpIHtcbiAgICAvLyBTZWUgY29tbWVudCBpbiBgZG9TaW5nbGVFbnRlckhvdmVyYC5cbiAgICBpZiAoaG92ZXJTdHlsZVtuYW1lXSAhPSBudWxsKSB7XG4gICAgICBub3JtYWxTdHlsZVtuYW1lXSA9IGVsU3R5bGVbbmFtZV07XG4gICAgfVxuICB9IC8vIEFsd2F5cyBjYWNoZSBmaWxsIGFuZCBzdHJva2UgdG8gbm9ybWFsU3R5bGUgZm9yIGxpZnRpbmcgY29sb3IuXG5cblxuICBub3JtYWxTdHlsZS5maWxsID0gZWxTdHlsZS5maWxsO1xuICBub3JtYWxTdHlsZS5zdHJva2UgPSBlbFN0eWxlLnN0cm9rZTtcbn1cblxuZnVuY3Rpb24gZG9TaW5nbGVFbnRlckhvdmVyKGVsKSB7XG4gIHZhciBob3ZlclN0bCA9IGVsLl9faG92ZXJTdGw7XG5cbiAgaWYgKCFob3ZlclN0bCB8fCBlbC5fX2hpZ2hsaWdodGVkKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHVzZUhvdmVyTGF5ZXIgPSBlbC51c2VIb3ZlckxheWVyO1xuICBlbC5fX2hpZ2hsaWdodGVkID0gdXNlSG92ZXJMYXllciA/ICdsYXllcicgOiAncGxhaW4nO1xuICB2YXIgenIgPSBlbC5fX3pyO1xuXG4gIGlmICghenIgJiYgdXNlSG92ZXJMYXllcikge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBlbFRhcmdldCA9IGVsO1xuICB2YXIgdGFyZ2V0U3R5bGUgPSBlbC5zdHlsZTtcblxuICBpZiAodXNlSG92ZXJMYXllcikge1xuICAgIGVsVGFyZ2V0ID0genIuYWRkSG92ZXIoZWwpO1xuICAgIHRhcmdldFN0eWxlID0gZWxUYXJnZXQuc3R5bGU7XG4gIH1cblxuICByb2xsYmFja0RlZmF1bHRUZXh0U3R5bGUodGFyZ2V0U3R5bGUpO1xuXG4gIGlmICghdXNlSG92ZXJMYXllcikge1xuICAgIGNhY2hlRWxlbWVudFN0bChlbFRhcmdldCk7XG4gIH0gLy8gc3R5bGVzIGNhbiBiZTpcbiAgLy8ge1xuICAvLyAgICBsYWJlbDoge1xuICAvLyAgICAgICAgc2hvdzogZmFsc2UsXG4gIC8vICAgICAgICBwb3NpdGlvbjogJ291dHNpZGUnLFxuICAvLyAgICAgICAgZm9udFNpemU6IDE4XG4gIC8vICAgIH0sXG4gIC8vICAgIGVtcGhhc2lzOiB7XG4gIC8vICAgICAgICBsYWJlbDoge1xuICAvLyAgICAgICAgICAgIHNob3c6IHRydWVcbiAgLy8gICAgICAgIH1cbiAgLy8gICAgfVxuICAvLyB9LFxuICAvLyB3aGVyZSBwcm9wZXJ0aWVzIG9mIGBlbXBoYXNpc2AgbWF5IG5vdCBhcHBlYXIgaW4gYG5vcm1hbGAuIFdlIHByZXZpb3VzbHkgdXNlXG4gIC8vIG1vZHVsZTplY2hhcnRzL3V0aWwvbW9kZWwjZGVmYXVsdEVtcGhhc2lzIHRvIG1lcmdlIGBub3JtYWxgIHRvIGBlbXBoYXNpc2AuXG4gIC8vIEJ1dCBjb25zaWRlciByaWNoIHRleHQgYW5kIHNldE9wdGlvbiBpbiBtZXJnZSBtb2RlLCBpdCBpcyBpbXBvc3NpYmxlIHRvIGNvdmVyXG4gIC8vIGFsbCBwcm9wZXJ0aWVzIGluIG1lcmdlLiBTbyB3ZSB1c2UgbWVyZ2UgbW9kZSB3aGVuIHNldHRpbmcgc3R5bGUgaGVyZSwgd2hlcmVcbiAgLy8gb25seSBwcm9wZXJ0aWVzIHRoYXQgaXMgbm90IGBudWxsL3VuZGVmaW5lZGAgY2FuIGJlIHNldC4gVGhlIGRpc2FkdmVudGFnZTpcbiAgLy8gbnVsbC91bmRlZmluZWQgY2FuIG5vdCBiZSB1c2VkIHRvIHJlbW92ZSBzdHlsZSBhbnkgbW9yZSBpbiBgZW1waGFzaXNgLlxuXG5cbiAgdGFyZ2V0U3R5bGUuZXh0ZW5kRnJvbShob3ZlclN0bCk7XG4gIHNldERlZmF1bHRIb3ZlckZpbGxTdHJva2UodGFyZ2V0U3R5bGUsIGhvdmVyU3RsLCAnZmlsbCcpO1xuICBzZXREZWZhdWx0SG92ZXJGaWxsU3Ryb2tlKHRhcmdldFN0eWxlLCBob3ZlclN0bCwgJ3N0cm9rZScpO1xuICBhcHBseURlZmF1bHRUZXh0U3R5bGUodGFyZ2V0U3R5bGUpO1xuXG4gIGlmICghdXNlSG92ZXJMYXllcikge1xuICAgIGVsLmRpcnR5KGZhbHNlKTtcbiAgICBlbC56MiArPSBaMl9FTVBIQVNJU19MSUZUO1xuICB9XG59XG5cbmZ1bmN0aW9uIHNldERlZmF1bHRIb3ZlckZpbGxTdHJva2UodGFyZ2V0U3R5bGUsIGhvdmVyU3R5bGUsIHByb3ApIHtcbiAgaWYgKCFoYXNGaWxsT3JTdHJva2UoaG92ZXJTdHlsZVtwcm9wXSkgJiYgaGFzRmlsbE9yU3Ryb2tlKHRhcmdldFN0eWxlW3Byb3BdKSkge1xuICAgIHRhcmdldFN0eWxlW3Byb3BdID0gbGlmdENvbG9yKHRhcmdldFN0eWxlW3Byb3BdKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBkb1NpbmdsZUxlYXZlSG92ZXIoZWwpIHtcbiAgdmFyIGhpZ2hsaWdodGVkID0gZWwuX19oaWdobGlnaHRlZDtcblxuICBpZiAoIWhpZ2hsaWdodGVkKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgZWwuX19oaWdobGlnaHRlZCA9IGZhbHNlO1xuXG4gIGlmIChoaWdobGlnaHRlZCA9PT0gJ2xheWVyJykge1xuICAgIGVsLl9fenIgJiYgZWwuX196ci5yZW1vdmVIb3ZlcihlbCk7XG4gIH0gZWxzZSBpZiAoaGlnaGxpZ2h0ZWQpIHtcbiAgICB2YXIgc3R5bGUgPSBlbC5zdHlsZTtcbiAgICB2YXIgbm9ybWFsU3RsID0gZWwuX19jYWNoZWROb3JtYWxTdGw7XG5cbiAgICBpZiAobm9ybWFsU3RsKSB7XG4gICAgICByb2xsYmFja0RlZmF1bHRUZXh0U3R5bGUoc3R5bGUpOyAvLyBDb25zaWRlciBudWxsL3VuZGVmaW5lZCB2YWx1ZSwgc2hvdWxkIHVzZVxuICAgICAgLy8gYHNldFN0eWxlYCBidXQgbm90IGBleHRlbmRGcm9tKHN0bCwgdHJ1ZSlgLlxuXG4gICAgICBlbC5zZXRTdHlsZShub3JtYWxTdGwpO1xuICAgICAgYXBwbHlEZWZhdWx0VGV4dFN0eWxlKHN0eWxlKTtcbiAgICB9IC8vIGBfX2NhY2hlZE5vcm1hbFoyYCB3aWxsIG5vdCBiZSByZXNldCBpZiBjYWxsaW5nIGBzZXRFbGVtZW50SG92ZXJTdHlsZWBcbiAgICAvLyB3aGVuIGBlbGAgaXMgb24gZW1waGFzaXMgc3RhdGUuIFNvIGhlcmUgYnkgY29tcGFyaW5nIHdpdGggMSwgd2UgdHJ5XG4gICAgLy8gaGFyZCB0byBtYWtlIHRoZSBidWcgY2FzZSByYXJlLlxuXG5cbiAgICB2YXIgbm9ybWFsWjIgPSBlbC5fX2NhY2hlZE5vcm1hbFoyO1xuXG4gICAgaWYgKG5vcm1hbFoyICE9IG51bGwgJiYgZWwuejIgLSBub3JtYWxaMiA9PT0gWjJfRU1QSEFTSVNfTElGVCkge1xuICAgICAgZWwuejIgPSBub3JtYWxaMjtcbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gdHJhdmVyc2VDYWxsKGVsLCBtZXRob2QpIHtcbiAgZWwuaXNHcm91cCA/IGVsLnRyYXZlcnNlKGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICFjaGlsZC5pc0dyb3VwICYmIG1ldGhvZChjaGlsZCk7XG4gIH0pIDogbWV0aG9kKGVsKTtcbn1cbi8qKlxuICogU2V0IGhvdmVyIHN0eWxlIChuYW1lbHkgXCJlbXBoYXNpcyBzdHlsZVwiKSBvZiBlbGVtZW50LCBiYXNlZCBvbiB0aGUgY3VycmVudFxuICogc3R5bGUgb2YgdGhlIGdpdmVuIGBlbGAuXG4gKiBUaGlzIG1ldGhvZCBzaG91bGQgYmUgY2FsbGVkIGFmdGVyIGFsbCBvZiB0aGUgbm9ybWFsIHN0eWxlcyBoYXZlIGJlZW4gYWRvcHRlZFxuICogdG8gdGhlIGBlbGAuIFNlZSB0aGUgcmVhc29uIG9uIGBzZXRIb3ZlclN0eWxlYC5cbiAqXG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9IGVsIFNob3VsZCBub3QgYmUgYHpyZW5kZXIvY29udGFpbmVyL0dyb3VwYC5cbiAqIEBwYXJhbSB7T2JqZWN0fGJvb2xlYW59IFtob3ZlclN0bF0gVGhlIHNwZWNpZmllZCBob3ZlciBzdHlsZS5cbiAqICAgICAgICBJZiBzZXQgYXMgYGZhbHNlYCwgZGlzYWJsZSB0aGUgaG92ZXIgc3R5bGUuXG4gKiAgICAgICAgU2ltaWxhcmx5LCBUaGUgYGVsLmhvdmVyU3R5bGVgIGNhbiBhbHNlIGJlIHNldFxuICogICAgICAgIGFzIGBmYWxzZWAgdG8gZGlzYWJsZSB0aGUgaG92ZXIgc3R5bGUuXG4gKiAgICAgICAgT3RoZXJ3aXNlLCB1c2UgdGhlIGRlZmF1bHQgaG92ZXIgc3R5bGUgaWYgbm90IHByb3ZpZGVkLlxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRdXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHQuaG92ZXJTaWxlbnRPblRvdWNoPWZhbHNlXSBTZWUgYGdyYXBoaWMuc2V0QXNIb3ZlclN0eWxlVHJpZ2dlcmBcbiAqL1xuXG5cbmZ1bmN0aW9uIHNldEVsZW1lbnRIb3ZlclN0eWxlKGVsLCBob3ZlclN0bCkge1xuICAvLyBGb3IgcGVyZm9ybWFuY2UgY29uc2lkZXJhdGlvbiwgaXQgbWlnaHQgYmUgYmV0dGVyIHRvIG1ha2UgdGhlIFwiaG92ZXIgc3R5bGVcIiBvbmx5IHRoZVxuICAvLyBkaWZmZXJlbmNlIHByb3BlcnRpZXMgZnJvbSB0aGUgXCJub3JtYWwgc3R5bGVcIiwgYnV0IG5vdCBhIGVudGlyZSBjb3B5IG9mIGFsbCBzdHlsZXMuXG4gIGhvdmVyU3RsID0gZWwuX19ob3ZlclN0bCA9IGhvdmVyU3RsICE9PSBmYWxzZSAmJiAoaG92ZXJTdGwgfHwge30pO1xuICBlbC5fX2hvdmVyU3RsRGlydHkgPSB0cnVlOyAvLyBGSVhNRVxuICAvLyBJdCBpcyBub3QgY29tcGxldGVseSByaWdodCB0byBzYXZlIFwibm9ybWFsXCIvXCJlbXBoYXNpc1wiIGZsYWcgb24gZWxlbWVudHMuXG4gIC8vIEl0IHByb2JhYmx5IHNob3VsZCBiZSBzYXZlZCBvbiBgZGF0YWAgb2Ygc2VyaWVzLiBDb25zaWRlciB0aGUgY2FzZXM6XG4gIC8vICgxKSBBIGhpZ2hsaWdodGVkIGVsZW1lbnRzIGFyZSBtb3ZlZCBvdXQgb2YgdGhlIHZpZXcgcG9ydCBhbmQgcmUtZW50ZXJcbiAgLy8gYWdhaW4gYnkgZGF0YVpvb20uXG4gIC8vICgyKSBjYWxsIGBzZXRPcHRpb25gIGFuZCByZXBsYWNlIGVsZW1lbnRzIHRvdGFsbHkgd2hlbiB0aGV5IGFyZSBoaWdobGlnaHRlZC5cblxuICBpZiAoZWwuX19oaWdobGlnaHRlZCkge1xuICAgIC8vIENvbnNpZGVyIHRoZSBjYXNlOlxuICAgIC8vIFRoZSBzdHlsZXMgb2YgYSBoaWdobGlnaHRlZCBgZWxgIGlzIGJlaW5nIHVwZGF0ZWQuIFRoZSBuZXcgXCJlbXBoYXNpcyBzdHlsZVwiXG4gICAgLy8gc2hvdWxkIGJlIGFkYXB0ZWQgdG8gdGhlIGBlbGAuIE5vdGljZSBoZXJlIG5ldyBcIm5vcm1hbCBzdHlsZXNcIiBzaG91bGQgaGF2ZVxuICAgIC8vIGJlZW4gc2V0IG91dHNpZGUgYW5kIHRoZSBjYWNoZWQgXCJub3JtYWwgc3R5bGVcIiBpcyBvdXQgb2YgZGF0ZS5cbiAgICBlbC5fX2NhY2hlZE5vcm1hbFN0bCA9IG51bGw7IC8vIERvIG5vdCBjbGVhciBgX19jYWNoZWROb3JtYWxaMmAgaGVyZSwgYmVjYXVzZSBzZXR0aW5nIGB6MmAgaXMgbm90IGEgY29uc3RyYWludFxuICAgIC8vIG9mIHRoaXMgbWV0aG9kLiBJbiBtb3N0IGNhc2VzLCBgejJgIGlzIG5vdCBzZXQgYW5kIGhvdmVyIHN0eWxlIHNob3VsZCBiZSBhYmxlXG4gICAgLy8gdG8gcm9sbGJhY2suIE9mIGNvdXJzZSwgdGhhdCB3b3VsZCBicmluZyBidWcsIGJ1dCBvbmx5IGluIGEgcmFyZSBjYXNlLCBzZWVcbiAgICAvLyBgZG9TaW5nbGVMZWF2ZUhvdmVyYCBmb3IgZGV0YWlscy5cblxuICAgIGRvU2luZ2xlTGVhdmVIb3ZlcihlbCk7XG4gICAgZG9TaW5nbGVFbnRlckhvdmVyKGVsKTtcbiAgfVxufVxuLyoqXG4gKiBFbXBoYXNpcyAoY2FsbGVkIGJ5IEFQSSkgaGFzIGhpZ2hlciBwcmlvcml0eSB0aGFuIGBtb3VzZW92ZXJgLlxuICogV2hlbiBlbGVtZW50IGhhcyBiZWVuIGNhbGxlZCB0byBiZSBlbnRlcmVkIGVtcGhhc2lzLCBtb3VzZSBvdmVyXG4gKiBzaG91bGQgbm90IHRyaWdnZXIgdGhlIGhpZ2hsaWdodCBlZmZlY3QgKGZvciBleGFtcGxlLCBhbmltYXRpb25cbiAqIHNjYWxlKSBhZ2FpbiwgYW5kIGBtb3VzZW91dGAgc2hvdWxkIG5vdCBkb3ducGxheSB0aGUgaGlnaGxpZ2h0XG4gKiBlZmZlY3QuIFNvIHRoZSBsaXN0ZW5lciBvZiBgbW91c2VvdmVyYCBhbmQgYG1vdXNlb3V0YCBzaG91bGRcbiAqIGNoZWNrIGBpc0luRW1waGFzaXNgLlxuICpcbiAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvRWxlbWVudH0gZWxcbiAqIEByZXR1cm4ge2Jvb2xlYW59XG4gKi9cblxuXG5mdW5jdGlvbiBpc0luRW1waGFzaXMoZWwpIHtcbiAgcmV0dXJuIGVsICYmIGVsLl9faXNFbXBoYXNpc0VudGVyZWQ7XG59XG5cbmZ1bmN0aW9uIG9uRWxlbWVudE1vdXNlT3ZlcihlKSB7XG4gIGlmICh0aGlzLl9faG92ZXJTaWxlbnRPblRvdWNoICYmIGUuenJCeVRvdWNoKSB7XG4gICAgcmV0dXJuO1xuICB9IC8vIE9ubHkgaWYgZWxlbWVudCBpcyBub3QgaW4gZW1waGFzaXMgc3RhdHVzXG5cblxuICAhdGhpcy5fX2lzRW1waGFzaXNFbnRlcmVkICYmIHRyYXZlcnNlQ2FsbCh0aGlzLCBkb1NpbmdsZUVudGVySG92ZXIpO1xufVxuXG5mdW5jdGlvbiBvbkVsZW1lbnRNb3VzZU91dChlKSB7XG4gIGlmICh0aGlzLl9faG92ZXJTaWxlbnRPblRvdWNoICYmIGUuenJCeVRvdWNoKSB7XG4gICAgcmV0dXJuO1xuICB9IC8vIE9ubHkgaWYgZWxlbWVudCBpcyBub3QgaW4gZW1waGFzaXMgc3RhdHVzXG5cblxuICAhdGhpcy5fX2lzRW1waGFzaXNFbnRlcmVkICYmIHRyYXZlcnNlQ2FsbCh0aGlzLCBkb1NpbmdsZUxlYXZlSG92ZXIpO1xufVxuXG5mdW5jdGlvbiBlbnRlckVtcGhhc2lzKCkge1xuICB0aGlzLl9faXNFbXBoYXNpc0VudGVyZWQgPSB0cnVlO1xuICB0cmF2ZXJzZUNhbGwodGhpcywgZG9TaW5nbGVFbnRlckhvdmVyKTtcbn1cblxuZnVuY3Rpb24gbGVhdmVFbXBoYXNpcygpIHtcbiAgdGhpcy5fX2lzRW1waGFzaXNFbnRlcmVkID0gZmFsc2U7XG4gIHRyYXZlcnNlQ2FsbCh0aGlzLCBkb1NpbmdsZUxlYXZlSG92ZXIpO1xufVxuLyoqXG4gKiBTZXQgaG92ZXIgc3R5bGUgKG5hbWVseSBcImVtcGhhc2lzIHN0eWxlXCIpIG9mIGVsZW1lbnQsXG4gKiBiYXNlZCBvbiB0aGUgY3VycmVudCBzdHlsZSBvZiB0aGUgZ2l2ZW4gYGVsYC5cbiAqXG4gKiAoMSlcbiAqICoqQ09OU1RSQUlOVFMqKiBmb3IgdGhpcyBtZXRob2Q6XG4gKiA8QT4gVGhpcyBtZXRob2QgTVVTVCBiZSBjYWxsZWQgYWZ0ZXIgYWxsIG9mIHRoZSBub3JtYWwgc3R5bGVzIGhhdmluZyBiZWVuIGFkb3B0ZWRcbiAqIHRvIHRoZSBgZWxgLlxuICogPEI+IFRoZSBpbnB1dCBgaG92ZXJTdHlsZWAgKHRoYXQgaXMsIFwiZW1waGFzaXMgc3R5bGVcIikgTVVTVCBiZSB0aGUgc3Vic2V0IG9mIHRoZVxuICogXCJub3JtYWwgc3R5bGVcIiBoYXZpbmcgYmVlbiBzZXQgdG8gdGhlIGVsLlxuICogPEM+IGBjb2xvcmAgTVVTVCBiZSBvbmUgb2YgdGhlIFwibm9ybWFsIHN0eWxlc1wiIChiZWNhdXNlIGNvbG9yIG1pZ2h0IGJlIGxpZnRlZCBhc1xuICogYSBkZWZhdWx0IGhvdmVyIHN0eWxlKS5cbiAqXG4gKiBUaGUgcmVhc29uOiB0aGlzIG1ldGhvZCB0cmVhdCB0aGUgY3VycmVudCBzdHlsZSBvZiB0aGUgYGVsYCBhcyB0aGUgXCJub3JtYWwgc3R5bGVcIlxuICogYW5kIGNhY2hlIHRoZW0gd2hlbiBlbnRlci91cGRhdGUgdGhlIFwiZW1waGFzaXMgc3R5bGVcIi4gQ29uc2lkZXIgdGhlIGNhc2U6IHRoZSBgZWxgXG4gKiBpcyBpbiBcImVtcGhhc2lzXCIgc3RhdGUgYW5kIGBzZXRPcHRpb25gL2BkaXNwYXRjaEFjdGlvbmAgdHJpZ2dlciB0aGUgc3R5bGUgdXBkYXRpbmdcbiAqIGxvZ2ljLCB3aGVyZSB0aGUgZWwgc2hvdWxkIHNoaWZ0IGZyb20gdGhlIG9yaWdpbmFsIGVtcGhhc2lzIHN0eWxlIHRvIHRoZSBuZXdcbiAqIFwiZW1waGFzaXMgc3R5bGVcIiBhbmQgc2hvdWxkIGJlIGFibGUgdG8gXCJkb3ducGxheVwiIGJhY2sgdG8gdGhlIG5ldyBcIm5vcm1hbCBzdHlsZVwiLlxuICpcbiAqIEluZGVlZCwgaXQgaXMgZXJyb3ItcHJvbmUgdG8gbWFrZSBhIGludGVyZmFjZSBoYXMgc28gbWFueSBjb25zdHJhaW50cywgYnV0IEkgaGF2ZVxuICogbm90IGZvdW5kIGEgYmV0dGVyIHNvbHV0aW9uIHlldCB0byBmaXQgdGhlIGJhY2t3YXJkIGNvbXBhdGliaWxpdHksIHBlcmZvcm1hbmNlIGFuZFxuICogdGhlIGN1cnJlbnQgcHJvZ3JhbW1pbmcgc3R5bGUuXG4gKlxuICogKDIpXG4gKiBDYWxsIHRoZSBtZXRob2QgZm9yIGEgXCJyb290XCIgZWxlbWVudCBvbmNlLiBEbyBub3QgY2FsbCBpdCBmb3IgZWFjaCBkZXNjZW5kYW50cy5cbiAqIElmIHRoZSBkZXNjZW5kYW50cyBlbGVtZW5ldHMgb2YgYSBncm91cCBoYXMgaXRzZWxmIGhvdmVyIHN0eWxlIGRpZmZlcmVudCBmcm9tIHRoZVxuICogcm9vdCBncm91cCwgd2UgY2FuIHNpbXBseSBtb3VudCB0aGUgc3R5bGUgb24gYGVsLmhvdmVyU3R5bGVgIGZvciB0aGVtLCBidXQgc2hvdWxkXG4gKiBub3QgY2FsbCB0aGlzIG1ldGhvZCBmb3IgdGhlbS5cbiAqXG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9IGVsXG4gKiBAcGFyYW0ge09iamVjdHxib29sZWFufSBbaG92ZXJTdHlsZV0gU2VlIGBncmFwaGljLnNldEVsZW1lbnRIb3ZlclN0eWxlYC5cbiAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0XVxuICogQHBhcmFtIHtib29sZWFufSBbb3B0LmhvdmVyU2lsZW50T25Ub3VjaD1mYWxzZV0gU2VlIGBncmFwaGljLnNldEFzSG92ZXJTdHlsZVRyaWdnZXJgLlxuICovXG5cblxuZnVuY3Rpb24gc2V0SG92ZXJTdHlsZShlbCwgaG92ZXJTdHlsZSwgb3B0KSB7XG4gIGVsLmlzR3JvdXAgPyBlbC50cmF2ZXJzZShmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAvLyBJZiBlbGVtZW50IGhhcyBzZXBjaWZpZWQgaG92ZXJTdHlsZSwgdGhlbiB1c2UgaXQgaW5zdGVhZCBvZiBnaXZlbiBob3ZlclN0eWxlXG4gICAgLy8gT2Z0ZW4gdXNlZCB3aGVuIGl0ZW0gZ3JvdXAgaGFzIGEgbGFiZWwgZWxlbWVudCBhbmQgaXQncyBob3ZlclN0eWxlIGlzIGRpZmZlcmVudFxuICAgICFjaGlsZC5pc0dyb3VwICYmIHNldEVsZW1lbnRIb3ZlclN0eWxlKGNoaWxkLCBjaGlsZC5ob3ZlclN0eWxlIHx8IGhvdmVyU3R5bGUpO1xuICB9KSA6IHNldEVsZW1lbnRIb3ZlclN0eWxlKGVsLCBlbC5ob3ZlclN0eWxlIHx8IGhvdmVyU3R5bGUpO1xuICBzZXRBc0hvdmVyU3R5bGVUcmlnZ2VyKGVsLCBvcHQpO1xufVxuLyoqXG4gKiBAcGFyYW0ge09iamVjdHxib29sZWFufSBbb3B0XSBJZiBgZmFsc2VgLCBtZWFucyBkaXNhYmxlIHRyaWdnZXIuXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHQuaG92ZXJTaWxlbnRPblRvdWNoPWZhbHNlXVxuICogICAgICAgIEluIHRvdWNoIGRldmljZSwgbW91c2VvdmVyIGV2ZW50IHdpbGwgYmUgdHJpZ2dlciBvbiB0b3VjaHN0YXJ0IGV2ZW50XG4gKiAgICAgICAgKHNlZSBtb2R1bGU6enJlbmRlci9kb20vSGFuZGxlclByb3h5KS4gQnkgdGhpcyBtZWNoYW5pc20sIHdlIGNhblxuICogICAgICAgIGNvbnZlbmllbnRseSB1c2UgaG92ZXJTdHlsZSB3aGVuIHRhcCBvbiB0b3VjaCBzY3JlZW4gd2l0aG91dCBhZGRpdGlvbmFsXG4gKiAgICAgICAgY29kZSBmb3IgY29tcGF0aWJpbGl0eS5cbiAqICAgICAgICBCdXQgaWYgdGhlIGNoYXJ0L2NvbXBvbmVudCBoYXMgc2VsZWN0IGZlYXR1cmUsIHdoaWNoIHVzdWFsbHkgYWxzbyB1c2VcbiAqICAgICAgICBob3ZlclN0eWxlLCB0aGVyZSBtaWdodCBiZSBjb25mbGljdCBiZXR3ZWVuICdzZWxlY3QtaGlnaGxpZ2h0JyBhbmRcbiAqICAgICAgICAnaG92ZXItaGlnaGxpZ2h0JyBlc3BlY2lhbGx5IHdoZW4gcm9hbSBpcyBlbmFibGVkIChzZWUgZ2VvIGZvciBleGFtcGxlKS5cbiAqICAgICAgICBJbiB0aGlzIGNhc2UsIGhvdmVyU2lsZW50T25Ub3VjaCBzaG91bGQgYmUgdXNlZCB0byBkaXNhYmxlIGhvdmVyLWhpZ2hsaWdodFxuICogICAgICAgIG9uIHRvdWNoIGRldmljZS5cbiAqL1xuXG5cbmZ1bmN0aW9uIHNldEFzSG92ZXJTdHlsZVRyaWdnZXIoZWwsIG9wdCkge1xuICB2YXIgZGlzYWJsZSA9IG9wdCA9PT0gZmFsc2U7XG4gIGVsLl9faG92ZXJTaWxlbnRPblRvdWNoID0gb3B0ICE9IG51bGwgJiYgb3B0LmhvdmVyU2lsZW50T25Ub3VjaDsgLy8gU2ltcGxlIG9wdGltaXplLCBzaW5jZSB0aGlzIG1ldGhvZCBtaWdodCBiZVxuICAvLyBjYWxsZWQgZm9yIGVhY2ggZWxlbWVudHMgb2YgYSBncm91cCBpbiBzb21lIGNhc2VzLlxuXG4gIGlmICghZGlzYWJsZSB8fCBlbC5fX2hvdmVyU3R5bGVUcmlnZ2VyKSB7XG4gICAgdmFyIG1ldGhvZCA9IGRpc2FibGUgPyAnb2ZmJyA6ICdvbic7IC8vIER1cGxpY2F0ZWQgZnVuY3Rpb24gd2lsbCBiZSBhdXRvLWlnbm9yZWQsIHNlZSBFdmVudGZ1bC5qcy5cblxuICAgIGVsW21ldGhvZF0oJ21vdXNlb3ZlcicsIG9uRWxlbWVudE1vdXNlT3ZlcilbbWV0aG9kXSgnbW91c2VvdXQnLCBvbkVsZW1lbnRNb3VzZU91dCk7IC8vIEVtcGhhc2lzLCBub3JtYWwgY2FuIGJlIHRyaWdnZXJlZCBtYW51YWxseVxuXG4gICAgZWxbbWV0aG9kXSgnZW1waGFzaXMnLCBlbnRlckVtcGhhc2lzKVttZXRob2RdKCdub3JtYWwnLCBsZWF2ZUVtcGhhc2lzKTtcbiAgICBlbC5fX2hvdmVyU3R5bGVUcmlnZ2VyID0gIWRpc2FibGU7XG4gIH1cbn1cbi8qKlxuICogU2VlIG1vcmUgaW5mbyBpbiBgc2V0VGV4dFN0eWxlQ29tbW9uYC5cbiAqIEBwYXJhbSB7T2JqZWN0fG1vZHVsZTp6cmVuZGVyL2dyYXBoaWMvU3R5bGV9IG5vcm1hbFN0eWxlXG4gKiBAcGFyYW0ge09iamVjdH0gZW1waGFzaXNTdHlsZVxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gbm9ybWFsTW9kZWxcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWx9IGVtcGhhc2lzTW9kZWxcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHQgQ2hlY2sgYG9wdGAgb2YgYHNldFRleHRTdHlsZUNvbW1vbmAgdG8gZmluZCBvdGhlciBwcm9wcy5cbiAqIEBwYXJhbSB7c3RyaW5nfEZ1bmN0aW9ufSBbb3B0LmRlZmF1bHRUZXh0XVxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gW29wdC5sYWJlbEZldGNoZXJdIEZldGNoIHRleHQgYnlcbiAqICAgICAgYG9wdC5sYWJlbEZldGNoZXIuZ2V0Rm9ybWF0dGVkTGFiZWwob3B0LmxhYmVsRGF0YUluZGV4LCAnbm9ybWFsJy8nZW1waGFzaXMnLCBudWxsLCBvcHQubGFiZWxEaW1JbmRleClgXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBbb3B0LmxhYmVsRGF0YUluZGV4XSBGZXRjaCB0ZXh0IGJ5XG4gKiAgICAgIGBvcHQudGV4dEZldGNoZXIuZ2V0Rm9ybWF0dGVkTGFiZWwob3B0LmxhYmVsRGF0YUluZGV4LCAnbm9ybWFsJy8nZW1waGFzaXMnLCBudWxsLCBvcHQubGFiZWxEaW1JbmRleClgXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBbb3B0LmxhYmVsRGltSW5kZXhdIEZldGNoIHRleHQgYnlcbiAqICAgICAgYG9wdC50ZXh0RmV0Y2hlci5nZXRGb3JtYXR0ZWRMYWJlbChvcHQubGFiZWxEYXRhSW5kZXgsICdub3JtYWwnLydlbXBoYXNpcycsIG51bGwsIG9wdC5sYWJlbERpbUluZGV4KWBcbiAqIEBwYXJhbSB7T2JqZWN0fSBbbm9ybWFsU3BlY2lmaWVkXVxuICogQHBhcmFtIHtPYmplY3R9IFtlbXBoYXNpc1NwZWNpZmllZF1cbiAqL1xuXG5cbmZ1bmN0aW9uIHNldExhYmVsU3R5bGUobm9ybWFsU3R5bGUsIGVtcGhhc2lzU3R5bGUsIG5vcm1hbE1vZGVsLCBlbXBoYXNpc01vZGVsLCBvcHQsIG5vcm1hbFNwZWNpZmllZCwgZW1waGFzaXNTcGVjaWZpZWQpIHtcbiAgb3B0ID0gb3B0IHx8IEVNUFRZX09CSjtcbiAgdmFyIGxhYmVsRmV0Y2hlciA9IG9wdC5sYWJlbEZldGNoZXI7XG4gIHZhciBsYWJlbERhdGFJbmRleCA9IG9wdC5sYWJlbERhdGFJbmRleDtcbiAgdmFyIGxhYmVsRGltSW5kZXggPSBvcHQubGFiZWxEaW1JbmRleDsgLy8gVGhpcyBzY2VuYXJpbywgYGxhYmVsLm5vcm1hbC5zaG93ID0gdHJ1ZTsgbGFiZWwuZW1waGFzaXMuc2hvdyA9IGZhbHNlYCxcbiAgLy8gaXMgbm90IHN1cHBvcnRlZCB1dGlsIHNvbWVvbmUgcmVxdWVzdHMuXG5cbiAgdmFyIHNob3dOb3JtYWwgPSBub3JtYWxNb2RlbC5nZXRTaGFsbG93KCdzaG93Jyk7XG4gIHZhciBzaG93RW1waGFzaXMgPSBlbXBoYXNpc01vZGVsLmdldFNoYWxsb3coJ3Nob3cnKTsgLy8gQ29uc2lkZXIgcGVyZm9ybWFuY2UsIG9ubHkgZmV0Y2ggbGFiZWwgd2hlbiBuZWNlc3NhcnkuXG4gIC8vIElmIGBub3JtYWwuc2hvd2AgaXMgYGZhbHNlYCBhbmQgYGVtcGhhc2lzLnNob3dgIGlzIGB0cnVlYCBhbmQgYGVtcGhhc2lzLmZvcm1hdHRlcmAgaXMgbm90IHNldCxcbiAgLy8gbGFiZWwgc2hvdWxkIGJlIGRpc3BsYXllZCwgd2hlcmUgdGV4dCBpcyBmZXRjaGVkIGJ5IGBub3JtYWwuZm9ybWF0dGVyYCBvciBgb3B0LmRlZmF1bHRUZXh0YC5cblxuICB2YXIgYmFzZVRleHQ7XG5cbiAgaWYgKHNob3dOb3JtYWwgfHwgc2hvd0VtcGhhc2lzKSB7XG4gICAgaWYgKGxhYmVsRmV0Y2hlcikge1xuICAgICAgYmFzZVRleHQgPSBsYWJlbEZldGNoZXIuZ2V0Rm9ybWF0dGVkTGFiZWwobGFiZWxEYXRhSW5kZXgsICdub3JtYWwnLCBudWxsLCBsYWJlbERpbUluZGV4KTtcbiAgICB9XG5cbiAgICBpZiAoYmFzZVRleHQgPT0gbnVsbCkge1xuICAgICAgYmFzZVRleHQgPSB6clV0aWwuaXNGdW5jdGlvbihvcHQuZGVmYXVsdFRleHQpID8gb3B0LmRlZmF1bHRUZXh0KGxhYmVsRGF0YUluZGV4LCBvcHQpIDogb3B0LmRlZmF1bHRUZXh0O1xuICAgIH1cbiAgfVxuXG4gIHZhciBub3JtYWxTdHlsZVRleHQgPSBzaG93Tm9ybWFsID8gYmFzZVRleHQgOiBudWxsO1xuICB2YXIgZW1waGFzaXNTdHlsZVRleHQgPSBzaG93RW1waGFzaXMgPyB6clV0aWwucmV0cmlldmUyKGxhYmVsRmV0Y2hlciA/IGxhYmVsRmV0Y2hlci5nZXRGb3JtYXR0ZWRMYWJlbChsYWJlbERhdGFJbmRleCwgJ2VtcGhhc2lzJywgbnVsbCwgbGFiZWxEaW1JbmRleCkgOiBudWxsLCBiYXNlVGV4dCkgOiBudWxsOyAvLyBPcHRpbWl6ZTogSWYgc3R5bGUudGV4dCBpcyBudWxsLCB0ZXh0IHdpbGwgbm90IGJlIGRyYXduLlxuXG4gIGlmIChub3JtYWxTdHlsZVRleHQgIT0gbnVsbCB8fCBlbXBoYXNpc1N0eWxlVGV4dCAhPSBudWxsKSB7XG4gICAgLy8gQWx3YXlzIHNldCBgdGV4dFN0eWxlYCBldmVuIGlmIGBub3JtYWxTdHlsZS50ZXh0YCBpcyBudWxsLCBiZWNhdXNlIGRlZmF1bHRcbiAgICAvLyB2YWx1ZXMgaGF2ZSB0byBiZSBzZXQgb24gYG5vcm1hbFN0eWxlYC5cbiAgICAvLyBJZiB3ZSBzZXQgZGVmYXVsdCB2YWx1ZXMgb24gYGVtcGhhc2lzU3R5bGVgLCBjb25zaWRlciBjYXNlOlxuICAgIC8vIEZpcnN0bHksIGBzZXRPcHRpb24oLi4uIGxhYmVsOiB7bm9ybWFsOiB7dGV4dDogbnVsbH0sIGVtcGhhc2lzOiB7c2hvdzogdHJ1ZX19IC4uLik7YFxuICAgIC8vIFNlY29uZGx5LCBgc2V0T3B0aW9uKC4uLiBsYWJlbDoge25vcmFtbDoge3Nob3c6IHRydWUsIHRleHQ6ICdhYmMnLCBjb2xvcjogJ3JlZCd9IC4uLik7YFxuICAgIC8vIFRoZW4gdGhlICdyZWQnIHdpbGwgbm90IHdvcmsgb24gZW1waGFzaXMuXG4gICAgc2V0VGV4dFN0eWxlKG5vcm1hbFN0eWxlLCBub3JtYWxNb2RlbCwgbm9ybWFsU3BlY2lmaWVkLCBvcHQpO1xuICAgIHNldFRleHRTdHlsZShlbXBoYXNpc1N0eWxlLCBlbXBoYXNpc01vZGVsLCBlbXBoYXNpc1NwZWNpZmllZCwgb3B0LCB0cnVlKTtcbiAgfVxuXG4gIG5vcm1hbFN0eWxlLnRleHQgPSBub3JtYWxTdHlsZVRleHQ7XG4gIGVtcGhhc2lzU3R5bGUudGV4dCA9IGVtcGhhc2lzU3R5bGVUZXh0O1xufVxuLyoqXG4gKiBTZXQgYmFzaWMgdGV4dFN0eWxlIHByb3BlcnRpZXMuXG4gKiBTZWUgbW9yZSBpbmZvIGluIGBzZXRUZXh0U3R5bGVDb21tb25gLlxuICogQHBhcmFtIHtPYmplY3R8bW9kdWxlOnpyZW5kZXIvZ3JhcGhpYy9TdHlsZX0gdGV4dFN0eWxlXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBtb2RlbFxuICogQHBhcmFtIHtPYmplY3R9IFtzcGVjaWZpZWRUZXh0U3R5bGVdIENhbiBiZSBvdmVycmlkZWQgYnkgc2V0dGluZ3MgaW4gbW9kZWwuXG4gKiBAcGFyYW0ge09iamVjdH0gW29wdF0gU2VlIGBvcHRgIG9mIGBzZXRUZXh0U3R5bGVDb21tb25gLlxuICogQHBhcmFtIHtib29sZWFufSBbaXNFbXBoYXNpc11cbiAqL1xuXG5cbmZ1bmN0aW9uIHNldFRleHRTdHlsZSh0ZXh0U3R5bGUsIHRleHRTdHlsZU1vZGVsLCBzcGVjaWZpZWRUZXh0U3R5bGUsIG9wdCwgaXNFbXBoYXNpcykge1xuICBzZXRUZXh0U3R5bGVDb21tb24odGV4dFN0eWxlLCB0ZXh0U3R5bGVNb2RlbCwgb3B0LCBpc0VtcGhhc2lzKTtcbiAgc3BlY2lmaWVkVGV4dFN0eWxlICYmIHpyVXRpbC5leHRlbmQodGV4dFN0eWxlLCBzcGVjaWZpZWRUZXh0U3R5bGUpOyAvLyB0ZXh0U3R5bGUuaG9zdCAmJiB0ZXh0U3R5bGUuaG9zdC5kaXJ0eSAmJiB0ZXh0U3R5bGUuaG9zdC5kaXJ0eShmYWxzZSk7XG5cbiAgcmV0dXJuIHRleHRTdHlsZTtcbn1cbi8qKlxuICogU2V0IHRleHQgb3B0aW9uIGluIHRoZSBzdHlsZS5cbiAqIFNlZSBtb3JlIGluZm8gaW4gYHNldFRleHRTdHlsZUNvbW1vbmAuXG4gKiBAZGVwcmVjYXRlZFxuICogQHBhcmFtIHtPYmplY3R9IHRleHRTdHlsZVxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gbGFiZWxNb2RlbFxuICogQHBhcmFtIHtzdHJpbmd8Ym9vbGVhbn0gZGVmYXVsdENvbG9yIERlZmF1bHQgdGV4dCBjb2xvci5cbiAqICAgICAgICBJZiBzZXQgYXMgZmFsc2UsIGl0IHdpbGwgYmUgcHJvY2Vzc2VkIGFzIGEgZW1waGFzaXMgc3R5bGUuXG4gKi9cblxuXG5mdW5jdGlvbiBzZXRUZXh0KHRleHRTdHlsZSwgbGFiZWxNb2RlbCwgZGVmYXVsdENvbG9yKSB7XG4gIHZhciBvcHQgPSB7XG4gICAgaXNSZWN0VGV4dDogdHJ1ZVxuICB9O1xuICB2YXIgaXNFbXBoYXNpcztcblxuICBpZiAoZGVmYXVsdENvbG9yID09PSBmYWxzZSkge1xuICAgIGlzRW1waGFzaXMgPSB0cnVlO1xuICB9IGVsc2Uge1xuICAgIC8vIFN1cHBvcnQgc2V0dGluZyBjb2xvciBhcyAnYXV0bycgdG8gZ2V0IHZpc3VhbCBjb2xvci5cbiAgICBvcHQuYXV0b0NvbG9yID0gZGVmYXVsdENvbG9yO1xuICB9XG5cbiAgc2V0VGV4dFN0eWxlQ29tbW9uKHRleHRTdHlsZSwgbGFiZWxNb2RlbCwgb3B0LCBpc0VtcGhhc2lzKTsgLy8gdGV4dFN0eWxlLmhvc3QgJiYgdGV4dFN0eWxlLmhvc3QuZGlydHkgJiYgdGV4dFN0eWxlLmhvc3QuZGlydHkoZmFsc2UpO1xufVxuLyoqXG4gKiBUaGUgdW5pZm9ybSBlbnRyeSBvZiBzZXQgdGV4dCBzdHlsZSwgdGhhdCBpcywgcmV0cmlldmUgc3R5bGUgZGVmaW5pdGlvbnNcbiAqIGZyb20gYG1vZGVsYCBhbmQgc2V0IHRvIGB0ZXh0U3R5bGVgIG9iamVjdC5cbiAqXG4gKiBOZXZlciBpbiBtZXJnZSBtb2RlLCBidXQgaW4gb3ZlcndyaXRlIG1vZGUsIHRoYXQgaXMsIGFsbCBvZiB0aGUgdGV4dCBzdHlsZVxuICogcHJvcGVydGllcyB3aWxsIGJlIHNldC4gKENvbnNpZGVyIHRoZSBzdGF0ZXMgb2Ygbm9ybWFsIGFuZCBlbXBoYXNpcyBhbmRcbiAqIGRlZmF1bHQgdmFsdWUgY2FuIGJlIGFkb3B0ZWQsIG1lcmdlIHdvdWxkIG1ha2UgdGhlIGxvZ2ljIHRvbyBjb21wbGljYXRlZFxuICogdG8gbWFuYWdlLilcbiAqXG4gKiBUaGUgYHRleHRTdHlsZWAgb2JqZWN0IGNhbiBlaXRoZXIgYmUgYSBwbGFpbiBvYmplY3Qgb3IgYW4gaW5zdGFuY2Ugb2ZcbiAqIGB6cmVuZGVyL3NyYy9ncmFwaGljL1N0eWxlYCwgYW5kIGVpdGhlciBiZSB0aGUgc3R5bGUgb2Ygbm9ybWFsIG9yIGVtcGhhc2lzLlxuICogQWZ0ZXIgdGhpcyBtb3Rob2QgY2FsbGVkLCB0aGUgYHRleHRTdHlsZWAgb2JqZWN0IGNhbiB0aGVuIGJlIHVzZWQgaW5cbiAqIGBlbC5zZXRTdHlsZSh0ZXh0U3R5bGUpYCBvciBgZWwuaG92ZXJTdHlsZSA9IHRleHRTdHlsZWAuXG4gKlxuICogRGVmYXVsdCB2YWx1ZSB3aWxsIGJlIGFkb3B0ZWQgYW5kIGBpbnNpZGVSb2xsYmFja09wdGAgd2lsbCBiZSBjcmVhdGVkLlxuICogU2VlIGBhcHBseURlZmF1bHRUZXh0U3R5bGVgIGByb2xsYmFja0RlZmF1bHRUZXh0U3R5bGVgIGZvciBtb3JlIGRldGFpbHMuXG4gKlxuICogb3B0OiB7XG4gKiAgICAgIGRpc2FibGVCb3g6IGJvb2xlYW4sIFdoZXRoZXIgZGlhYmxlIGRyYXdpbmcgYm94IG9mIGJsb2NrIChvdXRlciBtb3N0KS5cbiAqICAgICAgaXNSZWN0VGV4dDogYm9vbGVhbixcbiAqICAgICAgYXV0b0NvbG9yOiBzdHJpbmcsIHNwZWNpZnkgYSBjb2xvciB3aGVuIGNvbG9yIGlzICdhdXRvJyxcbiAqICAgICAgICAgICAgICBmb3IgdGV4dEZpbGwsIHRleHRTdHJva2UsIHRleHRCYWNrZ3JvdW5kQ29sb3IsIGFuZCB0ZXh0Qm9yZGVyQ29sb3IuXG4gKiAgICAgICAgICAgICAgSWYgYXV0b0NvbG9yIHNwZWNpZmllZCwgaXQgaXMgdXNlZCBhcyBkZWZhdWx0IHRleHRGaWxsLlxuICogICAgICB1c2VJbnNpZGVTdHlsZTpcbiAqICAgICAgICAgICAgICBgdHJ1ZWA6IFVzZSBpbnNpZGUgc3R5bGUgKHRleHRGaWxsLCB0ZXh0U3Ryb2tlLCB0ZXh0U3Ryb2tlV2lkdGgpXG4gKiAgICAgICAgICAgICAgICAgIGlmIGB0ZXh0RmlsbGAgaXMgbm90IHNwZWNpZmllZC5cbiAqICAgICAgICAgICAgICBgZmFsc2VgOiBEbyBub3QgdXNlIGluc2lkZSBzdHlsZS5cbiAqICAgICAgICAgICAgICBgbnVsbC91bmRlZmluZWRgOiB1c2UgaW5zaWRlIHN0eWxlIGlmIGBpc1JlY3RUZXh0YCBpcyB0cnVlIGFuZFxuICogICAgICAgICAgICAgICAgICBgdGV4dEZpbGxgIGlzIG5vdCBzcGVjaWZpZWQgYW5kIHRleHRQb3NpdGlvbiBjb250YWlucyBgJ2luc2lkZSdgLlxuICogICAgICBmb3JjZVJpY2g6IGJvb2xlYW5cbiAqIH1cbiAqL1xuXG5cbmZ1bmN0aW9uIHNldFRleHRTdHlsZUNvbW1vbih0ZXh0U3R5bGUsIHRleHRTdHlsZU1vZGVsLCBvcHQsIGlzRW1waGFzaXMpIHtcbiAgLy8gQ29uc2lkZXIgdGhlcmUgd2lsbCBiZSBhYm5vcm1hbCB3aGVuIG1lcmdlIGhvdmVyIHN0eWxlIHRvIG5vcm1hbCBzdHlsZSBpZiBnaXZlbiBkZWZhdWx0IHZhbHVlLlxuICBvcHQgPSBvcHQgfHwgRU1QVFlfT0JKO1xuXG4gIGlmIChvcHQuaXNSZWN0VGV4dCkge1xuICAgIHZhciB0ZXh0UG9zaXRpb24gPSB0ZXh0U3R5bGVNb2RlbC5nZXRTaGFsbG93KCdwb3NpdGlvbicpIHx8IChpc0VtcGhhc2lzID8gbnVsbCA6ICdpbnNpZGUnKTsgLy8gJ291dHNpZGUnIGlzIG5vdCBhIHZhbGlkIHpyIHRleHRQb3N0aW9uIHZhbHVlLCBidXQgdXNlZFxuICAgIC8vIGluIGJhciBzZXJpZXMsIGFuZCBtYWdyaWMgdHlwZSBzaG91bGQgYmUgY29uc2lkZXJlZC5cblxuICAgIHRleHRQb3NpdGlvbiA9PT0gJ291dHNpZGUnICYmICh0ZXh0UG9zaXRpb24gPSAndG9wJyk7XG4gICAgdGV4dFN0eWxlLnRleHRQb3NpdGlvbiA9IHRleHRQb3NpdGlvbjtcbiAgICB0ZXh0U3R5bGUudGV4dE9mZnNldCA9IHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ29mZnNldCcpO1xuICAgIHZhciBsYWJlbFJvdGF0ZSA9IHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ3JvdGF0ZScpO1xuICAgIGxhYmVsUm90YXRlICE9IG51bGwgJiYgKGxhYmVsUm90YXRlICo9IE1hdGguUEkgLyAxODApO1xuICAgIHRleHRTdHlsZS50ZXh0Um90YXRpb24gPSBsYWJlbFJvdGF0ZTtcbiAgICB0ZXh0U3R5bGUudGV4dERpc3RhbmNlID0genJVdGlsLnJldHJpZXZlMih0ZXh0U3R5bGVNb2RlbC5nZXRTaGFsbG93KCdkaXN0YW5jZScpLCBpc0VtcGhhc2lzID8gbnVsbCA6IDUpO1xuICB9XG5cbiAgdmFyIGVjTW9kZWwgPSB0ZXh0U3R5bGVNb2RlbC5lY01vZGVsO1xuICB2YXIgZ2xvYmFsVGV4dFN0eWxlID0gZWNNb2RlbCAmJiBlY01vZGVsLm9wdGlvbi50ZXh0U3R5bGU7IC8vIENvbnNpZGVyIGNhc2U6XG4gIC8vIHtcbiAgLy8gICAgIGRhdGE6IFt7XG4gIC8vICAgICAgICAgdmFsdWU6IDEyLFxuICAvLyAgICAgICAgIGxhYmVsOiB7XG4gIC8vICAgICAgICAgICAgIHJpY2g6IHtcbiAgLy8gICAgICAgICAgICAgICAgIC8vIG5vICdhJyBoZXJlIGJ1dCB1c2luZyBwYXJlbnQgJ2EnLlxuICAvLyAgICAgICAgICAgICB9XG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgfV0sXG4gIC8vICAgICByaWNoOiB7XG4gIC8vICAgICAgICAgYTogeyAuLi4gfVxuICAvLyAgICAgfVxuICAvLyB9XG5cbiAgdmFyIHJpY2hJdGVtTmFtZXMgPSBnZXRSaWNoSXRlbU5hbWVzKHRleHRTdHlsZU1vZGVsKTtcbiAgdmFyIHJpY2hSZXN1bHQ7XG5cbiAgaWYgKHJpY2hJdGVtTmFtZXMpIHtcbiAgICByaWNoUmVzdWx0ID0ge307XG5cbiAgICBmb3IgKHZhciBuYW1lIGluIHJpY2hJdGVtTmFtZXMpIHtcbiAgICAgIGlmIChyaWNoSXRlbU5hbWVzLmhhc093blByb3BlcnR5KG5hbWUpKSB7XG4gICAgICAgIC8vIENhc2NhZGUgaXMgc3VwcG9ydGVkIGluIHJpY2guXG4gICAgICAgIHZhciByaWNoVGV4dFN0eWxlID0gdGV4dFN0eWxlTW9kZWwuZ2V0TW9kZWwoWydyaWNoJywgbmFtZV0pOyAvLyBJbiByaWNoLCBuZXZlciBgZGlzYWJsZUJveGAuXG5cbiAgICAgICAgc2V0VG9rZW5UZXh0U3R5bGUocmljaFJlc3VsdFtuYW1lXSA9IHt9LCByaWNoVGV4dFN0eWxlLCBnbG9iYWxUZXh0U3R5bGUsIG9wdCwgaXNFbXBoYXNpcyk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgdGV4dFN0eWxlLnJpY2ggPSByaWNoUmVzdWx0O1xuICBzZXRUb2tlblRleHRTdHlsZSh0ZXh0U3R5bGUsIHRleHRTdHlsZU1vZGVsLCBnbG9iYWxUZXh0U3R5bGUsIG9wdCwgaXNFbXBoYXNpcywgdHJ1ZSk7XG5cbiAgaWYgKG9wdC5mb3JjZVJpY2ggJiYgIW9wdC50ZXh0U3R5bGUpIHtcbiAgICBvcHQudGV4dFN0eWxlID0ge307XG4gIH1cblxuICByZXR1cm4gdGV4dFN0eWxlO1xufSAvLyBDb25zaWRlciBjYXNlOlxuLy8ge1xuLy8gICAgIGRhdGE6IFt7XG4vLyAgICAgICAgIHZhbHVlOiAxMixcbi8vICAgICAgICAgbGFiZWw6IHtcbi8vICAgICAgICAgICAgIHJpY2g6IHtcbi8vICAgICAgICAgICAgICAgICAvLyBubyAnYScgaGVyZSBidXQgdXNpbmcgcGFyZW50ICdhJy5cbi8vICAgICAgICAgICAgIH1cbi8vICAgICAgICAgfVxuLy8gICAgIH1dLFxuLy8gICAgIHJpY2g6IHtcbi8vICAgICAgICAgYTogeyAuLi4gfVxuLy8gICAgIH1cbi8vIH1cblxuXG5mdW5jdGlvbiBnZXRSaWNoSXRlbU5hbWVzKHRleHRTdHlsZU1vZGVsKSB7XG4gIC8vIFVzZSBvYmplY3QgdG8gcmVtb3ZlIGR1cGxpY2F0ZWQgbmFtZXMuXG4gIHZhciByaWNoSXRlbU5hbWVNYXA7XG5cbiAgd2hpbGUgKHRleHRTdHlsZU1vZGVsICYmIHRleHRTdHlsZU1vZGVsICE9PSB0ZXh0U3R5bGVNb2RlbC5lY01vZGVsKSB7XG4gICAgdmFyIHJpY2ggPSAodGV4dFN0eWxlTW9kZWwub3B0aW9uIHx8IEVNUFRZX09CSikucmljaDtcblxuICAgIGlmIChyaWNoKSB7XG4gICAgICByaWNoSXRlbU5hbWVNYXAgPSByaWNoSXRlbU5hbWVNYXAgfHwge307XG5cbiAgICAgIGZvciAodmFyIG5hbWUgaW4gcmljaCkge1xuICAgICAgICBpZiAocmljaC5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xuICAgICAgICAgIHJpY2hJdGVtTmFtZU1hcFtuYW1lXSA9IDE7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICB0ZXh0U3R5bGVNb2RlbCA9IHRleHRTdHlsZU1vZGVsLnBhcmVudE1vZGVsO1xuICB9XG5cbiAgcmV0dXJuIHJpY2hJdGVtTmFtZU1hcDtcbn1cblxuZnVuY3Rpb24gc2V0VG9rZW5UZXh0U3R5bGUodGV4dFN0eWxlLCB0ZXh0U3R5bGVNb2RlbCwgZ2xvYmFsVGV4dFN0eWxlLCBvcHQsIGlzRW1waGFzaXMsIGlzQmxvY2spIHtcbiAgLy8gSW4gbWVyZ2UgbW9kZSwgZGVmYXVsdCB2YWx1ZSBzaG91bGQgbm90IGJlIGdpdmVuLlxuICBnbG9iYWxUZXh0U3R5bGUgPSAhaXNFbXBoYXNpcyAmJiBnbG9iYWxUZXh0U3R5bGUgfHwgRU1QVFlfT0JKO1xuICB0ZXh0U3R5bGUudGV4dEZpbGwgPSBnZXRBdXRvQ29sb3IodGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygnY29sb3InKSwgb3B0KSB8fCBnbG9iYWxUZXh0U3R5bGUuY29sb3I7XG4gIHRleHRTdHlsZS50ZXh0U3Ryb2tlID0gZ2V0QXV0b0NvbG9yKHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ3RleHRCb3JkZXJDb2xvcicpLCBvcHQpIHx8IGdsb2JhbFRleHRTdHlsZS50ZXh0Qm9yZGVyQ29sb3I7XG4gIHRleHRTdHlsZS50ZXh0U3Ryb2tlV2lkdGggPSB6clV0aWwucmV0cmlldmUyKHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ3RleHRCb3JkZXJXaWR0aCcpLCBnbG9iYWxUZXh0U3R5bGUudGV4dEJvcmRlcldpZHRoKTsgLy8gU2F2ZSBvcmlnaW5hbCB0ZXh0UG9zaXRpb24sIGJlY2F1c2Ugc3R5bGUudGV4dFBvc2l0aW9uIHdpbGwgYmUgcmVwYWxjZWQgYnlcbiAgLy8gcmVhbCBsb2NhdGlvbiAobGlrZSBbMTAsIDMwXSkgaW4genJlbmRlci5cblxuICB0ZXh0U3R5bGUuaW5zaWRlUmF3VGV4dFBvc2l0aW9uID0gdGV4dFN0eWxlLnRleHRQb3NpdGlvbjtcblxuICBpZiAoIWlzRW1waGFzaXMpIHtcbiAgICBpZiAoaXNCbG9jaykge1xuICAgICAgdGV4dFN0eWxlLmluc2lkZVJvbGxiYWNrT3B0ID0gb3B0O1xuICAgICAgYXBwbHlEZWZhdWx0VGV4dFN0eWxlKHRleHRTdHlsZSk7XG4gICAgfSAvLyBTZXQgZGVmYXVsdCBmaW5hbGx5LlxuXG5cbiAgICBpZiAodGV4dFN0eWxlLnRleHRGaWxsID09IG51bGwpIHtcbiAgICAgIHRleHRTdHlsZS50ZXh0RmlsbCA9IG9wdC5hdXRvQ29sb3I7XG4gICAgfVxuICB9IC8vIERvIG5vdCB1c2UgYGdldEZvbnRgIGhlcmUsIGJlY2F1c2UgbWVyZ2Ugc2hvdWxkIGJlIHN1cHBvcnRlZCwgd2hlcmVcbiAgLy8gcGFydCBvZiB0aGVzZSBwcm9wZXJ0aWVzIG1heSBiZSBjaGFuZ2VkIGluIGVtcGhhc2lzIHN0eWxlLCBhbmQgdGhlXG4gIC8vIG90aGVycyBzaG91bGQgcmVtYWluIHRoZWlyIG9yaWdpbmFsIHZhbHVlIGdvdCBmcm9tIG5vcm1hbCBzdHlsZS5cblxuXG4gIHRleHRTdHlsZS5mb250U3R5bGUgPSB0ZXh0U3R5bGVNb2RlbC5nZXRTaGFsbG93KCdmb250U3R5bGUnKSB8fCBnbG9iYWxUZXh0U3R5bGUuZm9udFN0eWxlO1xuICB0ZXh0U3R5bGUuZm9udFdlaWdodCA9IHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ2ZvbnRXZWlnaHQnKSB8fCBnbG9iYWxUZXh0U3R5bGUuZm9udFdlaWdodDtcbiAgdGV4dFN0eWxlLmZvbnRTaXplID0gdGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygnZm9udFNpemUnKSB8fCBnbG9iYWxUZXh0U3R5bGUuZm9udFNpemU7XG4gIHRleHRTdHlsZS5mb250RmFtaWx5ID0gdGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygnZm9udEZhbWlseScpIHx8IGdsb2JhbFRleHRTdHlsZS5mb250RmFtaWx5O1xuICB0ZXh0U3R5bGUudGV4dEFsaWduID0gdGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygnYWxpZ24nKTtcbiAgdGV4dFN0eWxlLnRleHRWZXJ0aWNhbEFsaWduID0gdGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygndmVydGljYWxBbGlnbicpIHx8IHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ2Jhc2VsaW5lJyk7XG4gIHRleHRTdHlsZS50ZXh0TGluZUhlaWdodCA9IHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ2xpbmVIZWlnaHQnKTtcbiAgdGV4dFN0eWxlLnRleHRXaWR0aCA9IHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ3dpZHRoJyk7XG4gIHRleHRTdHlsZS50ZXh0SGVpZ2h0ID0gdGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygnaGVpZ2h0Jyk7XG4gIHRleHRTdHlsZS50ZXh0VGFnID0gdGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygndGFnJyk7XG5cbiAgaWYgKCFpc0Jsb2NrIHx8ICFvcHQuZGlzYWJsZUJveCkge1xuICAgIHRleHRTdHlsZS50ZXh0QmFja2dyb3VuZENvbG9yID0gZ2V0QXV0b0NvbG9yKHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ2JhY2tncm91bmRDb2xvcicpLCBvcHQpO1xuICAgIHRleHRTdHlsZS50ZXh0UGFkZGluZyA9IHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ3BhZGRpbmcnKTtcbiAgICB0ZXh0U3R5bGUudGV4dEJvcmRlckNvbG9yID0gZ2V0QXV0b0NvbG9yKHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ2JvcmRlckNvbG9yJyksIG9wdCk7XG4gICAgdGV4dFN0eWxlLnRleHRCb3JkZXJXaWR0aCA9IHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ2JvcmRlcldpZHRoJyk7XG4gICAgdGV4dFN0eWxlLnRleHRCb3JkZXJSYWRpdXMgPSB0ZXh0U3R5bGVNb2RlbC5nZXRTaGFsbG93KCdib3JkZXJSYWRpdXMnKTtcbiAgICB0ZXh0U3R5bGUudGV4dEJveFNoYWRvd0NvbG9yID0gdGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygnc2hhZG93Q29sb3InKTtcbiAgICB0ZXh0U3R5bGUudGV4dEJveFNoYWRvd0JsdXIgPSB0ZXh0U3R5bGVNb2RlbC5nZXRTaGFsbG93KCdzaGFkb3dCbHVyJyk7XG4gICAgdGV4dFN0eWxlLnRleHRCb3hTaGFkb3dPZmZzZXRYID0gdGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygnc2hhZG93T2Zmc2V0WCcpO1xuICAgIHRleHRTdHlsZS50ZXh0Qm94U2hhZG93T2Zmc2V0WSA9IHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ3NoYWRvd09mZnNldFknKTtcbiAgfVxuXG4gIHRleHRTdHlsZS50ZXh0U2hhZG93Q29sb3IgPSB0ZXh0U3R5bGVNb2RlbC5nZXRTaGFsbG93KCd0ZXh0U2hhZG93Q29sb3InKSB8fCBnbG9iYWxUZXh0U3R5bGUudGV4dFNoYWRvd0NvbG9yO1xuICB0ZXh0U3R5bGUudGV4dFNoYWRvd0JsdXIgPSB0ZXh0U3R5bGVNb2RlbC5nZXRTaGFsbG93KCd0ZXh0U2hhZG93Qmx1cicpIHx8IGdsb2JhbFRleHRTdHlsZS50ZXh0U2hhZG93Qmx1cjtcbiAgdGV4dFN0eWxlLnRleHRTaGFkb3dPZmZzZXRYID0gdGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygndGV4dFNoYWRvd09mZnNldFgnKSB8fCBnbG9iYWxUZXh0U3R5bGUudGV4dFNoYWRvd09mZnNldFg7XG4gIHRleHRTdHlsZS50ZXh0U2hhZG93T2Zmc2V0WSA9IHRleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ3RleHRTaGFkb3dPZmZzZXRZJykgfHwgZ2xvYmFsVGV4dFN0eWxlLnRleHRTaGFkb3dPZmZzZXRZO1xufVxuXG5mdW5jdGlvbiBnZXRBdXRvQ29sb3IoY29sb3IsIG9wdCkge1xuICByZXR1cm4gY29sb3IgIT09ICdhdXRvJyA/IGNvbG9yIDogb3B0ICYmIG9wdC5hdXRvQ29sb3IgPyBvcHQuYXV0b0NvbG9yIDogbnVsbDtcbn1cbi8qKlxuICogR2l2ZSBzb21lIGRlZmF1bHQgdmFsdWUgdG8gdGhlIGlucHV0IGB0ZXh0U3R5bGVgIG9iamVjdCwgYmFzZWQgb24gdGhlIGN1cnJlbnQgc2V0dGluZ3NcbiAqIGluIHRoaXMgYHRleHRTdHlsZWAgb2JqZWN0LlxuICpcbiAqIFRoZSBTY2VuYXJpbzpcbiAqIHdoZW4gdGV4dCBwb3NpdGlvbiBpcyBgaW5zaWRlYCBhbmQgYHRleHRGaWxsYCBpcyBub3Qgc3BlY2lmaWVkLCB3ZSBzaG93XG4gKiB0ZXh0IGJvcmRlciBieSBkZWZhdWx0IGZvciBiZXR0ZXIgdmlldy4gQnV0IGl0IHNob3VsZCBiZSBjb25zaWRlcmVkIHRoYXQgdGV4dCBwb3NpdGlvblxuICogbWlnaHQgYmUgY2hhbmdlZCB3aGVuIGhvdmVyaW5nIG9yIGJlaW5nIGVtcGhhc2lzLCB3aGVyZSB0aGUgYGluc2lkZVJvbGxiYWNrYCBpcyB1c2VkIHRvXG4gKiByZXN0b3JlIHRoZSBzdHlsZS5cbiAqXG4gKiBVc2FnZSAoJiBOT1RJQ0UpOlxuICogV2hlbiBhIHN0eWxlIG9iamVjdCAoZWl0aG9yIHBsYWluIG9iamVjdCBvciBpbnN0YW5jZSBvZiBgenJlbmRlci9zcmMvZ3JhcGhpYy9TdHlsZWApIGlzXG4gKiBhYm91dCB0byBiZSBtb2RpZmllZCBvbiBpdHMgdGV4dCByZWxhdGVkIHByb3BlcnRpZXMsIGByb2xsYmFja0RlZmF1bHRUZXh0U3R5bGVgIHNob3VsZFxuICogYmUgY2FsbGVkIGJlZm9yZSB0aGUgbW9kaWZpY2F0aW9uIGFuZCBgYXBwbHlEZWZhdWx0VGV4dFN0eWxlYCBzaG91bGQgYmUgY2FsbGVkIGFmdGVyIHRoYXQuXG4gKiAoRm9yIHRoZSBjYXNlIHRoYXQgYWxsIG9mIHRoZSB0ZXh0IHJlbGF0ZWQgcHJvcGVydGllcyBpcyByZXNldCwgbGlrZSBgc2V0VGV4dFN0eWxlQ29tbW9uYFxuICogZG9lcywgYHJvbGxiYWNrRGVmYXVsdFRleHRTdHlsZWAgaXMgbm90IG5lZWRlZCB0byBiZSBjYWxsZWQpLlxuICovXG5cblxuZnVuY3Rpb24gYXBwbHlEZWZhdWx0VGV4dFN0eWxlKHRleHRTdHlsZSkge1xuICB2YXIgb3B0ID0gdGV4dFN0eWxlLmluc2lkZVJvbGxiYWNrT3B0OyAvLyBPbmx5IGBpbnNpZGVSb2xsYmFja09wdGAgY3JlYXRlZCAoaW4gYHNldFRleHRTdHlsZUNvbW1vbmApLFxuICAvLyBhcHBseURlZmF1bHRUZXh0U3R5bGUgd29ya3MuXG5cbiAgaWYgKCFvcHQgfHwgdGV4dFN0eWxlLnRleHRGaWxsICE9IG51bGwpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgdXNlSW5zaWRlU3R5bGUgPSBvcHQudXNlSW5zaWRlU3R5bGU7XG4gIHZhciB0ZXh0UG9zaXRpb24gPSB0ZXh0U3R5bGUuaW5zaWRlUmF3VGV4dFBvc2l0aW9uO1xuICB2YXIgaW5zaWRlUm9sbGJhY2s7XG4gIHZhciBhdXRvQ29sb3IgPSBvcHQuYXV0b0NvbG9yO1xuXG4gIGlmICh1c2VJbnNpZGVTdHlsZSAhPT0gZmFsc2UgJiYgKHVzZUluc2lkZVN0eWxlID09PSB0cnVlIHx8IG9wdC5pc1JlY3RUZXh0ICYmIHRleHRQb3NpdGlvbiAvLyB0ZXh0UG9zaXRpb24gY2FuIGJlIFsxMCwgMzBdXG4gICYmIHR5cGVvZiB0ZXh0UG9zaXRpb24gPT09ICdzdHJpbmcnICYmIHRleHRQb3NpdGlvbi5pbmRleE9mKCdpbnNpZGUnKSA+PSAwKSkge1xuICAgIGluc2lkZVJvbGxiYWNrID0ge1xuICAgICAgdGV4dEZpbGw6IG51bGwsXG4gICAgICB0ZXh0U3Ryb2tlOiB0ZXh0U3R5bGUudGV4dFN0cm9rZSxcbiAgICAgIHRleHRTdHJva2VXaWR0aDogdGV4dFN0eWxlLnRleHRTdHJva2VXaWR0aFxuICAgIH07XG4gICAgdGV4dFN0eWxlLnRleHRGaWxsID0gJyNmZmYnOyAvLyBDb25zaWRlciB0ZXh0IHdpdGggI2ZmZiBvdmVyZmxvdyBpdHMgY29udGFpbmVyLlxuXG4gICAgaWYgKHRleHRTdHlsZS50ZXh0U3Ryb2tlID09IG51bGwpIHtcbiAgICAgIHRleHRTdHlsZS50ZXh0U3Ryb2tlID0gYXV0b0NvbG9yO1xuICAgICAgdGV4dFN0eWxlLnRleHRTdHJva2VXaWR0aCA9PSBudWxsICYmICh0ZXh0U3R5bGUudGV4dFN0cm9rZVdpZHRoID0gMik7XG4gICAgfVxuICB9IGVsc2UgaWYgKGF1dG9Db2xvciAhPSBudWxsKSB7XG4gICAgaW5zaWRlUm9sbGJhY2sgPSB7XG4gICAgICB0ZXh0RmlsbDogbnVsbFxuICAgIH07XG4gICAgdGV4dFN0eWxlLnRleHRGaWxsID0gYXV0b0NvbG9yO1xuICB9IC8vIEFsd2F5cyBzZXQgYGluc2lkZVJvbGxiYWNrYCwgZm9yIGNsZWFyaW5nIHByZXZpb3VzLlxuXG5cbiAgaWYgKGluc2lkZVJvbGxiYWNrKSB7XG4gICAgdGV4dFN0eWxlLmluc2lkZVJvbGxiYWNrID0gaW5zaWRlUm9sbGJhY2s7XG4gIH1cbn1cbi8qKlxuICogQ29uc2lkZXIgdGhlIGNhc2U6IGluIGEgc2NhdHRlcixcbiAqIGxhYmVsOiB7XG4gKiAgICAgbm9ybWFsOiB7cG9zaXRpb246ICdpbnNpZGUnfSxcbiAqICAgICBlbXBoYXNpczoge3Bvc2l0aW9uOiAndG9wJ31cbiAqIH1cbiAqIEluIHRoZSBub3JtYWwgc3RhdGUsIHRoZSBgdGV4dEZpbGxgIHdpbGwgYmUgc2V0IGFzICcjZmZmJyBmb3IgcHJldHR5IHZpZXcgKHNlZVxuICogYGFwcGx5RGVmYXVsdFRleHRTdHlsZWApLCBidXQgd2hlbiBzd2l0Y2hpbmcgdG8gZW1waGFzaXMgc3RhdGUsIHRoZSBgdGV4dEZpbGxgXG4gKiBzaG91bGQgYmUgcmV0dXJlZCB0byAnYXV0b0NvbG9yJywgYnV0IG5vdCBrZWVwICcjZmZmJy5cbiAqL1xuXG5cbmZ1bmN0aW9uIHJvbGxiYWNrRGVmYXVsdFRleHRTdHlsZShzdHlsZSkge1xuICB2YXIgaW5zaWRlUm9sbGJhY2sgPSBzdHlsZS5pbnNpZGVSb2xsYmFjaztcblxuICBpZiAoaW5zaWRlUm9sbGJhY2spIHtcbiAgICBzdHlsZS50ZXh0RmlsbCA9IGluc2lkZVJvbGxiYWNrLnRleHRGaWxsO1xuICAgIHN0eWxlLnRleHRTdHJva2UgPSBpbnNpZGVSb2xsYmFjay50ZXh0U3Ryb2tlO1xuICAgIHN0eWxlLnRleHRTdHJva2VXaWR0aCA9IGluc2lkZVJvbGxiYWNrLnRleHRTdHJva2VXaWR0aDtcbiAgICBzdHlsZS5pbnNpZGVSb2xsYmFjayA9IG51bGw7XG4gIH1cbn1cblxuZnVuY3Rpb24gZ2V0Rm9udChvcHQsIGVjTW9kZWwpIHtcbiAgLy8gZWNNb2RlbCBvciBkZWZhdWx0IHRleHQgc3R5bGUgbW9kZWwuXG4gIHZhciBnVGV4dFN0eWxlTW9kZWwgPSBlY01vZGVsIHx8IGVjTW9kZWwuZ2V0TW9kZWwoJ3RleHRTdHlsZScpO1xuICByZXR1cm4genJVdGlsLnRyaW0oWy8vIEZJWE1FIGluIG5vZGUtY2FudmFzIGZvbnRXZWlnaHQgaXMgYmVmb3JlIGZvbnRTdHlsZVxuICBvcHQuZm9udFN0eWxlIHx8IGdUZXh0U3R5bGVNb2RlbCAmJiBnVGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygnZm9udFN0eWxlJykgfHwgJycsIG9wdC5mb250V2VpZ2h0IHx8IGdUZXh0U3R5bGVNb2RlbCAmJiBnVGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygnZm9udFdlaWdodCcpIHx8ICcnLCAob3B0LmZvbnRTaXplIHx8IGdUZXh0U3R5bGVNb2RlbCAmJiBnVGV4dFN0eWxlTW9kZWwuZ2V0U2hhbGxvdygnZm9udFNpemUnKSB8fCAxMikgKyAncHgnLCBvcHQuZm9udEZhbWlseSB8fCBnVGV4dFN0eWxlTW9kZWwgJiYgZ1RleHRTdHlsZU1vZGVsLmdldFNoYWxsb3coJ2ZvbnRGYW1pbHknKSB8fCAnc2Fucy1zZXJpZiddLmpvaW4oJyAnKSk7XG59XG5cbmZ1bmN0aW9uIGFuaW1hdGVPclNldFByb3BzKGlzVXBkYXRlLCBlbCwgcHJvcHMsIGFuaW1hdGFibGVNb2RlbCwgZGF0YUluZGV4LCBjYikge1xuICBpZiAodHlwZW9mIGRhdGFJbmRleCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGNiID0gZGF0YUluZGV4O1xuICAgIGRhdGFJbmRleCA9IG51bGw7XG4gIH0gLy8gRG8gbm90IGNoZWNrICdhbmltYXRpb24nIHByb3BlcnR5IGRpcmVjdGx5IGhlcmUuIENvbnNpZGVyIHRoaXMgY2FzZTpcbiAgLy8gYW5pbWF0aW9uIG1vZGVsIGlzIGFuIGBpdGVtTW9kZWxgLCB3aG9zZSBkb2VzIG5vdCBoYXZlIGBpc0FuaW1hdGlvbkVuYWJsZWRgXG4gIC8vIGJ1dCBpdHMgcGFyZW50IG1vZGVsIChgc2VyaWVzTW9kZWxgKSBkb2VzLlxuXG5cbiAgdmFyIGFuaW1hdGlvbkVuYWJsZWQgPSBhbmltYXRhYmxlTW9kZWwgJiYgYW5pbWF0YWJsZU1vZGVsLmlzQW5pbWF0aW9uRW5hYmxlZCgpO1xuXG4gIGlmIChhbmltYXRpb25FbmFibGVkKSB7XG4gICAgdmFyIHBvc3RmaXggPSBpc1VwZGF0ZSA/ICdVcGRhdGUnIDogJyc7XG4gICAgdmFyIGR1cmF0aW9uID0gYW5pbWF0YWJsZU1vZGVsLmdldFNoYWxsb3coJ2FuaW1hdGlvbkR1cmF0aW9uJyArIHBvc3RmaXgpO1xuICAgIHZhciBhbmltYXRpb25FYXNpbmcgPSBhbmltYXRhYmxlTW9kZWwuZ2V0U2hhbGxvdygnYW5pbWF0aW9uRWFzaW5nJyArIHBvc3RmaXgpO1xuICAgIHZhciBhbmltYXRpb25EZWxheSA9IGFuaW1hdGFibGVNb2RlbC5nZXRTaGFsbG93KCdhbmltYXRpb25EZWxheScgKyBwb3N0Zml4KTtcblxuICAgIGlmICh0eXBlb2YgYW5pbWF0aW9uRGVsYXkgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIGFuaW1hdGlvbkRlbGF5ID0gYW5pbWF0aW9uRGVsYXkoZGF0YUluZGV4LCBhbmltYXRhYmxlTW9kZWwuZ2V0QW5pbWF0aW9uRGVsYXlQYXJhbXMgPyBhbmltYXRhYmxlTW9kZWwuZ2V0QW5pbWF0aW9uRGVsYXlQYXJhbXMoZWwsIGRhdGFJbmRleCkgOiBudWxsKTtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIGR1cmF0aW9uID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBkdXJhdGlvbiA9IGR1cmF0aW9uKGRhdGFJbmRleCk7XG4gICAgfVxuXG4gICAgZHVyYXRpb24gPiAwID8gZWwuYW5pbWF0ZVRvKHByb3BzLCBkdXJhdGlvbiwgYW5pbWF0aW9uRGVsYXkgfHwgMCwgYW5pbWF0aW9uRWFzaW5nLCBjYiwgISFjYikgOiAoZWwuc3RvcEFuaW1hdGlvbigpLCBlbC5hdHRyKHByb3BzKSwgY2IgJiYgY2IoKSk7XG4gIH0gZWxzZSB7XG4gICAgZWwuc3RvcEFuaW1hdGlvbigpO1xuICAgIGVsLmF0dHIocHJvcHMpO1xuICAgIGNiICYmIGNiKCk7XG4gIH1cbn1cbi8qKlxuICogVXBkYXRlIGdyYXBoaWMgZWxlbWVudCBwcm9wZXJ0aWVzIHdpdGggb3Igd2l0aG91dCBhbmltYXRpb24gYWNjb3JkaW5nIHRvIHRoZVxuICogY29uZmlndXJhdGlvbiBpbiBzZXJpZXMuXG4gKlxuICogQ2F1dGlvbjogdGhpcyBtZXRob2Qgd2lsbCBzdG9wIHByZXZpb3VzIGFuaW1hdGlvbi5cbiAqIFNvIGlmIGRvIG5vdCB1c2UgdGhpcyBtZXRob2QgdG8gb25lIGVsZW1lbnQgdHdpY2UgYmVmb3JlXG4gKiBhbmltYXRpb24gc3RhcnRzLCB1bmxlc3MgeW91IGtub3cgd2hhdCB5b3UgYXJlIGRvaW5nLlxuICpcbiAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvRWxlbWVudH0gZWxcbiAqIEBwYXJhbSB7T2JqZWN0fSBwcm9wc1xuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gW2FuaW1hdGFibGVNb2RlbF1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbZGF0YUluZGV4XVxuICogQHBhcmFtIHtGdW5jdGlvbn0gW2NiXVxuICogQGV4YW1wbGVcbiAqICAgICBncmFwaGljLnVwZGF0ZVByb3BzKGVsLCB7XG4gKiAgICAgICAgIHBvc2l0aW9uOiBbMTAwLCAxMDBdXG4gKiAgICAgfSwgc2VyaWVzTW9kZWwsIGRhdGFJbmRleCwgZnVuY3Rpb24gKCkgeyBjb25zb2xlLmxvZygnQW5pbWF0aW9uIGRvbmUhJyk7IH0pO1xuICogICAgIC8vIE9yXG4gKiAgICAgZ3JhcGhpYy51cGRhdGVQcm9wcyhlbCwge1xuICogICAgICAgICBwb3NpdGlvbjogWzEwMCwgMTAwXVxuICogICAgIH0sIHNlcmllc01vZGVsLCBmdW5jdGlvbiAoKSB7IGNvbnNvbGUubG9nKCdBbmltYXRpb24gZG9uZSEnKTsgfSk7XG4gKi9cblxuXG5mdW5jdGlvbiB1cGRhdGVQcm9wcyhlbCwgcHJvcHMsIGFuaW1hdGFibGVNb2RlbCwgZGF0YUluZGV4LCBjYikge1xuICBhbmltYXRlT3JTZXRQcm9wcyh0cnVlLCBlbCwgcHJvcHMsIGFuaW1hdGFibGVNb2RlbCwgZGF0YUluZGV4LCBjYik7XG59XG4vKipcbiAqIEluaXQgZ3JhcGhpYyBlbGVtZW50IHByb3BlcnRpZXMgd2l0aCBvciB3aXRob3V0IGFuaW1hdGlvbiBhY2NvcmRpbmcgdG8gdGhlXG4gKiBjb25maWd1cmF0aW9uIGluIHNlcmllcy5cbiAqXG4gKiBDYXV0aW9uOiB0aGlzIG1ldGhvZCB3aWxsIHN0b3AgcHJldmlvdXMgYW5pbWF0aW9uLlxuICogU28gaWYgZG8gbm90IHVzZSB0aGlzIG1ldGhvZCB0byBvbmUgZWxlbWVudCB0d2ljZSBiZWZvcmVcbiAqIGFuaW1hdGlvbiBzdGFydHMsIHVubGVzcyB5b3Uga25vdyB3aGF0IHlvdSBhcmUgZG9pbmcuXG4gKlxuICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9FbGVtZW50fSBlbFxuICogQHBhcmFtIHtPYmplY3R9IHByb3BzXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBbYW5pbWF0YWJsZU1vZGVsXVxuICogQHBhcmFtIHtudW1iZXJ9IFtkYXRhSW5kZXhdXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYlxuICovXG5cblxuZnVuY3Rpb24gaW5pdFByb3BzKGVsLCBwcm9wcywgYW5pbWF0YWJsZU1vZGVsLCBkYXRhSW5kZXgsIGNiKSB7XG4gIGFuaW1hdGVPclNldFByb3BzKGZhbHNlLCBlbCwgcHJvcHMsIGFuaW1hdGFibGVNb2RlbCwgZGF0YUluZGV4LCBjYik7XG59XG4vKipcbiAqIEdldCB0cmFuc2Zvcm0gbWF0cml4IG9mIHRhcmdldCAocGFyYW0gdGFyZ2V0KSxcbiAqIGluIGNvb3JkaW5hdGUgb2YgaXRzIGFuY2VzdG9yIChwYXJhbSBhbmNlc3RvcilcbiAqXG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL21peGluL1RyYW5zZm9ybWFibGV9IHRhcmdldFxuICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9taXhpbi9UcmFuc2Zvcm1hYmxlfSBbYW5jZXN0b3JdXG4gKi9cblxuXG5mdW5jdGlvbiBnZXRUcmFuc2Zvcm0odGFyZ2V0LCBhbmNlc3Rvcikge1xuICB2YXIgbWF0ID0gbWF0cml4LmlkZW50aXR5KFtdKTtcblxuICB3aGlsZSAodGFyZ2V0ICYmIHRhcmdldCAhPT0gYW5jZXN0b3IpIHtcbiAgICBtYXRyaXgubXVsKG1hdCwgdGFyZ2V0LmdldExvY2FsVHJhbnNmb3JtKCksIG1hdCk7XG4gICAgdGFyZ2V0ID0gdGFyZ2V0LnBhcmVudDtcbiAgfVxuXG4gIHJldHVybiBtYXQ7XG59XG4vKipcbiAqIEFwcGx5IHRyYW5zZm9ybSB0byBhbiB2ZXJ0ZXguXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSB0YXJnZXQgW3gsIHldXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fFR5cGVkQXJyYXkuPG51bWJlcj58T2JqZWN0fSB0cmFuc2Zvcm0gQ2FuIGJlOlxuICogICAgICArIFRyYW5zZm9ybSBtYXRyaXg6IGxpa2UgWzEsIDAsIDAsIDEsIDAsIDBdXG4gKiAgICAgICsge3Bvc2l0aW9uLCByb3RhdGlvbiwgc2NhbGV9LCB0aGUgc2FtZSBhcyBgenJlbmRlci9UcmFuc2Zvcm1hYmxlYC5cbiAqIEBwYXJhbSB7Ym9vbGVhbj19IGludmVydCBXaGV0aGVyIHVzZSBpbnZlcnQgbWF0cml4LlxuICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59IFt4LCB5XVxuICovXG5cblxuZnVuY3Rpb24gYXBwbHlUcmFuc2Zvcm0odGFyZ2V0LCB0cmFuc2Zvcm0sIGludmVydCkge1xuICBpZiAodHJhbnNmb3JtICYmICF6clV0aWwuaXNBcnJheUxpa2UodHJhbnNmb3JtKSkge1xuICAgIHRyYW5zZm9ybSA9IFRyYW5zZm9ybWFibGUuZ2V0TG9jYWxUcmFuc2Zvcm0odHJhbnNmb3JtKTtcbiAgfVxuXG4gIGlmIChpbnZlcnQpIHtcbiAgICB0cmFuc2Zvcm0gPSBtYXRyaXguaW52ZXJ0KFtdLCB0cmFuc2Zvcm0pO1xuICB9XG5cbiAgcmV0dXJuIHZlY3Rvci5hcHBseVRyYW5zZm9ybShbXSwgdGFyZ2V0LCB0cmFuc2Zvcm0pO1xufVxuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ30gZGlyZWN0aW9uICdsZWZ0JyAncmlnaHQnICd0b3AnICdib3R0b20nXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSB0cmFuc2Zvcm0gVHJhbnNmb3JtIG1hdHJpeDogbGlrZSBbMSwgMCwgMCwgMSwgMCwgMF1cbiAqIEBwYXJhbSB7Ym9vbGVhbj19IGludmVydCBXaGV0aGVyIHVzZSBpbnZlcnQgbWF0cml4LlxuICogQHJldHVybiB7c3RyaW5nfSBUcmFuc2Zvcm1lZCBkaXJlY3Rpb24uICdsZWZ0JyAncmlnaHQnICd0b3AnICdib3R0b20nXG4gKi9cblxuXG5mdW5jdGlvbiB0cmFuc2Zvcm1EaXJlY3Rpb24oZGlyZWN0aW9uLCB0cmFuc2Zvcm0sIGludmVydCkge1xuICAvLyBQaWNrIGEgYmFzZSwgZW5zdXJlIHRoYXQgdHJhbnNmb3JtIHJlc3VsdCB3aWxsIG5vdCBiZSAoMCwgMCkuXG4gIHZhciBoQmFzZSA9IHRyYW5zZm9ybVs0XSA9PT0gMCB8fCB0cmFuc2Zvcm1bNV0gPT09IDAgfHwgdHJhbnNmb3JtWzBdID09PSAwID8gMSA6IE1hdGguYWJzKDIgKiB0cmFuc2Zvcm1bNF0gLyB0cmFuc2Zvcm1bMF0pO1xuICB2YXIgdkJhc2UgPSB0cmFuc2Zvcm1bNF0gPT09IDAgfHwgdHJhbnNmb3JtWzVdID09PSAwIHx8IHRyYW5zZm9ybVsyXSA9PT0gMCA/IDEgOiBNYXRoLmFicygyICogdHJhbnNmb3JtWzRdIC8gdHJhbnNmb3JtWzJdKTtcbiAgdmFyIHZlcnRleCA9IFtkaXJlY3Rpb24gPT09ICdsZWZ0JyA/IC1oQmFzZSA6IGRpcmVjdGlvbiA9PT0gJ3JpZ2h0JyA/IGhCYXNlIDogMCwgZGlyZWN0aW9uID09PSAndG9wJyA/IC12QmFzZSA6IGRpcmVjdGlvbiA9PT0gJ2JvdHRvbScgPyB2QmFzZSA6IDBdO1xuICB2ZXJ0ZXggPSBhcHBseVRyYW5zZm9ybSh2ZXJ0ZXgsIHRyYW5zZm9ybSwgaW52ZXJ0KTtcbiAgcmV0dXJuIE1hdGguYWJzKHZlcnRleFswXSkgPiBNYXRoLmFicyh2ZXJ0ZXhbMV0pID8gdmVydGV4WzBdID4gMCA/ICdyaWdodCcgOiAnbGVmdCcgOiB2ZXJ0ZXhbMV0gPiAwID8gJ2JvdHRvbScgOiAndG9wJztcbn1cbi8qKlxuICogQXBwbHkgZ3JvdXAgdHJhbnNpdGlvbiBhbmltYXRpb24gZnJvbSBnMSB0byBnMi5cbiAqIElmIG5vIGFuaW1hdGFibGVNb2RlbCwgbm8gYW5pbWF0aW9uLlxuICovXG5cblxuZnVuY3Rpb24gZ3JvdXBUcmFuc2l0aW9uKGcxLCBnMiwgYW5pbWF0YWJsZU1vZGVsLCBjYikge1xuICBpZiAoIWcxIHx8ICFnMikge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGZ1bmN0aW9uIGdldEVsTWFwKGcpIHtcbiAgICB2YXIgZWxNYXAgPSB7fTtcbiAgICBnLnRyYXZlcnNlKGZ1bmN0aW9uIChlbCkge1xuICAgICAgaWYgKCFlbC5pc0dyb3VwICYmIGVsLmFuaWQpIHtcbiAgICAgICAgZWxNYXBbZWwuYW5pZF0gPSBlbDtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gZWxNYXA7XG4gIH1cblxuICBmdW5jdGlvbiBnZXRBbmltYXRhYmxlUHJvcHMoZWwpIHtcbiAgICB2YXIgb2JqID0ge1xuICAgICAgcG9zaXRpb246IHZlY3Rvci5jbG9uZShlbC5wb3NpdGlvbiksXG4gICAgICByb3RhdGlvbjogZWwucm90YXRpb25cbiAgICB9O1xuXG4gICAgaWYgKGVsLnNoYXBlKSB7XG4gICAgICBvYmouc2hhcGUgPSB6clV0aWwuZXh0ZW5kKHt9LCBlbC5zaGFwZSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG9iajtcbiAgfVxuXG4gIHZhciBlbE1hcDEgPSBnZXRFbE1hcChnMSk7XG4gIGcyLnRyYXZlcnNlKGZ1bmN0aW9uIChlbCkge1xuICAgIGlmICghZWwuaXNHcm91cCAmJiBlbC5hbmlkKSB7XG4gICAgICB2YXIgb2xkRWwgPSBlbE1hcDFbZWwuYW5pZF07XG5cbiAgICAgIGlmIChvbGRFbCkge1xuICAgICAgICB2YXIgbmV3UHJvcCA9IGdldEFuaW1hdGFibGVQcm9wcyhlbCk7XG4gICAgICAgIGVsLmF0dHIoZ2V0QW5pbWF0YWJsZVByb3BzKG9sZEVsKSk7XG4gICAgICAgIHVwZGF0ZVByb3BzKGVsLCBuZXdQcm9wLCBhbmltYXRhYmxlTW9kZWwsIGVsLmRhdGFJbmRleCk7XG4gICAgICB9IC8vIGVsc2Uge1xuICAgICAgLy8gICAgIGlmIChlbC5wcmV2aW91c1Byb3BzKSB7XG4gICAgICAvLyAgICAgICAgIGdyYXBoaWMudXBkYXRlUHJvcHNcbiAgICAgIC8vICAgICB9XG4gICAgICAvLyB9XG5cbiAgICB9XG4gIH0pO1xufVxuLyoqXG4gKiBAcGFyYW0ge0FycmF5LjxBcnJheS48bnVtYmVyPj59IHBvaW50cyBMaWtlOiBbWzIzLCA0NF0sIFs1MywgNjZdLCAuLi5dXG4gKiBAcGFyYW0ge09iamVjdH0gcmVjdCB7eCwgeSwgd2lkdGgsIGhlaWdodH1cbiAqIEByZXR1cm4ge0FycmF5LjxBcnJheS48bnVtYmVyPj59IEEgbmV3IGNsaXBwZWQgcG9pbnRzLlxuICovXG5cblxuZnVuY3Rpb24gY2xpcFBvaW50c0J5UmVjdChwb2ludHMsIHJlY3QpIHtcbiAgLy8gRklYTUU6IHRoaXMgd2F5IG1pZ3RoIGJlIGluY29ycmVjdCB3aGVuIGdycGFoaWMgY2xpcHBlZCBieSBhIGNvcm5lci5cbiAgLy8gYW5kIHdoZW4gZWxlbWVudCBoYXZlIGJvcmRlci5cbiAgcmV0dXJuIHpyVXRpbC5tYXAocG9pbnRzLCBmdW5jdGlvbiAocG9pbnQpIHtcbiAgICB2YXIgeCA9IHBvaW50WzBdO1xuICAgIHggPSBtYXRoTWF4KHgsIHJlY3QueCk7XG4gICAgeCA9IG1hdGhNaW4oeCwgcmVjdC54ICsgcmVjdC53aWR0aCk7XG4gICAgdmFyIHkgPSBwb2ludFsxXTtcbiAgICB5ID0gbWF0aE1heCh5LCByZWN0LnkpO1xuICAgIHkgPSBtYXRoTWluKHksIHJlY3QueSArIHJlY3QuaGVpZ2h0KTtcbiAgICByZXR1cm4gW3gsIHldO1xuICB9KTtcbn1cbi8qKlxuICogQHBhcmFtIHtPYmplY3R9IHRhcmdldFJlY3Qge3gsIHksIHdpZHRoLCBoZWlnaHR9XG4gKiBAcGFyYW0ge09iamVjdH0gcmVjdCB7eCwgeSwgd2lkdGgsIGhlaWdodH1cbiAqIEByZXR1cm4ge09iamVjdH0gQSBuZXcgY2xpcHBlZCByZWN0LiBJZiByZWN0IHNpemUgYXJlIG5lZ2F0aXZlLCByZXR1cm4gdW5kZWZpbmVkLlxuICovXG5cblxuZnVuY3Rpb24gY2xpcFJlY3RCeVJlY3QodGFyZ2V0UmVjdCwgcmVjdCkge1xuICB2YXIgeCA9IG1hdGhNYXgodGFyZ2V0UmVjdC54LCByZWN0LngpO1xuICB2YXIgeDIgPSBtYXRoTWluKHRhcmdldFJlY3QueCArIHRhcmdldFJlY3Qud2lkdGgsIHJlY3QueCArIHJlY3Qud2lkdGgpO1xuICB2YXIgeSA9IG1hdGhNYXgodGFyZ2V0UmVjdC55LCByZWN0LnkpO1xuICB2YXIgeTIgPSBtYXRoTWluKHRhcmdldFJlY3QueSArIHRhcmdldFJlY3QuaGVpZ2h0LCByZWN0LnkgKyByZWN0LmhlaWdodCk7IC8vIElmIHRoZSB0b3RhbCByZWN0IGlzIGNsaXBlZCwgbm90aGluZywgaW5jbHVkaW5nIHRoZSBib3JkZXIsXG4gIC8vIHNob3VsZCBiZSBwYWludGVkLiBTbyByZXR1cm4gdW5kZWZpbmVkLlxuXG4gIGlmICh4MiA+PSB4ICYmIHkyID49IHkpIHtcbiAgICByZXR1cm4ge1xuICAgICAgeDogeCxcbiAgICAgIHk6IHksXG4gICAgICB3aWR0aDogeDIgLSB4LFxuICAgICAgaGVpZ2h0OiB5MiAtIHlcbiAgICB9O1xuICB9XG59XG4vKipcbiAqIEBwYXJhbSB7c3RyaW5nfSBpY29uU3RyIFN1cHBvcnQgJ2ltYWdlOi8vJyBvciAncGF0aDovLycgb3IgZGlyZWN0IHN2ZyBwYXRoLlxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRdIFByb3BlcnRpZXMgb2YgYG1vZHVsZTp6cmVuZGVyL0VsZW1lbnRgLCBleGNlcHQgYHN0eWxlYC5cbiAqIEBwYXJhbSB7T2JqZWN0fSBbcmVjdF0ge3gsIHksIHdpZHRoLCBoZWlnaHR9XG4gKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9FbGVtZW50fSBJY29uIHBhdGggb3IgaW1hZ2UgZWxlbWVudC5cbiAqL1xuXG5cbmZ1bmN0aW9uIGNyZWF0ZUljb24oaWNvblN0ciwgb3B0LCByZWN0KSB7XG4gIG9wdCA9IHpyVXRpbC5leHRlbmQoe1xuICAgIHJlY3RIb3ZlcjogdHJ1ZVxuICB9LCBvcHQpO1xuICB2YXIgc3R5bGUgPSBvcHQuc3R5bGUgPSB7XG4gICAgc3Ryb2tlTm9TY2FsZTogdHJ1ZVxuICB9O1xuICByZWN0ID0gcmVjdCB8fCB7XG4gICAgeDogLTEsXG4gICAgeTogLTEsXG4gICAgd2lkdGg6IDIsXG4gICAgaGVpZ2h0OiAyXG4gIH07XG5cbiAgaWYgKGljb25TdHIpIHtcbiAgICByZXR1cm4gaWNvblN0ci5pbmRleE9mKCdpbWFnZTovLycpID09PSAwID8gKHN0eWxlLmltYWdlID0gaWNvblN0ci5zbGljZSg4KSwgenJVdGlsLmRlZmF1bHRzKHN0eWxlLCByZWN0KSwgbmV3IFpJbWFnZShvcHQpKSA6IG1ha2VQYXRoKGljb25TdHIucmVwbGFjZSgncGF0aDovLycsICcnKSwgb3B0LCByZWN0LCAnY2VudGVyJyk7XG4gIH1cbn1cblxuZXhwb3J0cy5aMl9FTVBIQVNJU19MSUZUID0gWjJfRU1QSEFTSVNfTElGVDtcbmV4cG9ydHMuZXh0ZW5kU2hhcGUgPSBleHRlbmRTaGFwZTtcbmV4cG9ydHMuZXh0ZW5kUGF0aCA9IGV4dGVuZFBhdGg7XG5leHBvcnRzLm1ha2VQYXRoID0gbWFrZVBhdGg7XG5leHBvcnRzLm1ha2VJbWFnZSA9IG1ha2VJbWFnZTtcbmV4cG9ydHMubWVyZ2VQYXRoID0gbWVyZ2VQYXRoO1xuZXhwb3J0cy5yZXNpemVQYXRoID0gcmVzaXplUGF0aDtcbmV4cG9ydHMuc3ViUGl4ZWxPcHRpbWl6ZUxpbmUgPSBzdWJQaXhlbE9wdGltaXplTGluZTtcbmV4cG9ydHMuc3ViUGl4ZWxPcHRpbWl6ZVJlY3QgPSBzdWJQaXhlbE9wdGltaXplUmVjdDtcbmV4cG9ydHMuc3ViUGl4ZWxPcHRpbWl6ZSA9IHN1YlBpeGVsT3B0aW1pemU7XG5leHBvcnRzLnNldEVsZW1lbnRIb3ZlclN0eWxlID0gc2V0RWxlbWVudEhvdmVyU3R5bGU7XG5leHBvcnRzLmlzSW5FbXBoYXNpcyA9IGlzSW5FbXBoYXNpcztcbmV4cG9ydHMuc2V0SG92ZXJTdHlsZSA9IHNldEhvdmVyU3R5bGU7XG5leHBvcnRzLnNldEFzSG92ZXJTdHlsZVRyaWdnZXIgPSBzZXRBc0hvdmVyU3R5bGVUcmlnZ2VyO1xuZXhwb3J0cy5zZXRMYWJlbFN0eWxlID0gc2V0TGFiZWxTdHlsZTtcbmV4cG9ydHMuc2V0VGV4dFN0eWxlID0gc2V0VGV4dFN0eWxlO1xuZXhwb3J0cy5zZXRUZXh0ID0gc2V0VGV4dDtcbmV4cG9ydHMuZ2V0Rm9udCA9IGdldEZvbnQ7XG5leHBvcnRzLnVwZGF0ZVByb3BzID0gdXBkYXRlUHJvcHM7XG5leHBvcnRzLmluaXRQcm9wcyA9IGluaXRQcm9wcztcbmV4cG9ydHMuZ2V0VHJhbnNmb3JtID0gZ2V0VHJhbnNmb3JtO1xuZXhwb3J0cy5hcHBseVRyYW5zZm9ybSA9IGFwcGx5VHJhbnNmb3JtO1xuZXhwb3J0cy50cmFuc2Zvcm1EaXJlY3Rpb24gPSB0cmFuc2Zvcm1EaXJlY3Rpb247XG5leHBvcnRzLmdyb3VwVHJhbnNpdGlvbiA9IGdyb3VwVHJhbnNpdGlvbjtcbmV4cG9ydHMuY2xpcFBvaW50c0J5UmVjdCA9IGNsaXBQb2ludHNCeVJlY3Q7XG5leHBvcnRzLmNsaXBSZWN0QnlSZWN0ID0gY2xpcFJlY3RCeVJlY3Q7XG5leHBvcnRzLmNyZWF0ZUljb24gPSBjcmVhdGVJY29uOyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBaEJBO0FBa0JBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7OztBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQVdBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF3QkE7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/util/graphic.js
