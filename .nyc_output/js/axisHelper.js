/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var OrdinalScale = __webpack_require__(/*! ../scale/Ordinal */ "./node_modules/echarts/lib/scale/Ordinal.js");

var IntervalScale = __webpack_require__(/*! ../scale/Interval */ "./node_modules/echarts/lib/scale/Interval.js");

var Scale = __webpack_require__(/*! ../scale/Scale */ "./node_modules/echarts/lib/scale/Scale.js");

var numberUtil = __webpack_require__(/*! ../util/number */ "./node_modules/echarts/lib/util/number.js");

var _barGrid = __webpack_require__(/*! ../layout/barGrid */ "./node_modules/echarts/lib/layout/barGrid.js");

var prepareLayoutBarSeries = _barGrid.prepareLayoutBarSeries;
var makeColumnLayout = _barGrid.makeColumnLayout;
var retrieveColumnLayout = _barGrid.retrieveColumnLayout;

var BoundingRect = __webpack_require__(/*! zrender/lib/core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");

__webpack_require__(/*! ../scale/Time */ "./node_modules/echarts/lib/scale/Time.js");

__webpack_require__(/*! ../scale/Log */ "./node_modules/echarts/lib/scale/Log.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Get axis scale extent before niced.
 * Item of returned array can only be number (including Infinity and NaN).
 */


function getScaleExtent(scale, model) {
  var scaleType = scale.type;
  var min = model.getMin();
  var max = model.getMax();
  var fixMin = min != null;
  var fixMax = max != null;
  var originalExtent = scale.getExtent();
  var axisDataLen;
  var boundaryGap;
  var span;

  if (scaleType === 'ordinal') {
    axisDataLen = model.getCategories().length;
  } else {
    boundaryGap = model.get('boundaryGap');

    if (!zrUtil.isArray(boundaryGap)) {
      boundaryGap = [boundaryGap || 0, boundaryGap || 0];
    }

    if (typeof boundaryGap[0] === 'boolean') {
      boundaryGap = [0, 0];
    }

    boundaryGap[0] = numberUtil.parsePercent(boundaryGap[0], 1);
    boundaryGap[1] = numberUtil.parsePercent(boundaryGap[1], 1);
    span = originalExtent[1] - originalExtent[0] || Math.abs(originalExtent[0]);
  } // Notice: When min/max is not set (that is, when there are null/undefined,
  // which is the most common case), these cases should be ensured:
  // (1) For 'ordinal', show all axis.data.
  // (2) For others:
  //      + `boundaryGap` is applied (if min/max set, boundaryGap is
  //      disabled).
  //      + If `needCrossZero`, min/max should be zero, otherwise, min/max should
  //      be the result that originalExtent enlarged by boundaryGap.
  // (3) If no data, it should be ensured that `scale.setBlank` is set.
  // FIXME
  // (1) When min/max is 'dataMin' or 'dataMax', should boundaryGap be able to used?
  // (2) When `needCrossZero` and all data is positive/negative, should it be ensured
  // that the results processed by boundaryGap are positive/negative?


  if (min == null) {
    min = scaleType === 'ordinal' ? axisDataLen ? 0 : NaN : originalExtent[0] - boundaryGap[0] * span;
  }

  if (max == null) {
    max = scaleType === 'ordinal' ? axisDataLen ? axisDataLen - 1 : NaN : originalExtent[1] + boundaryGap[1] * span;
  }

  if (min === 'dataMin') {
    min = originalExtent[0];
  } else if (typeof min === 'function') {
    min = min({
      min: originalExtent[0],
      max: originalExtent[1]
    });
  }

  if (max === 'dataMax') {
    max = originalExtent[1];
  } else if (typeof max === 'function') {
    max = max({
      min: originalExtent[0],
      max: originalExtent[1]
    });
  }

  (min == null || !isFinite(min)) && (min = NaN);
  (max == null || !isFinite(max)) && (max = NaN);
  scale.setBlank(zrUtil.eqNaN(min) || zrUtil.eqNaN(max) || scaleType === 'ordinal' && !scale.getOrdinalMeta().categories.length); // Evaluate if axis needs cross zero

  if (model.getNeedCrossZero()) {
    // Axis is over zero and min is not set
    if (min > 0 && max > 0 && !fixMin) {
      min = 0;
    } // Axis is under zero and max is not set


    if (min < 0 && max < 0 && !fixMax) {
      max = 0;
    }
  } // If bars are placed on a base axis of type time or interval account for axis boundary overflow and current axis
  // is base axis
  // FIXME
  // (1) Consider support value axis, where below zero and axis `onZero` should be handled properly.
  // (2) Refactor the logic with `barGrid`. Is it not need to `makeBarWidthAndOffsetInfo` twice with different extent?
  //     Should not depend on series type `bar`?
  // (3) Fix that might overlap when using dataZoom.
  // (4) Consider other chart types using `barGrid`?
  // See #6728, #4862, `test/bar-overflow-time-plot.html`


  var ecModel = model.ecModel;

  if (ecModel && scaleType === 'time'
  /*|| scaleType === 'interval' */
  ) {
      var barSeriesModels = prepareLayoutBarSeries('bar', ecModel);
      var isBaseAxisAndHasBarSeries;
      zrUtil.each(barSeriesModels, function (seriesModel) {
        isBaseAxisAndHasBarSeries |= seriesModel.getBaseAxis() === model.axis;
      });

      if (isBaseAxisAndHasBarSeries) {
        // Calculate placement of bars on axis
        var barWidthAndOffset = makeColumnLayout(barSeriesModels); // Adjust axis min and max to account for overflow

        var adjustedScale = adjustScaleForOverflow(min, max, model, barWidthAndOffset);
        min = adjustedScale.min;
        max = adjustedScale.max;
      }
    }

  return [min, max];
}

function adjustScaleForOverflow(min, max, model, barWidthAndOffset) {
  // Get Axis Length
  var axisExtent = model.axis.getExtent();
  var axisLength = axisExtent[1] - axisExtent[0]; // Get bars on current base axis and calculate min and max overflow

  var barsOnCurrentAxis = retrieveColumnLayout(barWidthAndOffset, model.axis);

  if (barsOnCurrentAxis === undefined) {
    return {
      min: min,
      max: max
    };
  }

  var minOverflow = Infinity;
  zrUtil.each(barsOnCurrentAxis, function (item) {
    minOverflow = Math.min(item.offset, minOverflow);
  });
  var maxOverflow = -Infinity;
  zrUtil.each(barsOnCurrentAxis, function (item) {
    maxOverflow = Math.max(item.offset + item.width, maxOverflow);
  });
  minOverflow = Math.abs(minOverflow);
  maxOverflow = Math.abs(maxOverflow);
  var totalOverFlow = minOverflow + maxOverflow; // Calulate required buffer based on old range and overflow

  var oldRange = max - min;
  var oldRangePercentOfNew = 1 - (minOverflow + maxOverflow) / axisLength;
  var overflowBuffer = oldRange / oldRangePercentOfNew - oldRange;
  max += overflowBuffer * (maxOverflow / totalOverFlow);
  min -= overflowBuffer * (minOverflow / totalOverFlow);
  return {
    min: min,
    max: max
  };
}

function niceScaleExtent(scale, model) {
  var extent = getScaleExtent(scale, model);
  var fixMin = model.getMin() != null;
  var fixMax = model.getMax() != null;
  var splitNumber = model.get('splitNumber');

  if (scale.type === 'log') {
    scale.base = model.get('logBase');
  }

  var scaleType = scale.type;
  scale.setExtent(extent[0], extent[1]);
  scale.niceExtent({
    splitNumber: splitNumber,
    fixMin: fixMin,
    fixMax: fixMax,
    minInterval: scaleType === 'interval' || scaleType === 'time' ? model.get('minInterval') : null,
    maxInterval: scaleType === 'interval' || scaleType === 'time' ? model.get('maxInterval') : null
  }); // If some one specified the min, max. And the default calculated interval
  // is not good enough. He can specify the interval. It is often appeared
  // in angle axis with angle 0 - 360. Interval calculated in interval scale is hard
  // to be 60.
  // FIXME

  var interval = model.get('interval');

  if (interval != null) {
    scale.setInterval && scale.setInterval(interval);
  }
}
/**
 * @param {module:echarts/model/Model} model
 * @param {string} [axisType] Default retrieve from model.type
 * @return {module:echarts/scale/*}
 */


function createScaleByModel(model, axisType) {
  axisType = axisType || model.get('type');

  if (axisType) {
    switch (axisType) {
      // Buildin scale
      case 'category':
        return new OrdinalScale(model.getOrdinalMeta ? model.getOrdinalMeta() : model.getCategories(), [Infinity, -Infinity]);

      case 'value':
        return new IntervalScale();
      // Extended scale, like time and log

      default:
        return (Scale.getClass(axisType) || IntervalScale).create(model);
    }
  }
}
/**
 * Check if the axis corss 0
 */


function ifAxisCrossZero(axis) {
  var dataExtent = axis.scale.getExtent();
  var min = dataExtent[0];
  var max = dataExtent[1];
  return !(min > 0 && max > 0 || min < 0 && max < 0);
}
/**
 * @param {module:echarts/coord/Axis} axis
 * @return {Function} Label formatter function.
 *         param: {number} tickValue,
 *         param: {number} idx, the index in all ticks.
 *                         If category axis, this param is not requied.
 *         return: {string} label string.
 */


function makeLabelFormatter(axis) {
  var labelFormatter = axis.getLabelModel().get('formatter');
  var categoryTickStart = axis.type === 'category' ? axis.scale.getExtent()[0] : null;

  if (typeof labelFormatter === 'string') {
    labelFormatter = function (tpl) {
      return function (val) {
        // For category axis, get raw value; for numeric axis,
        // get foramtted label like '1,333,444'.
        val = axis.scale.getLabel(val);
        return tpl.replace('{value}', val != null ? val : '');
      };
    }(labelFormatter); // Consider empty array


    return labelFormatter;
  } else if (typeof labelFormatter === 'function') {
    return function (tickValue, idx) {
      // The original intention of `idx` is "the index of the tick in all ticks".
      // But the previous implementation of category axis do not consider the
      // `axisLabel.interval`, which cause that, for example, the `interval` is
      // `1`, then the ticks "name5", "name7", "name9" are displayed, where the
      // corresponding `idx` are `0`, `2`, `4`, but not `0`, `1`, `2`. So we keep
      // the definition here for back compatibility.
      if (categoryTickStart != null) {
        idx = tickValue - categoryTickStart;
      }

      return labelFormatter(getAxisRawValue(axis, tickValue), idx);
    };
  } else {
    return function (tick) {
      return axis.scale.getLabel(tick);
    };
  }
}

function getAxisRawValue(axis, value) {
  // In category axis with data zoom, tick is not the original
  // index of axis.data. So tick should not be exposed to user
  // in category axis.
  return axis.type === 'category' ? axis.scale.getLabel(value) : value;
}
/**
 * @param {module:echarts/coord/Axis} axis
 * @return {module:zrender/core/BoundingRect} Be null/undefined if no labels.
 */


function estimateLabelUnionRect(axis) {
  var axisModel = axis.model;
  var scale = axis.scale;

  if (!axisModel.get('axisLabel.show') || scale.isBlank()) {
    return;
  }

  var isCategory = axis.type === 'category';
  var realNumberScaleTicks;
  var tickCount;
  var categoryScaleExtent = scale.getExtent(); // Optimize for large category data, avoid call `getTicks()`.

  if (isCategory) {
    tickCount = scale.count();
  } else {
    realNumberScaleTicks = scale.getTicks();
    tickCount = realNumberScaleTicks.length;
  }

  var axisLabelModel = axis.getLabelModel();
  var labelFormatter = makeLabelFormatter(axis);
  var rect;
  var step = 1; // Simple optimization for large amount of labels

  if (tickCount > 40) {
    step = Math.ceil(tickCount / 40);
  }

  for (var i = 0; i < tickCount; i += step) {
    var tickValue = realNumberScaleTicks ? realNumberScaleTicks[i] : categoryScaleExtent[0] + i;
    var label = labelFormatter(tickValue);
    var unrotatedSingleRect = axisLabelModel.getTextRect(label);
    var singleRect = rotateTextRect(unrotatedSingleRect, axisLabelModel.get('rotate') || 0);
    rect ? rect.union(singleRect) : rect = singleRect;
  }

  return rect;
}

function rotateTextRect(textRect, rotate) {
  var rotateRadians = rotate * Math.PI / 180;
  var boundingBox = textRect.plain();
  var beforeWidth = boundingBox.width;
  var beforeHeight = boundingBox.height;
  var afterWidth = beforeWidth * Math.cos(rotateRadians) + beforeHeight * Math.sin(rotateRadians);
  var afterHeight = beforeWidth * Math.sin(rotateRadians) + beforeHeight * Math.cos(rotateRadians);
  var rotatedRect = new BoundingRect(boundingBox.x, boundingBox.y, afterWidth, afterHeight);
  return rotatedRect;
}
/**
 * @param {module:echarts/src/model/Model} model axisLabelModel or axisTickModel
 * @return {number|String} Can be null|'auto'|number|function
 */


function getOptionCategoryInterval(model) {
  var interval = model.get('interval');
  return interval == null ? 'auto' : interval;
}
/**
 * Set `categoryInterval` as 0 implicitly indicates that
 * show all labels reguardless of overlap.
 * @param {Object} axis axisModel.axis
 * @return {boolean}
 */


function shouldShowAllLabels(axis) {
  return axis.type === 'category' && getOptionCategoryInterval(axis.getLabelModel()) === 0;
}

exports.getScaleExtent = getScaleExtent;
exports.niceScaleExtent = niceScaleExtent;
exports.createScaleByModel = createScaleByModel;
exports.ifAxisCrossZero = ifAxisCrossZero;
exports.makeLabelFormatter = makeLabelFormatter;
exports.getAxisRawValue = getAxisRawValue;
exports.estimateLabelUnionRect = estimateLabelUnionRect;
exports.getOptionCategoryInterval = getOptionCategoryInterval;
exports.shouldShowAllLabels = shouldShowAllLabels;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvYXhpc0hlbHBlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2Nvb3JkL2F4aXNIZWxwZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfY29uZmlnID0gcmVxdWlyZShcIi4uL2NvbmZpZ1wiKTtcblxudmFyIF9fREVWX18gPSBfY29uZmlnLl9fREVWX187XG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgT3JkaW5hbFNjYWxlID0gcmVxdWlyZShcIi4uL3NjYWxlL09yZGluYWxcIik7XG5cbnZhciBJbnRlcnZhbFNjYWxlID0gcmVxdWlyZShcIi4uL3NjYWxlL0ludGVydmFsXCIpO1xuXG52YXIgU2NhbGUgPSByZXF1aXJlKFwiLi4vc2NhbGUvU2NhbGVcIik7XG5cbnZhciBudW1iZXJVdGlsID0gcmVxdWlyZShcIi4uL3V0aWwvbnVtYmVyXCIpO1xuXG52YXIgX2JhckdyaWQgPSByZXF1aXJlKFwiLi4vbGF5b3V0L2JhckdyaWRcIik7XG5cbnZhciBwcmVwYXJlTGF5b3V0QmFyU2VyaWVzID0gX2JhckdyaWQucHJlcGFyZUxheW91dEJhclNlcmllcztcbnZhciBtYWtlQ29sdW1uTGF5b3V0ID0gX2JhckdyaWQubWFrZUNvbHVtbkxheW91dDtcbnZhciByZXRyaWV2ZUNvbHVtbkxheW91dCA9IF9iYXJHcmlkLnJldHJpZXZlQ29sdW1uTGF5b3V0O1xuXG52YXIgQm91bmRpbmdSZWN0ID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvQm91bmRpbmdSZWN0XCIpO1xuXG5yZXF1aXJlKFwiLi4vc2NhbGUvVGltZVwiKTtcblxucmVxdWlyZShcIi4uL3NjYWxlL0xvZ1wiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEdldCBheGlzIHNjYWxlIGV4dGVudCBiZWZvcmUgbmljZWQuXG4gKiBJdGVtIG9mIHJldHVybmVkIGFycmF5IGNhbiBvbmx5IGJlIG51bWJlciAoaW5jbHVkaW5nIEluZmluaXR5IGFuZCBOYU4pLlxuICovXG5mdW5jdGlvbiBnZXRTY2FsZUV4dGVudChzY2FsZSwgbW9kZWwpIHtcbiAgdmFyIHNjYWxlVHlwZSA9IHNjYWxlLnR5cGU7XG4gIHZhciBtaW4gPSBtb2RlbC5nZXRNaW4oKTtcbiAgdmFyIG1heCA9IG1vZGVsLmdldE1heCgpO1xuICB2YXIgZml4TWluID0gbWluICE9IG51bGw7XG4gIHZhciBmaXhNYXggPSBtYXggIT0gbnVsbDtcbiAgdmFyIG9yaWdpbmFsRXh0ZW50ID0gc2NhbGUuZ2V0RXh0ZW50KCk7XG4gIHZhciBheGlzRGF0YUxlbjtcbiAgdmFyIGJvdW5kYXJ5R2FwO1xuICB2YXIgc3BhbjtcblxuICBpZiAoc2NhbGVUeXBlID09PSAnb3JkaW5hbCcpIHtcbiAgICBheGlzRGF0YUxlbiA9IG1vZGVsLmdldENhdGVnb3JpZXMoKS5sZW5ndGg7XG4gIH0gZWxzZSB7XG4gICAgYm91bmRhcnlHYXAgPSBtb2RlbC5nZXQoJ2JvdW5kYXJ5R2FwJyk7XG5cbiAgICBpZiAoIXpyVXRpbC5pc0FycmF5KGJvdW5kYXJ5R2FwKSkge1xuICAgICAgYm91bmRhcnlHYXAgPSBbYm91bmRhcnlHYXAgfHwgMCwgYm91bmRhcnlHYXAgfHwgMF07XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiBib3VuZGFyeUdhcFswXSA9PT0gJ2Jvb2xlYW4nKSB7XG4gICAgICBib3VuZGFyeUdhcCA9IFswLCAwXTtcbiAgICB9XG5cbiAgICBib3VuZGFyeUdhcFswXSA9IG51bWJlclV0aWwucGFyc2VQZXJjZW50KGJvdW5kYXJ5R2FwWzBdLCAxKTtcbiAgICBib3VuZGFyeUdhcFsxXSA9IG51bWJlclV0aWwucGFyc2VQZXJjZW50KGJvdW5kYXJ5R2FwWzFdLCAxKTtcbiAgICBzcGFuID0gb3JpZ2luYWxFeHRlbnRbMV0gLSBvcmlnaW5hbEV4dGVudFswXSB8fCBNYXRoLmFicyhvcmlnaW5hbEV4dGVudFswXSk7XG4gIH0gLy8gTm90aWNlOiBXaGVuIG1pbi9tYXggaXMgbm90IHNldCAodGhhdCBpcywgd2hlbiB0aGVyZSBhcmUgbnVsbC91bmRlZmluZWQsXG4gIC8vIHdoaWNoIGlzIHRoZSBtb3N0IGNvbW1vbiBjYXNlKSwgdGhlc2UgY2FzZXMgc2hvdWxkIGJlIGVuc3VyZWQ6XG4gIC8vICgxKSBGb3IgJ29yZGluYWwnLCBzaG93IGFsbCBheGlzLmRhdGEuXG4gIC8vICgyKSBGb3Igb3RoZXJzOlxuICAvLyAgICAgICsgYGJvdW5kYXJ5R2FwYCBpcyBhcHBsaWVkIChpZiBtaW4vbWF4IHNldCwgYm91bmRhcnlHYXAgaXNcbiAgLy8gICAgICBkaXNhYmxlZCkuXG4gIC8vICAgICAgKyBJZiBgbmVlZENyb3NzWmVyb2AsIG1pbi9tYXggc2hvdWxkIGJlIHplcm8sIG90aGVyd2lzZSwgbWluL21heCBzaG91bGRcbiAgLy8gICAgICBiZSB0aGUgcmVzdWx0IHRoYXQgb3JpZ2luYWxFeHRlbnQgZW5sYXJnZWQgYnkgYm91bmRhcnlHYXAuXG4gIC8vICgzKSBJZiBubyBkYXRhLCBpdCBzaG91bGQgYmUgZW5zdXJlZCB0aGF0IGBzY2FsZS5zZXRCbGFua2AgaXMgc2V0LlxuICAvLyBGSVhNRVxuICAvLyAoMSkgV2hlbiBtaW4vbWF4IGlzICdkYXRhTWluJyBvciAnZGF0YU1heCcsIHNob3VsZCBib3VuZGFyeUdhcCBiZSBhYmxlIHRvIHVzZWQ/XG4gIC8vICgyKSBXaGVuIGBuZWVkQ3Jvc3NaZXJvYCBhbmQgYWxsIGRhdGEgaXMgcG9zaXRpdmUvbmVnYXRpdmUsIHNob3VsZCBpdCBiZSBlbnN1cmVkXG4gIC8vIHRoYXQgdGhlIHJlc3VsdHMgcHJvY2Vzc2VkIGJ5IGJvdW5kYXJ5R2FwIGFyZSBwb3NpdGl2ZS9uZWdhdGl2ZT9cblxuXG4gIGlmIChtaW4gPT0gbnVsbCkge1xuICAgIG1pbiA9IHNjYWxlVHlwZSA9PT0gJ29yZGluYWwnID8gYXhpc0RhdGFMZW4gPyAwIDogTmFOIDogb3JpZ2luYWxFeHRlbnRbMF0gLSBib3VuZGFyeUdhcFswXSAqIHNwYW47XG4gIH1cblxuICBpZiAobWF4ID09IG51bGwpIHtcbiAgICBtYXggPSBzY2FsZVR5cGUgPT09ICdvcmRpbmFsJyA/IGF4aXNEYXRhTGVuID8gYXhpc0RhdGFMZW4gLSAxIDogTmFOIDogb3JpZ2luYWxFeHRlbnRbMV0gKyBib3VuZGFyeUdhcFsxXSAqIHNwYW47XG4gIH1cblxuICBpZiAobWluID09PSAnZGF0YU1pbicpIHtcbiAgICBtaW4gPSBvcmlnaW5hbEV4dGVudFswXTtcbiAgfSBlbHNlIGlmICh0eXBlb2YgbWluID09PSAnZnVuY3Rpb24nKSB7XG4gICAgbWluID0gbWluKHtcbiAgICAgIG1pbjogb3JpZ2luYWxFeHRlbnRbMF0sXG4gICAgICBtYXg6IG9yaWdpbmFsRXh0ZW50WzFdXG4gICAgfSk7XG4gIH1cblxuICBpZiAobWF4ID09PSAnZGF0YU1heCcpIHtcbiAgICBtYXggPSBvcmlnaW5hbEV4dGVudFsxXTtcbiAgfSBlbHNlIGlmICh0eXBlb2YgbWF4ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgbWF4ID0gbWF4KHtcbiAgICAgIG1pbjogb3JpZ2luYWxFeHRlbnRbMF0sXG4gICAgICBtYXg6IG9yaWdpbmFsRXh0ZW50WzFdXG4gICAgfSk7XG4gIH1cblxuICAobWluID09IG51bGwgfHwgIWlzRmluaXRlKG1pbikpICYmIChtaW4gPSBOYU4pO1xuICAobWF4ID09IG51bGwgfHwgIWlzRmluaXRlKG1heCkpICYmIChtYXggPSBOYU4pO1xuICBzY2FsZS5zZXRCbGFuayh6clV0aWwuZXFOYU4obWluKSB8fCB6clV0aWwuZXFOYU4obWF4KSB8fCBzY2FsZVR5cGUgPT09ICdvcmRpbmFsJyAmJiAhc2NhbGUuZ2V0T3JkaW5hbE1ldGEoKS5jYXRlZ29yaWVzLmxlbmd0aCk7IC8vIEV2YWx1YXRlIGlmIGF4aXMgbmVlZHMgY3Jvc3MgemVyb1xuXG4gIGlmIChtb2RlbC5nZXROZWVkQ3Jvc3NaZXJvKCkpIHtcbiAgICAvLyBBeGlzIGlzIG92ZXIgemVybyBhbmQgbWluIGlzIG5vdCBzZXRcbiAgICBpZiAobWluID4gMCAmJiBtYXggPiAwICYmICFmaXhNaW4pIHtcbiAgICAgIG1pbiA9IDA7XG4gICAgfSAvLyBBeGlzIGlzIHVuZGVyIHplcm8gYW5kIG1heCBpcyBub3Qgc2V0XG5cblxuICAgIGlmIChtaW4gPCAwICYmIG1heCA8IDAgJiYgIWZpeE1heCkge1xuICAgICAgbWF4ID0gMDtcbiAgICB9XG4gIH0gLy8gSWYgYmFycyBhcmUgcGxhY2VkIG9uIGEgYmFzZSBheGlzIG9mIHR5cGUgdGltZSBvciBpbnRlcnZhbCBhY2NvdW50IGZvciBheGlzIGJvdW5kYXJ5IG92ZXJmbG93IGFuZCBjdXJyZW50IGF4aXNcbiAgLy8gaXMgYmFzZSBheGlzXG4gIC8vIEZJWE1FXG4gIC8vICgxKSBDb25zaWRlciBzdXBwb3J0IHZhbHVlIGF4aXMsIHdoZXJlIGJlbG93IHplcm8gYW5kIGF4aXMgYG9uWmVyb2Agc2hvdWxkIGJlIGhhbmRsZWQgcHJvcGVybHkuXG4gIC8vICgyKSBSZWZhY3RvciB0aGUgbG9naWMgd2l0aCBgYmFyR3JpZGAuIElzIGl0IG5vdCBuZWVkIHRvIGBtYWtlQmFyV2lkdGhBbmRPZmZzZXRJbmZvYCB0d2ljZSB3aXRoIGRpZmZlcmVudCBleHRlbnQ/XG4gIC8vICAgICBTaG91bGQgbm90IGRlcGVuZCBvbiBzZXJpZXMgdHlwZSBgYmFyYD9cbiAgLy8gKDMpIEZpeCB0aGF0IG1pZ2h0IG92ZXJsYXAgd2hlbiB1c2luZyBkYXRhWm9vbS5cbiAgLy8gKDQpIENvbnNpZGVyIG90aGVyIGNoYXJ0IHR5cGVzIHVzaW5nIGBiYXJHcmlkYD9cbiAgLy8gU2VlICM2NzI4LCAjNDg2MiwgYHRlc3QvYmFyLW92ZXJmbG93LXRpbWUtcGxvdC5odG1sYFxuXG5cbiAgdmFyIGVjTW9kZWwgPSBtb2RlbC5lY01vZGVsO1xuXG4gIGlmIChlY01vZGVsICYmIHNjYWxlVHlwZSA9PT0gJ3RpbWUnXG4gIC8qfHwgc2NhbGVUeXBlID09PSAnaW50ZXJ2YWwnICovXG4gICkge1xuICAgIHZhciBiYXJTZXJpZXNNb2RlbHMgPSBwcmVwYXJlTGF5b3V0QmFyU2VyaWVzKCdiYXInLCBlY01vZGVsKTtcbiAgICB2YXIgaXNCYXNlQXhpc0FuZEhhc0JhclNlcmllcztcbiAgICB6clV0aWwuZWFjaChiYXJTZXJpZXNNb2RlbHMsIGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgICAgaXNCYXNlQXhpc0FuZEhhc0JhclNlcmllcyB8PSBzZXJpZXNNb2RlbC5nZXRCYXNlQXhpcygpID09PSBtb2RlbC5heGlzO1xuICAgIH0pO1xuXG4gICAgaWYgKGlzQmFzZUF4aXNBbmRIYXNCYXJTZXJpZXMpIHtcbiAgICAgIC8vIENhbGN1bGF0ZSBwbGFjZW1lbnQgb2YgYmFycyBvbiBheGlzXG4gICAgICB2YXIgYmFyV2lkdGhBbmRPZmZzZXQgPSBtYWtlQ29sdW1uTGF5b3V0KGJhclNlcmllc01vZGVscyk7IC8vIEFkanVzdCBheGlzIG1pbiBhbmQgbWF4IHRvIGFjY291bnQgZm9yIG92ZXJmbG93XG5cbiAgICAgIHZhciBhZGp1c3RlZFNjYWxlID0gYWRqdXN0U2NhbGVGb3JPdmVyZmxvdyhtaW4sIG1heCwgbW9kZWwsIGJhcldpZHRoQW5kT2Zmc2V0KTtcbiAgICAgIG1pbiA9IGFkanVzdGVkU2NhbGUubWluO1xuICAgICAgbWF4ID0gYWRqdXN0ZWRTY2FsZS5tYXg7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIFttaW4sIG1heF07XG59XG5cbmZ1bmN0aW9uIGFkanVzdFNjYWxlRm9yT3ZlcmZsb3cobWluLCBtYXgsIG1vZGVsLCBiYXJXaWR0aEFuZE9mZnNldCkge1xuICAvLyBHZXQgQXhpcyBMZW5ndGhcbiAgdmFyIGF4aXNFeHRlbnQgPSBtb2RlbC5heGlzLmdldEV4dGVudCgpO1xuICB2YXIgYXhpc0xlbmd0aCA9IGF4aXNFeHRlbnRbMV0gLSBheGlzRXh0ZW50WzBdOyAvLyBHZXQgYmFycyBvbiBjdXJyZW50IGJhc2UgYXhpcyBhbmQgY2FsY3VsYXRlIG1pbiBhbmQgbWF4IG92ZXJmbG93XG5cbiAgdmFyIGJhcnNPbkN1cnJlbnRBeGlzID0gcmV0cmlldmVDb2x1bW5MYXlvdXQoYmFyV2lkdGhBbmRPZmZzZXQsIG1vZGVsLmF4aXMpO1xuXG4gIGlmIChiYXJzT25DdXJyZW50QXhpcyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG1pbjogbWluLFxuICAgICAgbWF4OiBtYXhcbiAgICB9O1xuICB9XG5cbiAgdmFyIG1pbk92ZXJmbG93ID0gSW5maW5pdHk7XG4gIHpyVXRpbC5lYWNoKGJhcnNPbkN1cnJlbnRBeGlzLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgIG1pbk92ZXJmbG93ID0gTWF0aC5taW4oaXRlbS5vZmZzZXQsIG1pbk92ZXJmbG93KTtcbiAgfSk7XG4gIHZhciBtYXhPdmVyZmxvdyA9IC1JbmZpbml0eTtcbiAgenJVdGlsLmVhY2goYmFyc09uQ3VycmVudEF4aXMsIGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgbWF4T3ZlcmZsb3cgPSBNYXRoLm1heChpdGVtLm9mZnNldCArIGl0ZW0ud2lkdGgsIG1heE92ZXJmbG93KTtcbiAgfSk7XG4gIG1pbk92ZXJmbG93ID0gTWF0aC5hYnMobWluT3ZlcmZsb3cpO1xuICBtYXhPdmVyZmxvdyA9IE1hdGguYWJzKG1heE92ZXJmbG93KTtcbiAgdmFyIHRvdGFsT3ZlckZsb3cgPSBtaW5PdmVyZmxvdyArIG1heE92ZXJmbG93OyAvLyBDYWx1bGF0ZSByZXF1aXJlZCBidWZmZXIgYmFzZWQgb24gb2xkIHJhbmdlIGFuZCBvdmVyZmxvd1xuXG4gIHZhciBvbGRSYW5nZSA9IG1heCAtIG1pbjtcbiAgdmFyIG9sZFJhbmdlUGVyY2VudE9mTmV3ID0gMSAtIChtaW5PdmVyZmxvdyArIG1heE92ZXJmbG93KSAvIGF4aXNMZW5ndGg7XG4gIHZhciBvdmVyZmxvd0J1ZmZlciA9IG9sZFJhbmdlIC8gb2xkUmFuZ2VQZXJjZW50T2ZOZXcgLSBvbGRSYW5nZTtcbiAgbWF4ICs9IG92ZXJmbG93QnVmZmVyICogKG1heE92ZXJmbG93IC8gdG90YWxPdmVyRmxvdyk7XG4gIG1pbiAtPSBvdmVyZmxvd0J1ZmZlciAqIChtaW5PdmVyZmxvdyAvIHRvdGFsT3ZlckZsb3cpO1xuICByZXR1cm4ge1xuICAgIG1pbjogbWluLFxuICAgIG1heDogbWF4XG4gIH07XG59XG5cbmZ1bmN0aW9uIG5pY2VTY2FsZUV4dGVudChzY2FsZSwgbW9kZWwpIHtcbiAgdmFyIGV4dGVudCA9IGdldFNjYWxlRXh0ZW50KHNjYWxlLCBtb2RlbCk7XG4gIHZhciBmaXhNaW4gPSBtb2RlbC5nZXRNaW4oKSAhPSBudWxsO1xuICB2YXIgZml4TWF4ID0gbW9kZWwuZ2V0TWF4KCkgIT0gbnVsbDtcbiAgdmFyIHNwbGl0TnVtYmVyID0gbW9kZWwuZ2V0KCdzcGxpdE51bWJlcicpO1xuXG4gIGlmIChzY2FsZS50eXBlID09PSAnbG9nJykge1xuICAgIHNjYWxlLmJhc2UgPSBtb2RlbC5nZXQoJ2xvZ0Jhc2UnKTtcbiAgfVxuXG4gIHZhciBzY2FsZVR5cGUgPSBzY2FsZS50eXBlO1xuICBzY2FsZS5zZXRFeHRlbnQoZXh0ZW50WzBdLCBleHRlbnRbMV0pO1xuICBzY2FsZS5uaWNlRXh0ZW50KHtcbiAgICBzcGxpdE51bWJlcjogc3BsaXROdW1iZXIsXG4gICAgZml4TWluOiBmaXhNaW4sXG4gICAgZml4TWF4OiBmaXhNYXgsXG4gICAgbWluSW50ZXJ2YWw6IHNjYWxlVHlwZSA9PT0gJ2ludGVydmFsJyB8fCBzY2FsZVR5cGUgPT09ICd0aW1lJyA/IG1vZGVsLmdldCgnbWluSW50ZXJ2YWwnKSA6IG51bGwsXG4gICAgbWF4SW50ZXJ2YWw6IHNjYWxlVHlwZSA9PT0gJ2ludGVydmFsJyB8fCBzY2FsZVR5cGUgPT09ICd0aW1lJyA/IG1vZGVsLmdldCgnbWF4SW50ZXJ2YWwnKSA6IG51bGxcbiAgfSk7IC8vIElmIHNvbWUgb25lIHNwZWNpZmllZCB0aGUgbWluLCBtYXguIEFuZCB0aGUgZGVmYXVsdCBjYWxjdWxhdGVkIGludGVydmFsXG4gIC8vIGlzIG5vdCBnb29kIGVub3VnaC4gSGUgY2FuIHNwZWNpZnkgdGhlIGludGVydmFsLiBJdCBpcyBvZnRlbiBhcHBlYXJlZFxuICAvLyBpbiBhbmdsZSBheGlzIHdpdGggYW5nbGUgMCAtIDM2MC4gSW50ZXJ2YWwgY2FsY3VsYXRlZCBpbiBpbnRlcnZhbCBzY2FsZSBpcyBoYXJkXG4gIC8vIHRvIGJlIDYwLlxuICAvLyBGSVhNRVxuXG4gIHZhciBpbnRlcnZhbCA9IG1vZGVsLmdldCgnaW50ZXJ2YWwnKTtcblxuICBpZiAoaW50ZXJ2YWwgIT0gbnVsbCkge1xuICAgIHNjYWxlLnNldEludGVydmFsICYmIHNjYWxlLnNldEludGVydmFsKGludGVydmFsKTtcbiAgfVxufVxuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBtb2RlbFxuICogQHBhcmFtIHtzdHJpbmd9IFtheGlzVHlwZV0gRGVmYXVsdCByZXRyaWV2ZSBmcm9tIG1vZGVsLnR5cGVcbiAqIEByZXR1cm4ge21vZHVsZTplY2hhcnRzL3NjYWxlLyp9XG4gKi9cblxuXG5mdW5jdGlvbiBjcmVhdGVTY2FsZUJ5TW9kZWwobW9kZWwsIGF4aXNUeXBlKSB7XG4gIGF4aXNUeXBlID0gYXhpc1R5cGUgfHwgbW9kZWwuZ2V0KCd0eXBlJyk7XG5cbiAgaWYgKGF4aXNUeXBlKSB7XG4gICAgc3dpdGNoIChheGlzVHlwZSkge1xuICAgICAgLy8gQnVpbGRpbiBzY2FsZVxuICAgICAgY2FzZSAnY2F0ZWdvcnknOlxuICAgICAgICByZXR1cm4gbmV3IE9yZGluYWxTY2FsZShtb2RlbC5nZXRPcmRpbmFsTWV0YSA/IG1vZGVsLmdldE9yZGluYWxNZXRhKCkgOiBtb2RlbC5nZXRDYXRlZ29yaWVzKCksIFtJbmZpbml0eSwgLUluZmluaXR5XSk7XG5cbiAgICAgIGNhc2UgJ3ZhbHVlJzpcbiAgICAgICAgcmV0dXJuIG5ldyBJbnRlcnZhbFNjYWxlKCk7XG4gICAgICAvLyBFeHRlbmRlZCBzY2FsZSwgbGlrZSB0aW1lIGFuZCBsb2dcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIChTY2FsZS5nZXRDbGFzcyhheGlzVHlwZSkgfHwgSW50ZXJ2YWxTY2FsZSkuY3JlYXRlKG1vZGVsKTtcbiAgICB9XG4gIH1cbn1cbi8qKlxuICogQ2hlY2sgaWYgdGhlIGF4aXMgY29yc3MgMFxuICovXG5cblxuZnVuY3Rpb24gaWZBeGlzQ3Jvc3NaZXJvKGF4aXMpIHtcbiAgdmFyIGRhdGFFeHRlbnQgPSBheGlzLnNjYWxlLmdldEV4dGVudCgpO1xuICB2YXIgbWluID0gZGF0YUV4dGVudFswXTtcbiAgdmFyIG1heCA9IGRhdGFFeHRlbnRbMV07XG4gIHJldHVybiAhKG1pbiA+IDAgJiYgbWF4ID4gMCB8fCBtaW4gPCAwICYmIG1heCA8IDApO1xufVxuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2Nvb3JkL0F4aXN9IGF4aXNcbiAqIEByZXR1cm4ge0Z1bmN0aW9ufSBMYWJlbCBmb3JtYXR0ZXIgZnVuY3Rpb24uXG4gKiAgICAgICAgIHBhcmFtOiB7bnVtYmVyfSB0aWNrVmFsdWUsXG4gKiAgICAgICAgIHBhcmFtOiB7bnVtYmVyfSBpZHgsIHRoZSBpbmRleCBpbiBhbGwgdGlja3MuXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICBJZiBjYXRlZ29yeSBheGlzLCB0aGlzIHBhcmFtIGlzIG5vdCByZXF1aWVkLlxuICogICAgICAgICByZXR1cm46IHtzdHJpbmd9IGxhYmVsIHN0cmluZy5cbiAqL1xuXG5cbmZ1bmN0aW9uIG1ha2VMYWJlbEZvcm1hdHRlcihheGlzKSB7XG4gIHZhciBsYWJlbEZvcm1hdHRlciA9IGF4aXMuZ2V0TGFiZWxNb2RlbCgpLmdldCgnZm9ybWF0dGVyJyk7XG4gIHZhciBjYXRlZ29yeVRpY2tTdGFydCA9IGF4aXMudHlwZSA9PT0gJ2NhdGVnb3J5JyA/IGF4aXMuc2NhbGUuZ2V0RXh0ZW50KClbMF0gOiBudWxsO1xuXG4gIGlmICh0eXBlb2YgbGFiZWxGb3JtYXR0ZXIgPT09ICdzdHJpbmcnKSB7XG4gICAgbGFiZWxGb3JtYXR0ZXIgPSBmdW5jdGlvbiAodHBsKSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKHZhbCkge1xuICAgICAgICAvLyBGb3IgY2F0ZWdvcnkgYXhpcywgZ2V0IHJhdyB2YWx1ZTsgZm9yIG51bWVyaWMgYXhpcyxcbiAgICAgICAgLy8gZ2V0IGZvcmFtdHRlZCBsYWJlbCBsaWtlICcxLDMzMyw0NDQnLlxuICAgICAgICB2YWwgPSBheGlzLnNjYWxlLmdldExhYmVsKHZhbCk7XG4gICAgICAgIHJldHVybiB0cGwucmVwbGFjZSgne3ZhbHVlfScsIHZhbCAhPSBudWxsID8gdmFsIDogJycpO1xuICAgICAgfTtcbiAgICB9KGxhYmVsRm9ybWF0dGVyKTsgLy8gQ29uc2lkZXIgZW1wdHkgYXJyYXlcblxuXG4gICAgcmV0dXJuIGxhYmVsRm9ybWF0dGVyO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBsYWJlbEZvcm1hdHRlciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIHJldHVybiBmdW5jdGlvbiAodGlja1ZhbHVlLCBpZHgpIHtcbiAgICAgIC8vIFRoZSBvcmlnaW5hbCBpbnRlbnRpb24gb2YgYGlkeGAgaXMgXCJ0aGUgaW5kZXggb2YgdGhlIHRpY2sgaW4gYWxsIHRpY2tzXCIuXG4gICAgICAvLyBCdXQgdGhlIHByZXZpb3VzIGltcGxlbWVudGF0aW9uIG9mIGNhdGVnb3J5IGF4aXMgZG8gbm90IGNvbnNpZGVyIHRoZVxuICAgICAgLy8gYGF4aXNMYWJlbC5pbnRlcnZhbGAsIHdoaWNoIGNhdXNlIHRoYXQsIGZvciBleGFtcGxlLCB0aGUgYGludGVydmFsYCBpc1xuICAgICAgLy8gYDFgLCB0aGVuIHRoZSB0aWNrcyBcIm5hbWU1XCIsIFwibmFtZTdcIiwgXCJuYW1lOVwiIGFyZSBkaXNwbGF5ZWQsIHdoZXJlIHRoZVxuICAgICAgLy8gY29ycmVzcG9uZGluZyBgaWR4YCBhcmUgYDBgLCBgMmAsIGA0YCwgYnV0IG5vdCBgMGAsIGAxYCwgYDJgLiBTbyB3ZSBrZWVwXG4gICAgICAvLyB0aGUgZGVmaW5pdGlvbiBoZXJlIGZvciBiYWNrIGNvbXBhdGliaWxpdHkuXG4gICAgICBpZiAoY2F0ZWdvcnlUaWNrU3RhcnQgIT0gbnVsbCkge1xuICAgICAgICBpZHggPSB0aWNrVmFsdWUgLSBjYXRlZ29yeVRpY2tTdGFydDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGxhYmVsRm9ybWF0dGVyKGdldEF4aXNSYXdWYWx1ZShheGlzLCB0aWNrVmFsdWUpLCBpZHgpO1xuICAgIH07XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0aWNrKSB7XG4gICAgICByZXR1cm4gYXhpcy5zY2FsZS5nZXRMYWJlbCh0aWNrKTtcbiAgICB9O1xuICB9XG59XG5cbmZ1bmN0aW9uIGdldEF4aXNSYXdWYWx1ZShheGlzLCB2YWx1ZSkge1xuICAvLyBJbiBjYXRlZ29yeSBheGlzIHdpdGggZGF0YSB6b29tLCB0aWNrIGlzIG5vdCB0aGUgb3JpZ2luYWxcbiAgLy8gaW5kZXggb2YgYXhpcy5kYXRhLiBTbyB0aWNrIHNob3VsZCBub3QgYmUgZXhwb3NlZCB0byB1c2VyXG4gIC8vIGluIGNhdGVnb3J5IGF4aXMuXG4gIHJldHVybiBheGlzLnR5cGUgPT09ICdjYXRlZ29yeScgPyBheGlzLnNjYWxlLmdldExhYmVsKHZhbHVlKSA6IHZhbHVlO1xufVxuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2Nvb3JkL0F4aXN9IGF4aXNcbiAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyL2NvcmUvQm91bmRpbmdSZWN0fSBCZSBudWxsL3VuZGVmaW5lZCBpZiBubyBsYWJlbHMuXG4gKi9cblxuXG5mdW5jdGlvbiBlc3RpbWF0ZUxhYmVsVW5pb25SZWN0KGF4aXMpIHtcbiAgdmFyIGF4aXNNb2RlbCA9IGF4aXMubW9kZWw7XG4gIHZhciBzY2FsZSA9IGF4aXMuc2NhbGU7XG5cbiAgaWYgKCFheGlzTW9kZWwuZ2V0KCdheGlzTGFiZWwuc2hvdycpIHx8IHNjYWxlLmlzQmxhbmsoKSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBpc0NhdGVnb3J5ID0gYXhpcy50eXBlID09PSAnY2F0ZWdvcnknO1xuICB2YXIgcmVhbE51bWJlclNjYWxlVGlja3M7XG4gIHZhciB0aWNrQ291bnQ7XG4gIHZhciBjYXRlZ29yeVNjYWxlRXh0ZW50ID0gc2NhbGUuZ2V0RXh0ZW50KCk7IC8vIE9wdGltaXplIGZvciBsYXJnZSBjYXRlZ29yeSBkYXRhLCBhdm9pZCBjYWxsIGBnZXRUaWNrcygpYC5cblxuICBpZiAoaXNDYXRlZ29yeSkge1xuICAgIHRpY2tDb3VudCA9IHNjYWxlLmNvdW50KCk7XG4gIH0gZWxzZSB7XG4gICAgcmVhbE51bWJlclNjYWxlVGlja3MgPSBzY2FsZS5nZXRUaWNrcygpO1xuICAgIHRpY2tDb3VudCA9IHJlYWxOdW1iZXJTY2FsZVRpY2tzLmxlbmd0aDtcbiAgfVxuXG4gIHZhciBheGlzTGFiZWxNb2RlbCA9IGF4aXMuZ2V0TGFiZWxNb2RlbCgpO1xuICB2YXIgbGFiZWxGb3JtYXR0ZXIgPSBtYWtlTGFiZWxGb3JtYXR0ZXIoYXhpcyk7XG4gIHZhciByZWN0O1xuICB2YXIgc3RlcCA9IDE7IC8vIFNpbXBsZSBvcHRpbWl6YXRpb24gZm9yIGxhcmdlIGFtb3VudCBvZiBsYWJlbHNcblxuICBpZiAodGlja0NvdW50ID4gNDApIHtcbiAgICBzdGVwID0gTWF0aC5jZWlsKHRpY2tDb3VudCAvIDQwKTtcbiAgfVxuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgdGlja0NvdW50OyBpICs9IHN0ZXApIHtcbiAgICB2YXIgdGlja1ZhbHVlID0gcmVhbE51bWJlclNjYWxlVGlja3MgPyByZWFsTnVtYmVyU2NhbGVUaWNrc1tpXSA6IGNhdGVnb3J5U2NhbGVFeHRlbnRbMF0gKyBpO1xuICAgIHZhciBsYWJlbCA9IGxhYmVsRm9ybWF0dGVyKHRpY2tWYWx1ZSk7XG4gICAgdmFyIHVucm90YXRlZFNpbmdsZVJlY3QgPSBheGlzTGFiZWxNb2RlbC5nZXRUZXh0UmVjdChsYWJlbCk7XG4gICAgdmFyIHNpbmdsZVJlY3QgPSByb3RhdGVUZXh0UmVjdCh1bnJvdGF0ZWRTaW5nbGVSZWN0LCBheGlzTGFiZWxNb2RlbC5nZXQoJ3JvdGF0ZScpIHx8IDApO1xuICAgIHJlY3QgPyByZWN0LnVuaW9uKHNpbmdsZVJlY3QpIDogcmVjdCA9IHNpbmdsZVJlY3Q7XG4gIH1cblxuICByZXR1cm4gcmVjdDtcbn1cblxuZnVuY3Rpb24gcm90YXRlVGV4dFJlY3QodGV4dFJlY3QsIHJvdGF0ZSkge1xuICB2YXIgcm90YXRlUmFkaWFucyA9IHJvdGF0ZSAqIE1hdGguUEkgLyAxODA7XG4gIHZhciBib3VuZGluZ0JveCA9IHRleHRSZWN0LnBsYWluKCk7XG4gIHZhciBiZWZvcmVXaWR0aCA9IGJvdW5kaW5nQm94LndpZHRoO1xuICB2YXIgYmVmb3JlSGVpZ2h0ID0gYm91bmRpbmdCb3guaGVpZ2h0O1xuICB2YXIgYWZ0ZXJXaWR0aCA9IGJlZm9yZVdpZHRoICogTWF0aC5jb3Mocm90YXRlUmFkaWFucykgKyBiZWZvcmVIZWlnaHQgKiBNYXRoLnNpbihyb3RhdGVSYWRpYW5zKTtcbiAgdmFyIGFmdGVySGVpZ2h0ID0gYmVmb3JlV2lkdGggKiBNYXRoLnNpbihyb3RhdGVSYWRpYW5zKSArIGJlZm9yZUhlaWdodCAqIE1hdGguY29zKHJvdGF0ZVJhZGlhbnMpO1xuICB2YXIgcm90YXRlZFJlY3QgPSBuZXcgQm91bmRpbmdSZWN0KGJvdW5kaW5nQm94LngsIGJvdW5kaW5nQm94LnksIGFmdGVyV2lkdGgsIGFmdGVySGVpZ2h0KTtcbiAgcmV0dXJuIHJvdGF0ZWRSZWN0O1xufVxuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL3NyYy9tb2RlbC9Nb2RlbH0gbW9kZWwgYXhpc0xhYmVsTW9kZWwgb3IgYXhpc1RpY2tNb2RlbFxuICogQHJldHVybiB7bnVtYmVyfFN0cmluZ30gQ2FuIGJlIG51bGx8J2F1dG8nfG51bWJlcnxmdW5jdGlvblxuICovXG5cblxuZnVuY3Rpb24gZ2V0T3B0aW9uQ2F0ZWdvcnlJbnRlcnZhbChtb2RlbCkge1xuICB2YXIgaW50ZXJ2YWwgPSBtb2RlbC5nZXQoJ2ludGVydmFsJyk7XG4gIHJldHVybiBpbnRlcnZhbCA9PSBudWxsID8gJ2F1dG8nIDogaW50ZXJ2YWw7XG59XG4vKipcbiAqIFNldCBgY2F0ZWdvcnlJbnRlcnZhbGAgYXMgMCBpbXBsaWNpdGx5IGluZGljYXRlcyB0aGF0XG4gKiBzaG93IGFsbCBsYWJlbHMgcmVndWFyZGxlc3Mgb2Ygb3ZlcmxhcC5cbiAqIEBwYXJhbSB7T2JqZWN0fSBheGlzIGF4aXNNb2RlbC5heGlzXG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5cblxuZnVuY3Rpb24gc2hvdWxkU2hvd0FsbExhYmVscyhheGlzKSB7XG4gIHJldHVybiBheGlzLnR5cGUgPT09ICdjYXRlZ29yeScgJiYgZ2V0T3B0aW9uQ2F0ZWdvcnlJbnRlcnZhbChheGlzLmdldExhYmVsTW9kZWwoKSkgPT09IDA7XG59XG5cbmV4cG9ydHMuZ2V0U2NhbGVFeHRlbnQgPSBnZXRTY2FsZUV4dGVudDtcbmV4cG9ydHMubmljZVNjYWxlRXh0ZW50ID0gbmljZVNjYWxlRXh0ZW50O1xuZXhwb3J0cy5jcmVhdGVTY2FsZUJ5TW9kZWwgPSBjcmVhdGVTY2FsZUJ5TW9kZWw7XG5leHBvcnRzLmlmQXhpc0Nyb3NzWmVybyA9IGlmQXhpc0Nyb3NzWmVybztcbmV4cG9ydHMubWFrZUxhYmVsRm9ybWF0dGVyID0gbWFrZUxhYmVsRm9ybWF0dGVyO1xuZXhwb3J0cy5nZXRBeGlzUmF3VmFsdWUgPSBnZXRBeGlzUmF3VmFsdWU7XG5leHBvcnRzLmVzdGltYXRlTGFiZWxVbmlvblJlY3QgPSBlc3RpbWF0ZUxhYmVsVW5pb25SZWN0O1xuZXhwb3J0cy5nZXRPcHRpb25DYXRlZ29yeUludGVydmFsID0gZ2V0T3B0aW9uQ2F0ZWdvcnlJbnRlcnZhbDtcbmV4cG9ydHMuc2hvdWxkU2hvd0FsbExhYmVscyA9IHNob3VsZFNob3dBbGxMYWJlbHM7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/axisHelper.js
