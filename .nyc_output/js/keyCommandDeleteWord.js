/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule keyCommandDeleteWord
 * @format
 * 
 */


var DraftRemovableWord = __webpack_require__(/*! ./DraftRemovableWord */ "./node_modules/draft-js/lib/DraftRemovableWord.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var moveSelectionForward = __webpack_require__(/*! ./moveSelectionForward */ "./node_modules/draft-js/lib/moveSelectionForward.js");

var removeTextWithStrategy = __webpack_require__(/*! ./removeTextWithStrategy */ "./node_modules/draft-js/lib/removeTextWithStrategy.js");
/**
 * Delete the word that is right of the cursor, as well as any spaces or
 * punctuation before the word.
 */


function keyCommandDeleteWord(editorState) {
  var afterRemoval = removeTextWithStrategy(editorState, function (strategyState) {
    var selection = strategyState.getSelection();
    var offset = selection.getStartOffset();
    var key = selection.getStartKey();
    var content = strategyState.getCurrentContent();
    var text = content.getBlockForKey(key).getText().slice(offset);
    var toRemove = DraftRemovableWord.getForward(text); // If there are no words in front of the cursor, remove the newline.

    return moveSelectionForward(strategyState, toRemove.length || 1);
  }, 'forward');

  if (afterRemoval === editorState.getCurrentContent()) {
    return editorState;
  }

  return EditorState.push(editorState, afterRemoval, 'remove-range');
}

module.exports = keyCommandDeleteWord;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmREZWxldGVXb3JkLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmREZWxldGVXb3JkLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUga2V5Q29tbWFuZERlbGV0ZVdvcmRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0UmVtb3ZhYmxlV29yZCA9IHJlcXVpcmUoJy4vRHJhZnRSZW1vdmFibGVXb3JkJyk7XG52YXIgRWRpdG9yU3RhdGUgPSByZXF1aXJlKCcuL0VkaXRvclN0YXRlJyk7XG5cbnZhciBtb3ZlU2VsZWN0aW9uRm9yd2FyZCA9IHJlcXVpcmUoJy4vbW92ZVNlbGVjdGlvbkZvcndhcmQnKTtcbnZhciByZW1vdmVUZXh0V2l0aFN0cmF0ZWd5ID0gcmVxdWlyZSgnLi9yZW1vdmVUZXh0V2l0aFN0cmF0ZWd5Jyk7XG5cbi8qKlxuICogRGVsZXRlIHRoZSB3b3JkIHRoYXQgaXMgcmlnaHQgb2YgdGhlIGN1cnNvciwgYXMgd2VsbCBhcyBhbnkgc3BhY2VzIG9yXG4gKiBwdW5jdHVhdGlvbiBiZWZvcmUgdGhlIHdvcmQuXG4gKi9cbmZ1bmN0aW9uIGtleUNvbW1hbmREZWxldGVXb3JkKGVkaXRvclN0YXRlKSB7XG4gIHZhciBhZnRlclJlbW92YWwgPSByZW1vdmVUZXh0V2l0aFN0cmF0ZWd5KGVkaXRvclN0YXRlLCBmdW5jdGlvbiAoc3RyYXRlZ3lTdGF0ZSkge1xuICAgIHZhciBzZWxlY3Rpb24gPSBzdHJhdGVneVN0YXRlLmdldFNlbGVjdGlvbigpO1xuICAgIHZhciBvZmZzZXQgPSBzZWxlY3Rpb24uZ2V0U3RhcnRPZmZzZXQoKTtcbiAgICB2YXIga2V5ID0gc2VsZWN0aW9uLmdldFN0YXJ0S2V5KCk7XG4gICAgdmFyIGNvbnRlbnQgPSBzdHJhdGVneVN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdmFyIHRleHQgPSBjb250ZW50LmdldEJsb2NrRm9yS2V5KGtleSkuZ2V0VGV4dCgpLnNsaWNlKG9mZnNldCk7XG4gICAgdmFyIHRvUmVtb3ZlID0gRHJhZnRSZW1vdmFibGVXb3JkLmdldEZvcndhcmQodGV4dCk7XG5cbiAgICAvLyBJZiB0aGVyZSBhcmUgbm8gd29yZHMgaW4gZnJvbnQgb2YgdGhlIGN1cnNvciwgcmVtb3ZlIHRoZSBuZXdsaW5lLlxuICAgIHJldHVybiBtb3ZlU2VsZWN0aW9uRm9yd2FyZChzdHJhdGVneVN0YXRlLCB0b1JlbW92ZS5sZW5ndGggfHwgMSk7XG4gIH0sICdmb3J3YXJkJyk7XG5cbiAgaWYgKGFmdGVyUmVtb3ZhbCA9PT0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKSkge1xuICAgIHJldHVybiBlZGl0b3JTdGF0ZTtcbiAgfVxuXG4gIHJldHVybiBFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCBhZnRlclJlbW92YWwsICdyZW1vdmUtcmFuZ2UnKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBrZXlDb21tYW5kRGVsZXRlV29yZDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/keyCommandDeleteWord.js
