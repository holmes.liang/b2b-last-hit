var Path = __webpack_require__(/*! ../Path */ "./node_modules/zrender/lib/graphic/Path.js");
/**
 * 椭圆形状
 * @module zrender/graphic/shape/Ellipse
 */


var _default = Path.extend({
  type: 'ellipse',
  shape: {
    cx: 0,
    cy: 0,
    rx: 0,
    ry: 0
  },
  buildPath: function buildPath(ctx, shape) {
    var k = 0.5522848;
    var x = shape.cx;
    var y = shape.cy;
    var a = shape.rx;
    var b = shape.ry;
    var ox = a * k; // 水平控制点偏移量

    var oy = b * k; // 垂直控制点偏移量
    // 从椭圆的左端点开始顺时针绘制四条三次贝塞尔曲线

    ctx.moveTo(x - a, y);
    ctx.bezierCurveTo(x - a, y - oy, x - ox, y - b, x, y - b);
    ctx.bezierCurveTo(x + ox, y - b, x + a, y - oy, x + a, y);
    ctx.bezierCurveTo(x + a, y + oy, x + ox, y + b, x, y + b);
    ctx.bezierCurveTo(x - ox, y + b, x - a, y + oy, x - a, y);
    ctx.closePath();
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9FbGxpcHNlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9FbGxpcHNlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBQYXRoID0gcmVxdWlyZShcIi4uL1BhdGhcIik7XG5cbi8qKlxuICog5qSt5ZyG5b2i54q2XG4gKiBAbW9kdWxlIHpyZW5kZXIvZ3JhcGhpYy9zaGFwZS9FbGxpcHNlXG4gKi9cbnZhciBfZGVmYXVsdCA9IFBhdGguZXh0ZW5kKHtcbiAgdHlwZTogJ2VsbGlwc2UnLFxuICBzaGFwZToge1xuICAgIGN4OiAwLFxuICAgIGN5OiAwLFxuICAgIHJ4OiAwLFxuICAgIHJ5OiAwXG4gIH0sXG4gIGJ1aWxkUGF0aDogZnVuY3Rpb24gKGN0eCwgc2hhcGUpIHtcbiAgICB2YXIgayA9IDAuNTUyMjg0ODtcbiAgICB2YXIgeCA9IHNoYXBlLmN4O1xuICAgIHZhciB5ID0gc2hhcGUuY3k7XG4gICAgdmFyIGEgPSBzaGFwZS5yeDtcbiAgICB2YXIgYiA9IHNoYXBlLnJ5O1xuICAgIHZhciBveCA9IGEgKiBrOyAvLyDmsLTlubPmjqfliLbngrnlgY/np7vph49cblxuICAgIHZhciBveSA9IGIgKiBrOyAvLyDlnoLnm7TmjqfliLbngrnlgY/np7vph49cbiAgICAvLyDku47mpK3lnIbnmoTlt6bnq6/ngrnlvIDlp4vpobrml7bpkojnu5jliLblm5vmnaHkuInmrKHotJ3loZ7lsJTmm7Lnur9cblxuICAgIGN0eC5tb3ZlVG8oeCAtIGEsIHkpO1xuICAgIGN0eC5iZXppZXJDdXJ2ZVRvKHggLSBhLCB5IC0gb3ksIHggLSBveCwgeSAtIGIsIHgsIHkgLSBiKTtcbiAgICBjdHguYmV6aWVyQ3VydmVUbyh4ICsgb3gsIHkgLSBiLCB4ICsgYSwgeSAtIG95LCB4ICsgYSwgeSk7XG4gICAgY3R4LmJlemllckN1cnZlVG8oeCArIGEsIHkgKyBveSwgeCArIG94LCB5ICsgYiwgeCwgeSArIGIpO1xuICAgIGN0eC5iZXppZXJDdXJ2ZVRvKHggLSBveCwgeSArIGIsIHggLSBhLCB5ICsgb3ksIHggLSBhLCB5KTtcbiAgICBjdHguY2xvc2VQYXRoKCk7XG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFFQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXpCQTtBQUNBO0FBMkJBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/shape/Ellipse.js
