__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _breadcrumb__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../breadcrumb */ "./node_modules/antd/es/breadcrumb/index.js");
/* harmony import */ var _avatar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../avatar */ "./node_modules/antd/es/avatar/index.js");
/* harmony import */ var _util_transButton__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/transButton */ "./node_modules/antd/es/_util/transButton.js");
/* harmony import */ var _locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../locale-provider/LocaleReceiver */ "./node_modules/antd/es/locale-provider/LocaleReceiver.js");
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}










var renderBack = function renderBack(prefixCls, backIcon, onBack) {
  if (!backIcon || !onBack) {
    return null;
  }

  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_7__["default"], {
    componentName: "PageHeader"
  }, function (_ref) {
    var back = _ref.back;
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-back")
    }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_util_transButton__WEBPACK_IMPORTED_MODULE_6__["default"], {
      onClick: function onClick(e) {
        if (onBack) {
          onBack(e);
        }
      },
      className: "".concat(prefixCls, "-back-button"),
      "aria-label": back
    }, backIcon));
  });
};

var renderBreadcrumb = function renderBreadcrumb(breadcrumb) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_breadcrumb__WEBPACK_IMPORTED_MODULE_4__["default"], breadcrumb);
};

var renderTitle = function renderTitle(prefixCls, props) {
  var title = props.title,
      avatar = props.avatar,
      subTitle = props.subTitle,
      tags = props.tags,
      extra = props.extra,
      backIcon = props.backIcon,
      onBack = props.onBack;
  var headingPrefixCls = "".concat(prefixCls, "-heading");

  if (title || subTitle || tags || extra) {
    var backIconDom = renderBack(prefixCls, backIcon, onBack);
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: headingPrefixCls
    }, backIconDom, avatar && react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_avatar__WEBPACK_IMPORTED_MODULE_5__["default"], avatar), title && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
      className: "".concat(headingPrefixCls, "-title")
    }, title), subTitle && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
      className: "".concat(headingPrefixCls, "-sub-title")
    }, subTitle), tags && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
      className: "".concat(headingPrefixCls, "-tags")
    }, tags), extra && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
      className: "".concat(headingPrefixCls, "-extra")
    }, extra));
  }

  return null;
};

var renderFooter = function renderFooter(prefixCls, footer) {
  if (footer) {
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-footer")
    }, footer);
  }

  return null;
};

var renderChildren = function renderChildren(prefixCls, children) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: "".concat(prefixCls, "-content")
  }, children);
};

var PageHeader = function PageHeader(props) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_2__["ConfigConsumer"], null, function (_ref2) {
    var getPrefixCls = _ref2.getPrefixCls,
        pageHeader = _ref2.pageHeader;
    var customizePrefixCls = props.prefixCls,
        style = props.style,
        footer = props.footer,
        children = props.children,
        breadcrumb = props.breadcrumb,
        customizeClassName = props.className;
    var ghost = true; // Use `ghost` from `props` or from `ConfigProvider` instead.

    if ('ghost' in props) {
      ghost = props.ghost;
    } else if (pageHeader && 'ghost' in pageHeader) {
      ghost = pageHeader.ghost;
    }

    var prefixCls = getPrefixCls('page-header', customizePrefixCls);
    var breadcrumbDom = breadcrumb && breadcrumb.routes ? renderBreadcrumb(breadcrumb) : null;
    var className = classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, customizeClassName, _defineProperty({
      'has-breadcrumb': breadcrumbDom,
      'has-footer': footer
    }, "".concat(prefixCls, "-ghost"), ghost));
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: className,
      style: style
    }, breadcrumbDom, renderTitle(prefixCls, props), children && renderChildren(prefixCls, children), renderFooter(prefixCls, footer));
  });
};

PageHeader.defaultProps = {
  backIcon: react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_3__["default"], {
    type: "arrow-left"
  })
};
/* harmony default export */ __webpack_exports__["default"] = (PageHeader);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9wYWdlLWhlYWRlci9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvcGFnZS1oZWFkZXIvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBjbGFzc25hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IEljb24gZnJvbSAnLi4vaWNvbic7XG5pbXBvcnQgQnJlYWRjcnVtYiBmcm9tICcuLi9icmVhZGNydW1iJztcbmltcG9ydCBBdmF0YXIgZnJvbSAnLi4vYXZhdGFyJztcbmltcG9ydCBUcmFuc0J1dHRvbiBmcm9tICcuLi9fdXRpbC90cmFuc0J1dHRvbic7XG5pbXBvcnQgTG9jYWxlUmVjZWl2ZXIgZnJvbSAnLi4vbG9jYWxlLXByb3ZpZGVyL0xvY2FsZVJlY2VpdmVyJztcbmNvbnN0IHJlbmRlckJhY2sgPSAocHJlZml4Q2xzLCBiYWNrSWNvbiwgb25CYWNrKSA9PiB7XG4gICAgaWYgKCFiYWNrSWNvbiB8fCAhb25CYWNrKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICByZXR1cm4gKDxMb2NhbGVSZWNlaXZlciBjb21wb25lbnROYW1lPVwiUGFnZUhlYWRlclwiPlxuICAgICAgeyh7IGJhY2sgfSkgPT4gKDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWJhY2tgfT5cbiAgICAgICAgICA8VHJhbnNCdXR0b24gb25DbGljaz17KGUpID0+IHtcbiAgICAgICAgaWYgKG9uQmFjaykge1xuICAgICAgICAgICAgb25CYWNrKGUpO1xuICAgICAgICB9XG4gICAgfX0gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWJhY2stYnV0dG9uYH0gYXJpYS1sYWJlbD17YmFja30+XG4gICAgICAgICAgICB7YmFja0ljb259XG4gICAgICAgICAgPC9UcmFuc0J1dHRvbj5cbiAgICAgICAgPC9kaXY+KX1cbiAgICA8L0xvY2FsZVJlY2VpdmVyPik7XG59O1xuY29uc3QgcmVuZGVyQnJlYWRjcnVtYiA9IChicmVhZGNydW1iKSA9PiB7XG4gICAgcmV0dXJuIDxCcmVhZGNydW1iIHsuLi5icmVhZGNydW1ifS8+O1xufTtcbmNvbnN0IHJlbmRlclRpdGxlID0gKHByZWZpeENscywgcHJvcHMpID0+IHtcbiAgICBjb25zdCB7IHRpdGxlLCBhdmF0YXIsIHN1YlRpdGxlLCB0YWdzLCBleHRyYSwgYmFja0ljb24sIG9uQmFjayB9ID0gcHJvcHM7XG4gICAgY29uc3QgaGVhZGluZ1ByZWZpeENscyA9IGAke3ByZWZpeENsc30taGVhZGluZ2A7XG4gICAgaWYgKHRpdGxlIHx8IHN1YlRpdGxlIHx8IHRhZ3MgfHwgZXh0cmEpIHtcbiAgICAgICAgY29uc3QgYmFja0ljb25Eb20gPSByZW5kZXJCYWNrKHByZWZpeENscywgYmFja0ljb24sIG9uQmFjayk7XG4gICAgICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9e2hlYWRpbmdQcmVmaXhDbHN9PlxuICAgICAgICB7YmFja0ljb25Eb219XG4gICAgICAgIHthdmF0YXIgJiYgPEF2YXRhciB7Li4uYXZhdGFyfS8+fVxuICAgICAgICB7dGl0bGUgJiYgPHNwYW4gY2xhc3NOYW1lPXtgJHtoZWFkaW5nUHJlZml4Q2xzfS10aXRsZWB9Pnt0aXRsZX08L3NwYW4+fVxuICAgICAgICB7c3ViVGl0bGUgJiYgPHNwYW4gY2xhc3NOYW1lPXtgJHtoZWFkaW5nUHJlZml4Q2xzfS1zdWItdGl0bGVgfT57c3ViVGl0bGV9PC9zcGFuPn1cbiAgICAgICAge3RhZ3MgJiYgPHNwYW4gY2xhc3NOYW1lPXtgJHtoZWFkaW5nUHJlZml4Q2xzfS10YWdzYH0+e3RhZ3N9PC9zcGFuPn1cbiAgICAgICAge2V4dHJhICYmIDxzcGFuIGNsYXNzTmFtZT17YCR7aGVhZGluZ1ByZWZpeENsc30tZXh0cmFgfT57ZXh0cmF9PC9zcGFuPn1cbiAgICAgIDwvZGl2Pik7XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xufTtcbmNvbnN0IHJlbmRlckZvb3RlciA9IChwcmVmaXhDbHMsIGZvb3RlcikgPT4ge1xuICAgIGlmIChmb290ZXIpIHtcbiAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWZvb3RlcmB9Pntmb290ZXJ9PC9kaXY+O1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbn07XG5jb25zdCByZW5kZXJDaGlsZHJlbiA9IChwcmVmaXhDbHMsIGNoaWxkcmVuKSA9PiB7XG4gICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbnRlbnRgfT57Y2hpbGRyZW59PC9kaXY+O1xufTtcbmNvbnN0IFBhZ2VIZWFkZXIgPSBwcm9wcyA9PiAoPENvbmZpZ0NvbnN1bWVyPlxuICAgIHsoeyBnZXRQcmVmaXhDbHMsIHBhZ2VIZWFkZXIgfSkgPT4ge1xuICAgIGNvbnN0IHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIHN0eWxlLCBmb290ZXIsIGNoaWxkcmVuLCBicmVhZGNydW1iLCBjbGFzc05hbWU6IGN1c3RvbWl6ZUNsYXNzTmFtZSwgfSA9IHByb3BzO1xuICAgIGxldCBnaG9zdCA9IHRydWU7XG4gICAgLy8gVXNlIGBnaG9zdGAgZnJvbSBgcHJvcHNgIG9yIGZyb20gYENvbmZpZ1Byb3ZpZGVyYCBpbnN0ZWFkLlxuICAgIGlmICgnZ2hvc3QnIGluIHByb3BzKSB7XG4gICAgICAgIGdob3N0ID0gcHJvcHMuZ2hvc3Q7XG4gICAgfVxuICAgIGVsc2UgaWYgKHBhZ2VIZWFkZXIgJiYgJ2dob3N0JyBpbiBwYWdlSGVhZGVyKSB7XG4gICAgICAgIGdob3N0ID0gcGFnZUhlYWRlci5naG9zdDtcbiAgICB9XG4gICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdwYWdlLWhlYWRlcicsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgY29uc3QgYnJlYWRjcnVtYkRvbSA9IGJyZWFkY3J1bWIgJiYgYnJlYWRjcnVtYi5yb3V0ZXMgPyByZW5kZXJCcmVhZGNydW1iKGJyZWFkY3J1bWIpIDogbnVsbDtcbiAgICBjb25zdCBjbGFzc05hbWUgPSBjbGFzc25hbWVzKHByZWZpeENscywgY3VzdG9taXplQ2xhc3NOYW1lLCB7XG4gICAgICAgICdoYXMtYnJlYWRjcnVtYic6IGJyZWFkY3J1bWJEb20sXG4gICAgICAgICdoYXMtZm9vdGVyJzogZm9vdGVyLFxuICAgICAgICBbYCR7cHJlZml4Q2xzfS1naG9zdGBdOiBnaG9zdCxcbiAgICB9KTtcbiAgICByZXR1cm4gKDxkaXYgY2xhc3NOYW1lPXtjbGFzc05hbWV9IHN0eWxlPXtzdHlsZX0+XG4gICAgICAgICAge2JyZWFkY3J1bWJEb219XG4gICAgICAgICAge3JlbmRlclRpdGxlKHByZWZpeENscywgcHJvcHMpfVxuICAgICAgICAgIHtjaGlsZHJlbiAmJiByZW5kZXJDaGlsZHJlbihwcmVmaXhDbHMsIGNoaWxkcmVuKX1cbiAgICAgICAgICB7cmVuZGVyRm9vdGVyKHByZWZpeENscywgZm9vdGVyKX1cbiAgICAgICAgPC9kaXY+KTtcbn19XG4gIDwvQ29uZmlnQ29uc3VtZXI+KTtcblBhZ2VIZWFkZXIuZGVmYXVsdFByb3BzID0ge1xuICAgIGJhY2tJY29uOiA8SWNvbiB0eXBlPVwiYXJyb3ctbGVmdFwiLz4sXG59O1xuZXhwb3J0IGRlZmF1bHQgUGFnZUhlYWRlcjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBSUE7QUFBQTtBQUpBO0FBRkE7QUFKQTtBQUNBO0FBZUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBZEE7QUFDQTtBQWVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFKQTtBQUNBO0FBS0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFsQkE7QUFBQTtBQUNBO0FBeUJBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/page-header/index.js
