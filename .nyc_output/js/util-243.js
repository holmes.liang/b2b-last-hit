__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFullKeyList", function() { return getFullKeyList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calcRangeKeys", function() { return calcRangeKeys; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertDirectoryKeysToNodes", function() { return convertDirectoryKeysToNodes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFullKeyListByTreeData", function() { return getFullKeyListByTreeData; });
/* harmony import */ var rc_tree_es_util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rc-tree/es/util */ "./node_modules/rc-tree/es/util.js");
function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}


var Record;

(function (Record) {
  Record[Record["None"] = 0] = "None";
  Record[Record["Start"] = 1] = "Start";
  Record[Record["End"] = 2] = "End";
})(Record || (Record = {})); // TODO: Move this logic into `rc-tree`


function traverseNodesKey(rootChildren, callback) {
  var nodeList = Object(rc_tree_es_util__WEBPACK_IMPORTED_MODULE_0__["getNodeChildren"])(rootChildren) || [];

  function processNode(node) {
    var key = node.key,
        children = node.props.children;

    if (callback(key, node) !== false) {
      traverseNodesKey(children, callback);
    }
  }

  nodeList.forEach(processNode);
}

function getFullKeyList(children) {
  var _convertTreeToEntitie = Object(rc_tree_es_util__WEBPACK_IMPORTED_MODULE_0__["convertTreeToEntities"])(children),
      keyEntities = _convertTreeToEntitie.keyEntities;

  return Object.keys(keyEntities);
}
/** 计算选中范围，只考虑expanded情况以优化性能 */

function calcRangeKeys(rootChildren, expandedKeys, startKey, endKey) {
  var keys = [];
  var record = Record.None;

  if (startKey && startKey === endKey) {
    return [startKey];
  }

  if (!startKey || !endKey) {
    return [];
  }

  function matchKey(key) {
    return key === startKey || key === endKey;
  }

  traverseNodesKey(rootChildren, function (key) {
    if (record === Record.End) {
      return false;
    }

    if (matchKey(key)) {
      // Match test
      keys.push(key);

      if (record === Record.None) {
        record = Record.Start;
      } else if (record === Record.Start) {
        record = Record.End;
        return false;
      }
    } else if (record === Record.Start) {
      // Append selection
      keys.push(key);
    }

    if (expandedKeys.indexOf(key) === -1) {
      return false;
    }

    return true;
  });
  return keys;
}
function convertDirectoryKeysToNodes(rootChildren, keys) {
  var restKeys = _toConsumableArray(keys);

  var nodes = [];
  traverseNodesKey(rootChildren, function (key, node) {
    var index = restKeys.indexOf(key);

    if (index !== -1) {
      nodes.push(node);
      restKeys.splice(index, 1);
    }

    return !!restKeys.length;
  });
  return nodes;
}
function getFullKeyListByTreeData(treeData) {
  var keys = [];
  (treeData || []).forEach(function (item) {
    keys.push(item.key);

    if (item.children) {
      keys = [].concat(_toConsumableArray(keys), _toConsumableArray(getFullKeyListByTreeData(item.children)));
    }
  });
  return keys;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90cmVlL3V0aWwuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3RyZWUvdXRpbC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBnZXROb2RlQ2hpbGRyZW4sIGNvbnZlcnRUcmVlVG9FbnRpdGllcyB9IGZyb20gJ3JjLXRyZWUvbGliL3V0aWwnO1xudmFyIFJlY29yZDtcbihmdW5jdGlvbiAoUmVjb3JkKSB7XG4gICAgUmVjb3JkW1JlY29yZFtcIk5vbmVcIl0gPSAwXSA9IFwiTm9uZVwiO1xuICAgIFJlY29yZFtSZWNvcmRbXCJTdGFydFwiXSA9IDFdID0gXCJTdGFydFwiO1xuICAgIFJlY29yZFtSZWNvcmRbXCJFbmRcIl0gPSAyXSA9IFwiRW5kXCI7XG59KShSZWNvcmQgfHwgKFJlY29yZCA9IHt9KSk7XG4vLyBUT0RPOiBNb3ZlIHRoaXMgbG9naWMgaW50byBgcmMtdHJlZWBcbmZ1bmN0aW9uIHRyYXZlcnNlTm9kZXNLZXkocm9vdENoaWxkcmVuLCBjYWxsYmFjaykge1xuICAgIGNvbnN0IG5vZGVMaXN0ID0gZ2V0Tm9kZUNoaWxkcmVuKHJvb3RDaGlsZHJlbikgfHwgW107XG4gICAgZnVuY3Rpb24gcHJvY2Vzc05vZGUobm9kZSkge1xuICAgICAgICBjb25zdCB7IGtleSwgcHJvcHM6IHsgY2hpbGRyZW4gfSwgfSA9IG5vZGU7XG4gICAgICAgIGlmIChjYWxsYmFjayhrZXksIG5vZGUpICE9PSBmYWxzZSkge1xuICAgICAgICAgICAgdHJhdmVyc2VOb2Rlc0tleShjaGlsZHJlbiwgY2FsbGJhY2spO1xuICAgICAgICB9XG4gICAgfVxuICAgIG5vZGVMaXN0LmZvckVhY2gocHJvY2Vzc05vZGUpO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldEZ1bGxLZXlMaXN0KGNoaWxkcmVuKSB7XG4gICAgY29uc3QgeyBrZXlFbnRpdGllcyB9ID0gY29udmVydFRyZWVUb0VudGl0aWVzKGNoaWxkcmVuKTtcbiAgICByZXR1cm4gT2JqZWN0LmtleXMoa2V5RW50aXRpZXMpO1xufVxuLyoqIOiuoeeul+mAieS4reiMg+WbtO+8jOWPquiAg+iZkWV4cGFuZGVk5oOF5Ya15Lul5LyY5YyW5oCn6IO9ICovXG5leHBvcnQgZnVuY3Rpb24gY2FsY1JhbmdlS2V5cyhyb290Q2hpbGRyZW4sIGV4cGFuZGVkS2V5cywgc3RhcnRLZXksIGVuZEtleSkge1xuICAgIGNvbnN0IGtleXMgPSBbXTtcbiAgICBsZXQgcmVjb3JkID0gUmVjb3JkLk5vbmU7XG4gICAgaWYgKHN0YXJ0S2V5ICYmIHN0YXJ0S2V5ID09PSBlbmRLZXkpIHtcbiAgICAgICAgcmV0dXJuIFtzdGFydEtleV07XG4gICAgfVxuICAgIGlmICghc3RhcnRLZXkgfHwgIWVuZEtleSkge1xuICAgICAgICByZXR1cm4gW107XG4gICAgfVxuICAgIGZ1bmN0aW9uIG1hdGNoS2V5KGtleSkge1xuICAgICAgICByZXR1cm4ga2V5ID09PSBzdGFydEtleSB8fCBrZXkgPT09IGVuZEtleTtcbiAgICB9XG4gICAgdHJhdmVyc2VOb2Rlc0tleShyb290Q2hpbGRyZW4sIChrZXkpID0+IHtcbiAgICAgICAgaWYgKHJlY29yZCA9PT0gUmVjb3JkLkVuZCkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChtYXRjaEtleShrZXkpKSB7XG4gICAgICAgICAgICAvLyBNYXRjaCB0ZXN0XG4gICAgICAgICAgICBrZXlzLnB1c2goa2V5KTtcbiAgICAgICAgICAgIGlmIChyZWNvcmQgPT09IFJlY29yZC5Ob25lKSB7XG4gICAgICAgICAgICAgICAgcmVjb3JkID0gUmVjb3JkLlN0YXJ0O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAocmVjb3JkID09PSBSZWNvcmQuU3RhcnQpIHtcbiAgICAgICAgICAgICAgICByZWNvcmQgPSBSZWNvcmQuRW5kO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChyZWNvcmQgPT09IFJlY29yZC5TdGFydCkge1xuICAgICAgICAgICAgLy8gQXBwZW5kIHNlbGVjdGlvblxuICAgICAgICAgICAga2V5cy5wdXNoKGtleSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGV4cGFuZGVkS2V5cy5pbmRleE9mKGtleSkgPT09IC0xKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfSk7XG4gICAgcmV0dXJuIGtleXM7XG59XG5leHBvcnQgZnVuY3Rpb24gY29udmVydERpcmVjdG9yeUtleXNUb05vZGVzKHJvb3RDaGlsZHJlbiwga2V5cykge1xuICAgIGNvbnN0IHJlc3RLZXlzID0gWy4uLmtleXNdO1xuICAgIGNvbnN0IG5vZGVzID0gW107XG4gICAgdHJhdmVyc2VOb2Rlc0tleShyb290Q2hpbGRyZW4sIChrZXksIG5vZGUpID0+IHtcbiAgICAgICAgY29uc3QgaW5kZXggPSByZXN0S2V5cy5pbmRleE9mKGtleSk7XG4gICAgICAgIGlmIChpbmRleCAhPT0gLTEpIHtcbiAgICAgICAgICAgIG5vZGVzLnB1c2gobm9kZSk7XG4gICAgICAgICAgICByZXN0S2V5cy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAhIXJlc3RLZXlzLmxlbmd0aDtcbiAgICB9KTtcbiAgICByZXR1cm4gbm9kZXM7XG59XG5leHBvcnQgZnVuY3Rpb24gZ2V0RnVsbEtleUxpc3RCeVRyZWVEYXRhKHRyZWVEYXRhKSB7XG4gICAgbGV0IGtleXMgPSBbXTtcbiAgICAodHJlZURhdGEgfHwgW10pLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICAgIGtleXMucHVzaChpdGVtLmtleSk7XG4gICAgICAgIGlmIChpdGVtLmNoaWxkcmVuKSB7XG4gICAgICAgICAgICBrZXlzID0gWy4uLmtleXMsIC4uLmdldEZ1bGxLZXlMaXN0QnlUcmVlRGF0YShpdGVtLmNoaWxkcmVuKV07XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4ga2V5cztcbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBVEE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUF0QkE7QUF3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/tree/util.js
