var camel2hyphen = function camel2hyphen(str) {
  return str.replace(/[A-Z]/g, function (match) {
    return '-' + match.toLowerCase();
  }).toLowerCase();
};

module.exports = camel2hyphen;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvc3RyaW5nLWNvbnZlcnQvY2FtZWwyaHlwaGVuLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvc3RyaW5nLWNvbnZlcnQvY2FtZWwyaHlwaGVuLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBjYW1lbDJoeXBoZW4gPSBmdW5jdGlvbiAoc3RyKSB7XG4gIHJldHVybiBzdHJcbiAgICAgICAgICAucmVwbGFjZSgvW0EtWl0vZywgZnVuY3Rpb24gKG1hdGNoKSB7XG4gICAgICAgICAgICByZXR1cm4gJy0nICsgbWF0Y2gudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgICB9KVxuICAgICAgICAgIC50b0xvd2VyQ2FzZSgpO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBjYW1lbDJoeXBoZW47Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/string-convert/camel2hyphen.js
