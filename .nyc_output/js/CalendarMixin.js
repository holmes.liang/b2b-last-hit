__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getNowByCurrentStateValue", function() { return getNowByCurrentStateValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calendarMixinPropTypes", function() { return calendarMixinPropTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calendarMixinDefaultProps", function() { return calendarMixinDefaultProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calendarMixinWrapper", function() { return calendarMixinWrapper; });
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _util_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../util/index */ "./node_modules/rc-calendar/es/util/index.js");









function noop() {}

function getNowByCurrentStateValue(value) {
  var ret = void 0;

  if (value) {
    ret = Object(_util_index__WEBPACK_IMPORTED_MODULE_7__["getTodayTime"])(value);
  } else {
    ret = moment__WEBPACK_IMPORTED_MODULE_6___default()();
  }

  return ret;
}
var calendarMixinPropTypes = {
  value: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.object,
  defaultValue: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.object,
  onKeyDown: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.func
};
var calendarMixinDefaultProps = {
  onKeyDown: noop
};
var calendarMixinWrapper = function calendarMixinWrapper(ComposeComponent) {
  var _class, _temp2;

  return _temp2 = _class = function (_ComposeComponent) {
    babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_class, _ComposeComponent);

    function _class() {
      var _temp, _this, _ret;

      babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _class);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default()(this, _ComposeComponent.call.apply(_ComposeComponent, [this].concat(args))), _this), _this.onSelect = function (value, cause) {
        if (value) {
          _this.setValue(value);
        }

        _this.setSelectedValue(value, cause);
      }, _this.renderRoot = function (newProps) {
        var _className;

        var props = _this.props;
        var prefixCls = props.prefixCls;
        var className = (_className = {}, _className[prefixCls] = 1, _className[prefixCls + '-hidden'] = !props.visible, _className[props.className] = !!props.className, _className[newProps.className] = !!newProps.className, _className);
        return react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement('div', {
          ref: _this.saveRoot,
          className: '' + classnames__WEBPACK_IMPORTED_MODULE_5___default()(className),
          style: _this.props.style,
          tabIndex: '0',
          onKeyDown: _this.onKeyDown,
          onBlur: _this.onBlur
        }, newProps.children);
      }, _this.setSelectedValue = function (selectedValue, cause) {
        // if (this.isAllowedDate(selectedValue)) {
        if (!('selectedValue' in _this.props)) {
          _this.setState({
            selectedValue: selectedValue
          });
        }

        if (_this.props.onSelect) {
          _this.props.onSelect(selectedValue, cause);
        } // }

      }, _this.setValue = function (value) {
        var originalValue = _this.state.value;

        if (!('value' in _this.props)) {
          _this.setState({
            value: value
          });
        }

        if (originalValue && value && !originalValue.isSame(value) || !originalValue && value || originalValue && !value) {
          _this.props.onChange(value);
        }
      }, _this.isAllowedDate = function (value) {
        var disabledDate = _this.props.disabledDate;
        var disabledTime = _this.props.disabledTime;
        return Object(_util_index__WEBPACK_IMPORTED_MODULE_7__["isAllowedDate"])(value, disabledDate, disabledTime);
      }, _temp), babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default()(_this, _ret);
    }

    _class.getDerivedStateFromProps = function getDerivedStateFromProps(nextProps, prevState) {
      // Use origin function if provided
      if (ComposeComponent.getDerivedStateFromProps) {
        return ComposeComponent.getDerivedStateFromProps(nextProps, prevState);
      }

      var value = nextProps.value,
          selectedValue = nextProps.selectedValue;
      var newState = {};

      if ('value' in nextProps) {
        newState.value = value || nextProps.defaultValue || getNowByCurrentStateValue(prevState.value);
      }

      if ('selectedValue' in nextProps) {
        newState.selectedValue = selectedValue;
      }

      return newState;
    };

    return _class;
  }(ComposeComponent), _class.displayName = 'CalendarMixinWrapper', _class.defaultProps = ComposeComponent.defaultProps, _temp2;
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtY2FsZW5kYXIvZXMvbWl4aW4vQ2FsZW5kYXJNaXhpbi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWNhbGVuZGFyL2VzL21peGluL0NhbGVuZGFyTWl4aW4uanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9jbGFzc0NhbGxDaGVjayBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snO1xuaW1wb3J0IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJztcbmltcG9ydCBfaW5oZXJpdHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJztcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzbmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudCc7XG5pbXBvcnQgeyBpc0FsbG93ZWREYXRlLCBnZXRUb2RheVRpbWUgfSBmcm9tICcuLi91dGlsL2luZGV4JztcblxuZnVuY3Rpb24gbm9vcCgpIHt9XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXROb3dCeUN1cnJlbnRTdGF0ZVZhbHVlKHZhbHVlKSB7XG4gIHZhciByZXQgPSB2b2lkIDA7XG4gIGlmICh2YWx1ZSkge1xuICAgIHJldCA9IGdldFRvZGF5VGltZSh2YWx1ZSk7XG4gIH0gZWxzZSB7XG4gICAgcmV0ID0gbW9tZW50KCk7XG4gIH1cbiAgcmV0dXJuIHJldDtcbn1cblxuZXhwb3J0IHZhciBjYWxlbmRhck1peGluUHJvcFR5cGVzID0ge1xuICB2YWx1ZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgZGVmYXVsdFZhbHVlOiBQcm9wVHlwZXMub2JqZWN0LFxuICBvbktleURvd246IFByb3BUeXBlcy5mdW5jXG59O1xuXG5leHBvcnQgdmFyIGNhbGVuZGFyTWl4aW5EZWZhdWx0UHJvcHMgPSB7XG4gIG9uS2V5RG93bjogbm9vcFxufTtcblxuZXhwb3J0IHZhciBjYWxlbmRhck1peGluV3JhcHBlciA9IGZ1bmN0aW9uIGNhbGVuZGFyTWl4aW5XcmFwcGVyKENvbXBvc2VDb21wb25lbnQpIHtcbiAgdmFyIF9jbGFzcywgX3RlbXAyO1xuXG4gIHJldHVybiBfdGVtcDIgPSBfY2xhc3MgPSBmdW5jdGlvbiAoX0NvbXBvc2VDb21wb25lbnQpIHtcbiAgICBfaW5oZXJpdHMoX2NsYXNzLCBfQ29tcG9zZUNvbXBvbmVudCk7XG5cbiAgICBmdW5jdGlvbiBfY2xhc3MoKSB7XG4gICAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgX2NsYXNzKTtcblxuICAgICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29tcG9zZUNvbXBvbmVudC5jYWxsLmFwcGx5KF9Db21wb3NlQ29tcG9uZW50LCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMub25TZWxlY3QgPSBmdW5jdGlvbiAodmFsdWUsIGNhdXNlKSB7XG4gICAgICAgIGlmICh2YWx1ZSkge1xuICAgICAgICAgIF90aGlzLnNldFZhbHVlKHZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgICBfdGhpcy5zZXRTZWxlY3RlZFZhbHVlKHZhbHVlLCBjYXVzZSk7XG4gICAgICB9LCBfdGhpcy5yZW5kZXJSb290ID0gZnVuY3Rpb24gKG5ld1Byb3BzKSB7XG4gICAgICAgIHZhciBfY2xhc3NOYW1lO1xuXG4gICAgICAgIHZhciBwcm9wcyA9IF90aGlzLnByb3BzO1xuICAgICAgICB2YXIgcHJlZml4Q2xzID0gcHJvcHMucHJlZml4Q2xzO1xuXG4gICAgICAgIHZhciBjbGFzc05hbWUgPSAoX2NsYXNzTmFtZSA9IHt9LCBfY2xhc3NOYW1lW3ByZWZpeENsc10gPSAxLCBfY2xhc3NOYW1lW3ByZWZpeENscyArICctaGlkZGVuJ10gPSAhcHJvcHMudmlzaWJsZSwgX2NsYXNzTmFtZVtwcm9wcy5jbGFzc05hbWVdID0gISFwcm9wcy5jbGFzc05hbWUsIF9jbGFzc05hbWVbbmV3UHJvcHMuY2xhc3NOYW1lXSA9ICEhbmV3UHJvcHMuY2xhc3NOYW1lLCBfY2xhc3NOYW1lKTtcblxuICAgICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICB7XG4gICAgICAgICAgICByZWY6IF90aGlzLnNhdmVSb290LFxuICAgICAgICAgICAgY2xhc3NOYW1lOiAnJyArIGNsYXNzbmFtZXMoY2xhc3NOYW1lKSxcbiAgICAgICAgICAgIHN0eWxlOiBfdGhpcy5wcm9wcy5zdHlsZSxcbiAgICAgICAgICAgIHRhYkluZGV4OiAnMCcsXG4gICAgICAgICAgICBvbktleURvd246IF90aGlzLm9uS2V5RG93bixcbiAgICAgICAgICAgIG9uQmx1cjogX3RoaXMub25CbHVyXG4gICAgICAgICAgfSxcbiAgICAgICAgICBuZXdQcm9wcy5jaGlsZHJlblxuICAgICAgICApO1xuICAgICAgfSwgX3RoaXMuc2V0U2VsZWN0ZWRWYWx1ZSA9IGZ1bmN0aW9uIChzZWxlY3RlZFZhbHVlLCBjYXVzZSkge1xuICAgICAgICAvLyBpZiAodGhpcy5pc0FsbG93ZWREYXRlKHNlbGVjdGVkVmFsdWUpKSB7XG4gICAgICAgIGlmICghKCdzZWxlY3RlZFZhbHVlJyBpbiBfdGhpcy5wcm9wcykpIHtcbiAgICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBzZWxlY3RlZFZhbHVlOiBzZWxlY3RlZFZhbHVlXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKF90aGlzLnByb3BzLm9uU2VsZWN0KSB7XG4gICAgICAgICAgX3RoaXMucHJvcHMub25TZWxlY3Qoc2VsZWN0ZWRWYWx1ZSwgY2F1c2UpO1xuICAgICAgICB9XG4gICAgICAgIC8vIH1cbiAgICAgIH0sIF90aGlzLnNldFZhbHVlID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgIHZhciBvcmlnaW5hbFZhbHVlID0gX3RoaXMuc3RhdGUudmFsdWU7XG4gICAgICAgIGlmICghKCd2YWx1ZScgaW4gX3RoaXMucHJvcHMpKSB7XG4gICAgICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgdmFsdWU6IHZhbHVlXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG9yaWdpbmFsVmFsdWUgJiYgdmFsdWUgJiYgIW9yaWdpbmFsVmFsdWUuaXNTYW1lKHZhbHVlKSB8fCAhb3JpZ2luYWxWYWx1ZSAmJiB2YWx1ZSB8fCBvcmlnaW5hbFZhbHVlICYmICF2YWx1ZSkge1xuICAgICAgICAgIF90aGlzLnByb3BzLm9uQ2hhbmdlKHZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgfSwgX3RoaXMuaXNBbGxvd2VkRGF0ZSA9IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICB2YXIgZGlzYWJsZWREYXRlID0gX3RoaXMucHJvcHMuZGlzYWJsZWREYXRlO1xuICAgICAgICB2YXIgZGlzYWJsZWRUaW1lID0gX3RoaXMucHJvcHMuZGlzYWJsZWRUaW1lO1xuICAgICAgICByZXR1cm4gaXNBbGxvd2VkRGF0ZSh2YWx1ZSwgZGlzYWJsZWREYXRlLCBkaXNhYmxlZFRpbWUpO1xuICAgICAgfSwgX3RlbXApLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihfdGhpcywgX3JldCk7XG4gICAgfVxuXG4gICAgX2NsYXNzLmdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyA9IGZ1bmN0aW9uIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgLy8gVXNlIG9yaWdpbiBmdW5jdGlvbiBpZiBwcm92aWRlZFxuICAgICAgaWYgKENvbXBvc2VDb21wb25lbnQuZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzKSB7XG4gICAgICAgIHJldHVybiBDb21wb3NlQ29tcG9uZW50LmdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIHByZXZTdGF0ZSk7XG4gICAgICB9XG5cbiAgICAgIHZhciB2YWx1ZSA9IG5leHRQcm9wcy52YWx1ZSxcbiAgICAgICAgICBzZWxlY3RlZFZhbHVlID0gbmV4dFByb3BzLnNlbGVjdGVkVmFsdWU7XG5cbiAgICAgIHZhciBuZXdTdGF0ZSA9IHt9O1xuXG4gICAgICBpZiAoJ3ZhbHVlJyBpbiBuZXh0UHJvcHMpIHtcbiAgICAgICAgbmV3U3RhdGUudmFsdWUgPSB2YWx1ZSB8fCBuZXh0UHJvcHMuZGVmYXVsdFZhbHVlIHx8IGdldE5vd0J5Q3VycmVudFN0YXRlVmFsdWUocHJldlN0YXRlLnZhbHVlKTtcbiAgICAgIH1cbiAgICAgIGlmICgnc2VsZWN0ZWRWYWx1ZScgaW4gbmV4dFByb3BzKSB7XG4gICAgICAgIG5ld1N0YXRlLnNlbGVjdGVkVmFsdWUgPSBzZWxlY3RlZFZhbHVlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbmV3U3RhdGU7XG4gICAgfTtcblxuICAgIHJldHVybiBfY2xhc3M7XG4gIH0oQ29tcG9zZUNvbXBvbmVudCksIF9jbGFzcy5kaXNwbGF5TmFtZSA9ICdDYWxlbmRhck1peGluV3JhcHBlcicsIF9jbGFzcy5kZWZhdWx0UHJvcHMgPSBDb21wb3NlQ29tcG9uZW50LmRlZmF1bHRQcm9wcywgX3RlbXAyO1xufTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-calendar/es/mixin/CalendarMixin.js
