/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var textContain = __webpack_require__(/*! zrender/lib/contain/text */ "./node_modules/zrender/lib/contain/text.js");

var numberUtil = __webpack_require__(/*! ./number */ "./node_modules/echarts/lib/util/number.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// import Text from 'zrender/src/graphic/Text';

/**
 * 每三位默认加,格式化
 * @param {string|number} x
 * @return {string}
 */


function addCommas(x) {
  if (isNaN(x)) {
    return '-';
  }

  x = (x + '').split('.');
  return x[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,') + (x.length > 1 ? '.' + x[1] : '');
}
/**
 * @param {string} str
 * @param {boolean} [upperCaseFirst=false]
 * @return {string} str
 */


function toCamelCase(str, upperCaseFirst) {
  str = (str || '').toLowerCase().replace(/-(.)/g, function (match, group1) {
    return group1.toUpperCase();
  });

  if (upperCaseFirst && str) {
    str = str.charAt(0).toUpperCase() + str.slice(1);
  }

  return str;
}

var normalizeCssArray = zrUtil.normalizeCssArray;
var replaceReg = /([&<>"'])/g;
var replaceMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  '\'': '&#39;'
};

function encodeHTML(source) {
  return source == null ? '' : (source + '').replace(replaceReg, function (str, c) {
    return replaceMap[c];
  });
}

var TPL_VAR_ALIAS = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];

var wrapVar = function wrapVar(varName, seriesIdx) {
  return '{' + varName + (seriesIdx == null ? '' : seriesIdx) + '}';
};
/**
 * Template formatter
 * @param {string} tpl
 * @param {Array.<Object>|Object} paramsList
 * @param {boolean} [encode=false]
 * @return {string}
 */


function formatTpl(tpl, paramsList, encode) {
  if (!zrUtil.isArray(paramsList)) {
    paramsList = [paramsList];
  }

  var seriesLen = paramsList.length;

  if (!seriesLen) {
    return '';
  }

  var $vars = paramsList[0].$vars || [];

  for (var i = 0; i < $vars.length; i++) {
    var alias = TPL_VAR_ALIAS[i];
    tpl = tpl.replace(wrapVar(alias), wrapVar(alias, 0));
  }

  for (var seriesIdx = 0; seriesIdx < seriesLen; seriesIdx++) {
    for (var k = 0; k < $vars.length; k++) {
      var val = paramsList[seriesIdx][$vars[k]];
      tpl = tpl.replace(wrapVar(TPL_VAR_ALIAS[k], seriesIdx), encode ? encodeHTML(val) : val);
    }
  }

  return tpl;
}
/**
 * simple Template formatter
 *
 * @param {string} tpl
 * @param {Object} param
 * @param {boolean} [encode=false]
 * @return {string}
 */


function formatTplSimple(tpl, param, encode) {
  zrUtil.each(param, function (value, key) {
    tpl = tpl.replace('{' + key + '}', encode ? encodeHTML(value) : value);
  });
  return tpl;
}
/**
 * @param {Object|string} [opt] If string, means color.
 * @param {string} [opt.color]
 * @param {string} [opt.extraCssText]
 * @param {string} [opt.type='item'] 'item' or 'subItem'
 * @param {string} [opt.renderMode='html'] render mode of tooltip, 'html' or 'richText'
 * @param {string} [opt.markerId='X'] id name for marker. If only one marker is in a rich text, this can be omitted.
 * @return {string}
 */


function getTooltipMarker(opt, extraCssText) {
  opt = zrUtil.isString(opt) ? {
    color: opt,
    extraCssText: extraCssText
  } : opt || {};
  var color = opt.color;
  var type = opt.type;
  var extraCssText = opt.extraCssText;
  var renderMode = opt.renderMode || 'html';
  var markerId = opt.markerId || 'X';

  if (!color) {
    return '';
  }

  if (renderMode === 'html') {
    return type === 'subItem' ? '<span style="display:inline-block;vertical-align:middle;margin-right:8px;margin-left:3px;' + 'border-radius:4px;width:4px;height:4px;background-color:' + encodeHTML(color) + ';' + (extraCssText || '') + '"></span>' : '<span style="display:inline-block;margin-right:5px;' + 'border-radius:10px;width:10px;height:10px;background-color:' + encodeHTML(color) + ';' + (extraCssText || '') + '"></span>';
  } else {
    // Space for rich element marker
    return {
      renderMode: renderMode,
      content: '{marker' + markerId + '|}  ',
      style: {
        color: color
      }
    };
  }
}

function pad(str, len) {
  str += '';
  return '0000'.substr(0, len - str.length) + str;
}
/**
 * ISO Date format
 * @param {string} tpl
 * @param {number} value
 * @param {boolean} [isUTC=false] Default in local time.
 *           see `module:echarts/scale/Time`
 *           and `module:echarts/util/number#parseDate`.
 * @inner
 */


function formatTime(tpl, value, isUTC) {
  if (tpl === 'week' || tpl === 'month' || tpl === 'quarter' || tpl === 'half-year' || tpl === 'year') {
    tpl = 'MM-dd\nyyyy';
  }

  var date = numberUtil.parseDate(value);
  var utc = isUTC ? 'UTC' : '';
  var y = date['get' + utc + 'FullYear']();
  var M = date['get' + utc + 'Month']() + 1;
  var d = date['get' + utc + 'Date']();
  var h = date['get' + utc + 'Hours']();
  var m = date['get' + utc + 'Minutes']();
  var s = date['get' + utc + 'Seconds']();
  var S = date['get' + utc + 'Milliseconds']();
  tpl = tpl.replace('MM', pad(M, 2)).replace('M', M).replace('yyyy', y).replace('yy', y % 100).replace('dd', pad(d, 2)).replace('d', d).replace('hh', pad(h, 2)).replace('h', h).replace('mm', pad(m, 2)).replace('m', m).replace('ss', pad(s, 2)).replace('s', s).replace('SSS', pad(S, 3));
  return tpl;
}
/**
 * Capital first
 * @param {string} str
 * @return {string}
 */


function capitalFirst(str) {
  return str ? str.charAt(0).toUpperCase() + str.substr(1) : str;
}

var truncateText = textContain.truncateText;
/**
 * @public
 * @param {Object} opt
 * @param {string} opt.text
 * @param {string} opt.font
 * @param {string} [opt.textAlign='left']
 * @param {string} [opt.textVerticalAlign='top']
 * @param {Array.<number>} [opt.textPadding]
 * @param {number} [opt.textLineHeight]
 * @param {Object} [opt.rich]
 * @param {Object} [opt.truncate]
 * @return {Object} {x, y, width, height, lineHeight}
 */

function getTextBoundingRect(opt) {
  return textContain.getBoundingRect(opt.text, opt.font, opt.textAlign, opt.textVerticalAlign, opt.textPadding, opt.textLineHeight, opt.rich, opt.truncate);
}
/**
 * @deprecated
 * the `textLineHeight` was added later.
 * For backward compatiblility, put it as the last parameter.
 * But deprecated this interface. Please use `getTextBoundingRect` instead.
 */


function getTextRect(text, font, textAlign, textVerticalAlign, textPadding, rich, truncate, textLineHeight) {
  return textContain.getBoundingRect(text, font, textAlign, textVerticalAlign, textPadding, textLineHeight, rich, truncate);
}

exports.addCommas = addCommas;
exports.toCamelCase = toCamelCase;
exports.normalizeCssArray = normalizeCssArray;
exports.encodeHTML = encodeHTML;
exports.formatTpl = formatTpl;
exports.formatTplSimple = formatTplSimple;
exports.getTooltipMarker = getTooltipMarker;
exports.formatTime = formatTime;
exports.capitalFirst = capitalFirst;
exports.truncateText = truncateText;
exports.getTextBoundingRect = getTextBoundingRect;
exports.getTextRect = getTextRect;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdXRpbC9mb3JtYXQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi91dGlsL2Zvcm1hdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciB0ZXh0Q29udGFpbiA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb250YWluL3RleHRcIik7XG5cbnZhciBudW1iZXJVdGlsID0gcmVxdWlyZShcIi4vbnVtYmVyXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG4vLyBpbXBvcnQgVGV4dCBmcm9tICd6cmVuZGVyL3NyYy9ncmFwaGljL1RleHQnO1xuXG4vKipcbiAqIOavj+S4ieS9jem7mOiupOWKoCzmoLzlvI/ljJZcbiAqIEBwYXJhbSB7c3RyaW5nfG51bWJlcn0geFxuICogQHJldHVybiB7c3RyaW5nfVxuICovXG5mdW5jdGlvbiBhZGRDb21tYXMoeCkge1xuICBpZiAoaXNOYU4oeCkpIHtcbiAgICByZXR1cm4gJy0nO1xuICB9XG5cbiAgeCA9ICh4ICsgJycpLnNwbGl0KCcuJyk7XG4gIHJldHVybiB4WzBdLnJlcGxhY2UoLyhcXGR7MSwzfSkoPz0oPzpcXGR7M30pKyg/IVxcZCkpL2csICckMSwnKSArICh4Lmxlbmd0aCA+IDEgPyAnLicgKyB4WzFdIDogJycpO1xufVxuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ30gc3RyXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFt1cHBlckNhc2VGaXJzdD1mYWxzZV1cbiAqIEByZXR1cm4ge3N0cmluZ30gc3RyXG4gKi9cblxuXG5mdW5jdGlvbiB0b0NhbWVsQ2FzZShzdHIsIHVwcGVyQ2FzZUZpcnN0KSB7XG4gIHN0ciA9IChzdHIgfHwgJycpLnRvTG93ZXJDYXNlKCkucmVwbGFjZSgvLSguKS9nLCBmdW5jdGlvbiAobWF0Y2gsIGdyb3VwMSkge1xuICAgIHJldHVybiBncm91cDEudG9VcHBlckNhc2UoKTtcbiAgfSk7XG5cbiAgaWYgKHVwcGVyQ2FzZUZpcnN0ICYmIHN0cikge1xuICAgIHN0ciA9IHN0ci5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIHN0ci5zbGljZSgxKTtcbiAgfVxuXG4gIHJldHVybiBzdHI7XG59XG5cbnZhciBub3JtYWxpemVDc3NBcnJheSA9IHpyVXRpbC5ub3JtYWxpemVDc3NBcnJheTtcbnZhciByZXBsYWNlUmVnID0gLyhbJjw+XCInXSkvZztcbnZhciByZXBsYWNlTWFwID0ge1xuICAnJic6ICcmYW1wOycsXG4gICc8JzogJyZsdDsnLFxuICAnPic6ICcmZ3Q7JyxcbiAgJ1wiJzogJyZxdW90OycsXG4gICdcXCcnOiAnJiMzOTsnXG59O1xuXG5mdW5jdGlvbiBlbmNvZGVIVE1MKHNvdXJjZSkge1xuICByZXR1cm4gc291cmNlID09IG51bGwgPyAnJyA6IChzb3VyY2UgKyAnJykucmVwbGFjZShyZXBsYWNlUmVnLCBmdW5jdGlvbiAoc3RyLCBjKSB7XG4gICAgcmV0dXJuIHJlcGxhY2VNYXBbY107XG4gIH0pO1xufVxuXG52YXIgVFBMX1ZBUl9BTElBUyA9IFsnYScsICdiJywgJ2MnLCAnZCcsICdlJywgJ2YnLCAnZyddO1xuXG52YXIgd3JhcFZhciA9IGZ1bmN0aW9uICh2YXJOYW1lLCBzZXJpZXNJZHgpIHtcbiAgcmV0dXJuICd7JyArIHZhck5hbWUgKyAoc2VyaWVzSWR4ID09IG51bGwgPyAnJyA6IHNlcmllc0lkeCkgKyAnfSc7XG59O1xuLyoqXG4gKiBUZW1wbGF0ZSBmb3JtYXR0ZXJcbiAqIEBwYXJhbSB7c3RyaW5nfSB0cGxcbiAqIEBwYXJhbSB7QXJyYXkuPE9iamVjdD58T2JqZWN0fSBwYXJhbXNMaXN0XG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtlbmNvZGU9ZmFsc2VdXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKi9cblxuXG5mdW5jdGlvbiBmb3JtYXRUcGwodHBsLCBwYXJhbXNMaXN0LCBlbmNvZGUpIHtcbiAgaWYgKCF6clV0aWwuaXNBcnJheShwYXJhbXNMaXN0KSkge1xuICAgIHBhcmFtc0xpc3QgPSBbcGFyYW1zTGlzdF07XG4gIH1cblxuICB2YXIgc2VyaWVzTGVuID0gcGFyYW1zTGlzdC5sZW5ndGg7XG5cbiAgaWYgKCFzZXJpZXNMZW4pIHtcbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICB2YXIgJHZhcnMgPSBwYXJhbXNMaXN0WzBdLiR2YXJzIHx8IFtdO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgJHZhcnMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgYWxpYXMgPSBUUExfVkFSX0FMSUFTW2ldO1xuICAgIHRwbCA9IHRwbC5yZXBsYWNlKHdyYXBWYXIoYWxpYXMpLCB3cmFwVmFyKGFsaWFzLCAwKSk7XG4gIH1cblxuICBmb3IgKHZhciBzZXJpZXNJZHggPSAwOyBzZXJpZXNJZHggPCBzZXJpZXNMZW47IHNlcmllc0lkeCsrKSB7XG4gICAgZm9yICh2YXIgayA9IDA7IGsgPCAkdmFycy5sZW5ndGg7IGsrKykge1xuICAgICAgdmFyIHZhbCA9IHBhcmFtc0xpc3Rbc2VyaWVzSWR4XVskdmFyc1trXV07XG4gICAgICB0cGwgPSB0cGwucmVwbGFjZSh3cmFwVmFyKFRQTF9WQVJfQUxJQVNba10sIHNlcmllc0lkeCksIGVuY29kZSA/IGVuY29kZUhUTUwodmFsKSA6IHZhbCk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRwbDtcbn1cbi8qKlxuICogc2ltcGxlIFRlbXBsYXRlIGZvcm1hdHRlclxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSB0cGxcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXJhbVxuICogQHBhcmFtIHtib29sZWFufSBbZW5jb2RlPWZhbHNlXVxuICogQHJldHVybiB7c3RyaW5nfVxuICovXG5cblxuZnVuY3Rpb24gZm9ybWF0VHBsU2ltcGxlKHRwbCwgcGFyYW0sIGVuY29kZSkge1xuICB6clV0aWwuZWFjaChwYXJhbSwgZnVuY3Rpb24gKHZhbHVlLCBrZXkpIHtcbiAgICB0cGwgPSB0cGwucmVwbGFjZSgneycgKyBrZXkgKyAnfScsIGVuY29kZSA/IGVuY29kZUhUTUwodmFsdWUpIDogdmFsdWUpO1xuICB9KTtcbiAgcmV0dXJuIHRwbDtcbn1cbi8qKlxuICogQHBhcmFtIHtPYmplY3R8c3RyaW5nfSBbb3B0XSBJZiBzdHJpbmcsIG1lYW5zIGNvbG9yLlxuICogQHBhcmFtIHtzdHJpbmd9IFtvcHQuY29sb3JdXG4gKiBAcGFyYW0ge3N0cmluZ30gW29wdC5leHRyYUNzc1RleHRdXG4gKiBAcGFyYW0ge3N0cmluZ30gW29wdC50eXBlPSdpdGVtJ10gJ2l0ZW0nIG9yICdzdWJJdGVtJ1xuICogQHBhcmFtIHtzdHJpbmd9IFtvcHQucmVuZGVyTW9kZT0naHRtbCddIHJlbmRlciBtb2RlIG9mIHRvb2x0aXAsICdodG1sJyBvciAncmljaFRleHQnXG4gKiBAcGFyYW0ge3N0cmluZ30gW29wdC5tYXJrZXJJZD0nWCddIGlkIG5hbWUgZm9yIG1hcmtlci4gSWYgb25seSBvbmUgbWFya2VyIGlzIGluIGEgcmljaCB0ZXh0LCB0aGlzIGNhbiBiZSBvbWl0dGVkLlxuICogQHJldHVybiB7c3RyaW5nfVxuICovXG5cblxuZnVuY3Rpb24gZ2V0VG9vbHRpcE1hcmtlcihvcHQsIGV4dHJhQ3NzVGV4dCkge1xuICBvcHQgPSB6clV0aWwuaXNTdHJpbmcob3B0KSA/IHtcbiAgICBjb2xvcjogb3B0LFxuICAgIGV4dHJhQ3NzVGV4dDogZXh0cmFDc3NUZXh0XG4gIH0gOiBvcHQgfHwge307XG4gIHZhciBjb2xvciA9IG9wdC5jb2xvcjtcbiAgdmFyIHR5cGUgPSBvcHQudHlwZTtcbiAgdmFyIGV4dHJhQ3NzVGV4dCA9IG9wdC5leHRyYUNzc1RleHQ7XG4gIHZhciByZW5kZXJNb2RlID0gb3B0LnJlbmRlck1vZGUgfHwgJ2h0bWwnO1xuICB2YXIgbWFya2VySWQgPSBvcHQubWFya2VySWQgfHwgJ1gnO1xuXG4gIGlmICghY29sb3IpIHtcbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICBpZiAocmVuZGVyTW9kZSA9PT0gJ2h0bWwnKSB7XG4gICAgcmV0dXJuIHR5cGUgPT09ICdzdWJJdGVtJyA/ICc8c3BhbiBzdHlsZT1cImRpc3BsYXk6aW5saW5lLWJsb2NrO3ZlcnRpY2FsLWFsaWduOm1pZGRsZTttYXJnaW4tcmlnaHQ6OHB4O21hcmdpbi1sZWZ0OjNweDsnICsgJ2JvcmRlci1yYWRpdXM6NHB4O3dpZHRoOjRweDtoZWlnaHQ6NHB4O2JhY2tncm91bmQtY29sb3I6JyArIGVuY29kZUhUTUwoY29sb3IpICsgJzsnICsgKGV4dHJhQ3NzVGV4dCB8fCAnJykgKyAnXCI+PC9zcGFuPicgOiAnPHNwYW4gc3R5bGU9XCJkaXNwbGF5OmlubGluZS1ibG9jazttYXJnaW4tcmlnaHQ6NXB4OycgKyAnYm9yZGVyLXJhZGl1czoxMHB4O3dpZHRoOjEwcHg7aGVpZ2h0OjEwcHg7YmFja2dyb3VuZC1jb2xvcjonICsgZW5jb2RlSFRNTChjb2xvcikgKyAnOycgKyAoZXh0cmFDc3NUZXh0IHx8ICcnKSArICdcIj48L3NwYW4+JztcbiAgfSBlbHNlIHtcbiAgICAvLyBTcGFjZSBmb3IgcmljaCBlbGVtZW50IG1hcmtlclxuICAgIHJldHVybiB7XG4gICAgICByZW5kZXJNb2RlOiByZW5kZXJNb2RlLFxuICAgICAgY29udGVudDogJ3ttYXJrZXInICsgbWFya2VySWQgKyAnfH0gICcsXG4gICAgICBzdHlsZToge1xuICAgICAgICBjb2xvcjogY29sb3JcbiAgICAgIH1cbiAgICB9O1xuICB9XG59XG5cbmZ1bmN0aW9uIHBhZChzdHIsIGxlbikge1xuICBzdHIgKz0gJyc7XG4gIHJldHVybiAnMDAwMCcuc3Vic3RyKDAsIGxlbiAtIHN0ci5sZW5ndGgpICsgc3RyO1xufVxuLyoqXG4gKiBJU08gRGF0ZSBmb3JtYXRcbiAqIEBwYXJhbSB7c3RyaW5nfSB0cGxcbiAqIEBwYXJhbSB7bnVtYmVyfSB2YWx1ZVxuICogQHBhcmFtIHtib29sZWFufSBbaXNVVEM9ZmFsc2VdIERlZmF1bHQgaW4gbG9jYWwgdGltZS5cbiAqICAgICAgICAgICBzZWUgYG1vZHVsZTplY2hhcnRzL3NjYWxlL1RpbWVgXG4gKiAgICAgICAgICAgYW5kIGBtb2R1bGU6ZWNoYXJ0cy91dGlsL251bWJlciNwYXJzZURhdGVgLlxuICogQGlubmVyXG4gKi9cblxuXG5mdW5jdGlvbiBmb3JtYXRUaW1lKHRwbCwgdmFsdWUsIGlzVVRDKSB7XG4gIGlmICh0cGwgPT09ICd3ZWVrJyB8fCB0cGwgPT09ICdtb250aCcgfHwgdHBsID09PSAncXVhcnRlcicgfHwgdHBsID09PSAnaGFsZi15ZWFyJyB8fCB0cGwgPT09ICd5ZWFyJykge1xuICAgIHRwbCA9ICdNTS1kZFxcbnl5eXknO1xuICB9XG5cbiAgdmFyIGRhdGUgPSBudW1iZXJVdGlsLnBhcnNlRGF0ZSh2YWx1ZSk7XG4gIHZhciB1dGMgPSBpc1VUQyA/ICdVVEMnIDogJyc7XG4gIHZhciB5ID0gZGF0ZVsnZ2V0JyArIHV0YyArICdGdWxsWWVhciddKCk7XG4gIHZhciBNID0gZGF0ZVsnZ2V0JyArIHV0YyArICdNb250aCddKCkgKyAxO1xuICB2YXIgZCA9IGRhdGVbJ2dldCcgKyB1dGMgKyAnRGF0ZSddKCk7XG4gIHZhciBoID0gZGF0ZVsnZ2V0JyArIHV0YyArICdIb3VycyddKCk7XG4gIHZhciBtID0gZGF0ZVsnZ2V0JyArIHV0YyArICdNaW51dGVzJ10oKTtcbiAgdmFyIHMgPSBkYXRlWydnZXQnICsgdXRjICsgJ1NlY29uZHMnXSgpO1xuICB2YXIgUyA9IGRhdGVbJ2dldCcgKyB1dGMgKyAnTWlsbGlzZWNvbmRzJ10oKTtcbiAgdHBsID0gdHBsLnJlcGxhY2UoJ01NJywgcGFkKE0sIDIpKS5yZXBsYWNlKCdNJywgTSkucmVwbGFjZSgneXl5eScsIHkpLnJlcGxhY2UoJ3l5JywgeSAlIDEwMCkucmVwbGFjZSgnZGQnLCBwYWQoZCwgMikpLnJlcGxhY2UoJ2QnLCBkKS5yZXBsYWNlKCdoaCcsIHBhZChoLCAyKSkucmVwbGFjZSgnaCcsIGgpLnJlcGxhY2UoJ21tJywgcGFkKG0sIDIpKS5yZXBsYWNlKCdtJywgbSkucmVwbGFjZSgnc3MnLCBwYWQocywgMikpLnJlcGxhY2UoJ3MnLCBzKS5yZXBsYWNlKCdTU1MnLCBwYWQoUywgMykpO1xuICByZXR1cm4gdHBsO1xufVxuLyoqXG4gKiBDYXBpdGFsIGZpcnN0XG4gKiBAcGFyYW0ge3N0cmluZ30gc3RyXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKi9cblxuXG5mdW5jdGlvbiBjYXBpdGFsRmlyc3Qoc3RyKSB7XG4gIHJldHVybiBzdHIgPyBzdHIuY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBzdHIuc3Vic3RyKDEpIDogc3RyO1xufVxuXG52YXIgdHJ1bmNhdGVUZXh0ID0gdGV4dENvbnRhaW4udHJ1bmNhdGVUZXh0O1xuLyoqXG4gKiBAcHVibGljXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0XG4gKiBAcGFyYW0ge3N0cmluZ30gb3B0LnRleHRcbiAqIEBwYXJhbSB7c3RyaW5nfSBvcHQuZm9udFxuICogQHBhcmFtIHtzdHJpbmd9IFtvcHQudGV4dEFsaWduPSdsZWZ0J11cbiAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0LnRleHRWZXJ0aWNhbEFsaWduPSd0b3AnXVxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gW29wdC50ZXh0UGFkZGluZ11cbiAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0LnRleHRMaW5lSGVpZ2h0XVxuICogQHBhcmFtIHtPYmplY3R9IFtvcHQucmljaF1cbiAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0LnRydW5jYXRlXVxuICogQHJldHVybiB7T2JqZWN0fSB7eCwgeSwgd2lkdGgsIGhlaWdodCwgbGluZUhlaWdodH1cbiAqL1xuXG5mdW5jdGlvbiBnZXRUZXh0Qm91bmRpbmdSZWN0KG9wdCkge1xuICByZXR1cm4gdGV4dENvbnRhaW4uZ2V0Qm91bmRpbmdSZWN0KG9wdC50ZXh0LCBvcHQuZm9udCwgb3B0LnRleHRBbGlnbiwgb3B0LnRleHRWZXJ0aWNhbEFsaWduLCBvcHQudGV4dFBhZGRpbmcsIG9wdC50ZXh0TGluZUhlaWdodCwgb3B0LnJpY2gsIG9wdC50cnVuY2F0ZSk7XG59XG4vKipcbiAqIEBkZXByZWNhdGVkXG4gKiB0aGUgYHRleHRMaW5lSGVpZ2h0YCB3YXMgYWRkZWQgbGF0ZXIuXG4gKiBGb3IgYmFja3dhcmQgY29tcGF0aWJsaWxpdHksIHB1dCBpdCBhcyB0aGUgbGFzdCBwYXJhbWV0ZXIuXG4gKiBCdXQgZGVwcmVjYXRlZCB0aGlzIGludGVyZmFjZS4gUGxlYXNlIHVzZSBgZ2V0VGV4dEJvdW5kaW5nUmVjdGAgaW5zdGVhZC5cbiAqL1xuXG5cbmZ1bmN0aW9uIGdldFRleHRSZWN0KHRleHQsIGZvbnQsIHRleHRBbGlnbiwgdGV4dFZlcnRpY2FsQWxpZ24sIHRleHRQYWRkaW5nLCByaWNoLCB0cnVuY2F0ZSwgdGV4dExpbmVIZWlnaHQpIHtcbiAgcmV0dXJuIHRleHRDb250YWluLmdldEJvdW5kaW5nUmVjdCh0ZXh0LCBmb250LCB0ZXh0QWxpZ24sIHRleHRWZXJ0aWNhbEFsaWduLCB0ZXh0UGFkZGluZywgdGV4dExpbmVIZWlnaHQsIHJpY2gsIHRydW5jYXRlKTtcbn1cblxuZXhwb3J0cy5hZGRDb21tYXMgPSBhZGRDb21tYXM7XG5leHBvcnRzLnRvQ2FtZWxDYXNlID0gdG9DYW1lbENhc2U7XG5leHBvcnRzLm5vcm1hbGl6ZUNzc0FycmF5ID0gbm9ybWFsaXplQ3NzQXJyYXk7XG5leHBvcnRzLmVuY29kZUhUTUwgPSBlbmNvZGVIVE1MO1xuZXhwb3J0cy5mb3JtYXRUcGwgPSBmb3JtYXRUcGw7XG5leHBvcnRzLmZvcm1hdFRwbFNpbXBsZSA9IGZvcm1hdFRwbFNpbXBsZTtcbmV4cG9ydHMuZ2V0VG9vbHRpcE1hcmtlciA9IGdldFRvb2x0aXBNYXJrZXI7XG5leHBvcnRzLmZvcm1hdFRpbWUgPSBmb3JtYXRUaW1lO1xuZXhwb3J0cy5jYXBpdGFsRmlyc3QgPSBjYXBpdGFsRmlyc3Q7XG5leHBvcnRzLnRydW5jYXRlVGV4dCA9IHRydW5jYXRlVGV4dDtcbmV4cG9ydHMuZ2V0VGV4dEJvdW5kaW5nUmVjdCA9IGdldFRleHRCb3VuZGluZ1JlY3Q7XG5leHBvcnRzLmdldFRleHRSZWN0ID0gZ2V0VGV4dFJlY3Q7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTs7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/util/format.js
