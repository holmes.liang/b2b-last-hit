

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var PropTypes = __importStar(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

var mini_store_1 = __webpack_require__(/*! mini-store */ "./node_modules/mini-store/lib/index.js");

var classnames_1 = __importDefault(__webpack_require__(/*! classnames */ "./node_modules/classnames/index.js"));

var ColGroup_1 = __importDefault(__webpack_require__(/*! ./ColGroup */ "./node_modules/rc-table/es/ColGroup.js"));

var TableHeader_1 = __importDefault(__webpack_require__(/*! ./TableHeader */ "./node_modules/rc-table/es/TableHeader.js"));

var TableRow_1 = __importDefault(__webpack_require__(/*! ./TableRow */ "./node_modules/rc-table/es/TableRow.js"));

var ExpandableRow_1 = __importDefault(__webpack_require__(/*! ./ExpandableRow */ "./node_modules/rc-table/es/ExpandableRow.js"));

var BaseTable =
/*#__PURE__*/
function (_React$Component) {
  _inherits(BaseTable, _React$Component);

  function BaseTable() {
    var _this;

    _classCallCheck(this, BaseTable);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(BaseTable).apply(this, arguments));

    _this.handleRowHover = function (isHover, key) {
      _this.props.store.setState({
        currentHoverKey: isHover ? key : null
      });
    };

    _this.renderRows = function (renderData, indent) {
      var ancestorKeys = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
      var table = _this.context.table;
      var columnManager = table.columnManager,
          components = table.components;
      var _table$props = table.props,
          prefixCls = _table$props.prefixCls,
          childrenColumnName = _table$props.childrenColumnName,
          rowClassName = _table$props.rowClassName,
          rowRef = _table$props.rowRef,
          onRowClick = _table$props.onRowClick,
          onRowDoubleClick = _table$props.onRowDoubleClick,
          onRowContextMenu = _table$props.onRowContextMenu,
          onRowMouseEnter = _table$props.onRowMouseEnter,
          onRowMouseLeave = _table$props.onRowMouseLeave,
          onRow = _table$props.onRow;
      var _this$props = _this.props,
          getRowKey = _this$props.getRowKey,
          fixed = _this$props.fixed,
          expander = _this$props.expander,
          isAnyColumnsFixed = _this$props.isAnyColumnsFixed;
      var rows = [];

      var _loop = function _loop(i) {
        var record = renderData[i];
        var key = getRowKey(record, i);
        var className = typeof rowClassName === 'string' ? rowClassName : rowClassName(record, i, indent);
        var onHoverProps = {};

        if (columnManager.isAnyColumnsFixed()) {
          onHoverProps.onHover = _this.handleRowHover;
        }

        var leafColumns = void 0;

        if (fixed === 'left') {
          leafColumns = columnManager.leftLeafColumns();
        } else if (fixed === 'right') {
          leafColumns = columnManager.rightLeafColumns();
        } else {
          leafColumns = _this.getColumns(columnManager.leafColumns());
        }

        var rowPrefixCls = "".concat(prefixCls, "-row");
        var row = React.createElement(ExpandableRow_1.default, Object.assign({}, expander.props, {
          fixed: fixed,
          index: i,
          prefixCls: rowPrefixCls,
          record: record,
          key: key,
          rowKey: key,
          onRowClick: onRowClick,
          needIndentSpaced: expander.needIndentSpaced,
          onExpandedChange: expander.handleExpandChange
        }), function (expandableRow) {
          return React.createElement(TableRow_1.default, Object.assign({
            fixed: fixed,
            indent: indent,
            className: className,
            record: record,
            index: i,
            prefixCls: rowPrefixCls,
            childrenColumnName: childrenColumnName,
            columns: leafColumns,
            onRow: onRow,
            onRowDoubleClick: onRowDoubleClick,
            onRowContextMenu: onRowContextMenu,
            onRowMouseEnter: onRowMouseEnter,
            onRowMouseLeave: onRowMouseLeave
          }, onHoverProps, {
            rowKey: key,
            ancestorKeys: ancestorKeys,
            ref: rowRef(record, i, indent),
            components: components,
            isAnyColumnsFixed: isAnyColumnsFixed
          }, expandableRow));
        });
        rows.push(row);
        expander.renderRows(_this.renderRows, rows, record, i, indent, fixed, key, ancestorKeys);
      };

      for (var i = 0; i < renderData.length; i += 1) {
        _loop(i);
      }

      return rows;
    };

    return _this;
  }

  _createClass(BaseTable, [{
    key: "getColumns",
    value: function getColumns(cols) {
      var _this$props2 = this.props,
          _this$props2$columns = _this$props2.columns,
          columns = _this$props2$columns === void 0 ? [] : _this$props2$columns,
          fixed = _this$props2.fixed;
      var table = this.context.table;
      var prefixCls = table.props.prefixCls;
      return (cols || columns).map(function (column) {
        return _objectSpread({}, column, {
          className: !!column.fixed && !fixed ? classnames_1.default("".concat(prefixCls, "-fixed-columns-in-body"), column.className) : column.className
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var table = this.context.table;
      var components = table.components;
      var _table$props2 = table.props,
          prefixCls = _table$props2.prefixCls,
          scroll = _table$props2.scroll,
          data = _table$props2.data,
          getBodyWrapper = _table$props2.getBodyWrapper;
      var _this$props3 = this.props,
          expander = _this$props3.expander,
          tableClassName = _this$props3.tableClassName,
          hasHead = _this$props3.hasHead,
          hasBody = _this$props3.hasBody,
          fixed = _this$props3.fixed;
      var tableStyle = {};

      if (!fixed && scroll.x) {
        // not set width, then use content fixed width
        tableStyle.width = scroll.x === true ? 'max-content' : scroll.x;
      }

      var Table = hasBody ? components.table : 'table';
      var BodyWrapper = components.body.wrapper;
      var body;

      if (hasBody) {
        body = React.createElement(BodyWrapper, {
          className: "".concat(prefixCls, "-tbody")
        }, this.renderRows(data, 0));

        if (getBodyWrapper) {
          body = getBodyWrapper(body);
        }
      }

      var columns = this.getColumns();
      return React.createElement(Table, {
        className: tableClassName,
        style: tableStyle,
        key: "table"
      }, React.createElement(ColGroup_1.default, {
        columns: columns,
        fixed: fixed
      }), hasHead && React.createElement(TableHeader_1.default, {
        expander: expander,
        columns: columns,
        fixed: fixed
      }), body);
    }
  }]);

  return BaseTable;
}(React.Component);

BaseTable.contextTypes = {
  table: PropTypes.any
};
exports.default = mini_store_1.connect()(BaseTable);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvQmFzZVRhYmxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvQmFzZVRhYmxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuXG5mdW5jdGlvbiBfdHlwZW9mKG9iaikgeyBpZiAodHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIpIHsgX3R5cGVvZiA9IGZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9OyB9IGVsc2UgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07IH0gcmV0dXJuIF90eXBlb2Yob2JqKTsgfVxuXG5mdW5jdGlvbiBvd25LZXlzKG9iamVjdCwgZW51bWVyYWJsZU9ubHkpIHsgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhvYmplY3QpOyBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykgeyB2YXIgc3ltYm9scyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMob2JqZWN0KTsgaWYgKGVudW1lcmFibGVPbmx5KSBzeW1ib2xzID0gc3ltYm9scy5maWx0ZXIoZnVuY3Rpb24gKHN5bSkgeyByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmplY3QsIHN5bSkuZW51bWVyYWJsZTsgfSk7IGtleXMucHVzaC5hcHBseShrZXlzLCBzeW1ib2xzKTsgfSByZXR1cm4ga2V5czsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0U3ByZWFkKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldICE9IG51bGwgPyBhcmd1bWVudHNbaV0gOiB7fTsgaWYgKGkgJSAyKSB7IG93bktleXMoT2JqZWN0KHNvdXJjZSksIHRydWUpLmZvckVhY2goZnVuY3Rpb24gKGtleSkgeyBfZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHNvdXJjZVtrZXldKTsgfSk7IH0gZWxzZSBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcnMpIHsgT2JqZWN0LmRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycyhzb3VyY2UpKTsgfSBlbHNlIHsgb3duS2V5cyhPYmplY3Qoc291cmNlKSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihzb3VyY2UsIGtleSkpOyB9KTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnR5KG9iaiwga2V5LCB2YWx1ZSkgeyBpZiAoa2V5IGluIG9iaikgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHsgdmFsdWU6IHZhbHVlLCBlbnVtZXJhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUsIHdyaXRhYmxlOiB0cnVlIH0pOyB9IGVsc2UgeyBvYmpba2V5XSA9IHZhbHVlOyB9IHJldHVybiBvYmo7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfVxuXG5mdW5jdGlvbiBfY3JlYXRlQ2xhc3MoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmIChjYWxsICYmIChfdHlwZW9mKGNhbGwpID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpKSB7IHJldHVybiBjYWxsOyB9IHJldHVybiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpOyB9XG5cbmZ1bmN0aW9uIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoc2VsZikgeyBpZiAoc2VsZiA9PT0gdm9pZCAwKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gc2VsZjsgfVxuXG5mdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyBfZ2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3QuZ2V0UHJvdG90eXBlT2YgOiBmdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyByZXR1cm4gby5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKG8pOyB9OyByZXR1cm4gX2dldFByb3RvdHlwZU9mKG8pOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvblwiKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBfc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpOyB9XG5cbmZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7IF9zZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fCBmdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBvLl9fcHJvdG9fXyA9IHA7IHJldHVybiBvOyB9OyByZXR1cm4gX3NldFByb3RvdHlwZU9mKG8sIHApOyB9XG5cbnZhciBfX2ltcG9ydFN0YXIgPSB0aGlzICYmIHRoaXMuX19pbXBvcnRTdGFyIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcbiAgdmFyIHJlc3VsdCA9IHt9O1xuICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSB7XG4gICAgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcbiAgfVxuICByZXN1bHRbXCJkZWZhdWx0XCJdID0gbW9kO1xuICByZXR1cm4gcmVzdWx0O1xufTtcblxudmFyIF9faW1wb3J0RGVmYXVsdCA9IHRoaXMgJiYgdGhpcy5fX2ltcG9ydERlZmF1bHQgfHwgZnVuY3Rpb24gKG1vZCkge1xuICByZXR1cm4gbW9kICYmIG1vZC5fX2VzTW9kdWxlID8gbW9kIDoge1xuICAgIFwiZGVmYXVsdFwiOiBtb2RcbiAgfTtcbn07XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBSZWFjdCA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicmVhY3RcIikpO1xuXG52YXIgUHJvcFR5cGVzID0gX19pbXBvcnRTdGFyKHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpKTtcblxudmFyIG1pbmlfc3RvcmVfMSA9IHJlcXVpcmUoXCJtaW5pLXN0b3JlXCIpO1xuXG52YXIgY2xhc3NuYW1lc18xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJjbGFzc25hbWVzXCIpKTtcblxudmFyIENvbEdyb3VwXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcIi4vQ29sR3JvdXBcIikpO1xuXG52YXIgVGFibGVIZWFkZXJfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwiLi9UYWJsZUhlYWRlclwiKSk7XG5cbnZhciBUYWJsZVJvd18xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCIuL1RhYmxlUm93XCIpKTtcblxudmFyIEV4cGFuZGFibGVSb3dfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwiLi9FeHBhbmRhYmxlUm93XCIpKTtcblxudmFyIEJhc2VUYWJsZSA9XG4vKiNfX1BVUkVfXyovXG5mdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoQmFzZVRhYmxlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBCYXNlVGFibGUoKSB7XG4gICAgdmFyIF90aGlzO1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIEJhc2VUYWJsZSk7XG5cbiAgICBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9nZXRQcm90b3R5cGVPZihCYXNlVGFibGUpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuXG4gICAgX3RoaXMuaGFuZGxlUm93SG92ZXIgPSBmdW5jdGlvbiAoaXNIb3Zlciwga2V5KSB7XG4gICAgICBfdGhpcy5wcm9wcy5zdG9yZS5zZXRTdGF0ZSh7XG4gICAgICAgIGN1cnJlbnRIb3ZlcktleTogaXNIb3ZlciA/IGtleSA6IG51bGxcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICBfdGhpcy5yZW5kZXJSb3dzID0gZnVuY3Rpb24gKHJlbmRlckRhdGEsIGluZGVudCkge1xuICAgICAgdmFyIGFuY2VzdG9yS2V5cyA9IGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIGFyZ3VtZW50c1syXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzJdIDogW107XG4gICAgICB2YXIgdGFibGUgPSBfdGhpcy5jb250ZXh0LnRhYmxlO1xuICAgICAgdmFyIGNvbHVtbk1hbmFnZXIgPSB0YWJsZS5jb2x1bW5NYW5hZ2VyLFxuICAgICAgICAgIGNvbXBvbmVudHMgPSB0YWJsZS5jb21wb25lbnRzO1xuICAgICAgdmFyIF90YWJsZSRwcm9wcyA9IHRhYmxlLnByb3BzLFxuICAgICAgICAgIHByZWZpeENscyA9IF90YWJsZSRwcm9wcy5wcmVmaXhDbHMsXG4gICAgICAgICAgY2hpbGRyZW5Db2x1bW5OYW1lID0gX3RhYmxlJHByb3BzLmNoaWxkcmVuQ29sdW1uTmFtZSxcbiAgICAgICAgICByb3dDbGFzc05hbWUgPSBfdGFibGUkcHJvcHMucm93Q2xhc3NOYW1lLFxuICAgICAgICAgIHJvd1JlZiA9IF90YWJsZSRwcm9wcy5yb3dSZWYsXG4gICAgICAgICAgb25Sb3dDbGljayA9IF90YWJsZSRwcm9wcy5vblJvd0NsaWNrLFxuICAgICAgICAgIG9uUm93RG91YmxlQ2xpY2sgPSBfdGFibGUkcHJvcHMub25Sb3dEb3VibGVDbGljayxcbiAgICAgICAgICBvblJvd0NvbnRleHRNZW51ID0gX3RhYmxlJHByb3BzLm9uUm93Q29udGV4dE1lbnUsXG4gICAgICAgICAgb25Sb3dNb3VzZUVudGVyID0gX3RhYmxlJHByb3BzLm9uUm93TW91c2VFbnRlcixcbiAgICAgICAgICBvblJvd01vdXNlTGVhdmUgPSBfdGFibGUkcHJvcHMub25Sb3dNb3VzZUxlYXZlLFxuICAgICAgICAgIG9uUm93ID0gX3RhYmxlJHByb3BzLm9uUm93O1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgZ2V0Um93S2V5ID0gX3RoaXMkcHJvcHMuZ2V0Um93S2V5LFxuICAgICAgICAgIGZpeGVkID0gX3RoaXMkcHJvcHMuZml4ZWQsXG4gICAgICAgICAgZXhwYW5kZXIgPSBfdGhpcyRwcm9wcy5leHBhbmRlcixcbiAgICAgICAgICBpc0FueUNvbHVtbnNGaXhlZCA9IF90aGlzJHByb3BzLmlzQW55Q29sdW1uc0ZpeGVkO1xuICAgICAgdmFyIHJvd3MgPSBbXTtcblxuICAgICAgdmFyIF9sb29wID0gZnVuY3Rpb24gX2xvb3AoaSkge1xuICAgICAgICB2YXIgcmVjb3JkID0gcmVuZGVyRGF0YVtpXTtcbiAgICAgICAgdmFyIGtleSA9IGdldFJvd0tleShyZWNvcmQsIGkpO1xuICAgICAgICB2YXIgY2xhc3NOYW1lID0gdHlwZW9mIHJvd0NsYXNzTmFtZSA9PT0gJ3N0cmluZycgPyByb3dDbGFzc05hbWUgOiByb3dDbGFzc05hbWUocmVjb3JkLCBpLCBpbmRlbnQpO1xuICAgICAgICB2YXIgb25Ib3ZlclByb3BzID0ge307XG5cbiAgICAgICAgaWYgKGNvbHVtbk1hbmFnZXIuaXNBbnlDb2x1bW5zRml4ZWQoKSkge1xuICAgICAgICAgIG9uSG92ZXJQcm9wcy5vbkhvdmVyID0gX3RoaXMuaGFuZGxlUm93SG92ZXI7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgbGVhZkNvbHVtbnMgPSB2b2lkIDA7XG5cbiAgICAgICAgaWYgKGZpeGVkID09PSAnbGVmdCcpIHtcbiAgICAgICAgICBsZWFmQ29sdW1ucyA9IGNvbHVtbk1hbmFnZXIubGVmdExlYWZDb2x1bW5zKCk7XG4gICAgICAgIH0gZWxzZSBpZiAoZml4ZWQgPT09ICdyaWdodCcpIHtcbiAgICAgICAgICBsZWFmQ29sdW1ucyA9IGNvbHVtbk1hbmFnZXIucmlnaHRMZWFmQ29sdW1ucygpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGxlYWZDb2x1bW5zID0gX3RoaXMuZ2V0Q29sdW1ucyhjb2x1bW5NYW5hZ2VyLmxlYWZDb2x1bW5zKCkpO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIHJvd1ByZWZpeENscyA9IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItcm93XCIpO1xuICAgICAgICB2YXIgcm93ID0gUmVhY3QuY3JlYXRlRWxlbWVudChFeHBhbmRhYmxlUm93XzEuZGVmYXVsdCwgT2JqZWN0LmFzc2lnbih7fSwgZXhwYW5kZXIucHJvcHMsIHtcbiAgICAgICAgICBmaXhlZDogZml4ZWQsXG4gICAgICAgICAgaW5kZXg6IGksXG4gICAgICAgICAgcHJlZml4Q2xzOiByb3dQcmVmaXhDbHMsXG4gICAgICAgICAgcmVjb3JkOiByZWNvcmQsXG4gICAgICAgICAga2V5OiBrZXksXG4gICAgICAgICAgcm93S2V5OiBrZXksXG4gICAgICAgICAgb25Sb3dDbGljazogb25Sb3dDbGljayxcbiAgICAgICAgICBuZWVkSW5kZW50U3BhY2VkOiBleHBhbmRlci5uZWVkSW5kZW50U3BhY2VkLFxuICAgICAgICAgIG9uRXhwYW5kZWRDaGFuZ2U6IGV4cGFuZGVyLmhhbmRsZUV4cGFuZENoYW5nZVxuICAgICAgICB9KSwgZnVuY3Rpb24gKGV4cGFuZGFibGVSb3cpIHtcbiAgICAgICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChUYWJsZVJvd18xLmRlZmF1bHQsIE9iamVjdC5hc3NpZ24oe1xuICAgICAgICAgICAgZml4ZWQ6IGZpeGVkLFxuICAgICAgICAgICAgaW5kZW50OiBpbmRlbnQsXG4gICAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICAgIHJlY29yZDogcmVjb3JkLFxuICAgICAgICAgICAgaW5kZXg6IGksXG4gICAgICAgICAgICBwcmVmaXhDbHM6IHJvd1ByZWZpeENscyxcbiAgICAgICAgICAgIGNoaWxkcmVuQ29sdW1uTmFtZTogY2hpbGRyZW5Db2x1bW5OYW1lLFxuICAgICAgICAgICAgY29sdW1uczogbGVhZkNvbHVtbnMsXG4gICAgICAgICAgICBvblJvdzogb25Sb3csXG4gICAgICAgICAgICBvblJvd0RvdWJsZUNsaWNrOiBvblJvd0RvdWJsZUNsaWNrLFxuICAgICAgICAgICAgb25Sb3dDb250ZXh0TWVudTogb25Sb3dDb250ZXh0TWVudSxcbiAgICAgICAgICAgIG9uUm93TW91c2VFbnRlcjogb25Sb3dNb3VzZUVudGVyLFxuICAgICAgICAgICAgb25Sb3dNb3VzZUxlYXZlOiBvblJvd01vdXNlTGVhdmVcbiAgICAgICAgICB9LCBvbkhvdmVyUHJvcHMsIHtcbiAgICAgICAgICAgIHJvd0tleToga2V5LFxuICAgICAgICAgICAgYW5jZXN0b3JLZXlzOiBhbmNlc3RvcktleXMsXG4gICAgICAgICAgICByZWY6IHJvd1JlZihyZWNvcmQsIGksIGluZGVudCksXG4gICAgICAgICAgICBjb21wb25lbnRzOiBjb21wb25lbnRzLFxuICAgICAgICAgICAgaXNBbnlDb2x1bW5zRml4ZWQ6IGlzQW55Q29sdW1uc0ZpeGVkXG4gICAgICAgICAgfSwgZXhwYW5kYWJsZVJvdykpO1xuICAgICAgICB9KTtcbiAgICAgICAgcm93cy5wdXNoKHJvdyk7XG4gICAgICAgIGV4cGFuZGVyLnJlbmRlclJvd3MoX3RoaXMucmVuZGVyUm93cywgcm93cywgcmVjb3JkLCBpLCBpbmRlbnQsIGZpeGVkLCBrZXksIGFuY2VzdG9yS2V5cyk7XG4gICAgICB9O1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHJlbmRlckRhdGEubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgICAgX2xvb3AoaSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiByb3dzO1xuICAgIH07XG5cbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoQmFzZVRhYmxlLCBbe1xuICAgIGtleTogXCJnZXRDb2x1bW5zXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldENvbHVtbnMoY29scykge1xuICAgICAgdmFyIF90aGlzJHByb3BzMiA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgX3RoaXMkcHJvcHMyJGNvbHVtbnMgPSBfdGhpcyRwcm9wczIuY29sdW1ucyxcbiAgICAgICAgICBjb2x1bW5zID0gX3RoaXMkcHJvcHMyJGNvbHVtbnMgPT09IHZvaWQgMCA/IFtdIDogX3RoaXMkcHJvcHMyJGNvbHVtbnMsXG4gICAgICAgICAgZml4ZWQgPSBfdGhpcyRwcm9wczIuZml4ZWQ7XG4gICAgICB2YXIgdGFibGUgPSB0aGlzLmNvbnRleHQudGFibGU7XG4gICAgICB2YXIgcHJlZml4Q2xzID0gdGFibGUucHJvcHMucHJlZml4Q2xzO1xuICAgICAgcmV0dXJuIChjb2xzIHx8IGNvbHVtbnMpLm1hcChmdW5jdGlvbiAoY29sdW1uKSB7XG4gICAgICAgIHJldHVybiBfb2JqZWN0U3ByZWFkKHt9LCBjb2x1bW4sIHtcbiAgICAgICAgICBjbGFzc05hbWU6ICEhY29sdW1uLmZpeGVkICYmICFmaXhlZCA/IGNsYXNzbmFtZXNfMS5kZWZhdWx0KFwiXCIuY29uY2F0KHByZWZpeENscywgXCItZml4ZWQtY29sdW1ucy1pbi1ib2R5XCIpLCBjb2x1bW4uY2xhc3NOYW1lKSA6IGNvbHVtbi5jbGFzc05hbWVcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwicmVuZGVyXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciB0YWJsZSA9IHRoaXMuY29udGV4dC50YWJsZTtcbiAgICAgIHZhciBjb21wb25lbnRzID0gdGFibGUuY29tcG9uZW50cztcbiAgICAgIHZhciBfdGFibGUkcHJvcHMyID0gdGFibGUucHJvcHMsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3RhYmxlJHByb3BzMi5wcmVmaXhDbHMsXG4gICAgICAgICAgc2Nyb2xsID0gX3RhYmxlJHByb3BzMi5zY3JvbGwsXG4gICAgICAgICAgZGF0YSA9IF90YWJsZSRwcm9wczIuZGF0YSxcbiAgICAgICAgICBnZXRCb2R5V3JhcHBlciA9IF90YWJsZSRwcm9wczIuZ2V0Qm9keVdyYXBwZXI7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBleHBhbmRlciA9IF90aGlzJHByb3BzMy5leHBhbmRlcixcbiAgICAgICAgICB0YWJsZUNsYXNzTmFtZSA9IF90aGlzJHByb3BzMy50YWJsZUNsYXNzTmFtZSxcbiAgICAgICAgICBoYXNIZWFkID0gX3RoaXMkcHJvcHMzLmhhc0hlYWQsXG4gICAgICAgICAgaGFzQm9keSA9IF90aGlzJHByb3BzMy5oYXNCb2R5LFxuICAgICAgICAgIGZpeGVkID0gX3RoaXMkcHJvcHMzLmZpeGVkO1xuICAgICAgdmFyIHRhYmxlU3R5bGUgPSB7fTtcblxuICAgICAgaWYgKCFmaXhlZCAmJiBzY3JvbGwueCkge1xuICAgICAgICAvLyBub3Qgc2V0IHdpZHRoLCB0aGVuIHVzZSBjb250ZW50IGZpeGVkIHdpZHRoXG4gICAgICAgIHRhYmxlU3R5bGUud2lkdGggPSBzY3JvbGwueCA9PT0gdHJ1ZSA/ICdtYXgtY29udGVudCcgOiBzY3JvbGwueDtcbiAgICAgIH1cblxuICAgICAgdmFyIFRhYmxlID0gaGFzQm9keSA/IGNvbXBvbmVudHMudGFibGUgOiAndGFibGUnO1xuICAgICAgdmFyIEJvZHlXcmFwcGVyID0gY29tcG9uZW50cy5ib2R5LndyYXBwZXI7XG4gICAgICB2YXIgYm9keTtcblxuICAgICAgaWYgKGhhc0JvZHkpIHtcbiAgICAgICAgYm9keSA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoQm9keVdyYXBwZXIsIHtcbiAgICAgICAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItdGJvZHlcIilcbiAgICAgICAgfSwgdGhpcy5yZW5kZXJSb3dzKGRhdGEsIDApKTtcblxuICAgICAgICBpZiAoZ2V0Qm9keVdyYXBwZXIpIHtcbiAgICAgICAgICBib2R5ID0gZ2V0Qm9keVdyYXBwZXIoYm9keSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgdmFyIGNvbHVtbnMgPSB0aGlzLmdldENvbHVtbnMoKTtcbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFRhYmxlLCB7XG4gICAgICAgIGNsYXNzTmFtZTogdGFibGVDbGFzc05hbWUsXG4gICAgICAgIHN0eWxlOiB0YWJsZVN0eWxlLFxuICAgICAgICBrZXk6IFwidGFibGVcIlxuICAgICAgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChDb2xHcm91cF8xLmRlZmF1bHQsIHtcbiAgICAgICAgY29sdW1uczogY29sdW1ucyxcbiAgICAgICAgZml4ZWQ6IGZpeGVkXG4gICAgICB9KSwgaGFzSGVhZCAmJiBSZWFjdC5jcmVhdGVFbGVtZW50KFRhYmxlSGVhZGVyXzEuZGVmYXVsdCwge1xuICAgICAgICBleHBhbmRlcjogZXhwYW5kZXIsXG4gICAgICAgIGNvbHVtbnM6IGNvbHVtbnMsXG4gICAgICAgIGZpeGVkOiBmaXhlZFxuICAgICAgfSksIGJvZHkpO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBCYXNlVGFibGU7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cbkJhc2VUYWJsZS5jb250ZXh0VHlwZXMgPSB7XG4gIHRhYmxlOiBQcm9wVHlwZXMuYW55XG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gbWluaV9zdG9yZV8xLmNvbm5lY3QoKShCYXNlVGFibGUpOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBZEE7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQWxEQTtBQUNBO0FBb0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/BaseTable.js
