var Animator = __webpack_require__(/*! ../animation/Animator */ "./node_modules/zrender/lib/animation/Animator.js");

var log = __webpack_require__(/*! ../core/log */ "./node_modules/zrender/lib/core/log.js");

var _util = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var isString = _util.isString;
var isFunction = _util.isFunction;
var isObject = _util.isObject;
var isArrayLike = _util.isArrayLike;
var indexOf = _util.indexOf;
/**
 * @alias modue:zrender/mixin/Animatable
 * @constructor
 */

var Animatable = function Animatable() {
  /**
   * @type {Array.<module:zrender/animation/Animator>}
   * @readOnly
   */
  this.animators = [];
};

Animatable.prototype = {
  constructor: Animatable,

  /**
   * 动画
   *
   * @param {string} path The path to fetch value from object, like 'a.b.c'.
   * @param {boolean} [loop] Whether to loop animation.
   * @return {module:zrender/animation/Animator}
   * @example:
   *     el.animate('style', false)
   *         .when(1000, {x: 10} )
   *         .done(function(){ // Animation done })
   *         .start()
   */
  animate: function animate(path, loop) {
    var target;
    var animatingShape = false;
    var el = this;
    var zr = this.__zr;

    if (path) {
      var pathSplitted = path.split('.');
      var prop = el; // If animating shape

      animatingShape = pathSplitted[0] === 'shape';

      for (var i = 0, l = pathSplitted.length; i < l; i++) {
        if (!prop) {
          continue;
        }

        prop = prop[pathSplitted[i]];
      }

      if (prop) {
        target = prop;
      }
    } else {
      target = el;
    }

    if (!target) {
      log('Property "' + path + '" is not existed in element ' + el.id);
      return;
    }

    var animators = el.animators;
    var animator = new Animator(target, loop);
    animator.during(function (target) {
      el.dirty(animatingShape);
    }).done(function () {
      // FIXME Animator will not be removed if use `Animator#stop` to stop animation
      animators.splice(indexOf(animators, animator), 1);
    });
    animators.push(animator); // If animate after added to the zrender

    if (zr) {
      zr.animation.addAnimator(animator);
    }

    return animator;
  },

  /**
   * 停止动画
   * @param {boolean} forwardToLast If move to last frame before stop
   */
  stopAnimation: function stopAnimation(forwardToLast) {
    var animators = this.animators;
    var len = animators.length;

    for (var i = 0; i < len; i++) {
      animators[i].stop(forwardToLast);
    }

    animators.length = 0;
    return this;
  },

  /**
   * Caution: this method will stop previous animation.
   * So do not use this method to one element twice before
   * animation starts, unless you know what you are doing.
   * @param {Object} target
   * @param {number} [time=500] Time in ms
   * @param {string} [easing='linear']
   * @param {number} [delay=0]
   * @param {Function} [callback]
   * @param {Function} [forceAnimate] Prevent stop animation and callback
   *        immediently when target values are the same as current values.
   *
   * @example
   *  // Animate position
   *  el.animateTo({
   *      position: [10, 10]
   *  }, function () { // done })
   *
   *  // Animate shape, style and position in 100ms, delayed 100ms, with cubicOut easing
   *  el.animateTo({
   *      shape: {
   *          width: 500
   *      },
   *      style: {
   *          fill: 'red'
   *      }
   *      position: [10, 10]
   *  }, 100, 100, 'cubicOut', function () { // done })
   */
  // TODO Return animation key
  animateTo: function animateTo(target, time, delay, easing, callback, forceAnimate) {
    _animateTo(this, target, time, delay, easing, callback, forceAnimate);
  },

  /**
   * Animate from the target state to current state.
   * The params and the return value are the same as `this.animateTo`.
   */
  animateFrom: function animateFrom(target, time, delay, easing, callback, forceAnimate) {
    _animateTo(this, target, time, delay, easing, callback, forceAnimate, true);
  }
};

function _animateTo(animatable, target, time, delay, easing, callback, forceAnimate, reverse) {
  // animateTo(target, time, easing, callback);
  if (isString(delay)) {
    callback = easing;
    easing = delay;
    delay = 0;
  } // animateTo(target, time, delay, callback);
  else if (isFunction(easing)) {
      callback = easing;
      easing = 'linear';
      delay = 0;
    } // animateTo(target, time, callback);
    else if (isFunction(delay)) {
        callback = delay;
        delay = 0;
      } // animateTo(target, callback)
      else if (isFunction(time)) {
          callback = time;
          time = 500;
        } // animateTo(target)
        else if (!time) {
            time = 500;
          } // Stop all previous animations


  animatable.stopAnimation();
  animateToShallow(animatable, '', animatable, target, time, delay, reverse); // Animators may be removed immediately after start
  // if there is nothing to animate

  var animators = animatable.animators.slice();
  var count = animators.length;

  function done() {
    count--;

    if (!count) {
      callback && callback();
    }
  } // No animators. This should be checked before animators[i].start(),
  // because 'done' may be executed immediately if no need to animate.


  if (!count) {
    callback && callback();
  } // Start after all animators created
  // Incase any animator is done immediately when all animation properties are not changed


  for (var i = 0; i < animators.length; i++) {
    animators[i].done(done).start(easing, forceAnimate);
  }
}
/**
 * @param {string} path=''
 * @param {Object} source=animatable
 * @param {Object} target
 * @param {number} [time=500]
 * @param {number} [delay=0]
 * @param {boolean} [reverse] If `true`, animate
 *        from the `target` to current state.
 *
 * @example
 *  // Animate position
 *  el._animateToShallow({
 *      position: [10, 10]
 *  })
 *
 *  // Animate shape, style and position in 100ms, delayed 100ms
 *  el._animateToShallow({
 *      shape: {
 *          width: 500
 *      },
 *      style: {
 *          fill: 'red'
 *      }
 *      position: [10, 10]
 *  }, 100, 100)
 */


function animateToShallow(animatable, path, source, target, time, delay, reverse) {
  var objShallow = {};
  var propertyCount = 0;

  for (var name in target) {
    if (!target.hasOwnProperty(name)) {
      continue;
    }

    if (source[name] != null) {
      if (isObject(target[name]) && !isArrayLike(target[name])) {
        animateToShallow(animatable, path ? path + '.' + name : name, source[name], target[name], time, delay, reverse);
      } else {
        if (reverse) {
          objShallow[name] = source[name];
          setAttrByPath(animatable, path, name, target[name]);
        } else {
          objShallow[name] = target[name];
        }

        propertyCount++;
      }
    } else if (target[name] != null && !reverse) {
      setAttrByPath(animatable, path, name, target[name]);
    }
  }

  if (propertyCount > 0) {
    animatable.animate(path, false).when(time == null ? 500 : time, objShallow).delay(delay || 0);
  }
}

function setAttrByPath(el, path, name, value) {
  // Attr directly if not has property
  // FIXME, if some property not needed for element ?
  if (!path) {
    el.attr(name, value);
  } else {
    // Only support set shape or style
    var props = {};
    props[path] = {};
    props[path][name] = value;
    el.attr(props);
  }
}

var _default = Animatable;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvbWl4aW4vQW5pbWF0YWJsZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL21peGluL0FuaW1hdGFibGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIEFuaW1hdG9yID0gcmVxdWlyZShcIi4uL2FuaW1hdGlvbi9BbmltYXRvclwiKTtcblxudmFyIGxvZyA9IHJlcXVpcmUoXCIuLi9jb3JlL2xvZ1wiKTtcblxudmFyIF91dGlsID0gcmVxdWlyZShcIi4uL2NvcmUvdXRpbFwiKTtcblxudmFyIGlzU3RyaW5nID0gX3V0aWwuaXNTdHJpbmc7XG52YXIgaXNGdW5jdGlvbiA9IF91dGlsLmlzRnVuY3Rpb247XG52YXIgaXNPYmplY3QgPSBfdXRpbC5pc09iamVjdDtcbnZhciBpc0FycmF5TGlrZSA9IF91dGlsLmlzQXJyYXlMaWtlO1xudmFyIGluZGV4T2YgPSBfdXRpbC5pbmRleE9mO1xuXG4vKipcbiAqIEBhbGlhcyBtb2R1ZTp6cmVuZGVyL21peGluL0FuaW1hdGFibGVcbiAqIEBjb25zdHJ1Y3RvclxuICovXG52YXIgQW5pbWF0YWJsZSA9IGZ1bmN0aW9uICgpIHtcbiAgLyoqXG4gICAqIEB0eXBlIHtBcnJheS48bW9kdWxlOnpyZW5kZXIvYW5pbWF0aW9uL0FuaW1hdG9yPn1cbiAgICogQHJlYWRPbmx5XG4gICAqL1xuICB0aGlzLmFuaW1hdG9ycyA9IFtdO1xufTtcblxuQW5pbWF0YWJsZS5wcm90b3R5cGUgPSB7XG4gIGNvbnN0cnVjdG9yOiBBbmltYXRhYmxlLFxuXG4gIC8qKlxuICAgKiDliqjnlLtcbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IHBhdGggVGhlIHBhdGggdG8gZmV0Y2ggdmFsdWUgZnJvbSBvYmplY3QsIGxpa2UgJ2EuYi5jJy5cbiAgICogQHBhcmFtIHtib29sZWFufSBbbG9vcF0gV2hldGhlciB0byBsb29wIGFuaW1hdGlvbi5cbiAgICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvYW5pbWF0aW9uL0FuaW1hdG9yfVxuICAgKiBAZXhhbXBsZTpcbiAgICogICAgIGVsLmFuaW1hdGUoJ3N0eWxlJywgZmFsc2UpXG4gICAqICAgICAgICAgLndoZW4oMTAwMCwge3g6IDEwfSApXG4gICAqICAgICAgICAgLmRvbmUoZnVuY3Rpb24oKXsgLy8gQW5pbWF0aW9uIGRvbmUgfSlcbiAgICogICAgICAgICAuc3RhcnQoKVxuICAgKi9cbiAgYW5pbWF0ZTogZnVuY3Rpb24gKHBhdGgsIGxvb3ApIHtcbiAgICB2YXIgdGFyZ2V0O1xuICAgIHZhciBhbmltYXRpbmdTaGFwZSA9IGZhbHNlO1xuICAgIHZhciBlbCA9IHRoaXM7XG4gICAgdmFyIHpyID0gdGhpcy5fX3pyO1xuXG4gICAgaWYgKHBhdGgpIHtcbiAgICAgIHZhciBwYXRoU3BsaXR0ZWQgPSBwYXRoLnNwbGl0KCcuJyk7XG4gICAgICB2YXIgcHJvcCA9IGVsOyAvLyBJZiBhbmltYXRpbmcgc2hhcGVcblxuICAgICAgYW5pbWF0aW5nU2hhcGUgPSBwYXRoU3BsaXR0ZWRbMF0gPT09ICdzaGFwZSc7XG5cbiAgICAgIGZvciAodmFyIGkgPSAwLCBsID0gcGF0aFNwbGl0dGVkLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgICAgICBpZiAoIXByb3ApIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHByb3AgPSBwcm9wW3BhdGhTcGxpdHRlZFtpXV07XG4gICAgICB9XG5cbiAgICAgIGlmIChwcm9wKSB7XG4gICAgICAgIHRhcmdldCA9IHByb3A7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHRhcmdldCA9IGVsO1xuICAgIH1cblxuICAgIGlmICghdGFyZ2V0KSB7XG4gICAgICBsb2coJ1Byb3BlcnR5IFwiJyArIHBhdGggKyAnXCIgaXMgbm90IGV4aXN0ZWQgaW4gZWxlbWVudCAnICsgZWwuaWQpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBhbmltYXRvcnMgPSBlbC5hbmltYXRvcnM7XG4gICAgdmFyIGFuaW1hdG9yID0gbmV3IEFuaW1hdG9yKHRhcmdldCwgbG9vcCk7XG4gICAgYW5pbWF0b3IuZHVyaW5nKGZ1bmN0aW9uICh0YXJnZXQpIHtcbiAgICAgIGVsLmRpcnR5KGFuaW1hdGluZ1NoYXBlKTtcbiAgICB9KS5kb25lKGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIEZJWE1FIEFuaW1hdG9yIHdpbGwgbm90IGJlIHJlbW92ZWQgaWYgdXNlIGBBbmltYXRvciNzdG9wYCB0byBzdG9wIGFuaW1hdGlvblxuICAgICAgYW5pbWF0b3JzLnNwbGljZShpbmRleE9mKGFuaW1hdG9ycywgYW5pbWF0b3IpLCAxKTtcbiAgICB9KTtcbiAgICBhbmltYXRvcnMucHVzaChhbmltYXRvcik7IC8vIElmIGFuaW1hdGUgYWZ0ZXIgYWRkZWQgdG8gdGhlIHpyZW5kZXJcblxuICAgIGlmICh6cikge1xuICAgICAgenIuYW5pbWF0aW9uLmFkZEFuaW1hdG9yKGFuaW1hdG9yKTtcbiAgICB9XG5cbiAgICByZXR1cm4gYW5pbWF0b3I7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOWBnOatouWKqOeUu1xuICAgKiBAcGFyYW0ge2Jvb2xlYW59IGZvcndhcmRUb0xhc3QgSWYgbW92ZSB0byBsYXN0IGZyYW1lIGJlZm9yZSBzdG9wXG4gICAqL1xuICBzdG9wQW5pbWF0aW9uOiBmdW5jdGlvbiAoZm9yd2FyZFRvTGFzdCkge1xuICAgIHZhciBhbmltYXRvcnMgPSB0aGlzLmFuaW1hdG9ycztcbiAgICB2YXIgbGVuID0gYW5pbWF0b3JzLmxlbmd0aDtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgIGFuaW1hdG9yc1tpXS5zdG9wKGZvcndhcmRUb0xhc3QpO1xuICAgIH1cblxuICAgIGFuaW1hdG9ycy5sZW5ndGggPSAwO1xuICAgIHJldHVybiB0aGlzO1xuICB9LFxuXG4gIC8qKlxuICAgKiBDYXV0aW9uOiB0aGlzIG1ldGhvZCB3aWxsIHN0b3AgcHJldmlvdXMgYW5pbWF0aW9uLlxuICAgKiBTbyBkbyBub3QgdXNlIHRoaXMgbWV0aG9kIHRvIG9uZSBlbGVtZW50IHR3aWNlIGJlZm9yZVxuICAgKiBhbmltYXRpb24gc3RhcnRzLCB1bmxlc3MgeW91IGtub3cgd2hhdCB5b3UgYXJlIGRvaW5nLlxuICAgKiBAcGFyYW0ge09iamVjdH0gdGFyZ2V0XG4gICAqIEBwYXJhbSB7bnVtYmVyfSBbdGltZT01MDBdIFRpbWUgaW4gbXNcbiAgICogQHBhcmFtIHtzdHJpbmd9IFtlYXNpbmc9J2xpbmVhciddXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBbZGVsYXk9MF1cbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gW2NhbGxiYWNrXVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBbZm9yY2VBbmltYXRlXSBQcmV2ZW50IHN0b3AgYW5pbWF0aW9uIGFuZCBjYWxsYmFja1xuICAgKiAgICAgICAgaW1tZWRpZW50bHkgd2hlbiB0YXJnZXQgdmFsdWVzIGFyZSB0aGUgc2FtZSBhcyBjdXJyZW50IHZhbHVlcy5cbiAgICpcbiAgICogQGV4YW1wbGVcbiAgICogIC8vIEFuaW1hdGUgcG9zaXRpb25cbiAgICogIGVsLmFuaW1hdGVUbyh7XG4gICAqICAgICAgcG9zaXRpb246IFsxMCwgMTBdXG4gICAqICB9LCBmdW5jdGlvbiAoKSB7IC8vIGRvbmUgfSlcbiAgICpcbiAgICogIC8vIEFuaW1hdGUgc2hhcGUsIHN0eWxlIGFuZCBwb3NpdGlvbiBpbiAxMDBtcywgZGVsYXllZCAxMDBtcywgd2l0aCBjdWJpY091dCBlYXNpbmdcbiAgICogIGVsLmFuaW1hdGVUbyh7XG4gICAqICAgICAgc2hhcGU6IHtcbiAgICogICAgICAgICAgd2lkdGg6IDUwMFxuICAgKiAgICAgIH0sXG4gICAqICAgICAgc3R5bGU6IHtcbiAgICogICAgICAgICAgZmlsbDogJ3JlZCdcbiAgICogICAgICB9XG4gICAqICAgICAgcG9zaXRpb246IFsxMCwgMTBdXG4gICAqICB9LCAxMDAsIDEwMCwgJ2N1YmljT3V0JywgZnVuY3Rpb24gKCkgeyAvLyBkb25lIH0pXG4gICAqL1xuICAvLyBUT0RPIFJldHVybiBhbmltYXRpb24ga2V5XG4gIGFuaW1hdGVUbzogZnVuY3Rpb24gKHRhcmdldCwgdGltZSwgZGVsYXksIGVhc2luZywgY2FsbGJhY2ssIGZvcmNlQW5pbWF0ZSkge1xuICAgIGFuaW1hdGVUbyh0aGlzLCB0YXJnZXQsIHRpbWUsIGRlbGF5LCBlYXNpbmcsIGNhbGxiYWNrLCBmb3JjZUFuaW1hdGUpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBBbmltYXRlIGZyb20gdGhlIHRhcmdldCBzdGF0ZSB0byBjdXJyZW50IHN0YXRlLlxuICAgKiBUaGUgcGFyYW1zIGFuZCB0aGUgcmV0dXJuIHZhbHVlIGFyZSB0aGUgc2FtZSBhcyBgdGhpcy5hbmltYXRlVG9gLlxuICAgKi9cbiAgYW5pbWF0ZUZyb206IGZ1bmN0aW9uICh0YXJnZXQsIHRpbWUsIGRlbGF5LCBlYXNpbmcsIGNhbGxiYWNrLCBmb3JjZUFuaW1hdGUpIHtcbiAgICBhbmltYXRlVG8odGhpcywgdGFyZ2V0LCB0aW1lLCBkZWxheSwgZWFzaW5nLCBjYWxsYmFjaywgZm9yY2VBbmltYXRlLCB0cnVlKTtcbiAgfVxufTtcblxuZnVuY3Rpb24gYW5pbWF0ZVRvKGFuaW1hdGFibGUsIHRhcmdldCwgdGltZSwgZGVsYXksIGVhc2luZywgY2FsbGJhY2ssIGZvcmNlQW5pbWF0ZSwgcmV2ZXJzZSkge1xuICAvLyBhbmltYXRlVG8odGFyZ2V0LCB0aW1lLCBlYXNpbmcsIGNhbGxiYWNrKTtcbiAgaWYgKGlzU3RyaW5nKGRlbGF5KSkge1xuICAgIGNhbGxiYWNrID0gZWFzaW5nO1xuICAgIGVhc2luZyA9IGRlbGF5O1xuICAgIGRlbGF5ID0gMDtcbiAgfSAvLyBhbmltYXRlVG8odGFyZ2V0LCB0aW1lLCBkZWxheSwgY2FsbGJhY2spO1xuICBlbHNlIGlmIChpc0Z1bmN0aW9uKGVhc2luZykpIHtcbiAgICAgIGNhbGxiYWNrID0gZWFzaW5nO1xuICAgICAgZWFzaW5nID0gJ2xpbmVhcic7XG4gICAgICBkZWxheSA9IDA7XG4gICAgfSAvLyBhbmltYXRlVG8odGFyZ2V0LCB0aW1lLCBjYWxsYmFjayk7XG4gICAgZWxzZSBpZiAoaXNGdW5jdGlvbihkZWxheSkpIHtcbiAgICAgICAgY2FsbGJhY2sgPSBkZWxheTtcbiAgICAgICAgZGVsYXkgPSAwO1xuICAgICAgfSAvLyBhbmltYXRlVG8odGFyZ2V0LCBjYWxsYmFjaylcbiAgICAgIGVsc2UgaWYgKGlzRnVuY3Rpb24odGltZSkpIHtcbiAgICAgICAgICBjYWxsYmFjayA9IHRpbWU7XG4gICAgICAgICAgdGltZSA9IDUwMDtcbiAgICAgICAgfSAvLyBhbmltYXRlVG8odGFyZ2V0KVxuICAgICAgICBlbHNlIGlmICghdGltZSkge1xuICAgICAgICAgICAgdGltZSA9IDUwMDtcbiAgICAgICAgICB9IC8vIFN0b3AgYWxsIHByZXZpb3VzIGFuaW1hdGlvbnNcblxuXG4gIGFuaW1hdGFibGUuc3RvcEFuaW1hdGlvbigpO1xuICBhbmltYXRlVG9TaGFsbG93KGFuaW1hdGFibGUsICcnLCBhbmltYXRhYmxlLCB0YXJnZXQsIHRpbWUsIGRlbGF5LCByZXZlcnNlKTsgLy8gQW5pbWF0b3JzIG1heSBiZSByZW1vdmVkIGltbWVkaWF0ZWx5IGFmdGVyIHN0YXJ0XG4gIC8vIGlmIHRoZXJlIGlzIG5vdGhpbmcgdG8gYW5pbWF0ZVxuXG4gIHZhciBhbmltYXRvcnMgPSBhbmltYXRhYmxlLmFuaW1hdG9ycy5zbGljZSgpO1xuICB2YXIgY291bnQgPSBhbmltYXRvcnMubGVuZ3RoO1xuXG4gIGZ1bmN0aW9uIGRvbmUoKSB7XG4gICAgY291bnQtLTtcblxuICAgIGlmICghY291bnQpIHtcbiAgICAgIGNhbGxiYWNrICYmIGNhbGxiYWNrKCk7XG4gICAgfVxuICB9IC8vIE5vIGFuaW1hdG9ycy4gVGhpcyBzaG91bGQgYmUgY2hlY2tlZCBiZWZvcmUgYW5pbWF0b3JzW2ldLnN0YXJ0KCksXG4gIC8vIGJlY2F1c2UgJ2RvbmUnIG1heSBiZSBleGVjdXRlZCBpbW1lZGlhdGVseSBpZiBubyBuZWVkIHRvIGFuaW1hdGUuXG5cblxuICBpZiAoIWNvdW50KSB7XG4gICAgY2FsbGJhY2sgJiYgY2FsbGJhY2soKTtcbiAgfSAvLyBTdGFydCBhZnRlciBhbGwgYW5pbWF0b3JzIGNyZWF0ZWRcbiAgLy8gSW5jYXNlIGFueSBhbmltYXRvciBpcyBkb25lIGltbWVkaWF0ZWx5IHdoZW4gYWxsIGFuaW1hdGlvbiBwcm9wZXJ0aWVzIGFyZSBub3QgY2hhbmdlZFxuXG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBhbmltYXRvcnMubGVuZ3RoOyBpKyspIHtcbiAgICBhbmltYXRvcnNbaV0uZG9uZShkb25lKS5zdGFydChlYXNpbmcsIGZvcmNlQW5pbWF0ZSk7XG4gIH1cbn1cbi8qKlxuICogQHBhcmFtIHtzdHJpbmd9IHBhdGg9JydcbiAqIEBwYXJhbSB7T2JqZWN0fSBzb3VyY2U9YW5pbWF0YWJsZVxuICogQHBhcmFtIHtPYmplY3R9IHRhcmdldFxuICogQHBhcmFtIHtudW1iZXJ9IFt0aW1lPTUwMF1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbZGVsYXk9MF1cbiAqIEBwYXJhbSB7Ym9vbGVhbn0gW3JldmVyc2VdIElmIGB0cnVlYCwgYW5pbWF0ZVxuICogICAgICAgIGZyb20gdGhlIGB0YXJnZXRgIHRvIGN1cnJlbnQgc3RhdGUuXG4gKlxuICogQGV4YW1wbGVcbiAqICAvLyBBbmltYXRlIHBvc2l0aW9uXG4gKiAgZWwuX2FuaW1hdGVUb1NoYWxsb3coe1xuICogICAgICBwb3NpdGlvbjogWzEwLCAxMF1cbiAqICB9KVxuICpcbiAqICAvLyBBbmltYXRlIHNoYXBlLCBzdHlsZSBhbmQgcG9zaXRpb24gaW4gMTAwbXMsIGRlbGF5ZWQgMTAwbXNcbiAqICBlbC5fYW5pbWF0ZVRvU2hhbGxvdyh7XG4gKiAgICAgIHNoYXBlOiB7XG4gKiAgICAgICAgICB3aWR0aDogNTAwXG4gKiAgICAgIH0sXG4gKiAgICAgIHN0eWxlOiB7XG4gKiAgICAgICAgICBmaWxsOiAncmVkJ1xuICogICAgICB9XG4gKiAgICAgIHBvc2l0aW9uOiBbMTAsIDEwXVxuICogIH0sIDEwMCwgMTAwKVxuICovXG5cblxuZnVuY3Rpb24gYW5pbWF0ZVRvU2hhbGxvdyhhbmltYXRhYmxlLCBwYXRoLCBzb3VyY2UsIHRhcmdldCwgdGltZSwgZGVsYXksIHJldmVyc2UpIHtcbiAgdmFyIG9ialNoYWxsb3cgPSB7fTtcbiAgdmFyIHByb3BlcnR5Q291bnQgPSAwO1xuXG4gIGZvciAodmFyIG5hbWUgaW4gdGFyZ2V0KSB7XG4gICAgaWYgKCF0YXJnZXQuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cblxuICAgIGlmIChzb3VyY2VbbmFtZV0gIT0gbnVsbCkge1xuICAgICAgaWYgKGlzT2JqZWN0KHRhcmdldFtuYW1lXSkgJiYgIWlzQXJyYXlMaWtlKHRhcmdldFtuYW1lXSkpIHtcbiAgICAgICAgYW5pbWF0ZVRvU2hhbGxvdyhhbmltYXRhYmxlLCBwYXRoID8gcGF0aCArICcuJyArIG5hbWUgOiBuYW1lLCBzb3VyY2VbbmFtZV0sIHRhcmdldFtuYW1lXSwgdGltZSwgZGVsYXksIHJldmVyc2UpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKHJldmVyc2UpIHtcbiAgICAgICAgICBvYmpTaGFsbG93W25hbWVdID0gc291cmNlW25hbWVdO1xuICAgICAgICAgIHNldEF0dHJCeVBhdGgoYW5pbWF0YWJsZSwgcGF0aCwgbmFtZSwgdGFyZ2V0W25hbWVdKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBvYmpTaGFsbG93W25hbWVdID0gdGFyZ2V0W25hbWVdO1xuICAgICAgICB9XG5cbiAgICAgICAgcHJvcGVydHlDb3VudCsrO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAodGFyZ2V0W25hbWVdICE9IG51bGwgJiYgIXJldmVyc2UpIHtcbiAgICAgIHNldEF0dHJCeVBhdGgoYW5pbWF0YWJsZSwgcGF0aCwgbmFtZSwgdGFyZ2V0W25hbWVdKTtcbiAgICB9XG4gIH1cblxuICBpZiAocHJvcGVydHlDb3VudCA+IDApIHtcbiAgICBhbmltYXRhYmxlLmFuaW1hdGUocGF0aCwgZmFsc2UpLndoZW4odGltZSA9PSBudWxsID8gNTAwIDogdGltZSwgb2JqU2hhbGxvdykuZGVsYXkoZGVsYXkgfHwgMCk7XG4gIH1cbn1cblxuZnVuY3Rpb24gc2V0QXR0ckJ5UGF0aChlbCwgcGF0aCwgbmFtZSwgdmFsdWUpIHtcbiAgLy8gQXR0ciBkaXJlY3RseSBpZiBub3QgaGFzIHByb3BlcnR5XG4gIC8vIEZJWE1FLCBpZiBzb21lIHByb3BlcnR5IG5vdCBuZWVkZWQgZm9yIGVsZW1lbnQgP1xuICBpZiAoIXBhdGgpIHtcbiAgICBlbC5hdHRyKG5hbWUsIHZhbHVlKTtcbiAgfSBlbHNlIHtcbiAgICAvLyBPbmx5IHN1cHBvcnQgc2V0IHNoYXBlIG9yIHN0eWxlXG4gICAgdmFyIHByb3BzID0ge307XG4gICAgcHJvcHNbcGF0aF0gPSB7fTtcbiAgICBwcm9wc1twYXRoXVtuYW1lXSA9IHZhbHVlO1xuICAgIGVsLmF0dHIocHJvcHMpO1xuICB9XG59XG5cbnZhciBfZGVmYXVsdCA9IEFuaW1hdGFibGU7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUlBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUF4SEE7QUFDQTtBQTBIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/mixin/Animatable.js
