

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var PropTypes = __importStar(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

var TableHeaderRow_1 = __importDefault(__webpack_require__(/*! ./TableHeaderRow */ "./node_modules/rc-table/es/TableHeaderRow.js"));

function getHeaderRows(_ref) {
  var _ref$columns = _ref.columns,
      columns = _ref$columns === void 0 ? [] : _ref$columns,
      _ref$currentRow = _ref.currentRow,
      currentRow = _ref$currentRow === void 0 ? 0 : _ref$currentRow,
      _ref$rows = _ref.rows,
      rows = _ref$rows === void 0 ? [] : _ref$rows,
      _ref$isLast = _ref.isLast,
      isLast = _ref$isLast === void 0 ? true : _ref$isLast; // eslint-disable-next-line no-param-reassign

  rows[currentRow] = rows[currentRow] || [];
  columns.forEach(function (column, i) {
    if (column.rowSpan && rows.length < column.rowSpan) {
      while (rows.length < column.rowSpan) {
        rows.push([]);
      }
    }

    var cellIsLast = isLast && i === columns.length - 1;
    var cell = {
      key: column.key,
      className: column.className || '',
      children: column.title,
      isLast: cellIsLast,
      column: column
    };

    if (column.children) {
      getHeaderRows({
        columns: column.children,
        currentRow: currentRow + 1,
        rows: rows,
        isLast: cellIsLast
      });
    }

    if ('colSpan' in column) {
      cell.colSpan = column.colSpan;
    }

    if ('rowSpan' in column) {
      cell.rowSpan = column.rowSpan;
    }

    if (cell.colSpan !== 0) {
      rows[currentRow].push(cell);
    }
  });
  return rows.filter(function (row) {
    return row.length > 0;
  });
}

var TableHeader = function TableHeader(props, _ref2) {
  var table = _ref2.table;
  var components = table.components;
  var _table$props = table.props,
      prefixCls = _table$props.prefixCls,
      showHeader = _table$props.showHeader,
      onHeaderRow = _table$props.onHeaderRow;
  var expander = props.expander,
      columns = props.columns,
      fixed = props.fixed;

  if (!showHeader) {
    return null;
  }

  var rows = getHeaderRows({
    columns: columns
  });
  expander.renderExpandIndentCell(rows, fixed);
  var HeaderWrapper = components.header.wrapper;
  return React.createElement(HeaderWrapper, {
    className: "".concat(prefixCls, "-thead")
  }, rows.map(function (row, index) {
    return React.createElement(TableHeaderRow_1.default, {
      prefixCls: prefixCls,
      key: index,
      index: index,
      fixed: fixed,
      columns: columns,
      rows: rows,
      row: row,
      components: components,
      onHeaderRow: onHeaderRow
    });
  }));
};

TableHeader.contextTypes = {
  table: PropTypes.any
};
exports.default = TableHeader;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvVGFibGVIZWFkZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy10YWJsZS9lcy9UYWJsZUhlYWRlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcblxudmFyIF9faW1wb3J0U3RhciA9IHRoaXMgJiYgdGhpcy5fX2ltcG9ydFN0YXIgfHwgZnVuY3Rpb24gKG1vZCkge1xuICBpZiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSByZXR1cm4gbW9kO1xuICB2YXIgcmVzdWx0ID0ge307XG4gIGlmIChtb2QgIT0gbnVsbCkgZm9yICh2YXIgayBpbiBtb2QpIHtcbiAgICBpZiAoT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobW9kLCBrKSkgcmVzdWx0W2tdID0gbW9kW2tdO1xuICB9XG4gIHJlc3VsdFtcImRlZmF1bHRcIl0gPSBtb2Q7XG4gIHJldHVybiByZXN1bHQ7XG59O1xuXG52YXIgX19pbXBvcnREZWZhdWx0ID0gdGhpcyAmJiB0aGlzLl9faW1wb3J0RGVmYXVsdCB8fCBmdW5jdGlvbiAobW9kKSB7XG4gIHJldHVybiBtb2QgJiYgbW9kLl9fZXNNb2R1bGUgPyBtb2QgOiB7XG4gICAgXCJkZWZhdWx0XCI6IG1vZFxuICB9O1xufTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIFJlYWN0ID0gX19pbXBvcnRTdGFyKHJlcXVpcmUoXCJyZWFjdFwiKSk7XG5cbnZhciBQcm9wVHlwZXMgPSBfX2ltcG9ydFN0YXIocmVxdWlyZShcInByb3AtdHlwZXNcIikpO1xuXG52YXIgVGFibGVIZWFkZXJSb3dfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwiLi9UYWJsZUhlYWRlclJvd1wiKSk7XG5cbmZ1bmN0aW9uIGdldEhlYWRlclJvd3MoX3JlZikge1xuICB2YXIgX3JlZiRjb2x1bW5zID0gX3JlZi5jb2x1bW5zLFxuICAgICAgY29sdW1ucyA9IF9yZWYkY29sdW1ucyA9PT0gdm9pZCAwID8gW10gOiBfcmVmJGNvbHVtbnMsXG4gICAgICBfcmVmJGN1cnJlbnRSb3cgPSBfcmVmLmN1cnJlbnRSb3csXG4gICAgICBjdXJyZW50Um93ID0gX3JlZiRjdXJyZW50Um93ID09PSB2b2lkIDAgPyAwIDogX3JlZiRjdXJyZW50Um93LFxuICAgICAgX3JlZiRyb3dzID0gX3JlZi5yb3dzLFxuICAgICAgcm93cyA9IF9yZWYkcm93cyA9PT0gdm9pZCAwID8gW10gOiBfcmVmJHJvd3MsXG4gICAgICBfcmVmJGlzTGFzdCA9IF9yZWYuaXNMYXN0LFxuICAgICAgaXNMYXN0ID0gX3JlZiRpc0xhc3QgPT09IHZvaWQgMCA/IHRydWUgOiBfcmVmJGlzTGFzdDtcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXBhcmFtLXJlYXNzaWduXG4gIHJvd3NbY3VycmVudFJvd10gPSByb3dzW2N1cnJlbnRSb3ddIHx8IFtdO1xuICBjb2x1bW5zLmZvckVhY2goZnVuY3Rpb24gKGNvbHVtbiwgaSkge1xuICAgIGlmIChjb2x1bW4ucm93U3BhbiAmJiByb3dzLmxlbmd0aCA8IGNvbHVtbi5yb3dTcGFuKSB7XG4gICAgICB3aGlsZSAocm93cy5sZW5ndGggPCBjb2x1bW4ucm93U3Bhbikge1xuICAgICAgICByb3dzLnB1c2goW10pO1xuICAgICAgfVxuICAgIH1cblxuICAgIHZhciBjZWxsSXNMYXN0ID0gaXNMYXN0ICYmIGkgPT09IGNvbHVtbnMubGVuZ3RoIC0gMTtcbiAgICB2YXIgY2VsbCA9IHtcbiAgICAgIGtleTogY29sdW1uLmtleSxcbiAgICAgIGNsYXNzTmFtZTogY29sdW1uLmNsYXNzTmFtZSB8fCAnJyxcbiAgICAgIGNoaWxkcmVuOiBjb2x1bW4udGl0bGUsXG4gICAgICBpc0xhc3Q6IGNlbGxJc0xhc3QsXG4gICAgICBjb2x1bW46IGNvbHVtblxuICAgIH07XG5cbiAgICBpZiAoY29sdW1uLmNoaWxkcmVuKSB7XG4gICAgICBnZXRIZWFkZXJSb3dzKHtcbiAgICAgICAgY29sdW1uczogY29sdW1uLmNoaWxkcmVuLFxuICAgICAgICBjdXJyZW50Um93OiBjdXJyZW50Um93ICsgMSxcbiAgICAgICAgcm93czogcm93cyxcbiAgICAgICAgaXNMYXN0OiBjZWxsSXNMYXN0XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZiAoJ2NvbFNwYW4nIGluIGNvbHVtbikge1xuICAgICAgY2VsbC5jb2xTcGFuID0gY29sdW1uLmNvbFNwYW47XG4gICAgfVxuXG4gICAgaWYgKCdyb3dTcGFuJyBpbiBjb2x1bW4pIHtcbiAgICAgIGNlbGwucm93U3BhbiA9IGNvbHVtbi5yb3dTcGFuO1xuICAgIH1cblxuICAgIGlmIChjZWxsLmNvbFNwYW4gIT09IDApIHtcbiAgICAgIHJvd3NbY3VycmVudFJvd10ucHVzaChjZWxsKTtcbiAgICB9XG4gIH0pO1xuICByZXR1cm4gcm93cy5maWx0ZXIoZnVuY3Rpb24gKHJvdykge1xuICAgIHJldHVybiByb3cubGVuZ3RoID4gMDtcbiAgfSk7XG59XG5cbnZhciBUYWJsZUhlYWRlciA9IGZ1bmN0aW9uIFRhYmxlSGVhZGVyKHByb3BzLCBfcmVmMikge1xuICB2YXIgdGFibGUgPSBfcmVmMi50YWJsZTtcbiAgdmFyIGNvbXBvbmVudHMgPSB0YWJsZS5jb21wb25lbnRzO1xuICB2YXIgX3RhYmxlJHByb3BzID0gdGFibGUucHJvcHMsXG4gICAgICBwcmVmaXhDbHMgPSBfdGFibGUkcHJvcHMucHJlZml4Q2xzLFxuICAgICAgc2hvd0hlYWRlciA9IF90YWJsZSRwcm9wcy5zaG93SGVhZGVyLFxuICAgICAgb25IZWFkZXJSb3cgPSBfdGFibGUkcHJvcHMub25IZWFkZXJSb3c7XG4gIHZhciBleHBhbmRlciA9IHByb3BzLmV4cGFuZGVyLFxuICAgICAgY29sdW1ucyA9IHByb3BzLmNvbHVtbnMsXG4gICAgICBmaXhlZCA9IHByb3BzLmZpeGVkO1xuXG4gIGlmICghc2hvd0hlYWRlcikge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgdmFyIHJvd3MgPSBnZXRIZWFkZXJSb3dzKHtcbiAgICBjb2x1bW5zOiBjb2x1bW5zXG4gIH0pO1xuICBleHBhbmRlci5yZW5kZXJFeHBhbmRJbmRlbnRDZWxsKHJvd3MsIGZpeGVkKTtcbiAgdmFyIEhlYWRlcldyYXBwZXIgPSBjb21wb25lbnRzLmhlYWRlci53cmFwcGVyO1xuICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChIZWFkZXJXcmFwcGVyLCB7XG4gICAgY2xhc3NOYW1lOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXRoZWFkXCIpXG4gIH0sIHJvd3MubWFwKGZ1bmN0aW9uIChyb3csIGluZGV4KSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoVGFibGVIZWFkZXJSb3dfMS5kZWZhdWx0LCB7XG4gICAgICBwcmVmaXhDbHM6IHByZWZpeENscyxcbiAgICAgIGtleTogaW5kZXgsXG4gICAgICBpbmRleDogaW5kZXgsXG4gICAgICBmaXhlZDogZml4ZWQsXG4gICAgICBjb2x1bW5zOiBjb2x1bW5zLFxuICAgICAgcm93czogcm93cyxcbiAgICAgIHJvdzogcm93LFxuICAgICAgY29tcG9uZW50czogY29tcG9uZW50cyxcbiAgICAgIG9uSGVhZGVyUm93OiBvbkhlYWRlclJvd1xuICAgIH0pO1xuICB9KSk7XG59O1xuXG5UYWJsZUhlYWRlci5jb250ZXh0VHlwZXMgPSB7XG4gIHRhYmxlOiBQcm9wVHlwZXMuYW55XG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gVGFibGVIZWFkZXI7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/TableHeader.js
