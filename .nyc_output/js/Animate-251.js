__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "genAnimate", function() { return genAnimate; });
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");
/* harmony import */ var fbjs_lib_warning__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! fbjs/lib/warning */ "./node_modules/fbjs/lib/warning.js");
/* harmony import */ var fbjs_lib_warning__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(fbjs_lib_warning__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _AnimateChild__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./AnimateChild */ "./node_modules/rc-tree-select/node_modules/rc-trigger/node_modules/rc-animate/es/AnimateChild.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./util */ "./node_modules/rc-tree-select/node_modules/rc-trigger/node_modules/rc-animate/es/util.js");












var defaultKey = 'rc_animate_' + Date.now();
var clonePropList = ['children'];
/**
 * Default use `AnimateChild` as component.
 * Here can also pass customize `ChildComponent` for test usage.
 */

function genAnimate(ChildComponent) {
  var Animate = function (_React$Component) {
    babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default()(Animate, _React$Component);

    function Animate() {
      var _ref;

      var _temp, _this, _ret;

      babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Animate);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, (_ref = Animate.__proto__ || Object.getPrototypeOf(Animate)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
        appeared: true,
        mergedChildren: []
      }, _this.onChildLeaved = function (key) {
        // Remove child which not exist anymore
        if (!_this.hasChild(key)) {
          var mergedChildren = _this.state.mergedChildren;

          _this.setState({
            mergedChildren: mergedChildren.filter(function (node) {
              return node.key !== key;
            })
          });
        }
      }, _this.hasChild = function (key) {
        var children = _this.props.children;
        return Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_8__["default"])(children).some(function (node) {
          return node && node.key === key;
        });
      }, _temp), babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(_this, _ret);
    } // [Legacy] Not sure usage
    // commit: https://github.com/react-component/animate/commit/0a1cbfd647407498b10a8c6602a2dea80b42e324
    // eslint-disable-line


    babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(Animate, [{
      key: 'componentDidMount',
      value: function componentDidMount() {
        // No need to re-render
        this.state.appeared = false;
      }
    }, {
      key: 'render',
      value: function render() {
        var _this2 = this;

        var _state = this.state,
            appeared = _state.appeared,
            mergedChildren = _state.mergedChildren;
        var _props = this.props,
            Component = _props.component,
            componentProps = _props.componentProps,
            className = _props.className,
            style = _props.style,
            showProp = _props.showProp;
        var $children = mergedChildren.map(function (node) {
          if (mergedChildren.length > 1 && !node.key) {
            fbjs_lib_warning__WEBPACK_IMPORTED_MODULE_9___default()(false, 'must set key for <rc-animate> children');
            return null;
          }

          var show = true;

          if (!_this2.hasChild(node.key)) {
            show = false;
          } else if (showProp) {
            show = node.props[showProp];
          }

          var key = node.key || defaultKey;
          return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(ChildComponent, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, _this2.props, {
            appeared: appeared,
            show: show,
            className: node.props.className,
            style: node.props.style,
            key: key,
            animateKey: node.key // Keep trans origin key
            ,
            onChildLeaved: _this2.onChildLeaved
          }), node);
        }); // Wrap with component

        if (Component) {
          var passedProps = this.props;

          if (typeof Component === 'string') {
            passedProps = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
              className: className,
              style: style
            }, componentProps);
          }

          return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Component, passedProps, $children);
        }

        return $children[0] || null;
      }
    }], [{
      key: 'getDerivedStateFromProps',
      value: function getDerivedStateFromProps(nextProps, prevState) {
        var _prevState$prevProps = prevState.prevProps,
            prevProps = _prevState$prevProps === undefined ? {} : _prevState$prevProps;
        var newState = {
          prevProps: Object(_util__WEBPACK_IMPORTED_MODULE_11__["cloneProps"])(nextProps, clonePropList)
        };
        var showProp = nextProps.showProp;

        function processState(propName, updater) {
          if (prevProps[propName] !== nextProps[propName]) {
            updater(nextProps[propName]);
            return true;
          }

          return false;
        }

        processState('children', function (children) {
          var currentChildren = Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_8__["default"])(children).filter(function (node) {
            return node;
          });
          var prevChildren = prevState.mergedChildren.filter(function (node) {
            // Remove prev child if not show anymore
            if (currentChildren.every(function (_ref2) {
              var key = _ref2.key;
              return key !== node.key;
            }) && showProp && !node.props[showProp]) {
              return false;
            }

            return true;
          }); // Merge prev children to keep the animation

          newState.mergedChildren = Object(_util__WEBPACK_IMPORTED_MODULE_11__["mergeChildren"])(prevChildren, currentChildren);
        });
        return newState;
      }
    }]);

    return Animate;
  }(react__WEBPACK_IMPORTED_MODULE_5___default.a.Component);

  Animate.isAnimate = true;
  Animate.propTypes = {
    component: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.any,
    componentProps: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.object,
    animation: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.object,
    transitionName: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.object]),
    transitionEnter: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
    transitionAppear: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
    exclusive: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
    transitionLeave: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
    onEnd: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
    onEnter: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
    onLeave: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
    onAppear: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
    showProp: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string,
    children: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.node,
    style: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.object,
    className: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string
  };
  Animate.defaultProps = {
    animation: {},
    component: 'span',
    componentProps: {},
    transitionEnter: true,
    transitionLeave: true,
    transitionAppear: false
  };
  Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_7__["polyfill"])(Animate);
  return Animate;
}
/* harmony default export */ __webpack_exports__["default"] = (genAnimate(_AnimateChild__WEBPACK_IMPORTED_MODULE_10__["default"]));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3Qvbm9kZV9tb2R1bGVzL3JjLXRyaWdnZXIvbm9kZV9tb2R1bGVzL3JjLWFuaW1hdGUvZXMvQW5pbWF0ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXRyZWUtc2VsZWN0L25vZGVfbW9kdWxlcy9yYy10cmlnZ2VyL25vZGVfbW9kdWxlcy9yYy1hbmltYXRlL2VzL0FuaW1hdGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBfY2xhc3NDYWxsQ2hlY2sgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJztcbmltcG9ydCBfY3JlYXRlQ2xhc3MgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJztcbmltcG9ydCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybiBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybic7XG5pbXBvcnQgX2luaGVyaXRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cyc7XG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IHRvQXJyYXkgZnJvbSAncmMtdXRpbC9lcy9DaGlsZHJlbi90b0FycmF5JztcbmltcG9ydCB3YXJuaW5nIGZyb20gJ2ZianMvbGliL3dhcm5pbmcnO1xuXG5pbXBvcnQgQW5pbWF0ZUNoaWxkIGZyb20gJy4vQW5pbWF0ZUNoaWxkJztcbmltcG9ydCB7IGNsb25lUHJvcHMsIG1lcmdlQ2hpbGRyZW4gfSBmcm9tICcuL3V0aWwnO1xuXG52YXIgZGVmYXVsdEtleSA9ICdyY19hbmltYXRlXycgKyBEYXRlLm5vdygpO1xudmFyIGNsb25lUHJvcExpc3QgPSBbJ2NoaWxkcmVuJ107XG5cbi8qKlxuICogRGVmYXVsdCB1c2UgYEFuaW1hdGVDaGlsZGAgYXMgY29tcG9uZW50LlxuICogSGVyZSBjYW4gYWxzbyBwYXNzIGN1c3RvbWl6ZSBgQ2hpbGRDb21wb25lbnRgIGZvciB0ZXN0IHVzYWdlLlxuICovXG5leHBvcnQgZnVuY3Rpb24gZ2VuQW5pbWF0ZShDaGlsZENvbXBvbmVudCkge1xuICB2YXIgQW5pbWF0ZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICAgX2luaGVyaXRzKEFuaW1hdGUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gICAgZnVuY3Rpb24gQW5pbWF0ZSgpIHtcbiAgICAgIHZhciBfcmVmO1xuXG4gICAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQW5pbWF0ZSk7XG5cbiAgICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKF9yZWYgPSBBbmltYXRlLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoQW5pbWF0ZSkpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLnN0YXRlID0ge1xuICAgICAgICBhcHBlYXJlZDogdHJ1ZSxcbiAgICAgICAgbWVyZ2VkQ2hpbGRyZW46IFtdXG4gICAgICB9LCBfdGhpcy5vbkNoaWxkTGVhdmVkID0gZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAvLyBSZW1vdmUgY2hpbGQgd2hpY2ggbm90IGV4aXN0IGFueW1vcmVcbiAgICAgICAgaWYgKCFfdGhpcy5oYXNDaGlsZChrZXkpKSB7XG4gICAgICAgICAgdmFyIG1lcmdlZENoaWxkcmVuID0gX3RoaXMuc3RhdGUubWVyZ2VkQ2hpbGRyZW47XG5cbiAgICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBtZXJnZWRDaGlsZHJlbjogbWVyZ2VkQ2hpbGRyZW4uZmlsdGVyKGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICAgICAgICAgIHJldHVybiBub2RlLmtleSAhPT0ga2V5O1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSwgX3RoaXMuaGFzQ2hpbGQgPSBmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgIHZhciBjaGlsZHJlbiA9IF90aGlzLnByb3BzLmNoaWxkcmVuO1xuXG5cbiAgICAgICAgcmV0dXJuIHRvQXJyYXkoY2hpbGRyZW4pLnNvbWUoZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgICAgICByZXR1cm4gbm9kZSAmJiBub2RlLmtleSA9PT0ga2V5O1xuICAgICAgICB9KTtcbiAgICAgIH0sIF90ZW1wKSwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICAgIH1cbiAgICAvLyBbTGVnYWN5XSBOb3Qgc3VyZSB1c2FnZVxuICAgIC8vIGNvbW1pdDogaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0LWNvbXBvbmVudC9hbmltYXRlL2NvbW1pdC8wYTFjYmZkNjQ3NDA3NDk4YjEwYThjNjYwMmEyZGVhODBiNDJlMzI0XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuXG4gICAgX2NyZWF0ZUNsYXNzKEFuaW1hdGUsIFt7XG4gICAgICBrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIC8vIE5vIG5lZWQgdG8gcmUtcmVuZGVyXG4gICAgICAgIHRoaXMuc3RhdGUuYXBwZWFyZWQgPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9LCB7XG4gICAgICBrZXk6ICdyZW5kZXInLFxuICAgICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgICAgdmFyIF9zdGF0ZSA9IHRoaXMuc3RhdGUsXG4gICAgICAgICAgICBhcHBlYXJlZCA9IF9zdGF0ZS5hcHBlYXJlZCxcbiAgICAgICAgICAgIG1lcmdlZENoaWxkcmVuID0gX3N0YXRlLm1lcmdlZENoaWxkcmVuO1xuICAgICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICAgIENvbXBvbmVudCA9IF9wcm9wcy5jb21wb25lbnQsXG4gICAgICAgICAgICBjb21wb25lbnRQcm9wcyA9IF9wcm9wcy5jb21wb25lbnRQcm9wcyxcbiAgICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgICBzdHlsZSA9IF9wcm9wcy5zdHlsZSxcbiAgICAgICAgICAgIHNob3dQcm9wID0gX3Byb3BzLnNob3dQcm9wO1xuXG5cbiAgICAgICAgdmFyICRjaGlsZHJlbiA9IG1lcmdlZENoaWxkcmVuLm1hcChmdW5jdGlvbiAobm9kZSkge1xuICAgICAgICAgIGlmIChtZXJnZWRDaGlsZHJlbi5sZW5ndGggPiAxICYmICFub2RlLmtleSkge1xuICAgICAgICAgICAgd2FybmluZyhmYWxzZSwgJ211c3Qgc2V0IGtleSBmb3IgPHJjLWFuaW1hdGU+IGNoaWxkcmVuJyk7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB2YXIgc2hvdyA9IHRydWU7XG5cbiAgICAgICAgICBpZiAoIV90aGlzMi5oYXNDaGlsZChub2RlLmtleSkpIHtcbiAgICAgICAgICAgIHNob3cgPSBmYWxzZTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHNob3dQcm9wKSB7XG4gICAgICAgICAgICBzaG93ID0gbm9kZS5wcm9wc1tzaG93UHJvcF07XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdmFyIGtleSA9IG5vZGUua2V5IHx8IGRlZmF1bHRLZXk7XG5cbiAgICAgICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgIENoaWxkQ29tcG9uZW50LFxuICAgICAgICAgICAgX2V4dGVuZHMoe30sIF90aGlzMi5wcm9wcywge1xuICAgICAgICAgICAgICBhcHBlYXJlZDogYXBwZWFyZWQsXG4gICAgICAgICAgICAgIHNob3c6IHNob3csXG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogbm9kZS5wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgICAgIHN0eWxlOiBub2RlLnByb3BzLnN0eWxlLFxuICAgICAgICAgICAgICBrZXk6IGtleSxcblxuICAgICAgICAgICAgICBhbmltYXRlS2V5OiBub2RlLmtleSAvLyBLZWVwIHRyYW5zIG9yaWdpbiBrZXlcbiAgICAgICAgICAgICAgLCBvbkNoaWxkTGVhdmVkOiBfdGhpczIub25DaGlsZExlYXZlZFxuICAgICAgICAgICAgfSksXG4gICAgICAgICAgICBub2RlXG4gICAgICAgICAgKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gV3JhcCB3aXRoIGNvbXBvbmVudFxuICAgICAgICBpZiAoQ29tcG9uZW50KSB7XG4gICAgICAgICAgdmFyIHBhc3NlZFByb3BzID0gdGhpcy5wcm9wcztcbiAgICAgICAgICBpZiAodHlwZW9mIENvbXBvbmVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgIHBhc3NlZFByb3BzID0gX2V4dGVuZHMoe1xuICAgICAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICAgICAgc3R5bGU6IHN0eWxlXG4gICAgICAgICAgICB9LCBjb21wb25lbnRQcm9wcyk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICBDb21wb25lbnQsXG4gICAgICAgICAgICBwYXNzZWRQcm9wcyxcbiAgICAgICAgICAgICRjaGlsZHJlblxuICAgICAgICAgICk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gJGNoaWxkcmVuWzBdIHx8IG51bGw7XG4gICAgICB9XG4gICAgfV0sIFt7XG4gICAgICBrZXk6ICdnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMnLFxuICAgICAgdmFsdWU6IGZ1bmN0aW9uIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgICB2YXIgX3ByZXZTdGF0ZSRwcmV2UHJvcHMgPSBwcmV2U3RhdGUucHJldlByb3BzLFxuICAgICAgICAgICAgcHJldlByb3BzID0gX3ByZXZTdGF0ZSRwcmV2UHJvcHMgPT09IHVuZGVmaW5lZCA/IHt9IDogX3ByZXZTdGF0ZSRwcmV2UHJvcHM7XG5cbiAgICAgICAgdmFyIG5ld1N0YXRlID0ge1xuICAgICAgICAgIHByZXZQcm9wczogY2xvbmVQcm9wcyhuZXh0UHJvcHMsIGNsb25lUHJvcExpc3QpXG4gICAgICAgIH07XG4gICAgICAgIHZhciBzaG93UHJvcCA9IG5leHRQcm9wcy5zaG93UHJvcDtcblxuXG4gICAgICAgIGZ1bmN0aW9uIHByb2Nlc3NTdGF0ZShwcm9wTmFtZSwgdXBkYXRlcikge1xuICAgICAgICAgIGlmIChwcmV2UHJvcHNbcHJvcE5hbWVdICE9PSBuZXh0UHJvcHNbcHJvcE5hbWVdKSB7XG4gICAgICAgICAgICB1cGRhdGVyKG5leHRQcm9wc1twcm9wTmFtZV0pO1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHByb2Nlc3NTdGF0ZSgnY2hpbGRyZW4nLCBmdW5jdGlvbiAoY2hpbGRyZW4pIHtcbiAgICAgICAgICB2YXIgY3VycmVudENoaWxkcmVuID0gdG9BcnJheShjaGlsZHJlbikuZmlsdGVyKGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICAgICAgICByZXR1cm4gbm9kZTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB2YXIgcHJldkNoaWxkcmVuID0gcHJldlN0YXRlLm1lcmdlZENoaWxkcmVuLmZpbHRlcihmdW5jdGlvbiAobm9kZSkge1xuICAgICAgICAgICAgLy8gUmVtb3ZlIHByZXYgY2hpbGQgaWYgbm90IHNob3cgYW55bW9yZVxuICAgICAgICAgICAgaWYgKGN1cnJlbnRDaGlsZHJlbi5ldmVyeShmdW5jdGlvbiAoX3JlZjIpIHtcbiAgICAgICAgICAgICAgdmFyIGtleSA9IF9yZWYyLmtleTtcbiAgICAgICAgICAgICAgcmV0dXJuIGtleSAhPT0gbm9kZS5rZXk7XG4gICAgICAgICAgICB9KSAmJiBzaG93UHJvcCAmJiAhbm9kZS5wcm9wc1tzaG93UHJvcF0pIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgICAvLyBNZXJnZSBwcmV2IGNoaWxkcmVuIHRvIGtlZXAgdGhlIGFuaW1hdGlvblxuICAgICAgICAgIG5ld1N0YXRlLm1lcmdlZENoaWxkcmVuID0gbWVyZ2VDaGlsZHJlbihwcmV2Q2hpbGRyZW4sIGN1cnJlbnRDaGlsZHJlbik7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBuZXdTdGF0ZTtcbiAgICAgIH1cbiAgICB9XSk7XG5cbiAgICByZXR1cm4gQW5pbWF0ZTtcbiAgfShSZWFjdC5Db21wb25lbnQpO1xuXG4gIEFuaW1hdGUuaXNBbmltYXRlID0gdHJ1ZTtcbiAgQW5pbWF0ZS5wcm9wVHlwZXMgPSB7XG4gICAgY29tcG9uZW50OiBQcm9wVHlwZXMuYW55LFxuICAgIGNvbXBvbmVudFByb3BzOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGFuaW1hdGlvbjogUHJvcFR5cGVzLm9iamVjdCxcbiAgICB0cmFuc2l0aW9uTmFtZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLm9iamVjdF0pLFxuICAgIHRyYW5zaXRpb25FbnRlcjogUHJvcFR5cGVzLmJvb2wsXG4gICAgdHJhbnNpdGlvbkFwcGVhcjogUHJvcFR5cGVzLmJvb2wsXG4gICAgZXhjbHVzaXZlOiBQcm9wVHlwZXMuYm9vbCxcbiAgICB0cmFuc2l0aW9uTGVhdmU6IFByb3BUeXBlcy5ib29sLFxuICAgIG9uRW5kOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBvbkVudGVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBvbkxlYXZlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBvbkFwcGVhcjogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc2hvd1Byb3A6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY2hpbGRyZW46IFByb3BUeXBlcy5ub2RlLFxuICAgIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZ1xuICB9O1xuICBBbmltYXRlLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBhbmltYXRpb246IHt9LFxuICAgIGNvbXBvbmVudDogJ3NwYW4nLFxuICAgIGNvbXBvbmVudFByb3BzOiB7fSxcbiAgICB0cmFuc2l0aW9uRW50ZXI6IHRydWUsXG4gICAgdHJhbnNpdGlvbkxlYXZlOiB0cnVlLFxuICAgIHRyYW5zaXRpb25BcHBlYXI6IGZhbHNlXG4gIH07XG5cblxuICBwb2x5ZmlsbChBbmltYXRlKTtcblxuICByZXR1cm4gQW5pbWF0ZTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgZ2VuQW5pbWF0ZShBbmltYXRlQ2hpbGQpOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFQQTtBQVFBO0FBUkE7QUFZQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBbEVBO0FBb0VBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUF4Q0E7QUFDQTtBQTBDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVVBO0FBRUE7QUFDQTtBQUVBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-tree-select/node_modules/rc-trigger/node_modules/rc-animate/es/Animate.js
