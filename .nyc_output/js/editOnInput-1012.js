/* WEBPACK VAR INJECTION */(function(global) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule editOnInput
 * @format
 * 
 */


var DraftFeatureFlags = __webpack_require__(/*! ./DraftFeatureFlags */ "./node_modules/draft-js/lib/DraftFeatureFlags.js");

var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var DraftOffsetKey = __webpack_require__(/*! ./DraftOffsetKey */ "./node_modules/draft-js/lib/DraftOffsetKey.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var UserAgent = __webpack_require__(/*! fbjs/lib/UserAgent */ "./node_modules/fbjs/lib/UserAgent.js");

var findAncestorOffsetKey = __webpack_require__(/*! ./findAncestorOffsetKey */ "./node_modules/draft-js/lib/findAncestorOffsetKey.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");

var isGecko = UserAgent.isEngine('Gecko');
var DOUBLE_NEWLINE = '\n\n';
/**
 * This function is intended to handle spellcheck and autocorrect changes,
 * which occur in the DOM natively without any opportunity to observe or
 * interpret the changes before they occur.
 *
 * The `input` event fires in contentEditable elements reliably for non-IE
 * browsers, immediately after changes occur to the editor DOM. Since our other
 * handlers override or otherwise handle cover other varieties of text input,
 * the DOM state should match the model in all controlled input cases. Thus,
 * when an `input` change leads to a DOM/model mismatch, the change should be
 * due to a spellcheck change, and we can incorporate it into our model.
 */

function editOnInput(editor) {
  if (editor._pendingStateFromBeforeInput !== undefined) {
    editor.update(editor._pendingStateFromBeforeInput);
    editor._pendingStateFromBeforeInput = undefined;
  }

  var domSelection = global.getSelection();
  var anchorNode = domSelection.anchorNode,
      isCollapsed = domSelection.isCollapsed;
  var isNotTextNode = anchorNode.nodeType !== Node.TEXT_NODE;
  var isNotTextOrElementNode = anchorNode.nodeType !== Node.TEXT_NODE && anchorNode.nodeType !== Node.ELEMENT_NODE;

  if (DraftFeatureFlags.draft_killswitch_allow_nontextnodes) {
    if (isNotTextNode) {
      return;
    }
  } else {
    if (isNotTextOrElementNode) {
      // TODO: (t16149272) figure out context for this change
      return;
    }
  }

  if (anchorNode.nodeType === Node.TEXT_NODE && (anchorNode.previousSibling !== null || anchorNode.nextSibling !== null)) {
    // When typing at the beginning of a visual line, Chrome splits the text
    // nodes into two. Why? No one knows. This commit is suspicious:
    // https://chromium.googlesource.com/chromium/src/+/a3b600981286b135632371477f902214c55a1724
    // To work around, we'll merge the sibling text nodes back into this one.
    var span = anchorNode.parentNode;
    anchorNode.nodeValue = span.textContent;

    for (var child = span.firstChild; child !== null; child = child.nextSibling) {
      if (child !== anchorNode) {
        span.removeChild(child);
      }
    }
  }

  var domText = anchorNode.textContent;
  var editorState = editor._latestEditorState;
  var offsetKey = nullthrows(findAncestorOffsetKey(anchorNode));

  var _DraftOffsetKey$decod = DraftOffsetKey.decode(offsetKey),
      blockKey = _DraftOffsetKey$decod.blockKey,
      decoratorKey = _DraftOffsetKey$decod.decoratorKey,
      leafKey = _DraftOffsetKey$decod.leafKey;

  var _editorState$getBlock = editorState.getBlockTree(blockKey).getIn([decoratorKey, 'leaves', leafKey]),
      start = _editorState$getBlock.start,
      end = _editorState$getBlock.end;

  var content = editorState.getCurrentContent();
  var block = content.getBlockForKey(blockKey);
  var modelText = block.getText().slice(start, end); // Special-case soft newlines here. If the DOM text ends in a soft newline,
  // we will have manually inserted an extra soft newline in DraftEditorLeaf.
  // We want to remove this extra newline for the purpose of our comparison
  // of DOM and model text.

  if (domText.endsWith(DOUBLE_NEWLINE)) {
    domText = domText.slice(0, -1);
  } // No change -- the DOM is up to date. Nothing to do here.


  if (domText === modelText) {
    // This can be buggy for some Android keyboards because they don't fire
    // standard onkeydown/pressed events and only fired editOnInput
    // so domText is already changed by the browser and ends up being equal
    // to modelText unexpectedly
    return;
  }

  var selection = editorState.getSelection(); // We'll replace the entire leaf with the text content of the target.

  var targetRange = selection.merge({
    anchorOffset: start,
    focusOffset: end,
    isBackward: false
  });
  var entityKey = block.getEntityAt(start);
  var entity = entityKey && content.getEntity(entityKey);
  var entityType = entity && entity.getMutability();
  var preserveEntity = entityType === 'MUTABLE'; // Immutable or segmented entities cannot properly be handled by the
  // default browser undo, so we have to use a different change type to
  // force using our internal undo method instead of falling through to the
  // native browser undo.

  var changeType = preserveEntity ? 'spellcheck-change' : 'apply-entity';
  var newContent = DraftModifier.replaceText(content, targetRange, domText, block.getInlineStyleAt(start), preserveEntity ? block.getEntityAt(start) : null);
  var anchorOffset, focusOffset, startOffset, endOffset;

  if (isGecko) {
    // Firefox selection does not change while the context menu is open, so
    // we preserve the anchor and focus values of the DOM selection.
    anchorOffset = domSelection.anchorOffset;
    focusOffset = domSelection.focusOffset;
    startOffset = start + Math.min(anchorOffset, focusOffset);
    endOffset = startOffset + Math.abs(anchorOffset - focusOffset);
    anchorOffset = startOffset;
    focusOffset = endOffset;
  } else {
    // Browsers other than Firefox may adjust DOM selection while the context
    // menu is open, and Safari autocorrect is prone to providing an inaccurate
    // DOM selection. Don't trust it. Instead, use our existing SelectionState
    // and adjust it based on the number of characters changed during the
    // mutation.
    var charDelta = domText.length - modelText.length;
    startOffset = selection.getStartOffset();
    endOffset = selection.getEndOffset();
    anchorOffset = isCollapsed ? endOffset + charDelta : startOffset;
    focusOffset = endOffset + charDelta;
  } // Segmented entities are completely or partially removed when their
  // text content changes. For this case we do not want any text to be selected
  // after the change, so we are not merging the selection.


  var contentWithAdjustedDOMSelection = newContent.merge({
    selectionBefore: content.getSelectionAfter(),
    selectionAfter: selection.merge({
      anchorOffset: anchorOffset,
      focusOffset: focusOffset
    })
  });
  editor.update(EditorState.push(editorState, contentWithAdjustedDOMSelection, changeType));
}

module.exports = editOnInput;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbklucHV0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbklucHV0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZWRpdE9uSW5wdXRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0RmVhdHVyZUZsYWdzID0gcmVxdWlyZSgnLi9EcmFmdEZlYXR1cmVGbGFncycpO1xudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcbnZhciBEcmFmdE9mZnNldEtleSA9IHJlcXVpcmUoJy4vRHJhZnRPZmZzZXRLZXknKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcbnZhciBVc2VyQWdlbnQgPSByZXF1aXJlKCdmYmpzL2xpYi9Vc2VyQWdlbnQnKTtcblxudmFyIGZpbmRBbmNlc3Rvck9mZnNldEtleSA9IHJlcXVpcmUoJy4vZmluZEFuY2VzdG9yT2Zmc2V0S2V5Jyk7XG52YXIgbnVsbHRocm93cyA9IHJlcXVpcmUoJ2ZianMvbGliL251bGx0aHJvd3MnKTtcblxudmFyIGlzR2Vja28gPSBVc2VyQWdlbnQuaXNFbmdpbmUoJ0dlY2tvJyk7XG5cbnZhciBET1VCTEVfTkVXTElORSA9ICdcXG5cXG4nO1xuXG4vKipcbiAqIFRoaXMgZnVuY3Rpb24gaXMgaW50ZW5kZWQgdG8gaGFuZGxlIHNwZWxsY2hlY2sgYW5kIGF1dG9jb3JyZWN0IGNoYW5nZXMsXG4gKiB3aGljaCBvY2N1ciBpbiB0aGUgRE9NIG5hdGl2ZWx5IHdpdGhvdXQgYW55IG9wcG9ydHVuaXR5IHRvIG9ic2VydmUgb3JcbiAqIGludGVycHJldCB0aGUgY2hhbmdlcyBiZWZvcmUgdGhleSBvY2N1ci5cbiAqXG4gKiBUaGUgYGlucHV0YCBldmVudCBmaXJlcyBpbiBjb250ZW50RWRpdGFibGUgZWxlbWVudHMgcmVsaWFibHkgZm9yIG5vbi1JRVxuICogYnJvd3NlcnMsIGltbWVkaWF0ZWx5IGFmdGVyIGNoYW5nZXMgb2NjdXIgdG8gdGhlIGVkaXRvciBET00uIFNpbmNlIG91ciBvdGhlclxuICogaGFuZGxlcnMgb3ZlcnJpZGUgb3Igb3RoZXJ3aXNlIGhhbmRsZSBjb3ZlciBvdGhlciB2YXJpZXRpZXMgb2YgdGV4dCBpbnB1dCxcbiAqIHRoZSBET00gc3RhdGUgc2hvdWxkIG1hdGNoIHRoZSBtb2RlbCBpbiBhbGwgY29udHJvbGxlZCBpbnB1dCBjYXNlcy4gVGh1cyxcbiAqIHdoZW4gYW4gYGlucHV0YCBjaGFuZ2UgbGVhZHMgdG8gYSBET00vbW9kZWwgbWlzbWF0Y2gsIHRoZSBjaGFuZ2Ugc2hvdWxkIGJlXG4gKiBkdWUgdG8gYSBzcGVsbGNoZWNrIGNoYW5nZSwgYW5kIHdlIGNhbiBpbmNvcnBvcmF0ZSBpdCBpbnRvIG91ciBtb2RlbC5cbiAqL1xuZnVuY3Rpb24gZWRpdE9uSW5wdXQoZWRpdG9yKSB7XG4gIGlmIChlZGl0b3IuX3BlbmRpbmdTdGF0ZUZyb21CZWZvcmVJbnB1dCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgZWRpdG9yLnVwZGF0ZShlZGl0b3IuX3BlbmRpbmdTdGF0ZUZyb21CZWZvcmVJbnB1dCk7XG4gICAgZWRpdG9yLl9wZW5kaW5nU3RhdGVGcm9tQmVmb3JlSW5wdXQgPSB1bmRlZmluZWQ7XG4gIH1cblxuICB2YXIgZG9tU2VsZWN0aW9uID0gZ2xvYmFsLmdldFNlbGVjdGlvbigpO1xuXG4gIHZhciBhbmNob3JOb2RlID0gZG9tU2VsZWN0aW9uLmFuY2hvck5vZGUsXG4gICAgICBpc0NvbGxhcHNlZCA9IGRvbVNlbGVjdGlvbi5pc0NvbGxhcHNlZDtcblxuICB2YXIgaXNOb3RUZXh0Tm9kZSA9IGFuY2hvck5vZGUubm9kZVR5cGUgIT09IE5vZGUuVEVYVF9OT0RFO1xuICB2YXIgaXNOb3RUZXh0T3JFbGVtZW50Tm9kZSA9IGFuY2hvck5vZGUubm9kZVR5cGUgIT09IE5vZGUuVEVYVF9OT0RFICYmIGFuY2hvck5vZGUubm9kZVR5cGUgIT09IE5vZGUuRUxFTUVOVF9OT0RFO1xuXG4gIGlmIChEcmFmdEZlYXR1cmVGbGFncy5kcmFmdF9raWxsc3dpdGNoX2FsbG93X25vbnRleHRub2Rlcykge1xuICAgIGlmIChpc05vdFRleHROb2RlKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIGlmIChpc05vdFRleHRPckVsZW1lbnROb2RlKSB7XG4gICAgICAvLyBUT0RPOiAodDE2MTQ5MjcyKSBmaWd1cmUgb3V0IGNvbnRleHQgZm9yIHRoaXMgY2hhbmdlXG4gICAgICByZXR1cm47XG4gICAgfVxuICB9XG5cbiAgaWYgKGFuY2hvck5vZGUubm9kZVR5cGUgPT09IE5vZGUuVEVYVF9OT0RFICYmIChhbmNob3JOb2RlLnByZXZpb3VzU2libGluZyAhPT0gbnVsbCB8fCBhbmNob3JOb2RlLm5leHRTaWJsaW5nICE9PSBudWxsKSkge1xuICAgIC8vIFdoZW4gdHlwaW5nIGF0IHRoZSBiZWdpbm5pbmcgb2YgYSB2aXN1YWwgbGluZSwgQ2hyb21lIHNwbGl0cyB0aGUgdGV4dFxuICAgIC8vIG5vZGVzIGludG8gdHdvLiBXaHk/IE5vIG9uZSBrbm93cy4gVGhpcyBjb21taXQgaXMgc3VzcGljaW91czpcbiAgICAvLyBodHRwczovL2Nocm9taXVtLmdvb2dsZXNvdXJjZS5jb20vY2hyb21pdW0vc3JjLysvYTNiNjAwOTgxMjg2YjEzNTYzMjM3MTQ3N2Y5MDIyMTRjNTVhMTcyNFxuICAgIC8vIFRvIHdvcmsgYXJvdW5kLCB3ZSdsbCBtZXJnZSB0aGUgc2libGluZyB0ZXh0IG5vZGVzIGJhY2sgaW50byB0aGlzIG9uZS5cbiAgICB2YXIgc3BhbiA9IGFuY2hvck5vZGUucGFyZW50Tm9kZTtcbiAgICBhbmNob3JOb2RlLm5vZGVWYWx1ZSA9IHNwYW4udGV4dENvbnRlbnQ7XG4gICAgZm9yICh2YXIgY2hpbGQgPSBzcGFuLmZpcnN0Q2hpbGQ7IGNoaWxkICE9PSBudWxsOyBjaGlsZCA9IGNoaWxkLm5leHRTaWJsaW5nKSB7XG4gICAgICBpZiAoY2hpbGQgIT09IGFuY2hvck5vZGUpIHtcbiAgICAgICAgc3Bhbi5yZW1vdmVDaGlsZChjaGlsZCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgdmFyIGRvbVRleHQgPSBhbmNob3JOb2RlLnRleHRDb250ZW50O1xuICB2YXIgZWRpdG9yU3RhdGUgPSBlZGl0b3IuX2xhdGVzdEVkaXRvclN0YXRlO1xuICB2YXIgb2Zmc2V0S2V5ID0gbnVsbHRocm93cyhmaW5kQW5jZXN0b3JPZmZzZXRLZXkoYW5jaG9yTm9kZSkpO1xuXG4gIHZhciBfRHJhZnRPZmZzZXRLZXkkZGVjb2QgPSBEcmFmdE9mZnNldEtleS5kZWNvZGUob2Zmc2V0S2V5KSxcbiAgICAgIGJsb2NrS2V5ID0gX0RyYWZ0T2Zmc2V0S2V5JGRlY29kLmJsb2NrS2V5LFxuICAgICAgZGVjb3JhdG9yS2V5ID0gX0RyYWZ0T2Zmc2V0S2V5JGRlY29kLmRlY29yYXRvcktleSxcbiAgICAgIGxlYWZLZXkgPSBfRHJhZnRPZmZzZXRLZXkkZGVjb2QubGVhZktleTtcblxuICB2YXIgX2VkaXRvclN0YXRlJGdldEJsb2NrID0gZWRpdG9yU3RhdGUuZ2V0QmxvY2tUcmVlKGJsb2NrS2V5KS5nZXRJbihbZGVjb3JhdG9yS2V5LCAnbGVhdmVzJywgbGVhZktleV0pLFxuICAgICAgc3RhcnQgPSBfZWRpdG9yU3RhdGUkZ2V0QmxvY2suc3RhcnQsXG4gICAgICBlbmQgPSBfZWRpdG9yU3RhdGUkZ2V0QmxvY2suZW5kO1xuXG4gIHZhciBjb250ZW50ID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgdmFyIGJsb2NrID0gY29udGVudC5nZXRCbG9ja0ZvcktleShibG9ja0tleSk7XG4gIHZhciBtb2RlbFRleHQgPSBibG9jay5nZXRUZXh0KCkuc2xpY2Uoc3RhcnQsIGVuZCk7XG5cbiAgLy8gU3BlY2lhbC1jYXNlIHNvZnQgbmV3bGluZXMgaGVyZS4gSWYgdGhlIERPTSB0ZXh0IGVuZHMgaW4gYSBzb2Z0IG5ld2xpbmUsXG4gIC8vIHdlIHdpbGwgaGF2ZSBtYW51YWxseSBpbnNlcnRlZCBhbiBleHRyYSBzb2Z0IG5ld2xpbmUgaW4gRHJhZnRFZGl0b3JMZWFmLlxuICAvLyBXZSB3YW50IHRvIHJlbW92ZSB0aGlzIGV4dHJhIG5ld2xpbmUgZm9yIHRoZSBwdXJwb3NlIG9mIG91ciBjb21wYXJpc29uXG4gIC8vIG9mIERPTSBhbmQgbW9kZWwgdGV4dC5cbiAgaWYgKGRvbVRleHQuZW5kc1dpdGgoRE9VQkxFX05FV0xJTkUpKSB7XG4gICAgZG9tVGV4dCA9IGRvbVRleHQuc2xpY2UoMCwgLTEpO1xuICB9XG5cbiAgLy8gTm8gY2hhbmdlIC0tIHRoZSBET00gaXMgdXAgdG8gZGF0ZS4gTm90aGluZyB0byBkbyBoZXJlLlxuICBpZiAoZG9tVGV4dCA9PT0gbW9kZWxUZXh0KSB7XG4gICAgLy8gVGhpcyBjYW4gYmUgYnVnZ3kgZm9yIHNvbWUgQW5kcm9pZCBrZXlib2FyZHMgYmVjYXVzZSB0aGV5IGRvbid0IGZpcmVcbiAgICAvLyBzdGFuZGFyZCBvbmtleWRvd24vcHJlc3NlZCBldmVudHMgYW5kIG9ubHkgZmlyZWQgZWRpdE9uSW5wdXRcbiAgICAvLyBzbyBkb21UZXh0IGlzIGFscmVhZHkgY2hhbmdlZCBieSB0aGUgYnJvd3NlciBhbmQgZW5kcyB1cCBiZWluZyBlcXVhbFxuICAgIC8vIHRvIG1vZGVsVGV4dCB1bmV4cGVjdGVkbHlcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG5cbiAgLy8gV2UnbGwgcmVwbGFjZSB0aGUgZW50aXJlIGxlYWYgd2l0aCB0aGUgdGV4dCBjb250ZW50IG9mIHRoZSB0YXJnZXQuXG4gIHZhciB0YXJnZXRSYW5nZSA9IHNlbGVjdGlvbi5tZXJnZSh7XG4gICAgYW5jaG9yT2Zmc2V0OiBzdGFydCxcbiAgICBmb2N1c09mZnNldDogZW5kLFxuICAgIGlzQmFja3dhcmQ6IGZhbHNlXG4gIH0pO1xuXG4gIHZhciBlbnRpdHlLZXkgPSBibG9jay5nZXRFbnRpdHlBdChzdGFydCk7XG4gIHZhciBlbnRpdHkgPSBlbnRpdHlLZXkgJiYgY29udGVudC5nZXRFbnRpdHkoZW50aXR5S2V5KTtcbiAgdmFyIGVudGl0eVR5cGUgPSBlbnRpdHkgJiYgZW50aXR5LmdldE11dGFiaWxpdHkoKTtcbiAgdmFyIHByZXNlcnZlRW50aXR5ID0gZW50aXR5VHlwZSA9PT0gJ01VVEFCTEUnO1xuXG4gIC8vIEltbXV0YWJsZSBvciBzZWdtZW50ZWQgZW50aXRpZXMgY2Fubm90IHByb3Blcmx5IGJlIGhhbmRsZWQgYnkgdGhlXG4gIC8vIGRlZmF1bHQgYnJvd3NlciB1bmRvLCBzbyB3ZSBoYXZlIHRvIHVzZSBhIGRpZmZlcmVudCBjaGFuZ2UgdHlwZSB0b1xuICAvLyBmb3JjZSB1c2luZyBvdXIgaW50ZXJuYWwgdW5kbyBtZXRob2QgaW5zdGVhZCBvZiBmYWxsaW5nIHRocm91Z2ggdG8gdGhlXG4gIC8vIG5hdGl2ZSBicm93c2VyIHVuZG8uXG4gIHZhciBjaGFuZ2VUeXBlID0gcHJlc2VydmVFbnRpdHkgPyAnc3BlbGxjaGVjay1jaGFuZ2UnIDogJ2FwcGx5LWVudGl0eSc7XG5cbiAgdmFyIG5ld0NvbnRlbnQgPSBEcmFmdE1vZGlmaWVyLnJlcGxhY2VUZXh0KGNvbnRlbnQsIHRhcmdldFJhbmdlLCBkb21UZXh0LCBibG9jay5nZXRJbmxpbmVTdHlsZUF0KHN0YXJ0KSwgcHJlc2VydmVFbnRpdHkgPyBibG9jay5nZXRFbnRpdHlBdChzdGFydCkgOiBudWxsKTtcblxuICB2YXIgYW5jaG9yT2Zmc2V0LCBmb2N1c09mZnNldCwgc3RhcnRPZmZzZXQsIGVuZE9mZnNldDtcblxuICBpZiAoaXNHZWNrbykge1xuICAgIC8vIEZpcmVmb3ggc2VsZWN0aW9uIGRvZXMgbm90IGNoYW5nZSB3aGlsZSB0aGUgY29udGV4dCBtZW51IGlzIG9wZW4sIHNvXG4gICAgLy8gd2UgcHJlc2VydmUgdGhlIGFuY2hvciBhbmQgZm9jdXMgdmFsdWVzIG9mIHRoZSBET00gc2VsZWN0aW9uLlxuICAgIGFuY2hvck9mZnNldCA9IGRvbVNlbGVjdGlvbi5hbmNob3JPZmZzZXQ7XG4gICAgZm9jdXNPZmZzZXQgPSBkb21TZWxlY3Rpb24uZm9jdXNPZmZzZXQ7XG4gICAgc3RhcnRPZmZzZXQgPSBzdGFydCArIE1hdGgubWluKGFuY2hvck9mZnNldCwgZm9jdXNPZmZzZXQpO1xuICAgIGVuZE9mZnNldCA9IHN0YXJ0T2Zmc2V0ICsgTWF0aC5hYnMoYW5jaG9yT2Zmc2V0IC0gZm9jdXNPZmZzZXQpO1xuICAgIGFuY2hvck9mZnNldCA9IHN0YXJ0T2Zmc2V0O1xuICAgIGZvY3VzT2Zmc2V0ID0gZW5kT2Zmc2V0O1xuICB9IGVsc2Uge1xuICAgIC8vIEJyb3dzZXJzIG90aGVyIHRoYW4gRmlyZWZveCBtYXkgYWRqdXN0IERPTSBzZWxlY3Rpb24gd2hpbGUgdGhlIGNvbnRleHRcbiAgICAvLyBtZW51IGlzIG9wZW4sIGFuZCBTYWZhcmkgYXV0b2NvcnJlY3QgaXMgcHJvbmUgdG8gcHJvdmlkaW5nIGFuIGluYWNjdXJhdGVcbiAgICAvLyBET00gc2VsZWN0aW9uLiBEb24ndCB0cnVzdCBpdC4gSW5zdGVhZCwgdXNlIG91ciBleGlzdGluZyBTZWxlY3Rpb25TdGF0ZVxuICAgIC8vIGFuZCBhZGp1c3QgaXQgYmFzZWQgb24gdGhlIG51bWJlciBvZiBjaGFyYWN0ZXJzIGNoYW5nZWQgZHVyaW5nIHRoZVxuICAgIC8vIG11dGF0aW9uLlxuICAgIHZhciBjaGFyRGVsdGEgPSBkb21UZXh0Lmxlbmd0aCAtIG1vZGVsVGV4dC5sZW5ndGg7XG4gICAgc3RhcnRPZmZzZXQgPSBzZWxlY3Rpb24uZ2V0U3RhcnRPZmZzZXQoKTtcbiAgICBlbmRPZmZzZXQgPSBzZWxlY3Rpb24uZ2V0RW5kT2Zmc2V0KCk7XG5cbiAgICBhbmNob3JPZmZzZXQgPSBpc0NvbGxhcHNlZCA/IGVuZE9mZnNldCArIGNoYXJEZWx0YSA6IHN0YXJ0T2Zmc2V0O1xuICAgIGZvY3VzT2Zmc2V0ID0gZW5kT2Zmc2V0ICsgY2hhckRlbHRhO1xuICB9XG5cbiAgLy8gU2VnbWVudGVkIGVudGl0aWVzIGFyZSBjb21wbGV0ZWx5IG9yIHBhcnRpYWxseSByZW1vdmVkIHdoZW4gdGhlaXJcbiAgLy8gdGV4dCBjb250ZW50IGNoYW5nZXMuIEZvciB0aGlzIGNhc2Ugd2UgZG8gbm90IHdhbnQgYW55IHRleHQgdG8gYmUgc2VsZWN0ZWRcbiAgLy8gYWZ0ZXIgdGhlIGNoYW5nZSwgc28gd2UgYXJlIG5vdCBtZXJnaW5nIHRoZSBzZWxlY3Rpb24uXG4gIHZhciBjb250ZW50V2l0aEFkanVzdGVkRE9NU2VsZWN0aW9uID0gbmV3Q29udGVudC5tZXJnZSh7XG4gICAgc2VsZWN0aW9uQmVmb3JlOiBjb250ZW50LmdldFNlbGVjdGlvbkFmdGVyKCksXG4gICAgc2VsZWN0aW9uQWZ0ZXI6IHNlbGVjdGlvbi5tZXJnZSh7IGFuY2hvck9mZnNldDogYW5jaG9yT2Zmc2V0LCBmb2N1c09mZnNldDogZm9jdXNPZmZzZXQgfSlcbiAgfSk7XG5cbiAgZWRpdG9yLnVwZGF0ZShFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCBjb250ZW50V2l0aEFkanVzdGVkRE9NU2VsZWN0aW9uLCBjaGFuZ2VUeXBlKSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZWRpdE9uSW5wdXQ7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7Ozs7Ozs7Ozs7Ozs7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/editOnInput.js
