/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var isTypedArray = _util.isTypedArray;
var extend = _util.extend;
var assert = _util.assert;
var each = _util.each;
var isObject = _util.isObject;

var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var getDataItemValue = _model.getDataItemValue;
var isDataItemOption = _model.isDataItemOption;

var _number = __webpack_require__(/*! ../../util/number */ "./node_modules/echarts/lib/util/number.js");

var parseDate = _number.parseDate;

var Source = __webpack_require__(/*! ../Source */ "./node_modules/echarts/lib/data/Source.js");

var _sourceType = __webpack_require__(/*! ./sourceType */ "./node_modules/echarts/lib/data/helper/sourceType.js");

var SOURCE_FORMAT_TYPED_ARRAY = _sourceType.SOURCE_FORMAT_TYPED_ARRAY;
var SOURCE_FORMAT_ARRAY_ROWS = _sourceType.SOURCE_FORMAT_ARRAY_ROWS;
var SOURCE_FORMAT_ORIGINAL = _sourceType.SOURCE_FORMAT_ORIGINAL;
var SOURCE_FORMAT_OBJECT_ROWS = _sourceType.SOURCE_FORMAT_OBJECT_ROWS;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// TODO
// ??? refactor? check the outer usage of data provider.
// merge with defaultDimValueGetter?

/**
 * If normal array used, mutable chunk size is supported.
 * If typed array used, chunk size must be fixed.
 */

function DefaultDataProvider(source, dimSize) {
  if (!Source.isInstance(source)) {
    source = Source.seriesDataToSource(source);
  }

  this._source = source;
  var data = this._data = source.data;
  var sourceFormat = source.sourceFormat; // Typed array. TODO IE10+?

  if (sourceFormat === SOURCE_FORMAT_TYPED_ARRAY) {
    this._offset = 0;
    this._dimSize = dimSize;
    this._data = data;
  }

  var methods = providerMethods[sourceFormat === SOURCE_FORMAT_ARRAY_ROWS ? sourceFormat + '_' + source.seriesLayoutBy : sourceFormat];
  extend(this, methods);
}

var providerProto = DefaultDataProvider.prototype; // If data is pure without style configuration

providerProto.pure = false; // If data is persistent and will not be released after use.

providerProto.persistent = true; // ???! FIXME legacy data provider do not has method getSource

providerProto.getSource = function () {
  return this._source;
};

var providerMethods = {
  'arrayRows_column': {
    pure: true,
    count: function count() {
      return Math.max(0, this._data.length - this._source.startIndex);
    },
    getItem: function getItem(idx) {
      return this._data[idx + this._source.startIndex];
    },
    appendData: appendDataSimply
  },
  'arrayRows_row': {
    pure: true,
    count: function count() {
      var row = this._data[0];
      return row ? Math.max(0, row.length - this._source.startIndex) : 0;
    },
    getItem: function getItem(idx) {
      idx += this._source.startIndex;
      var item = [];
      var data = this._data;

      for (var i = 0; i < data.length; i++) {
        var row = data[i];
        item.push(row ? row[idx] : null);
      }

      return item;
    },
    appendData: function appendData() {
      throw new Error('Do not support appendData when set seriesLayoutBy: "row".');
    }
  },
  'objectRows': {
    pure: true,
    count: countSimply,
    getItem: getItemSimply,
    appendData: appendDataSimply
  },
  'keyedColumns': {
    pure: true,
    count: function count() {
      var dimName = this._source.dimensionsDefine[0].name;
      var col = this._data[dimName];
      return col ? col.length : 0;
    },
    getItem: function getItem(idx) {
      var item = [];
      var dims = this._source.dimensionsDefine;

      for (var i = 0; i < dims.length; i++) {
        var col = this._data[dims[i].name];
        item.push(col ? col[idx] : null);
      }

      return item;
    },
    appendData: function appendData(newData) {
      var data = this._data;
      each(newData, function (newCol, key) {
        var oldCol = data[key] || (data[key] = []);

        for (var i = 0; i < (newCol || []).length; i++) {
          oldCol.push(newCol[i]);
        }
      });
    }
  },
  'original': {
    count: countSimply,
    getItem: getItemSimply,
    appendData: appendDataSimply
  },
  'typedArray': {
    persistent: false,
    pure: true,
    count: function count() {
      return this._data ? this._data.length / this._dimSize : 0;
    },
    getItem: function getItem(idx, out) {
      idx = idx - this._offset;
      out = out || [];
      var offset = this._dimSize * idx;

      for (var i = 0; i < this._dimSize; i++) {
        out[i] = this._data[offset + i];
      }

      return out;
    },
    appendData: function appendData(newData) {
      this._data = newData;
    },
    // Clean self if data is already used.
    clean: function clean() {
      // PENDING
      this._offset += this.count();
      this._data = null;
    }
  }
};

function countSimply() {
  return this._data.length;
}

function getItemSimply(idx) {
  return this._data[idx];
}

function appendDataSimply(newData) {
  for (var i = 0; i < newData.length; i++) {
    this._data.push(newData[i]);
  }
}

var rawValueGetters = {
  arrayRows: getRawValueSimply,
  objectRows: function objectRows(dataItem, dataIndex, dimIndex, dimName) {
    return dimIndex != null ? dataItem[dimName] : dataItem;
  },
  keyedColumns: getRawValueSimply,
  original: function original(dataItem, dataIndex, dimIndex, dimName) {
    // FIXME
    // In some case (markpoint in geo (geo-map.html)), dataItem
    // is {coord: [...]}
    var value = getDataItemValue(dataItem);
    return dimIndex == null || !(value instanceof Array) ? value : value[dimIndex];
  },
  typedArray: getRawValueSimply
};

function getRawValueSimply(dataItem, dataIndex, dimIndex, dimName) {
  return dimIndex != null ? dataItem[dimIndex] : dataItem;
}

var defaultDimValueGetters = {
  arrayRows: getDimValueSimply,
  objectRows: function objectRows(dataItem, dimName, dataIndex, dimIndex) {
    return converDataValue(dataItem[dimName], this._dimensionInfos[dimName]);
  },
  keyedColumns: getDimValueSimply,
  original: function original(dataItem, dimName, dataIndex, dimIndex) {
    // Performance sensitive, do not use modelUtil.getDataItemValue.
    // If dataItem is an plain object with no value field, the var `value`
    // will be assigned with the object, but it will be tread correctly
    // in the `convertDataValue`.
    var value = dataItem && (dataItem.value == null ? dataItem : dataItem.value); // If any dataItem is like { value: 10 }

    if (!this._rawData.pure && isDataItemOption(dataItem)) {
      this.hasItemOption = true;
    }

    return converDataValue(value instanceof Array ? value[dimIndex] // If value is a single number or something else not array.
    : value, this._dimensionInfos[dimName]);
  },
  typedArray: function typedArray(dataItem, dimName, dataIndex, dimIndex) {
    return dataItem[dimIndex];
  }
};

function getDimValueSimply(dataItem, dimName, dataIndex, dimIndex) {
  return converDataValue(dataItem[dimIndex], this._dimensionInfos[dimName]);
}
/**
 * This helper method convert value in data.
 * @param {string|number|Date} value
 * @param {Object|string} [dimInfo] If string (like 'x'), dimType defaults 'number'.
 *        If "dimInfo.ordinalParseAndSave", ordinal value can be parsed.
 */


function converDataValue(value, dimInfo) {
  // Performance sensitive.
  var dimType = dimInfo && dimInfo.type;

  if (dimType === 'ordinal') {
    // If given value is a category string
    var ordinalMeta = dimInfo && dimInfo.ordinalMeta;
    return ordinalMeta ? ordinalMeta.parseAndCollect(value) : value;
  }

  if (dimType === 'time' // spead up when using timestamp
  && typeof value !== 'number' && value != null && value !== '-') {
    value = +parseDate(value);
  } // dimType defaults 'number'.
  // If dimType is not ordinal and value is null or undefined or NaN or '-',
  // parse to NaN.


  return value == null || value === '' ? NaN // If string (like '-'), using '+' parse to NaN
  // If object, also parse to NaN
  : +value;
} // ??? FIXME can these logic be more neat: getRawValue, getRawDataItem,
// Consider persistent.
// Caution: why use raw value to display on label or tooltip?
// A reason is to avoid format. For example time value we do not know
// how to format is expected. More over, if stack is used, calculated
// value may be 0.91000000001, which have brings trouble to display.
// TODO: consider how to treat null/undefined/NaN when display?

/**
 * @param {module:echarts/data/List} data
 * @param {number} dataIndex
 * @param {string|number} [dim] dimName or dimIndex
 * @return {Array.<number>|string|number} can be null/undefined.
 */


function retrieveRawValue(data, dataIndex, dim) {
  if (!data) {
    return;
  } // Consider data may be not persistent.


  var dataItem = data.getRawDataItem(dataIndex);

  if (dataItem == null) {
    return;
  }

  var sourceFormat = data.getProvider().getSource().sourceFormat;
  var dimName;
  var dimIndex;
  var dimInfo = data.getDimensionInfo(dim);

  if (dimInfo) {
    dimName = dimInfo.name;
    dimIndex = dimInfo.index;
  }

  return rawValueGetters[sourceFormat](dataItem, dataIndex, dimIndex, dimName);
}
/**
 * Compatible with some cases (in pie, map) like:
 * data: [{name: 'xx', value: 5, selected: true}, ...]
 * where only sourceFormat is 'original' and 'objectRows' supported.
 *
 * ??? TODO
 * Supported detail options in data item when using 'arrayRows'.
 *
 * @param {module:echarts/data/List} data
 * @param {number} dataIndex
 * @param {string} attr like 'selected'
 */


function retrieveRawAttr(data, dataIndex, attr) {
  if (!data) {
    return;
  }

  var sourceFormat = data.getProvider().getSource().sourceFormat;

  if (sourceFormat !== SOURCE_FORMAT_ORIGINAL && sourceFormat !== SOURCE_FORMAT_OBJECT_ROWS) {
    return;
  }

  var dataItem = data.getRawDataItem(dataIndex);

  if (sourceFormat === SOURCE_FORMAT_ORIGINAL && !isObject(dataItem)) {
    dataItem = null;
  }

  if (dataItem) {
    return dataItem[attr];
  }
}

exports.DefaultDataProvider = DefaultDataProvider;
exports.defaultDimValueGetters = defaultDimValueGetters;
exports.retrieveRawValue = retrieveRawValue;
exports.retrieveRawAttr = retrieveRawAttr;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9oZWxwZXIvZGF0YVByb3ZpZGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9oZWxwZXIvZGF0YVByb3ZpZGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX2NvbmZpZyA9IHJlcXVpcmUoXCIuLi8uLi9jb25maWdcIik7XG5cbnZhciBfX0RFVl9fID0gX2NvbmZpZy5fX0RFVl9fO1xuXG52YXIgX3V0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgaXNUeXBlZEFycmF5ID0gX3V0aWwuaXNUeXBlZEFycmF5O1xudmFyIGV4dGVuZCA9IF91dGlsLmV4dGVuZDtcbnZhciBhc3NlcnQgPSBfdXRpbC5hc3NlcnQ7XG52YXIgZWFjaCA9IF91dGlsLmVhY2g7XG52YXIgaXNPYmplY3QgPSBfdXRpbC5pc09iamVjdDtcblxudmFyIF9tb2RlbCA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL21vZGVsXCIpO1xuXG52YXIgZ2V0RGF0YUl0ZW1WYWx1ZSA9IF9tb2RlbC5nZXREYXRhSXRlbVZhbHVlO1xudmFyIGlzRGF0YUl0ZW1PcHRpb24gPSBfbW9kZWwuaXNEYXRhSXRlbU9wdGlvbjtcblxudmFyIF9udW1iZXIgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9udW1iZXJcIik7XG5cbnZhciBwYXJzZURhdGUgPSBfbnVtYmVyLnBhcnNlRGF0ZTtcblxudmFyIFNvdXJjZSA9IHJlcXVpcmUoXCIuLi9Tb3VyY2VcIik7XG5cbnZhciBfc291cmNlVHlwZSA9IHJlcXVpcmUoXCIuL3NvdXJjZVR5cGVcIik7XG5cbnZhciBTT1VSQ0VfRk9STUFUX1RZUEVEX0FSUkFZID0gX3NvdXJjZVR5cGUuU09VUkNFX0ZPUk1BVF9UWVBFRF9BUlJBWTtcbnZhciBTT1VSQ0VfRk9STUFUX0FSUkFZX1JPV1MgPSBfc291cmNlVHlwZS5TT1VSQ0VfRk9STUFUX0FSUkFZX1JPV1M7XG52YXIgU09VUkNFX0ZPUk1BVF9PUklHSU5BTCA9IF9zb3VyY2VUeXBlLlNPVVJDRV9GT1JNQVRfT1JJR0lOQUw7XG52YXIgU09VUkNFX0ZPUk1BVF9PQkpFQ1RfUk9XUyA9IF9zb3VyY2VUeXBlLlNPVVJDRV9GT1JNQVRfT0JKRUNUX1JPV1M7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbi8vIFRPRE9cbi8vID8/PyByZWZhY3Rvcj8gY2hlY2sgdGhlIG91dGVyIHVzYWdlIG9mIGRhdGEgcHJvdmlkZXIuXG4vLyBtZXJnZSB3aXRoIGRlZmF1bHREaW1WYWx1ZUdldHRlcj9cblxuLyoqXG4gKiBJZiBub3JtYWwgYXJyYXkgdXNlZCwgbXV0YWJsZSBjaHVuayBzaXplIGlzIHN1cHBvcnRlZC5cbiAqIElmIHR5cGVkIGFycmF5IHVzZWQsIGNodW5rIHNpemUgbXVzdCBiZSBmaXhlZC5cbiAqL1xuZnVuY3Rpb24gRGVmYXVsdERhdGFQcm92aWRlcihzb3VyY2UsIGRpbVNpemUpIHtcbiAgaWYgKCFTb3VyY2UuaXNJbnN0YW5jZShzb3VyY2UpKSB7XG4gICAgc291cmNlID0gU291cmNlLnNlcmllc0RhdGFUb1NvdXJjZShzb3VyY2UpO1xuICB9XG5cbiAgdGhpcy5fc291cmNlID0gc291cmNlO1xuICB2YXIgZGF0YSA9IHRoaXMuX2RhdGEgPSBzb3VyY2UuZGF0YTtcbiAgdmFyIHNvdXJjZUZvcm1hdCA9IHNvdXJjZS5zb3VyY2VGb3JtYXQ7IC8vIFR5cGVkIGFycmF5LiBUT0RPIElFMTArP1xuXG4gIGlmIChzb3VyY2VGb3JtYXQgPT09IFNPVVJDRV9GT1JNQVRfVFlQRURfQVJSQVkpIHtcbiAgICB0aGlzLl9vZmZzZXQgPSAwO1xuICAgIHRoaXMuX2RpbVNpemUgPSBkaW1TaXplO1xuICAgIHRoaXMuX2RhdGEgPSBkYXRhO1xuICB9XG5cbiAgdmFyIG1ldGhvZHMgPSBwcm92aWRlck1ldGhvZHNbc291cmNlRm9ybWF0ID09PSBTT1VSQ0VfRk9STUFUX0FSUkFZX1JPV1MgPyBzb3VyY2VGb3JtYXQgKyAnXycgKyBzb3VyY2Uuc2VyaWVzTGF5b3V0QnkgOiBzb3VyY2VGb3JtYXRdO1xuICBleHRlbmQodGhpcywgbWV0aG9kcyk7XG59XG5cbnZhciBwcm92aWRlclByb3RvID0gRGVmYXVsdERhdGFQcm92aWRlci5wcm90b3R5cGU7IC8vIElmIGRhdGEgaXMgcHVyZSB3aXRob3V0IHN0eWxlIGNvbmZpZ3VyYXRpb25cblxucHJvdmlkZXJQcm90by5wdXJlID0gZmFsc2U7IC8vIElmIGRhdGEgaXMgcGVyc2lzdGVudCBhbmQgd2lsbCBub3QgYmUgcmVsZWFzZWQgYWZ0ZXIgdXNlLlxuXG5wcm92aWRlclByb3RvLnBlcnNpc3RlbnQgPSB0cnVlOyAvLyA/Pz8hIEZJWE1FIGxlZ2FjeSBkYXRhIHByb3ZpZGVyIGRvIG5vdCBoYXMgbWV0aG9kIGdldFNvdXJjZVxuXG5wcm92aWRlclByb3RvLmdldFNvdXJjZSA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuX3NvdXJjZTtcbn07XG5cbnZhciBwcm92aWRlck1ldGhvZHMgPSB7XG4gICdhcnJheVJvd3NfY29sdW1uJzoge1xuICAgIHB1cmU6IHRydWUsXG4gICAgY291bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBNYXRoLm1heCgwLCB0aGlzLl9kYXRhLmxlbmd0aCAtIHRoaXMuX3NvdXJjZS5zdGFydEluZGV4KTtcbiAgICB9LFxuICAgIGdldEl0ZW06IGZ1bmN0aW9uIChpZHgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9kYXRhW2lkeCArIHRoaXMuX3NvdXJjZS5zdGFydEluZGV4XTtcbiAgICB9LFxuICAgIGFwcGVuZERhdGE6IGFwcGVuZERhdGFTaW1wbHlcbiAgfSxcbiAgJ2FycmF5Um93c19yb3cnOiB7XG4gICAgcHVyZTogdHJ1ZSxcbiAgICBjb3VudDogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHJvdyA9IHRoaXMuX2RhdGFbMF07XG4gICAgICByZXR1cm4gcm93ID8gTWF0aC5tYXgoMCwgcm93Lmxlbmd0aCAtIHRoaXMuX3NvdXJjZS5zdGFydEluZGV4KSA6IDA7XG4gICAgfSxcbiAgICBnZXRJdGVtOiBmdW5jdGlvbiAoaWR4KSB7XG4gICAgICBpZHggKz0gdGhpcy5fc291cmNlLnN0YXJ0SW5kZXg7XG4gICAgICB2YXIgaXRlbSA9IFtdO1xuICAgICAgdmFyIGRhdGEgPSB0aGlzLl9kYXRhO1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGRhdGEubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIHJvdyA9IGRhdGFbaV07XG4gICAgICAgIGl0ZW0ucHVzaChyb3cgPyByb3dbaWR4XSA6IG51bGwpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gaXRlbTtcbiAgICB9LFxuICAgIGFwcGVuZERhdGE6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignRG8gbm90IHN1cHBvcnQgYXBwZW5kRGF0YSB3aGVuIHNldCBzZXJpZXNMYXlvdXRCeTogXCJyb3dcIi4nKTtcbiAgICB9XG4gIH0sXG4gICdvYmplY3RSb3dzJzoge1xuICAgIHB1cmU6IHRydWUsXG4gICAgY291bnQ6IGNvdW50U2ltcGx5LFxuICAgIGdldEl0ZW06IGdldEl0ZW1TaW1wbHksXG4gICAgYXBwZW5kRGF0YTogYXBwZW5kRGF0YVNpbXBseVxuICB9LFxuICAna2V5ZWRDb2x1bW5zJzoge1xuICAgIHB1cmU6IHRydWUsXG4gICAgY291bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBkaW1OYW1lID0gdGhpcy5fc291cmNlLmRpbWVuc2lvbnNEZWZpbmVbMF0ubmFtZTtcbiAgICAgIHZhciBjb2wgPSB0aGlzLl9kYXRhW2RpbU5hbWVdO1xuICAgICAgcmV0dXJuIGNvbCA/IGNvbC5sZW5ndGggOiAwO1xuICAgIH0sXG4gICAgZ2V0SXRlbTogZnVuY3Rpb24gKGlkeCkge1xuICAgICAgdmFyIGl0ZW0gPSBbXTtcbiAgICAgIHZhciBkaW1zID0gdGhpcy5fc291cmNlLmRpbWVuc2lvbnNEZWZpbmU7XG5cbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZGltcy5sZW5ndGg7IGkrKykge1xuICAgICAgICB2YXIgY29sID0gdGhpcy5fZGF0YVtkaW1zW2ldLm5hbWVdO1xuICAgICAgICBpdGVtLnB1c2goY29sID8gY29sW2lkeF0gOiBudWxsKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGl0ZW07XG4gICAgfSxcbiAgICBhcHBlbmREYXRhOiBmdW5jdGlvbiAobmV3RGF0YSkge1xuICAgICAgdmFyIGRhdGEgPSB0aGlzLl9kYXRhO1xuICAgICAgZWFjaChuZXdEYXRhLCBmdW5jdGlvbiAobmV3Q29sLCBrZXkpIHtcbiAgICAgICAgdmFyIG9sZENvbCA9IGRhdGFba2V5XSB8fCAoZGF0YVtrZXldID0gW10pO1xuXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgKG5ld0NvbCB8fCBbXSkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBvbGRDb2wucHVzaChuZXdDb2xbaV0pO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH0sXG4gICdvcmlnaW5hbCc6IHtcbiAgICBjb3VudDogY291bnRTaW1wbHksXG4gICAgZ2V0SXRlbTogZ2V0SXRlbVNpbXBseSxcbiAgICBhcHBlbmREYXRhOiBhcHBlbmREYXRhU2ltcGx5XG4gIH0sXG4gICd0eXBlZEFycmF5Jzoge1xuICAgIHBlcnNpc3RlbnQ6IGZhbHNlLFxuICAgIHB1cmU6IHRydWUsXG4gICAgY291bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9kYXRhID8gdGhpcy5fZGF0YS5sZW5ndGggLyB0aGlzLl9kaW1TaXplIDogMDtcbiAgICB9LFxuICAgIGdldEl0ZW06IGZ1bmN0aW9uIChpZHgsIG91dCkge1xuICAgICAgaWR4ID0gaWR4IC0gdGhpcy5fb2Zmc2V0O1xuICAgICAgb3V0ID0gb3V0IHx8IFtdO1xuICAgICAgdmFyIG9mZnNldCA9IHRoaXMuX2RpbVNpemUgKiBpZHg7XG5cbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5fZGltU2l6ZTsgaSsrKSB7XG4gICAgICAgIG91dFtpXSA9IHRoaXMuX2RhdGFbb2Zmc2V0ICsgaV07XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBvdXQ7XG4gICAgfSxcbiAgICBhcHBlbmREYXRhOiBmdW5jdGlvbiAobmV3RGF0YSkge1xuICAgICAgdGhpcy5fZGF0YSA9IG5ld0RhdGE7XG4gICAgfSxcbiAgICAvLyBDbGVhbiBzZWxmIGlmIGRhdGEgaXMgYWxyZWFkeSB1c2VkLlxuICAgIGNsZWFuOiBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBQRU5ESU5HXG4gICAgICB0aGlzLl9vZmZzZXQgKz0gdGhpcy5jb3VudCgpO1xuICAgICAgdGhpcy5fZGF0YSA9IG51bGw7XG4gICAgfVxuICB9XG59O1xuXG5mdW5jdGlvbiBjb3VudFNpbXBseSgpIHtcbiAgcmV0dXJuIHRoaXMuX2RhdGEubGVuZ3RoO1xufVxuXG5mdW5jdGlvbiBnZXRJdGVtU2ltcGx5KGlkeCkge1xuICByZXR1cm4gdGhpcy5fZGF0YVtpZHhdO1xufVxuXG5mdW5jdGlvbiBhcHBlbmREYXRhU2ltcGx5KG5ld0RhdGEpIHtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBuZXdEYXRhLmxlbmd0aDsgaSsrKSB7XG4gICAgdGhpcy5fZGF0YS5wdXNoKG5ld0RhdGFbaV0pO1xuICB9XG59XG5cbnZhciByYXdWYWx1ZUdldHRlcnMgPSB7XG4gIGFycmF5Um93czogZ2V0UmF3VmFsdWVTaW1wbHksXG4gIG9iamVjdFJvd3M6IGZ1bmN0aW9uIChkYXRhSXRlbSwgZGF0YUluZGV4LCBkaW1JbmRleCwgZGltTmFtZSkge1xuICAgIHJldHVybiBkaW1JbmRleCAhPSBudWxsID8gZGF0YUl0ZW1bZGltTmFtZV0gOiBkYXRhSXRlbTtcbiAgfSxcbiAga2V5ZWRDb2x1bW5zOiBnZXRSYXdWYWx1ZVNpbXBseSxcbiAgb3JpZ2luYWw6IGZ1bmN0aW9uIChkYXRhSXRlbSwgZGF0YUluZGV4LCBkaW1JbmRleCwgZGltTmFtZSkge1xuICAgIC8vIEZJWE1FXG4gICAgLy8gSW4gc29tZSBjYXNlIChtYXJrcG9pbnQgaW4gZ2VvIChnZW8tbWFwLmh0bWwpKSwgZGF0YUl0ZW1cbiAgICAvLyBpcyB7Y29vcmQ6IFsuLi5dfVxuICAgIHZhciB2YWx1ZSA9IGdldERhdGFJdGVtVmFsdWUoZGF0YUl0ZW0pO1xuICAgIHJldHVybiBkaW1JbmRleCA9PSBudWxsIHx8ICEodmFsdWUgaW5zdGFuY2VvZiBBcnJheSkgPyB2YWx1ZSA6IHZhbHVlW2RpbUluZGV4XTtcbiAgfSxcbiAgdHlwZWRBcnJheTogZ2V0UmF3VmFsdWVTaW1wbHlcbn07XG5cbmZ1bmN0aW9uIGdldFJhd1ZhbHVlU2ltcGx5KGRhdGFJdGVtLCBkYXRhSW5kZXgsIGRpbUluZGV4LCBkaW1OYW1lKSB7XG4gIHJldHVybiBkaW1JbmRleCAhPSBudWxsID8gZGF0YUl0ZW1bZGltSW5kZXhdIDogZGF0YUl0ZW07XG59XG5cbnZhciBkZWZhdWx0RGltVmFsdWVHZXR0ZXJzID0ge1xuICBhcnJheVJvd3M6IGdldERpbVZhbHVlU2ltcGx5LFxuICBvYmplY3RSb3dzOiBmdW5jdGlvbiAoZGF0YUl0ZW0sIGRpbU5hbWUsIGRhdGFJbmRleCwgZGltSW5kZXgpIHtcbiAgICByZXR1cm4gY29udmVyRGF0YVZhbHVlKGRhdGFJdGVtW2RpbU5hbWVdLCB0aGlzLl9kaW1lbnNpb25JbmZvc1tkaW1OYW1lXSk7XG4gIH0sXG4gIGtleWVkQ29sdW1uczogZ2V0RGltVmFsdWVTaW1wbHksXG4gIG9yaWdpbmFsOiBmdW5jdGlvbiAoZGF0YUl0ZW0sIGRpbU5hbWUsIGRhdGFJbmRleCwgZGltSW5kZXgpIHtcbiAgICAvLyBQZXJmb3JtYW5jZSBzZW5zaXRpdmUsIGRvIG5vdCB1c2UgbW9kZWxVdGlsLmdldERhdGFJdGVtVmFsdWUuXG4gICAgLy8gSWYgZGF0YUl0ZW0gaXMgYW4gcGxhaW4gb2JqZWN0IHdpdGggbm8gdmFsdWUgZmllbGQsIHRoZSB2YXIgYHZhbHVlYFxuICAgIC8vIHdpbGwgYmUgYXNzaWduZWQgd2l0aCB0aGUgb2JqZWN0LCBidXQgaXQgd2lsbCBiZSB0cmVhZCBjb3JyZWN0bHlcbiAgICAvLyBpbiB0aGUgYGNvbnZlcnREYXRhVmFsdWVgLlxuICAgIHZhciB2YWx1ZSA9IGRhdGFJdGVtICYmIChkYXRhSXRlbS52YWx1ZSA9PSBudWxsID8gZGF0YUl0ZW0gOiBkYXRhSXRlbS52YWx1ZSk7IC8vIElmIGFueSBkYXRhSXRlbSBpcyBsaWtlIHsgdmFsdWU6IDEwIH1cblxuICAgIGlmICghdGhpcy5fcmF3RGF0YS5wdXJlICYmIGlzRGF0YUl0ZW1PcHRpb24oZGF0YUl0ZW0pKSB7XG4gICAgICB0aGlzLmhhc0l0ZW1PcHRpb24gPSB0cnVlO1xuICAgIH1cblxuICAgIHJldHVybiBjb252ZXJEYXRhVmFsdWUodmFsdWUgaW5zdGFuY2VvZiBBcnJheSA/IHZhbHVlW2RpbUluZGV4XSAvLyBJZiB2YWx1ZSBpcyBhIHNpbmdsZSBudW1iZXIgb3Igc29tZXRoaW5nIGVsc2Ugbm90IGFycmF5LlxuICAgIDogdmFsdWUsIHRoaXMuX2RpbWVuc2lvbkluZm9zW2RpbU5hbWVdKTtcbiAgfSxcbiAgdHlwZWRBcnJheTogZnVuY3Rpb24gKGRhdGFJdGVtLCBkaW1OYW1lLCBkYXRhSW5kZXgsIGRpbUluZGV4KSB7XG4gICAgcmV0dXJuIGRhdGFJdGVtW2RpbUluZGV4XTtcbiAgfVxufTtcblxuZnVuY3Rpb24gZ2V0RGltVmFsdWVTaW1wbHkoZGF0YUl0ZW0sIGRpbU5hbWUsIGRhdGFJbmRleCwgZGltSW5kZXgpIHtcbiAgcmV0dXJuIGNvbnZlckRhdGFWYWx1ZShkYXRhSXRlbVtkaW1JbmRleF0sIHRoaXMuX2RpbWVuc2lvbkluZm9zW2RpbU5hbWVdKTtcbn1cbi8qKlxuICogVGhpcyBoZWxwZXIgbWV0aG9kIGNvbnZlcnQgdmFsdWUgaW4gZGF0YS5cbiAqIEBwYXJhbSB7c3RyaW5nfG51bWJlcnxEYXRlfSB2YWx1ZVxuICogQHBhcmFtIHtPYmplY3R8c3RyaW5nfSBbZGltSW5mb10gSWYgc3RyaW5nIChsaWtlICd4JyksIGRpbVR5cGUgZGVmYXVsdHMgJ251bWJlcicuXG4gKiAgICAgICAgSWYgXCJkaW1JbmZvLm9yZGluYWxQYXJzZUFuZFNhdmVcIiwgb3JkaW5hbCB2YWx1ZSBjYW4gYmUgcGFyc2VkLlxuICovXG5cblxuZnVuY3Rpb24gY29udmVyRGF0YVZhbHVlKHZhbHVlLCBkaW1JbmZvKSB7XG4gIC8vIFBlcmZvcm1hbmNlIHNlbnNpdGl2ZS5cbiAgdmFyIGRpbVR5cGUgPSBkaW1JbmZvICYmIGRpbUluZm8udHlwZTtcblxuICBpZiAoZGltVHlwZSA9PT0gJ29yZGluYWwnKSB7XG4gICAgLy8gSWYgZ2l2ZW4gdmFsdWUgaXMgYSBjYXRlZ29yeSBzdHJpbmdcbiAgICB2YXIgb3JkaW5hbE1ldGEgPSBkaW1JbmZvICYmIGRpbUluZm8ub3JkaW5hbE1ldGE7XG4gICAgcmV0dXJuIG9yZGluYWxNZXRhID8gb3JkaW5hbE1ldGEucGFyc2VBbmRDb2xsZWN0KHZhbHVlKSA6IHZhbHVlO1xuICB9XG5cbiAgaWYgKGRpbVR5cGUgPT09ICd0aW1lJyAvLyBzcGVhZCB1cCB3aGVuIHVzaW5nIHRpbWVzdGFtcFxuICAmJiB0eXBlb2YgdmFsdWUgIT09ICdudW1iZXInICYmIHZhbHVlICE9IG51bGwgJiYgdmFsdWUgIT09ICctJykge1xuICAgIHZhbHVlID0gK3BhcnNlRGF0ZSh2YWx1ZSk7XG4gIH0gLy8gZGltVHlwZSBkZWZhdWx0cyAnbnVtYmVyJy5cbiAgLy8gSWYgZGltVHlwZSBpcyBub3Qgb3JkaW5hbCBhbmQgdmFsdWUgaXMgbnVsbCBvciB1bmRlZmluZWQgb3IgTmFOIG9yICctJyxcbiAgLy8gcGFyc2UgdG8gTmFOLlxuXG5cbiAgcmV0dXJuIHZhbHVlID09IG51bGwgfHwgdmFsdWUgPT09ICcnID8gTmFOIC8vIElmIHN0cmluZyAobGlrZSAnLScpLCB1c2luZyAnKycgcGFyc2UgdG8gTmFOXG4gIC8vIElmIG9iamVjdCwgYWxzbyBwYXJzZSB0byBOYU5cbiAgOiArdmFsdWU7XG59IC8vID8/PyBGSVhNRSBjYW4gdGhlc2UgbG9naWMgYmUgbW9yZSBuZWF0OiBnZXRSYXdWYWx1ZSwgZ2V0UmF3RGF0YUl0ZW0sXG4vLyBDb25zaWRlciBwZXJzaXN0ZW50LlxuLy8gQ2F1dGlvbjogd2h5IHVzZSByYXcgdmFsdWUgdG8gZGlzcGxheSBvbiBsYWJlbCBvciB0b29sdGlwP1xuLy8gQSByZWFzb24gaXMgdG8gYXZvaWQgZm9ybWF0LiBGb3IgZXhhbXBsZSB0aW1lIHZhbHVlIHdlIGRvIG5vdCBrbm93XG4vLyBob3cgdG8gZm9ybWF0IGlzIGV4cGVjdGVkLiBNb3JlIG92ZXIsIGlmIHN0YWNrIGlzIHVzZWQsIGNhbGN1bGF0ZWRcbi8vIHZhbHVlIG1heSBiZSAwLjkxMDAwMDAwMDAxLCB3aGljaCBoYXZlIGJyaW5ncyB0cm91YmxlIHRvIGRpc3BsYXkuXG4vLyBUT0RPOiBjb25zaWRlciBob3cgdG8gdHJlYXQgbnVsbC91bmRlZmluZWQvTmFOIHdoZW4gZGlzcGxheT9cblxuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2RhdGEvTGlzdH0gZGF0YVxuICogQHBhcmFtIHtudW1iZXJ9IGRhdGFJbmRleFxuICogQHBhcmFtIHtzdHJpbmd8bnVtYmVyfSBbZGltXSBkaW1OYW1lIG9yIGRpbUluZGV4XG4gKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPnxzdHJpbmd8bnVtYmVyfSBjYW4gYmUgbnVsbC91bmRlZmluZWQuXG4gKi9cblxuXG5mdW5jdGlvbiByZXRyaWV2ZVJhd1ZhbHVlKGRhdGEsIGRhdGFJbmRleCwgZGltKSB7XG4gIGlmICghZGF0YSkge1xuICAgIHJldHVybjtcbiAgfSAvLyBDb25zaWRlciBkYXRhIG1heSBiZSBub3QgcGVyc2lzdGVudC5cblxuXG4gIHZhciBkYXRhSXRlbSA9IGRhdGEuZ2V0UmF3RGF0YUl0ZW0oZGF0YUluZGV4KTtcblxuICBpZiAoZGF0YUl0ZW0gPT0gbnVsbCkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBzb3VyY2VGb3JtYXQgPSBkYXRhLmdldFByb3ZpZGVyKCkuZ2V0U291cmNlKCkuc291cmNlRm9ybWF0O1xuICB2YXIgZGltTmFtZTtcbiAgdmFyIGRpbUluZGV4O1xuICB2YXIgZGltSW5mbyA9IGRhdGEuZ2V0RGltZW5zaW9uSW5mbyhkaW0pO1xuXG4gIGlmIChkaW1JbmZvKSB7XG4gICAgZGltTmFtZSA9IGRpbUluZm8ubmFtZTtcbiAgICBkaW1JbmRleCA9IGRpbUluZm8uaW5kZXg7XG4gIH1cblxuICByZXR1cm4gcmF3VmFsdWVHZXR0ZXJzW3NvdXJjZUZvcm1hdF0oZGF0YUl0ZW0sIGRhdGFJbmRleCwgZGltSW5kZXgsIGRpbU5hbWUpO1xufVxuLyoqXG4gKiBDb21wYXRpYmxlIHdpdGggc29tZSBjYXNlcyAoaW4gcGllLCBtYXApIGxpa2U6XG4gKiBkYXRhOiBbe25hbWU6ICd4eCcsIHZhbHVlOiA1LCBzZWxlY3RlZDogdHJ1ZX0sIC4uLl1cbiAqIHdoZXJlIG9ubHkgc291cmNlRm9ybWF0IGlzICdvcmlnaW5hbCcgYW5kICdvYmplY3RSb3dzJyBzdXBwb3J0ZWQuXG4gKlxuICogPz8/IFRPRE9cbiAqIFN1cHBvcnRlZCBkZXRhaWwgb3B0aW9ucyBpbiBkYXRhIGl0ZW0gd2hlbiB1c2luZyAnYXJyYXlSb3dzJy5cbiAqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2RhdGEvTGlzdH0gZGF0YVxuICogQHBhcmFtIHtudW1iZXJ9IGRhdGFJbmRleFxuICogQHBhcmFtIHtzdHJpbmd9IGF0dHIgbGlrZSAnc2VsZWN0ZWQnXG4gKi9cblxuXG5mdW5jdGlvbiByZXRyaWV2ZVJhd0F0dHIoZGF0YSwgZGF0YUluZGV4LCBhdHRyKSB7XG4gIGlmICghZGF0YSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBzb3VyY2VGb3JtYXQgPSBkYXRhLmdldFByb3ZpZGVyKCkuZ2V0U291cmNlKCkuc291cmNlRm9ybWF0O1xuXG4gIGlmIChzb3VyY2VGb3JtYXQgIT09IFNPVVJDRV9GT1JNQVRfT1JJR0lOQUwgJiYgc291cmNlRm9ybWF0ICE9PSBTT1VSQ0VfRk9STUFUX09CSkVDVF9ST1dTKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIGRhdGFJdGVtID0gZGF0YS5nZXRSYXdEYXRhSXRlbShkYXRhSW5kZXgpO1xuXG4gIGlmIChzb3VyY2VGb3JtYXQgPT09IFNPVVJDRV9GT1JNQVRfT1JJR0lOQUwgJiYgIWlzT2JqZWN0KGRhdGFJdGVtKSkge1xuICAgIGRhdGFJdGVtID0gbnVsbDtcbiAgfVxuXG4gIGlmIChkYXRhSXRlbSkge1xuICAgIHJldHVybiBkYXRhSXRlbVthdHRyXTtcbiAgfVxufVxuXG5leHBvcnRzLkRlZmF1bHREYXRhUHJvdmlkZXIgPSBEZWZhdWx0RGF0YVByb3ZpZGVyO1xuZXhwb3J0cy5kZWZhdWx0RGltVmFsdWVHZXR0ZXJzID0gZGVmYXVsdERpbVZhbHVlR2V0dGVycztcbmV4cG9ydHMucmV0cmlldmVSYXdWYWx1ZSA9IHJldHJpZXZlUmF3VmFsdWU7XG5leHBvcnRzLnJldHJpZXZlUmF3QXR0ciA9IHJldHJpZXZlUmF3QXR0cjsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXBCQTtBQXNCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTNCQTtBQTZCQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXpCQTtBQXpFQTtBQUNBO0FBcUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFDQTtBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQXRCQTtBQUNBO0FBd0JBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/data/helper/dataProvider.js
