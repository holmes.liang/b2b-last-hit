__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formDateLayout", function() { return formDateLayout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Details", function() { return Details; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");
/* harmony import */ var _desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk-component/antd/button-back */ "./src/app/desk/component/antd/button-back.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-component/attachment/doc-types */ "./src/app/desk/component/attachment/doc-types.tsx");
/* harmony import */ var _all_steps__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../all-steps */ "./src/app/desk/quote/compare/all-steps.tsx");
/* harmony import */ var _quote__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./quote */ "./src/app/desk/quote/compare/vmi/quote.tsx");
/* harmony import */ var _component_wizard__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _component_index__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../../component/index */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var _common_index__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../common/index */ "./src/app/desk/quote/common/index.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/compare/vmi/details.tsx";















var PAYMENT_METHOD_RECURRING = _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].PAYMENT_METHOD_RECURRING;
var formDateLayout = {
  labelCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  },
  wrapperCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  }
};
var isMobile = _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].getIsMobile();

var Details =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(Details, _QuoteStep);

  function Details() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Details);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Details)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.handleNext = function () {
      _this.props.form.validateFields(function (err, fieldsValue) {
        if (err) {
          return;
        }

        var newPolicy = _this.props.mergePolicyToServiceModel();

        newPolicy.policy.ext._ui.step = _all_steps__WEBPACK_IMPORTED_MODULE_14__["AllSteps"].CONFIRM;

        _this.getHelpers().getAjax().post("/cart/bind?autoUW=N", newPolicy, {
          loading: true
        }).then(function (response) {
          var _ref = response.body || {
            respData: {}
          },
              respData = _ref.respData;

          var policy = respData.policy,
              suspended = respData.suspended;

          _this.props.mergePolicyToUIModel(respData.cart || {});

          Object(_component_wizard__WEBPACK_IMPORTED_MODULE_16__["pageTo"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this), _all_steps__WEBPACK_IMPORTED_MODULE_14__["AllSteps"].CONFIRM);
        });
      });
    };

    _this.handleSave = function () {
      _this.getHelpers().getAjax().post("/cart/merge", _this.props.mergePolicyToServiceModel()).then(function (response) {
        var _ref2 = response.body || {
          respData: {}
        },
            respData = _ref2.respData;

        var policy = respData.policy,
            suspended = respData.suspended;

        _this.props.mergePolicyToUIModel(respData);

        _common_index__WEBPACK_IMPORTED_MODULE_19__["default"].tipSaveMsg();
      });
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Details, [{
    key: "initState",
    value: function initState() {
      return {
        disabled: false
      };
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName) {
      if (!propName) return "policy";
      return "policy.".concat(propName);
    }
  }, {
    key: "renderActions",
    value: function renderActions() {
      var _this2 = this;

      var disabled = this.state.disabled;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        className: "action ".concat(isMobile ? "mobile-action-large" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_10__["ButtonBack"], {
        handel: function handel() {
          Object(_component_wizard__WEBPACK_IMPORTED_MODULE_16__["pageTo"])(_this2, _all_steps__WEBPACK_IMPORTED_MODULE_14__["AllSteps"].ADD_ONS);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Button"], {
        size: "large",
        type: "primary",
        disabled: disabled,
        onClick: this.handleNext,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Next").thai("ต่อไป").my("နောက်တစ်ခု").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Button"], {
        size: "large",
        onClick: this.handleSave,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Save").thai("บันทึก").my("ကယ်ဆယ်").getMessage()));
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var _this3 = this;

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      var planTypeCode = this.getValueFromModel("policy.planTypeCode");
      var openEnd = Boolean(this.getValueFromModel("policy.openEnd")),
          isNewVehicle = this.getValueFromModel("policy.ext.isNewVehicle") === "Y";
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        className: "quote",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NCollapse"], {
        defaultActiveKey: ["1", "2", "3", "4"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Vehicle").thai("รายละเอียดรถยนต์").my("မော်တော်ယာဉ်").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 113
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_component_index__WEBPACK_IMPORTED_MODULE_17__["FieldGroup"], {
        className: "date-group",
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NDateFilter"], {
        form: form,
        model: model,
        size: "large",
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Voluntary Start Date").thai("วันที่เริ่มต้นโดยสมัครใจ").my("စတင်သည့်ရက်စွဲ").getMessage(),
        layoutCol: formDateLayout,
        propName: "policy.effDate",
        format: _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].DATE_FORMAT.DATE_FORMAT,
        onChangeStartDate: function onChangeStartDate(value) {
          if (!openEnd) {
            //TODO this will be refactor
            _model__WEBPACK_IMPORTED_MODULE_9__["Modeller"].setValue({
              model: model
            }, "policy.expDate", moment__WEBPACK_IMPORTED_MODULE_8___default()(value, _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE).add(1, "years").format(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE), true);
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 121
        },
        __self: this
      }), !openEnd && _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("span", {
        className: "to-date",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 150
        },
        __self: this
      }, "~"), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NDate"], {
        form: form,
        model: model,
        size: "large",
        disabled: true,
        layoutCol: formDateLayout,
        format: _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].DATE_FORMAT.DATE_FORMAT,
        propName: "policy.expDate",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 152
        },
        __self: this
      }))), !isNewVehicle && _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_component_index__WEBPACK_IMPORTED_MODULE_17__["FieldGroup"], {
        className: "vehicle-group",
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        minWidth: "110px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NSelect"], {
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Registered No.").thai("เลขทะเบียน").my("မှတ်ပုံတင်မ။").getMessage(),
        required: true,
        tableName: "vehicleprovince",
        dataFixed: "policy",
        propName: "policy.ext.vehicleProvince",
        size: "large",
        disabled: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 172
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NText"], {
        form: form,
        model: model,
        transform: function transform() {
          return (_this3.getValueFromModel("policy.ext.vehicleNo1") || "").toUpperCase();
        },
        propName: "policy.ext.vehicleNo1",
        placeholder: "e.g., 1\u0E01\u0E02",
        rules: [{
          validator: function validator(rule, value, callback) {
            if (!_common__WEBPACK_IMPORTED_MODULE_12__["Rules"].isValidVehicleNo1(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("The first part of the Registered No. must be 1-3 digits, and in Thai alphabet or numbers").thai("ช่องที่ 2 สำหรับตัวอักษร และ ช่องที่ 3 สำหรับตัวเลข").getMessage());
            } else {
              callback();
            }
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 187
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("span", {
        className: "to-vehicle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 214
        },
        __self: this
      }, "-"), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NText"], {
        form: form,
        model: model,
        propName: "policy.ext.vehicleNo2",
        placeholder: "e.g., 1234",
        transform: function transform() {
          return (_this3.getValueFromModel("policy.ext.vehicleNo2") || "").toUpperCase();
        },
        rules: [{
          validator: function validator(rule, value, callback) {
            if (!_common__WEBPACK_IMPORTED_MODULE_12__["Rules"].isValidVehicleNo2(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("The second part of the Registered No. must be 1-4 digits, and in Thai alphabet or numbers").thai("ช่องที่ 2 สำหรับตัวอักษร และ ช่องที่ 3 สำหรับตัวเลข").getMessage());
            } else {
              callback();
            }
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 216
        },
        __self: this
      })), isNewVehicle && _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_component_index__WEBPACK_IMPORTED_MODULE_17__["FieldGroup"], {
        className: "year-group",
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Registered No.").my("မှတ်ပုံတင်မ။").getMessage(),
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        minWidth: "110px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 246
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NSelect"], {
        form: form,
        model: model,
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Registered No.").thai("เลขทะเบียน").my("မှတ်ပုံတင်မ။").getMessage(),
        tableName: "vehicleprovince",
        dataFixed: "policy",
        propName: "policy.ext.vehicleProvince",
        size: "large",
        disabled: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 253
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NCheckbox"], {
        form: form,
        model: model,
        layoutCol: _quote__WEBPACK_IMPORTED_MODULE_15__["formGroupSelectLayout"],
        propName: "policy.ext.isNewVehicle",
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("New Vehicle").thai("รถใหม่").getMessage(),
        checked: this.getValueFromModel("policy.ext.isNewVehicle") === "Y",
        disabled: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 268
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NText"], {
        form: form,
        model: model,
        required: true,
        transform: function transform() {
          return (_this3.getValueFromModel("policy.ext.chassisNo") || "").toUpperCase();
        },
        onChange: function onChange(value) {
          var newValue = (value || "").toUpperCase();

          if (newValue != value) {
            _this3.setValuesToModel({
              "policy.ext.chassisNo": newValue
            });
          }
        },
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Chassis No.").thai("หมายเลขตัวถัง").my("ကိုယ်ထည်အမှတ်").getMessage(),
        propName: "policy.ext.chassisNo",
        rules: [{
          validator: function validator(rule, value, callback) {
            if (!_common__WEBPACK_IMPORTED_MODULE_12__["Rules"].isValidChassisNo(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Chassis No. must be 6-17 digits, and in English letters or numbers").thai("หมายเลขตัวถังต้องมี 6-17 หลักและเป็นตัวอักษรภาษาอังกฤษ").getMessage());
            } else {
              callback();
            }
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 282
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NSwitch"], {
        form: form,
        model: model,
        propName: "policy.ext.mortgageLoan",
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Mortgage Loan?").thai("สินเชื่อรถยนต์?").my("ပေါင်နှံငွေချေး?").getMessage(),
        checked: this.getValueFromModel(this.generatePropName("ext.mortgageLoan")) === "Y",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 319
        },
        __self: this
      }), this.getValueFromModel(this.generatePropName("ext.mortgageLoan")) === "Y" && _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NSelect"], {
        size: "large",
        form: form,
        required: true,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Financing Co.").thai("บริษัทสินเชื่อ").my("ငှားရမ်းသည့်ကုမ္ပဏီ").getMessage(),
        tableName: "beneficiary",
        dataFixed: "policy",
        propName: "policy.ext.beneficiary",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 331
        },
        __self: this
      }), this.getValueFromModel(this.generatePropName("ext.mortgageLoan")) === "Y" && this.getValueFromModel(this.generatePropName("ext.beneficiary")) === _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].BENEFICIARY_OTHERS && _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NText"], {
        form: form,
        model: model,
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Beneficiary Name").thai("ชื่อบริษัทสินเชื่อ").getMessage(),
        propName: "policy.ext.beneficiaryName",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 348
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPanel"], {
        key: "2",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Insured").thai("ผู้เอาประกัน").my("အာမခံထား").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 360
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_component_index__WEBPACK_IMPORTED_MODULE_17__["Insured"], {
        model: model,
        form: form,
        dataIdPrefix: "policy.ext.insured",
        dataFixed: "policy",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 367
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPanel"], {
        key: "3",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Payer").thai("ผู้ชำระเบี้ยประกัน").my("အခွန်ဆောင်သူ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 370
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_component_index__WEBPACK_IMPORTED_MODULE_17__["Payer"], {
        model: model,
        form: form,
        dataIdPrefix: "policy.ext.payer",
        insuredDataIdPrefix: "policy",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 377
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPanel"], {
        key: "4",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Attachments").thai("สิ่งที่แนบมา").my("ချိတ်တွဲမှုများ").my("ချိတ်တွဲမှုများ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 380
        },
        __self: this
      }, planTypeCode === "CLASS1" ? _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_component_index__WEBPACK_IMPORTED_MODULE_17__["Attachment"], {
        model: model,
        form: form,
        dataIdPrefix: "policy",
        docTypes: Object(_desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_13__["getDocTypeForVMIClass1"])(),
        onChange: function onChange(disableBtn) {
          _this3.setState({
            disabled: !disableBtn
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 387
        },
        __self: this
      }) : _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_component_index__WEBPACK_IMPORTED_MODULE_17__["Attachment"], {
        model: model,
        form: form,
        dataIdPrefix: "policy",
        docTypes: Object(_desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_13__["getDocTypeForVMI"])(),
        onChange: function onChange(disableBtn) {
          _this3.setState({
            disabled: !disableBtn
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 398
        },
        __self: this
      }))));
    }
  }, {
    key: "renderPolicyPremium",
    value: function renderPolicyPremium() {
      var policy = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var paymentSchedule = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var cartPremium = arguments.length > 2 ? arguments[2] : undefined;
      var openEnd = policy.openEnd,
          recurringPayment = policy.recurringPayment;
      var policyPremium = _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPolicyPremiumVmi(policy, paymentSchedule, cartPremium);

      var _Utils$formatCurrency = _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].formatCurrencyInfo(policyPremium.ar, policy.currencyCode),
          amount = _Utils$formatCurrency.amount,
          currencyText = _Utils$formatCurrency.currencyText;

      var isInstallmentPayment = openEnd || recurringPayment || paymentSchedule === PAYMENT_METHOD_RECURRING;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("span", {
        className: "price price--lg",
        "data-postfix": isInstallmentPayment ? _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en(" / month").thai(" /เดือน").getMessage() : "",
        "data-prefix": currencyText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 422
        },
        __self: this
      }, amount);
    }
  }, {
    key: "renderTitle",
    value: function renderTitle() {
      var C = this.getComponents();
      var ext = this.getValueFromModel(this.generatePropName("ext"));
      var itntName = this.getValueFromModel(this.generatePropName("itntName"));
      var itntCode = this.getValueFromModel(this.generatePropName("itntCode"));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 444
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 445
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Policy Details").thai("รายละเอียดกรมธรรม์").my("မူဝါဒအသေးစိတ်").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("p", {
        className: "page-descript",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 451
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 452
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("You have selected").thai("แผนประกันที่เลือก").my("သင်ရွေးချယ်ထားသော").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("a", {
        className: "plan-name",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 458
        },
        __self: this
      }, ext.planName || ""), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("a", {
        className: "insurance-company",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 459
        },
        __self: this
      }, itntName || itntCode), this.renderPolicyPremium(this.getValueFromModel("policy"), "", this.getValueFromModel("cartPremium"))));
    }
  }]);

  return Details;
}(_quote_step__WEBPACK_IMPORTED_MODULE_18__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcGFyZS92bWkvZGV0YWlscy50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9jb21wYXJlL3ZtaS9kZXRhaWxzLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgQnV0dG9uLCBub3RpZmljYXRpb24gfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IG1vbWVudCwgeyBNb21lbnQgfSBmcm9tIFwibW9tZW50XCI7XG5cbmltcG9ydCB7IEFqYXhSZXNwb25zZSB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IE1vZGVsbGVyIH0gZnJvbSBcIkBtb2RlbFwiO1xuaW1wb3J0IHsgQnV0dG9uQmFjayB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvYW50ZC9idXR0b24tYmFja1wiO1xuaW1wb3J0IHsgTlRleHQsIE5EYXRlLCBOU2VsZWN0LCBORGF0ZUZpbHRlciB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTGFuZ3VhZ2UsIFV0aWxzLCBDb25zdHMsIFJ1bGVzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5DaGVja2JveCwgTkNvbGxhcHNlLCBOUGFuZWwsIE5Td2l0Y2ggfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IGdldERvY1R5cGVGb3JWTUksIGdldERvY1R5cGVGb3JWTUlDbGFzczEgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L2F0dGFjaG1lbnQvZG9jLXR5cGVzXCI7XG5cbmltcG9ydCB7IEFsbFN0ZXBzIH0gZnJvbSBcIi4uL2FsbC1zdGVwc1wiO1xuaW1wb3J0IHsgZm9ybUdyb3VwU2VsZWN0TGF5b3V0IH0gZnJvbSBcIi4vcXVvdGVcIjtcbmltcG9ydCB7IHBhZ2VUbyB9IGZyb20gXCIuLi8uLi8uLi9jb21wb25lbnQvd2l6YXJkXCI7XG5pbXBvcnQgeyBGaWVsZEdyb3VwLCBJbnN1cmVkLCBQYXllciwgQXR0YWNobWVudCB9IGZyb20gXCIuLi8uLi8uLi9jb21wb25lbnQvaW5kZXhcIjtcbmltcG9ydCBRdW90ZVN0ZXAsIHsgU3RlcENvbXBvbmVudHMsIFN0ZXBQcm9wcyB9IGZyb20gXCIuLi8uLi9xdW90ZS1zdGVwXCI7XG5pbXBvcnQgUXVvdGVDb21tb24gZnJvbSBcIi4uLy4uL2NvbW1vbi9pbmRleFwiO1xuXG5cbmNvbnN0IHsgUEFZTUVOVF9NRVRIT0RfUkVDVVJSSU5HIH0gPSBDb25zdHM7XG5cbmV4cG9ydCBjb25zdCBmb3JtRGF0ZUxheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxMiB9LFxuICAgIHNtOiB7IHNwYW46IDEyIH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxMiB9LFxuICAgIHNtOiB7IHNwYW46IDEyIH0sXG4gIH0sXG59O1xuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xudHlwZSBEZXRhaWxzU3RhdGUgPSB7XG4gIGRpc2FibGVkOiBib29sZWFuO1xufTtcblxuY2xhc3MgRGV0YWlsczxQIGV4dGVuZHMgU3RlcFByb3BzLCBTIGV4dGVuZHMgRGV0YWlsc1N0YXRlLCBDIGV4dGVuZHMgU3RlcENvbXBvbmVudHM+IGV4dGVuZHMgUXVvdGVTdGVwPFAsIFMsIEM+IHtcbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4ge1xuICAgICAgZGlzYWJsZWQ6IGZhbHNlLFxuICAgIH0gYXMgUztcbiAgfVxuXG4gIHByaXZhdGUgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZT86IHN0cmluZyk6IHN0cmluZyB7XG4gICAgaWYgKCFwcm9wTmFtZSkgcmV0dXJuIGBwb2xpY3lgO1xuXG4gICAgcmV0dXJuIGBwb2xpY3kuJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgaGFuZGxlTmV4dCA9ICgpOiB2b2lkID0+IHtcbiAgICB0aGlzLnByb3BzLmZvcm0udmFsaWRhdGVGaWVsZHMoKGVycjogYW55LCBmaWVsZHNWYWx1ZTogYW55KSA9PiB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGNvbnN0IG5ld1BvbGljeSA9IHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbCgpO1xuICAgICAgbmV3UG9saWN5LnBvbGljeS5leHQuX3VpLnN0ZXAgPSBBbGxTdGVwcy5DT05GSVJNO1xuICAgICAgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgICAgLmdldEFqYXgoKVxuICAgICAgICAucG9zdChcIi9jYXJ0L2JpbmQ/YXV0b1VXPU5cIiwgbmV3UG9saWN5LCB7IGxvYWRpbmc6IHRydWUgfSlcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5IHx8IHsgcmVzcERhdGE6IHt9IH07XG4gICAgICAgICAgY29uc3QgeyBwb2xpY3ksIHN1c3BlbmRlZCB9ID0gcmVzcERhdGE7XG4gICAgICAgICAgdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvVUlNb2RlbChyZXNwRGF0YS5jYXJ0IHx8IHt9KTtcbiAgICAgICAgICBwYWdlVG8odGhpcywgQWxsU3RlcHMuQ09ORklSTSk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xuICB9O1xuXG4gIGhhbmRsZVNhdmUgPSAoKTogdm9pZCA9PiB7XG4gICAgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgIC5nZXRBamF4KClcbiAgICAgIC5wb3N0KFwiL2NhcnQvbWVyZ2VcIiwgdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvU2VydmljZU1vZGVsKCkpXG4gICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5IHx8IHsgcmVzcERhdGE6IHt9IH07XG4gICAgICAgIGNvbnN0IHsgcG9saWN5LCBzdXNwZW5kZWQgfSA9IHJlc3BEYXRhO1xuICAgICAgICB0aGlzLnByb3BzLm1lcmdlUG9saWN5VG9VSU1vZGVsKHJlc3BEYXRhKTtcbiAgICAgICAgUXVvdGVDb21tb24udGlwU2F2ZU1zZygpO1xuICAgICAgfSk7XG4gIH07XG5cbiAgcHJvdGVjdGVkIHJlbmRlckFjdGlvbnMoKSB7XG4gICAgY29uc3QgeyBkaXNhYmxlZCB9ID0gdGhpcy5zdGF0ZTtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e2BhY3Rpb24gJHtpc01vYmlsZSA/IFwibW9iaWxlLWFjdGlvbi1sYXJnZVwiIDogXCJcIn1gfT5cbiAgICAgICAgPEJ1dHRvbkJhY2sgaGFuZGVsPXsoKSA9PiB7XG4gICAgICAgICAgcGFnZVRvKHRoaXMsIEFsbFN0ZXBzLkFERF9PTlMpO1xuICAgICAgICB9fS8+XG4gICAgICAgIDxCdXR0b24gc2l6ZT1cImxhcmdlXCIgdHlwZT1cInByaW1hcnlcIiBkaXNhYmxlZD17ZGlzYWJsZWR9IG9uQ2xpY2s9e3RoaXMuaGFuZGxlTmV4dH0+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiTmV4dFwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguJXguYjguK3guYTguJtcIikubXkoXCLhgJThgLHhgKzhgIDhgLrhgJDhgIXhgLrhgIHhgK9cIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvQnV0dG9uPlxuICAgICAgICA8QnV0dG9uIHNpemU9XCJsYXJnZVwiIG9uQ2xpY2s9e3RoaXMuaGFuZGxlU2F2ZX0+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiU2F2ZVwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguJrguLHguJnguJfguLbguIFcIilcbiAgICAgICAgICAgIC5teShcIuGAgOGAmuGAuuGAhuGAmuGAulwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPC9CdXR0b24+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlckNvbnRlbnQoKSB7XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICBsZXQgcGxhblR5cGVDb2RlID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeS5wbGFuVHlwZUNvZGVcIik7XG4gICAgY29uc3Qgb3BlbkVuZCA9IEJvb2xlYW4odGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeS5vcGVuRW5kXCIpKSxcbiAgICAgIGlzTmV3VmVoaWNsZSA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJwb2xpY3kuZXh0LmlzTmV3VmVoaWNsZVwiKSA9PT0gXCJZXCI7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJxdW90ZVwiPlxuICAgICAgICA8TkNvbGxhcHNlIGRlZmF1bHRBY3RpdmVLZXk9e1tcIjFcIiwgXCIyXCIsIFwiM1wiLCBcIjRcIl19PlxuICAgICAgICAgIDxOUGFuZWxcbiAgICAgICAgICAgIGtleT1cIjFcIlxuICAgICAgICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcIlZlaGljbGVcIilcbiAgICAgICAgICAgICAgLnRoYWkoXCLguKPguLLguKLguKXguLDguYDguK3guLXguKLguJTguKPguJbguKLguJnguJXguYxcIilcbiAgICAgICAgICAgICAgLm15KFwi4YCZ4YCx4YCs4YC64YCQ4YCx4YCs4YC64YCa4YCs4YCJ4YC6XCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPEZpZWxkR3JvdXAgY2xhc3NOYW1lPVwiZGF0ZS1ncm91cFwiIHNlbGVjdFhzU209e3sgeHM6IDgsIHNtOiA2IH19IHRleHRYc1NtPXt7IHhzOiAxNiwgc206IDEzIH19PlxuICAgICAgICAgICAgICA8TkRhdGVGaWx0ZXJcbiAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiVm9sdW50YXJ5IFN0YXJ0IERhdGVcIilcbiAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Lin4Lix4LiZ4LiX4Li14LmI4LmA4Lij4Li04LmI4Lih4LiV4LmJ4LiZ4LmC4LiU4Lii4Liq4Lih4Lix4LiE4Lij4LmD4LiIXCIpXG4gICAgICAgICAgICAgICAgICAubXkoXCLhgIXhgJDhgIThgLrhgJ7hgIrhgLrhgLfhgJvhgIDhgLrhgIXhgL3hgLJcIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgbGF5b3V0Q29sPXtmb3JtRGF0ZUxheW91dH1cbiAgICAgICAgICAgICAgICBwcm9wTmFtZT1cInBvbGljeS5lZmZEYXRlXCJcbiAgICAgICAgICAgICAgICBmb3JtYXQ9e0NvbnN0cy5EQVRFX0ZPUk1BVC5EQVRFX0ZPUk1BVH1cbiAgICAgICAgICAgICAgICBvbkNoYW5nZVN0YXJ0RGF0ZT17KHZhbHVlOiBNb21lbnQpID0+IHtcbiAgICAgICAgICAgICAgICAgIGlmICghb3BlbkVuZCkge1xuICAgICAgICAgICAgICAgICAgICAvL1RPRE8gdGhpcyB3aWxsIGJlIHJlZmFjdG9yXG4gICAgICAgICAgICAgICAgICAgIE1vZGVsbGVyLnNldFZhbHVlKFxuICAgICAgICAgICAgICAgICAgICAgIHsgbW9kZWw6IG1vZGVsIH0sXG4gICAgICAgICAgICAgICAgICAgICAgXCJwb2xpY3kuZXhwRGF0ZVwiLFxuICAgICAgICAgICAgICAgICAgICAgIG1vbWVudCh2YWx1ZSwgQ29uc3RzLkRBVEVfRk9STUFULkRBVEVfVElNRV9XSVRIX1RJTUVfWk9ORSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hZGQoMSwgXCJ5ZWFyc1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmZvcm1hdChDb25zdHMuREFURV9GT1JNQVQuREFURV9USU1FX1dJVEhfVElNRV9aT05FKSxcbiAgICAgICAgICAgICAgICAgICAgICB0cnVlLFxuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIC8+XG5cbiAgICAgICAgICAgICAgeyFvcGVuRW5kICYmIChcbiAgICAgICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwidG8tZGF0ZVwiPn48L3NwYW4+XG5cbiAgICAgICAgICAgICAgICAgIDxORGF0ZVxuICAgICAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgICAgIGxheW91dENvbD17Zm9ybURhdGVMYXlvdXR9XG4gICAgICAgICAgICAgICAgICAgIGZvcm1hdD17Q29uc3RzLkRBVEVfRk9STUFULkRBVEVfRk9STUFUfVxuICAgICAgICAgICAgICAgICAgICBwcm9wTmFtZT1cInBvbGljeS5leHBEYXRlXCJcbiAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC8+XG4gICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L0ZpZWxkR3JvdXA+XG5cbiAgICAgICAgICAgIHshaXNOZXdWZWhpY2xlICYmIChcbiAgICAgICAgICAgICAgPEZpZWxkR3JvdXBcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ2ZWhpY2xlLWdyb3VwXCJcbiAgICAgICAgICAgICAgICBzZWxlY3RYc1NtPXt7IHhzOiA4LCBzbTogNiB9fVxuICAgICAgICAgICAgICAgIHRleHRYc1NtPXt7IHhzOiAxNiwgc206IDEzIH19XG4gICAgICAgICAgICAgICAgbWluV2lkdGg9XCIxMTBweFwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8TlNlbGVjdFxuICAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlJlZ2lzdGVyZWQgTm8uXCIpXG4gICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4LmA4Lil4LiC4LiX4Liw4LmA4Lia4Li14Lii4LiZXCIpXG4gICAgICAgICAgICAgICAgICAgIC5teShcIuGAmeGAvuGAkOGAuuGAleGAr+GAtuGAkOGAhOGAuuGAmeGBi1wiKVxuICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgICB0YWJsZU5hbWU9XCJ2ZWhpY2xlcHJvdmluY2VcIlxuICAgICAgICAgICAgICAgICAgZGF0YUZpeGVkPXtcInBvbGljeVwifVxuICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9XCJwb2xpY3kuZXh0LnZlaGljbGVQcm92aW5jZVwiXG4gICAgICAgICAgICAgICAgICBzaXplPVwibGFyZ2VcIlxuICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgLz5cblxuICAgICAgICAgICAgICAgIDxOVGV4dFxuICAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybT17KCk6IGFueSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAodGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeS5leHQudmVoaWNsZU5vMVwiKSB8fCBcIlwiKS50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgIHByb3BOYW1lPVwicG9saWN5LmV4dC52ZWhpY2xlTm8xXCJcbiAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiZS5nLiwgMeC4geC4glwiXG4gICAgICAgICAgICAgICAgICBydWxlcz17W1xuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yKHJ1bGU6IGFueSwgdmFsdWU6IGFueSwgY2FsbGJhY2s6IGFueSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFSdWxlcy5pc1ZhbGlkVmVoaWNsZU5vMSh2YWx1ZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTGFuZ3VhZ2UuZW4oXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIlRoZSBmaXJzdCBwYXJ0IG9mIHRoZSBSZWdpc3RlcmVkIE5vLiBtdXN0IGJlIDEtMyBkaWdpdHMsIGFuZCBpbiBUaGFpIGFscGhhYmV0IG9yIG51bWJlcnNcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGhhaShcIuC4iuC5iOC4reC4h+C4l+C4teC5iCAyIOC4quC4s+C4q+C4o+C4seC4muC4leC4seC4p+C4reC4seC4geC4qeC4oyDguYHguKXguLAg4LiK4LmI4Lit4LiH4LiX4Li14LmIIDMg4Liq4Liz4Lir4Lij4Lix4Lia4LiV4Lix4Lin4LmA4Lil4LiCXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgXX1cbiAgICAgICAgICAgICAgICAvPlxuXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwidG8tdmVoaWNsZVwiPi08L3NwYW4+XG5cbiAgICAgICAgICAgICAgICA8TlRleHRcbiAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICBwcm9wTmFtZT1cInBvbGljeS5leHQudmVoaWNsZU5vMlwiXG4gICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cImUuZy4sIDEyMzRcIlxuICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtPXsoKTogYW55ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicG9saWN5LmV4dC52ZWhpY2xlTm8yXCIpIHx8IFwiXCIpLnRvVXBwZXJDYXNlKCk7XG4gICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgcnVsZXM9e1tcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcihydWxlOiBhbnksIHZhbHVlOiBhbnksIGNhbGxiYWNrOiBhbnkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghUnVsZXMuaXNWYWxpZFZlaGljbGVObzIodmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIExhbmd1YWdlLmVuKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJUaGUgc2Vjb25kIHBhcnQgb2YgdGhlIFJlZ2lzdGVyZWQgTm8uIG11c3QgYmUgMS00IGRpZ2l0cywgYW5kIGluIFRoYWkgYWxwaGFiZXQgb3IgbnVtYmVyc1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4LiK4LmI4Lit4LiH4LiX4Li14LmIIDIg4Liq4Liz4Lir4Lij4Lix4Lia4LiV4Lix4Lin4Lit4Lix4LiB4Lip4LijIOC5geC4peC4sCDguIrguYjguK3guIfguJfguLXguYggMyDguKrguLPguKvguKPguLHguJrguJXguLHguKfguYDguKXguIJcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBdfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvRmllbGRHcm91cD5cbiAgICAgICAgICAgICl9XG5cbiAgICAgICAgICAgIHtpc05ld1ZlaGljbGUgJiYgKFxuICAgICAgICAgICAgICA8RmllbGRHcm91cFxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInllYXItZ3JvdXBcIlxuICAgICAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlJlZ2lzdGVyZWQgTm8uXCIpLm15KFwi4YCZ4YC+4YCQ4YC64YCV4YCv4YC24YCQ4YCE4YC64YCZ4YGLXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICBzZWxlY3RYc1NtPXt7IHhzOiA4LCBzbTogNiB9fVxuICAgICAgICAgICAgICAgIHRleHRYc1NtPXt7IHhzOiAxNiwgc206IDEzIH19XG4gICAgICAgICAgICAgICAgbWluV2lkdGg9XCIxMTBweFwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8TlNlbGVjdFxuICAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiUmVnaXN0ZXJlZCBOby5cIilcbiAgICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguYDguKXguILguJfguLDguYDguJrguLXguKLguJlcIilcbiAgICAgICAgICAgICAgICAgICAgLm15KFwi4YCZ4YC+4YCQ4YC64YCV4YCv4YC24YCQ4YCE4YC64YCZ4YGLXCIpXG4gICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICB0YWJsZU5hbWU9XCJ2ZWhpY2xlcHJvdmluY2VcIlxuICAgICAgICAgICAgICAgICAgZGF0YUZpeGVkPXtcInBvbGljeVwifVxuICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9XCJwb2xpY3kuZXh0LnZlaGljbGVQcm92aW5jZVwiXG4gICAgICAgICAgICAgICAgICBzaXplPVwibGFyZ2VcIlxuICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgLz5cblxuICAgICAgICAgICAgICAgIDxOQ2hlY2tib3hcbiAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICBsYXlvdXRDb2w9e2Zvcm1Hcm91cFNlbGVjdExheW91dH1cbiAgICAgICAgICAgICAgICAgIHByb3BOYW1lPVwicG9saWN5LmV4dC5pc05ld1ZlaGljbGVcIlxuICAgICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiTmV3IFZlaGljbGVcIilcbiAgICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguKPguJbguYPguKvguKHguYhcIilcbiAgICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3RoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJwb2xpY3kuZXh0LmlzTmV3VmVoaWNsZVwiKSA9PT0gXCJZXCJ9XG4gICAgICAgICAgICAgICAgICBkaXNhYmxlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L0ZpZWxkR3JvdXA+XG4gICAgICAgICAgICApfVxuXG4gICAgICAgICAgICA8TlRleHRcbiAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgdHJhbnNmb3JtPXsoKTogYW55ID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJwb2xpY3kuZXh0LmNoYXNzaXNOb1wiKSB8fCBcIlwiKS50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBuZXdWYWx1ZSA9ICh2YWx1ZSB8fCBcIlwiKS50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgICAgIGlmIChuZXdWYWx1ZSAhPSB2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZXNUb01vZGVsKHtcbiAgICAgICAgICAgICAgICAgICAgXCJwb2xpY3kuZXh0LmNoYXNzaXNOb1wiOiBuZXdWYWx1ZSxcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiQ2hhc3NpcyBOby5cIilcbiAgICAgICAgICAgICAgICAudGhhaShcIuC4q+C4oeC4suC4ouC5gOC4peC4guC4leC4seC4p+C4luC4seC4h1wiKVxuICAgICAgICAgICAgICAgIC5teShcIuGAgOGAreGAr+GAmuGAuuGAkeGAiuGAuuGAoeGAmeGAvuGAkOGAulwiKVxuICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgIHByb3BOYW1lPVwicG9saWN5LmV4dC5jaGFzc2lzTm9cIlxuICAgICAgICAgICAgICBydWxlcz17W1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcihydWxlOiBhbnksIHZhbHVlOiBhbnksIGNhbGxiYWNrOiBhbnkpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFSdWxlcy5pc1ZhbGlkQ2hhc3Npc05vKHZhbHVlKSkge1xuICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKFxuICAgICAgICAgICAgICAgICAgICAgICAgTGFuZ3VhZ2UuZW4oXCJDaGFzc2lzIE5vLiBtdXN0IGJlIDYtMTcgZGlnaXRzLCBhbmQgaW4gRW5nbGlzaCBsZXR0ZXJzIG9yIG51bWJlcnNcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguKvguKHguLLguKLguYDguKXguILguJXguLHguKfguJbguLHguIfguJXguYnguK3guIfguKHguLUgNi0xNyDguKvguKXguLHguIHguYHguKXguLDguYDguJvguYfguJnguJXguLHguKfguK3guLHguIHguKnguKPguKDguLLguKnguLLguK3guLHguIfguIHguKTguKlcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgXX1cbiAgICAgICAgICAgIC8+XG5cbiAgICAgICAgICAgIDxOU3dpdGNoXG4gICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgcHJvcE5hbWU9e1wicG9saWN5LmV4dC5tb3J0Z2FnZUxvYW5cIn1cbiAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiTW9ydGdhZ2UgTG9hbj9cIilcbiAgICAgICAgICAgICAgICAudGhhaShcIuC4quC4tOC4meC5gOC4iuC4t+C5iOC4reC4o+C4luC4ouC4meC4leC5jD9cIilcbiAgICAgICAgICAgICAgICAubXkoXCLhgJXhgLHhgKvhgIThgLrhgJThgL7hgLbhgIThgL3hgLHhgIHhgLvhgLHhgLg/XCIpXG4gICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgY2hlY2tlZD17dGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJleHQubW9ydGdhZ2VMb2FuXCIpKSA9PT0gXCJZXCJ9XG4gICAgICAgICAgICAvPlxuXG4gICAgICAgICAgICB7dGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJleHQubW9ydGdhZ2VMb2FuXCIpKSA9PT0gXCJZXCIgJiYgKFxuICAgICAgICAgICAgICA8TlNlbGVjdFxuICAgICAgICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJGaW5hbmNpbmcgQ28uXCIpXG4gICAgICAgICAgICAgICAgICAudGhhaShcIuC4muC4o+C4tOC4qeC4seC4l+C4quC4tOC4meC5gOC4iuC4t+C5iOC4rVwiKVxuICAgICAgICAgICAgICAgICAgLm15KFwi4YCE4YC+4YCs4YC44YCb4YCZ4YC64YC44YCe4YCK4YC64YC34YCA4YCv4YCZ4YC54YCV4YCP4YCuXCIpXG4gICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgIHRhYmxlTmFtZT1cImJlbmVmaWNpYXJ5XCJcbiAgICAgICAgICAgICAgICBkYXRhRml4ZWQ9e1wicG9saWN5XCJ9XG4gICAgICAgICAgICAgICAgcHJvcE5hbWU9XCJwb2xpY3kuZXh0LmJlbmVmaWNpYXJ5XCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICl9XG5cbiAgICAgICAgICAgIHt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImV4dC5tb3J0Z2FnZUxvYW5cIikpID09PSBcIllcIiAmJlxuICAgICAgICAgICAgdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJleHQuYmVuZWZpY2lhcnlcIikpID09PSBDb25zdHMuQkVORUZJQ0lBUllfT1RIRVJTICYmIChcbiAgICAgICAgICAgICAgPE5UZXh0XG4gICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiQmVuZWZpY2lhcnkgTmFtZVwiKVxuICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguIrguLfguYjguK3guJrguKPguLTguKnguLHguJfguKrguLTguJnguYDguIrguLfguYjguK1cIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgcHJvcE5hbWU9XCJwb2xpY3kuZXh0LmJlbmVmaWNpYXJ5TmFtZVwiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICApfVxuICAgICAgICAgIDwvTlBhbmVsPlxuXG4gICAgICAgICAgPE5QYW5lbFxuICAgICAgICAgICAga2V5PVwiMlwiXG4gICAgICAgICAgICBoZWFkZXI9e0xhbmd1YWdlLmVuKFwiSW5zdXJlZFwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4nOC4ueC5ieC5gOC4reC4suC4m+C4o+C4sOC4geC4seC4mVwiKVxuICAgICAgICAgICAgICAubXkoXCLhgKHhgKzhgJnhgIHhgLbhgJHhgKzhgLhcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8SW5zdXJlZCBtb2RlbD17bW9kZWx9IGZvcm09e2Zvcm19IGRhdGFJZFByZWZpeD17XCJwb2xpY3kuZXh0Lmluc3VyZWRcIn0gZGF0YUZpeGVkPXtcInBvbGljeVwifS8+XG4gICAgICAgICAgPC9OUGFuZWw+XG5cbiAgICAgICAgICA8TlBhbmVsXG4gICAgICAgICAgICBrZXk9XCIzXCJcbiAgICAgICAgICAgIGhlYWRlcj17TGFuZ3VhZ2UuZW4oXCJQYXllclwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4nOC4ueC5ieC4iuC4s+C4o+C4sOC5gOC4muC4teC5ieC4ouC4m+C4o+C4sOC4geC4seC4mVwiKVxuICAgICAgICAgICAgICAubXkoXCLhgKHhgIHhgL3hgJThgLrhgIbhgLHhgKzhgIThgLrhgJ7hgLBcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8UGF5ZXIgbW9kZWw9e21vZGVsfSBmb3JtPXtmb3JtfSBkYXRhSWRQcmVmaXg9e1wicG9saWN5LmV4dC5wYXllclwifSBpbnN1cmVkRGF0YUlkUHJlZml4PXtcInBvbGljeVwifS8+XG4gICAgICAgICAgPC9OUGFuZWw+XG5cbiAgICAgICAgICA8TlBhbmVsXG4gICAgICAgICAgICBrZXk9XCI0XCJcbiAgICAgICAgICAgIGhlYWRlcj17TGFuZ3VhZ2UuZW4oXCJBdHRhY2htZW50c1wiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4quC4tOC5iOC4h+C4l+C4teC5iOC5geC4meC4muC4oeC4slwiKS5teShcIuGAgeGAu+GAreGAkOGAuuGAkOGAveGAsuGAmeGAvuGAr+GAmeGAu+GArOGAuFwiKS5teShcIuGAgeGAu+GAreGAkOGAuuGAkOGAveGAsuGAmeGAvuGAr+GAmeGAu+GArOGAuFwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIHtwbGFuVHlwZUNvZGUgPT09IFwiQ0xBU1MxXCIgP1xuICAgICAgICAgICAgICA8QXR0YWNobWVudFxuICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgIGRhdGFJZFByZWZpeD17XCJwb2xpY3lcIn1cbiAgICAgICAgICAgICAgICBkb2NUeXBlcz17Z2V0RG9jVHlwZUZvclZNSUNsYXNzMSgpfVxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtkaXNhYmxlQnRuID0+IHtcbiAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogIWRpc2FibGVCdG4sXG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAvPiA6XG4gICAgICAgICAgICAgIDxBdHRhY2htZW50XG4gICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgZGF0YUlkUHJlZml4PXtcInBvbGljeVwifVxuICAgICAgICAgICAgICAgIGRvY1R5cGVzPXtnZXREb2NUeXBlRm9yVk1JKCl9XG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9e2Rpc2FibGVCdG4gPT4ge1xuICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiAhZGlzYWJsZUJ0bixcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgPC9OUGFuZWw+XG4gICAgICAgIDwvTkNvbGxhcHNlPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVyUG9saWN5UHJlbWl1bShwb2xpY3k6IGFueSA9IHt9LCBwYXltZW50U2NoZWR1bGU6IGFueSA9IG51bGwsIGNhcnRQcmVtaXVtOiBhbnkpIHtcbiAgICBjb25zdCB7IG9wZW5FbmQsIHJlY3VycmluZ1BheW1lbnQgfSA9IHBvbGljeTtcbiAgICBjb25zdCBwb2xpY3lQcmVtaXVtID0gVXRpbHMuZ2V0UG9saWN5UHJlbWl1bVZtaShwb2xpY3ksIHBheW1lbnRTY2hlZHVsZSwgY2FydFByZW1pdW0pO1xuICAgIGNvbnN0IHsgYW1vdW50LCBjdXJyZW5jeVRleHQgfSA9IFV0aWxzLmZvcm1hdEN1cnJlbmN5SW5mbyhwb2xpY3lQcmVtaXVtLmFyLCBwb2xpY3kuY3VycmVuY3lDb2RlKTtcbiAgICBjb25zdCBpc0luc3RhbGxtZW50UGF5bWVudCA9IG9wZW5FbmQgfHwgcmVjdXJyaW5nUGF5bWVudCB8fCBwYXltZW50U2NoZWR1bGUgPT09IFBBWU1FTlRfTUVUSE9EX1JFQ1VSUklORztcbiAgICByZXR1cm4gKFxuICAgICAgPHNwYW5cbiAgICAgICAgY2xhc3NOYW1lPVwicHJpY2UgcHJpY2UtLWxnXCJcbiAgICAgICAgZGF0YS1wb3N0Zml4PXtcbiAgICAgICAgICBpc0luc3RhbGxtZW50UGF5bWVudFxuICAgICAgICAgICAgPyBMYW5ndWFnZS5lbihcIiAvIG1vbnRoXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwiIC/guYDguJTguLfguK3guJlcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKVxuICAgICAgICAgICAgOiBcIlwiXG4gICAgICAgIH1cbiAgICAgICAgZGF0YS1wcmVmaXg9e2N1cnJlbmN5VGV4dH1cbiAgICAgID5cbiAgICAgICAge2Ftb3VudH1cbiAgICAgIDwvc3Bhbj5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlclRpdGxlKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCBleHQgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImV4dFwiKSk7XG4gICAgY29uc3QgaXRudE5hbWUgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIml0bnROYW1lXCIpKTtcbiAgICBjb25zdCBpdG50Q29kZSA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiaXRudENvZGVcIikpO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRpdGxlXCI+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiUG9saWN5IERldGFpbHNcIilcbiAgICAgICAgICAgIC50aGFpKFwi4Lij4Liy4Lii4Lil4Liw4LmA4Lit4Li14Lii4LiU4LiB4Lij4Lih4LiY4Lij4Lij4Lih4LmMXCIpXG4gICAgICAgICAgICAubXkoXCLhgJnhgLDhgJ3hgKvhgJLhgKHhgJ7hgLHhgLjhgIXhgK3hgJDhgLpcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8cCBjbGFzc05hbWU9XCJwYWdlLWRlc2NyaXB0XCI+XG4gICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJZb3UgaGF2ZSBzZWxlY3RlZFwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC5geC4nOC4meC4m+C4o+C4sOC4geC4seC4meC4l+C4teC5iOC5gOC4peC4t+C4reC4gVwiKVxuICAgICAgICAgICAgICAubXkoXCLhgJ7hgIThgLrhgJvhgL3hgLHhgLjhgIHhgLvhgJrhgLrhgJHhgKzhgLjhgJ7hgLHhgKxcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPGEgY2xhc3NOYW1lPVwicGxhbi1uYW1lXCI+e2V4dC5wbGFuTmFtZSB8fCBcIlwifTwvYT5cbiAgICAgICAgICA8YSBjbGFzc05hbWU9XCJpbnN1cmFuY2UtY29tcGFueVwiPntpdG50TmFtZSB8fCBpdG50Q29kZX08L2E+XG4gICAgICAgICAge3RoaXMucmVuZGVyUG9saWN5UHJlbWl1bSh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicG9saWN5XCIpLCBcIlwiLCB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwiY2FydFByZW1pdW1cIikpfVxuICAgICAgICA8L3A+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCB7IERldGFpbHMgfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBVUE7QUFDQTtBQUlBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQURBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFEQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUExQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTs7O0FBRUE7QUFDQTtBQUVBO0FBQ0E7OztBQWlDQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFPQTtBQUNBO0FBeEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTZCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUEyQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFiQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQStCQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFYQTtBQXJCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFzQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFYQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQVBBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFnQkE7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBT0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFjQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7OztBQTFhQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/compare/vmi/details.tsx
