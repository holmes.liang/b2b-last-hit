/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var Scale = __webpack_require__(/*! ./Scale */ "./node_modules/echarts/lib/scale/Scale.js");

var numberUtil = __webpack_require__(/*! ../util/number */ "./node_modules/echarts/lib/util/number.js");

var IntervalScale = __webpack_require__(/*! ./Interval */ "./node_modules/echarts/lib/scale/Interval.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Log scale
 * @module echarts/scale/Log
 */
// Use some method of IntervalScale


var scaleProto = Scale.prototype;
var intervalScaleProto = IntervalScale.prototype;
var getPrecisionSafe = numberUtil.getPrecisionSafe;
var roundingErrorFix = numberUtil.round;
var mathFloor = Math.floor;
var mathCeil = Math.ceil;
var mathPow = Math.pow;
var mathLog = Math.log;
var LogScale = Scale.extend({
  type: 'log',
  base: 10,
  $constructor: function $constructor() {
    Scale.apply(this, arguments);
    this._originalScale = new IntervalScale();
  },

  /**
   * @return {Array.<number>}
   */
  getTicks: function getTicks() {
    var originalScale = this._originalScale;
    var extent = this._extent;
    var originalExtent = originalScale.getExtent();
    return zrUtil.map(intervalScaleProto.getTicks.call(this), function (val) {
      var powVal = numberUtil.round(mathPow(this.base, val)); // Fix #4158

      powVal = val === extent[0] && originalScale.__fixMin ? fixRoundingError(powVal, originalExtent[0]) : powVal;
      powVal = val === extent[1] && originalScale.__fixMax ? fixRoundingError(powVal, originalExtent[1]) : powVal;
      return powVal;
    }, this);
  },

  /**
   * @param {number} val
   * @return {string}
   */
  getLabel: intervalScaleProto.getLabel,

  /**
   * @param  {number} val
   * @return {number}
   */
  scale: function scale(val) {
    val = scaleProto.scale.call(this, val);
    return mathPow(this.base, val);
  },

  /**
   * @param {number} start
   * @param {number} end
   */
  setExtent: function setExtent(start, end) {
    var base = this.base;
    start = mathLog(start) / mathLog(base);
    end = mathLog(end) / mathLog(base);
    intervalScaleProto.setExtent.call(this, start, end);
  },

  /**
   * @return {number} end
   */
  getExtent: function getExtent() {
    var base = this.base;
    var extent = scaleProto.getExtent.call(this);
    extent[0] = mathPow(base, extent[0]);
    extent[1] = mathPow(base, extent[1]); // Fix #4158

    var originalScale = this._originalScale;
    var originalExtent = originalScale.getExtent();
    originalScale.__fixMin && (extent[0] = fixRoundingError(extent[0], originalExtent[0]));
    originalScale.__fixMax && (extent[1] = fixRoundingError(extent[1], originalExtent[1]));
    return extent;
  },

  /**
   * @param  {Array.<number>} extent
   */
  unionExtent: function unionExtent(extent) {
    this._originalScale.unionExtent(extent);

    var base = this.base;
    extent[0] = mathLog(extent[0]) / mathLog(base);
    extent[1] = mathLog(extent[1]) / mathLog(base);
    scaleProto.unionExtent.call(this, extent);
  },

  /**
   * @override
   */
  unionExtentFromData: function unionExtentFromData(data, dim) {
    // TODO
    // filter value that <= 0
    this.unionExtent(data.getApproximateExtent(dim));
  },

  /**
   * Update interval and extent of intervals for nice ticks
   * @param  {number} [approxTickNum = 10] Given approx tick number
   */
  niceTicks: function niceTicks(approxTickNum) {
    approxTickNum = approxTickNum || 10;
    var extent = this._extent;
    var span = extent[1] - extent[0];

    if (span === Infinity || span <= 0) {
      return;
    }

    var interval = numberUtil.quantity(span);
    var err = approxTickNum / span * interval; // Filter ticks to get closer to the desired count.

    if (err <= 0.5) {
      interval *= 10;
    } // Interval should be integer


    while (!isNaN(interval) && Math.abs(interval) < 1 && Math.abs(interval) > 0) {
      interval *= 10;
    }

    var niceExtent = [numberUtil.round(mathCeil(extent[0] / interval) * interval), numberUtil.round(mathFloor(extent[1] / interval) * interval)];
    this._interval = interval;
    this._niceExtent = niceExtent;
  },

  /**
   * Nice extent.
   * @override
   */
  niceExtent: function niceExtent(opt) {
    intervalScaleProto.niceExtent.call(this, opt);
    var originalScale = this._originalScale;
    originalScale.__fixMin = opt.fixMin;
    originalScale.__fixMax = opt.fixMax;
  }
});
zrUtil.each(['contain', 'normalize'], function (methodName) {
  LogScale.prototype[methodName] = function (val) {
    val = mathLog(val) / mathLog(this.base);
    return scaleProto[methodName].call(this, val);
  };
});

LogScale.create = function () {
  return new LogScale();
};

function fixRoundingError(val, originalVal) {
  return roundingErrorFix(val, getPrecisionSafe(originalVal));
}

var _default = LogScale;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvc2NhbGUvTG9nLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvc2NhbGUvTG9nLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIFNjYWxlID0gcmVxdWlyZShcIi4vU2NhbGVcIik7XG5cbnZhciBudW1iZXJVdGlsID0gcmVxdWlyZShcIi4uL3V0aWwvbnVtYmVyXCIpO1xuXG52YXIgSW50ZXJ2YWxTY2FsZSA9IHJlcXVpcmUoXCIuL0ludGVydmFsXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qKlxuICogTG9nIHNjYWxlXG4gKiBAbW9kdWxlIGVjaGFydHMvc2NhbGUvTG9nXG4gKi9cbi8vIFVzZSBzb21lIG1ldGhvZCBvZiBJbnRlcnZhbFNjYWxlXG52YXIgc2NhbGVQcm90byA9IFNjYWxlLnByb3RvdHlwZTtcbnZhciBpbnRlcnZhbFNjYWxlUHJvdG8gPSBJbnRlcnZhbFNjYWxlLnByb3RvdHlwZTtcbnZhciBnZXRQcmVjaXNpb25TYWZlID0gbnVtYmVyVXRpbC5nZXRQcmVjaXNpb25TYWZlO1xudmFyIHJvdW5kaW5nRXJyb3JGaXggPSBudW1iZXJVdGlsLnJvdW5kO1xudmFyIG1hdGhGbG9vciA9IE1hdGguZmxvb3I7XG52YXIgbWF0aENlaWwgPSBNYXRoLmNlaWw7XG52YXIgbWF0aFBvdyA9IE1hdGgucG93O1xudmFyIG1hdGhMb2cgPSBNYXRoLmxvZztcbnZhciBMb2dTY2FsZSA9IFNjYWxlLmV4dGVuZCh7XG4gIHR5cGU6ICdsb2cnLFxuICBiYXNlOiAxMCxcbiAgJGNvbnN0cnVjdG9yOiBmdW5jdGlvbiAoKSB7XG4gICAgU2NhbGUuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICB0aGlzLl9vcmlnaW5hbFNjYWxlID0gbmV3IEludGVydmFsU2NhbGUoKTtcbiAgfSxcblxuICAvKipcbiAgICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59XG4gICAqL1xuICBnZXRUaWNrczogZnVuY3Rpb24gKCkge1xuICAgIHZhciBvcmlnaW5hbFNjYWxlID0gdGhpcy5fb3JpZ2luYWxTY2FsZTtcbiAgICB2YXIgZXh0ZW50ID0gdGhpcy5fZXh0ZW50O1xuICAgIHZhciBvcmlnaW5hbEV4dGVudCA9IG9yaWdpbmFsU2NhbGUuZ2V0RXh0ZW50KCk7XG4gICAgcmV0dXJuIHpyVXRpbC5tYXAoaW50ZXJ2YWxTY2FsZVByb3RvLmdldFRpY2tzLmNhbGwodGhpcyksIGZ1bmN0aW9uICh2YWwpIHtcbiAgICAgIHZhciBwb3dWYWwgPSBudW1iZXJVdGlsLnJvdW5kKG1hdGhQb3codGhpcy5iYXNlLCB2YWwpKTsgLy8gRml4ICM0MTU4XG5cbiAgICAgIHBvd1ZhbCA9IHZhbCA9PT0gZXh0ZW50WzBdICYmIG9yaWdpbmFsU2NhbGUuX19maXhNaW4gPyBmaXhSb3VuZGluZ0Vycm9yKHBvd1ZhbCwgb3JpZ2luYWxFeHRlbnRbMF0pIDogcG93VmFsO1xuICAgICAgcG93VmFsID0gdmFsID09PSBleHRlbnRbMV0gJiYgb3JpZ2luYWxTY2FsZS5fX2ZpeE1heCA/IGZpeFJvdW5kaW5nRXJyb3IocG93VmFsLCBvcmlnaW5hbEV4dGVudFsxXSkgOiBwb3dWYWw7XG4gICAgICByZXR1cm4gcG93VmFsO1xuICAgIH0sIHRoaXMpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge251bWJlcn0gdmFsXG4gICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICovXG4gIGdldExhYmVsOiBpbnRlcnZhbFNjYWxlUHJvdG8uZ2V0TGFiZWwsXG5cbiAgLyoqXG4gICAqIEBwYXJhbSAge251bWJlcn0gdmFsXG4gICAqIEByZXR1cm4ge251bWJlcn1cbiAgICovXG4gIHNjYWxlOiBmdW5jdGlvbiAodmFsKSB7XG4gICAgdmFsID0gc2NhbGVQcm90by5zY2FsZS5jYWxsKHRoaXMsIHZhbCk7XG4gICAgcmV0dXJuIG1hdGhQb3codGhpcy5iYXNlLCB2YWwpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge251bWJlcn0gc3RhcnRcbiAgICogQHBhcmFtIHtudW1iZXJ9IGVuZFxuICAgKi9cbiAgc2V0RXh0ZW50OiBmdW5jdGlvbiAoc3RhcnQsIGVuZCkge1xuICAgIHZhciBiYXNlID0gdGhpcy5iYXNlO1xuICAgIHN0YXJ0ID0gbWF0aExvZyhzdGFydCkgLyBtYXRoTG9nKGJhc2UpO1xuICAgIGVuZCA9IG1hdGhMb2coZW5kKSAvIG1hdGhMb2coYmFzZSk7XG4gICAgaW50ZXJ2YWxTY2FsZVByb3RvLnNldEV4dGVudC5jYWxsKHRoaXMsIHN0YXJ0LCBlbmQpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcmV0dXJuIHtudW1iZXJ9IGVuZFxuICAgKi9cbiAgZ2V0RXh0ZW50OiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGJhc2UgPSB0aGlzLmJhc2U7XG4gICAgdmFyIGV4dGVudCA9IHNjYWxlUHJvdG8uZ2V0RXh0ZW50LmNhbGwodGhpcyk7XG4gICAgZXh0ZW50WzBdID0gbWF0aFBvdyhiYXNlLCBleHRlbnRbMF0pO1xuICAgIGV4dGVudFsxXSA9IG1hdGhQb3coYmFzZSwgZXh0ZW50WzFdKTsgLy8gRml4ICM0MTU4XG5cbiAgICB2YXIgb3JpZ2luYWxTY2FsZSA9IHRoaXMuX29yaWdpbmFsU2NhbGU7XG4gICAgdmFyIG9yaWdpbmFsRXh0ZW50ID0gb3JpZ2luYWxTY2FsZS5nZXRFeHRlbnQoKTtcbiAgICBvcmlnaW5hbFNjYWxlLl9fZml4TWluICYmIChleHRlbnRbMF0gPSBmaXhSb3VuZGluZ0Vycm9yKGV4dGVudFswXSwgb3JpZ2luYWxFeHRlbnRbMF0pKTtcbiAgICBvcmlnaW5hbFNjYWxlLl9fZml4TWF4ICYmIChleHRlbnRbMV0gPSBmaXhSb3VuZGluZ0Vycm9yKGV4dGVudFsxXSwgb3JpZ2luYWxFeHRlbnRbMV0pKTtcbiAgICByZXR1cm4gZXh0ZW50O1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0gIHtBcnJheS48bnVtYmVyPn0gZXh0ZW50XG4gICAqL1xuICB1bmlvbkV4dGVudDogZnVuY3Rpb24gKGV4dGVudCkge1xuICAgIHRoaXMuX29yaWdpbmFsU2NhbGUudW5pb25FeHRlbnQoZXh0ZW50KTtcblxuICAgIHZhciBiYXNlID0gdGhpcy5iYXNlO1xuICAgIGV4dGVudFswXSA9IG1hdGhMb2coZXh0ZW50WzBdKSAvIG1hdGhMb2coYmFzZSk7XG4gICAgZXh0ZW50WzFdID0gbWF0aExvZyhleHRlbnRbMV0pIC8gbWF0aExvZyhiYXNlKTtcbiAgICBzY2FsZVByb3RvLnVuaW9uRXh0ZW50LmNhbGwodGhpcywgZXh0ZW50KTtcbiAgfSxcblxuICAvKipcbiAgICogQG92ZXJyaWRlXG4gICAqL1xuICB1bmlvbkV4dGVudEZyb21EYXRhOiBmdW5jdGlvbiAoZGF0YSwgZGltKSB7XG4gICAgLy8gVE9ET1xuICAgIC8vIGZpbHRlciB2YWx1ZSB0aGF0IDw9IDBcbiAgICB0aGlzLnVuaW9uRXh0ZW50KGRhdGEuZ2V0QXBwcm94aW1hdGVFeHRlbnQoZGltKSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBpbnRlcnZhbCBhbmQgZXh0ZW50IG9mIGludGVydmFscyBmb3IgbmljZSB0aWNrc1xuICAgKiBAcGFyYW0gIHtudW1iZXJ9IFthcHByb3hUaWNrTnVtID0gMTBdIEdpdmVuIGFwcHJveCB0aWNrIG51bWJlclxuICAgKi9cbiAgbmljZVRpY2tzOiBmdW5jdGlvbiAoYXBwcm94VGlja051bSkge1xuICAgIGFwcHJveFRpY2tOdW0gPSBhcHByb3hUaWNrTnVtIHx8IDEwO1xuICAgIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7XG4gICAgdmFyIHNwYW4gPSBleHRlbnRbMV0gLSBleHRlbnRbMF07XG5cbiAgICBpZiAoc3BhbiA9PT0gSW5maW5pdHkgfHwgc3BhbiA8PSAwKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIGludGVydmFsID0gbnVtYmVyVXRpbC5xdWFudGl0eShzcGFuKTtcbiAgICB2YXIgZXJyID0gYXBwcm94VGlja051bSAvIHNwYW4gKiBpbnRlcnZhbDsgLy8gRmlsdGVyIHRpY2tzIHRvIGdldCBjbG9zZXIgdG8gdGhlIGRlc2lyZWQgY291bnQuXG5cbiAgICBpZiAoZXJyIDw9IDAuNSkge1xuICAgICAgaW50ZXJ2YWwgKj0gMTA7XG4gICAgfSAvLyBJbnRlcnZhbCBzaG91bGQgYmUgaW50ZWdlclxuXG5cbiAgICB3aGlsZSAoIWlzTmFOKGludGVydmFsKSAmJiBNYXRoLmFicyhpbnRlcnZhbCkgPCAxICYmIE1hdGguYWJzKGludGVydmFsKSA+IDApIHtcbiAgICAgIGludGVydmFsICo9IDEwO1xuICAgIH1cblxuICAgIHZhciBuaWNlRXh0ZW50ID0gW251bWJlclV0aWwucm91bmQobWF0aENlaWwoZXh0ZW50WzBdIC8gaW50ZXJ2YWwpICogaW50ZXJ2YWwpLCBudW1iZXJVdGlsLnJvdW5kKG1hdGhGbG9vcihleHRlbnRbMV0gLyBpbnRlcnZhbCkgKiBpbnRlcnZhbCldO1xuICAgIHRoaXMuX2ludGVydmFsID0gaW50ZXJ2YWw7XG4gICAgdGhpcy5fbmljZUV4dGVudCA9IG5pY2VFeHRlbnQ7XG4gIH0sXG5cbiAgLyoqXG4gICAqIE5pY2UgZXh0ZW50LlxuICAgKiBAb3ZlcnJpZGVcbiAgICovXG4gIG5pY2VFeHRlbnQ6IGZ1bmN0aW9uIChvcHQpIHtcbiAgICBpbnRlcnZhbFNjYWxlUHJvdG8ubmljZUV4dGVudC5jYWxsKHRoaXMsIG9wdCk7XG4gICAgdmFyIG9yaWdpbmFsU2NhbGUgPSB0aGlzLl9vcmlnaW5hbFNjYWxlO1xuICAgIG9yaWdpbmFsU2NhbGUuX19maXhNaW4gPSBvcHQuZml4TWluO1xuICAgIG9yaWdpbmFsU2NhbGUuX19maXhNYXggPSBvcHQuZml4TWF4O1xuICB9XG59KTtcbnpyVXRpbC5lYWNoKFsnY29udGFpbicsICdub3JtYWxpemUnXSwgZnVuY3Rpb24gKG1ldGhvZE5hbWUpIHtcbiAgTG9nU2NhbGUucHJvdG90eXBlW21ldGhvZE5hbWVdID0gZnVuY3Rpb24gKHZhbCkge1xuICAgIHZhbCA9IG1hdGhMb2codmFsKSAvIG1hdGhMb2codGhpcy5iYXNlKTtcbiAgICByZXR1cm4gc2NhbGVQcm90b1ttZXRob2ROYW1lXS5jYWxsKHRoaXMsIHZhbCk7XG4gIH07XG59KTtcblxuTG9nU2NhbGUuY3JlYXRlID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gbmV3IExvZ1NjYWxlKCk7XG59O1xuXG5mdW5jdGlvbiBmaXhSb3VuZGluZ0Vycm9yKHZhbCwgb3JpZ2luYWxWYWwpIHtcbiAgcmV0dXJuIHJvdW5kaW5nRXJyb3JGaXgodmFsLCBnZXRQcmVjaXNpb25TYWZlKG9yaWdpbmFsVmFsKSk7XG59XG5cbnZhciBfZGVmYXVsdCA9IExvZ1NjYWxlO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7OztBQUlBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE5SEE7QUFnSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/scale/Log.js
