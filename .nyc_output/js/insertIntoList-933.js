/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule insertIntoList
 * @format
 * 
 */

/**
 * Maintain persistence for target list when appending and prepending.
 */

function insertIntoList(targetList, toInsert, offset) {
  if (offset === targetList.count()) {
    toInsert.forEach(function (c) {
      targetList = targetList.push(c);
    });
  } else if (offset === 0) {
    toInsert.reverse().forEach(function (c) {
      targetList = targetList.unshift(c);
    });
  } else {
    var head = targetList.slice(0, offset);
    var tail = targetList.slice(offset);
    targetList = head.concat(toInsert, tail).toList();
  }

  return targetList;
}

module.exports = insertIntoList;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2luc2VydEludG9MaXN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2luc2VydEludG9MaXN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgaW5zZXJ0SW50b0xpc3RcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBNYWludGFpbiBwZXJzaXN0ZW5jZSBmb3IgdGFyZ2V0IGxpc3Qgd2hlbiBhcHBlbmRpbmcgYW5kIHByZXBlbmRpbmcuXG4gKi9cbmZ1bmN0aW9uIGluc2VydEludG9MaXN0KHRhcmdldExpc3QsIHRvSW5zZXJ0LCBvZmZzZXQpIHtcbiAgaWYgKG9mZnNldCA9PT0gdGFyZ2V0TGlzdC5jb3VudCgpKSB7XG4gICAgdG9JbnNlcnQuZm9yRWFjaChmdW5jdGlvbiAoYykge1xuICAgICAgdGFyZ2V0TGlzdCA9IHRhcmdldExpc3QucHVzaChjKTtcbiAgICB9KTtcbiAgfSBlbHNlIGlmIChvZmZzZXQgPT09IDApIHtcbiAgICB0b0luc2VydC5yZXZlcnNlKCkuZm9yRWFjaChmdW5jdGlvbiAoYykge1xuICAgICAgdGFyZ2V0TGlzdCA9IHRhcmdldExpc3QudW5zaGlmdChjKTtcbiAgICB9KTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgaGVhZCA9IHRhcmdldExpc3Quc2xpY2UoMCwgb2Zmc2V0KTtcbiAgICB2YXIgdGFpbCA9IHRhcmdldExpc3Quc2xpY2Uob2Zmc2V0KTtcbiAgICB0YXJnZXRMaXN0ID0gaGVhZC5jb25jYXQodG9JbnNlcnQsIHRhaWwpLnRvTGlzdCgpO1xuICB9XG4gIHJldHVybiB0YXJnZXRMaXN0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGluc2VydEludG9MaXN0OyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBRUE7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/insertIntoList.js
