/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule keyCommandBackspaceWord
 * @format
 * 
 */


var DraftRemovableWord = __webpack_require__(/*! ./DraftRemovableWord */ "./node_modules/draft-js/lib/DraftRemovableWord.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var moveSelectionBackward = __webpack_require__(/*! ./moveSelectionBackward */ "./node_modules/draft-js/lib/moveSelectionBackward.js");

var removeTextWithStrategy = __webpack_require__(/*! ./removeTextWithStrategy */ "./node_modules/draft-js/lib/removeTextWithStrategy.js");
/**
 * Delete the word that is left of the cursor, as well as any spaces or
 * punctuation after the word.
 */


function keyCommandBackspaceWord(editorState) {
  var afterRemoval = removeTextWithStrategy(editorState, function (strategyState) {
    var selection = strategyState.getSelection();
    var offset = selection.getStartOffset(); // If there are no words before the cursor, remove the preceding newline.

    if (offset === 0) {
      return moveSelectionBackward(strategyState, 1);
    }

    var key = selection.getStartKey();
    var content = strategyState.getCurrentContent();
    var text = content.getBlockForKey(key).getText().slice(0, offset);
    var toRemove = DraftRemovableWord.getBackward(text);
    return moveSelectionBackward(strategyState, toRemove.length || 1);
  }, 'backward');

  if (afterRemoval === editorState.getCurrentContent()) {
    return editorState;
  }

  return EditorState.push(editorState, afterRemoval, 'remove-range');
}

module.exports = keyCommandBackspaceWord;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRCYWNrc3BhY2VXb3JkLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRCYWNrc3BhY2VXb3JkLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUga2V5Q29tbWFuZEJhY2tzcGFjZVdvcmRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0UmVtb3ZhYmxlV29yZCA9IHJlcXVpcmUoJy4vRHJhZnRSZW1vdmFibGVXb3JkJyk7XG52YXIgRWRpdG9yU3RhdGUgPSByZXF1aXJlKCcuL0VkaXRvclN0YXRlJyk7XG5cbnZhciBtb3ZlU2VsZWN0aW9uQmFja3dhcmQgPSByZXF1aXJlKCcuL21vdmVTZWxlY3Rpb25CYWNrd2FyZCcpO1xudmFyIHJlbW92ZVRleHRXaXRoU3RyYXRlZ3kgPSByZXF1aXJlKCcuL3JlbW92ZVRleHRXaXRoU3RyYXRlZ3knKTtcblxuLyoqXG4gKiBEZWxldGUgdGhlIHdvcmQgdGhhdCBpcyBsZWZ0IG9mIHRoZSBjdXJzb3IsIGFzIHdlbGwgYXMgYW55IHNwYWNlcyBvclxuICogcHVuY3R1YXRpb24gYWZ0ZXIgdGhlIHdvcmQuXG4gKi9cbmZ1bmN0aW9uIGtleUNvbW1hbmRCYWNrc3BhY2VXb3JkKGVkaXRvclN0YXRlKSB7XG4gIHZhciBhZnRlclJlbW92YWwgPSByZW1vdmVUZXh0V2l0aFN0cmF0ZWd5KGVkaXRvclN0YXRlLCBmdW5jdGlvbiAoc3RyYXRlZ3lTdGF0ZSkge1xuICAgIHZhciBzZWxlY3Rpb24gPSBzdHJhdGVneVN0YXRlLmdldFNlbGVjdGlvbigpO1xuICAgIHZhciBvZmZzZXQgPSBzZWxlY3Rpb24uZ2V0U3RhcnRPZmZzZXQoKTtcbiAgICAvLyBJZiB0aGVyZSBhcmUgbm8gd29yZHMgYmVmb3JlIHRoZSBjdXJzb3IsIHJlbW92ZSB0aGUgcHJlY2VkaW5nIG5ld2xpbmUuXG4gICAgaWYgKG9mZnNldCA9PT0gMCkge1xuICAgICAgcmV0dXJuIG1vdmVTZWxlY3Rpb25CYWNrd2FyZChzdHJhdGVneVN0YXRlLCAxKTtcbiAgICB9XG4gICAgdmFyIGtleSA9IHNlbGVjdGlvbi5nZXRTdGFydEtleSgpO1xuICAgIHZhciBjb250ZW50ID0gc3RyYXRlZ3lTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpO1xuICAgIHZhciB0ZXh0ID0gY29udGVudC5nZXRCbG9ja0ZvcktleShrZXkpLmdldFRleHQoKS5zbGljZSgwLCBvZmZzZXQpO1xuICAgIHZhciB0b1JlbW92ZSA9IERyYWZ0UmVtb3ZhYmxlV29yZC5nZXRCYWNrd2FyZCh0ZXh0KTtcbiAgICByZXR1cm4gbW92ZVNlbGVjdGlvbkJhY2t3YXJkKHN0cmF0ZWd5U3RhdGUsIHRvUmVtb3ZlLmxlbmd0aCB8fCAxKTtcbiAgfSwgJ2JhY2t3YXJkJyk7XG5cbiAgaWYgKGFmdGVyUmVtb3ZhbCA9PT0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKSkge1xuICAgIHJldHVybiBlZGl0b3JTdGF0ZTtcbiAgfVxuXG4gIHJldHVybiBFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCBhZnRlclJlbW92YWwsICdyZW1vdmUtcmFuZ2UnKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBrZXlDb21tYW5kQmFja3NwYWNlV29yZDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/keyCommandBackspaceWord.js
