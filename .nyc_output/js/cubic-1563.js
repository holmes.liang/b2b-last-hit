var curve = __webpack_require__(/*! ../core/curve */ "./node_modules/zrender/lib/core/curve.js");
/**
 * 三次贝塞尔曲线描边包含判断
 * @param  {number}  x0
 * @param  {number}  y0
 * @param  {number}  x1
 * @param  {number}  y1
 * @param  {number}  x2
 * @param  {number}  y2
 * @param  {number}  x3
 * @param  {number}  y3
 * @param  {number}  lineWidth
 * @param  {number}  x
 * @param  {number}  y
 * @return {boolean}
 */


function containStroke(x0, y0, x1, y1, x2, y2, x3, y3, lineWidth, x, y) {
  if (lineWidth === 0) {
    return false;
  }

  var _l = lineWidth; // Quick reject

  if (y > y0 + _l && y > y1 + _l && y > y2 + _l && y > y3 + _l || y < y0 - _l && y < y1 - _l && y < y2 - _l && y < y3 - _l || x > x0 + _l && x > x1 + _l && x > x2 + _l && x > x3 + _l || x < x0 - _l && x < x1 - _l && x < x2 - _l && x < x3 - _l) {
    return false;
  }

  var d = curve.cubicProjectPoint(x0, y0, x1, y1, x2, y2, x3, y3, x, y, null);
  return d <= _l / 2;
}

exports.containStroke = containStroke;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi9jdWJpYy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2NvbnRhaW4vY3ViaWMuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIGN1cnZlID0gcmVxdWlyZShcIi4uL2NvcmUvY3VydmVcIik7XG5cbi8qKlxuICog5LiJ5qyh6LSd5aGe5bCU5puy57q/5o+P6L655YyF5ZCr5Yik5patXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB4MFxuICogQHBhcmFtICB7bnVtYmVyfSAgeTBcbiAqIEBwYXJhbSAge251bWJlcn0gIHgxXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB5MVxuICogQHBhcmFtICB7bnVtYmVyfSAgeDJcbiAqIEBwYXJhbSAge251bWJlcn0gIHkyXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB4M1xuICogQHBhcmFtICB7bnVtYmVyfSAgeTNcbiAqIEBwYXJhbSAge251bWJlcn0gIGxpbmVXaWR0aFxuICogQHBhcmFtICB7bnVtYmVyfSAgeFxuICogQHBhcmFtICB7bnVtYmVyfSAgeVxuICogQHJldHVybiB7Ym9vbGVhbn1cbiAqL1xuZnVuY3Rpb24gY29udGFpblN0cm9rZSh4MCwgeTAsIHgxLCB5MSwgeDIsIHkyLCB4MywgeTMsIGxpbmVXaWR0aCwgeCwgeSkge1xuICBpZiAobGluZVdpZHRoID09PSAwKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgdmFyIF9sID0gbGluZVdpZHRoOyAvLyBRdWljayByZWplY3RcblxuICBpZiAoeSA+IHkwICsgX2wgJiYgeSA+IHkxICsgX2wgJiYgeSA+IHkyICsgX2wgJiYgeSA+IHkzICsgX2wgfHwgeSA8IHkwIC0gX2wgJiYgeSA8IHkxIC0gX2wgJiYgeSA8IHkyIC0gX2wgJiYgeSA8IHkzIC0gX2wgfHwgeCA+IHgwICsgX2wgJiYgeCA+IHgxICsgX2wgJiYgeCA+IHgyICsgX2wgJiYgeCA+IHgzICsgX2wgfHwgeCA8IHgwIC0gX2wgJiYgeCA8IHgxIC0gX2wgJiYgeCA8IHgyIC0gX2wgJiYgeCA8IHgzIC0gX2wpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICB2YXIgZCA9IGN1cnZlLmN1YmljUHJvamVjdFBvaW50KHgwLCB5MCwgeDEsIHkxLCB4MiwgeTIsIHgzLCB5MywgeCwgeSwgbnVsbCk7XG4gIHJldHVybiBkIDw9IF9sIC8gMjtcbn1cblxuZXhwb3J0cy5jb250YWluU3Ryb2tlID0gY29udGFpblN0cm9rZTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/contain/cubic.js
