
/**
 * Determine if a DOM element matches a CSS selector
 *
 * @param {Element} elem
 * @param {String} selector
 * @return {Boolean}
 * @api public
 */

function matches(elem, selector) {
  // Vendor-specific implementations of `Element.prototype.matches()`.
  var proto = window.Element.prototype;
  var nativeMatches = proto.matches || proto.mozMatchesSelector || proto.msMatchesSelector || proto.oMatchesSelector || proto.webkitMatchesSelector;

  if (!elem || elem.nodeType !== 1) {
    return false;
  }

  var parentElem = elem.parentNode; // use native 'matches'

  if (nativeMatches) {
    return nativeMatches.call(elem, selector);
  } // native support for `matches` is missing and a fallback is required


  var nodes = parentElem.querySelectorAll(selector);
  var len = nodes.length;

  for (var i = 0; i < len; i++) {
    if (nodes[i] === elem) {
      return true;
    }
  }

  return false;
}
/**
 * Expose `matches`
 */


module.exports = matches;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZG9tLW1hdGNoZXMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kb20tbWF0Y2hlcy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgRE9NIGVsZW1lbnQgbWF0Y2hlcyBhIENTUyBzZWxlY3RvclxuICpcbiAqIEBwYXJhbSB7RWxlbWVudH0gZWxlbVxuICogQHBhcmFtIHtTdHJpbmd9IHNlbGVjdG9yXG4gKiBAcmV0dXJuIHtCb29sZWFufVxuICogQGFwaSBwdWJsaWNcbiAqL1xuXG5mdW5jdGlvbiBtYXRjaGVzKGVsZW0sIHNlbGVjdG9yKSB7XG4gIC8vIFZlbmRvci1zcGVjaWZpYyBpbXBsZW1lbnRhdGlvbnMgb2YgYEVsZW1lbnQucHJvdG90eXBlLm1hdGNoZXMoKWAuXG4gIHZhciBwcm90byA9IHdpbmRvdy5FbGVtZW50LnByb3RvdHlwZTtcbiAgdmFyIG5hdGl2ZU1hdGNoZXMgPSBwcm90by5tYXRjaGVzIHx8XG4gICAgICBwcm90by5tb3pNYXRjaGVzU2VsZWN0b3IgfHxcbiAgICAgIHByb3RvLm1zTWF0Y2hlc1NlbGVjdG9yIHx8XG4gICAgICBwcm90by5vTWF0Y2hlc1NlbGVjdG9yIHx8XG4gICAgICBwcm90by53ZWJraXRNYXRjaGVzU2VsZWN0b3I7XG5cbiAgaWYgKCFlbGVtIHx8IGVsZW0ubm9kZVR5cGUgIT09IDEpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICB2YXIgcGFyZW50RWxlbSA9IGVsZW0ucGFyZW50Tm9kZTtcblxuICAvLyB1c2UgbmF0aXZlICdtYXRjaGVzJ1xuICBpZiAobmF0aXZlTWF0Y2hlcykge1xuICAgIHJldHVybiBuYXRpdmVNYXRjaGVzLmNhbGwoZWxlbSwgc2VsZWN0b3IpO1xuICB9XG5cbiAgLy8gbmF0aXZlIHN1cHBvcnQgZm9yIGBtYXRjaGVzYCBpcyBtaXNzaW5nIGFuZCBhIGZhbGxiYWNrIGlzIHJlcXVpcmVkXG4gIHZhciBub2RlcyA9IHBhcmVudEVsZW0ucXVlcnlTZWxlY3RvckFsbChzZWxlY3Rvcik7XG4gIHZhciBsZW4gPSBub2Rlcy5sZW5ndGg7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgIGlmIChub2Rlc1tpXSA9PT0gZWxlbSkge1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGZhbHNlO1xufVxuXG4vKipcbiAqIEV4cG9zZSBgbWF0Y2hlc2BcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IG1hdGNoZXM7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBRUE7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUlBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/dom-matches/index.js
