function windingLine(x0, y0, x1, y1, x, y) {
  if (y > y0 && y > y1 || y < y0 && y < y1) {
    return 0;
  } // Ignore horizontal line


  if (y1 === y0) {
    return 0;
  }

  var dir = y1 < y0 ? 1 : -1;
  var t = (y - y0) / (y1 - y0); // Avoid winding error when intersection point is the connect point of two line of polygon

  if (t === 1 || t === 0) {
    dir = y1 < y0 ? 0.5 : -0.5;
  }

  var x_ = t * (x1 - x0) + x0; // If (x, y) on the line, considered as "contain".

  return x_ === x ? Infinity : x_ > x ? dir : 0;
}

module.exports = windingLine;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi93aW5kaW5nTGluZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2NvbnRhaW4vd2luZGluZ0xpbmUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gd2luZGluZ0xpbmUoeDAsIHkwLCB4MSwgeTEsIHgsIHkpIHtcbiAgaWYgKHkgPiB5MCAmJiB5ID4geTEgfHwgeSA8IHkwICYmIHkgPCB5MSkge1xuICAgIHJldHVybiAwO1xuICB9IC8vIElnbm9yZSBob3Jpem9udGFsIGxpbmVcblxuXG4gIGlmICh5MSA9PT0geTApIHtcbiAgICByZXR1cm4gMDtcbiAgfVxuXG4gIHZhciBkaXIgPSB5MSA8IHkwID8gMSA6IC0xO1xuICB2YXIgdCA9ICh5IC0geTApIC8gKHkxIC0geTApOyAvLyBBdm9pZCB3aW5kaW5nIGVycm9yIHdoZW4gaW50ZXJzZWN0aW9uIHBvaW50IGlzIHRoZSBjb25uZWN0IHBvaW50IG9mIHR3byBsaW5lIG9mIHBvbHlnb25cblxuICBpZiAodCA9PT0gMSB8fCB0ID09PSAwKSB7XG4gICAgZGlyID0geTEgPCB5MCA/IDAuNSA6IC0wLjU7XG4gIH1cblxuICB2YXIgeF8gPSB0ICogKHgxIC0geDApICsgeDA7IC8vIElmICh4LCB5KSBvbiB0aGUgbGluZSwgY29uc2lkZXJlZCBhcyBcImNvbnRhaW5cIi5cblxuICByZXR1cm4geF8gPT09IHggPyBJbmZpbml0eSA6IHhfID4geCA/IGRpciA6IDA7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gd2luZGluZ0xpbmU7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/contain/windingLine.js
