__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _message_count_state__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../message-count-state */ "./src/app/desk/component/page/message-count-state.tsx");
/* harmony import */ var _desk_component_quill_html__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/quill-html */ "./src/app/desk/component/quill-html.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/page/components/email-table.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_6__["default"])(["\n  ", "\n  .email-title {\n    display: flex; \n    align-items: center;\n    padding: 5px 10px 10px;\n    border-bottom: 1px solid #d9d9d9;\n  }\n  .email-body {\n    min-height: 100px;\n    max-height: 400px;\n    display: flex;\n    align-items: center;\n    flex-wrap: wrap;\n    >li{\n      cursor: pointer;\n      list-style: none;\n      width: 100%;\n      padding: 10px 15px;\n      text-align: center;\n      &:not(:last-child) {\n        border-bottom: 1px solid #d9d9d9;\n      }\n      >p:nth-child(1) {\n        font-weight: 500;\n        text-align: left;\n        font-size: 16px;\n\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0width:100%;\n        white-space: nowrap;\n        overflow: hidden;\n        text-overflow: ellipsis;\n      }\n      >.email-content-div {\n        display: flex;\n        justify-content: space-between;\n        width:100%;\n        >p {\n          white-space: nowrap;\n          overflow: hidden;\n          text-overflow: ellipsis;\n          flex: 1;\n          text-align: left;\n          color: #9e9e9e;\n          font-size: 12px;\n        }\n      }\n     \n    }\n  }\n  .email-footer {\n    border-top: 1px solid #d9d9d9;\n    padding: 10px 0;\n    text-align: center;\n    color: ", ";\n    cursor: pointer;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}











var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_13__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

var isMobile = _common__WEBPACK_IMPORTED_MODULE_11__["Utils"].getIsMobile();
var EmailDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject(), isMobile ? "width: 300px" : "width: 400px", COLOR_PRIMARY);

var EmailCountTable = function EmailCountTable(props) {
  var countChange = _message_count_state__WEBPACK_IMPORTED_MODULE_14__["default"].useContainer();
  return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(EmailTable, Object.assign({}, props, {
    onRead: countChange.getCount,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85
    },
    __self: this
  }));
};

var EmailTable =
/*#__PURE__*/
function (_React$Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(EmailTable, _React$Component);

  function EmailTable() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, EmailTable);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(EmailTable)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      emailOptions: [],
      loading: false
    };

    _this.showEmailContent = function (item) {
      _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].patch("/consume/imsgs/".concat(item.imsgId, "/read")).then(function () {
        _this.props.changeVisible();

        _this.props.onRead();

        if (item.hasContent) {
          _this.setState({
            loading: true
          });

          _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].get("/consume/imsgs/".concat(item.imsgId, "/content")).then(function (res) {
            _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_12__["default"].create({
              Component: function Component(_ref) {
                var onCancel = _ref.onCancel,
                    onOk = _ref.onOk;
                return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Modal"], {
                  width: isMobile ? "100%" : "60%",
                  centered: true,
                  visible: true,
                  title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("MESSAGE").thai("MESSAGE").my("MESSAGE").getMessage(),
                  onCancel: onCancel,
                  footer: [_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                    key: "OK",
                    type: "primary",
                    onClick: onOk,
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 146
                    },
                    __self: this
                  }, _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("OK").thai("OK").my("OK").getMessage())],
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 136
                  },
                  __self: this
                }, _this.showHtml(res.body.respData));
              },
              onOk: function onOk() {}
            });
          }).finally(function () {
            _this.setState({
              loading: false
            });
          });
        }
      });
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(EmailTable, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var params = {
        readStatus: "unread",
        pageSize: 5,
        pageIndex: 1
      };
      this.setState({
        loading: true
      });
      _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_11__["Apis"].MESSAGES_QUERY_EMAIL, params).then(function (res) {
        _this2.setState({
          emailOptions: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(res, "body.respData.items", [])
        });
      }).finally(function () {
        _this2.setState({
          loading: false
        });
      });
    }
  }, {
    key: "showHtml",
    value: function showHtml(htmlString) {
      var style = isMobile ? {
        maxHeight: 300,
        overflow: "auto"
      } : {
        maxHeight: 500,
        overflow: "auto"
      };
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component_quill_html__WEBPACK_IMPORTED_MODULE_15__["default"], {
        style: style,
        text: htmlString,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120
        },
        __self: this
      });
    }
  }, {
    key: "renderEmails",
    value: function renderEmails() {
      var _this3 = this;

      var options = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(this.state.emailOptions);

      if (lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(options)) return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 172
        },
        __self: this
      }, "No message yet");
      return options.map(function (item) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("li", {
          onClick: function onClick() {
            _this3.showEmailContent(item);
          },
          key: item.imsgId,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 174
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("p", {
          title: item.subject,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 177
          },
          __self: this
        }, item.subject), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
          className: "email-content-div",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 178
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("p", {
          title: item.publishedUserName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 179
          },
          __self: this
        }, item.publishedUserName), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 180
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_11__["Utils"].dateTimeFromNow(item.publishedAt))));
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(EmailDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 188
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "email-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 189
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        type: "mail",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 190
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("span", {
        style: {
          marginLeft: 10,
          fontWeight: 600
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }, "MESSAGES")), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Spin"], {
        spinning: this.state.loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 193
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("ul", {
        className: "email-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 194
        },
        __self: this
      }, this.renderEmails())), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "email-footer",
        onClick: function onClick() {
          _this4.props.changeVisible();

          _this4.props.history.push(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"].ADMIN_MESSAGE_EMAIL_DETAILS);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 198
        },
        __self: this
      }, "View All", _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        type: "right",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 203
        },
        __self: this
      })));
    }
  }]);

  return EmailTable;
}(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Component);

;
/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_9__["withRouter"])(EmailCountTable));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BhZ2UvY29tcG9uZW50cy9lbWFpbC10YWJsZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcGFnZS9jb21wb25lbnRzL2VtYWlsLXRhYmxlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBJY29uLCBTcGluLCBNb2RhbCwgQnV0dG9uIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IHdpdGhSb3V0ZXIsIFJvdXRlQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwicmVhY3Qtcm91dGVyLWRvbVwiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuXG5pbXBvcnQgeyBBamF4LCBBcGlzLCBMYW5ndWFnZSwgUEFUSCwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IE1hc2sgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9jcmVhdGUtZGlhbG9nXCI7XG5pbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXNcIjtcbmltcG9ydCBDb3VudENvbnRhaW5lciBmcm9tIFwiLi4vbWVzc2FnZS1jb3VudC1zdGF0ZVwiO1xuaW1wb3J0IFF1aWxsSHRtbCBmcm9tIFwiQGRlc2stY29tcG9uZW50L3F1aWxsLWh0bWxcIjtcblxuY29uc3QgeyBDT0xPUl9QUklNQVJZIH0gPSBUaGVtZS5nZXRUaGVtZSgpO1xuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuY29uc3QgRW1haWxEaXYgPSBTdHlsZWQuZGl2YFxuICAke2lzTW9iaWxlID8gXCJ3aWR0aDogMzAwcHhcIiA6IFwid2lkdGg6IDQwMHB4XCJ9XG4gIC5lbWFpbC10aXRsZSB7XG4gICAgZGlzcGxheTogZmxleDsgXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiA1cHggMTBweCAxMHB4O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDlkOWQ5O1xuICB9XG4gIC5lbWFpbC1ib2R5IHtcbiAgICBtaW4taGVpZ2h0OiAxMDBweDtcbiAgICBtYXgtaGVpZ2h0OiA0MDBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgID5saXtcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIHBhZGRpbmc6IDEwcHggMTVweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICY6bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDlkOWQ5O1xuICAgICAgfVxuICAgICAgPnA6bnRoLWNoaWxkKDEpIHtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuwqDCoMKgwqDCoMKgwqDCoHdpZHRoOjEwMCU7XG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgfVxuICAgICAgPi5lbWFpbC1jb250ZW50LWRpdiB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgd2lkdGg6MTAwJTtcbiAgICAgICAgPnAge1xuICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICAgICAgICBmbGV4OiAxO1xuICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgY29sb3I6ICM5ZTllOWU7XG4gICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgIFxuICAgIH1cbiAgfVxuICAuZW1haWwtZm9vdGVyIHtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2Q5ZDlkOTtcbiAgICBwYWRkaW5nOiAxMHB4IDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAke0NPTE9SX1BSSU1BUll9O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgfVxuYDtcblxuaW50ZXJmYWNlIElTdGF0ZSB7XG4gIGVtYWlsT3B0aW9uczogYW55W10sXG4gIGxvYWRpbmc6IGJvb2xlYW4sXG59XG5cbmludGVyZmFjZSBJUHJvcHMgZXh0ZW5kcyBSb3V0ZUNvbXBvbmVudFByb3BzIHtcbiAgY2hhbmdlVmlzaWJsZTogRnVuY3Rpb24sXG4gIG9uUmVhZDogRnVuY3Rpb24sXG59XG5cbmNvbnN0IEVtYWlsQ291bnRUYWJsZSA9IChwcm9wczogYW55KSA9PiB7XG4gIGxldCBjb3VudENoYW5nZSA9IENvdW50Q29udGFpbmVyLnVzZUNvbnRhaW5lcigpO1xuXG4gIHJldHVybiA8RW1haWxUYWJsZSB7Li4ucHJvcHN9IG9uUmVhZD17Y291bnRDaGFuZ2UuZ2V0Q291bnR9Lz47XG59O1xuXG5jbGFzcyBFbWFpbFRhYmxlIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50PElQcm9wcywgSVN0YXRlPiB7XG5cbiAgc3RhdGU6IElTdGF0ZSA9IHtcbiAgICBlbWFpbE9wdGlvbnM6IFtdLFxuICAgIGxvYWRpbmc6IGZhbHNlLFxuICB9O1xuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGNvbnN0IHBhcmFtcyA9IHtcbiAgICAgIHJlYWRTdGF0dXM6IFwidW5yZWFkXCIsXG4gICAgICBwYWdlU2l6ZTogNSxcbiAgICAgIHBhZ2VJbmRleDogMSxcbiAgICB9O1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgbG9hZGluZzogdHJ1ZSxcbiAgICB9KTtcbiAgICBBamF4LnBvc3QoQXBpcy5NRVNTQUdFU19RVUVSWV9FTUFJTCwgcGFyYW1zKVxuICAgICAgLnRoZW4oKHJlcykgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBlbWFpbE9wdGlvbnM6IF8uZ2V0KHJlcywgXCJib2R5LnJlc3BEYXRhLml0ZW1zXCIsIFtdKSxcbiAgICAgICAgfSk7XG4gICAgICB9KVxuICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgfVxuXG4gIHNob3dIdG1sKGh0bWxTdHJpbmc6IGFueSkge1xuICAgIGNvbnN0IHN0eWxlID0gaXNNb2JpbGVcbiAgICAgID8geyBtYXhIZWlnaHQ6IDMwMCwgb3ZlcmZsb3c6IFwiYXV0b1wiIH0gOiB7IG1heEhlaWdodDogNTAwLCBvdmVyZmxvdzogXCJhdXRvXCIgfTtcbiAgICByZXR1cm4gPFF1aWxsSHRtbCBzdHlsZT17c3R5bGV9IHRleHQ9e2h0bWxTdHJpbmd9Lz47XG4gIH1cblxuICBzaG93RW1haWxDb250ZW50ID0gKGl0ZW06IGFueSkgPT4ge1xuICAgIEFqYXgucGF0Y2goYC9jb25zdW1lL2ltc2dzLyR7aXRlbS5pbXNnSWR9L3JlYWRgKVxuICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICB0aGlzLnByb3BzLmNoYW5nZVZpc2libGUoKTtcbiAgICAgICAgdGhpcy5wcm9wcy5vblJlYWQoKTtcbiAgICAgICAgaWYgKGl0ZW0uaGFzQ29udGVudCkge1xuICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgbG9hZGluZzogdHJ1ZSxcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBBamF4LmdldChgL2NvbnN1bWUvaW1zZ3MvJHtpdGVtLmltc2dJZH0vY29udGVudGApXG4gICAgICAgICAgICAudGhlbigocmVzKSA9PiB7XG4gICAgICAgICAgICAgIE1hc2suY3JlYXRlKHtcbiAgICAgICAgICAgICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsLCBvbk9rIH06IGFueSkgPT4gKFxuICAgICAgICAgICAgICAgICAgPE1vZGFsXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoPXtpc01vYmlsZSA/IFwiMTAwJVwiIDogXCI2MCVcIn1cbiAgICAgICAgICAgICAgICAgICAgY2VudGVyZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgICAgIHZpc2libGU9e3RydWV9XG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIk1FU1NBR0VcIilcbiAgICAgICAgICAgICAgICAgICAgICAudGhhaShcIk1FU1NBR0VcIilcbiAgICAgICAgICAgICAgICAgICAgICAubXkoXCJNRVNTQUdFXCIpXG4gICAgICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9e29uQ2FuY2VsfVxuICAgICAgICAgICAgICAgICAgICBmb290ZXI9e1tcbiAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIGtleT1cIk9LXCIgdHlwZT1cInByaW1hcnlcIiBvbkNsaWNrPXtvbk9rfT5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIk9LXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwiT0tcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLm15KFwiT0tcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgICA8L0J1dHRvbj4sXG4gICAgICAgICAgICAgICAgICAgIF19XG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIHt0aGlzLnNob3dIdG1sKHJlcy5ib2R5LnJlc3BEYXRhKX1cbiAgICAgICAgICAgICAgICAgIDwvTW9kYWw+XG4gICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICBvbk9rOiAoKSA9PiB7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gIH07XG5cbiAgcmVuZGVyRW1haWxzKCkge1xuICAgIGNvbnN0IG9wdGlvbnMgPSBbLi4udGhpcy5zdGF0ZS5lbWFpbE9wdGlvbnNdO1xuICAgIGlmIChfLmlzRW1wdHkob3B0aW9ucykpIHJldHVybiA8bGk+Tm8gbWVzc2FnZSB5ZXQ8L2xpPjtcbiAgICByZXR1cm4gb3B0aW9ucy5tYXAoKGl0ZW06IGFueSkgPT4ge1xuICAgICAgcmV0dXJuICg8bGkgb25DbGljaz17KCkgPT4ge1xuICAgICAgICB0aGlzLnNob3dFbWFpbENvbnRlbnQoaXRlbSk7XG4gICAgICB9fSBrZXk9e2l0ZW0uaW1zZ0lkfT5cbiAgICAgICAgPHAgdGl0bGU9e2l0ZW0uc3ViamVjdH0+e2l0ZW0uc3ViamVjdH08L3A+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtcImVtYWlsLWNvbnRlbnQtZGl2XCJ9PlxuICAgICAgICAgIDxwIHRpdGxlPXtpdGVtLnB1Ymxpc2hlZFVzZXJOYW1lfT57aXRlbS5wdWJsaXNoZWRVc2VyTmFtZX08L3A+XG4gICAgICAgICAgPHNwYW4+e1V0aWxzLmRhdGVUaW1lRnJvbU5vdyhpdGVtLnB1Ymxpc2hlZEF0KX08L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9saT4pO1xuICAgIH0pO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPEVtYWlsRGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17XCJlbWFpbC10aXRsZVwifT5cbiAgICAgICAgICA8SWNvbiB0eXBlPVwibWFpbFwiLz5cbiAgICAgICAgICA8c3BhbiBzdHlsZT17eyBtYXJnaW5MZWZ0OiAxMCwgZm9udFdlaWdodDogNjAwIH19Pk1FU1NBR0VTPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPFNwaW4gc3Bpbm5pbmc9e3RoaXMuc3RhdGUubG9hZGluZ30+XG4gICAgICAgICAgPHVsIGNsYXNzTmFtZT17XCJlbWFpbC1ib2R5XCJ9PlxuICAgICAgICAgICAge3RoaXMucmVuZGVyRW1haWxzKCl9XG4gICAgICAgICAgPC91bD5cbiAgICAgICAgPC9TcGluPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17XCJlbWFpbC1mb290ZXJcIn0gb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgIHRoaXMucHJvcHMuY2hhbmdlVmlzaWJsZSgpO1xuICAgICAgICAgIHRoaXMucHJvcHMuaGlzdG9yeS5wdXNoKFBBVEguQURNSU5fTUVTU0FHRV9FTUFJTF9ERVRBSUxTKTtcbiAgICAgICAgfX0+XG4gICAgICAgICAgVmlldyBBbGxcbiAgICAgICAgICA8SWNvbiB0eXBlPVwicmlnaHRcIi8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9FbWFpbERpdj5cbiAgICApO1xuICB9XG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoUm91dGVyKEVtYWlsQ291bnRUYWJsZSk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQW1FQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBZ0NBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFWQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQXNCQTtBQXZCQTtBQTBCQTtBQUVBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQTFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUVBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBaURBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7Ozs7QUF2SEE7QUFDQTtBQXVIQTtBQUVBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/page/components/email-table.tsx
