__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");


function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    .kpi {\n      ul {\n        padding: 0;\n        height: 148px;\n        list-style: none;\n        margin: 0;\n        li {\n          margin-top: 11px;\n          zoom: 1;\n          color: rgba(0,0,0,.75);\n          .i-span {\n            margin: 0 30px 0 10px;\n          }\n          .ant-progress-text {\n            color: #535353;\n          }\n          span {\n            font-size: 12px;\n            font-weight: 300;\n          }\n          label {\n            display: inline-block;\n            width: 60px;\n            text-align: right;\n          }\n          &.li-active {\n            label {\n              color: rgba(0,0,0,1);\n              font-size: 30px;\n            }\n          }\n        }\n      }\n    }\n  "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}


/* harmony default export */ __webpack_exports__["default"] = ({
  KPI: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject())
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svaG9tZS9pbmRleC1jb21tb24tc3R5bGUudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svaG9tZS9pbmRleC1jb21tb24tc3R5bGUudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIEtQSTogU3R5bGVkLmRpdmBcbiAgICAua3BpIHtcbiAgICAgIHVsIHtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgaGVpZ2h0OiAxNDhweDtcbiAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBsaSB7XG4gICAgICAgICAgbWFyZ2luLXRvcDogMTFweDtcbiAgICAgICAgICB6b29tOiAxO1xuICAgICAgICAgIGNvbG9yOiByZ2JhKDAsMCwwLC43NSk7XG4gICAgICAgICAgLmktc3BhbiB7XG4gICAgICAgICAgICBtYXJnaW46IDAgMzBweCAwIDEwcHg7XG4gICAgICAgICAgfVxuICAgICAgICAgIC5hbnQtcHJvZ3Jlc3MtdGV4dCB7XG4gICAgICAgICAgICBjb2xvcjogIzUzNTM1MztcbiAgICAgICAgICB9XG4gICAgICAgICAgc3BhbiB7XG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgICAgIH1cbiAgICAgICAgICBsYWJlbCB7XG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgICAgICB3aWR0aDogNjBweDtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICAgIH1cbiAgICAgICAgICAmLmxpLWFjdGl2ZSB7XG4gICAgICAgICAgICBsYWJlbCB7XG4gICAgICAgICAgICAgIGNvbG9yOiByZ2JhKDAsMCwwLDEpO1xuICAgICAgICAgICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICBgLFxufTsiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBRUE7QUFDQTtBQURBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/home/index-common-style.tsx
