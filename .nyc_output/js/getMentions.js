__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getMentions; });
/* harmony import */ var _getRegExp__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./getRegExp */ "./node_modules/rc-editor-mention/es/utils/getRegExp.js");

function getMentions(contentState) {
  var prefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '@';
  var regex = Object(_getRegExp__WEBPACK_IMPORTED_MODULE_0__["default"])(prefix);
  var entities = [];
  contentState.getBlockMap().forEach(function (block) {
    var blockText = block.getText();
    var matchArr = void 0;

    while ((matchArr = regex.exec(blockText)) !== null) {
      // eslint-disable-line
      entities.push(matchArr[0].trim());
    }
  });
  return entities;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvZ2V0TWVudGlvbnMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1lZGl0b3ItbWVudGlvbi9lcy91dGlscy9nZXRNZW50aW9ucy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZ2V0UmVnRXhwIGZyb20gJy4vZ2V0UmVnRXhwJztcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gZ2V0TWVudGlvbnMoY29udGVudFN0YXRlKSB7XG4gIHZhciBwcmVmaXggPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6ICdAJztcblxuICB2YXIgcmVnZXggPSBnZXRSZWdFeHAocHJlZml4KTtcbiAgdmFyIGVudGl0aWVzID0gW107XG4gIGNvbnRlbnRTdGF0ZS5nZXRCbG9ja01hcCgpLmZvckVhY2goZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgdmFyIGJsb2NrVGV4dCA9IGJsb2NrLmdldFRleHQoKTtcbiAgICB2YXIgbWF0Y2hBcnIgPSB2b2lkIDA7XG4gICAgd2hpbGUgKChtYXRjaEFyciA9IHJlZ2V4LmV4ZWMoYmxvY2tUZXh0KSkgIT09IG51bGwpIHtcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLWxpbmVcbiAgICAgIGVudGl0aWVzLnB1c2gobWF0Y2hBcnJbMF0udHJpbSgpKTtcbiAgICB9XG4gIH0pO1xuICByZXR1cm4gZW50aXRpZXM7XG59Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/utils/getMentions.js
