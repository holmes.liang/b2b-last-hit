var util = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var _event = __webpack_require__(/*! ../core/event */ "./node_modules/zrender/lib/core/event.js");

var Dispatcher = _event.Dispatcher;

var requestAnimationFrame = __webpack_require__(/*! ./requestAnimationFrame */ "./node_modules/zrender/lib/animation/requestAnimationFrame.js");

var Animator = __webpack_require__(/*! ./Animator */ "./node_modules/zrender/lib/animation/Animator.js");
/**
 * 动画主类, 调度和管理所有动画控制器
 *
 * @module zrender/animation/Animation
 * @author pissang(https://github.com/pissang)
 */
// TODO Additive animation
// http://iosoteric.com/additive-animations-animatewithduration-in-ios-8/
// https://developer.apple.com/videos/wwdc2014/#236

/**
 * @typedef {Object} IZRenderStage
 * @property {Function} update
 */

/**
 * @alias module:zrender/animation/Animation
 * @constructor
 * @param {Object} [options]
 * @param {Function} [options.onframe]
 * @param {IZRenderStage} [options.stage]
 * @example
 *     var animation = new Animation();
 *     var obj = {
 *         x: 100,
 *         y: 100
 *     };
 *     animation.animate(node.position)
 *         .when(1000, {
 *             x: 500,
 *             y: 500
 *         })
 *         .when(2000, {
 *             x: 100,
 *             y: 100
 *         })
 *         .start('spline');
 */


var Animation = function Animation(options) {
  options = options || {};
  this.stage = options.stage || {};

  this.onframe = options.onframe || function () {}; // private properties


  this._clips = [];
  this._running = false;
  this._time;
  this._pausedTime;
  this._pauseStart;
  this._paused = false;
  Dispatcher.call(this);
};

Animation.prototype = {
  constructor: Animation,

  /**
   * 添加 clip
   * @param {module:zrender/animation/Clip} clip
   */
  addClip: function addClip(clip) {
    this._clips.push(clip);
  },

  /**
   * 添加 animator
   * @param {module:zrender/animation/Animator} animator
   */
  addAnimator: function addAnimator(animator) {
    animator.animation = this;
    var clips = animator.getClips();

    for (var i = 0; i < clips.length; i++) {
      this.addClip(clips[i]);
    }
  },

  /**
   * 删除动画片段
   * @param {module:zrender/animation/Clip} clip
   */
  removeClip: function removeClip(clip) {
    var idx = util.indexOf(this._clips, clip);

    if (idx >= 0) {
      this._clips.splice(idx, 1);
    }
  },

  /**
   * 删除动画片段
   * @param {module:zrender/animation/Animator} animator
   */
  removeAnimator: function removeAnimator(animator) {
    var clips = animator.getClips();

    for (var i = 0; i < clips.length; i++) {
      this.removeClip(clips[i]);
    }

    animator.animation = null;
  },
  _update: function _update() {
    var time = new Date().getTime() - this._pausedTime;

    var delta = time - this._time;
    var clips = this._clips;
    var len = clips.length;
    var deferredEvents = [];
    var deferredClips = [];

    for (var i = 0; i < len; i++) {
      var clip = clips[i];
      var e = clip.step(time, delta); // Throw out the events need to be called after
      // stage.update, like destroy

      if (e) {
        deferredEvents.push(e);
        deferredClips.push(clip);
      }
    } // Remove the finished clip


    for (var i = 0; i < len;) {
      if (clips[i]._needsRemove) {
        clips[i] = clips[len - 1];
        clips.pop();
        len--;
      } else {
        i++;
      }
    }

    len = deferredEvents.length;

    for (var i = 0; i < len; i++) {
      deferredClips[i].fire(deferredEvents[i]);
    }

    this._time = time;
    this.onframe(delta); // 'frame' should be triggered before stage, because upper application
    // depends on the sequence (e.g., echarts-stream and finish
    // event judge)

    this.trigger('frame', delta);

    if (this.stage.update) {
      this.stage.update();
    }
  },
  _startLoop: function _startLoop() {
    var self = this;
    this._running = true;

    function step() {
      if (self._running) {
        requestAnimationFrame(step);
        !self._paused && self._update();
      }
    }

    requestAnimationFrame(step);
  },

  /**
   * Start animation.
   */
  start: function start() {
    this._time = new Date().getTime();
    this._pausedTime = 0;

    this._startLoop();
  },

  /**
   * Stop animation.
   */
  stop: function stop() {
    this._running = false;
  },

  /**
   * Pause animation.
   */
  pause: function pause() {
    if (!this._paused) {
      this._pauseStart = new Date().getTime();
      this._paused = true;
    }
  },

  /**
   * Resume animation.
   */
  resume: function resume() {
    if (this._paused) {
      this._pausedTime += new Date().getTime() - this._pauseStart;
      this._paused = false;
    }
  },

  /**
   * Clear animation.
   */
  clear: function clear() {
    this._clips = [];
  },

  /**
   * Whether animation finished.
   */
  isFinished: function isFinished() {
    return !this._clips.length;
  },

  /**
   * Creat animator for a target, whose props can be animated.
   *
   * @param  {Object} target
   * @param  {Object} options
   * @param  {boolean} [options.loop=false] Whether loop animation.
   * @param  {Function} [options.getter=null] Get value from target.
   * @param  {Function} [options.setter=null] Set value to target.
   * @return {module:zrender/animation/Animation~Animator}
   */
  // TODO Gap
  animate: function animate(target, options) {
    options = options || {};
    var animator = new Animator(target, options.loop, options.getter, options.setter);
    this.addAnimator(animator);
    return animator;
  }
};
util.mixin(Animation, Dispatcher);
var _default = Animation;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvYW5pbWF0aW9uL0FuaW1hdGlvbi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2FuaW1hdGlvbi9BbmltYXRpb24uanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIHV0aWwgPSByZXF1aXJlKFwiLi4vY29yZS91dGlsXCIpO1xuXG52YXIgX2V2ZW50ID0gcmVxdWlyZShcIi4uL2NvcmUvZXZlbnRcIik7XG5cbnZhciBEaXNwYXRjaGVyID0gX2V2ZW50LkRpc3BhdGNoZXI7XG5cbnZhciByZXF1ZXN0QW5pbWF0aW9uRnJhbWUgPSByZXF1aXJlKFwiLi9yZXF1ZXN0QW5pbWF0aW9uRnJhbWVcIik7XG5cbnZhciBBbmltYXRvciA9IHJlcXVpcmUoXCIuL0FuaW1hdG9yXCIpO1xuXG4vKipcbiAqIOWKqOeUu+S4u+exuywg6LCD5bqm5ZKM566h55CG5omA5pyJ5Yqo55S75o6n5Yi25ZmoXG4gKlxuICogQG1vZHVsZSB6cmVuZGVyL2FuaW1hdGlvbi9BbmltYXRpb25cbiAqIEBhdXRob3IgcGlzc2FuZyhodHRwczovL2dpdGh1Yi5jb20vcGlzc2FuZylcbiAqL1xuLy8gVE9ETyBBZGRpdGl2ZSBhbmltYXRpb25cbi8vIGh0dHA6Ly9pb3NvdGVyaWMuY29tL2FkZGl0aXZlLWFuaW1hdGlvbnMtYW5pbWF0ZXdpdGhkdXJhdGlvbi1pbi1pb3MtOC9cbi8vIGh0dHBzOi8vZGV2ZWxvcGVyLmFwcGxlLmNvbS92aWRlb3Mvd3dkYzIwMTQvIzIzNlxuXG4vKipcbiAqIEB0eXBlZGVmIHtPYmplY3R9IElaUmVuZGVyU3RhZ2VcbiAqIEBwcm9wZXJ0eSB7RnVuY3Rpb259IHVwZGF0ZVxuICovXG5cbi8qKlxuICogQGFsaWFzIG1vZHVsZTp6cmVuZGVyL2FuaW1hdGlvbi9BbmltYXRpb25cbiAqIEBjb25zdHJ1Y3RvclxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRpb25zXVxuICogQHBhcmFtIHtGdW5jdGlvbn0gW29wdGlvbnMub25mcmFtZV1cbiAqIEBwYXJhbSB7SVpSZW5kZXJTdGFnZX0gW29wdGlvbnMuc3RhZ2VdXG4gKiBAZXhhbXBsZVxuICogICAgIHZhciBhbmltYXRpb24gPSBuZXcgQW5pbWF0aW9uKCk7XG4gKiAgICAgdmFyIG9iaiA9IHtcbiAqICAgICAgICAgeDogMTAwLFxuICogICAgICAgICB5OiAxMDBcbiAqICAgICB9O1xuICogICAgIGFuaW1hdGlvbi5hbmltYXRlKG5vZGUucG9zaXRpb24pXG4gKiAgICAgICAgIC53aGVuKDEwMDAsIHtcbiAqICAgICAgICAgICAgIHg6IDUwMCxcbiAqICAgICAgICAgICAgIHk6IDUwMFxuICogICAgICAgICB9KVxuICogICAgICAgICAud2hlbigyMDAwLCB7XG4gKiAgICAgICAgICAgICB4OiAxMDAsXG4gKiAgICAgICAgICAgICB5OiAxMDBcbiAqICAgICAgICAgfSlcbiAqICAgICAgICAgLnN0YXJ0KCdzcGxpbmUnKTtcbiAqL1xudmFyIEFuaW1hdGlvbiA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICB0aGlzLnN0YWdlID0gb3B0aW9ucy5zdGFnZSB8fCB7fTtcblxuICB0aGlzLm9uZnJhbWUgPSBvcHRpb25zLm9uZnJhbWUgfHwgZnVuY3Rpb24gKCkge307IC8vIHByaXZhdGUgcHJvcGVydGllc1xuXG5cbiAgdGhpcy5fY2xpcHMgPSBbXTtcbiAgdGhpcy5fcnVubmluZyA9IGZhbHNlO1xuICB0aGlzLl90aW1lO1xuICB0aGlzLl9wYXVzZWRUaW1lO1xuICB0aGlzLl9wYXVzZVN0YXJ0O1xuICB0aGlzLl9wYXVzZWQgPSBmYWxzZTtcbiAgRGlzcGF0Y2hlci5jYWxsKHRoaXMpO1xufTtcblxuQW5pbWF0aW9uLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IEFuaW1hdGlvbixcblxuICAvKipcbiAgICog5re75YqgIGNsaXBcbiAgICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9hbmltYXRpb24vQ2xpcH0gY2xpcFxuICAgKi9cbiAgYWRkQ2xpcDogZnVuY3Rpb24gKGNsaXApIHtcbiAgICB0aGlzLl9jbGlwcy5wdXNoKGNsaXApO1xuICB9LFxuXG4gIC8qKlxuICAgKiDmt7vliqAgYW5pbWF0b3JcbiAgICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9hbmltYXRpb24vQW5pbWF0b3J9IGFuaW1hdG9yXG4gICAqL1xuICBhZGRBbmltYXRvcjogZnVuY3Rpb24gKGFuaW1hdG9yKSB7XG4gICAgYW5pbWF0b3IuYW5pbWF0aW9uID0gdGhpcztcbiAgICB2YXIgY2xpcHMgPSBhbmltYXRvci5nZXRDbGlwcygpO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjbGlwcy5sZW5ndGg7IGkrKykge1xuICAgICAgdGhpcy5hZGRDbGlwKGNsaXBzW2ldKTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIOWIoOmZpOWKqOeUu+eJh+autVxuICAgKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL2FuaW1hdGlvbi9DbGlwfSBjbGlwXG4gICAqL1xuICByZW1vdmVDbGlwOiBmdW5jdGlvbiAoY2xpcCkge1xuICAgIHZhciBpZHggPSB1dGlsLmluZGV4T2YodGhpcy5fY2xpcHMsIGNsaXApO1xuXG4gICAgaWYgKGlkeCA+PSAwKSB7XG4gICAgICB0aGlzLl9jbGlwcy5zcGxpY2UoaWR4LCAxKTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIOWIoOmZpOWKqOeUu+eJh+autVxuICAgKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL2FuaW1hdGlvbi9BbmltYXRvcn0gYW5pbWF0b3JcbiAgICovXG4gIHJlbW92ZUFuaW1hdG9yOiBmdW5jdGlvbiAoYW5pbWF0b3IpIHtcbiAgICB2YXIgY2xpcHMgPSBhbmltYXRvci5nZXRDbGlwcygpO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjbGlwcy5sZW5ndGg7IGkrKykge1xuICAgICAgdGhpcy5yZW1vdmVDbGlwKGNsaXBzW2ldKTtcbiAgICB9XG5cbiAgICBhbmltYXRvci5hbmltYXRpb24gPSBudWxsO1xuICB9LFxuICBfdXBkYXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHRpbWUgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKSAtIHRoaXMuX3BhdXNlZFRpbWU7XG5cbiAgICB2YXIgZGVsdGEgPSB0aW1lIC0gdGhpcy5fdGltZTtcbiAgICB2YXIgY2xpcHMgPSB0aGlzLl9jbGlwcztcbiAgICB2YXIgbGVuID0gY2xpcHMubGVuZ3RoO1xuICAgIHZhciBkZWZlcnJlZEV2ZW50cyA9IFtdO1xuICAgIHZhciBkZWZlcnJlZENsaXBzID0gW107XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICB2YXIgY2xpcCA9IGNsaXBzW2ldO1xuICAgICAgdmFyIGUgPSBjbGlwLnN0ZXAodGltZSwgZGVsdGEpOyAvLyBUaHJvdyBvdXQgdGhlIGV2ZW50cyBuZWVkIHRvIGJlIGNhbGxlZCBhZnRlclxuICAgICAgLy8gc3RhZ2UudXBkYXRlLCBsaWtlIGRlc3Ryb3lcblxuICAgICAgaWYgKGUpIHtcbiAgICAgICAgZGVmZXJyZWRFdmVudHMucHVzaChlKTtcbiAgICAgICAgZGVmZXJyZWRDbGlwcy5wdXNoKGNsaXApO1xuICAgICAgfVxuICAgIH0gLy8gUmVtb3ZlIHRoZSBmaW5pc2hlZCBjbGlwXG5cblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOykge1xuICAgICAgaWYgKGNsaXBzW2ldLl9uZWVkc1JlbW92ZSkge1xuICAgICAgICBjbGlwc1tpXSA9IGNsaXBzW2xlbiAtIDFdO1xuICAgICAgICBjbGlwcy5wb3AoKTtcbiAgICAgICAgbGVuLS07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpKys7XG4gICAgICB9XG4gICAgfVxuXG4gICAgbGVuID0gZGVmZXJyZWRFdmVudHMubGVuZ3RoO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgICAgZGVmZXJyZWRDbGlwc1tpXS5maXJlKGRlZmVycmVkRXZlbnRzW2ldKTtcbiAgICB9XG5cbiAgICB0aGlzLl90aW1lID0gdGltZTtcbiAgICB0aGlzLm9uZnJhbWUoZGVsdGEpOyAvLyAnZnJhbWUnIHNob3VsZCBiZSB0cmlnZ2VyZWQgYmVmb3JlIHN0YWdlLCBiZWNhdXNlIHVwcGVyIGFwcGxpY2F0aW9uXG4gICAgLy8gZGVwZW5kcyBvbiB0aGUgc2VxdWVuY2UgKGUuZy4sIGVjaGFydHMtc3RyZWFtIGFuZCBmaW5pc2hcbiAgICAvLyBldmVudCBqdWRnZSlcblxuICAgIHRoaXMudHJpZ2dlcignZnJhbWUnLCBkZWx0YSk7XG5cbiAgICBpZiAodGhpcy5zdGFnZS51cGRhdGUpIHtcbiAgICAgIHRoaXMuc3RhZ2UudXBkYXRlKCk7XG4gICAgfVxuICB9LFxuICBfc3RhcnRMb29wOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHRoaXMuX3J1bm5pbmcgPSB0cnVlO1xuXG4gICAgZnVuY3Rpb24gc3RlcCgpIHtcbiAgICAgIGlmIChzZWxmLl9ydW5uaW5nKSB7XG4gICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShzdGVwKTtcbiAgICAgICAgIXNlbGYuX3BhdXNlZCAmJiBzZWxmLl91cGRhdGUoKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoc3RlcCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFN0YXJ0IGFuaW1hdGlvbi5cbiAgICovXG4gIHN0YXJ0OiBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5fdGltZSA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgIHRoaXMuX3BhdXNlZFRpbWUgPSAwO1xuXG4gICAgdGhpcy5fc3RhcnRMb29wKCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFN0b3AgYW5pbWF0aW9uLlxuICAgKi9cbiAgc3RvcDogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX3J1bm5pbmcgPSBmYWxzZTtcbiAgfSxcblxuICAvKipcbiAgICogUGF1c2UgYW5pbWF0aW9uLlxuICAgKi9cbiAgcGF1c2U6IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoIXRoaXMuX3BhdXNlZCkge1xuICAgICAgdGhpcy5fcGF1c2VTdGFydCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgdGhpcy5fcGF1c2VkID0gdHJ1ZTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIFJlc3VtZSBhbmltYXRpb24uXG4gICAqL1xuICByZXN1bWU6IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAodGhpcy5fcGF1c2VkKSB7XG4gICAgICB0aGlzLl9wYXVzZWRUaW1lICs9IG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gdGhpcy5fcGF1c2VTdGFydDtcbiAgICAgIHRoaXMuX3BhdXNlZCA9IGZhbHNlO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQ2xlYXIgYW5pbWF0aW9uLlxuICAgKi9cbiAgY2xlYXI6IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLl9jbGlwcyA9IFtdO1xuICB9LFxuXG4gIC8qKlxuICAgKiBXaGV0aGVyIGFuaW1hdGlvbiBmaW5pc2hlZC5cbiAgICovXG4gIGlzRmluaXNoZWQ6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gIXRoaXMuX2NsaXBzLmxlbmd0aDtcbiAgfSxcblxuICAvKipcbiAgICogQ3JlYXQgYW5pbWF0b3IgZm9yIGEgdGFyZ2V0LCB3aG9zZSBwcm9wcyBjYW4gYmUgYW5pbWF0ZWQuXG4gICAqXG4gICAqIEBwYXJhbSAge09iamVjdH0gdGFyZ2V0XG4gICAqIEBwYXJhbSAge09iamVjdH0gb3B0aW9uc1xuICAgKiBAcGFyYW0gIHtib29sZWFufSBbb3B0aW9ucy5sb29wPWZhbHNlXSBXaGV0aGVyIGxvb3AgYW5pbWF0aW9uLlxuICAgKiBAcGFyYW0gIHtGdW5jdGlvbn0gW29wdGlvbnMuZ2V0dGVyPW51bGxdIEdldCB2YWx1ZSBmcm9tIHRhcmdldC5cbiAgICogQHBhcmFtICB7RnVuY3Rpb259IFtvcHRpb25zLnNldHRlcj1udWxsXSBTZXQgdmFsdWUgdG8gdGFyZ2V0LlxuICAgKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9hbmltYXRpb24vQW5pbWF0aW9ufkFuaW1hdG9yfVxuICAgKi9cbiAgLy8gVE9ETyBHYXBcbiAgYW5pbWF0ZTogZnVuY3Rpb24gKHRhcmdldCwgb3B0aW9ucykge1xuICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICAgIHZhciBhbmltYXRvciA9IG5ldyBBbmltYXRvcih0YXJnZXQsIG9wdGlvbnMubG9vcCwgb3B0aW9ucy5nZXR0ZXIsIG9wdGlvbnMuc2V0dGVyKTtcbiAgICB0aGlzLmFkZEFuaW1hdG9yKGFuaW1hdG9yKTtcbiAgICByZXR1cm4gYW5pbWF0b3I7XG4gIH1cbn07XG51dGlsLm1peGluKEFuaW1hdGlvbiwgRGlzcGF0Y2hlcik7XG52YXIgX2RlZmF1bHQgPSBBbmltYXRpb247XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbExBO0FBb0xBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/animation/Animation.js
