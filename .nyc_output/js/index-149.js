__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var rc_resize_observer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-resize-observer */ "./node_modules/rc-resize-observer/es/index.js");
/* harmony import */ var rc_resize_observer__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rc_resize_observer__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_throttleByAnimationFrame__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/throttleByAnimationFrame */ "./node_modules/antd/es/_util/throttleByAnimationFrame.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./utils */ "./node_modules/antd/es/affix/utils.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};











function getDefaultTarget() {
  return typeof window !== 'undefined' ? window : null;
}

var AffixStatus;

(function (AffixStatus) {
  AffixStatus[AffixStatus["None"] = 0] = "None";
  AffixStatus[AffixStatus["Prepare"] = 1] = "Prepare";
})(AffixStatus || (AffixStatus = {}));

var Affix =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Affix, _React$Component);

  function Affix() {
    var _this;

    _classCallCheck(this, Affix);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Affix).apply(this, arguments));
    _this.state = {
      status: AffixStatus.None,
      lastAffix: false,
      prevTarget: null
    };

    _this.getOffsetTop = function () {
      var _this$props = _this.props,
          offset = _this$props.offset,
          offsetBottom = _this$props.offsetBottom;
      var offsetTop = _this.props.offsetTop;

      if (typeof offsetTop === 'undefined') {
        offsetTop = offset;
        Object(_util_warning__WEBPACK_IMPORTED_MODULE_7__["default"])(typeof offset === 'undefined', 'Affix', '`offset` is deprecated. Please use `offsetTop` instead.');
      }

      if (offsetBottom === undefined && offsetTop === undefined) {
        offsetTop = 0;
      }

      return offsetTop;
    };

    _this.getOffsetBottom = function () {
      return _this.props.offsetBottom;
    };

    _this.savePlaceholderNode = function (node) {
      _this.placeholderNode = node;
    };

    _this.saveFixedNode = function (node) {
      _this.fixedNode = node;
    }; // =================== Measure ===================


    _this.measure = function () {
      var _this$state = _this.state,
          status = _this$state.status,
          lastAffix = _this$state.lastAffix;
      var _this$props2 = _this.props,
          target = _this$props2.target,
          onChange = _this$props2.onChange;

      if (status !== AffixStatus.Prepare || !_this.fixedNode || !_this.placeholderNode || !target) {
        return;
      }

      var offsetTop = _this.getOffsetTop();

      var offsetBottom = _this.getOffsetBottom();

      var targetNode = target();

      if (!targetNode) {
        return;
      }

      var newState = {
        status: AffixStatus.None
      };
      var targetRect = Object(_utils__WEBPACK_IMPORTED_MODULE_8__["getTargetRect"])(targetNode);
      var placeholderReact = Object(_utils__WEBPACK_IMPORTED_MODULE_8__["getTargetRect"])(_this.placeholderNode);
      var fixedTop = Object(_utils__WEBPACK_IMPORTED_MODULE_8__["getFixedTop"])(placeholderReact, targetRect, offsetTop);
      var fixedBottom = Object(_utils__WEBPACK_IMPORTED_MODULE_8__["getFixedBottom"])(placeholderReact, targetRect, offsetBottom);

      if (fixedTop !== undefined) {
        newState.affixStyle = {
          position: 'fixed',
          top: fixedTop,
          width: placeholderReact.width,
          height: placeholderReact.height
        };
        newState.placeholderStyle = {
          width: placeholderReact.width,
          height: placeholderReact.height
        };
      } else if (fixedBottom !== undefined) {
        newState.affixStyle = {
          position: 'fixed',
          bottom: fixedBottom,
          width: placeholderReact.width,
          height: placeholderReact.height
        };
        newState.placeholderStyle = {
          width: placeholderReact.width,
          height: placeholderReact.height
        };
      }

      newState.lastAffix = !!newState.affixStyle;

      if (onChange && lastAffix !== newState.lastAffix) {
        onChange(newState.lastAffix);
      }

      _this.setState(newState);
    }; // @ts-ignore TS6133


    _this.prepareMeasure = function () {
      // event param is used before. Keep compatible ts define here.
      _this.setState({
        status: AffixStatus.Prepare,
        affixStyle: undefined,
        placeholderStyle: undefined
      }); // Test if `updatePosition` called


      if (false) { var onTestUpdatePosition; }
    }; // =================== Render ===================


    _this.renderAffix = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;
      var _this$state2 = _this.state,
          affixStyle = _this$state2.affixStyle,
          placeholderStyle = _this$state2.placeholderStyle;
      var _this$props3 = _this.props,
          prefixCls = _this$props3.prefixCls,
          children = _this$props3.children;
      var className = classnames__WEBPACK_IMPORTED_MODULE_2___default()(_defineProperty({}, getPrefixCls('affix', prefixCls), affixStyle));
      var props = Object(omit_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_this.props, ['prefixCls', 'offsetTop', 'offsetBottom', 'target', 'onChange']); // Omit this since `onTestUpdatePosition` only works on test.

      if (false) {}

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_resize_observer__WEBPACK_IMPORTED_MODULE_4___default.a, {
        onResize: function onResize() {
          _this.updatePosition();
        }
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({}, props, {
        ref: _this.savePlaceholderNode
      }), affixStyle && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        style: placeholderStyle,
        "aria-hidden": "true"
      }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: className,
        ref: _this.saveFixedNode,
        style: affixStyle
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_resize_observer__WEBPACK_IMPORTED_MODULE_4___default.a, {
        onResize: function onResize() {
          _this.updatePosition();
        }
      }, children))));
    };

    return _this;
  } // Event handler


  _createClass(Affix, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var target = this.props.target;

      if (target) {
        // [Legacy] Wait for parent component ref has its value.
        // We should use target as directly element instead of function which makes element check hard.
        this.timeout = setTimeout(function () {
          Object(_utils__WEBPACK_IMPORTED_MODULE_8__["addObserveTarget"])(target(), _this2); // Mock Event object.

          _this2.updatePosition();
        });
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var prevTarget = this.state.prevTarget;
      var target = this.props.target;
      var newTarget = null;

      if (target) {
        newTarget = target() || null;
      }

      if (prevTarget !== newTarget) {
        Object(_utils__WEBPACK_IMPORTED_MODULE_8__["removeObserveTarget"])(this);

        if (newTarget) {
          Object(_utils__WEBPACK_IMPORTED_MODULE_8__["addObserveTarget"])(newTarget, this); // Mock Event object.

          this.updatePosition();
        }

        this.setState({
          prevTarget: newTarget
        });
      }

      if (prevProps.offsetTop !== this.props.offsetTop || prevProps.offsetBottom !== this.props.offsetBottom) {
        this.updatePosition();
      }

      this.measure();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearTimeout(this.timeout);
      Object(_utils__WEBPACK_IMPORTED_MODULE_8__["removeObserveTarget"])(this);
      this.updatePosition.cancel();
    } // Handle realign logic

  }, {
    key: "updatePosition",
    value: function updatePosition() {
      this.prepareMeasure();
    }
  }, {
    key: "lazyUpdatePosition",
    value: function lazyUpdatePosition() {
      var target = this.props.target;
      var affixStyle = this.state.affixStyle; // Check position change before measure to make Safari smooth

      if (target && affixStyle) {
        var offsetTop = this.getOffsetTop();
        var offsetBottom = this.getOffsetBottom();
        var targetNode = target();

        if (targetNode) {
          var targetRect = Object(_utils__WEBPACK_IMPORTED_MODULE_8__["getTargetRect"])(targetNode);
          var placeholderReact = Object(_utils__WEBPACK_IMPORTED_MODULE_8__["getTargetRect"])(this.placeholderNode);
          var fixedTop = Object(_utils__WEBPACK_IMPORTED_MODULE_8__["getFixedTop"])(placeholderReact, targetRect, offsetTop);
          var fixedBottom = Object(_utils__WEBPACK_IMPORTED_MODULE_8__["getFixedBottom"])(placeholderReact, targetRect, offsetBottom);

          if (fixedTop !== undefined && affixStyle.top === fixedTop || fixedBottom !== undefined && affixStyle.bottom === fixedBottom) {
            return;
          }
        }
      } // Directly call prepare measure since it's already throttled.


      this.prepareMeasure();
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, this.renderAffix);
    }
  }]);

  return Affix;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Affix.defaultProps = {
  target: getDefaultTarget
};

__decorate([Object(_util_throttleByAnimationFrame__WEBPACK_IMPORTED_MODULE_6__["throttleByAnimationFrameDecorator"])()], Affix.prototype, "updatePosition", null);

__decorate([Object(_util_throttleByAnimationFrame__WEBPACK_IMPORTED_MODULE_6__["throttleByAnimationFrameDecorator"])()], Affix.prototype, "lazyUpdatePosition", null);

Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_1__["polyfill"])(Affix);
/* harmony default export */ __webpack_exports__["default"] = (Affix);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9hZmZpeC9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvYWZmaXgvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbInZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xufTtcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCBSZXNpemVPYnNlcnZlciBmcm9tICdyYy1yZXNpemUtb2JzZXJ2ZXInO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IHsgdGhyb3R0bGVCeUFuaW1hdGlvbkZyYW1lRGVjb3JhdG9yIH0gZnJvbSAnLi4vX3V0aWwvdGhyb3R0bGVCeUFuaW1hdGlvbkZyYW1lJztcbmltcG9ydCB3YXJuaW5nIGZyb20gJy4uL191dGlsL3dhcm5pbmcnO1xuaW1wb3J0IHsgYWRkT2JzZXJ2ZVRhcmdldCwgcmVtb3ZlT2JzZXJ2ZVRhcmdldCwgZ2V0VGFyZ2V0UmVjdCwgZ2V0Rml4ZWRUb3AsIGdldEZpeGVkQm90dG9tLCB9IGZyb20gJy4vdXRpbHMnO1xuZnVuY3Rpb24gZ2V0RGVmYXVsdFRhcmdldCgpIHtcbiAgICByZXR1cm4gdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgPyB3aW5kb3cgOiBudWxsO1xufVxudmFyIEFmZml4U3RhdHVzO1xuKGZ1bmN0aW9uIChBZmZpeFN0YXR1cykge1xuICAgIEFmZml4U3RhdHVzW0FmZml4U3RhdHVzW1wiTm9uZVwiXSA9IDBdID0gXCJOb25lXCI7XG4gICAgQWZmaXhTdGF0dXNbQWZmaXhTdGF0dXNbXCJQcmVwYXJlXCJdID0gMV0gPSBcIlByZXBhcmVcIjtcbn0pKEFmZml4U3RhdHVzIHx8IChBZmZpeFN0YXR1cyA9IHt9KSk7XG5jbGFzcyBBZmZpeCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICBzdGF0dXM6IEFmZml4U3RhdHVzLk5vbmUsXG4gICAgICAgICAgICBsYXN0QWZmaXg6IGZhbHNlLFxuICAgICAgICAgICAgcHJldlRhcmdldDogbnVsbCxcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5nZXRPZmZzZXRUb3AgPSAoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IG9mZnNldCwgb2Zmc2V0Qm90dG9tIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgbGV0IHsgb2Zmc2V0VG9wIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBvZmZzZXRUb3AgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgb2Zmc2V0VG9wID0gb2Zmc2V0O1xuICAgICAgICAgICAgICAgIHdhcm5pbmcodHlwZW9mIG9mZnNldCA9PT0gJ3VuZGVmaW5lZCcsICdBZmZpeCcsICdgb2Zmc2V0YCBpcyBkZXByZWNhdGVkLiBQbGVhc2UgdXNlIGBvZmZzZXRUb3BgIGluc3RlYWQuJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAob2Zmc2V0Qm90dG9tID09PSB1bmRlZmluZWQgJiYgb2Zmc2V0VG9wID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBvZmZzZXRUb3AgPSAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG9mZnNldFRvcDtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5nZXRPZmZzZXRCb3R0b20gPSAoKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5vZmZzZXRCb3R0b207XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc2F2ZVBsYWNlaG9sZGVyTm9kZSA9IChub2RlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnBsYWNlaG9sZGVyTm9kZSA9IG5vZGU7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc2F2ZUZpeGVkTm9kZSA9IChub2RlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmZpeGVkTm9kZSA9IG5vZGU7XG4gICAgICAgIH07XG4gICAgICAgIC8vID09PT09PT09PT09PT09PT09PT0gTWVhc3VyZSA9PT09PT09PT09PT09PT09PT09XG4gICAgICAgIHRoaXMubWVhc3VyZSA9ICgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgc3RhdHVzLCBsYXN0QWZmaXggfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICBjb25zdCB7IHRhcmdldCwgb25DaGFuZ2UgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAoc3RhdHVzICE9PSBBZmZpeFN0YXR1cy5QcmVwYXJlIHx8ICF0aGlzLmZpeGVkTm9kZSB8fCAhdGhpcy5wbGFjZWhvbGRlck5vZGUgfHwgIXRhcmdldCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IG9mZnNldFRvcCA9IHRoaXMuZ2V0T2Zmc2V0VG9wKCk7XG4gICAgICAgICAgICBjb25zdCBvZmZzZXRCb3R0b20gPSB0aGlzLmdldE9mZnNldEJvdHRvbSgpO1xuICAgICAgICAgICAgY29uc3QgdGFyZ2V0Tm9kZSA9IHRhcmdldCgpO1xuICAgICAgICAgICAgaWYgKCF0YXJnZXROb2RlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgbmV3U3RhdGUgPSB7XG4gICAgICAgICAgICAgICAgc3RhdHVzOiBBZmZpeFN0YXR1cy5Ob25lLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGNvbnN0IHRhcmdldFJlY3QgPSBnZXRUYXJnZXRSZWN0KHRhcmdldE5vZGUpO1xuICAgICAgICAgICAgY29uc3QgcGxhY2Vob2xkZXJSZWFjdCA9IGdldFRhcmdldFJlY3QodGhpcy5wbGFjZWhvbGRlck5vZGUpO1xuICAgICAgICAgICAgY29uc3QgZml4ZWRUb3AgPSBnZXRGaXhlZFRvcChwbGFjZWhvbGRlclJlYWN0LCB0YXJnZXRSZWN0LCBvZmZzZXRUb3ApO1xuICAgICAgICAgICAgY29uc3QgZml4ZWRCb3R0b20gPSBnZXRGaXhlZEJvdHRvbShwbGFjZWhvbGRlclJlYWN0LCB0YXJnZXRSZWN0LCBvZmZzZXRCb3R0b20pO1xuICAgICAgICAgICAgaWYgKGZpeGVkVG9wICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBuZXdTdGF0ZS5hZmZpeFN0eWxlID0ge1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ2ZpeGVkJyxcbiAgICAgICAgICAgICAgICAgICAgdG9wOiBmaXhlZFRvcCxcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IHBsYWNlaG9sZGVyUmVhY3Qud2lkdGgsXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogcGxhY2Vob2xkZXJSZWFjdC5oZWlnaHQsXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBuZXdTdGF0ZS5wbGFjZWhvbGRlclN0eWxlID0ge1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogcGxhY2Vob2xkZXJSZWFjdC53aWR0aCxcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBwbGFjZWhvbGRlclJlYWN0LmhlaWdodCxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAoZml4ZWRCb3R0b20gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIG5ld1N0YXRlLmFmZml4U3R5bGUgPSB7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAnZml4ZWQnLFxuICAgICAgICAgICAgICAgICAgICBib3R0b206IGZpeGVkQm90dG9tLFxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogcGxhY2Vob2xkZXJSZWFjdC53aWR0aCxcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBwbGFjZWhvbGRlclJlYWN0LmhlaWdodCxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIG5ld1N0YXRlLnBsYWNlaG9sZGVyU3R5bGUgPSB7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiBwbGFjZWhvbGRlclJlYWN0LndpZHRoLFxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IHBsYWNlaG9sZGVyUmVhY3QuaGVpZ2h0LFxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBuZXdTdGF0ZS5sYXN0QWZmaXggPSAhIW5ld1N0YXRlLmFmZml4U3R5bGU7XG4gICAgICAgICAgICBpZiAob25DaGFuZ2UgJiYgbGFzdEFmZml4ICE9PSBuZXdTdGF0ZS5sYXN0QWZmaXgpIHtcbiAgICAgICAgICAgICAgICBvbkNoYW5nZShuZXdTdGF0ZS5sYXN0QWZmaXgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZShuZXdTdGF0ZSk7XG4gICAgICAgIH07XG4gICAgICAgIC8vIEB0cy1pZ25vcmUgVFM2MTMzXG4gICAgICAgIHRoaXMucHJlcGFyZU1lYXN1cmUgPSAoKSA9PiB7XG4gICAgICAgICAgICAvLyBldmVudCBwYXJhbSBpcyB1c2VkIGJlZm9yZS4gS2VlcCBjb21wYXRpYmxlIHRzIGRlZmluZSBoZXJlLlxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgc3RhdHVzOiBBZmZpeFN0YXR1cy5QcmVwYXJlLFxuICAgICAgICAgICAgICAgIGFmZml4U3R5bGU6IHVuZGVmaW5lZCxcbiAgICAgICAgICAgICAgICBwbGFjZWhvbGRlclN0eWxlOiB1bmRlZmluZWQsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIC8vIFRlc3QgaWYgYHVwZGF0ZVBvc2l0aW9uYCBjYWxsZWRcbiAgICAgICAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ3Rlc3QnKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgeyBvblRlc3RVcGRhdGVQb3NpdGlvbiB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgICAgICBpZiAob25UZXN0VXBkYXRlUG9zaXRpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgb25UZXN0VXBkYXRlUG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIC8vID09PT09PT09PT09PT09PT09PT0gUmVuZGVyID09PT09PT09PT09PT09PT09PT1cbiAgICAgICAgdGhpcy5yZW5kZXJBZmZpeCA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGFmZml4U3R5bGUsIHBsYWNlaG9sZGVyU3R5bGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENscywgY2hpbGRyZW4gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCBjbGFzc05hbWUgPSBjbGFzc05hbWVzKHtcbiAgICAgICAgICAgICAgICBbZ2V0UHJlZml4Q2xzKCdhZmZpeCcsIHByZWZpeENscyldOiBhZmZpeFN0eWxlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBsZXQgcHJvcHMgPSBvbWl0KHRoaXMucHJvcHMsIFsncHJlZml4Q2xzJywgJ29mZnNldFRvcCcsICdvZmZzZXRCb3R0b20nLCAndGFyZ2V0JywgJ29uQ2hhbmdlJ10pO1xuICAgICAgICAgICAgLy8gT21pdCB0aGlzIHNpbmNlIGBvblRlc3RVcGRhdGVQb3NpdGlvbmAgb25seSB3b3JrcyBvbiB0ZXN0LlxuICAgICAgICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAndGVzdCcpIHtcbiAgICAgICAgICAgICAgICBwcm9wcyA9IG9taXQocHJvcHMsIFsnb25UZXN0VXBkYXRlUG9zaXRpb24nXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gKDxSZXNpemVPYnNlcnZlciBvblJlc2l6ZT17KCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUG9zaXRpb24oKTtcbiAgICAgICAgICAgIH19PlxuICAgICAgICA8ZGl2IHsuLi5wcm9wc30gcmVmPXt0aGlzLnNhdmVQbGFjZWhvbGRlck5vZGV9PlxuICAgICAgICAgIHthZmZpeFN0eWxlICYmIDxkaXYgc3R5bGU9e3BsYWNlaG9sZGVyU3R5bGV9IGFyaWEtaGlkZGVuPVwidHJ1ZVwiLz59XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzTmFtZX0gcmVmPXt0aGlzLnNhdmVGaXhlZE5vZGV9IHN0eWxlPXthZmZpeFN0eWxlfT5cbiAgICAgICAgICAgIDxSZXNpemVPYnNlcnZlciBvblJlc2l6ZT17KCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUG9zaXRpb24oKTtcbiAgICAgICAgICAgIH19PlxuICAgICAgICAgICAgICB7Y2hpbGRyZW59XG4gICAgICAgICAgICA8L1Jlc2l6ZU9ic2VydmVyPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvUmVzaXplT2JzZXJ2ZXI+KTtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgLy8gRXZlbnQgaGFuZGxlclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgICBjb25zdCB7IHRhcmdldCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKHRhcmdldCkge1xuICAgICAgICAgICAgLy8gW0xlZ2FjeV0gV2FpdCBmb3IgcGFyZW50IGNvbXBvbmVudCByZWYgaGFzIGl0cyB2YWx1ZS5cbiAgICAgICAgICAgIC8vIFdlIHNob3VsZCB1c2UgdGFyZ2V0IGFzIGRpcmVjdGx5IGVsZW1lbnQgaW5zdGVhZCBvZiBmdW5jdGlvbiB3aGljaCBtYWtlcyBlbGVtZW50IGNoZWNrIGhhcmQuXG4gICAgICAgICAgICB0aGlzLnRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICBhZGRPYnNlcnZlVGFyZ2V0KHRhcmdldCgpLCB0aGlzKTtcbiAgICAgICAgICAgICAgICAvLyBNb2NrIEV2ZW50IG9iamVjdC5cbiAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVBvc2l0aW9uKCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzKSB7XG4gICAgICAgIGNvbnN0IHsgcHJldlRhcmdldCB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgY29uc3QgeyB0YXJnZXQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGxldCBuZXdUYXJnZXQgPSBudWxsO1xuICAgICAgICBpZiAodGFyZ2V0KSB7XG4gICAgICAgICAgICBuZXdUYXJnZXQgPSB0YXJnZXQoKSB8fCBudWxsO1xuICAgICAgICB9XG4gICAgICAgIGlmIChwcmV2VGFyZ2V0ICE9PSBuZXdUYXJnZXQpIHtcbiAgICAgICAgICAgIHJlbW92ZU9ic2VydmVUYXJnZXQodGhpcyk7XG4gICAgICAgICAgICBpZiAobmV3VGFyZ2V0KSB7XG4gICAgICAgICAgICAgICAgYWRkT2JzZXJ2ZVRhcmdldChuZXdUYXJnZXQsIHRoaXMpO1xuICAgICAgICAgICAgICAgIC8vIE1vY2sgRXZlbnQgb2JqZWN0LlxuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUG9zaXRpb24oKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBwcmV2VGFyZ2V0OiBuZXdUYXJnZXQgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHByZXZQcm9wcy5vZmZzZXRUb3AgIT09IHRoaXMucHJvcHMub2Zmc2V0VG9wIHx8XG4gICAgICAgICAgICBwcmV2UHJvcHMub2Zmc2V0Qm90dG9tICE9PSB0aGlzLnByb3BzLm9mZnNldEJvdHRvbSkge1xuICAgICAgICAgICAgdGhpcy51cGRhdGVQb3NpdGlvbigpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubWVhc3VyZSgpO1xuICAgIH1cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMudGltZW91dCk7XG4gICAgICAgIHJlbW92ZU9ic2VydmVUYXJnZXQodGhpcyk7XG4gICAgICAgIHRoaXMudXBkYXRlUG9zaXRpb24uY2FuY2VsKCk7XG4gICAgfVxuICAgIC8vIEhhbmRsZSByZWFsaWduIGxvZ2ljXG4gICAgdXBkYXRlUG9zaXRpb24oKSB7XG4gICAgICAgIHRoaXMucHJlcGFyZU1lYXN1cmUoKTtcbiAgICB9XG4gICAgbGF6eVVwZGF0ZVBvc2l0aW9uKCkge1xuICAgICAgICBjb25zdCB7IHRhcmdldCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgY29uc3QgeyBhZmZpeFN0eWxlIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAvLyBDaGVjayBwb3NpdGlvbiBjaGFuZ2UgYmVmb3JlIG1lYXN1cmUgdG8gbWFrZSBTYWZhcmkgc21vb3RoXG4gICAgICAgIGlmICh0YXJnZXQgJiYgYWZmaXhTdHlsZSkge1xuICAgICAgICAgICAgY29uc3Qgb2Zmc2V0VG9wID0gdGhpcy5nZXRPZmZzZXRUb3AoKTtcbiAgICAgICAgICAgIGNvbnN0IG9mZnNldEJvdHRvbSA9IHRoaXMuZ2V0T2Zmc2V0Qm90dG9tKCk7XG4gICAgICAgICAgICBjb25zdCB0YXJnZXROb2RlID0gdGFyZ2V0KCk7XG4gICAgICAgICAgICBpZiAodGFyZ2V0Tm9kZSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IHRhcmdldFJlY3QgPSBnZXRUYXJnZXRSZWN0KHRhcmdldE5vZGUpO1xuICAgICAgICAgICAgICAgIGNvbnN0IHBsYWNlaG9sZGVyUmVhY3QgPSBnZXRUYXJnZXRSZWN0KHRoaXMucGxhY2Vob2xkZXJOb2RlKTtcbiAgICAgICAgICAgICAgICBjb25zdCBmaXhlZFRvcCA9IGdldEZpeGVkVG9wKHBsYWNlaG9sZGVyUmVhY3QsIHRhcmdldFJlY3QsIG9mZnNldFRvcCk7XG4gICAgICAgICAgICAgICAgY29uc3QgZml4ZWRCb3R0b20gPSBnZXRGaXhlZEJvdHRvbShwbGFjZWhvbGRlclJlYWN0LCB0YXJnZXRSZWN0LCBvZmZzZXRCb3R0b20pO1xuICAgICAgICAgICAgICAgIGlmICgoZml4ZWRUb3AgIT09IHVuZGVmaW5lZCAmJiBhZmZpeFN0eWxlLnRvcCA9PT0gZml4ZWRUb3ApIHx8XG4gICAgICAgICAgICAgICAgICAgIChmaXhlZEJvdHRvbSAhPT0gdW5kZWZpbmVkICYmIGFmZml4U3R5bGUuYm90dG9tID09PSBmaXhlZEJvdHRvbSkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAvLyBEaXJlY3RseSBjYWxsIHByZXBhcmUgbWVhc3VyZSBzaW5jZSBpdCdzIGFscmVhZHkgdGhyb3R0bGVkLlxuICAgICAgICB0aGlzLnByZXBhcmVNZWFzdXJlKCk7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJBZmZpeH08L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG5BZmZpeC5kZWZhdWx0UHJvcHMgPSB7XG4gICAgdGFyZ2V0OiBnZXREZWZhdWx0VGFyZ2V0LFxufTtcbl9fZGVjb3JhdGUoW1xuICAgIHRocm90dGxlQnlBbmltYXRpb25GcmFtZURlY29yYXRvcigpXG5dLCBBZmZpeC5wcm90b3R5cGUsIFwidXBkYXRlUG9zaXRpb25cIiwgbnVsbCk7XG5fX2RlY29yYXRlKFtcbiAgICB0aHJvdHRsZUJ5QW5pbWF0aW9uRnJhbWVEZWNvcmF0b3IoKVxuXSwgQWZmaXgucHJvdG90eXBlLCBcImxhenlVcGRhdGVQb3NpdGlvblwiLCBudWxsKTtcbnBvbHlmaWxsKEFmZml4KTtcbmV4cG9ydCBkZWZhdWx0IEFmZml4O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBSkE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUdBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBVkE7QUFDQTtBQVdBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBMUJBO0FBQ0E7QUFDQTtBQTJCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUZBO0FBUEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQTVFQTtBQUNBO0FBQ0E7QUE2RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUNBO0FBSUEsOENBS0E7QUE1RkE7QUFDQTtBQUNBO0FBNkZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUdBO0FBQ0E7QUFDQSxtQkFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUdBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBakJBO0FBQ0E7QUFoR0E7QUEwSEE7QUFDQTtBQUNBOzs7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7OztBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTs7O0FBQUE7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQWpCQTtBQUNBO0FBQ0E7QUFrQkE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7O0FBL0xBO0FBQ0E7QUFnTUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/affix/index.js
