__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! events */ "./node_modules/events/events.js");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(events__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");




var PROPERTY_FILTER_PATTERN = /\[.+\]/;
var PROPERTY_FILTER_UNWRAPPER = /[\[\]]/g;
var SIMPLE_ID = "simple";
var INDEX_ID = "index";
var EXPRESSION_ID = "expression";
var FILTER_PATTERN = /^([\w|\.|\[|\]|\s|\/|=|!|&]+?)(={0,2}|>=?|<=?|<>|!={1,2})([\-|\w|\.|\[|\]|\s|\u4e00-\u9fff]+)$/;
var INJECTED_PROPERTY_NAMES;

(function (INJECTED_PROPERTY_NAMES) {
  INJECTED_PROPERTY_NAMES["EMITTER"] = "$$emitter";
  INJECTED_PROPERTY_NAMES["ON"] = "$$on";
  INJECTED_PROPERTY_NAMES["OFF"] = "$$off";
  INJECTED_PROPERTY_NAMES["CREATE_EVENT"] = "$$createEvent";
  INJECTED_PROPERTY_NAMES["FIRE_EVENT"] = "$$fireEvent";
  INJECTED_PROPERTY_NAMES["MODEL"] = "$$model";
  INJECTED_PROPERTY_NAMES["PROXIED"] = "$$proxied";
  INJECTED_PROPERTY_NAMES["SWITCH_MODEL"] = "$$switchModel";
})(INJECTED_PROPERTY_NAMES || (INJECTED_PROPERTY_NAMES = {}));

var Modeller =
/*#__PURE__*/
function () {
  function Modeller() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Modeller);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Modeller, null, [{
    key: "findEmittingId",
    value: function findEmittingId(id) {
      var ids = Modeller.parseId(id);
      return ids.map(function (id) {
        switch (id.type) {
          case INDEX_ID:
            return "".concat(id.id, "[").concat(id.index, "]");

          case EXPRESSION_ID:
            return id.id;

          case SIMPLE_ID:
          default:
            return id.id;
        }
      }).join(".");
    }
  }, {
    key: "asProxied",
    value: function asProxied(model) {
      if (Modeller.isProxied(model)) {
        return model;
      }

      var emitter = new events__WEBPACK_IMPORTED_MODULE_2___default.a().setMaxListeners(Infinity);

      var createEvent = function createEvent(model, prop, oldValue, newValue) {
        return {
          model: model,
          prop: prop,
          oldValue: oldValue,
          newValue: newValue
        };
      };

      var target = model;
      return new Proxy(model, {
        get: function get(obj, prop) {
          if (_common__WEBPACK_IMPORTED_MODULE_3__["Utils"].isSymbol(prop)) {
            return target[prop];
          }

          switch (prop) {
            case INJECTED_PROPERTY_NAMES.MODEL:
              return target;

            case INJECTED_PROPERTY_NAMES.PROXIED:
              return true;

            case INJECTED_PROPERTY_NAMES.EMITTER:
              return emitter;

            case INJECTED_PROPERTY_NAMES.ON:
              return function (prop, listener) {
                var id = Modeller.findEmittingId(prop);
                var exists = emitter.listeners(id);

                if (!exists.includes(listener)) {
                  emitter.on(id, listener);
                }

                return this;
              };

            case INJECTED_PROPERTY_NAMES.OFF:
              return function (prop, listener) {
                emitter.off(Modeller.findEmittingId(prop), listener);
                return this;
              };

            case INJECTED_PROPERTY_NAMES.CREATE_EVENT:
              return createEvent;

            case INJECTED_PROPERTY_NAMES.FIRE_EVENT:
              return function (prop, oldValue, newValue) {
                emitter.emit(Modeller.findEmittingId(prop), createEvent(target, prop, oldValue, newValue));
                return this;
              };

            case INJECTED_PROPERTY_NAMES.SWITCH_MODEL:
              return function (newModel) {
                if (newModel == null) {
                  throw new Error("Model to switch cannot be null.");
                }

                target = newModel;
                return newModel;
              };

            default:
              return Modeller.getValueFromModel(target, prop);
          }
        },
        set: function set(obj, prop, value) {
          var result = Modeller.setValueToModel(target, prop, value);

          if (result.changed) {
            emitter.emit(Modeller.findEmittingId(prop), createEvent(target, prop, result.oldValue, value));
          }

          return true;
        },
        deleteProperty: function deleteProperty(obj, prop) {
          if (prop in target) {
            var oldValue = target[prop];
            emitter.emit(Modeller.findEmittingId(prop), createEvent(target, prop, oldValue, undefined));
            delete target[prop];
          }

          return true;
        },
        ownKeys: function ownKeys(obj) {
          return Reflect.ownKeys(target);
        },
        has: function has(obj, prop) {
          return Reflect.has(target, prop);
        },
        getPrototypeOf: function getPrototypeOf(target) {
          return Reflect.getPrototypeOf(target);
        },
        getOwnPropertyDescriptor: function getOwnPropertyDescriptor(obj, prop) {
          if (prop.startsWith("$$")) {
            return {
              configurable: false,
              enumerable: false,
              writable: false
            };
          }

          return Reflect.getOwnPropertyDescriptor(target, prop);
        }
      });
    }
  }, {
    key: "isProxied",
    value: function isProxied(model) {
      if (model == null) {
        return false;
      } else {
        return model.$$proxied === true;
      }
    }
  }, {
    key: "on",
    value: function on(model, prop, listener) {
      if (model != null) {
        model.$$on(prop, listener);
      }
    }
  }, {
    key: "off",
    value: function off(model, prop, listener) {
      if (model != null) {
        model.$$off(prop, listener);
      }
    }
  }, {
    key: "isFromRoot",
    value: function isFromRoot(id) {
      return Boolean(id && id.startsWith("/"));
    }
  }, {
    key: "getRealId",
    value: function getRealId(id) {
      return Modeller.isFromRoot(id) ? id.substring(1) : id;
    }
  }, {
    key: "parseFilter",
    value: function parseFilter(filter) {
      var match;

      if (!filter.includes("[") && !filter.includes("]") && (filter.includes("&&") || filter.includes("||"))) {
        var top = {
          type: "and",
          expressions: []
        };
        var stack = [top];
        var currentExpression = "";
        var first = true;
        filter.split("").forEach(function (ch) {
          if (ch === "(") {
            var group = {
              type: "and",
              expressions: []
            };
            stack[stack.length - 1].expressions.push(group);
            stack.push(group);
          } else if (ch === ")") {
            if (currentExpression) {
              stack[stack.length - 1].expressions.push(Modeller.parseFilter(currentExpression));
            }

            stack.pop();
            currentExpression = "";
          } else if ((ch === "&" || ch === "|") && first) {
            if (ch === "|") {
              stack[stack.length - 1].type = "or";
            }

            first = false;

            if (currentExpression) {
              stack[stack.length - 1].expressions.push(Modeller.parseFilter(currentExpression));
            }

            currentExpression = "";
          } else if ((ch === "&" || ch === "|") && !first) {
            first = true;
          } else {
            currentExpression += ch;
          }
        });

        if (currentExpression) {
          stack[stack.length - 1].expressions.push(Modeller.parseFilter(currentExpression));
        }

        return top;
      } else if ((match = filter.match(FILTER_PATTERN)) != null) {
        return {
          prop: match[1].trim(),
          operator: match[2],
          value: match[3].trim()
        };
      } else {
        throw new Error("Cannot parse filter[".concat(filter, "]."));
      }
    }
  }, {
    key: "isSimpleExpression",
    value: function isSimpleExpression(filter) {
      return filter.type == undefined;
    }
  }, {
    key: "matchFilter",
    value: function matchFilter(model, filter) {
      if (Modeller.isSimpleExpression(filter)) {
        var value = Modeller.getValueFromModel(model, filter.prop);
        var op = filter.operator;
        return Modeller.matchValue(value, op, filter.value);
      } else if (filter.type === "and") {
        return !filter.expressions.some(function (sub) {
          return !Modeller.matchFilter(model, sub);
        });
      } else if (filter.type === "or") {
        return filter.expressions.some(function (sub) {
          return Modeller.matchFilter(model, sub);
        });
      } else {
        console.error(filter);
        throw new Error("Unsupported filter.");
      }
    }
  }, {
    key: "equals",
    value: function equals(a, b) {
      var v = b === "true" ? true : b === "false" ? false : b === "null" ? null : b === "" ? "" : b;

      if (v === true) {
        return a === true;
      } else if (v === null || v === false || v === "") {
        return a == null || a === false || a === "";
      } else {
        return a == b;
      }
    }
  }, {
    key: "forceEquals",
    value: function forceEquals(a, b) {
      if (b === "true") {
        return a === true;
      } else if (b === "false") {
        return a === false;
      } else if (b === "null") {
        return a === null || typeof a === "undefined";
      } else {
        return a === b;
      }
    }
  }, {
    key: "matchValue",
    value: function matchValue(value, op, filterValue) {
      if (op === "=" || op === "==") {
        return Modeller.equals(value, filterValue);
      } else if (op === "<>" || op === "!=") {
        return !Modeller.equals(value, filterValue);
      } else if (op === ">") {
        return value > +filterValue;
      } else if (op === "<") {
        return value < +filterValue;
      } else if (op === ">=") {
        return value >= +filterValue;
      } else if (op === "<=") {
        return value <= +filterValue;
      } else if (op === "===") {
        return Modeller.forceEquals(value, filterValue);
      } else if (op === "!==") {
        return !Modeller.forceEquals(value, filterValue);
      }

      return true;
    }
  }, {
    key: "getFromModel",
    value: function getFromModel(model, id) {
      return model ? model[id] : null;
    }
  }, {
    key: "setToModel",
    value: function setToModel(model, id, value) {
      if (model) {
        model[id] = value;
      }
    }
  }, {
    key: "getOrCreateByIds",
    value: function getOrCreateByIds(model, ids, id) {
      return ids.reduce(function (model, idPart, idPartIndex) {
        switch (idPart.type) {
          case INDEX_ID:
            var array = Modeller.getOrCreateArrayFromModel(model, idPart.id);
            var length = array.length;
            var index = idPart.index;

            if (index < 0 || index > length - 1) {
              throw new Error("Index out of bounds when get value by [".concat(id, "]"));
            } else {
              return array[index];
            }

          case EXPRESSION_ID:
            var arrayData = Modeller.getOrCreateArrayFromModel(model, idPart.id).filter(function (item) {
              return Modeller.matchFilter(item, idPart.filter);
            });

            if (idPart.exact) {
              if (arrayData.length !== 1) {
                throw new Error("Index out of bounds when get value by [".concat(id, "]"));
              } else {
                return arrayData[0];
              }
            } else {
              throw new Error("Expression[".concat(id, "] is not supported."));
            }

          case SIMPLE_ID:
          default:
            var object = Modeller.getFromModel(model, idPart.id);

            if (object == null) {
              object = {};
              Modeller.setToModel(model, idPart.id, object);
            }

            return object;
        }
      }, model);
    }
  }, {
    key: "getValueFromModel",
    value: function getValueFromModel(model, id) {
      if (!model && !id) {
        return null;
      }

      var ids = Modeller.parseId(id);
      return ids.reduce(function (model, idPart) {
        if (model) {
          switch (idPart.type) {
            case INDEX_ID:
              var array = Modeller.getOrCreateArrayFromModel(model, idPart.id);
              var length = array.length;
              var index = idPart.index;

              if (index < 0 || index > length - 1) {
                throw new Error("Index out of bounds when get value by [".concat(id, "]"));
              } else {
                return array[index];
              }

            case EXPRESSION_ID:
              var arrayData = Modeller.getOrCreateArrayFromModel(model, idPart.id).filter(function (item) {
                return Modeller.matchFilter(item, idPart.filter);
              });

              if (idPart.exact) {
                if (arrayData.length !== 1) {
                  // eslint-disable-next-line
                  throw new Error("Index out of bounds when get value by [".concat(id, "]"));
                } else {
                  return arrayData[0];
                }
              } else {
                return arrayData;
              }

            case SIMPLE_ID:
            default:
              return Modeller.getFromModel(model, idPart.id);
          }
        } else {
          return null;
        }
      }, model);
    }
  }, {
    key: "getValue",
    value: function getValue(where, id) {
      if (where.model == null) {
        return Modeller.getValue({
          model: where,
          root: where
        }, id);
      } else {
        var model = Modeller.isFromRoot(id) ? where.root : where.model;

        if (Modeller.isProxied(model)) {
          return model[Modeller.getRealId(id)];
        } else {
          return Modeller.getValueFromModel(model, Modeller.getRealId(id));
        }
      }
    }
  }, {
    key: "setValueToModel",
    value: function setValueToModel(model, id, value) {
      var force = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
      var oldValue;

      if (!force && value == (oldValue = Modeller.getValueFromModel(model, id))) {
        return {
          changed: false
        };
      }

      var ids = Modeller.parseId(id);
      var last = ids.pop();
      var object = Modeller.getOrCreateByIds(model, ids, id);
      Modeller.setToModel(object, last.id, value);
      return {
        changed: true,
        oldValue: oldValue
      };
    }
  }, {
    key: "setValue",
    value: function setValue(where, id, value) {
      var force = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

      if (where.model == null) {
        Modeller.setValue({
          model: where,
          root: where
        }, id, value, force);
      } else {
        var model = Modeller.isFromRoot(id) ? where.root : where.model;

        if (Modeller.isProxied(model)) {
          return model[Modeller.getRealId(id)] = value;
        } else {
          return Modeller.setValueToModel(model, Modeller.getRealId(id), value, force);
        }
      }
    }
  }, {
    key: "parseId",
    value: function parseId(id) {
      // const ids = id.split('.');
      var currentId = "";
      var inExpression = false;
      var ids = [];
      id.split("").forEach(function (ch) {
        if (ch !== ".") {
          currentId += ch;
        }

        if (ch === "[") {
          inExpression = true;
        } else if (ch === "]") {
          inExpression = false;
        } else if (ch === "." && inExpression) {
          currentId += ch;
        } else if (ch === "." && !inExpression) {
          ids.push(currentId);
          currentId = "";
        }
      });
      ids.push(currentId);
      return ids.map(function (id) {
        var matcher = id.match(PROPERTY_FILTER_PATTERN);

        if (matcher) {
          var filter = matcher[0].replace(PROPERTY_FILTER_UNWRAPPER, "");
          var realId = id.substring(0, matcher.index);

          if (!isNaN(+filter)) {
            return {
              id: realId,
              type: INDEX_ID,
              index: +filter
            };
          } else if (filter.startsWith("!")) {
            return {
              id: realId,
              type: EXPRESSION_ID,
              filter: Modeller.parseFilter(filter.substring(1).trim()),
              exact: true
            };
          } else {
            return {
              id: realId,
              type: EXPRESSION_ID,
              filter: Modeller.parseFilter(filter),
              exact: false
            };
          }
        } else {
          return {
            id: id,
            type: SIMPLE_ID
          };
        }
      });
    }
  }]);

  return Modeller;
}();

Modeller.getOrCreateArrayFromModel = function (model, id) {
  var array = Modeller.getFromModel(model, id);

  if (array == null) {
    array = [];
    Modeller.setToModel(model, id, array);
  }

  return array;
};

/* harmony default export */ __webpack_exports__["default"] = (Modeller);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvZGF0YS1tb2RlbC9tb2RlbC11dGlscy50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9kYXRhLW1vZGVsL21vZGVsLXV0aWxzLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBNb2RlbERhdGFDaGFuZ2VFdmVudExpc3RlbmVyLFxuICBNb2RlbERhdGFDaGFuZ2VkRXZlbnQsXG4gIEZpbHRlckV4cHJlc3Npb24sXG4gIEZpbHRlckV4cHJlc3Npb25TaW1wbGUsXG4gIFBhcnNlZElkSW5kZXgsXG4gIFBhcnNlZElkRXhwcmVzc2lvbixcbiAgUGFyc2VkSWRTaW1wbGUsXG59IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCBFdmVudEVtaXR0ZXIgZnJvbSBcImV2ZW50c1wiO1xuaW1wb3J0IHsgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuXG50eXBlIFZhbHVlQ2hhbmdlID0ge1xuICBjaGFuZ2VkOiBib29sZWFuO1xuICBvbGRWYWx1ZT86IGFueTtcbn07XG5cbmNvbnN0IFBST1BFUlRZX0ZJTFRFUl9QQVRURVJOID0gL1xcWy4rXFxdLztcbmNvbnN0IFBST1BFUlRZX0ZJTFRFUl9VTldSQVBQRVIgPSAvW1xcW1xcXV0vZztcbmNvbnN0IFNJTVBMRV9JRCA9IFwic2ltcGxlXCI7XG5jb25zdCBJTkRFWF9JRCA9IFwiaW5kZXhcIjtcbmNvbnN0IEVYUFJFU1NJT05fSUQgPSBcImV4cHJlc3Npb25cIjtcblxuY29uc3QgRklMVEVSX1BBVFRFUk4gPSAvXihbXFx3fFxcLnxcXFt8XFxdfFxcc3xcXC98PXwhfCZdKz8pKD17MCwyfXw+PT98PD0/fDw+fCE9ezEsMn0pKFtcXC18XFx3fFxcLnxcXFt8XFxdfFxcc3xcXHU0ZTAwLVxcdTlmZmZdKykkLztcblxuZW51bSBJTkpFQ1RFRF9QUk9QRVJUWV9OQU1FUyB7XG4gIEVNSVRURVIgPSBcIiQkZW1pdHRlclwiLFxuICBPTiA9IFwiJCRvblwiLFxuICBPRkYgPSBcIiQkb2ZmXCIsXG4gIENSRUFURV9FVkVOVCA9IFwiJCRjcmVhdGVFdmVudFwiLFxuICBGSVJFX0VWRU5UID0gXCIkJGZpcmVFdmVudFwiLFxuICBNT0RFTCA9IFwiJCRtb2RlbFwiLFxuICBQUk9YSUVEID0gXCIkJHByb3hpZWRcIixcbiAgU1dJVENIX01PREVMID0gXCIkJHN3aXRjaE1vZGVsXCJcbn1cblxuY2xhc3MgTW9kZWxsZXIge1xuXG4gIHByaXZhdGUgc3RhdGljIGZpbmRFbWl0dGluZ0lkKGlkOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIGNvbnN0IGlkcyA9IE1vZGVsbGVyLnBhcnNlSWQoaWQpO1xuICAgIHJldHVybiBpZHNcbiAgICAgIC5tYXAoaWQgPT4ge1xuICAgICAgICBzd2l0Y2ggKGlkLnR5cGUpIHtcbiAgICAgICAgICBjYXNlIElOREVYX0lEOlxuICAgICAgICAgICAgcmV0dXJuIGAke2lkLmlkfVske2lkLmluZGV4fV1gO1xuICAgICAgICAgIGNhc2UgRVhQUkVTU0lPTl9JRDpcbiAgICAgICAgICAgIHJldHVybiBpZC5pZDtcbiAgICAgICAgICBjYXNlIFNJTVBMRV9JRDpcbiAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgcmV0dXJuIGlkLmlkO1xuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgLmpvaW4oXCIuXCIpO1xuICB9XG5cbiAgc3RhdGljIGFzUHJveGllZChtb2RlbDogYW55KTogYW55IHtcbiAgICBpZiAoTW9kZWxsZXIuaXNQcm94aWVkKG1vZGVsKSkge1xuICAgICAgcmV0dXJuIG1vZGVsO1xuICAgIH1cblxuICAgIGNvbnN0IGVtaXR0ZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCkuc2V0TWF4TGlzdGVuZXJzKEluZmluaXR5KTtcbiAgICBjb25zdCBjcmVhdGVFdmVudCA9IChtb2RlbDogYW55LCBwcm9wOiBzdHJpbmcsIG9sZFZhbHVlOiBhbnksIG5ld1ZhbHVlOiBhbnkpOiBNb2RlbERhdGFDaGFuZ2VkRXZlbnQgPT4ge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbW9kZWw6IG1vZGVsLFxuICAgICAgICBwcm9wOiBwcm9wLFxuICAgICAgICBvbGRWYWx1ZTogb2xkVmFsdWUsXG4gICAgICAgIG5ld1ZhbHVlOiBuZXdWYWx1ZSxcbiAgICAgIH07XG4gICAgfTtcblxuICAgIGxldCB0YXJnZXQgPSBtb2RlbDtcbiAgICByZXR1cm4gbmV3IFByb3h5KG1vZGVsLCB7XG4gICAgICBnZXQ6IGZ1bmN0aW9uKG9iaiwgcHJvcCkge1xuICAgICAgICBpZiAoVXRpbHMuaXNTeW1ib2wocHJvcCkpIHtcbiAgICAgICAgICByZXR1cm4gdGFyZ2V0W3Byb3BdO1xuICAgICAgICB9XG5cbiAgICAgICAgc3dpdGNoIChwcm9wKSB7XG4gICAgICAgICAgY2FzZSBJTkpFQ1RFRF9QUk9QRVJUWV9OQU1FUy5NT0RFTDpcbiAgICAgICAgICAgIHJldHVybiB0YXJnZXQ7XG4gICAgICAgICAgY2FzZSBJTkpFQ1RFRF9QUk9QRVJUWV9OQU1FUy5QUk9YSUVEOlxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgY2FzZSBJTkpFQ1RFRF9QUk9QRVJUWV9OQU1FUy5FTUlUVEVSOlxuICAgICAgICAgICAgcmV0dXJuIGVtaXR0ZXI7XG4gICAgICAgICAgY2FzZSBJTkpFQ1RFRF9QUk9QRVJUWV9OQU1FUy5PTjpcbiAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbih0aGlzOiBhbnksIHByb3A6IHN0cmluZywgbGlzdGVuZXI6IE1vZGVsRGF0YUNoYW5nZUV2ZW50TGlzdGVuZXIpOiBhbnkge1xuICAgICAgICAgICAgICBjb25zdCBpZCA9IE1vZGVsbGVyLmZpbmRFbWl0dGluZ0lkKHByb3ApO1xuICAgICAgICAgICAgICBjb25zdCBleGlzdHMgPSBlbWl0dGVyLmxpc3RlbmVycyhpZCkgYXMgRnVuY3Rpb25bXTtcbiAgICAgICAgICAgICAgaWYgKCFleGlzdHMuaW5jbHVkZXMobGlzdGVuZXIpKSB7XG4gICAgICAgICAgICAgICAgZW1pdHRlci5vbihpZCwgbGlzdGVuZXIpO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgIGNhc2UgSU5KRUNURURfUFJPUEVSVFlfTkFNRVMuT0ZGOlxuICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKHRoaXM6IGFueSwgcHJvcDogc3RyaW5nLCBsaXN0ZW5lcjogTW9kZWxEYXRhQ2hhbmdlRXZlbnRMaXN0ZW5lcik6IGFueSB7XG4gICAgICAgICAgICAgIGVtaXR0ZXIub2ZmKE1vZGVsbGVyLmZpbmRFbWl0dGluZ0lkKHByb3ApLCBsaXN0ZW5lcik7XG4gICAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICBjYXNlIElOSkVDVEVEX1BST1BFUlRZX05BTUVTLkNSRUFURV9FVkVOVDpcbiAgICAgICAgICAgIHJldHVybiBjcmVhdGVFdmVudDtcbiAgICAgICAgICBjYXNlIElOSkVDVEVEX1BST1BFUlRZX05BTUVTLkZJUkVfRVZFTlQ6XG4gICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24odGhpczogYW55LCBwcm9wOiBzdHJpbmcsIG9sZFZhbHVlOiBhbnksIG5ld1ZhbHVlOiBhbnkpOiBhbnkge1xuICAgICAgICAgICAgICBlbWl0dGVyLmVtaXQoTW9kZWxsZXIuZmluZEVtaXR0aW5nSWQocHJvcCksIGNyZWF0ZUV2ZW50KHRhcmdldCwgcHJvcCwgb2xkVmFsdWUsIG5ld1ZhbHVlKSk7XG4gICAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICBjYXNlIElOSkVDVEVEX1BST1BFUlRZX05BTUVTLlNXSVRDSF9NT0RFTDpcbiAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbihuZXdNb2RlbDogYW55KTogYW55IHtcbiAgICAgICAgICAgICAgaWYgKG5ld01vZGVsID09IG51bGwpIHtcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJNb2RlbCB0byBzd2l0Y2ggY2Fubm90IGJlIG51bGwuXCIpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHRhcmdldCA9IG5ld01vZGVsO1xuICAgICAgICAgICAgICByZXR1cm4gbmV3TW9kZWw7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICByZXR1cm4gTW9kZWxsZXIuZ2V0VmFsdWVGcm9tTW9kZWwodGFyZ2V0LCBwcm9wIGFzIHN0cmluZyk7XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBzZXQ6IGZ1bmN0aW9uKG9iajogYW55LCBwcm9wOiBzdHJpbmcsIHZhbHVlOiBhbnkpOiBib29sZWFuIHtcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gTW9kZWxsZXIuc2V0VmFsdWVUb01vZGVsKHRhcmdldCwgcHJvcCwgdmFsdWUpO1xuICAgICAgICBpZiAocmVzdWx0LmNoYW5nZWQpIHtcbiAgICAgICAgICBlbWl0dGVyLmVtaXQoTW9kZWxsZXIuZmluZEVtaXR0aW5nSWQocHJvcCksIGNyZWF0ZUV2ZW50KHRhcmdldCwgcHJvcCwgcmVzdWx0Lm9sZFZhbHVlLCB2YWx1ZSkpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfSxcbiAgICAgIGRlbGV0ZVByb3BlcnR5OiBmdW5jdGlvbihvYmo6IGFueSwgcHJvcDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgICAgIGlmIChwcm9wIGluIHRhcmdldCkge1xuICAgICAgICAgIGNvbnN0IG9sZFZhbHVlID0gdGFyZ2V0W3Byb3BdO1xuICAgICAgICAgIGVtaXR0ZXIuZW1pdChNb2RlbGxlci5maW5kRW1pdHRpbmdJZChwcm9wKSwgY3JlYXRlRXZlbnQodGFyZ2V0LCBwcm9wLCBvbGRWYWx1ZSwgdW5kZWZpbmVkKSk7XG4gICAgICAgICAgZGVsZXRlIHRhcmdldFtwcm9wXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH0sXG4gICAgICBvd25LZXlzOiBmdW5jdGlvbihvYmo6IGFueSk6IChzdHJpbmcgfCBudW1iZXIgfCBzeW1ib2wpW10ge1xuICAgICAgICByZXR1cm4gUmVmbGVjdC5vd25LZXlzKHRhcmdldCk7XG4gICAgICB9LFxuICAgICAgaGFzOiBmdW5jdGlvbihvYmo6IGFueSwgcHJvcDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiBSZWZsZWN0Lmhhcyh0YXJnZXQsIHByb3ApO1xuICAgICAgfSxcbiAgICAgIGdldFByb3RvdHlwZU9mOiBmdW5jdGlvbih0YXJnZXQ6IGFueSk6IG9iamVjdCB8IG51bGwge1xuICAgICAgICByZXR1cm4gUmVmbGVjdC5nZXRQcm90b3R5cGVPZih0YXJnZXQpO1xuICAgICAgfSxcbiAgICAgIGdldE93blByb3BlcnR5RGVzY3JpcHRvcjogZnVuY3Rpb24ob2JqOiBhbnksIHByb3A6IHN0cmluZyk6IFByb3BlcnR5RGVzY3JpcHRvciB8IHVuZGVmaW5lZCB7XG4gICAgICAgIGlmIChwcm9wLnN0YXJ0c1dpdGgoXCIkJFwiKSkge1xuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjb25maWd1cmFibGU6IGZhbHNlLFxuICAgICAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXG4gICAgICAgICAgICB3cml0YWJsZTogZmFsc2UsXG4gICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gUmVmbGVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IodGFyZ2V0LCBwcm9wKTtcbiAgICAgIH0sXG4gICAgfSk7XG4gIH1cblxuICBzdGF0aWMgaXNQcm94aWVkKG1vZGVsOiBhbnkpOiBib29sZWFuIHtcbiAgICBpZiAobW9kZWwgPT0gbnVsbCkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gbW9kZWwuJCRwcm94aWVkID09PSB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIHN0YXRpYyBvbihtb2RlbDogYW55LCBwcm9wOiBzdHJpbmcsIGxpc3RlbmVyOiBNb2RlbERhdGFDaGFuZ2VFdmVudExpc3RlbmVyKTogdm9pZCB7XG4gICAgaWYgKG1vZGVsICE9IG51bGwpIHtcbiAgICAgIG1vZGVsLiQkb24ocHJvcCwgbGlzdGVuZXIpO1xuICAgIH1cbiAgfVxuXG4gIHN0YXRpYyBvZmYobW9kZWw6IGFueSwgcHJvcDogc3RyaW5nLCBsaXN0ZW5lcjogTW9kZWxEYXRhQ2hhbmdlRXZlbnRMaXN0ZW5lcik6IHZvaWQge1xuICAgIGlmIChtb2RlbCAhPSBudWxsKSB7XG4gICAgICBtb2RlbC4kJG9mZihwcm9wLCBsaXN0ZW5lcik7XG4gICAgfVxuICB9XG5cbiAgc3RhdGljIGlzRnJvbVJvb3QoaWQ6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBCb29sZWFuKGlkICYmIGlkLnN0YXJ0c1dpdGgoXCIvXCIpKTtcbiAgfVxuXG4gIHN0YXRpYyBnZXRSZWFsSWQoaWQ6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgcmV0dXJuIE1vZGVsbGVyLmlzRnJvbVJvb3QoaWQpID8gaWQuc3Vic3RyaW5nKDEpIDogaWQ7XG4gIH1cblxuICBzdGF0aWMgcGFyc2VGaWx0ZXIoZmlsdGVyOiBzdHJpbmcpOiBGaWx0ZXJFeHByZXNzaW9uIHwgRmlsdGVyRXhwcmVzc2lvblNpbXBsZSB7XG4gICAgbGV0IG1hdGNoO1xuICAgIGlmICghZmlsdGVyLmluY2x1ZGVzKFwiW1wiKSAmJiAhZmlsdGVyLmluY2x1ZGVzKFwiXVwiKSAmJiAoZmlsdGVyLmluY2x1ZGVzKFwiJiZcIikgfHwgZmlsdGVyLmluY2x1ZGVzKFwifHxcIikpKSB7XG4gICAgICBjb25zdCB0b3AgPSB7XG4gICAgICAgIHR5cGU6IFwiYW5kXCIsXG4gICAgICAgIGV4cHJlc3Npb25zOiBbXSxcbiAgICAgIH0gYXMgRmlsdGVyRXhwcmVzc2lvbjtcblxuICAgICAgY29uc3Qgc3RhY2sgPSBbdG9wXTtcbiAgICAgIGxldCBjdXJyZW50RXhwcmVzc2lvbiA9IFwiXCI7XG4gICAgICBsZXQgZmlyc3QgPSB0cnVlO1xuXG4gICAgICBmaWx0ZXIuc3BsaXQoXCJcIikuZm9yRWFjaChjaCA9PiB7XG4gICAgICAgIGlmIChjaCA9PT0gXCIoXCIpIHtcbiAgICAgICAgICBjb25zdCBncm91cCA9IHtcbiAgICAgICAgICAgIHR5cGU6IFwiYW5kXCIsXG4gICAgICAgICAgICBleHByZXNzaW9uczogW10sXG4gICAgICAgICAgfSBhcyBGaWx0ZXJFeHByZXNzaW9uO1xuICAgICAgICAgIHN0YWNrW3N0YWNrLmxlbmd0aCAtIDFdLmV4cHJlc3Npb25zLnB1c2goZ3JvdXApO1xuICAgICAgICAgIHN0YWNrLnB1c2goZ3JvdXApO1xuICAgICAgICB9IGVsc2UgaWYgKGNoID09PSBcIilcIikge1xuICAgICAgICAgIGlmIChjdXJyZW50RXhwcmVzc2lvbikge1xuICAgICAgICAgICAgc3RhY2tbc3RhY2subGVuZ3RoIC0gMV0uZXhwcmVzc2lvbnMucHVzaChNb2RlbGxlci5wYXJzZUZpbHRlcihjdXJyZW50RXhwcmVzc2lvbikpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBzdGFjay5wb3AoKTtcbiAgICAgICAgICBjdXJyZW50RXhwcmVzc2lvbiA9IFwiXCI7XG4gICAgICAgIH0gZWxzZSBpZiAoKGNoID09PSBcIiZcIiB8fCBjaCA9PT0gXCJ8XCIpICYmIGZpcnN0KSB7XG4gICAgICAgICAgaWYgKGNoID09PSBcInxcIikge1xuICAgICAgICAgICAgc3RhY2tbc3RhY2subGVuZ3RoIC0gMV0udHlwZSA9IFwib3JcIjtcbiAgICAgICAgICB9XG4gICAgICAgICAgZmlyc3QgPSBmYWxzZTtcbiAgICAgICAgICBpZiAoY3VycmVudEV4cHJlc3Npb24pIHtcbiAgICAgICAgICAgIHN0YWNrW3N0YWNrLmxlbmd0aCAtIDFdLmV4cHJlc3Npb25zLnB1c2goTW9kZWxsZXIucGFyc2VGaWx0ZXIoY3VycmVudEV4cHJlc3Npb24pKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgY3VycmVudEV4cHJlc3Npb24gPSBcIlwiO1xuICAgICAgICB9IGVsc2UgaWYgKChjaCA9PT0gXCImXCIgfHwgY2ggPT09IFwifFwiKSAmJiAhZmlyc3QpIHtcbiAgICAgICAgICBmaXJzdCA9IHRydWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY3VycmVudEV4cHJlc3Npb24gKz0gY2g7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgaWYgKGN1cnJlbnRFeHByZXNzaW9uKSB7XG4gICAgICAgIHN0YWNrW3N0YWNrLmxlbmd0aCAtIDFdLmV4cHJlc3Npb25zLnB1c2goTW9kZWxsZXIucGFyc2VGaWx0ZXIoY3VycmVudEV4cHJlc3Npb24pKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0b3A7XG4gICAgfSBlbHNlIGlmICgobWF0Y2ggPSBmaWx0ZXIubWF0Y2goRklMVEVSX1BBVFRFUk4pKSAhPSBudWxsKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBwcm9wOiBtYXRjaFsxXS50cmltKCksXG4gICAgICAgIG9wZXJhdG9yOiBtYXRjaFsyXSxcbiAgICAgICAgdmFsdWU6IG1hdGNoWzNdLnRyaW0oKSxcbiAgICAgIH0gYXMgRmlsdGVyRXhwcmVzc2lvblNpbXBsZTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKGBDYW5ub3QgcGFyc2UgZmlsdGVyWyR7ZmlsdGVyfV0uYCk7XG4gICAgfVxuICB9XG5cbiAgc3RhdGljIGlzU2ltcGxlRXhwcmVzc2lvbihmaWx0ZXI6IEZpbHRlckV4cHJlc3Npb25TaW1wbGUgfCBGaWx0ZXJFeHByZXNzaW9uKTogZmlsdGVyIGlzIEZpbHRlckV4cHJlc3Npb25TaW1wbGUge1xuICAgIHJldHVybiAoZmlsdGVyIGFzIGFueSkudHlwZSA9PSB1bmRlZmluZWQ7XG4gIH1cblxuICBzdGF0aWMgbWF0Y2hGaWx0ZXIobW9kZWw6IGFueSwgZmlsdGVyOiBGaWx0ZXJFeHByZXNzaW9uU2ltcGxlIHwgRmlsdGVyRXhwcmVzc2lvbik6IGJvb2xlYW4ge1xuICAgIGlmIChNb2RlbGxlci5pc1NpbXBsZUV4cHJlc3Npb24oZmlsdGVyKSkge1xuICAgICAgY29uc3QgdmFsdWUgPSBNb2RlbGxlci5nZXRWYWx1ZUZyb21Nb2RlbChtb2RlbCwgZmlsdGVyLnByb3ApO1xuICAgICAgY29uc3Qgb3AgPSBmaWx0ZXIub3BlcmF0b3I7XG4gICAgICByZXR1cm4gTW9kZWxsZXIubWF0Y2hWYWx1ZSh2YWx1ZSwgb3AsIGZpbHRlci52YWx1ZSk7XG4gICAgfSBlbHNlIGlmIChmaWx0ZXIudHlwZSA9PT0gXCJhbmRcIikge1xuICAgICAgcmV0dXJuICFmaWx0ZXIuZXhwcmVzc2lvbnMuc29tZShzdWIgPT4ge1xuICAgICAgICByZXR1cm4gIU1vZGVsbGVyLm1hdGNoRmlsdGVyKG1vZGVsLCBzdWIpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIGlmIChmaWx0ZXIudHlwZSA9PT0gXCJvclwiKSB7XG4gICAgICByZXR1cm4gZmlsdGVyLmV4cHJlc3Npb25zLnNvbWUoc3ViID0+IHtcbiAgICAgICAgcmV0dXJuIE1vZGVsbGVyLm1hdGNoRmlsdGVyKG1vZGVsLCBzdWIpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZmlsdGVyKTtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIlVuc3VwcG9ydGVkIGZpbHRlci5cIik7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgZXF1YWxzKGE6IGFueSwgYjogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgY29uc3QgdiA9IGIgPT09IFwidHJ1ZVwiID8gdHJ1ZSA6IGIgPT09IFwiZmFsc2VcIiA/IGZhbHNlIDogYiA9PT0gXCJudWxsXCIgPyBudWxsIDogYiA9PT0gXCJcIiA/IFwiXCIgOiBiO1xuICAgIGlmICh2ID09PSB0cnVlKSB7XG4gICAgICByZXR1cm4gYSA9PT0gdHJ1ZTtcbiAgICB9IGVsc2UgaWYgKHYgPT09IG51bGwgfHwgdiA9PT0gZmFsc2UgfHwgdiA9PT0gXCJcIikge1xuICAgICAgcmV0dXJuIGEgPT0gbnVsbCB8fCBhID09PSBmYWxzZSB8fCBhID09PSBcIlwiO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gYSA9PSBiO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc3RhdGljIGZvcmNlRXF1YWxzKGE6IGFueSwgYjogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgaWYgKGIgPT09IFwidHJ1ZVwiKSB7XG4gICAgICByZXR1cm4gYSA9PT0gdHJ1ZTtcbiAgICB9IGVsc2UgaWYgKGIgPT09IFwiZmFsc2VcIikge1xuICAgICAgcmV0dXJuIGEgPT09IGZhbHNlO1xuICAgIH0gZWxzZSBpZiAoYiA9PT0gXCJudWxsXCIpIHtcbiAgICAgIHJldHVybiBhID09PSBudWxsIHx8IHR5cGVvZiBhID09PSBcInVuZGVmaW5lZFwiO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gYSA9PT0gYjtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHN0YXRpYyBtYXRjaFZhbHVlKHZhbHVlOiBhbnksIG9wOiBzdHJpbmcsIGZpbHRlclZhbHVlOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICBpZiAob3AgPT09IFwiPVwiIHx8IG9wID09PSBcIj09XCIpIHtcbiAgICAgIHJldHVybiBNb2RlbGxlci5lcXVhbHModmFsdWUsIGZpbHRlclZhbHVlKTtcbiAgICB9IGVsc2UgaWYgKG9wID09PSBcIjw+XCIgfHwgb3AgPT09IFwiIT1cIikge1xuICAgICAgcmV0dXJuICFNb2RlbGxlci5lcXVhbHModmFsdWUsIGZpbHRlclZhbHVlKTtcbiAgICB9IGVsc2UgaWYgKG9wID09PSBcIj5cIikge1xuICAgICAgcmV0dXJuIHZhbHVlID4gK2ZpbHRlclZhbHVlO1xuICAgIH0gZWxzZSBpZiAob3AgPT09IFwiPFwiKSB7XG4gICAgICByZXR1cm4gdmFsdWUgPCArZmlsdGVyVmFsdWU7XG4gICAgfSBlbHNlIGlmIChvcCA9PT0gXCI+PVwiKSB7XG4gICAgICByZXR1cm4gdmFsdWUgPj0gK2ZpbHRlclZhbHVlO1xuICAgIH0gZWxzZSBpZiAob3AgPT09IFwiPD1cIikge1xuICAgICAgcmV0dXJuIHZhbHVlIDw9ICtmaWx0ZXJWYWx1ZTtcbiAgICB9IGVsc2UgaWYgKG9wID09PSBcIj09PVwiKSB7XG4gICAgICByZXR1cm4gTW9kZWxsZXIuZm9yY2VFcXVhbHModmFsdWUsIGZpbHRlclZhbHVlKTtcbiAgICB9IGVsc2UgaWYgKG9wID09PSBcIiE9PVwiKSB7XG4gICAgICByZXR1cm4gIU1vZGVsbGVyLmZvcmNlRXF1YWxzKHZhbHVlLCBmaWx0ZXJWYWx1ZSk7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgZ2V0RnJvbU1vZGVsKG1vZGVsOiBhbnksIGlkOiBzdHJpbmcpOiBhbnkgfCBudWxsIHwgdW5kZWZpbmVkIHtcbiAgICByZXR1cm4gbW9kZWwgPyBtb2RlbFtpZF0gOiBudWxsO1xuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgc2V0VG9Nb2RlbChtb2RlbDogYW55LCBpZDogc3RyaW5nLCB2YWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKG1vZGVsKSB7XG4gICAgICBtb2RlbFtpZF0gPSB2YWx1ZTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHN0YXRpYyBnZXRPckNyZWF0ZUJ5SWRzKFxuICAgIG1vZGVsOiBhbnksXG4gICAgaWRzOiAoUGFyc2VkSWRTaW1wbGUgfCBQYXJzZWRJZEV4cHJlc3Npb24gfCBQYXJzZWRJZEluZGV4KVtdLFxuICAgIGlkOiBzdHJpbmcsXG4gICkge1xuICAgIHJldHVybiBpZHMucmVkdWNlKChtb2RlbDogYW55LCBpZFBhcnQsIGlkUGFydEluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgIHN3aXRjaCAoaWRQYXJ0LnR5cGUpIHtcbiAgICAgICAgY2FzZSBJTkRFWF9JRDpcbiAgICAgICAgICBjb25zdCBhcnJheSA9IE1vZGVsbGVyLmdldE9yQ3JlYXRlQXJyYXlGcm9tTW9kZWwobW9kZWwsIGlkUGFydC5pZCk7XG4gICAgICAgICAgY29uc3QgbGVuZ3RoID0gYXJyYXkubGVuZ3RoO1xuICAgICAgICAgIGNvbnN0IGluZGV4ID0gaWRQYXJ0LmluZGV4O1xuICAgICAgICAgIGlmIChpbmRleCA8IDAgfHwgaW5kZXggPiBsZW5ndGggLSAxKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYEluZGV4IG91dCBvZiBib3VuZHMgd2hlbiBnZXQgdmFsdWUgYnkgWyR7aWR9XWApO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gYXJyYXlbaW5kZXhdO1xuICAgICAgICAgIH1cbiAgICAgICAgY2FzZSBFWFBSRVNTSU9OX0lEOlxuICAgICAgICAgIGNvbnN0IGFycmF5RGF0YSA9IE1vZGVsbGVyLmdldE9yQ3JlYXRlQXJyYXlGcm9tTW9kZWwobW9kZWwsIGlkUGFydC5pZCkuZmlsdGVyKGl0ZW0gPT4ge1xuICAgICAgICAgICAgcmV0dXJuIE1vZGVsbGVyLm1hdGNoRmlsdGVyKGl0ZW0sIGlkUGFydC5maWx0ZXIpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIGlmIChpZFBhcnQuZXhhY3QpIHtcbiAgICAgICAgICAgIGlmIChhcnJheURhdGEubGVuZ3RoICE9PSAxKSB7XG4gICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgSW5kZXggb3V0IG9mIGJvdW5kcyB3aGVuIGdldCB2YWx1ZSBieSBbJHtpZH1dYCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICByZXR1cm4gYXJyYXlEYXRhWzBdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYEV4cHJlc3Npb25bJHtpZH1dIGlzIG5vdCBzdXBwb3J0ZWQuYCk7XG4gICAgICAgICAgfVxuICAgICAgICBjYXNlIFNJTVBMRV9JRDpcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICBsZXQgb2JqZWN0ID0gTW9kZWxsZXIuZ2V0RnJvbU1vZGVsKG1vZGVsLCBpZFBhcnQuaWQpO1xuICAgICAgICAgIGlmIChvYmplY3QgPT0gbnVsbCkge1xuICAgICAgICAgICAgb2JqZWN0ID0ge307XG4gICAgICAgICAgICBNb2RlbGxlci5zZXRUb01vZGVsKG1vZGVsLCBpZFBhcnQuaWQsIG9iamVjdCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgICB9XG4gICAgfSwgbW9kZWwpO1xuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgZ2V0T3JDcmVhdGVBcnJheUZyb21Nb2RlbCA9IGZ1bmN0aW9uKG1vZGVsOiBhbnksIGlkOiBzdHJpbmcpOiBbXSB7XG4gICAgbGV0IGFycmF5ID0gTW9kZWxsZXIuZ2V0RnJvbU1vZGVsKG1vZGVsLCBpZCk7XG4gICAgaWYgKGFycmF5ID09IG51bGwpIHtcbiAgICAgIGFycmF5ID0gW107XG4gICAgICBNb2RlbGxlci5zZXRUb01vZGVsKG1vZGVsLCBpZCwgYXJyYXkpO1xuICAgIH1cbiAgICByZXR1cm4gYXJyYXk7XG4gIH07XG5cbiAgcHJpdmF0ZSBzdGF0aWMgZ2V0VmFsdWVGcm9tTW9kZWwobW9kZWw6IGFueSwgaWQ6IHN0cmluZyk6IGFueSB7XG4gICAgaWYgKCFtb2RlbCAmJiAhaWQpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICBjb25zdCBpZHMgPSBNb2RlbGxlci5wYXJzZUlkKGlkKTtcbiAgICByZXR1cm4gaWRzLnJlZHVjZSgobW9kZWwsIGlkUGFydCkgPT4ge1xuICAgICAgaWYgKG1vZGVsKSB7XG4gICAgICAgIHN3aXRjaCAoaWRQYXJ0LnR5cGUpIHtcbiAgICAgICAgICBjYXNlIElOREVYX0lEOlxuICAgICAgICAgICAgY29uc3QgYXJyYXkgPSBNb2RlbGxlci5nZXRPckNyZWF0ZUFycmF5RnJvbU1vZGVsKG1vZGVsLCBpZFBhcnQuaWQpO1xuICAgICAgICAgICAgY29uc3QgbGVuZ3RoID0gYXJyYXkubGVuZ3RoO1xuICAgICAgICAgICAgY29uc3QgaW5kZXggPSBpZFBhcnQuaW5kZXg7XG4gICAgICAgICAgICBpZiAoaW5kZXggPCAwIHx8IGluZGV4ID4gbGVuZ3RoIC0gMSkge1xuICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYEluZGV4IG91dCBvZiBib3VuZHMgd2hlbiBnZXQgdmFsdWUgYnkgWyR7aWR9XWApO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGFycmF5W2luZGV4XTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBjYXNlIEVYUFJFU1NJT05fSUQ6XG4gICAgICAgICAgICBjb25zdCBhcnJheURhdGEgPSBNb2RlbGxlci5nZXRPckNyZWF0ZUFycmF5RnJvbU1vZGVsKG1vZGVsLCBpZFBhcnQuaWQpLmZpbHRlcihpdGVtID0+IHtcbiAgICAgICAgICAgICAgcmV0dXJuIE1vZGVsbGVyLm1hdGNoRmlsdGVyKGl0ZW0sIGlkUGFydC5maWx0ZXIpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBpZiAoaWRQYXJ0LmV4YWN0KSB7XG4gICAgICAgICAgICAgIGlmIChhcnJheURhdGEubGVuZ3RoICE9PSAxKSB7XG4gICAgICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBJbmRleCBvdXQgb2YgYm91bmRzIHdoZW4gZ2V0IHZhbHVlIGJ5IFske2lkfV1gKTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gYXJyYXlEYXRhWzBdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICByZXR1cm4gYXJyYXlEYXRhO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIGNhc2UgU0lNUExFX0lEOlxuICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICByZXR1cm4gTW9kZWxsZXIuZ2V0RnJvbU1vZGVsKG1vZGVsLCBpZFBhcnQuaWQpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cbiAgICB9LCBtb2RlbCk7XG4gIH1cblxuICBzdGF0aWMgZ2V0VmFsdWUod2hlcmU6IHsgbW9kZWw6IGFueTsgcm9vdDogYW55IH0sIGlkOiBzdHJpbmcpOiBhbnkge1xuICAgIGlmICh3aGVyZS5tb2RlbCA9PSBudWxsKSB7XG4gICAgICByZXR1cm4gTW9kZWxsZXIuZ2V0VmFsdWUoeyBtb2RlbDogd2hlcmUsIHJvb3Q6IHdoZXJlIH0sIGlkKTtcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc3QgbW9kZWwgPSBNb2RlbGxlci5pc0Zyb21Sb290KGlkKSA/IHdoZXJlLnJvb3QgOiB3aGVyZS5tb2RlbDtcbiAgICAgIGlmIChNb2RlbGxlci5pc1Byb3hpZWQobW9kZWwpKSB7XG4gICAgICAgIHJldHVybiBtb2RlbFtNb2RlbGxlci5nZXRSZWFsSWQoaWQpXTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBNb2RlbGxlci5nZXRWYWx1ZUZyb21Nb2RlbChtb2RlbCwgTW9kZWxsZXIuZ2V0UmVhbElkKGlkKSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgc2V0VmFsdWVUb01vZGVsKG1vZGVsOiBhbnksIGlkOiBzdHJpbmcsIHZhbHVlOiBhbnksIGZvcmNlOiBib29sZWFuID0gZmFsc2UpOiBWYWx1ZUNoYW5nZSB7XG4gICAgbGV0IG9sZFZhbHVlO1xuICAgIGlmICghZm9yY2UgJiYgdmFsdWUgPT0gKG9sZFZhbHVlID0gTW9kZWxsZXIuZ2V0VmFsdWVGcm9tTW9kZWwobW9kZWwsIGlkKSkpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGNoYW5nZWQ6IGZhbHNlLFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBpZHMgPSBNb2RlbGxlci5wYXJzZUlkKGlkKTtcbiAgICBjb25zdCBsYXN0ID0gaWRzLnBvcCgpO1xuICAgIGNvbnN0IG9iamVjdCA9IE1vZGVsbGVyLmdldE9yQ3JlYXRlQnlJZHMobW9kZWwsIGlkcywgaWQpO1xuICAgIE1vZGVsbGVyLnNldFRvTW9kZWwob2JqZWN0LCBsYXN0IS5pZCwgdmFsdWUpO1xuICAgIHJldHVybiB7XG4gICAgICBjaGFuZ2VkOiB0cnVlLFxuICAgICAgb2xkVmFsdWUsXG4gICAgfTtcbiAgfVxuXG4gIHN0YXRpYyBzZXRWYWx1ZSh3aGVyZTogeyBtb2RlbDogYW55OyByb290OiBhbnkgfSB8IGFueSwgaWQ6IHN0cmluZywgdmFsdWU6IGFueSwgZm9yY2U6IGJvb2xlYW4gPSBmYWxzZSk6IGFueSB7XG4gICAgaWYgKHdoZXJlLm1vZGVsID09IG51bGwpIHtcbiAgICAgIE1vZGVsbGVyLnNldFZhbHVlKHsgbW9kZWw6IHdoZXJlLCByb290OiB3aGVyZSB9LCBpZCwgdmFsdWUsIGZvcmNlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc3QgbW9kZWwgPSBNb2RlbGxlci5pc0Zyb21Sb290KGlkKSA/IHdoZXJlLnJvb3QgOiB3aGVyZS5tb2RlbDtcbiAgICAgIGlmIChNb2RlbGxlci5pc1Byb3hpZWQobW9kZWwpKSB7XG4gICAgICAgIHJldHVybiAobW9kZWxbTW9kZWxsZXIuZ2V0UmVhbElkKGlkKV0gPSB2YWx1ZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gTW9kZWxsZXIuc2V0VmFsdWVUb01vZGVsKG1vZGVsLCBNb2RlbGxlci5nZXRSZWFsSWQoaWQpLCB2YWx1ZSwgZm9yY2UpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc3RhdGljIHBhcnNlSWQoaWQ6IHN0cmluZyk6IChQYXJzZWRJZFNpbXBsZSB8IFBhcnNlZElkRXhwcmVzc2lvbiB8IFBhcnNlZElkSW5kZXgpW10ge1xuICAgIC8vIGNvbnN0IGlkcyA9IGlkLnNwbGl0KCcuJyk7XG4gICAgbGV0IGN1cnJlbnRJZCA9IFwiXCI7XG4gICAgbGV0IGluRXhwcmVzc2lvbiA9IGZhbHNlO1xuXG4gICAgY29uc3QgaWRzID0gW107XG4gICAgaWQuc3BsaXQoXCJcIikuZm9yRWFjaChjaCA9PiB7XG4gICAgICBpZiAoY2ggIT09IFwiLlwiKSB7XG4gICAgICAgIGN1cnJlbnRJZCArPSBjaDtcbiAgICAgIH1cbiAgICAgIGlmIChjaCA9PT0gXCJbXCIpIHtcbiAgICAgICAgaW5FeHByZXNzaW9uID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSBpZiAoY2ggPT09IFwiXVwiKSB7XG4gICAgICAgIGluRXhwcmVzc2lvbiA9IGZhbHNlO1xuICAgICAgfSBlbHNlIGlmIChjaCA9PT0gXCIuXCIgJiYgaW5FeHByZXNzaW9uKSB7XG4gICAgICAgIGN1cnJlbnRJZCArPSBjaDtcbiAgICAgIH0gZWxzZSBpZiAoY2ggPT09IFwiLlwiICYmICFpbkV4cHJlc3Npb24pIHtcbiAgICAgICAgaWRzLnB1c2goY3VycmVudElkKTtcbiAgICAgICAgY3VycmVudElkID0gXCJcIjtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBpZHMucHVzaChjdXJyZW50SWQpO1xuXG4gICAgcmV0dXJuIGlkcy5tYXAoaWQgPT4ge1xuICAgICAgY29uc3QgbWF0Y2hlciA9IGlkLm1hdGNoKFBST1BFUlRZX0ZJTFRFUl9QQVRURVJOKTtcbiAgICAgIGlmIChtYXRjaGVyKSB7XG4gICAgICAgIGNvbnN0IGZpbHRlciA9IG1hdGNoZXJbMF0ucmVwbGFjZShQUk9QRVJUWV9GSUxURVJfVU5XUkFQUEVSLCBcIlwiKTtcbiAgICAgICAgY29uc3QgcmVhbElkID0gaWQuc3Vic3RyaW5nKDAsIG1hdGNoZXIuaW5kZXgpO1xuICAgICAgICBpZiAoIWlzTmFOKCtmaWx0ZXIpKSB7XG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGlkOiByZWFsSWQsXG4gICAgICAgICAgICB0eXBlOiBJTkRFWF9JRCxcbiAgICAgICAgICAgIGluZGV4OiArZmlsdGVyLFxuICAgICAgICAgIH0gYXMgUGFyc2VkSWRJbmRleDtcbiAgICAgICAgfSBlbHNlIGlmIChmaWx0ZXIuc3RhcnRzV2l0aChcIiFcIikpIHtcbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgaWQ6IHJlYWxJZCxcbiAgICAgICAgICAgIHR5cGU6IEVYUFJFU1NJT05fSUQsXG4gICAgICAgICAgICBmaWx0ZXI6IE1vZGVsbGVyLnBhcnNlRmlsdGVyKGZpbHRlci5zdWJzdHJpbmcoMSkudHJpbSgpKSxcbiAgICAgICAgICAgIGV4YWN0OiB0cnVlLFxuICAgICAgICAgIH0gYXMgUGFyc2VkSWRFeHByZXNzaW9uO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBpZDogcmVhbElkLFxuICAgICAgICAgICAgdHlwZTogRVhQUkVTU0lPTl9JRCxcbiAgICAgICAgICAgIGZpbHRlcjogTW9kZWxsZXIucGFyc2VGaWx0ZXIoZmlsdGVyKSxcbiAgICAgICAgICAgIGV4YWN0OiBmYWxzZSxcbiAgICAgICAgICB9IGFzIFBhcnNlZElkRXhwcmVzc2lvbjtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBpZDogaWQsXG4gICAgICAgICAgdHlwZTogU0lNUExFX0lELFxuICAgICAgICB9IGFzIFBhcnNlZElkU2ltcGxlO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IE1vZGVsbGVyO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQVNBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVVBOzs7Ozs7Ozs7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFFQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBdENBO0FBd0NBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQWhGQTtBQWtGQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBOUJBO0FBZ0NBO0FBQ0E7OztBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQTFCQTtBQTRCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7Ozs7OztBQXZkQTtBQWtVQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQWlKQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/data-model/model-utils.tsx
