/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule editOnSelect
 * @format
 * 
 */


var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var ReactDOM = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var getDraftEditorSelection = __webpack_require__(/*! ./getDraftEditorSelection */ "./node_modules/draft-js/lib/getDraftEditorSelection.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

function editOnSelect(editor) {
  if (editor._blockSelectEvents || editor._latestEditorState !== editor.props.editorState) {
    return;
  }

  var editorState = editor.props.editorState;
  var editorNode = ReactDOM.findDOMNode(editor.editorContainer);
  !editorNode ?  true ? invariant(false, 'Missing editorNode') : undefined : void 0;
  !(editorNode.firstChild instanceof HTMLElement) ?  true ? invariant(false, 'editorNode.firstChild is not an HTMLElement') : undefined : void 0;
  var documentSelection = getDraftEditorSelection(editorState, editorNode.firstChild);
  var updatedSelectionState = documentSelection.selectionState;

  if (updatedSelectionState !== editorState.getSelection()) {
    if (documentSelection.needsRecovery) {
      editorState = EditorState.forceSelection(editorState, updatedSelectionState);
    } else {
      editorState = EditorState.acceptSelection(editorState, updatedSelectionState);
    }

    editor.update(editorState);
  }
}

module.exports = editOnSelect;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPblNlbGVjdC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9lZGl0T25TZWxlY3QuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBlZGl0T25TZWxlY3RcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xudmFyIFJlYWN0RE9NID0gcmVxdWlyZSgncmVhY3QtZG9tJyk7XG5cbnZhciBnZXREcmFmdEVkaXRvclNlbGVjdGlvbiA9IHJlcXVpcmUoJy4vZ2V0RHJhZnRFZGl0b3JTZWxlY3Rpb24nKTtcbnZhciBpbnZhcmlhbnQgPSByZXF1aXJlKCdmYmpzL2xpYi9pbnZhcmlhbnQnKTtcblxuZnVuY3Rpb24gZWRpdE9uU2VsZWN0KGVkaXRvcikge1xuICBpZiAoZWRpdG9yLl9ibG9ja1NlbGVjdEV2ZW50cyB8fCBlZGl0b3IuX2xhdGVzdEVkaXRvclN0YXRlICE9PSBlZGl0b3IucHJvcHMuZWRpdG9yU3RhdGUpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgZWRpdG9yU3RhdGUgPSBlZGl0b3IucHJvcHMuZWRpdG9yU3RhdGU7XG4gIHZhciBlZGl0b3JOb2RlID0gUmVhY3RET00uZmluZERPTU5vZGUoZWRpdG9yLmVkaXRvckNvbnRhaW5lcik7XG4gICFlZGl0b3JOb2RlID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ01pc3NpbmcgZWRpdG9yTm9kZScpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcbiAgIShlZGl0b3JOb2RlLmZpcnN0Q2hpbGQgaW5zdGFuY2VvZiBIVE1MRWxlbWVudCkgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAnZWRpdG9yTm9kZS5maXJzdENoaWxkIGlzIG5vdCBhbiBIVE1MRWxlbWVudCcpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcbiAgdmFyIGRvY3VtZW50U2VsZWN0aW9uID0gZ2V0RHJhZnRFZGl0b3JTZWxlY3Rpb24oZWRpdG9yU3RhdGUsIGVkaXRvck5vZGUuZmlyc3RDaGlsZCk7XG4gIHZhciB1cGRhdGVkU2VsZWN0aW9uU3RhdGUgPSBkb2N1bWVudFNlbGVjdGlvbi5zZWxlY3Rpb25TdGF0ZTtcblxuICBpZiAodXBkYXRlZFNlbGVjdGlvblN0YXRlICE9PSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKSkge1xuICAgIGlmIChkb2N1bWVudFNlbGVjdGlvbi5uZWVkc1JlY292ZXJ5KSB7XG4gICAgICBlZGl0b3JTdGF0ZSA9IEVkaXRvclN0YXRlLmZvcmNlU2VsZWN0aW9uKGVkaXRvclN0YXRlLCB1cGRhdGVkU2VsZWN0aW9uU3RhdGUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBlZGl0b3JTdGF0ZSA9IEVkaXRvclN0YXRlLmFjY2VwdFNlbGVjdGlvbihlZGl0b3JTdGF0ZSwgdXBkYXRlZFNlbGVjdGlvblN0YXRlKTtcbiAgICB9XG4gICAgZWRpdG9yLnVwZGF0ZShlZGl0b3JTdGF0ZSk7XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBlZGl0T25TZWxlY3Q7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/editOnSelect.js
