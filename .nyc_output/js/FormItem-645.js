__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FormItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rc_animate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-animate */ "./node_modules/rc-animate/es/Animate.js");
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _grid_row__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../grid/row */ "./node_modules/antd/es/grid/row.js");
/* harmony import */ var _grid_col__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../grid/col */ "./node_modules/antd/es/grid/col.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
/* harmony import */ var _util_type__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/es/_util/type.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./constants */ "./node_modules/antd/es/form/constants.js");
/* harmony import */ var _context__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./context */ "./node_modules/antd/es/form/context.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};















var ValidateStatuses = Object(_util_type__WEBPACK_IMPORTED_MODULE_11__["tuple"])('success', 'warning', 'error', 'validating', '');
var FormLabelAligns = Object(_util_type__WEBPACK_IMPORTED_MODULE_11__["tuple"])('left', 'right');

function intersperseSpace(list) {
  return list.reduce(function (current, item) {
    return [].concat(_toConsumableArray(current), [' ', item]);
  }, []).slice(1);
}

var FormItem =
/*#__PURE__*/
function (_React$Component) {
  _inherits(FormItem, _React$Component);

  function FormItem() {
    var _this;

    _classCallCheck(this, FormItem);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FormItem).apply(this, arguments));
    _this.helpShow = false; // Resolve duplicated ids bug between different forms
    // https://github.com/ant-design/ant-design/issues/7351

    _this.onLabelClick = function () {
      var id = _this.props.id || _this.getId();

      if (!id) {
        return;
      }

      var formItemNode = react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"](_assertThisInitialized(_this));
      var control = formItemNode.querySelector("[id=\"".concat(id, "\"]"));

      if (control && control.focus) {
        control.focus();
      }
    };

    _this.onHelpAnimEnd = function (_key, helpShow) {
      _this.helpShow = helpShow;

      if (!helpShow) {
        _this.setState({});
      }
    };

    _this.renderFormItem = function (_ref) {
      var _itemClassName;

      var getPrefixCls = _ref.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          style = _a.style,
          className = _a.className,
          restProps = __rest(_a, ["prefixCls", "style", "className"]);

      var prefixCls = getPrefixCls('form', customizePrefixCls);

      var children = _this.renderChildren(prefixCls);

      var itemClassName = (_itemClassName = {}, _defineProperty(_itemClassName, "".concat(prefixCls, "-item"), true), _defineProperty(_itemClassName, "".concat(prefixCls, "-item-with-help"), _this.helpShow), _defineProperty(_itemClassName, "".concat(className), !!className), _itemClassName);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_grid_row__WEBPACK_IMPORTED_MODULE_6__["default"], _extends({
        className: classnames__WEBPACK_IMPORTED_MODULE_3___default()(itemClassName),
        style: style
      }, Object(omit_js__WEBPACK_IMPORTED_MODULE_5__["default"])(restProps, ['id', 'htmlFor', 'label', 'labelAlign', 'labelCol', 'wrapperCol', 'help', 'extra', 'validateStatus', 'hasFeedback', 'required', 'colon']), {
        key: "row"
      }), children);
    };

    return _this;
  }

  _createClass(FormItem, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          children = _this$props.children,
          help = _this$props.help,
          validateStatus = _this$props.validateStatus,
          id = _this$props.id;
      Object(_util_warning__WEBPACK_IMPORTED_MODULE_10__["default"])(this.getControls(children, true).length <= 1 || help !== undefined || validateStatus !== undefined, 'Form.Item', 'Cannot generate `validateStatus` and `help` automatically, ' + 'while there are more than one `getFieldDecorator` in it.');
      Object(_util_warning__WEBPACK_IMPORTED_MODULE_10__["default"])(!id, 'Form.Item', '`id` is deprecated for its label `htmlFor`. Please use `htmlFor` directly.');
    }
  }, {
    key: "getHelpMessage",
    value: function getHelpMessage() {
      var help = this.props.help;

      if (help === undefined && this.getOnlyControl()) {
        var _this$getField = this.getField(),
            errors = _this$getField.errors;

        if (errors) {
          return intersperseSpace(errors.map(function (e, index) {
            var node = null;

            if (react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](e)) {
              node = e;
            } else if (react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](e.message)) {
              node = e.message;
            } // eslint-disable-next-line react/no-array-index-key


            return node ? react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](node, {
              key: index
            }) : e.message;
          }));
        }

        return '';
      }

      return help;
    }
  }, {
    key: "getControls",
    value: function getControls(children, recursively) {
      var controls = [];
      var childrenArray = react__WEBPACK_IMPORTED_MODULE_0__["Children"].toArray(children);

      for (var i = 0; i < childrenArray.length; i++) {
        if (!recursively && controls.length > 0) {
          break;
        }

        var child = childrenArray[i];

        if (child.type && (child.type === FormItem || child.type.displayName === 'FormItem')) {
          continue;
        }

        if (!child.props) {
          continue;
        }

        if (_constants__WEBPACK_IMPORTED_MODULE_12__["FIELD_META_PROP"] in child.props) {
          // And means FIELD_DATA_PROP in child.props, too.
          controls.push(child);
        } else if (child.props.children) {
          controls = controls.concat(this.getControls(child.props.children, recursively));
        }
      }

      return controls;
    }
  }, {
    key: "getOnlyControl",
    value: function getOnlyControl() {
      var child = this.getControls(this.props.children, false)[0];
      return child !== undefined ? child : null;
    }
  }, {
    key: "getChildProp",
    value: function getChildProp(prop) {
      var child = this.getOnlyControl();
      return child && child.props && child.props[prop];
    }
  }, {
    key: "getId",
    value: function getId() {
      return this.getChildProp('id');
    }
  }, {
    key: "getMeta",
    value: function getMeta() {
      return this.getChildProp(_constants__WEBPACK_IMPORTED_MODULE_12__["FIELD_META_PROP"]);
    }
  }, {
    key: "getField",
    value: function getField() {
      return this.getChildProp(_constants__WEBPACK_IMPORTED_MODULE_12__["FIELD_DATA_PROP"]);
    }
  }, {
    key: "getValidateStatus",
    value: function getValidateStatus() {
      var onlyControl = this.getOnlyControl();

      if (!onlyControl) {
        return '';
      }

      var field = this.getField();

      if (field.validating) {
        return 'validating';
      }

      if (field.errors) {
        return 'error';
      }

      var fieldValue = 'value' in field ? field.value : this.getMeta().initialValue;

      if (fieldValue !== undefined && fieldValue !== null && fieldValue !== '') {
        return 'success';
      }

      return '';
    }
  }, {
    key: "isRequired",
    value: function isRequired() {
      var required = this.props.required;

      if (required !== undefined) {
        return required;
      }

      if (this.getOnlyControl()) {
        var meta = this.getMeta() || {};
        var validate = meta.validate || [];
        return validate.filter(function (item) {
          return !!item.rules;
        }).some(function (item) {
          return item.rules.some(function (rule) {
            return rule.required;
          });
        });
      }

      return false;
    }
  }, {
    key: "renderHelp",
    value: function renderHelp(prefixCls) {
      var help = this.getHelpMessage();
      var children = help ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-explain"),
        key: "help"
      }, help) : null;

      if (children) {
        this.helpShow = !!children;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_animate__WEBPACK_IMPORTED_MODULE_4__["default"], {
        transitionName: "show-help",
        component: "",
        transitionAppear: true,
        key: "help",
        onEnd: this.onHelpAnimEnd
      }, children);
    }
  }, {
    key: "renderExtra",
    value: function renderExtra(prefixCls) {
      var extra = this.props.extra;
      return extra ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-extra")
      }, extra) : null;
    }
  }, {
    key: "renderValidateWrapper",
    value: function renderValidateWrapper(prefixCls, c1, c2, c3) {
      var props = this.props;
      var onlyControl = this.getOnlyControl;
      var validateStatus = props.validateStatus === undefined && onlyControl ? this.getValidateStatus() : props.validateStatus;
      var classes = "".concat(prefixCls, "-item-control");

      if (validateStatus) {
        classes = classnames__WEBPACK_IMPORTED_MODULE_3___default()("".concat(prefixCls, "-item-control"), {
          'has-feedback': props.hasFeedback || validateStatus === 'validating',
          'has-success': validateStatus === 'success',
          'has-warning': validateStatus === 'warning',
          'has-error': validateStatus === 'error',
          'is-validating': validateStatus === 'validating'
        });
      }

      var iconType = '';

      switch (validateStatus) {
        case 'success':
          iconType = 'check-circle';
          break;

        case 'warning':
          iconType = 'exclamation-circle';
          break;

        case 'error':
          iconType = 'close-circle';
          break;

        case 'validating':
          iconType = 'loading';
          break;

        default:
          iconType = '';
          break;
      }

      var icon = props.hasFeedback && iconType ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-item-children-icon")
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_8__["default"], {
        type: iconType,
        theme: iconType === 'loading' ? 'outlined' : 'filled'
      })) : null;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: classes
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-item-children")
      }, c1, icon), c2, c3);
    }
  }, {
    key: "renderWrapper",
    value: function renderWrapper(prefixCls, children) {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_context__WEBPACK_IMPORTED_MODULE_13__["default"].Consumer, {
        key: "wrapper"
      }, function (_ref2) {
        var contextWrapperCol = _ref2.wrapperCol,
            vertical = _ref2.vertical;
        var wrapperCol = _this2.props.wrapperCol;
        var mergedWrapperCol = ('wrapperCol' in _this2.props ? wrapperCol : contextWrapperCol) || {};
        var className = classnames__WEBPACK_IMPORTED_MODULE_3___default()("".concat(prefixCls, "-item-control-wrapper"), mergedWrapperCol.className); // No pass FormContext since it's useless

        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_context__WEBPACK_IMPORTED_MODULE_13__["default"].Provider, {
          value: {
            vertical: vertical
          }
        }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_grid_col__WEBPACK_IMPORTED_MODULE_7__["default"], _extends({}, mergedWrapperCol, {
          className: className
        }), children));
      });
    }
  }, {
    key: "renderLabel",
    value: function renderLabel(prefixCls) {
      var _this3 = this;

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_context__WEBPACK_IMPORTED_MODULE_13__["default"].Consumer, {
        key: "label"
      }, function (_ref3) {
        var _classNames;

        var vertical = _ref3.vertical,
            contextLabelAlign = _ref3.labelAlign,
            contextLabelCol = _ref3.labelCol,
            contextColon = _ref3.colon;
        var _this3$props = _this3.props,
            label = _this3$props.label,
            labelCol = _this3$props.labelCol,
            labelAlign = _this3$props.labelAlign,
            colon = _this3$props.colon,
            id = _this3$props.id,
            htmlFor = _this3$props.htmlFor;

        var required = _this3.isRequired();

        var mergedLabelCol = ('labelCol' in _this3.props ? labelCol : contextLabelCol) || {};
        var mergedLabelAlign = 'labelAlign' in _this3.props ? labelAlign : contextLabelAlign;
        var labelClsBasic = "".concat(prefixCls, "-item-label");
        var labelColClassName = classnames__WEBPACK_IMPORTED_MODULE_3___default()(labelClsBasic, mergedLabelAlign === 'left' && "".concat(labelClsBasic, "-left"), mergedLabelCol.className);
        var labelChildren = label; // Keep label is original where there should have no colon

        var computedColon = colon === true || contextColon !== false && colon !== false;
        var haveColon = computedColon && !vertical; // Remove duplicated user input colon

        if (haveColon && typeof label === 'string' && label.trim() !== '') {
          labelChildren = label.replace(/[：:]\s*$/, '');
        }

        var labelClassName = classnames__WEBPACK_IMPORTED_MODULE_3___default()((_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-item-required"), required), _defineProperty(_classNames, "".concat(prefixCls, "-item-no-colon"), !computedColon), _classNames));
        return label ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_grid_col__WEBPACK_IMPORTED_MODULE_7__["default"], _extends({}, mergedLabelCol, {
          className: labelColClassName
        }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", {
          htmlFor: htmlFor || id || _this3.getId(),
          className: labelClassName,
          title: typeof label === 'string' ? label : '',
          onClick: _this3.onLabelClick
        }, labelChildren)) : null;
      });
    }
  }, {
    key: "renderChildren",
    value: function renderChildren(prefixCls) {
      var children = this.props.children;
      return [this.renderLabel(prefixCls), this.renderWrapper(prefixCls, this.renderValidateWrapper(prefixCls, children, this.renderHelp(prefixCls), this.renderExtra(prefixCls)))];
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_9__["ConfigConsumer"], null, this.renderFormItem);
    }
  }]);

  return FormItem;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


FormItem.defaultProps = {
  hasFeedback: false
};
FormItem.propTypes = {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_2__["string"],
  label: prop_types__WEBPACK_IMPORTED_MODULE_2__["oneOfType"]([prop_types__WEBPACK_IMPORTED_MODULE_2__["string"], prop_types__WEBPACK_IMPORTED_MODULE_2__["node"]]),
  labelCol: prop_types__WEBPACK_IMPORTED_MODULE_2__["object"],
  help: prop_types__WEBPACK_IMPORTED_MODULE_2__["oneOfType"]([prop_types__WEBPACK_IMPORTED_MODULE_2__["node"], prop_types__WEBPACK_IMPORTED_MODULE_2__["bool"]]),
  validateStatus: prop_types__WEBPACK_IMPORTED_MODULE_2__["oneOf"](ValidateStatuses),
  hasFeedback: prop_types__WEBPACK_IMPORTED_MODULE_2__["bool"],
  wrapperCol: prop_types__WEBPACK_IMPORTED_MODULE_2__["object"],
  className: prop_types__WEBPACK_IMPORTED_MODULE_2__["string"],
  id: prop_types__WEBPACK_IMPORTED_MODULE_2__["string"],
  children: prop_types__WEBPACK_IMPORTED_MODULE_2__["node"],
  colon: prop_types__WEBPACK_IMPORTED_MODULE_2__["bool"]
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9mb3JtL0Zvcm1JdGVtLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9mb3JtL0Zvcm1JdGVtLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgKiBhcyBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xuaW1wb3J0ICogYXMgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgQW5pbWF0ZSBmcm9tICdyYy1hbmltYXRlJztcbmltcG9ydCBvbWl0IGZyb20gJ29taXQuanMnO1xuaW1wb3J0IFJvdyBmcm9tICcuLi9ncmlkL3Jvdyc7XG5pbXBvcnQgQ29sIGZyb20gJy4uL2dyaWQvY29sJztcbmltcG9ydCBJY29uIGZyb20gJy4uL2ljb24nO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IHdhcm5pbmcgZnJvbSAnLi4vX3V0aWwvd2FybmluZyc7XG5pbXBvcnQgeyB0dXBsZSB9IGZyb20gJy4uL191dGlsL3R5cGUnO1xuaW1wb3J0IHsgRklFTERfTUVUQV9QUk9QLCBGSUVMRF9EQVRBX1BST1AgfSBmcm9tICcuL2NvbnN0YW50cyc7XG5pbXBvcnQgRm9ybUNvbnRleHQgZnJvbSAnLi9jb250ZXh0JztcbmNvbnN0IFZhbGlkYXRlU3RhdHVzZXMgPSB0dXBsZSgnc3VjY2VzcycsICd3YXJuaW5nJywgJ2Vycm9yJywgJ3ZhbGlkYXRpbmcnLCAnJyk7XG5jb25zdCBGb3JtTGFiZWxBbGlnbnMgPSB0dXBsZSgnbGVmdCcsICdyaWdodCcpO1xuZnVuY3Rpb24gaW50ZXJzcGVyc2VTcGFjZShsaXN0KSB7XG4gICAgcmV0dXJuIGxpc3QucmVkdWNlKChjdXJyZW50LCBpdGVtKSA9PiBbLi4uY3VycmVudCwgJyAnLCBpdGVtXSwgW10pLnNsaWNlKDEpO1xufVxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRm9ybUl0ZW0gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlciguLi5hcmd1bWVudHMpO1xuICAgICAgICB0aGlzLmhlbHBTaG93ID0gZmFsc2U7XG4gICAgICAgIC8vIFJlc29sdmUgZHVwbGljYXRlZCBpZHMgYnVnIGJldHdlZW4gZGlmZmVyZW50IGZvcm1zXG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzczNTFcbiAgICAgICAgdGhpcy5vbkxhYmVsQ2xpY2sgPSAoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBpZCA9IHRoaXMucHJvcHMuaWQgfHwgdGhpcy5nZXRJZCgpO1xuICAgICAgICAgICAgaWYgKCFpZCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IGZvcm1JdGVtTm9kZSA9IFJlYWN0RE9NLmZpbmRET01Ob2RlKHRoaXMpO1xuICAgICAgICAgICAgY29uc3QgY29udHJvbCA9IGZvcm1JdGVtTm9kZS5xdWVyeVNlbGVjdG9yKGBbaWQ9XCIke2lkfVwiXWApO1xuICAgICAgICAgICAgaWYgKGNvbnRyb2wgJiYgY29udHJvbC5mb2N1cykge1xuICAgICAgICAgICAgICAgIGNvbnRyb2wuZm9jdXMoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5vbkhlbHBBbmltRW5kID0gKF9rZXksIGhlbHBTaG93KSA9PiB7XG4gICAgICAgICAgICB0aGlzLmhlbHBTaG93ID0gaGVscFNob3c7XG4gICAgICAgICAgICBpZiAoIWhlbHBTaG93KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7fSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyRm9ybUl0ZW0gPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgX2EgPSB0aGlzLnByb3BzLCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBzdHlsZSwgY2xhc3NOYW1lIH0gPSBfYSwgcmVzdFByb3BzID0gX19yZXN0KF9hLCBbXCJwcmVmaXhDbHNcIiwgXCJzdHlsZVwiLCBcImNsYXNzTmFtZVwiXSk7XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ2Zvcm0nLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3QgY2hpbGRyZW4gPSB0aGlzLnJlbmRlckNoaWxkcmVuKHByZWZpeENscyk7XG4gICAgICAgICAgICBjb25zdCBpdGVtQ2xhc3NOYW1lID0ge1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWl0ZW1gXTogdHJ1ZSxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1pdGVtLXdpdGgtaGVscGBdOiB0aGlzLmhlbHBTaG93LFxuICAgICAgICAgICAgICAgIFtgJHtjbGFzc05hbWV9YF06ICEhY2xhc3NOYW1lLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIHJldHVybiAoPFJvdyBjbGFzc05hbWU9e2NsYXNzTmFtZXMoaXRlbUNsYXNzTmFtZSl9IHN0eWxlPXtzdHlsZX0gey4uLm9taXQocmVzdFByb3BzLCBbXG4gICAgICAgICAgICAgICAgJ2lkJyxcbiAgICAgICAgICAgICAgICAnaHRtbEZvcicsXG4gICAgICAgICAgICAgICAgJ2xhYmVsJyxcbiAgICAgICAgICAgICAgICAnbGFiZWxBbGlnbicsXG4gICAgICAgICAgICAgICAgJ2xhYmVsQ29sJyxcbiAgICAgICAgICAgICAgICAnd3JhcHBlckNvbCcsXG4gICAgICAgICAgICAgICAgJ2hlbHAnLFxuICAgICAgICAgICAgICAgICdleHRyYScsXG4gICAgICAgICAgICAgICAgJ3ZhbGlkYXRlU3RhdHVzJyxcbiAgICAgICAgICAgICAgICAnaGFzRmVlZGJhY2snLFxuICAgICAgICAgICAgICAgICdyZXF1aXJlZCcsXG4gICAgICAgICAgICAgICAgJ2NvbG9uJyxcbiAgICAgICAgICAgIF0pfSBrZXk9XCJyb3dcIj5cbiAgICAgICAge2NoaWxkcmVufVxuICAgICAgPC9Sb3c+KTtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIGNvbnN0IHsgY2hpbGRyZW4sIGhlbHAsIHZhbGlkYXRlU3RhdHVzLCBpZCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgd2FybmluZyh0aGlzLmdldENvbnRyb2xzKGNoaWxkcmVuLCB0cnVlKS5sZW5ndGggPD0gMSB8fFxuICAgICAgICAgICAgaGVscCAhPT0gdW5kZWZpbmVkIHx8XG4gICAgICAgICAgICB2YWxpZGF0ZVN0YXR1cyAhPT0gdW5kZWZpbmVkLCAnRm9ybS5JdGVtJywgJ0Nhbm5vdCBnZW5lcmF0ZSBgdmFsaWRhdGVTdGF0dXNgIGFuZCBgaGVscGAgYXV0b21hdGljYWxseSwgJyArXG4gICAgICAgICAgICAnd2hpbGUgdGhlcmUgYXJlIG1vcmUgdGhhbiBvbmUgYGdldEZpZWxkRGVjb3JhdG9yYCBpbiBpdC4nKTtcbiAgICAgICAgd2FybmluZyghaWQsICdGb3JtLkl0ZW0nLCAnYGlkYCBpcyBkZXByZWNhdGVkIGZvciBpdHMgbGFiZWwgYGh0bWxGb3JgLiBQbGVhc2UgdXNlIGBodG1sRm9yYCBkaXJlY3RseS4nKTtcbiAgICB9XG4gICAgZ2V0SGVscE1lc3NhZ2UoKSB7XG4gICAgICAgIGNvbnN0IHsgaGVscCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKGhlbHAgPT09IHVuZGVmaW5lZCAmJiB0aGlzLmdldE9ubHlDb250cm9sKCkpIHtcbiAgICAgICAgICAgIGNvbnN0IHsgZXJyb3JzIH0gPSB0aGlzLmdldEZpZWxkKCk7XG4gICAgICAgICAgICBpZiAoZXJyb3JzKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGludGVyc3BlcnNlU3BhY2UoZXJyb3JzLm1hcCgoZSwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IG5vZGUgPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICBpZiAoUmVhY3QuaXNWYWxpZEVsZW1lbnQoZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vZGUgPSBlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2UgaWYgKFJlYWN0LmlzVmFsaWRFbGVtZW50KGUubWVzc2FnZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vZGUgPSBlLm1lc3NhZ2U7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHJlYWN0L25vLWFycmF5LWluZGV4LWtleVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbm9kZSA/IFJlYWN0LmNsb25lRWxlbWVudChub2RlLCB7IGtleTogaW5kZXggfSkgOiBlLm1lc3NhZ2U7XG4gICAgICAgICAgICAgICAgfSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBoZWxwO1xuICAgIH1cbiAgICBnZXRDb250cm9scyhjaGlsZHJlbiwgcmVjdXJzaXZlbHkpIHtcbiAgICAgICAgbGV0IGNvbnRyb2xzID0gW107XG4gICAgICAgIGNvbnN0IGNoaWxkcmVuQXJyYXkgPSBSZWFjdC5DaGlsZHJlbi50b0FycmF5KGNoaWxkcmVuKTtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjaGlsZHJlbkFycmF5Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoIXJlY3Vyc2l2ZWx5ICYmIGNvbnRyb2xzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IGNoaWxkID0gY2hpbGRyZW5BcnJheVtpXTtcbiAgICAgICAgICAgIGlmIChjaGlsZC50eXBlICYmXG4gICAgICAgICAgICAgICAgKGNoaWxkLnR5cGUgPT09IEZvcm1JdGVtIHx8IGNoaWxkLnR5cGUuZGlzcGxheU5hbWUgPT09ICdGb3JtSXRlbScpKSB7XG4gICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoIWNoaWxkLnByb3BzKSB7XG4gICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoRklFTERfTUVUQV9QUk9QIGluIGNoaWxkLnByb3BzKSB7XG4gICAgICAgICAgICAgICAgLy8gQW5kIG1lYW5zIEZJRUxEX0RBVEFfUFJPUCBpbiBjaGlsZC5wcm9wcywgdG9vLlxuICAgICAgICAgICAgICAgIGNvbnRyb2xzLnB1c2goY2hpbGQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAoY2hpbGQucHJvcHMuY2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgICBjb250cm9scyA9IGNvbnRyb2xzLmNvbmNhdCh0aGlzLmdldENvbnRyb2xzKGNoaWxkLnByb3BzLmNoaWxkcmVuLCByZWN1cnNpdmVseSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBjb250cm9scztcbiAgICB9XG4gICAgZ2V0T25seUNvbnRyb2woKSB7XG4gICAgICAgIGNvbnN0IGNoaWxkID0gdGhpcy5nZXRDb250cm9scyh0aGlzLnByb3BzLmNoaWxkcmVuLCBmYWxzZSlbMF07XG4gICAgICAgIHJldHVybiBjaGlsZCAhPT0gdW5kZWZpbmVkID8gY2hpbGQgOiBudWxsO1xuICAgIH1cbiAgICBnZXRDaGlsZFByb3AocHJvcCkge1xuICAgICAgICBjb25zdCBjaGlsZCA9IHRoaXMuZ2V0T25seUNvbnRyb2woKTtcbiAgICAgICAgcmV0dXJuIGNoaWxkICYmIGNoaWxkLnByb3BzICYmIGNoaWxkLnByb3BzW3Byb3BdO1xuICAgIH1cbiAgICBnZXRJZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0Q2hpbGRQcm9wKCdpZCcpO1xuICAgIH1cbiAgICBnZXRNZXRhKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5nZXRDaGlsZFByb3AoRklFTERfTUVUQV9QUk9QKTtcbiAgICB9XG4gICAgZ2V0RmllbGQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldENoaWxkUHJvcChGSUVMRF9EQVRBX1BST1ApO1xuICAgIH1cbiAgICBnZXRWYWxpZGF0ZVN0YXR1cygpIHtcbiAgICAgICAgY29uc3Qgb25seUNvbnRyb2wgPSB0aGlzLmdldE9ubHlDb250cm9sKCk7XG4gICAgICAgIGlmICghb25seUNvbnRyb2wpIHtcbiAgICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBmaWVsZCA9IHRoaXMuZ2V0RmllbGQoKTtcbiAgICAgICAgaWYgKGZpZWxkLnZhbGlkYXRpbmcpIHtcbiAgICAgICAgICAgIHJldHVybiAndmFsaWRhdGluZyc7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGZpZWxkLmVycm9ycykge1xuICAgICAgICAgICAgcmV0dXJuICdlcnJvcic7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgZmllbGRWYWx1ZSA9ICd2YWx1ZScgaW4gZmllbGQgPyBmaWVsZC52YWx1ZSA6IHRoaXMuZ2V0TWV0YSgpLmluaXRpYWxWYWx1ZTtcbiAgICAgICAgaWYgKGZpZWxkVmFsdWUgIT09IHVuZGVmaW5lZCAmJiBmaWVsZFZhbHVlICE9PSBudWxsICYmIGZpZWxkVmFsdWUgIT09ICcnKSB7XG4gICAgICAgICAgICByZXR1cm4gJ3N1Y2Nlc3MnO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAnJztcbiAgICB9XG4gICAgaXNSZXF1aXJlZCgpIHtcbiAgICAgICAgY29uc3QgeyByZXF1aXJlZCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKHJlcXVpcmVkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybiByZXF1aXJlZDtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5nZXRPbmx5Q29udHJvbCgpKSB7XG4gICAgICAgICAgICBjb25zdCBtZXRhID0gdGhpcy5nZXRNZXRhKCkgfHwge307XG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0ZSA9IG1ldGEudmFsaWRhdGUgfHwgW107XG4gICAgICAgICAgICByZXR1cm4gdmFsaWRhdGVcbiAgICAgICAgICAgICAgICAuZmlsdGVyKChpdGVtKSA9PiAhIWl0ZW0ucnVsZXMpXG4gICAgICAgICAgICAgICAgLnNvbWUoKGl0ZW0pID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gaXRlbS5ydWxlcy5zb21lKChydWxlKSA9PiBydWxlLnJlcXVpcmVkKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgcmVuZGVySGVscChwcmVmaXhDbHMpIHtcbiAgICAgICAgY29uc3QgaGVscCA9IHRoaXMuZ2V0SGVscE1lc3NhZ2UoKTtcbiAgICAgICAgY29uc3QgY2hpbGRyZW4gPSBoZWxwID8gKDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWV4cGxhaW5gfSBrZXk9XCJoZWxwXCI+XG4gICAgICAgIHtoZWxwfVxuICAgICAgPC9kaXY+KSA6IG51bGw7XG4gICAgICAgIGlmIChjaGlsZHJlbikge1xuICAgICAgICAgICAgdGhpcy5oZWxwU2hvdyA9ICEhY2hpbGRyZW47XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuICg8QW5pbWF0ZSB0cmFuc2l0aW9uTmFtZT1cInNob3ctaGVscFwiIGNvbXBvbmVudD1cIlwiIHRyYW5zaXRpb25BcHBlYXIga2V5PVwiaGVscFwiIG9uRW5kPXt0aGlzLm9uSGVscEFuaW1FbmR9PlxuICAgICAgICB7Y2hpbGRyZW59XG4gICAgICA8L0FuaW1hdGU+KTtcbiAgICB9XG4gICAgcmVuZGVyRXh0cmEocHJlZml4Q2xzKSB7XG4gICAgICAgIGNvbnN0IHsgZXh0cmEgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHJldHVybiBleHRyYSA/IDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWV4dHJhYH0+e2V4dHJhfTwvZGl2PiA6IG51bGw7XG4gICAgfVxuICAgIHJlbmRlclZhbGlkYXRlV3JhcHBlcihwcmVmaXhDbHMsIGMxLCBjMiwgYzMpIHtcbiAgICAgICAgY29uc3QgeyBwcm9wcyB9ID0gdGhpcztcbiAgICAgICAgY29uc3Qgb25seUNvbnRyb2wgPSB0aGlzLmdldE9ubHlDb250cm9sO1xuICAgICAgICBjb25zdCB2YWxpZGF0ZVN0YXR1cyA9IHByb3BzLnZhbGlkYXRlU3RhdHVzID09PSB1bmRlZmluZWQgJiYgb25seUNvbnRyb2xcbiAgICAgICAgICAgID8gdGhpcy5nZXRWYWxpZGF0ZVN0YXR1cygpXG4gICAgICAgICAgICA6IHByb3BzLnZhbGlkYXRlU3RhdHVzO1xuICAgICAgICBsZXQgY2xhc3NlcyA9IGAke3ByZWZpeENsc30taXRlbS1jb250cm9sYDtcbiAgICAgICAgaWYgKHZhbGlkYXRlU3RhdHVzKSB7XG4gICAgICAgICAgICBjbGFzc2VzID0gY2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LWl0ZW0tY29udHJvbGAsIHtcbiAgICAgICAgICAgICAgICAnaGFzLWZlZWRiYWNrJzogcHJvcHMuaGFzRmVlZGJhY2sgfHwgdmFsaWRhdGVTdGF0dXMgPT09ICd2YWxpZGF0aW5nJyxcbiAgICAgICAgICAgICAgICAnaGFzLXN1Y2Nlc3MnOiB2YWxpZGF0ZVN0YXR1cyA9PT0gJ3N1Y2Nlc3MnLFxuICAgICAgICAgICAgICAgICdoYXMtd2FybmluZyc6IHZhbGlkYXRlU3RhdHVzID09PSAnd2FybmluZycsXG4gICAgICAgICAgICAgICAgJ2hhcy1lcnJvcic6IHZhbGlkYXRlU3RhdHVzID09PSAnZXJyb3InLFxuICAgICAgICAgICAgICAgICdpcy12YWxpZGF0aW5nJzogdmFsaWRhdGVTdGF0dXMgPT09ICd2YWxpZGF0aW5nJyxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGxldCBpY29uVHlwZSA9ICcnO1xuICAgICAgICBzd2l0Y2ggKHZhbGlkYXRlU3RhdHVzKSB7XG4gICAgICAgICAgICBjYXNlICdzdWNjZXNzJzpcbiAgICAgICAgICAgICAgICBpY29uVHlwZSA9ICdjaGVjay1jaXJjbGUnO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAnd2FybmluZyc6XG4gICAgICAgICAgICAgICAgaWNvblR5cGUgPSAnZXhjbGFtYXRpb24tY2lyY2xlJztcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ2Vycm9yJzpcbiAgICAgICAgICAgICAgICBpY29uVHlwZSA9ICdjbG9zZS1jaXJjbGUnO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAndmFsaWRhdGluZyc6XG4gICAgICAgICAgICAgICAgaWNvblR5cGUgPSAnbG9hZGluZyc7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIGljb25UeXBlID0gJyc7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgaWNvbiA9IHByb3BzLmhhc0ZlZWRiYWNrICYmIGljb25UeXBlID8gKDxzcGFuIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1pdGVtLWNoaWxkcmVuLWljb25gfT5cbiAgICAgICAgICA8SWNvbiB0eXBlPXtpY29uVHlwZX0gdGhlbWU9e2ljb25UeXBlID09PSAnbG9hZGluZycgPyAnb3V0bGluZWQnIDogJ2ZpbGxlZCd9Lz5cbiAgICAgICAgPC9zcGFuPikgOiBudWxsO1xuICAgICAgICByZXR1cm4gKDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzfT5cbiAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWl0ZW0tY2hpbGRyZW5gfT5cbiAgICAgICAgICB7YzF9XG4gICAgICAgICAge2ljb259XG4gICAgICAgIDwvc3Bhbj5cbiAgICAgICAge2MyfVxuICAgICAgICB7YzN9XG4gICAgICA8L2Rpdj4pO1xuICAgIH1cbiAgICByZW5kZXJXcmFwcGVyKHByZWZpeENscywgY2hpbGRyZW4pIHtcbiAgICAgICAgcmV0dXJuICg8Rm9ybUNvbnRleHQuQ29uc3VtZXIga2V5PVwid3JhcHBlclwiPlxuICAgICAgICB7KHsgd3JhcHBlckNvbDogY29udGV4dFdyYXBwZXJDb2wsIHZlcnRpY2FsIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgd3JhcHBlckNvbCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IG1lcmdlZFdyYXBwZXJDb2wgPSAoJ3dyYXBwZXJDb2wnIGluIHRoaXMucHJvcHMgPyB3cmFwcGVyQ29sIDogY29udGV4dFdyYXBwZXJDb2wpIHx8IHt9O1xuICAgICAgICAgICAgY29uc3QgY2xhc3NOYW1lID0gY2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LWl0ZW0tY29udHJvbC13cmFwcGVyYCwgbWVyZ2VkV3JhcHBlckNvbC5jbGFzc05hbWUpO1xuICAgICAgICAgICAgLy8gTm8gcGFzcyBGb3JtQ29udGV4dCBzaW5jZSBpdCdzIHVzZWxlc3NcbiAgICAgICAgICAgIHJldHVybiAoPEZvcm1Db250ZXh0LlByb3ZpZGVyIHZhbHVlPXt7IHZlcnRpY2FsIH19PlxuICAgICAgICAgICAgICA8Q29sIHsuLi5tZXJnZWRXcmFwcGVyQ29sfSBjbGFzc05hbWU9e2NsYXNzTmFtZX0+XG4gICAgICAgICAgICAgICAge2NoaWxkcmVufVxuICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgIDwvRm9ybUNvbnRleHQuUHJvdmlkZXI+KTtcbiAgICAgICAgfX1cbiAgICAgIDwvRm9ybUNvbnRleHQuQ29uc3VtZXI+KTtcbiAgICB9XG4gICAgcmVuZGVyTGFiZWwocHJlZml4Q2xzKSB7XG4gICAgICAgIHJldHVybiAoPEZvcm1Db250ZXh0LkNvbnN1bWVyIGtleT1cImxhYmVsXCI+XG4gICAgICAgIHsoeyB2ZXJ0aWNhbCwgbGFiZWxBbGlnbjogY29udGV4dExhYmVsQWxpZ24sIGxhYmVsQ29sOiBjb250ZXh0TGFiZWxDb2wsIGNvbG9uOiBjb250ZXh0Q29sb24sIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgbGFiZWwsIGxhYmVsQ29sLCBsYWJlbEFsaWduLCBjb2xvbiwgaWQsIGh0bWxGb3IgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCByZXF1aXJlZCA9IHRoaXMuaXNSZXF1aXJlZCgpO1xuICAgICAgICAgICAgY29uc3QgbWVyZ2VkTGFiZWxDb2wgPSAoJ2xhYmVsQ29sJyBpbiB0aGlzLnByb3BzID8gbGFiZWxDb2wgOiBjb250ZXh0TGFiZWxDb2wpIHx8IHt9O1xuICAgICAgICAgICAgY29uc3QgbWVyZ2VkTGFiZWxBbGlnbiA9ICdsYWJlbEFsaWduJyBpbiB0aGlzLnByb3BzID8gbGFiZWxBbGlnbiA6IGNvbnRleHRMYWJlbEFsaWduO1xuICAgICAgICAgICAgY29uc3QgbGFiZWxDbHNCYXNpYyA9IGAke3ByZWZpeENsc30taXRlbS1sYWJlbGA7XG4gICAgICAgICAgICBjb25zdCBsYWJlbENvbENsYXNzTmFtZSA9IGNsYXNzTmFtZXMobGFiZWxDbHNCYXNpYywgbWVyZ2VkTGFiZWxBbGlnbiA9PT0gJ2xlZnQnICYmIGAke2xhYmVsQ2xzQmFzaWN9LWxlZnRgLCBtZXJnZWRMYWJlbENvbC5jbGFzc05hbWUpO1xuICAgICAgICAgICAgbGV0IGxhYmVsQ2hpbGRyZW4gPSBsYWJlbDtcbiAgICAgICAgICAgIC8vIEtlZXAgbGFiZWwgaXMgb3JpZ2luYWwgd2hlcmUgdGhlcmUgc2hvdWxkIGhhdmUgbm8gY29sb25cbiAgICAgICAgICAgIGNvbnN0IGNvbXB1dGVkQ29sb24gPSBjb2xvbiA9PT0gdHJ1ZSB8fCAoY29udGV4dENvbG9uICE9PSBmYWxzZSAmJiBjb2xvbiAhPT0gZmFsc2UpO1xuICAgICAgICAgICAgY29uc3QgaGF2ZUNvbG9uID0gY29tcHV0ZWRDb2xvbiAmJiAhdmVydGljYWw7XG4gICAgICAgICAgICAvLyBSZW1vdmUgZHVwbGljYXRlZCB1c2VyIGlucHV0IGNvbG9uXG4gICAgICAgICAgICBpZiAoaGF2ZUNvbG9uICYmIHR5cGVvZiBsYWJlbCA9PT0gJ3N0cmluZycgJiYgbGFiZWwudHJpbSgpICE9PSAnJykge1xuICAgICAgICAgICAgICAgIGxhYmVsQ2hpbGRyZW4gPSBsYWJlbC5yZXBsYWNlKC9b77yaOl1cXHMqJC8sICcnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IGxhYmVsQ2xhc3NOYW1lID0gY2xhc3NOYW1lcyh7XG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30taXRlbS1yZXF1aXJlZGBdOiByZXF1aXJlZCxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1pdGVtLW5vLWNvbG9uYF06ICFjb21wdXRlZENvbG9uLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gbGFiZWwgPyAoPENvbCB7Li4ubWVyZ2VkTGFiZWxDb2x9IGNsYXNzTmFtZT17bGFiZWxDb2xDbGFzc05hbWV9PlxuICAgICAgICAgICAgICA8bGFiZWwgaHRtbEZvcj17aHRtbEZvciB8fCBpZCB8fCB0aGlzLmdldElkKCl9IGNsYXNzTmFtZT17bGFiZWxDbGFzc05hbWV9IHRpdGxlPXt0eXBlb2YgbGFiZWwgPT09ICdzdHJpbmcnID8gbGFiZWwgOiAnJ30gb25DbGljaz17dGhpcy5vbkxhYmVsQ2xpY2t9PlxuICAgICAgICAgICAgICAgIHtsYWJlbENoaWxkcmVufVxuICAgICAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICAgICAgPC9Db2w+KSA6IG51bGw7XG4gICAgICAgIH19XG4gICAgICA8L0Zvcm1Db250ZXh0LkNvbnN1bWVyPik7XG4gICAgfVxuICAgIHJlbmRlckNoaWxkcmVuKHByZWZpeENscykge1xuICAgICAgICBjb25zdCB7IGNoaWxkcmVuIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgdGhpcy5yZW5kZXJMYWJlbChwcmVmaXhDbHMpLFxuICAgICAgICAgICAgdGhpcy5yZW5kZXJXcmFwcGVyKHByZWZpeENscywgdGhpcy5yZW5kZXJWYWxpZGF0ZVdyYXBwZXIocHJlZml4Q2xzLCBjaGlsZHJlbiwgdGhpcy5yZW5kZXJIZWxwKHByZWZpeENscyksIHRoaXMucmVuZGVyRXh0cmEocHJlZml4Q2xzKSkpLFxuICAgICAgICBdO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyRm9ybUl0ZW19PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuRm9ybUl0ZW0uZGVmYXVsdFByb3BzID0ge1xuICAgIGhhc0ZlZWRiYWNrOiBmYWxzZSxcbn07XG5Gb3JtSXRlbS5wcm9wVHlwZXMgPSB7XG4gICAgcHJlZml4Q2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGxhYmVsOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMubm9kZV0pLFxuICAgIGxhYmVsQ29sOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGhlbHA6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5ub2RlLCBQcm9wVHlwZXMuYm9vbF0pLFxuICAgIHZhbGlkYXRlU3RhdHVzOiBQcm9wVHlwZXMub25lT2YoVmFsaWRhdGVTdGF0dXNlcyksXG4gICAgaGFzRmVlZGJhY2s6IFByb3BUeXBlcy5ib29sLFxuICAgIHdyYXBwZXJDb2w6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGlkOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGNoaWxkcmVuOiBQcm9wVHlwZXMubm9kZSxcbiAgICBjb2xvbjogUHJvcFR5cGVzLmJvb2wsXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFLQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBYUE7QUFiQTtBQVRBO0FBQ0E7QUF2QkE7QUFnREE7QUFDQTs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFJQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFOQTtBQUNBO0FBQ0E7QUFPQTtBQUFBO0FBQUE7QUFUQTtBQVdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQUE7QUFBQTtBQUhBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7OztBQUNBO0FBQUE7QUFFQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFmQTtBQUNBO0FBZ0JBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFPQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFQQTtBQWFBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBckJBO0FBMkJBOzs7QUFDQTtBQUFBO0FBRUE7QUFJQTs7O0FBQ0E7QUFDQTtBQUNBOzs7O0FBOVFBO0FBQ0E7QUFEQTtBQWdSQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFYQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/form/FormItem.js
