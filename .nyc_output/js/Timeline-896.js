__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Timeline; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _TimelineItem__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TimelineItem */ "./node_modules/antd/es/timeline/TimelineItem.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};







var Timeline =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Timeline, _React$Component);

  function Timeline() {
    var _this;

    _classCallCheck(this, Timeline);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Timeline).apply(this, arguments));

    _this.renderTimeline = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          _a$pending = _a.pending,
          pending = _a$pending === void 0 ? null : _a$pending,
          pendingDot = _a.pendingDot,
          children = _a.children,
          className = _a.className,
          reverse = _a.reverse,
          mode = _a.mode,
          restProps = __rest(_a, ["prefixCls", "pending", "pendingDot", "children", "className", "reverse", "mode"]);

      var prefixCls = getPrefixCls('timeline', customizePrefixCls);
      var pendingNode = typeof pending === 'boolean' ? null : pending;
      var classString = classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-pending"), !!pending), _defineProperty(_classNames, "".concat(prefixCls, "-reverse"), !!reverse), _defineProperty(_classNames, "".concat(prefixCls, "-").concat(mode), !!mode), _classNames), className);
      var pendingItem = pending ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_TimelineItem__WEBPACK_IMPORTED_MODULE_2__["default"], {
        pending: !!pending,
        dot: pendingDot || react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_3__["default"], {
          type: "loading"
        })
      }, pendingNode) : null;
      var timeLineItems = reverse ? [pendingItem].concat(_toConsumableArray(react__WEBPACK_IMPORTED_MODULE_0__["Children"].toArray(children).reverse())) : [].concat(_toConsumableArray(react__WEBPACK_IMPORTED_MODULE_0__["Children"].toArray(children)), [pendingItem]);

      var getPositionCls = function getPositionCls(ele, idx) {
        if (mode === 'alternate') {
          if (ele.props.position === 'right') return "".concat(prefixCls, "-item-right");
          if (ele.props.position === 'left') return "".concat(prefixCls, "-item-left");
          return idx % 2 === 0 ? "".concat(prefixCls, "-item-left") : "".concat(prefixCls, "-item-right");
        }

        if (mode === 'left') return "".concat(prefixCls, "-item-left");
        if (mode === 'right') return "".concat(prefixCls, "-item-right");
        if (ele.props.position === 'right') return "".concat(prefixCls, "-item-right");
        return '';
      }; // Remove falsy items


      var truthyItems = timeLineItems.filter(function (item) {
        return !!item;
      });
      var itemsCount = react__WEBPACK_IMPORTED_MODULE_0__["Children"].count(truthyItems);
      var lastCls = "".concat(prefixCls, "-item-last");
      var items = react__WEBPACK_IMPORTED_MODULE_0__["Children"].map(truthyItems, function (ele, idx) {
        var pendingClass = idx === itemsCount - 2 ? lastCls : '';
        var readyClass = idx === itemsCount - 1 ? lastCls : '';
        return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](ele, {
          className: classnames__WEBPACK_IMPORTED_MODULE_1___default()([ele.props.className, !reverse && !!pending ? pendingClass : readyClass, getPositionCls(ele, idx)])
        });
      });
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", _extends({}, restProps, {
        className: classString
      }), items);
    };

    return _this;
  }

  _createClass(Timeline, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_4__["ConfigConsumer"], null, this.renderTimeline);
    }
  }]);

  return Timeline;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Timeline.Item = _TimelineItem__WEBPACK_IMPORTED_MODULE_2__["default"];
Timeline.defaultProps = {
  reverse: false,
  mode: ''
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90aW1lbGluZS9UaW1lbGluZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvdGltZWxpbmUvVGltZWxpbmUuanN4Il0sInNvdXJjZXNDb250ZW50IjpbInZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IFRpbWVsaW5lSXRlbSBmcm9tICcuL1RpbWVsaW5lSXRlbSc7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFRpbWVsaW5lIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcbiAgICAgICAgdGhpcy5yZW5kZXJUaW1lbGluZSA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBfYSA9IHRoaXMucHJvcHMsIHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIHBlbmRpbmcgPSBudWxsLCBwZW5kaW5nRG90LCBjaGlsZHJlbiwgY2xhc3NOYW1lLCByZXZlcnNlLCBtb2RlIH0gPSBfYSwgcmVzdFByb3BzID0gX19yZXN0KF9hLCBbXCJwcmVmaXhDbHNcIiwgXCJwZW5kaW5nXCIsIFwicGVuZGluZ0RvdFwiLCBcImNoaWxkcmVuXCIsIFwiY2xhc3NOYW1lXCIsIFwicmV2ZXJzZVwiLCBcIm1vZGVcIl0pO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCd0aW1lbGluZScsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBjb25zdCBwZW5kaW5nTm9kZSA9IHR5cGVvZiBwZW5kaW5nID09PSAnYm9vbGVhbicgPyBudWxsIDogcGVuZGluZztcbiAgICAgICAgICAgIGNvbnN0IGNsYXNzU3RyaW5nID0gY2xhc3NOYW1lcyhwcmVmaXhDbHMsIHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1wZW5kaW5nYF06ICEhcGVuZGluZyxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1yZXZlcnNlYF06ICEhcmV2ZXJzZSxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS0ke21vZGV9YF06ICEhbW9kZSxcbiAgICAgICAgICAgIH0sIGNsYXNzTmFtZSk7XG4gICAgICAgICAgICBjb25zdCBwZW5kaW5nSXRlbSA9IHBlbmRpbmcgPyAoPFRpbWVsaW5lSXRlbSBwZW5kaW5nPXshIXBlbmRpbmd9IGRvdD17cGVuZGluZ0RvdCB8fCA8SWNvbiB0eXBlPVwibG9hZGluZ1wiLz59PlxuICAgICAgICB7cGVuZGluZ05vZGV9XG4gICAgICA8L1RpbWVsaW5lSXRlbT4pIDogbnVsbDtcbiAgICAgICAgICAgIGNvbnN0IHRpbWVMaW5lSXRlbXMgPSByZXZlcnNlXG4gICAgICAgICAgICAgICAgPyBbcGVuZGluZ0l0ZW0sIC4uLlJlYWN0LkNoaWxkcmVuLnRvQXJyYXkoY2hpbGRyZW4pLnJldmVyc2UoKV1cbiAgICAgICAgICAgICAgICA6IFsuLi5SZWFjdC5DaGlsZHJlbi50b0FycmF5KGNoaWxkcmVuKSwgcGVuZGluZ0l0ZW1dO1xuICAgICAgICAgICAgY29uc3QgZ2V0UG9zaXRpb25DbHMgPSAoZWxlLCBpZHgpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAobW9kZSA9PT0gJ2FsdGVybmF0ZScpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVsZS5wcm9wcy5wb3NpdGlvbiA9PT0gJ3JpZ2h0JylcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBgJHtwcmVmaXhDbHN9LWl0ZW0tcmlnaHRgO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZWxlLnByb3BzLnBvc2l0aW9uID09PSAnbGVmdCcpXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYCR7cHJlZml4Q2xzfS1pdGVtLWxlZnRgO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gaWR4ICUgMiA9PT0gMCA/IGAke3ByZWZpeENsc30taXRlbS1sZWZ0YCA6IGAke3ByZWZpeENsc30taXRlbS1yaWdodGA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChtb2RlID09PSAnbGVmdCcpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBgJHtwcmVmaXhDbHN9LWl0ZW0tbGVmdGA7XG4gICAgICAgICAgICAgICAgaWYgKG1vZGUgPT09ICdyaWdodCcpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBgJHtwcmVmaXhDbHN9LWl0ZW0tcmlnaHRgO1xuICAgICAgICAgICAgICAgIGlmIChlbGUucHJvcHMucG9zaXRpb24gPT09ICdyaWdodCcpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBgJHtwcmVmaXhDbHN9LWl0ZW0tcmlnaHRgO1xuICAgICAgICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICAvLyBSZW1vdmUgZmFsc3kgaXRlbXNcbiAgICAgICAgICAgIGNvbnN0IHRydXRoeUl0ZW1zID0gdGltZUxpbmVJdGVtcy5maWx0ZXIoaXRlbSA9PiAhIWl0ZW0pO1xuICAgICAgICAgICAgY29uc3QgaXRlbXNDb3VudCA9IFJlYWN0LkNoaWxkcmVuLmNvdW50KHRydXRoeUl0ZW1zKTtcbiAgICAgICAgICAgIGNvbnN0IGxhc3RDbHMgPSBgJHtwcmVmaXhDbHN9LWl0ZW0tbGFzdGA7XG4gICAgICAgICAgICBjb25zdCBpdGVtcyA9IFJlYWN0LkNoaWxkcmVuLm1hcCh0cnV0aHlJdGVtcywgKGVsZSwgaWR4KSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgcGVuZGluZ0NsYXNzID0gaWR4ID09PSBpdGVtc0NvdW50IC0gMiA/IGxhc3RDbHMgOiAnJztcbiAgICAgICAgICAgICAgICBjb25zdCByZWFkeUNsYXNzID0gaWR4ID09PSBpdGVtc0NvdW50IC0gMSA/IGxhc3RDbHMgOiAnJztcbiAgICAgICAgICAgICAgICByZXR1cm4gUmVhY3QuY2xvbmVFbGVtZW50KGVsZSwge1xuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZXMoW1xuICAgICAgICAgICAgICAgICAgICAgICAgZWxlLnByb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICFyZXZlcnNlICYmICEhcGVuZGluZyA/IHBlbmRpbmdDbGFzcyA6IHJlYWR5Q2xhc3MsXG4gICAgICAgICAgICAgICAgICAgICAgICBnZXRQb3NpdGlvbkNscyhlbGUsIGlkeCksXG4gICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gKDx1bCB7Li4ucmVzdFByb3BzfSBjbGFzc05hbWU9e2NsYXNzU3RyaW5nfT5cbiAgICAgICAge2l0ZW1zfVxuICAgICAgPC91bD4pO1xuICAgICAgICB9O1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyVGltZWxpbmV9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuVGltZWxpbmUuSXRlbSA9IFRpbWVsaW5lSXRlbTtcblRpbWVsaW5lLmRlZmF1bHRQcm9wcyA9IHtcbiAgICByZXZlcnNlOiBmYWxzZSxcbiAgICBtb2RlOiAnJyxcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFFQTtBQUVBO0FBN0JBO0FBQ0E7QUFDQTtBQThCQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSEE7QUFXQTtBQUFBO0FBQUE7QUE5Q0E7QUFDQTtBQUhBO0FBb0RBO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTs7OztBQXhEQTtBQUNBO0FBREE7QUEwREE7QUFDQTtBQUNBO0FBQ0E7QUFGQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/timeline/Timeline.js
