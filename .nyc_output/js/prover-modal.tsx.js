__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TypeModal; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/mobile-modal/prover-modal.tsx";



var isMobile = _common__WEBPACK_IMPORTED_MODULE_5__["Utils"].getIsMobile();

var TypeModal =
/*#__PURE__*/
function (_React$Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(TypeModal, _React$Component);

  function TypeModal() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, TypeModal);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(TypeModal)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      typeData: null,
      showMsg: "",
      isLoading: false
    };
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(TypeModal, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          onCancel = _this$props.onCancel,
          onOk = _this$props.onOk,
          childHtml = _this$props.childHtml;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Modal"], {
        width: isMobile ? "100%" : "45%",
        maskClosable: false,
        visible: true,
        centered: true,
        title: null,
        onCancel: onCancel,
        onOk: onOk,
        footer: null,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 29
        },
        __self: this
      }, childHtml);
    }
  }]);

  return TypeModal;
}(_common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].Component);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L21vYmlsZS1tb2RhbC9wcm92ZXItbW9kYWwudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L21vYmlsZS1tb2RhbC9wcm92ZXItbW9kYWwudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RhbCB9IGZyb20gXCJhbnRkXCI7XG5cbmludGVyZmFjZSBJU3RhdGUge1xuICB0eXBlRGF0YTogYW55LFxuICBzaG93TXNnOiBhbnksXG4gIGlzTG9hZGluZzogYm9vbGVhbixcbn1cblxuaW50ZXJmYWNlIElQcm9wcyB7XG4gIG9uQ2FuY2VsOiBhbnksXG4gIG9uT2s/OiBhbnksXG4gIGNoaWxkSHRtbDogYW55O1xufVxuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFR5cGVNb2RhbCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudDxJUHJvcHMsIElTdGF0ZT4ge1xuICBzdGF0ZTogSVN0YXRlID0ge1xuICAgIHR5cGVEYXRhOiBudWxsLFxuICAgIHNob3dNc2c6IFwiXCIsXG4gICAgaXNMb2FkaW5nOiBmYWxzZSxcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBvbkNhbmNlbCwgb25PaywgY2hpbGRIdG1sIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8TW9kYWxcbiAgICAgICAgd2lkdGg9e2lzTW9iaWxlID8gXCIxMDAlXCIgOiBcIjQ1JVwifVxuICAgICAgICBtYXNrQ2xvc2FibGU9e2ZhbHNlfVxuICAgICAgICB2aXNpYmxlPXt0cnVlfVxuICAgICAgICBjZW50ZXJlZD17dHJ1ZX1cbiAgICAgICAgdGl0bGU9e251bGx9XG4gICAgICAgIG9uQ2FuY2VsPXtvbkNhbmNlbH1cbiAgICAgICAgb25Paz17b25Pa31cbiAgICAgICAgZm9vdGVyPXtudWxsfT5cbiAgICAgICAge2NoaWxkSHRtbH1cbiAgICAgIDwvTW9kYWw+XG4gICAgKTtcbiAgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBY0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7Ozs7OztBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTs7OztBQXRCQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/mobile-modal/prover-modal.tsx
