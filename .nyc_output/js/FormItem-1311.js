

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var React = _interopRequireWildcard(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var ReactDOM = _interopRequireWildcard(__webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js"));

var PropTypes = _interopRequireWildcard(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

var _classnames = _interopRequireDefault(__webpack_require__(/*! classnames */ "./node_modules/classnames/index.js"));

var _rcAnimate = _interopRequireDefault(__webpack_require__(/*! rc-animate */ "./node_modules/rc-animate/es/Animate.js"));

var _omit = _interopRequireDefault(__webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js"));

var _row = _interopRequireDefault(__webpack_require__(/*! ../grid/row */ "./node_modules/antd/lib/grid/row.js"));

var _col = _interopRequireDefault(__webpack_require__(/*! ../grid/col */ "./node_modules/antd/lib/grid/col.js"));

var _icon = _interopRequireDefault(__webpack_require__(/*! ../icon */ "./node_modules/antd/lib/icon/index.js"));

var _configProvider = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/lib/config-provider/index.js");

var _warning = _interopRequireDefault(__webpack_require__(/*! ../_util/warning */ "./node_modules/antd/lib/_util/warning.js"));

var _type = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/lib/_util/type.js");

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/antd/lib/form/constants.js");

var _context = _interopRequireDefault(__webpack_require__(/*! ./context */ "./node_modules/antd/lib/form/context.js"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

var __rest = void 0 && (void 0).__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};

var ValidateStatuses = (0, _type.tuple)('success', 'warning', 'error', 'validating', '');
var FormLabelAligns = (0, _type.tuple)('left', 'right');

function intersperseSpace(list) {
  return list.reduce(function (current, item) {
    return [].concat(_toConsumableArray(current), [' ', item]);
  }, []).slice(1);
}

var FormItem =
/*#__PURE__*/
function (_React$Component) {
  _inherits(FormItem, _React$Component);

  function FormItem() {
    var _this;

    _classCallCheck(this, FormItem);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FormItem).apply(this, arguments));
    _this.helpShow = false; // Resolve duplicated ids bug between different forms
    // https://github.com/ant-design/ant-design/issues/7351

    _this.onLabelClick = function () {
      var id = _this.props.id || _this.getId();

      if (!id) {
        return;
      }

      var formItemNode = ReactDOM.findDOMNode(_assertThisInitialized(_this));
      var control = formItemNode.querySelector("[id=\"".concat(id, "\"]"));

      if (control && control.focus) {
        control.focus();
      }
    };

    _this.onHelpAnimEnd = function (_key, helpShow) {
      _this.helpShow = helpShow;

      if (!helpShow) {
        _this.setState({});
      }
    };

    _this.renderFormItem = function (_ref) {
      var _itemClassName;

      var getPrefixCls = _ref.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          style = _a.style,
          className = _a.className,
          restProps = __rest(_a, ["prefixCls", "style", "className"]);

      var prefixCls = getPrefixCls('form', customizePrefixCls);

      var children = _this.renderChildren(prefixCls);

      var itemClassName = (_itemClassName = {}, _defineProperty(_itemClassName, "".concat(prefixCls, "-item"), true), _defineProperty(_itemClassName, "".concat(prefixCls, "-item-with-help"), _this.helpShow), _defineProperty(_itemClassName, "".concat(className), !!className), _itemClassName);
      return React.createElement(_row["default"], _extends({
        className: (0, _classnames["default"])(itemClassName),
        style: style
      }, (0, _omit["default"])(restProps, ['id', 'htmlFor', 'label', 'labelAlign', 'labelCol', 'wrapperCol', 'help', 'extra', 'validateStatus', 'hasFeedback', 'required', 'colon']), {
        key: "row"
      }), children);
    };

    return _this;
  }

  _createClass(FormItem, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          children = _this$props.children,
          help = _this$props.help,
          validateStatus = _this$props.validateStatus,
          id = _this$props.id;
      (0, _warning["default"])(this.getControls(children, true).length <= 1 || help !== undefined || validateStatus !== undefined, 'Form.Item', 'Cannot generate `validateStatus` and `help` automatically, ' + 'while there are more than one `getFieldDecorator` in it.');
      (0, _warning["default"])(!id, 'Form.Item', '`id` is deprecated for its label `htmlFor`. Please use `htmlFor` directly.');
    }
  }, {
    key: "getHelpMessage",
    value: function getHelpMessage() {
      var help = this.props.help;

      if (help === undefined && this.getOnlyControl()) {
        var _this$getField = this.getField(),
            errors = _this$getField.errors;

        if (errors) {
          return intersperseSpace(errors.map(function (e, index) {
            var node = null;

            if (React.isValidElement(e)) {
              node = e;
            } else if (React.isValidElement(e.message)) {
              node = e.message;
            } // eslint-disable-next-line react/no-array-index-key


            return node ? React.cloneElement(node, {
              key: index
            }) : e.message;
          }));
        }

        return '';
      }

      return help;
    }
  }, {
    key: "getControls",
    value: function getControls(children, recursively) {
      var controls = [];
      var childrenArray = React.Children.toArray(children);

      for (var i = 0; i < childrenArray.length; i++) {
        if (!recursively && controls.length > 0) {
          break;
        }

        var child = childrenArray[i];

        if (child.type && (child.type === FormItem || child.type.displayName === 'FormItem')) {
          continue;
        }

        if (!child.props) {
          continue;
        }

        if (_constants.FIELD_META_PROP in child.props) {
          // And means FIELD_DATA_PROP in child.props, too.
          controls.push(child);
        } else if (child.props.children) {
          controls = controls.concat(this.getControls(child.props.children, recursively));
        }
      }

      return controls;
    }
  }, {
    key: "getOnlyControl",
    value: function getOnlyControl() {
      var child = this.getControls(this.props.children, false)[0];
      return child !== undefined ? child : null;
    }
  }, {
    key: "getChildProp",
    value: function getChildProp(prop) {
      var child = this.getOnlyControl();
      return child && child.props && child.props[prop];
    }
  }, {
    key: "getId",
    value: function getId() {
      return this.getChildProp('id');
    }
  }, {
    key: "getMeta",
    value: function getMeta() {
      return this.getChildProp(_constants.FIELD_META_PROP);
    }
  }, {
    key: "getField",
    value: function getField() {
      return this.getChildProp(_constants.FIELD_DATA_PROP);
    }
  }, {
    key: "getValidateStatus",
    value: function getValidateStatus() {
      var onlyControl = this.getOnlyControl();

      if (!onlyControl) {
        return '';
      }

      var field = this.getField();

      if (field.validating) {
        return 'validating';
      }

      if (field.errors) {
        return 'error';
      }

      var fieldValue = 'value' in field ? field.value : this.getMeta().initialValue;

      if (fieldValue !== undefined && fieldValue !== null && fieldValue !== '') {
        return 'success';
      }

      return '';
    }
  }, {
    key: "isRequired",
    value: function isRequired() {
      var required = this.props.required;

      if (required !== undefined) {
        return required;
      }

      if (this.getOnlyControl()) {
        var meta = this.getMeta() || {};
        var validate = meta.validate || [];
        return validate.filter(function (item) {
          return !!item.rules;
        }).some(function (item) {
          return item.rules.some(function (rule) {
            return rule.required;
          });
        });
      }

      return false;
    }
  }, {
    key: "renderHelp",
    value: function renderHelp(prefixCls) {
      var help = this.getHelpMessage();
      var children = help ? React.createElement("div", {
        className: "".concat(prefixCls, "-explain"),
        key: "help"
      }, help) : null;

      if (children) {
        this.helpShow = !!children;
      }

      return React.createElement(_rcAnimate["default"], {
        transitionName: "show-help",
        component: "",
        transitionAppear: true,
        key: "help",
        onEnd: this.onHelpAnimEnd
      }, children);
    }
  }, {
    key: "renderExtra",
    value: function renderExtra(prefixCls) {
      var extra = this.props.extra;
      return extra ? React.createElement("div", {
        className: "".concat(prefixCls, "-extra")
      }, extra) : null;
    }
  }, {
    key: "renderValidateWrapper",
    value: function renderValidateWrapper(prefixCls, c1, c2, c3) {
      var props = this.props;
      var onlyControl = this.getOnlyControl;
      var validateStatus = props.validateStatus === undefined && onlyControl ? this.getValidateStatus() : props.validateStatus;
      var classes = "".concat(prefixCls, "-item-control");

      if (validateStatus) {
        classes = (0, _classnames["default"])("".concat(prefixCls, "-item-control"), {
          'has-feedback': props.hasFeedback || validateStatus === 'validating',
          'has-success': validateStatus === 'success',
          'has-warning': validateStatus === 'warning',
          'has-error': validateStatus === 'error',
          'is-validating': validateStatus === 'validating'
        });
      }

      var iconType = '';

      switch (validateStatus) {
        case 'success':
          iconType = 'check-circle';
          break;

        case 'warning':
          iconType = 'exclamation-circle';
          break;

        case 'error':
          iconType = 'close-circle';
          break;

        case 'validating':
          iconType = 'loading';
          break;

        default:
          iconType = '';
          break;
      }

      var icon = props.hasFeedback && iconType ? React.createElement("span", {
        className: "".concat(prefixCls, "-item-children-icon")
      }, React.createElement(_icon["default"], {
        type: iconType,
        theme: iconType === 'loading' ? 'outlined' : 'filled'
      })) : null;
      return React.createElement("div", {
        className: classes
      }, React.createElement("span", {
        className: "".concat(prefixCls, "-item-children")
      }, c1, icon), c2, c3);
    }
  }, {
    key: "renderWrapper",
    value: function renderWrapper(prefixCls, children) {
      var _this2 = this;

      return React.createElement(_context["default"].Consumer, {
        key: "wrapper"
      }, function (_ref2) {
        var contextWrapperCol = _ref2.wrapperCol,
            vertical = _ref2.vertical;
        var wrapperCol = _this2.props.wrapperCol;
        var mergedWrapperCol = ('wrapperCol' in _this2.props ? wrapperCol : contextWrapperCol) || {};
        var className = (0, _classnames["default"])("".concat(prefixCls, "-item-control-wrapper"), mergedWrapperCol.className); // No pass FormContext since it's useless

        return React.createElement(_context["default"].Provider, {
          value: {
            vertical: vertical
          }
        }, React.createElement(_col["default"], _extends({}, mergedWrapperCol, {
          className: className
        }), children));
      });
    }
  }, {
    key: "renderLabel",
    value: function renderLabel(prefixCls) {
      var _this3 = this;

      return React.createElement(_context["default"].Consumer, {
        key: "label"
      }, function (_ref3) {
        var _classNames;

        var vertical = _ref3.vertical,
            contextLabelAlign = _ref3.labelAlign,
            contextLabelCol = _ref3.labelCol,
            contextColon = _ref3.colon;
        var _this3$props = _this3.props,
            label = _this3$props.label,
            labelCol = _this3$props.labelCol,
            labelAlign = _this3$props.labelAlign,
            colon = _this3$props.colon,
            id = _this3$props.id,
            htmlFor = _this3$props.htmlFor;

        var required = _this3.isRequired();

        var mergedLabelCol = ('labelCol' in _this3.props ? labelCol : contextLabelCol) || {};
        var mergedLabelAlign = 'labelAlign' in _this3.props ? labelAlign : contextLabelAlign;
        var labelClsBasic = "".concat(prefixCls, "-item-label");
        var labelColClassName = (0, _classnames["default"])(labelClsBasic, mergedLabelAlign === 'left' && "".concat(labelClsBasic, "-left"), mergedLabelCol.className);
        var labelChildren = label; // Keep label is original where there should have no colon

        var computedColon = colon === true || contextColon !== false && colon !== false;
        var haveColon = computedColon && !vertical; // Remove duplicated user input colon

        if (haveColon && typeof label === 'string' && label.trim() !== '') {
          labelChildren = label.replace(/[：:]\s*$/, '');
        }

        var labelClassName = (0, _classnames["default"])((_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-item-required"), required), _defineProperty(_classNames, "".concat(prefixCls, "-item-no-colon"), !computedColon), _classNames));
        return label ? React.createElement(_col["default"], _extends({}, mergedLabelCol, {
          className: labelColClassName
        }), React.createElement("label", {
          htmlFor: htmlFor || id || _this3.getId(),
          className: labelClassName,
          title: typeof label === 'string' ? label : '',
          onClick: _this3.onLabelClick
        }, labelChildren)) : null;
      });
    }
  }, {
    key: "renderChildren",
    value: function renderChildren(prefixCls) {
      var children = this.props.children;
      return [this.renderLabel(prefixCls), this.renderWrapper(prefixCls, this.renderValidateWrapper(prefixCls, children, this.renderHelp(prefixCls), this.renderExtra(prefixCls)))];
    }
  }, {
    key: "render",
    value: function render() {
      return React.createElement(_configProvider.ConfigConsumer, null, this.renderFormItem);
    }
  }]);

  return FormItem;
}(React.Component);

exports["default"] = FormItem;
FormItem.defaultProps = {
  hasFeedback: false
};
FormItem.propTypes = {
  prefixCls: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  labelCol: PropTypes.object,
  help: PropTypes.oneOfType([PropTypes.node, PropTypes.bool]),
  validateStatus: PropTypes.oneOf(ValidateStatuses),
  hasFeedback: PropTypes.bool,
  wrapperCol: PropTypes.object,
  className: PropTypes.string,
  id: PropTypes.string,
  children: PropTypes.node,
  colon: PropTypes.bool
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9saWIvZm9ybS9Gb3JtSXRlbS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvZm9ybS9Gb3JtSXRlbS5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0ICogYXMgUmVhY3RET00gZnJvbSAncmVhY3QtZG9tJztcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IEFuaW1hdGUgZnJvbSAncmMtYW5pbWF0ZSc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCBSb3cgZnJvbSAnLi4vZ3JpZC9yb3cnO1xuaW1wb3J0IENvbCBmcm9tICcuLi9ncmlkL2NvbCc7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmltcG9ydCB3YXJuaW5nIGZyb20gJy4uL191dGlsL3dhcm5pbmcnO1xuaW1wb3J0IHsgdHVwbGUgfSBmcm9tICcuLi9fdXRpbC90eXBlJztcbmltcG9ydCB7IEZJRUxEX01FVEFfUFJPUCwgRklFTERfREFUQV9QUk9QIH0gZnJvbSAnLi9jb25zdGFudHMnO1xuaW1wb3J0IEZvcm1Db250ZXh0IGZyb20gJy4vY29udGV4dCc7XG5jb25zdCBWYWxpZGF0ZVN0YXR1c2VzID0gdHVwbGUoJ3N1Y2Nlc3MnLCAnd2FybmluZycsICdlcnJvcicsICd2YWxpZGF0aW5nJywgJycpO1xuY29uc3QgRm9ybUxhYmVsQWxpZ25zID0gdHVwbGUoJ2xlZnQnLCAncmlnaHQnKTtcbmZ1bmN0aW9uIGludGVyc3BlcnNlU3BhY2UobGlzdCkge1xuICAgIHJldHVybiBsaXN0LnJlZHVjZSgoY3VycmVudCwgaXRlbSkgPT4gWy4uLmN1cnJlbnQsICcgJywgaXRlbV0sIFtdKS5zbGljZSgxKTtcbn1cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEZvcm1JdGVtIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcbiAgICAgICAgdGhpcy5oZWxwU2hvdyA9IGZhbHNlO1xuICAgICAgICAvLyBSZXNvbHZlIGR1cGxpY2F0ZWQgaWRzIGJ1ZyBiZXR3ZWVuIGRpZmZlcmVudCBmb3Jtc1xuICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy83MzUxXG4gICAgICAgIHRoaXMub25MYWJlbENsaWNrID0gKCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgaWQgPSB0aGlzLnByb3BzLmlkIHx8IHRoaXMuZ2V0SWQoKTtcbiAgICAgICAgICAgIGlmICghaWQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBmb3JtSXRlbU5vZGUgPSBSZWFjdERPTS5maW5kRE9NTm9kZSh0aGlzKTtcbiAgICAgICAgICAgIGNvbnN0IGNvbnRyb2wgPSBmb3JtSXRlbU5vZGUucXVlcnlTZWxlY3RvcihgW2lkPVwiJHtpZH1cIl1gKTtcbiAgICAgICAgICAgIGlmIChjb250cm9sICYmIGNvbnRyb2wuZm9jdXMpIHtcbiAgICAgICAgICAgICAgICBjb250cm9sLmZvY3VzKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25IZWxwQW5pbUVuZCA9IChfa2V5LCBoZWxwU2hvdykgPT4ge1xuICAgICAgICAgICAgdGhpcy5oZWxwU2hvdyA9IGhlbHBTaG93O1xuICAgICAgICAgICAgaWYgKCFoZWxwU2hvdykge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe30pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlckZvcm1JdGVtID0gKHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IF9hID0gdGhpcy5wcm9wcywgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgc3R5bGUsIGNsYXNzTmFtZSB9ID0gX2EsIHJlc3RQcm9wcyA9IF9fcmVzdChfYSwgW1wicHJlZml4Q2xzXCIsIFwic3R5bGVcIiwgXCJjbGFzc05hbWVcIl0pO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdmb3JtJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGNoaWxkcmVuID0gdGhpcy5yZW5kZXJDaGlsZHJlbihwcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3QgaXRlbUNsYXNzTmFtZSA9IHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1pdGVtYF06IHRydWUsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30taXRlbS13aXRoLWhlbHBgXTogdGhpcy5oZWxwU2hvdyxcbiAgICAgICAgICAgICAgICBbYCR7Y2xhc3NOYW1lfWBdOiAhIWNsYXNzTmFtZSxcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICByZXR1cm4gKDxSb3cgY2xhc3NOYW1lPXtjbGFzc05hbWVzKGl0ZW1DbGFzc05hbWUpfSBzdHlsZT17c3R5bGV9IHsuLi5vbWl0KHJlc3RQcm9wcywgW1xuICAgICAgICAgICAgICAgICdpZCcsXG4gICAgICAgICAgICAgICAgJ2h0bWxGb3InLFxuICAgICAgICAgICAgICAgICdsYWJlbCcsXG4gICAgICAgICAgICAgICAgJ2xhYmVsQWxpZ24nLFxuICAgICAgICAgICAgICAgICdsYWJlbENvbCcsXG4gICAgICAgICAgICAgICAgJ3dyYXBwZXJDb2wnLFxuICAgICAgICAgICAgICAgICdoZWxwJyxcbiAgICAgICAgICAgICAgICAnZXh0cmEnLFxuICAgICAgICAgICAgICAgICd2YWxpZGF0ZVN0YXR1cycsXG4gICAgICAgICAgICAgICAgJ2hhc0ZlZWRiYWNrJyxcbiAgICAgICAgICAgICAgICAncmVxdWlyZWQnLFxuICAgICAgICAgICAgICAgICdjb2xvbicsXG4gICAgICAgICAgICBdKX0ga2V5PVwicm93XCI+XG4gICAgICAgIHtjaGlsZHJlbn1cbiAgICAgIDwvUm93Pik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgICBjb25zdCB7IGNoaWxkcmVuLCBoZWxwLCB2YWxpZGF0ZVN0YXR1cywgaWQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHdhcm5pbmcodGhpcy5nZXRDb250cm9scyhjaGlsZHJlbiwgdHJ1ZSkubGVuZ3RoIDw9IDEgfHxcbiAgICAgICAgICAgIGhlbHAgIT09IHVuZGVmaW5lZCB8fFxuICAgICAgICAgICAgdmFsaWRhdGVTdGF0dXMgIT09IHVuZGVmaW5lZCwgJ0Zvcm0uSXRlbScsICdDYW5ub3QgZ2VuZXJhdGUgYHZhbGlkYXRlU3RhdHVzYCBhbmQgYGhlbHBgIGF1dG9tYXRpY2FsbHksICcgK1xuICAgICAgICAgICAgJ3doaWxlIHRoZXJlIGFyZSBtb3JlIHRoYW4gb25lIGBnZXRGaWVsZERlY29yYXRvcmAgaW4gaXQuJyk7XG4gICAgICAgIHdhcm5pbmcoIWlkLCAnRm9ybS5JdGVtJywgJ2BpZGAgaXMgZGVwcmVjYXRlZCBmb3IgaXRzIGxhYmVsIGBodG1sRm9yYC4gUGxlYXNlIHVzZSBgaHRtbEZvcmAgZGlyZWN0bHkuJyk7XG4gICAgfVxuICAgIGdldEhlbHBNZXNzYWdlKCkge1xuICAgICAgICBjb25zdCB7IGhlbHAgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmIChoZWxwID09PSB1bmRlZmluZWQgJiYgdGhpcy5nZXRPbmx5Q29udHJvbCgpKSB7XG4gICAgICAgICAgICBjb25zdCB7IGVycm9ycyB9ID0gdGhpcy5nZXRGaWVsZCgpO1xuICAgICAgICAgICAgaWYgKGVycm9ycykge1xuICAgICAgICAgICAgICAgIHJldHVybiBpbnRlcnNwZXJzZVNwYWNlKGVycm9ycy5tYXAoKGUsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBub2RlID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgaWYgKFJlYWN0LmlzVmFsaWRFbGVtZW50KGUpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBub2RlID0gZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChSZWFjdC5pc1ZhbGlkRWxlbWVudChlLm1lc3NhZ2UpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBub2RlID0gZS5tZXNzYWdlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSByZWFjdC9uby1hcnJheS1pbmRleC1rZXlcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5vZGUgPyBSZWFjdC5jbG9uZUVsZW1lbnQobm9kZSwgeyBrZXk6IGluZGV4IH0pIDogZS5tZXNzYWdlO1xuICAgICAgICAgICAgICAgIH0pKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaGVscDtcbiAgICB9XG4gICAgZ2V0Q29udHJvbHMoY2hpbGRyZW4sIHJlY3Vyc2l2ZWx5KSB7XG4gICAgICAgIGxldCBjb250cm9scyA9IFtdO1xuICAgICAgICBjb25zdCBjaGlsZHJlbkFycmF5ID0gUmVhY3QuQ2hpbGRyZW4udG9BcnJheShjaGlsZHJlbik7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpbGRyZW5BcnJheS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKCFyZWN1cnNpdmVseSAmJiBjb250cm9scy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBjaGlsZCA9IGNoaWxkcmVuQXJyYXlbaV07XG4gICAgICAgICAgICBpZiAoY2hpbGQudHlwZSAmJlxuICAgICAgICAgICAgICAgIChjaGlsZC50eXBlID09PSBGb3JtSXRlbSB8fCBjaGlsZC50eXBlLmRpc3BsYXlOYW1lID09PSAnRm9ybUl0ZW0nKSkge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKCFjaGlsZC5wcm9wcykge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKEZJRUxEX01FVEFfUFJPUCBpbiBjaGlsZC5wcm9wcykge1xuICAgICAgICAgICAgICAgIC8vIEFuZCBtZWFucyBGSUVMRF9EQVRBX1BST1AgaW4gY2hpbGQucHJvcHMsIHRvby5cbiAgICAgICAgICAgICAgICBjb250cm9scy5wdXNoKGNoaWxkKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKGNoaWxkLnByb3BzLmNoaWxkcmVuKSB7XG4gICAgICAgICAgICAgICAgY29udHJvbHMgPSBjb250cm9scy5jb25jYXQodGhpcy5nZXRDb250cm9scyhjaGlsZC5wcm9wcy5jaGlsZHJlbiwgcmVjdXJzaXZlbHkpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gY29udHJvbHM7XG4gICAgfVxuICAgIGdldE9ubHlDb250cm9sKCkge1xuICAgICAgICBjb25zdCBjaGlsZCA9IHRoaXMuZ2V0Q29udHJvbHModGhpcy5wcm9wcy5jaGlsZHJlbiwgZmFsc2UpWzBdO1xuICAgICAgICByZXR1cm4gY2hpbGQgIT09IHVuZGVmaW5lZCA/IGNoaWxkIDogbnVsbDtcbiAgICB9XG4gICAgZ2V0Q2hpbGRQcm9wKHByb3ApIHtcbiAgICAgICAgY29uc3QgY2hpbGQgPSB0aGlzLmdldE9ubHlDb250cm9sKCk7XG4gICAgICAgIHJldHVybiBjaGlsZCAmJiBjaGlsZC5wcm9wcyAmJiBjaGlsZC5wcm9wc1twcm9wXTtcbiAgICB9XG4gICAgZ2V0SWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldENoaWxkUHJvcCgnaWQnKTtcbiAgICB9XG4gICAgZ2V0TWV0YSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0Q2hpbGRQcm9wKEZJRUxEX01FVEFfUFJPUCk7XG4gICAgfVxuICAgIGdldEZpZWxkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5nZXRDaGlsZFByb3AoRklFTERfREFUQV9QUk9QKTtcbiAgICB9XG4gICAgZ2V0VmFsaWRhdGVTdGF0dXMoKSB7XG4gICAgICAgIGNvbnN0IG9ubHlDb250cm9sID0gdGhpcy5nZXRPbmx5Q29udHJvbCgpO1xuICAgICAgICBpZiAoIW9ubHlDb250cm9sKSB7XG4gICAgICAgICAgICByZXR1cm4gJyc7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgZmllbGQgPSB0aGlzLmdldEZpZWxkKCk7XG4gICAgICAgIGlmIChmaWVsZC52YWxpZGF0aW5nKSB7XG4gICAgICAgICAgICByZXR1cm4gJ3ZhbGlkYXRpbmcnO1xuICAgICAgICB9XG4gICAgICAgIGlmIChmaWVsZC5lcnJvcnMpIHtcbiAgICAgICAgICAgIHJldHVybiAnZXJyb3InO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IGZpZWxkVmFsdWUgPSAndmFsdWUnIGluIGZpZWxkID8gZmllbGQudmFsdWUgOiB0aGlzLmdldE1ldGEoKS5pbml0aWFsVmFsdWU7XG4gICAgICAgIGlmIChmaWVsZFZhbHVlICE9PSB1bmRlZmluZWQgJiYgZmllbGRWYWx1ZSAhPT0gbnVsbCAmJiBmaWVsZFZhbHVlICE9PSAnJykge1xuICAgICAgICAgICAgcmV0dXJuICdzdWNjZXNzJztcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gJyc7XG4gICAgfVxuICAgIGlzUmVxdWlyZWQoKSB7XG4gICAgICAgIGNvbnN0IHsgcmVxdWlyZWQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmIChyZXF1aXJlZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gcmVxdWlyZWQ7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuZ2V0T25seUNvbnRyb2woKSkge1xuICAgICAgICAgICAgY29uc3QgbWV0YSA9IHRoaXMuZ2V0TWV0YSgpIHx8IHt9O1xuICAgICAgICAgICAgY29uc3QgdmFsaWRhdGUgPSBtZXRhLnZhbGlkYXRlIHx8IFtdO1xuICAgICAgICAgICAgcmV0dXJuIHZhbGlkYXRlXG4gICAgICAgICAgICAgICAgLmZpbHRlcigoaXRlbSkgPT4gISFpdGVtLnJ1bGVzKVxuICAgICAgICAgICAgICAgIC5zb21lKChpdGVtKSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGl0ZW0ucnVsZXMuc29tZSgocnVsZSkgPT4gcnVsZS5yZXF1aXJlZCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJlbmRlckhlbHAocHJlZml4Q2xzKSB7XG4gICAgICAgIGNvbnN0IGhlbHAgPSB0aGlzLmdldEhlbHBNZXNzYWdlKCk7XG4gICAgICAgIGNvbnN0IGNoaWxkcmVuID0gaGVscCA/ICg8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1leHBsYWluYH0ga2V5PVwiaGVscFwiPlxuICAgICAgICB7aGVscH1cbiAgICAgIDwvZGl2PikgOiBudWxsO1xuICAgICAgICBpZiAoY2hpbGRyZW4pIHtcbiAgICAgICAgICAgIHRoaXMuaGVscFNob3cgPSAhIWNoaWxkcmVuO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAoPEFuaW1hdGUgdHJhbnNpdGlvbk5hbWU9XCJzaG93LWhlbHBcIiBjb21wb25lbnQ9XCJcIiB0cmFuc2l0aW9uQXBwZWFyIGtleT1cImhlbHBcIiBvbkVuZD17dGhpcy5vbkhlbHBBbmltRW5kfT5cbiAgICAgICAge2NoaWxkcmVufVxuICAgICAgPC9BbmltYXRlPik7XG4gICAgfVxuICAgIHJlbmRlckV4dHJhKHByZWZpeENscykge1xuICAgICAgICBjb25zdCB7IGV4dHJhIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICByZXR1cm4gZXh0cmEgPyA8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1leHRyYWB9PntleHRyYX08L2Rpdj4gOiBudWxsO1xuICAgIH1cbiAgICByZW5kZXJWYWxpZGF0ZVdyYXBwZXIocHJlZml4Q2xzLCBjMSwgYzIsIGMzKSB7XG4gICAgICAgIGNvbnN0IHsgcHJvcHMgfSA9IHRoaXM7XG4gICAgICAgIGNvbnN0IG9ubHlDb250cm9sID0gdGhpcy5nZXRPbmx5Q29udHJvbDtcbiAgICAgICAgY29uc3QgdmFsaWRhdGVTdGF0dXMgPSBwcm9wcy52YWxpZGF0ZVN0YXR1cyA9PT0gdW5kZWZpbmVkICYmIG9ubHlDb250cm9sXG4gICAgICAgICAgICA/IHRoaXMuZ2V0VmFsaWRhdGVTdGF0dXMoKVxuICAgICAgICAgICAgOiBwcm9wcy52YWxpZGF0ZVN0YXR1cztcbiAgICAgICAgbGV0IGNsYXNzZXMgPSBgJHtwcmVmaXhDbHN9LWl0ZW0tY29udHJvbGA7XG4gICAgICAgIGlmICh2YWxpZGF0ZVN0YXR1cykge1xuICAgICAgICAgICAgY2xhc3NlcyA9IGNsYXNzTmFtZXMoYCR7cHJlZml4Q2xzfS1pdGVtLWNvbnRyb2xgLCB7XG4gICAgICAgICAgICAgICAgJ2hhcy1mZWVkYmFjayc6IHByb3BzLmhhc0ZlZWRiYWNrIHx8IHZhbGlkYXRlU3RhdHVzID09PSAndmFsaWRhdGluZycsXG4gICAgICAgICAgICAgICAgJ2hhcy1zdWNjZXNzJzogdmFsaWRhdGVTdGF0dXMgPT09ICdzdWNjZXNzJyxcbiAgICAgICAgICAgICAgICAnaGFzLXdhcm5pbmcnOiB2YWxpZGF0ZVN0YXR1cyA9PT0gJ3dhcm5pbmcnLFxuICAgICAgICAgICAgICAgICdoYXMtZXJyb3InOiB2YWxpZGF0ZVN0YXR1cyA9PT0gJ2Vycm9yJyxcbiAgICAgICAgICAgICAgICAnaXMtdmFsaWRhdGluZyc6IHZhbGlkYXRlU3RhdHVzID09PSAndmFsaWRhdGluZycsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBsZXQgaWNvblR5cGUgPSAnJztcbiAgICAgICAgc3dpdGNoICh2YWxpZGF0ZVN0YXR1cykge1xuICAgICAgICAgICAgY2FzZSAnc3VjY2Vzcyc6XG4gICAgICAgICAgICAgICAgaWNvblR5cGUgPSAnY2hlY2stY2lyY2xlJztcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ3dhcm5pbmcnOlxuICAgICAgICAgICAgICAgIGljb25UeXBlID0gJ2V4Y2xhbWF0aW9uLWNpcmNsZSc7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICdlcnJvcic6XG4gICAgICAgICAgICAgICAgaWNvblR5cGUgPSAnY2xvc2UtY2lyY2xlJztcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ3ZhbGlkYXRpbmcnOlxuICAgICAgICAgICAgICAgIGljb25UeXBlID0gJ2xvYWRpbmcnO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICBpY29uVHlwZSA9ICcnO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IGljb24gPSBwcm9wcy5oYXNGZWVkYmFjayAmJiBpY29uVHlwZSA/ICg8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taXRlbS1jaGlsZHJlbi1pY29uYH0+XG4gICAgICAgICAgPEljb24gdHlwZT17aWNvblR5cGV9IHRoZW1lPXtpY29uVHlwZSA9PT0gJ2xvYWRpbmcnID8gJ291dGxpbmVkJyA6ICdmaWxsZWQnfS8+XG4gICAgICAgIDwvc3Bhbj4pIDogbnVsbDtcbiAgICAgICAgcmV0dXJuICg8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlc30+XG4gICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1pdGVtLWNoaWxkcmVuYH0+XG4gICAgICAgICAge2MxfVxuICAgICAgICAgIHtpY29ufVxuICAgICAgICA8L3NwYW4+XG4gICAgICAgIHtjMn1cbiAgICAgICAge2MzfVxuICAgICAgPC9kaXY+KTtcbiAgICB9XG4gICAgcmVuZGVyV3JhcHBlcihwcmVmaXhDbHMsIGNoaWxkcmVuKSB7XG4gICAgICAgIHJldHVybiAoPEZvcm1Db250ZXh0LkNvbnN1bWVyIGtleT1cIndyYXBwZXJcIj5cbiAgICAgICAgeyh7IHdyYXBwZXJDb2w6IGNvbnRleHRXcmFwcGVyQ29sLCB2ZXJ0aWNhbCB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHdyYXBwZXJDb2wgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCBtZXJnZWRXcmFwcGVyQ29sID0gKCd3cmFwcGVyQ29sJyBpbiB0aGlzLnByb3BzID8gd3JhcHBlckNvbCA6IGNvbnRleHRXcmFwcGVyQ29sKSB8fCB7fTtcbiAgICAgICAgICAgIGNvbnN0IGNsYXNzTmFtZSA9IGNsYXNzTmFtZXMoYCR7cHJlZml4Q2xzfS1pdGVtLWNvbnRyb2wtd3JhcHBlcmAsIG1lcmdlZFdyYXBwZXJDb2wuY2xhc3NOYW1lKTtcbiAgICAgICAgICAgIC8vIE5vIHBhc3MgRm9ybUNvbnRleHQgc2luY2UgaXQncyB1c2VsZXNzXG4gICAgICAgICAgICByZXR1cm4gKDxGb3JtQ29udGV4dC5Qcm92aWRlciB2YWx1ZT17eyB2ZXJ0aWNhbCB9fT5cbiAgICAgICAgICAgICAgPENvbCB7Li4ubWVyZ2VkV3JhcHBlckNvbH0gY2xhc3NOYW1lPXtjbGFzc05hbWV9PlxuICAgICAgICAgICAgICAgIHtjaGlsZHJlbn1cbiAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICA8L0Zvcm1Db250ZXh0LlByb3ZpZGVyPik7XG4gICAgICAgIH19XG4gICAgICA8L0Zvcm1Db250ZXh0LkNvbnN1bWVyPik7XG4gICAgfVxuICAgIHJlbmRlckxhYmVsKHByZWZpeENscykge1xuICAgICAgICByZXR1cm4gKDxGb3JtQ29udGV4dC5Db25zdW1lciBrZXk9XCJsYWJlbFwiPlxuICAgICAgICB7KHsgdmVydGljYWwsIGxhYmVsQWxpZ246IGNvbnRleHRMYWJlbEFsaWduLCBsYWJlbENvbDogY29udGV4dExhYmVsQ29sLCBjb2xvbjogY29udGV4dENvbG9uLCB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGxhYmVsLCBsYWJlbENvbCwgbGFiZWxBbGlnbiwgY29sb24sIGlkLCBodG1sRm9yIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgcmVxdWlyZWQgPSB0aGlzLmlzUmVxdWlyZWQoKTtcbiAgICAgICAgICAgIGNvbnN0IG1lcmdlZExhYmVsQ29sID0gKCdsYWJlbENvbCcgaW4gdGhpcy5wcm9wcyA/IGxhYmVsQ29sIDogY29udGV4dExhYmVsQ29sKSB8fCB7fTtcbiAgICAgICAgICAgIGNvbnN0IG1lcmdlZExhYmVsQWxpZ24gPSAnbGFiZWxBbGlnbicgaW4gdGhpcy5wcm9wcyA/IGxhYmVsQWxpZ24gOiBjb250ZXh0TGFiZWxBbGlnbjtcbiAgICAgICAgICAgIGNvbnN0IGxhYmVsQ2xzQmFzaWMgPSBgJHtwcmVmaXhDbHN9LWl0ZW0tbGFiZWxgO1xuICAgICAgICAgICAgY29uc3QgbGFiZWxDb2xDbGFzc05hbWUgPSBjbGFzc05hbWVzKGxhYmVsQ2xzQmFzaWMsIG1lcmdlZExhYmVsQWxpZ24gPT09ICdsZWZ0JyAmJiBgJHtsYWJlbENsc0Jhc2ljfS1sZWZ0YCwgbWVyZ2VkTGFiZWxDb2wuY2xhc3NOYW1lKTtcbiAgICAgICAgICAgIGxldCBsYWJlbENoaWxkcmVuID0gbGFiZWw7XG4gICAgICAgICAgICAvLyBLZWVwIGxhYmVsIGlzIG9yaWdpbmFsIHdoZXJlIHRoZXJlIHNob3VsZCBoYXZlIG5vIGNvbG9uXG4gICAgICAgICAgICBjb25zdCBjb21wdXRlZENvbG9uID0gY29sb24gPT09IHRydWUgfHwgKGNvbnRleHRDb2xvbiAhPT0gZmFsc2UgJiYgY29sb24gIT09IGZhbHNlKTtcbiAgICAgICAgICAgIGNvbnN0IGhhdmVDb2xvbiA9IGNvbXB1dGVkQ29sb24gJiYgIXZlcnRpY2FsO1xuICAgICAgICAgICAgLy8gUmVtb3ZlIGR1cGxpY2F0ZWQgdXNlciBpbnB1dCBjb2xvblxuICAgICAgICAgICAgaWYgKGhhdmVDb2xvbiAmJiB0eXBlb2YgbGFiZWwgPT09ICdzdHJpbmcnICYmIGxhYmVsLnRyaW0oKSAhPT0gJycpIHtcbiAgICAgICAgICAgICAgICBsYWJlbENoaWxkcmVuID0gbGFiZWwucmVwbGFjZSgvW++8mjpdXFxzKiQvLCAnJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBsYWJlbENsYXNzTmFtZSA9IGNsYXNzTmFtZXMoe1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWl0ZW0tcmVxdWlyZWRgXTogcmVxdWlyZWQsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30taXRlbS1uby1jb2xvbmBdOiAhY29tcHV0ZWRDb2xvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGxhYmVsID8gKDxDb2wgey4uLm1lcmdlZExhYmVsQ29sfSBjbGFzc05hbWU9e2xhYmVsQ29sQ2xhc3NOYW1lfT5cbiAgICAgICAgICAgICAgPGxhYmVsIGh0bWxGb3I9e2h0bWxGb3IgfHwgaWQgfHwgdGhpcy5nZXRJZCgpfSBjbGFzc05hbWU9e2xhYmVsQ2xhc3NOYW1lfSB0aXRsZT17dHlwZW9mIGxhYmVsID09PSAnc3RyaW5nJyA/IGxhYmVsIDogJyd9IG9uQ2xpY2s9e3RoaXMub25MYWJlbENsaWNrfT5cbiAgICAgICAgICAgICAgICB7bGFiZWxDaGlsZHJlbn1cbiAgICAgICAgICAgICAgPC9sYWJlbD5cbiAgICAgICAgICAgIDwvQ29sPikgOiBudWxsO1xuICAgICAgICB9fVxuICAgICAgPC9Gb3JtQ29udGV4dC5Db25zdW1lcj4pO1xuICAgIH1cbiAgICByZW5kZXJDaGlsZHJlbihwcmVmaXhDbHMpIHtcbiAgICAgICAgY29uc3QgeyBjaGlsZHJlbiB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgIHRoaXMucmVuZGVyTGFiZWwocHJlZml4Q2xzKSxcbiAgICAgICAgICAgIHRoaXMucmVuZGVyV3JhcHBlcihwcmVmaXhDbHMsIHRoaXMucmVuZGVyVmFsaWRhdGVXcmFwcGVyKHByZWZpeENscywgY2hpbGRyZW4sIHRoaXMucmVuZGVySGVscChwcmVmaXhDbHMpLCB0aGlzLnJlbmRlckV4dHJhKHByZWZpeENscykpKSxcbiAgICAgICAgXTtcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gPENvbmZpZ0NvbnN1bWVyPnt0aGlzLnJlbmRlckZvcm1JdGVtfTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cbkZvcm1JdGVtLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBoYXNGZWVkYmFjazogZmFsc2UsXG59O1xuRm9ybUl0ZW0ucHJvcFR5cGVzID0ge1xuICAgIHByZWZpeENsczogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBsYWJlbDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLm5vZGVdKSxcbiAgICBsYWJlbENvbDogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBoZWxwOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMubm9kZSwgUHJvcFR5cGVzLmJvb2xdKSxcbiAgICB2YWxpZGF0ZVN0YXR1czogUHJvcFR5cGVzLm9uZU9mKFZhbGlkYXRlU3RhdHVzZXMpLFxuICAgIGhhc0ZlZWRiYWNrOiBQcm9wVHlwZXMuYm9vbCxcbiAgICB3cmFwcGVyQ29sOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBpZDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLm5vZGUsXG4gICAgY29sb246IFByb3BUeXBlcy5ib29sLFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQVdBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXpCQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQXdCQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVRBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBS0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBYkE7QUFUQTtBQUNBO0FBdkJBO0FBZ0RBO0FBQ0E7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBSUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUlBO0FBTkE7QUFDQTtBQUNBO0FBT0E7QUFBQTtBQUFBO0FBVEE7QUFXQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFIQTtBQUtBO0FBQ0E7QUFBQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTs7O0FBQ0E7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBOzs7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBZkE7QUFDQTtBQWdCQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBT0E7OztBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBUEE7QUFhQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXJCQTtBQTJCQTs7O0FBQ0E7QUFBQTtBQUVBO0FBSUE7OztBQUNBO0FBQ0E7QUFDQTs7OztBQTlRQTtBQUNBOztBQStRQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFYQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/lib/form/FormItem.js
