__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_date__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-component/date */ "./src/app/desk/component/date.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/ph/componnets/extra-info.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var ExtraInfo =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(ExtraInfo, _ModelWidget);

  function ExtraInfo(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, ExtraInfo);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ExtraInfo).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(ExtraInfo, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setDob();
    }
  }, {
    key: "setDob",
    value: function setDob() {
      var dob = this.getValueFromModel(this.getDataIdProp("dob"));
      dob = _common__WEBPACK_IMPORTED_MODULE_12__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_12__["DateUtils"].toDate(dob));

      if (dob && dob.split("/").length === 3) {
        this.setState({
          dob: {
            day: dob.split("/")[0],
            month: dob.split("/")[1],
            year: dob.split("/")[2]
          }
        });
      }
    }
  }, {
    key: "getDataIdProp",
    value: function getDataIdProp(propName) {
      var _this$props = this.props,
          dataFixed = _this$props.dataFixed,
          propsNameFixed = _this$props.propsNameFixed;
      var extraPropsName = propsNameFixed ? "".concat(propsNameFixed) : "ext.policyholder";
      var propsName = dataFixed ? "".concat(dataFixed, ".").concat(extraPropsName) : extraPropsName;
      if (propName) return "".concat(propsName, ".").concat(propName);
      return "".concat(propsName);
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();

      var _this$props2 = this.props,
          model = _this$props2.model,
          form = _this$props2.form,
          dataFixed = _this$props2.dataFixed,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$props2, ["model", "form", "dataFixed"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NRadio"], {
        form: form,
        model: model,
        tableName: "gender",
        dataFixed: dataFixed,
        propName: this.getDataIdProp("gender"),
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Gender").thai("เพศ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_desk_component_date__WEBPACK_IMPORTED_MODULE_13__["DateText"], {
        form: form,
        model: model,
        defaultValue: lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(this.state, "dob", {}),
        propName: this.getDataIdProp("dob"),
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 73
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NSelect"], {
        form: form,
        model: model,
        propName: this.getDataIdProp("nationality"),
        tableName: "nationality",
        dataFixed: dataFixed,
        required: true,
        size: "large",
        style: {
          width: "100%"
        },
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Nationality").thai("Nationality").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        },
        __self: this
      })));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ExtraInfo.prototype), "initState", this).call(this), {
        dob: {}
      });
    }
  }]);

  return ExtraInfo;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (ExtraInfo);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BoL2NvbXBvbm5ldHMvZXh0cmEtaW5mby50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcGgvY29tcG9ubmV0cy9leHRyYS1pbmZvLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBOUmFkaW8sIE5TZWxlY3QgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IERhdGVVdGlscywgTGFuZ3VhZ2UgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXRQcm9wcyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCB7IERhdGVUZXh0IH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9kYXRlXCI7XG5cbnR5cGUgUGF5ZXJTdGF0ZSA9IHtcbiAgICBkb2I6IGFueSxcbn07XG5cbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBQYXllclBhZ2VDb21wb25lbnRzID0ge1xuICAgIEJveENvbnRlbnQ6IFN0eWxlZERJVjtcbn07XG5leHBvcnQgdHlwZSBQYXllclByb3BzID0ge1xuICAgIG1vZGVsOiBhbnk7XG4gICAgZm9ybTogYW55O1xuICAgIGRhdGFGaXhlZD86IGFueTtcbiAgICBwcm9wc05hbWVGaXhlZD86IGFueTtcbn0gJiBNb2RlbFdpZGdldFByb3BzO1xuXG5jbGFzcyBFeHRyYUluZm88UCBleHRlbmRzIFBheWVyUHJvcHMsIFMgZXh0ZW5kcyBQYXllclN0YXRlLCBDIGV4dGVuZHMgUGF5ZXJQYWdlQ29tcG9uZW50cz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcm9wczogUGF5ZXJQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgICAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gICAgfVxuXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIHRoaXMuc2V0RG9iKCk7XG4gICAgfVxuXG4gICAgc2V0RG9iKCkge1xuICAgICAgICBsZXQgZG9iID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldERhdGFJZFByb3AoXCJkb2JcIikpO1xuICAgICAgICBkb2IgPSBEYXRlVXRpbHMuZm9ybWF0RGF0ZShEYXRlVXRpbHMudG9EYXRlKGRvYikpO1xuICAgICAgICBpZiAoZG9iICYmIGRvYi5zcGxpdChcIi9cIikubGVuZ3RoID09PSAzKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBkb2I6IHtcbiAgICAgICAgICAgICAgICAgICAgZGF5OiBkb2Iuc3BsaXQoXCIvXCIpWzBdLFxuICAgICAgICAgICAgICAgICAgICBtb250aDogZG9iLnNwbGl0KFwiL1wiKVsxXSxcbiAgICAgICAgICAgICAgICAgICAgeWVhcjogZG9iLnNwbGl0KFwiL1wiKVsyXSxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSBhcyBhbnkpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0RGF0YUlkUHJvcChwcm9wTmFtZT86IHN0cmluZykge1xuICAgICAgICBjb25zdCB7ZGF0YUZpeGVkLCBwcm9wc05hbWVGaXhlZH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBjb25zdCBleHRyYVByb3BzTmFtZSA9IHByb3BzTmFtZUZpeGVkID8gYCR7cHJvcHNOYW1lRml4ZWR9YCA6IGBleHQucG9saWN5aG9sZGVyYDtcbiAgICAgICAgY29uc3QgcHJvcHNOYW1lID0gZGF0YUZpeGVkID8gYCR7ZGF0YUZpeGVkfS4ke2V4dHJhUHJvcHNOYW1lfWAgOiBleHRyYVByb3BzTmFtZTtcbiAgICAgICAgaWYocHJvcE5hbWUpIHJldHVybiBgJHtwcm9wc05hbWV9LiR7cHJvcE5hbWV9YDtcbiAgICAgICAgcmV0dXJuIGAke3Byb3BzTmFtZX1gO1xuICAgIH1cblxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgICAgICBjb25zdCB7bW9kZWwsIGZvcm0sIGRhdGFGaXhlZCwgLi4ucmVzdH0gPSB0aGlzLnByb3BzO1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPEMuQm94Q29udGVudCB7Li4ucmVzdH0+XG4gICAgICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICAgICAgPE5SYWRpb1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYmxlTmFtZT17XCJnZW5kZXJcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFGaXhlZD17ZGF0YUZpeGVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2V0RGF0YUlkUHJvcChgZ2VuZGVyYCl9XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIkdlbmRlclwiKS50aGFpKFwi4LmA4Lie4LioXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPERhdGVUZXh0XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFZhbHVlPXtfLmdldCh0aGlzLnN0YXRlLCBcImRvYlwiLCB7fSl9XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZXREYXRhSWRQcm9wKGBkb2JgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfS8+XG4gICAgICAgICAgICAgICAgICAgIDxOU2VsZWN0XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2V0RGF0YUlkUHJvcChgbmF0aW9uYWxpdHlgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYmxlTmFtZT17XCJuYXRpb25hbGl0eVwifVxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YUZpeGVkPXtkYXRhRml4ZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIk5hdGlvbmFsaXR5XCIpLnRoYWkoXCJOYXRpb25hbGl0eVwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC8+XG4gICAgICAgICAgICA8L0MuQm94Q29udGVudD5cbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBCb3hDb250ZW50OiBTdHlsZWQuZGl2YFxuICAgICAgICAgICAgYCxcbiAgICAgICAgfSBhcyBDO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICAgICAgICBkb2I6IHt9LFxuICAgICAgICB9KSBhcyBTO1xuICAgIH1cbn1cblxuXG5leHBvcnQgZGVmYXVsdCBFeHRyYUluZm87XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBaUJBOzs7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBREE7QUFPQTtBQUNBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7O0FBaEZBO0FBQ0E7QUFtRkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/ph/componnets/extra-info.tsx
