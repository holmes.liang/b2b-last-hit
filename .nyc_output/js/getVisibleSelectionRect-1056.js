/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getVisibleSelectionRect
 * @format
 * 
 */


var getRangeBoundingClientRect = __webpack_require__(/*! ./getRangeBoundingClientRect */ "./node_modules/draft-js/lib/getRangeBoundingClientRect.js");
/**
 * Return the bounding ClientRect for the visible DOM selection, if any.
 * In cases where there are no selected ranges or the bounding rect is
 * temporarily invalid, return null.
 */


function getVisibleSelectionRect(global) {
  var selection = global.getSelection();

  if (!selection.rangeCount) {
    return null;
  }

  var range = selection.getRangeAt(0);
  var boundingRect = getRangeBoundingClientRect(range);
  var top = boundingRect.top,
      right = boundingRect.right,
      bottom = boundingRect.bottom,
      left = boundingRect.left; // When a re-render leads to a node being removed, the DOM selection will
  // temporarily be placed on an ancestor node, which leads to an invalid
  // bounding rect. Discard this state.

  if (top === 0 && right === 0 && bottom === 0 && left === 0) {
    return null;
  }

  return boundingRect;
}

module.exports = getVisibleSelectionRect;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFZpc2libGVTZWxlY3Rpb25SZWN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFZpc2libGVTZWxlY3Rpb25SZWN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZ2V0VmlzaWJsZVNlbGVjdGlvblJlY3RcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGdldFJhbmdlQm91bmRpbmdDbGllbnRSZWN0ID0gcmVxdWlyZSgnLi9nZXRSYW5nZUJvdW5kaW5nQ2xpZW50UmVjdCcpO1xuXG4vKipcbiAqIFJldHVybiB0aGUgYm91bmRpbmcgQ2xpZW50UmVjdCBmb3IgdGhlIHZpc2libGUgRE9NIHNlbGVjdGlvbiwgaWYgYW55LlxuICogSW4gY2FzZXMgd2hlcmUgdGhlcmUgYXJlIG5vIHNlbGVjdGVkIHJhbmdlcyBvciB0aGUgYm91bmRpbmcgcmVjdCBpc1xuICogdGVtcG9yYXJpbHkgaW52YWxpZCwgcmV0dXJuIG51bGwuXG4gKi9cbmZ1bmN0aW9uIGdldFZpc2libGVTZWxlY3Rpb25SZWN0KGdsb2JhbCkge1xuICB2YXIgc2VsZWN0aW9uID0gZ2xvYmFsLmdldFNlbGVjdGlvbigpO1xuICBpZiAoIXNlbGVjdGlvbi5yYW5nZUNvdW50KSB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICB2YXIgcmFuZ2UgPSBzZWxlY3Rpb24uZ2V0UmFuZ2VBdCgwKTtcbiAgdmFyIGJvdW5kaW5nUmVjdCA9IGdldFJhbmdlQm91bmRpbmdDbGllbnRSZWN0KHJhbmdlKTtcbiAgdmFyIHRvcCA9IGJvdW5kaW5nUmVjdC50b3AsXG4gICAgICByaWdodCA9IGJvdW5kaW5nUmVjdC5yaWdodCxcbiAgICAgIGJvdHRvbSA9IGJvdW5kaW5nUmVjdC5ib3R0b20sXG4gICAgICBsZWZ0ID0gYm91bmRpbmdSZWN0LmxlZnQ7XG5cbiAgLy8gV2hlbiBhIHJlLXJlbmRlciBsZWFkcyB0byBhIG5vZGUgYmVpbmcgcmVtb3ZlZCwgdGhlIERPTSBzZWxlY3Rpb24gd2lsbFxuICAvLyB0ZW1wb3JhcmlseSBiZSBwbGFjZWQgb24gYW4gYW5jZXN0b3Igbm9kZSwgd2hpY2ggbGVhZHMgdG8gYW4gaW52YWxpZFxuICAvLyBib3VuZGluZyByZWN0LiBEaXNjYXJkIHRoaXMgc3RhdGUuXG5cbiAgaWYgKHRvcCA9PT0gMCAmJiByaWdodCA9PT0gMCAmJiBib3R0b20gPT09IDAgJiYgbGVmdCA9PT0gMCkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgcmV0dXJuIGJvdW5kaW5nUmVjdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBnZXRWaXNpYmxlU2VsZWN0aW9uUmVjdDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getVisibleSelectionRect.js
