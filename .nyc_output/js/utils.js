__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTargetRect", function() { return getTargetRect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFixedTop", function() { return getFixedTop; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFixedBottom", function() { return getFixedBottom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getObserverEntities", function() { return getObserverEntities; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addObserveTarget", function() { return addObserveTarget; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeObserveTarget", function() { return removeObserveTarget; });
/* harmony import */ var rc_util_es_Dom_addEventListener__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rc-util/es/Dom/addEventListener */ "./node_modules/rc-util/es/Dom/addEventListener.js");

function getTargetRect(target) {
  return target !== window ? target.getBoundingClientRect() : {
    top: 0,
    bottom: window.innerHeight
  };
}
function getFixedTop(placeholderReact, targetRect, offsetTop) {
  if (offsetTop !== undefined && targetRect.top > placeholderReact.top - offsetTop) {
    return offsetTop + targetRect.top;
  }

  return undefined;
}
function getFixedBottom(placeholderReact, targetRect, offsetBottom) {
  if (offsetBottom !== undefined && targetRect.bottom < placeholderReact.bottom + offsetBottom) {
    var targetBottomOffset = window.innerHeight - targetRect.bottom;
    return offsetBottom + targetBottomOffset;
  }

  return undefined;
} // ======================== Observer ========================

var TRIGGER_EVENTS = ['resize', 'scroll', 'touchstart', 'touchmove', 'touchend', 'pageshow', 'load'];
var observerEntities = [];
function getObserverEntities() {
  // Only used in test env. Can be removed if refactor.
  return observerEntities;
}
function addObserveTarget(target, affix) {
  if (!target) return;
  var entity = observerEntities.find(function (item) {
    return item.target === target;
  });

  if (entity) {
    entity.affixList.push(affix);
  } else {
    entity = {
      target: target,
      affixList: [affix],
      eventHandlers: {}
    };
    observerEntities.push(entity); // Add listener

    TRIGGER_EVENTS.forEach(function (eventName) {
      entity.eventHandlers[eventName] = Object(rc_util_es_Dom_addEventListener__WEBPACK_IMPORTED_MODULE_0__["default"])(target, eventName, function () {
        entity.affixList.forEach(function (targetAffix) {
          targetAffix.lazyUpdatePosition();
        });
      });
    });
  }
}
function removeObserveTarget(affix) {
  var observerEntity = observerEntities.find(function (oriObserverEntity) {
    var hasAffix = oriObserverEntity.affixList.some(function (item) {
      return item === affix;
    });

    if (hasAffix) {
      oriObserverEntity.affixList = oriObserverEntity.affixList.filter(function (item) {
        return item !== affix;
      });
    }

    return hasAffix;
  });

  if (observerEntity && observerEntity.affixList.length === 0) {
    observerEntities = observerEntities.filter(function (item) {
      return item !== observerEntity;
    }); // Remove listener

    TRIGGER_EVENTS.forEach(function (eventName) {
      var handler = observerEntity.eventHandlers[eventName];

      if (handler && handler.remove) {
        handler.remove();
      }
    });
  }
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9hZmZpeC91dGlscy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvYWZmaXgvdXRpbHMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGFkZEV2ZW50TGlzdGVuZXIgZnJvbSAncmMtdXRpbC9saWIvRG9tL2FkZEV2ZW50TGlzdGVuZXInO1xuZXhwb3J0IGZ1bmN0aW9uIGdldFRhcmdldFJlY3QodGFyZ2V0KSB7XG4gICAgcmV0dXJuIHRhcmdldCAhPT0gd2luZG93XG4gICAgICAgID8gdGFyZ2V0LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpXG4gICAgICAgIDogeyB0b3A6IDAsIGJvdHRvbTogd2luZG93LmlubmVySGVpZ2h0IH07XG59XG5leHBvcnQgZnVuY3Rpb24gZ2V0Rml4ZWRUb3AocGxhY2Vob2xkZXJSZWFjdCwgdGFyZ2V0UmVjdCwgb2Zmc2V0VG9wKSB7XG4gICAgaWYgKG9mZnNldFRvcCAhPT0gdW5kZWZpbmVkICYmIHRhcmdldFJlY3QudG9wID4gcGxhY2Vob2xkZXJSZWFjdC50b3AgLSBvZmZzZXRUb3ApIHtcbiAgICAgICAgcmV0dXJuIG9mZnNldFRvcCArIHRhcmdldFJlY3QudG9wO1xuICAgIH1cbiAgICByZXR1cm4gdW5kZWZpbmVkO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldEZpeGVkQm90dG9tKHBsYWNlaG9sZGVyUmVhY3QsIHRhcmdldFJlY3QsIG9mZnNldEJvdHRvbSkge1xuICAgIGlmIChvZmZzZXRCb3R0b20gIT09IHVuZGVmaW5lZCAmJiB0YXJnZXRSZWN0LmJvdHRvbSA8IHBsYWNlaG9sZGVyUmVhY3QuYm90dG9tICsgb2Zmc2V0Qm90dG9tKSB7XG4gICAgICAgIGNvbnN0IHRhcmdldEJvdHRvbU9mZnNldCA9IHdpbmRvdy5pbm5lckhlaWdodCAtIHRhcmdldFJlY3QuYm90dG9tO1xuICAgICAgICByZXR1cm4gb2Zmc2V0Qm90dG9tICsgdGFyZ2V0Qm90dG9tT2Zmc2V0O1xuICAgIH1cbiAgICByZXR1cm4gdW5kZWZpbmVkO1xufVxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09IE9ic2VydmVyID09PT09PT09PT09PT09PT09PT09PT09PVxuY29uc3QgVFJJR0dFUl9FVkVOVFMgPSBbXG4gICAgJ3Jlc2l6ZScsXG4gICAgJ3Njcm9sbCcsXG4gICAgJ3RvdWNoc3RhcnQnLFxuICAgICd0b3VjaG1vdmUnLFxuICAgICd0b3VjaGVuZCcsXG4gICAgJ3BhZ2VzaG93JyxcbiAgICAnbG9hZCcsXG5dO1xubGV0IG9ic2VydmVyRW50aXRpZXMgPSBbXTtcbmV4cG9ydCBmdW5jdGlvbiBnZXRPYnNlcnZlckVudGl0aWVzKCkge1xuICAgIC8vIE9ubHkgdXNlZCBpbiB0ZXN0IGVudi4gQ2FuIGJlIHJlbW92ZWQgaWYgcmVmYWN0b3IuXG4gICAgcmV0dXJuIG9ic2VydmVyRW50aXRpZXM7XG59XG5leHBvcnQgZnVuY3Rpb24gYWRkT2JzZXJ2ZVRhcmdldCh0YXJnZXQsIGFmZml4KSB7XG4gICAgaWYgKCF0YXJnZXQpXG4gICAgICAgIHJldHVybjtcbiAgICBsZXQgZW50aXR5ID0gb2JzZXJ2ZXJFbnRpdGllcy5maW5kKGl0ZW0gPT4gaXRlbS50YXJnZXQgPT09IHRhcmdldCk7XG4gICAgaWYgKGVudGl0eSkge1xuICAgICAgICBlbnRpdHkuYWZmaXhMaXN0LnB1c2goYWZmaXgpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgZW50aXR5ID0ge1xuICAgICAgICAgICAgdGFyZ2V0LFxuICAgICAgICAgICAgYWZmaXhMaXN0OiBbYWZmaXhdLFxuICAgICAgICAgICAgZXZlbnRIYW5kbGVyczoge30sXG4gICAgICAgIH07XG4gICAgICAgIG9ic2VydmVyRW50aXRpZXMucHVzaChlbnRpdHkpO1xuICAgICAgICAvLyBBZGQgbGlzdGVuZXJcbiAgICAgICAgVFJJR0dFUl9FVkVOVFMuZm9yRWFjaChldmVudE5hbWUgPT4ge1xuICAgICAgICAgICAgZW50aXR5LmV2ZW50SGFuZGxlcnNbZXZlbnROYW1lXSA9IGFkZEV2ZW50TGlzdGVuZXIodGFyZ2V0LCBldmVudE5hbWUsICgpID0+IHtcbiAgICAgICAgICAgICAgICBlbnRpdHkuYWZmaXhMaXN0LmZvckVhY2godGFyZ2V0QWZmaXggPT4ge1xuICAgICAgICAgICAgICAgICAgICB0YXJnZXRBZmZpeC5sYXp5VXBkYXRlUG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG59XG5leHBvcnQgZnVuY3Rpb24gcmVtb3ZlT2JzZXJ2ZVRhcmdldChhZmZpeCkge1xuICAgIGNvbnN0IG9ic2VydmVyRW50aXR5ID0gb2JzZXJ2ZXJFbnRpdGllcy5maW5kKG9yaU9ic2VydmVyRW50aXR5ID0+IHtcbiAgICAgICAgY29uc3QgaGFzQWZmaXggPSBvcmlPYnNlcnZlckVudGl0eS5hZmZpeExpc3Quc29tZShpdGVtID0+IGl0ZW0gPT09IGFmZml4KTtcbiAgICAgICAgaWYgKGhhc0FmZml4KSB7XG4gICAgICAgICAgICBvcmlPYnNlcnZlckVudGl0eS5hZmZpeExpc3QgPSBvcmlPYnNlcnZlckVudGl0eS5hZmZpeExpc3QuZmlsdGVyKGl0ZW0gPT4gaXRlbSAhPT0gYWZmaXgpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBoYXNBZmZpeDtcbiAgICB9KTtcbiAgICBpZiAob2JzZXJ2ZXJFbnRpdHkgJiYgb2JzZXJ2ZXJFbnRpdHkuYWZmaXhMaXN0Lmxlbmd0aCA9PT0gMCkge1xuICAgICAgICBvYnNlcnZlckVudGl0aWVzID0gb2JzZXJ2ZXJFbnRpdGllcy5maWx0ZXIoaXRlbSA9PiBpdGVtICE9PSBvYnNlcnZlckVudGl0eSk7XG4gICAgICAgIC8vIFJlbW92ZSBsaXN0ZW5lclxuICAgICAgICBUUklHR0VSX0VWRU5UUy5mb3JFYWNoKGV2ZW50TmFtZSA9PiB7XG4gICAgICAgICAgICBjb25zdCBoYW5kbGVyID0gb2JzZXJ2ZXJFbnRpdHkuZXZlbnRIYW5kbGVyc1tldmVudE5hbWVdO1xuICAgICAgICAgICAgaWYgKGhhbmRsZXIgJiYgaGFuZGxlci5yZW1vdmUpIHtcbiAgICAgICAgICAgICAgICBoYW5kbGVyLnJlbW92ZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG59XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFEQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUxBO0FBQ0E7QUFNQTtBQUNBO0FBQUE7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/affix/utils.js
