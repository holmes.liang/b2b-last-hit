/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule SecondaryClipboard
 * @format
 * 
 */


var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var getContentStateFragment = __webpack_require__(/*! ./getContentStateFragment */ "./node_modules/draft-js/lib/getContentStateFragment.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");

var clipboard = null;
/**
 * Some systems offer a "secondary" clipboard to allow quick internal cut
 * and paste behavior. For instance, Ctrl+K (cut) and Ctrl+Y (paste).
 */

var SecondaryClipboard = {
  cut: function cut(editorState) {
    var content = editorState.getCurrentContent();
    var selection = editorState.getSelection();
    var targetRange = null;

    if (selection.isCollapsed()) {
      var anchorKey = selection.getAnchorKey();
      var blockEnd = content.getBlockForKey(anchorKey).getLength();

      if (blockEnd === selection.getAnchorOffset()) {
        return editorState;
      }

      targetRange = selection.set('focusOffset', blockEnd);
    } else {
      targetRange = selection;
    }

    targetRange = nullthrows(targetRange);
    clipboard = getContentStateFragment(content, targetRange);
    var afterRemoval = DraftModifier.removeRange(content, targetRange, 'forward');

    if (afterRemoval === content) {
      return editorState;
    }

    return EditorState.push(editorState, afterRemoval, 'remove-range');
  },
  paste: function paste(editorState) {
    if (!clipboard) {
      return editorState;
    }

    var newContent = DraftModifier.replaceWithFragment(editorState.getCurrentContent(), editorState.getSelection(), clipboard);
    return EditorState.push(editorState, newContent, 'insert-fragment');
  }
};
module.exports = SecondaryClipboard;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL1NlY29uZGFyeUNsaXBib2FyZC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9TZWNvbmRhcnlDbGlwYm9hcmQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBTZWNvbmRhcnlDbGlwYm9hcmRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcblxudmFyIGdldENvbnRlbnRTdGF0ZUZyYWdtZW50ID0gcmVxdWlyZSgnLi9nZXRDb250ZW50U3RhdGVGcmFnbWVudCcpO1xudmFyIG51bGx0aHJvd3MgPSByZXF1aXJlKCdmYmpzL2xpYi9udWxsdGhyb3dzJyk7XG5cbnZhciBjbGlwYm9hcmQgPSBudWxsO1xuXG4vKipcbiAqIFNvbWUgc3lzdGVtcyBvZmZlciBhIFwic2Vjb25kYXJ5XCIgY2xpcGJvYXJkIHRvIGFsbG93IHF1aWNrIGludGVybmFsIGN1dFxuICogYW5kIHBhc3RlIGJlaGF2aW9yLiBGb3IgaW5zdGFuY2UsIEN0cmwrSyAoY3V0KSBhbmQgQ3RybCtZIChwYXN0ZSkuXG4gKi9cbnZhciBTZWNvbmRhcnlDbGlwYm9hcmQgPSB7XG4gIGN1dDogZnVuY3Rpb24gY3V0KGVkaXRvclN0YXRlKSB7XG4gICAgdmFyIGNvbnRlbnQgPSBlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpO1xuICAgIHZhciBzZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcbiAgICB2YXIgdGFyZ2V0UmFuZ2UgPSBudWxsO1xuXG4gICAgaWYgKHNlbGVjdGlvbi5pc0NvbGxhcHNlZCgpKSB7XG4gICAgICB2YXIgYW5jaG9yS2V5ID0gc2VsZWN0aW9uLmdldEFuY2hvcktleSgpO1xuICAgICAgdmFyIGJsb2NrRW5kID0gY29udGVudC5nZXRCbG9ja0ZvcktleShhbmNob3JLZXkpLmdldExlbmd0aCgpO1xuXG4gICAgICBpZiAoYmxvY2tFbmQgPT09IHNlbGVjdGlvbi5nZXRBbmNob3JPZmZzZXQoKSkge1xuICAgICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgICB9XG5cbiAgICAgIHRhcmdldFJhbmdlID0gc2VsZWN0aW9uLnNldCgnZm9jdXNPZmZzZXQnLCBibG9ja0VuZCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRhcmdldFJhbmdlID0gc2VsZWN0aW9uO1xuICAgIH1cblxuICAgIHRhcmdldFJhbmdlID0gbnVsbHRocm93cyh0YXJnZXRSYW5nZSk7XG4gICAgY2xpcGJvYXJkID0gZ2V0Q29udGVudFN0YXRlRnJhZ21lbnQoY29udGVudCwgdGFyZ2V0UmFuZ2UpO1xuXG4gICAgdmFyIGFmdGVyUmVtb3ZhbCA9IERyYWZ0TW9kaWZpZXIucmVtb3ZlUmFuZ2UoY29udGVudCwgdGFyZ2V0UmFuZ2UsICdmb3J3YXJkJyk7XG5cbiAgICBpZiAoYWZ0ZXJSZW1vdmFsID09PSBjb250ZW50KSB7XG4gICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgfVxuXG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIGFmdGVyUmVtb3ZhbCwgJ3JlbW92ZS1yYW5nZScpO1xuICB9LFxuXG4gIHBhc3RlOiBmdW5jdGlvbiBwYXN0ZShlZGl0b3JTdGF0ZSkge1xuICAgIGlmICghY2xpcGJvYXJkKSB7XG4gICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgfVxuXG4gICAgdmFyIG5ld0NvbnRlbnQgPSBEcmFmdE1vZGlmaWVyLnJlcGxhY2VXaXRoRnJhZ21lbnQoZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKSwgZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCksIGNsaXBib2FyZCk7XG5cbiAgICByZXR1cm4gRWRpdG9yU3RhdGUucHVzaChlZGl0b3JTdGF0ZSwgbmV3Q29udGVudCwgJ2luc2VydC1mcmFnbWVudCcpO1xuICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IFNlY29uZGFyeUNsaXBib2FyZDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUF2Q0E7QUEwQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/SecondaryClipboard.js
