__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SiderContext", function() { return SiderContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Sider; });
/* harmony import */ var _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ant-design/create-react-context */ "./node_modules/@ant-design/create-react-context/lib/index.js");
/* harmony import */ var _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./layout */ "./node_modules/antd/es/layout/layout.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _util_isNumeric__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_util/isNumeric */ "./node_modules/antd/es/_util/isNumeric.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};









 // matchMedia polyfill for
// https://github.com/WickyNilliams/enquire.js/issues/82
// TODO: Will be removed in antd 4.0 because we will no longer support ie9

if (typeof window !== 'undefined') {
  var matchMediaPolyfill = function matchMediaPolyfill(mediaQuery) {
    return {
      media: mediaQuery,
      matches: false,
      addListener: function addListener() {},
      removeListener: function removeListener() {}
    };
  }; // ref: https://github.com/ant-design/ant-design/issues/18774


  if (!window.matchMedia) window.matchMedia = matchMediaPolyfill;
}

var dimensionMaxMap = {
  xs: '479.98px',
  sm: '575.98px',
  md: '767.98px',
  lg: '991.98px',
  xl: '1199.98px',
  xxl: '1599.98px'
};
var SiderContext = _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0___default()({});

var generateId = function () {
  var i = 0;
  return function () {
    var prefix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    i += 1;
    return "".concat(prefix).concat(i);
  };
}();

var InternalSider =
/*#__PURE__*/
function (_React$Component) {
  _inherits(InternalSider, _React$Component);

  function InternalSider(props) {
    var _this;

    _classCallCheck(this, InternalSider);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(InternalSider).call(this, props));

    _this.responsiveHandler = function (mql) {
      _this.setState({
        below: mql.matches
      });

      var onBreakpoint = _this.props.onBreakpoint;

      if (onBreakpoint) {
        onBreakpoint(mql.matches);
      }

      if (_this.state.collapsed !== mql.matches) {
        _this.setCollapsed(mql.matches, 'responsive');
      }
    };

    _this.setCollapsed = function (collapsed, type) {
      if (!('collapsed' in _this.props)) {
        _this.setState({
          collapsed: collapsed
        });
      }

      var onCollapse = _this.props.onCollapse;

      if (onCollapse) {
        onCollapse(collapsed, type);
      }
    };

    _this.toggle = function () {
      var collapsed = !_this.state.collapsed;

      _this.setCollapsed(collapsed, 'clickTrigger');
    };

    _this.belowShowChange = function () {
      _this.setState(function (_ref) {
        var belowShow = _ref.belowShow;
        return {
          belowShow: !belowShow
        };
      });
    };

    _this.renderSider = function (_ref2) {
      var _classNames;

      var getPrefixCls = _ref2.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          className = _a.className,
          theme = _a.theme,
          collapsible = _a.collapsible,
          reverseArrow = _a.reverseArrow,
          trigger = _a.trigger,
          style = _a.style,
          width = _a.width,
          collapsedWidth = _a.collapsedWidth,
          zeroWidthTriggerStyle = _a.zeroWidthTriggerStyle,
          others = __rest(_a, ["prefixCls", "className", "theme", "collapsible", "reverseArrow", "trigger", "style", "width", "collapsedWidth", "zeroWidthTriggerStyle"]);

      var prefixCls = getPrefixCls('layout-sider', customizePrefixCls);
      var divProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_4__["default"])(others, ['collapsed', 'defaultCollapsed', 'onCollapse', 'breakpoint', 'onBreakpoint', 'siderHook', 'zeroWidthTriggerStyle']);
      var rawWidth = _this.state.collapsed ? collapsedWidth : width; // use "px" as fallback unit for width

      var siderWidth = Object(_util_isNumeric__WEBPACK_IMPORTED_MODULE_8__["default"])(rawWidth) ? "".concat(rawWidth, "px") : String(rawWidth); // special trigger when collapsedWidth == 0

      var zeroWidthTrigger = parseFloat(String(collapsedWidth || 0)) === 0 ? react__WEBPACK_IMPORTED_MODULE_1__["createElement"]("span", {
        onClick: _this.toggle,
        className: "".concat(prefixCls, "-zero-width-trigger ").concat(prefixCls, "-zero-width-trigger-").concat(reverseArrow ? 'right' : 'left'),
        style: zeroWidthTriggerStyle
      }, react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_7__["default"], {
        type: "bars"
      })) : null;
      var iconObj = {
        expanded: reverseArrow ? react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_7__["default"], {
          type: "right"
        }) : react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_7__["default"], {
          type: "left"
        }),
        collapsed: reverseArrow ? react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_7__["default"], {
          type: "left"
        }) : react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_7__["default"], {
          type: "right"
        })
      };
      var status = _this.state.collapsed ? 'collapsed' : 'expanded';
      var defaultTrigger = iconObj[status];
      var triggerDom = trigger !== null ? zeroWidthTrigger || react__WEBPACK_IMPORTED_MODULE_1__["createElement"]("div", {
        className: "".concat(prefixCls, "-trigger"),
        onClick: _this.toggle,
        style: {
          width: siderWidth
        }
      }, trigger || defaultTrigger) : null;

      var divStyle = _extends(_extends({}, style), {
        flex: "0 0 ".concat(siderWidth),
        maxWidth: siderWidth,
        minWidth: siderWidth,
        width: siderWidth
      });

      var siderCls = classnames__WEBPACK_IMPORTED_MODULE_3___default()(className, prefixCls, "".concat(prefixCls, "-").concat(theme), (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-collapsed"), !!_this.state.collapsed), _defineProperty(_classNames, "".concat(prefixCls, "-has-trigger"), collapsible && trigger !== null && !zeroWidthTrigger), _defineProperty(_classNames, "".concat(prefixCls, "-below"), !!_this.state.below), _defineProperty(_classNames, "".concat(prefixCls, "-zero-width"), parseFloat(siderWidth) === 0), _classNames));
      return react__WEBPACK_IMPORTED_MODULE_1__["createElement"]("aside", _extends({
        className: siderCls
      }, divProps, {
        style: divStyle
      }), react__WEBPACK_IMPORTED_MODULE_1__["createElement"]("div", {
        className: "".concat(prefixCls, "-children")
      }, _this.props.children), collapsible || _this.state.below && zeroWidthTrigger ? triggerDom : null);
    };

    _this.uniqueId = generateId('ant-sider-');
    var matchMedia;

    if (typeof window !== 'undefined') {
      matchMedia = window.matchMedia;
    }

    if (matchMedia && props.breakpoint && props.breakpoint in dimensionMaxMap) {
      _this.mql = matchMedia("(max-width: ".concat(dimensionMaxMap[props.breakpoint], ")"));
    }

    var collapsed;

    if ('collapsed' in props) {
      collapsed = props.collapsed;
    } else {
      collapsed = props.defaultCollapsed;
    }

    _this.state = {
      collapsed: collapsed,
      below: false
    };
    return _this;
  }

  _createClass(InternalSider, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.mql) {
        this.mql.addListener(this.responsiveHandler);
        this.responsiveHandler(this.mql);
      }

      if (this.props.siderHook) {
        this.props.siderHook.addSider(this.uniqueId);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.mql) {
        this.mql.removeListener(this.responsiveHandler);
      }

      if (this.props.siderHook) {
        this.props.siderHook.removeSider(this.uniqueId);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var collapsed = this.state.collapsed;
      var collapsedWidth = this.props.collapsedWidth;
      return react__WEBPACK_IMPORTED_MODULE_1__["createElement"](SiderContext.Provider, {
        value: {
          siderCollapsed: collapsed,
          collapsedWidth: collapsedWidth
        }
      }, react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderSider));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps) {
      if ('collapsed' in nextProps) {
        return {
          collapsed: nextProps.collapsed
        };
      }

      return null;
    }
  }]);

  return InternalSider;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

InternalSider.defaultProps = {
  collapsible: false,
  defaultCollapsed: false,
  reverseArrow: false,
  width: 200,
  collapsedWidth: 80,
  style: {},
  theme: 'dark'
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__["polyfill"])(InternalSider); // eslint-disable-next-line react/prefer-stateless-function

var Sider =
/*#__PURE__*/
function (_React$Component2) {
  _inherits(Sider, _React$Component2);

  function Sider() {
    _classCallCheck(this, Sider);

    return _possibleConstructorReturn(this, _getPrototypeOf(Sider).apply(this, arguments));
  }

  _createClass(Sider, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_layout__WEBPACK_IMPORTED_MODULE_5__["LayoutContext"].Consumer, null, function (context) {
        return react__WEBPACK_IMPORTED_MODULE_1__["createElement"](InternalSider, _extends({}, context, _this2.props));
      });
    }
  }]);

  return Sider;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9sYXlvdXQvU2lkZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2xheW91dC9TaWRlci5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0IGNyZWF0ZUNvbnRleHQgZnJvbSAnQGFudC1kZXNpZ24vY3JlYXRlLXJlYWN0LWNvbnRleHQnO1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgcG9seWZpbGwgfSBmcm9tICdyZWFjdC1saWZlY3ljbGVzLWNvbXBhdCc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBvbWl0IGZyb20gJ29taXQuanMnO1xuaW1wb3J0IHsgTGF5b3V0Q29udGV4dCB9IGZyb20gJy4vbGF5b3V0JztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmltcG9ydCBJY29uIGZyb20gJy4uL2ljb24nO1xuaW1wb3J0IGlzTnVtZXJpYyBmcm9tICcuLi9fdXRpbC9pc051bWVyaWMnO1xuLy8gbWF0Y2hNZWRpYSBwb2x5ZmlsbCBmb3Jcbi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9XaWNreU5pbGxpYW1zL2VucXVpcmUuanMvaXNzdWVzLzgyXG4vLyBUT0RPOiBXaWxsIGJlIHJlbW92ZWQgaW4gYW50ZCA0LjAgYmVjYXVzZSB3ZSB3aWxsIG5vIGxvbmdlciBzdXBwb3J0IGllOVxuaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgY29uc3QgbWF0Y2hNZWRpYVBvbHlmaWxsID0gKG1lZGlhUXVlcnkpID0+IHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIG1lZGlhOiBtZWRpYVF1ZXJ5LFxuICAgICAgICAgICAgbWF0Y2hlczogZmFsc2UsXG4gICAgICAgICAgICBhZGRMaXN0ZW5lcigpIHsgfSxcbiAgICAgICAgICAgIHJlbW92ZUxpc3RlbmVyKCkgeyB9LFxuICAgICAgICB9O1xuICAgIH07XG4gICAgLy8gcmVmOiBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xODc3NFxuICAgIGlmICghd2luZG93Lm1hdGNoTWVkaWEpXG4gICAgICAgIHdpbmRvdy5tYXRjaE1lZGlhID0gbWF0Y2hNZWRpYVBvbHlmaWxsO1xufVxuY29uc3QgZGltZW5zaW9uTWF4TWFwID0ge1xuICAgIHhzOiAnNDc5Ljk4cHgnLFxuICAgIHNtOiAnNTc1Ljk4cHgnLFxuICAgIG1kOiAnNzY3Ljk4cHgnLFxuICAgIGxnOiAnOTkxLjk4cHgnLFxuICAgIHhsOiAnMTE5OS45OHB4JyxcbiAgICB4eGw6ICcxNTk5Ljk4cHgnLFxufTtcbmV4cG9ydCBjb25zdCBTaWRlckNvbnRleHQgPSBjcmVhdGVDb250ZXh0KHt9KTtcbmNvbnN0IGdlbmVyYXRlSWQgPSAoKCkgPT4ge1xuICAgIGxldCBpID0gMDtcbiAgICByZXR1cm4gKHByZWZpeCA9ICcnKSA9PiB7XG4gICAgICAgIGkgKz0gMTtcbiAgICAgICAgcmV0dXJuIGAke3ByZWZpeH0ke2l9YDtcbiAgICB9O1xufSkoKTtcbmNsYXNzIEludGVybmFsU2lkZXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgICAgIHN1cGVyKHByb3BzKTtcbiAgICAgICAgdGhpcy5yZXNwb25zaXZlSGFuZGxlciA9IChtcWwpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBiZWxvdzogbXFsLm1hdGNoZXMgfSk7XG4gICAgICAgICAgICBjb25zdCB7IG9uQnJlYWtwb2ludCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChvbkJyZWFrcG9pbnQpIHtcbiAgICAgICAgICAgICAgICBvbkJyZWFrcG9pbnQobXFsLm1hdGNoZXMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuY29sbGFwc2VkICE9PSBtcWwubWF0Y2hlcykge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0Q29sbGFwc2VkKG1xbC5tYXRjaGVzLCAncmVzcG9uc2l2ZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLnNldENvbGxhcHNlZCA9IChjb2xsYXBzZWQsIHR5cGUpID0+IHtcbiAgICAgICAgICAgIGlmICghKCdjb2xsYXBzZWQnIGluIHRoaXMucHJvcHMpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGNvbGxhcHNlZCxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IHsgb25Db2xsYXBzZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChvbkNvbGxhcHNlKSB7XG4gICAgICAgICAgICAgICAgb25Db2xsYXBzZShjb2xsYXBzZWQsIHR5cGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLnRvZ2dsZSA9ICgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGNvbGxhcHNlZCA9ICF0aGlzLnN0YXRlLmNvbGxhcHNlZDtcbiAgICAgICAgICAgIHRoaXMuc2V0Q29sbGFwc2VkKGNvbGxhcHNlZCwgJ2NsaWNrVHJpZ2dlcicpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmJlbG93U2hvd0NoYW5nZSA9ICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoKHsgYmVsb3dTaG93IH0pID0+ICh7IGJlbG93U2hvdzogIWJlbG93U2hvdyB9KSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyU2lkZXIgPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgX2EgPSB0aGlzLnByb3BzLCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBjbGFzc05hbWUsIHRoZW1lLCBjb2xsYXBzaWJsZSwgcmV2ZXJzZUFycm93LCB0cmlnZ2VyLCBzdHlsZSwgd2lkdGgsIGNvbGxhcHNlZFdpZHRoLCB6ZXJvV2lkdGhUcmlnZ2VyU3R5bGUgfSA9IF9hLCBvdGhlcnMgPSBfX3Jlc3QoX2EsIFtcInByZWZpeENsc1wiLCBcImNsYXNzTmFtZVwiLCBcInRoZW1lXCIsIFwiY29sbGFwc2libGVcIiwgXCJyZXZlcnNlQXJyb3dcIiwgXCJ0cmlnZ2VyXCIsIFwic3R5bGVcIiwgXCJ3aWR0aFwiLCBcImNvbGxhcHNlZFdpZHRoXCIsIFwiemVyb1dpZHRoVHJpZ2dlclN0eWxlXCJdKTtcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnbGF5b3V0LXNpZGVyJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGRpdlByb3BzID0gb21pdChvdGhlcnMsIFtcbiAgICAgICAgICAgICAgICAnY29sbGFwc2VkJyxcbiAgICAgICAgICAgICAgICAnZGVmYXVsdENvbGxhcHNlZCcsXG4gICAgICAgICAgICAgICAgJ29uQ29sbGFwc2UnLFxuICAgICAgICAgICAgICAgICdicmVha3BvaW50JyxcbiAgICAgICAgICAgICAgICAnb25CcmVha3BvaW50JyxcbiAgICAgICAgICAgICAgICAnc2lkZXJIb29rJyxcbiAgICAgICAgICAgICAgICAnemVyb1dpZHRoVHJpZ2dlclN0eWxlJyxcbiAgICAgICAgICAgIF0pO1xuICAgICAgICAgICAgY29uc3QgcmF3V2lkdGggPSB0aGlzLnN0YXRlLmNvbGxhcHNlZCA/IGNvbGxhcHNlZFdpZHRoIDogd2lkdGg7XG4gICAgICAgICAgICAvLyB1c2UgXCJweFwiIGFzIGZhbGxiYWNrIHVuaXQgZm9yIHdpZHRoXG4gICAgICAgICAgICBjb25zdCBzaWRlcldpZHRoID0gaXNOdW1lcmljKHJhd1dpZHRoKSA/IGAke3Jhd1dpZHRofXB4YCA6IFN0cmluZyhyYXdXaWR0aCk7XG4gICAgICAgICAgICAvLyBzcGVjaWFsIHRyaWdnZXIgd2hlbiBjb2xsYXBzZWRXaWR0aCA9PSAwXG4gICAgICAgICAgICBjb25zdCB6ZXJvV2lkdGhUcmlnZ2VyID0gcGFyc2VGbG9hdChTdHJpbmcoY29sbGFwc2VkV2lkdGggfHwgMCkpID09PSAwID8gKDxzcGFuIG9uQ2xpY2s9e3RoaXMudG9nZ2xlfSBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30temVyby13aWR0aC10cmlnZ2VyICR7cHJlZml4Q2xzfS16ZXJvLXdpZHRoLXRyaWdnZXItJHtyZXZlcnNlQXJyb3cgPyAncmlnaHQnIDogJ2xlZnQnfWB9IHN0eWxlPXt6ZXJvV2lkdGhUcmlnZ2VyU3R5bGV9PlxuICAgICAgICAgIDxJY29uIHR5cGU9XCJiYXJzXCIvPlxuICAgICAgICA8L3NwYW4+KSA6IG51bGw7XG4gICAgICAgICAgICBjb25zdCBpY29uT2JqID0ge1xuICAgICAgICAgICAgICAgIGV4cGFuZGVkOiByZXZlcnNlQXJyb3cgPyA8SWNvbiB0eXBlPVwicmlnaHRcIi8+IDogPEljb24gdHlwZT1cImxlZnRcIi8+LFxuICAgICAgICAgICAgICAgIGNvbGxhcHNlZDogcmV2ZXJzZUFycm93ID8gPEljb24gdHlwZT1cImxlZnRcIi8+IDogPEljb24gdHlwZT1cInJpZ2h0XCIvPixcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBjb25zdCBzdGF0dXMgPSB0aGlzLnN0YXRlLmNvbGxhcHNlZCA/ICdjb2xsYXBzZWQnIDogJ2V4cGFuZGVkJztcbiAgICAgICAgICAgIGNvbnN0IGRlZmF1bHRUcmlnZ2VyID0gaWNvbk9ialtzdGF0dXNdO1xuICAgICAgICAgICAgY29uc3QgdHJpZ2dlckRvbSA9IHRyaWdnZXIgIT09IG51bGxcbiAgICAgICAgICAgICAgICA/IHplcm9XaWR0aFRyaWdnZXIgfHwgKDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LXRyaWdnZXJgfSBvbkNsaWNrPXt0aGlzLnRvZ2dsZX0gc3R5bGU9e3sgd2lkdGg6IHNpZGVyV2lkdGggfX0+XG4gICAgICAgICAgICAgIHt0cmlnZ2VyIHx8IGRlZmF1bHRUcmlnZ2VyfVxuICAgICAgICAgICAgPC9kaXY+KVxuICAgICAgICAgICAgICAgIDogbnVsbDtcbiAgICAgICAgICAgIGNvbnN0IGRpdlN0eWxlID0gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBzdHlsZSksIHsgZmxleDogYDAgMCAke3NpZGVyV2lkdGh9YCwgbWF4V2lkdGg6IHNpZGVyV2lkdGgsIG1pbldpZHRoOiBzaWRlcldpZHRoLCB3aWR0aDogc2lkZXJXaWR0aCB9KTtcbiAgICAgICAgICAgIGNvbnN0IHNpZGVyQ2xzID0gY2xhc3NOYW1lcyhjbGFzc05hbWUsIHByZWZpeENscywgYCR7cHJlZml4Q2xzfS0ke3RoZW1lfWAsIHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1jb2xsYXBzZWRgXTogISF0aGlzLnN0YXRlLmNvbGxhcHNlZCxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1oYXMtdHJpZ2dlcmBdOiBjb2xsYXBzaWJsZSAmJiB0cmlnZ2VyICE9PSBudWxsICYmICF6ZXJvV2lkdGhUcmlnZ2VyLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWJlbG93YF06ICEhdGhpcy5zdGF0ZS5iZWxvdyxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS16ZXJvLXdpZHRoYF06IHBhcnNlRmxvYXQoc2lkZXJXaWR0aCkgPT09IDAsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiAoPGFzaWRlIGNsYXNzTmFtZT17c2lkZXJDbHN9IHsuLi5kaXZQcm9wc30gc3R5bGU9e2RpdlN0eWxlfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tY2hpbGRyZW5gfT57dGhpcy5wcm9wcy5jaGlsZHJlbn08L2Rpdj5cbiAgICAgICAge2NvbGxhcHNpYmxlIHx8ICh0aGlzLnN0YXRlLmJlbG93ICYmIHplcm9XaWR0aFRyaWdnZXIpID8gdHJpZ2dlckRvbSA6IG51bGx9XG4gICAgICA8L2FzaWRlPik7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMudW5pcXVlSWQgPSBnZW5lcmF0ZUlkKCdhbnQtc2lkZXItJyk7XG4gICAgICAgIGxldCBtYXRjaE1lZGlhO1xuICAgICAgICBpZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIG1hdGNoTWVkaWEgPSB3aW5kb3cubWF0Y2hNZWRpYTtcbiAgICAgICAgfVxuICAgICAgICBpZiAobWF0Y2hNZWRpYSAmJiBwcm9wcy5icmVha3BvaW50ICYmIHByb3BzLmJyZWFrcG9pbnQgaW4gZGltZW5zaW9uTWF4TWFwKSB7XG4gICAgICAgICAgICB0aGlzLm1xbCA9IG1hdGNoTWVkaWEoYChtYXgtd2lkdGg6ICR7ZGltZW5zaW9uTWF4TWFwW3Byb3BzLmJyZWFrcG9pbnRdfSlgKTtcbiAgICAgICAgfVxuICAgICAgICBsZXQgY29sbGFwc2VkO1xuICAgICAgICBpZiAoJ2NvbGxhcHNlZCcgaW4gcHJvcHMpIHtcbiAgICAgICAgICAgIGNvbGxhcHNlZCA9IHByb3BzLmNvbGxhcHNlZDtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNvbGxhcHNlZCA9IHByb3BzLmRlZmF1bHRDb2xsYXBzZWQ7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIGNvbGxhcHNlZCxcbiAgICAgICAgICAgIGJlbG93OiBmYWxzZSxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgc3RhdGljIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMpIHtcbiAgICAgICAgaWYgKCdjb2xsYXBzZWQnIGluIG5leHRQcm9wcykge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICBjb2xsYXBzZWQ6IG5leHRQcm9wcy5jb2xsYXBzZWQsXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgaWYgKHRoaXMubXFsKSB7XG4gICAgICAgICAgICB0aGlzLm1xbC5hZGRMaXN0ZW5lcih0aGlzLnJlc3BvbnNpdmVIYW5kbGVyKTtcbiAgICAgICAgICAgIHRoaXMucmVzcG9uc2l2ZUhhbmRsZXIodGhpcy5tcWwpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLnByb3BzLnNpZGVySG9vaykge1xuICAgICAgICAgICAgdGhpcy5wcm9wcy5zaWRlckhvb2suYWRkU2lkZXIodGhpcy51bmlxdWVJZCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICAgIGlmICh0aGlzLm1xbCkge1xuICAgICAgICAgICAgdGhpcy5tcWwucmVtb3ZlTGlzdGVuZXIodGhpcy5yZXNwb25zaXZlSGFuZGxlcik7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMucHJvcHMuc2lkZXJIb29rKSB7XG4gICAgICAgICAgICB0aGlzLnByb3BzLnNpZGVySG9vay5yZW1vdmVTaWRlcih0aGlzLnVuaXF1ZUlkKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIGNvbnN0IHsgY29sbGFwc2VkIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBjb25zdCB7IGNvbGxhcHNlZFdpZHRoIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICByZXR1cm4gKDxTaWRlckNvbnRleHQuUHJvdmlkZXIgdmFsdWU9e3tcbiAgICAgICAgICAgIHNpZGVyQ29sbGFwc2VkOiBjb2xsYXBzZWQsXG4gICAgICAgICAgICBjb2xsYXBzZWRXaWR0aCxcbiAgICAgICAgfX0+XG4gICAgICAgIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJTaWRlcn08L0NvbmZpZ0NvbnN1bWVyPlxuICAgICAgPC9TaWRlckNvbnRleHQuUHJvdmlkZXI+KTtcbiAgICB9XG59XG5JbnRlcm5hbFNpZGVyLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBjb2xsYXBzaWJsZTogZmFsc2UsXG4gICAgZGVmYXVsdENvbGxhcHNlZDogZmFsc2UsXG4gICAgcmV2ZXJzZUFycm93OiBmYWxzZSxcbiAgICB3aWR0aDogMjAwLFxuICAgIGNvbGxhcHNlZFdpZHRoOiA4MCxcbiAgICBzdHlsZToge30sXG4gICAgdGhlbWU6ICdkYXJrJyxcbn07XG5wb2x5ZmlsbChJbnRlcm5hbFNpZGVyKTtcbi8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSByZWFjdC9wcmVmZXItc3RhdGVsZXNzLWZ1bmN0aW9uXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWRlciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gKDxMYXlvdXRDb250ZXh0LkNvbnN1bWVyPlxuICAgICAgICB7KGNvbnRleHQpID0+IDxJbnRlcm5hbFNpZGVyIHsuLi5jb250ZXh0fSB7Li4udGhpcy5wcm9wc30vPn1cbiAgICAgIDwvTGF5b3V0Q29udGV4dC5Db25zdW1lcj4pO1xuICAgIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFGQTtBQUNBO0FBQ0E7QUFRQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBRkE7QUFDQTtBQU1BOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUZBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVJBO0FBQ0E7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQU5BO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQUE7QUFGQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQXRDQTtBQUNBO0FBeUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUF2RkE7QUEyRkE7QUFDQTs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBTUE7OztBQWxDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7Ozs7QUFwR0E7QUFDQTtBQWdJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTs7OztBQUxBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/layout/Sider.js
