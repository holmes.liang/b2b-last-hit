/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _dataStackHelper = __webpack_require__(/*! ../../data/helper/dataStackHelper */ "./node_modules/echarts/lib/data/helper/dataStackHelper.js");

var isDimensionStacked = _dataStackHelper.isDimensionStacked;

var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var map = _util.map;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @param {Object} coordSys
 * @param {module:echarts/data/List} data
 * @param {string} valueOrigin lineSeries.option.areaStyle.origin
 */

function prepareDataCoordInfo(coordSys, data, valueOrigin) {
  var baseAxis = coordSys.getBaseAxis();
  var valueAxis = coordSys.getOtherAxis(baseAxis);
  var valueStart = getValueStart(valueAxis, valueOrigin);
  var baseAxisDim = baseAxis.dim;
  var valueAxisDim = valueAxis.dim;
  var valueDim = data.mapDimension(valueAxisDim);
  var baseDim = data.mapDimension(baseAxisDim);
  var baseDataOffset = valueAxisDim === 'x' || valueAxisDim === 'radius' ? 1 : 0;
  var dims = map(coordSys.dimensions, function (coordDim) {
    return data.mapDimension(coordDim);
  });
  var stacked;
  var stackResultDim = data.getCalculationInfo('stackResultDimension');

  if (stacked |= isDimensionStacked(data, dims[0]
  /*, dims[1]*/
  )) {
    // jshint ignore:line
    dims[0] = stackResultDim;
  }

  if (stacked |= isDimensionStacked(data, dims[1]
  /*, dims[0]*/
  )) {
    // jshint ignore:line
    dims[1] = stackResultDim;
  }

  return {
    dataDimsForPoint: dims,
    valueStart: valueStart,
    valueAxisDim: valueAxisDim,
    baseAxisDim: baseAxisDim,
    stacked: !!stacked,
    valueDim: valueDim,
    baseDim: baseDim,
    baseDataOffset: baseDataOffset,
    stackedOverDimension: data.getCalculationInfo('stackedOverDimension')
  };
}

function getValueStart(valueAxis, valueOrigin) {
  var valueStart = 0;
  var extent = valueAxis.scale.getExtent();

  if (valueOrigin === 'start') {
    valueStart = extent[0];
  } else if (valueOrigin === 'end') {
    valueStart = extent[1];
  } // auto
  else {
      // Both positive
      if (extent[0] > 0) {
        valueStart = extent[0];
      } // Both negative
      else if (extent[1] < 0) {
          valueStart = extent[1];
        } // If is one positive, and one negative, onZero shall be true

    }

  return valueStart;
}

function getStackedOnPoint(dataCoordInfo, coordSys, data, idx) {
  var value = NaN;

  if (dataCoordInfo.stacked) {
    value = data.get(data.getCalculationInfo('stackedOverDimension'), idx);
  }

  if (isNaN(value)) {
    value = dataCoordInfo.valueStart;
  }

  var baseDataOffset = dataCoordInfo.baseDataOffset;
  var stackedData = [];
  stackedData[baseDataOffset] = data.get(dataCoordInfo.baseDim, idx);
  stackedData[1 - baseDataOffset] = value;
  return coordSys.dataToPoint(stackedData);
}

exports.prepareDataCoordInfo = prepareDataCoordInfo;
exports.getStackedOnPoint = getStackedOnPoint;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvbGluZS9oZWxwZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jaGFydC9saW5lL2hlbHBlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIF9kYXRhU3RhY2tIZWxwZXIgPSByZXF1aXJlKFwiLi4vLi4vZGF0YS9oZWxwZXIvZGF0YVN0YWNrSGVscGVyXCIpO1xuXG52YXIgaXNEaW1lbnNpb25TdGFja2VkID0gX2RhdGFTdGFja0hlbHBlci5pc0RpbWVuc2lvblN0YWNrZWQ7XG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBtYXAgPSBfdXRpbC5tYXA7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBAcGFyYW0ge09iamVjdH0gY29vcmRTeXNcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvZGF0YS9MaXN0fSBkYXRhXG4gKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVPcmlnaW4gbGluZVNlcmllcy5vcHRpb24uYXJlYVN0eWxlLm9yaWdpblxuICovXG5mdW5jdGlvbiBwcmVwYXJlRGF0YUNvb3JkSW5mbyhjb29yZFN5cywgZGF0YSwgdmFsdWVPcmlnaW4pIHtcbiAgdmFyIGJhc2VBeGlzID0gY29vcmRTeXMuZ2V0QmFzZUF4aXMoKTtcbiAgdmFyIHZhbHVlQXhpcyA9IGNvb3JkU3lzLmdldE90aGVyQXhpcyhiYXNlQXhpcyk7XG4gIHZhciB2YWx1ZVN0YXJ0ID0gZ2V0VmFsdWVTdGFydCh2YWx1ZUF4aXMsIHZhbHVlT3JpZ2luKTtcbiAgdmFyIGJhc2VBeGlzRGltID0gYmFzZUF4aXMuZGltO1xuICB2YXIgdmFsdWVBeGlzRGltID0gdmFsdWVBeGlzLmRpbTtcbiAgdmFyIHZhbHVlRGltID0gZGF0YS5tYXBEaW1lbnNpb24odmFsdWVBeGlzRGltKTtcbiAgdmFyIGJhc2VEaW0gPSBkYXRhLm1hcERpbWVuc2lvbihiYXNlQXhpc0RpbSk7XG4gIHZhciBiYXNlRGF0YU9mZnNldCA9IHZhbHVlQXhpc0RpbSA9PT0gJ3gnIHx8IHZhbHVlQXhpc0RpbSA9PT0gJ3JhZGl1cycgPyAxIDogMDtcbiAgdmFyIGRpbXMgPSBtYXAoY29vcmRTeXMuZGltZW5zaW9ucywgZnVuY3Rpb24gKGNvb3JkRGltKSB7XG4gICAgcmV0dXJuIGRhdGEubWFwRGltZW5zaW9uKGNvb3JkRGltKTtcbiAgfSk7XG4gIHZhciBzdGFja2VkO1xuICB2YXIgc3RhY2tSZXN1bHREaW0gPSBkYXRhLmdldENhbGN1bGF0aW9uSW5mbygnc3RhY2tSZXN1bHREaW1lbnNpb24nKTtcblxuICBpZiAoc3RhY2tlZCB8PSBpc0RpbWVuc2lvblN0YWNrZWQoZGF0YSwgZGltc1swXVxuICAvKiwgZGltc1sxXSovXG4gICkpIHtcbiAgICAvLyBqc2hpbnQgaWdub3JlOmxpbmVcbiAgICBkaW1zWzBdID0gc3RhY2tSZXN1bHREaW07XG4gIH1cblxuICBpZiAoc3RhY2tlZCB8PSBpc0RpbWVuc2lvblN0YWNrZWQoZGF0YSwgZGltc1sxXVxuICAvKiwgZGltc1swXSovXG4gICkpIHtcbiAgICAvLyBqc2hpbnQgaWdub3JlOmxpbmVcbiAgICBkaW1zWzFdID0gc3RhY2tSZXN1bHREaW07XG4gIH1cblxuICByZXR1cm4ge1xuICAgIGRhdGFEaW1zRm9yUG9pbnQ6IGRpbXMsXG4gICAgdmFsdWVTdGFydDogdmFsdWVTdGFydCxcbiAgICB2YWx1ZUF4aXNEaW06IHZhbHVlQXhpc0RpbSxcbiAgICBiYXNlQXhpc0RpbTogYmFzZUF4aXNEaW0sXG4gICAgc3RhY2tlZDogISFzdGFja2VkLFxuICAgIHZhbHVlRGltOiB2YWx1ZURpbSxcbiAgICBiYXNlRGltOiBiYXNlRGltLFxuICAgIGJhc2VEYXRhT2Zmc2V0OiBiYXNlRGF0YU9mZnNldCxcbiAgICBzdGFja2VkT3ZlckRpbWVuc2lvbjogZGF0YS5nZXRDYWxjdWxhdGlvbkluZm8oJ3N0YWNrZWRPdmVyRGltZW5zaW9uJylcbiAgfTtcbn1cblxuZnVuY3Rpb24gZ2V0VmFsdWVTdGFydCh2YWx1ZUF4aXMsIHZhbHVlT3JpZ2luKSB7XG4gIHZhciB2YWx1ZVN0YXJ0ID0gMDtcbiAgdmFyIGV4dGVudCA9IHZhbHVlQXhpcy5zY2FsZS5nZXRFeHRlbnQoKTtcblxuICBpZiAodmFsdWVPcmlnaW4gPT09ICdzdGFydCcpIHtcbiAgICB2YWx1ZVN0YXJ0ID0gZXh0ZW50WzBdO1xuICB9IGVsc2UgaWYgKHZhbHVlT3JpZ2luID09PSAnZW5kJykge1xuICAgIHZhbHVlU3RhcnQgPSBleHRlbnRbMV07XG4gIH0gLy8gYXV0b1xuICBlbHNlIHtcbiAgICAgIC8vIEJvdGggcG9zaXRpdmVcbiAgICAgIGlmIChleHRlbnRbMF0gPiAwKSB7XG4gICAgICAgIHZhbHVlU3RhcnQgPSBleHRlbnRbMF07XG4gICAgICB9IC8vIEJvdGggbmVnYXRpdmVcbiAgICAgIGVsc2UgaWYgKGV4dGVudFsxXSA8IDApIHtcbiAgICAgICAgICB2YWx1ZVN0YXJ0ID0gZXh0ZW50WzFdO1xuICAgICAgICB9IC8vIElmIGlzIG9uZSBwb3NpdGl2ZSwgYW5kIG9uZSBuZWdhdGl2ZSwgb25aZXJvIHNoYWxsIGJlIHRydWVcblxuICAgIH1cblxuICByZXR1cm4gdmFsdWVTdGFydDtcbn1cblxuZnVuY3Rpb24gZ2V0U3RhY2tlZE9uUG9pbnQoZGF0YUNvb3JkSW5mbywgY29vcmRTeXMsIGRhdGEsIGlkeCkge1xuICB2YXIgdmFsdWUgPSBOYU47XG5cbiAgaWYgKGRhdGFDb29yZEluZm8uc3RhY2tlZCkge1xuICAgIHZhbHVlID0gZGF0YS5nZXQoZGF0YS5nZXRDYWxjdWxhdGlvbkluZm8oJ3N0YWNrZWRPdmVyRGltZW5zaW9uJyksIGlkeCk7XG4gIH1cblxuICBpZiAoaXNOYU4odmFsdWUpKSB7XG4gICAgdmFsdWUgPSBkYXRhQ29vcmRJbmZvLnZhbHVlU3RhcnQ7XG4gIH1cblxuICB2YXIgYmFzZURhdGFPZmZzZXQgPSBkYXRhQ29vcmRJbmZvLmJhc2VEYXRhT2Zmc2V0O1xuICB2YXIgc3RhY2tlZERhdGEgPSBbXTtcbiAgc3RhY2tlZERhdGFbYmFzZURhdGFPZmZzZXRdID0gZGF0YS5nZXQoZGF0YUNvb3JkSW5mby5iYXNlRGltLCBpZHgpO1xuICBzdGFja2VkRGF0YVsxIC0gYmFzZURhdGFPZmZzZXRdID0gdmFsdWU7XG4gIHJldHVybiBjb29yZFN5cy5kYXRhVG9Qb2ludChzdGFja2VkRGF0YSk7XG59XG5cbmV4cG9ydHMucHJlcGFyZURhdGFDb29yZEluZm8gPSBwcmVwYXJlRGF0YUNvb3JkSW5mbztcbmV4cG9ydHMuZ2V0U3RhY2tlZE9uUG9pbnQgPSBnZXRTdGFja2VkT25Qb2ludDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/line/helper.js
