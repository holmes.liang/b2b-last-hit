__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return QuillView; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_6__);





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/react-quill/quill-view.tsx";



var QuillView =
/*#__PURE__*/
function (_Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(QuillView, _Component);

  function QuillView() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, QuillView);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(QuillView)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.quillRef = void 0;

    _this.setRef = function (ref) {
      _this.quillRef = ref;
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(QuillView, [{
    key: "requestFullScreen",
    value: function requestFullScreen() {
      var dom = react_dom__WEBPACK_IMPORTED_MODULE_6___default.a.findDOMNode(this.quillRef) || {}; //绑定想要全屏的组件

      if (dom.requestFullscreen) {
        dom.requestFullscreen();
      } else if (dom.mozRequestFullScreen) {
        dom.mozRequestFullScreen();
      } else if (dom.webkitRequestFullScreen) {
        dom.webkitRequestFullScreen();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$text = this.props.text,
          text = _this$props$text === void 0 ? "" : _this$props$text;
      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_5___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        ref: this.setRef,
        style: {
          border: 0
        },
        className: "ql-container ql-snow",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 33
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "ql-editor",
        style: {
          paddingTop: "5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 34
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        dangerouslySetInnerHTML: {
          __html: text
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 35
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        style: {
          textAlign: "right"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          marginRight: 5
        },
        className: "iconfont icon-full",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("a", {
        onClick: this.requestFullScreen.bind(this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, "View In Full Screen")));
    }
  }]);

  return QuillView;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

QuillView.defaultProps = {
  text: ""
};
//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3JlYWN0LXF1aWxsL3F1aWxsLXZpZXcudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3JlYWN0LXF1aWxsL3F1aWxsLXZpZXcudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBSZWFjdERPTSBmcm9tIFwicmVhY3QtZG9tXCI7XG5cbmludGVyZmFjZSBJUHJvcHMge1xuICB0ZXh0OiBhbnk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFF1aWxsVmlldyBleHRlbmRzIENvbXBvbmVudDxJUHJvcHMsIGFueT4ge1xuICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgIHRleHQ6IFwiXCIsXG4gIH07XG5cbiAgcXVpbGxSZWY6IGFueTtcblxuICBzZXRSZWYgPSAocmVmOiBhbnkpID0+IHtcbiAgICB0aGlzLnF1aWxsUmVmID0gcmVmO1xuICB9O1xuXG4gIHJlcXVlc3RGdWxsU2NyZWVuKCkge1xuICAgIHZhciBkb206IGFueSA9IFJlYWN0RE9NLmZpbmRET01Ob2RlKHRoaXMucXVpbGxSZWYpIHx8IHt9Oy8v57uR5a6a5oOz6KaB5YWo5bGP55qE57uE5Lu2XG4gICAgaWYgKGRvbS5yZXF1ZXN0RnVsbHNjcmVlbikge1xuICAgICAgZG9tLnJlcXVlc3RGdWxsc2NyZWVuKCk7XG4gICAgfSBlbHNlIGlmIChkb20ubW96UmVxdWVzdEZ1bGxTY3JlZW4pIHtcbiAgICAgIGRvbS5tb3pSZXF1ZXN0RnVsbFNjcmVlbigpO1xuICAgIH0gZWxzZSBpZiAoZG9tLndlYmtpdFJlcXVlc3RGdWxsU2NyZWVuKSB7XG4gICAgICBkb20ud2Via2l0UmVxdWVzdEZ1bGxTY3JlZW4oKTtcbiAgICB9XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgdGV4dCA9IFwiXCIgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIDw+XG4gICAgICA8ZGl2IHJlZj17dGhpcy5zZXRSZWZ9IHN0eWxlPXt7IGJvcmRlcjogMCB9fSBjbGFzc05hbWU9XCJxbC1jb250YWluZXIgcWwtc25vd1wiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInFsLWVkaXRvclwiIHN0eWxlPXt7IHBhZGRpbmdUb3A6IFwiNXB4XCIgfX0+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e3sgX19odG1sOiB0ZXh0IH19Lz5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgc3R5bGU9e3tcbiAgICAgICAgdGV4dEFsaWduOiBcInJpZ2h0XCIsXG4gICAgICB9fT5cbiAgICAgICAgPGkgc3R5bGU9e3tcbiAgICAgICAgICBtYXJnaW5SaWdodDogNSxcbiAgICAgICAgfX0gY2xhc3NOYW1lPXtcImljb25mb250IGljb24tZnVsbFwifS8+XG4gICAgICAgIDxhIG9uQ2xpY2s9e3RoaXMucmVxdWVzdEZ1bGxTY3JlZW4uYmluZCh0aGlzKX0+VmlldyBJbiBGdWxsIFNjcmVlbjwvYT5cbiAgICAgIDwvZGl2PlxuICAgIDwvPjtcbiAgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUtBOzs7Ozs7Ozs7Ozs7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBREE7QUFFQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7Ozs7QUF4Q0E7QUFDQTtBQURBO0FBRUE7QUFEQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/react-quill/quill-view.tsx
