__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderTitle", function() { return HeaderTitle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromoGroup", function() { return PromoGroup; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleWithoutMarginBottom", function() { return StyleWithoutMarginBottom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableHeader", function() { return TableHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableStyle", function() { return TableStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextGroupStyle", function() { return TextGroupStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleBox", function() { return StyleBox; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeskPageBox", function() { return DeskPageBox; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/styles/global.tsx";

function _templateObject7() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\nbackground-color: #fff\n"]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  .tax-no-group {\n      .ant-form-item {\n          &:first-child {\n              .has-error {\n                  .ant-input{\n                      border-left: 1px solid #f5222d;\n                  }\n              }\n              .ant-input{\n                  border-left: 1px solid #d9d9d9;\n                  border-top-left-radius: 4px;\n                  border-bottom-left-radius: 4px;\n                  border-top-right-radius: 0;\n                  border-bottom-right-radius: 0;\n              }\n          }\n      }\n  }\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  table thead.ant-table-thead th {\n    // border-top: 1px solid #e8e8e8;\n    &:first-child {\n      // border-left: 1px solid #e8e8e8;\n    }\n  }\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  .ant-table-wrapper{\n    thead.ant-table-thead{\n      tr{\n        th{\n          color: #9e9e9e;\n          font-size: 13px;\n          font-weight: 300\n        }\n      }\n    }\n    tbody.ant-table-tbody{\n      tr{\n        td{\n          font-size: 16px;\n          &.amount-right {\n            text-align: right;\n          }\n          &.amount-center {\n            text-align: center;\n          }\n        }\n        &:last-child {\n          td span {\n           font-weight: bolder !important;\n          }\n        }\n      }\n    }\n  }\n  \n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  .ant-form-item .ant-form-item-control-wrapper {\n      width: 100%;\n  }\n  .ant-form-item{\n      margin-bottom: 0 !important;\n      .ant-select-selection.ant-select-selection--multiple {\n        min-height: 28px\n        .ant-select-selection__rendered {\n          height: auto;\n          line-height: 30px\n        }\n      }\n  }\n  .ant-form-explain {\n    display: none !important;\n  }\n  .ant-form-item-control {\n    line-height: normal !important;\n  }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  width: 100%;\n  display: flex;\n  align-items: center;\n  .ant-row.ant-form-item{\n    flex: 1;\n    .ant-form-item-control-wrapper .ant-input {\n      border-left: 1px solid #ebebeb;\n      border-top-left-radius: 4px;\n      border-bottom-left-radius: 4px;\n      &:hover,:focus {\n       border-color: ", ";\n      }\n    }\n  }\n  .voucher-link {\n    margin-left: 10px;\n    margin-bottom: 24px;\n    &.mobile-link {\n      margin-bottom: 15px;\n    }\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    font-weight: 500;\n    color: rgba(0,0,0,.85);\n    white-space: normal;\n    font-size: 20px;\n    padding: 14px 24px 24px 24px;\n    text-align: center;\n    text-transform: uppercase;\n    min-height: 0 !important;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}





var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_3__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var HeaderTitle = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject());
var PromoGroup = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject2(), COLOR_A);
var StyleWithoutMarginBottom = styled_components__WEBPACK_IMPORTED_MODULE_2__["default"].div(_templateObject3());
var TableHeader = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject4());
var TableStyle = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject5());
var TextGroupStyle = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject6()); // 白色背景

var StyleBox = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject7());
var DeskPageBox = function DeskPageBox(props) {
  return _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement(StyleBox, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 128
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement("div", {
    className: "container",
    style: props.style,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 129
    },
    __self: this
  }, " ", props.children));
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svc3R5bGVzL2dsb2JhbC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9zdHlsZXMvZ2xvYmFsLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgc3R5bGVkIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IFRoZW1lIGZyb20gXCJAc3R5bGVzXCI7XG5cbmNvbnN0IHsgQ09MT1JfQSB9ID0gVGhlbWUuZ2V0VGhlbWUoKTtcblxuZXhwb3J0IGNvbnN0IEhlYWRlclRpdGxlID0gU3R5bGVkLmRpdmBcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGNvbG9yOiByZ2JhKDAsMCwwLC44NSk7XG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgcGFkZGluZzogMTRweCAyNHB4IDI0cHggMjRweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBtaW4taGVpZ2h0OiAwICFpbXBvcnRhbnQ7XG5gO1xuXG5leHBvcnQgY29uc3QgUHJvbW9Hcm91cCA9IFN0eWxlZC5kaXZgXG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAuYW50LXJvdy5hbnQtZm9ybS1pdGVte1xuICAgIGZsZXg6IDE7XG4gICAgLmFudC1mb3JtLWl0ZW0tY29udHJvbC13cmFwcGVyIC5hbnQtaW5wdXQge1xuICAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjZWJlYmViO1xuICAgICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogNHB4O1xuICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNHB4O1xuICAgICAgJjpob3Zlciw6Zm9jdXMge1xuICAgICAgIGJvcmRlci1jb2xvcjogJHtDT0xPUl9BfTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLnZvdWNoZXItbGluayB7XG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjRweDtcbiAgICAmLm1vYmlsZS1saW5rIHtcbiAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gICAgfVxuICB9XG5gO1xuXG5leHBvcnQgY29uc3QgU3R5bGVXaXRob3V0TWFyZ2luQm90dG9tID0gc3R5bGVkLmRpdmBcbiAgLmFudC1mb3JtLWl0ZW0gLmFudC1mb3JtLWl0ZW0tY29udHJvbC13cmFwcGVyIHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5hbnQtZm9ybS1pdGVte1xuICAgICAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xuICAgICAgLmFudC1zZWxlY3Qtc2VsZWN0aW9uLmFudC1zZWxlY3Qtc2VsZWN0aW9uLS1tdWx0aXBsZSB7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDI4cHhcbiAgICAgICAgLmFudC1zZWxlY3Qtc2VsZWN0aW9uX19yZW5kZXJlZCB7XG4gICAgICAgICAgaGVpZ2h0OiBhdXRvO1xuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAzMHB4XG4gICAgICAgIH1cbiAgICAgIH1cbiAgfVxuICAuYW50LWZvcm0tZXhwbGFpbiB7XG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xuICB9XG4gIC5hbnQtZm9ybS1pdGVtLWNvbnRyb2wge1xuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWwgIWltcG9ydGFudDtcbiAgfVxuYDtcbmV4cG9ydCBjb25zdCBUYWJsZUhlYWRlciA9IFN0eWxlZC5kaXZgXG4gIC5hbnQtdGFibGUtd3JhcHBlcntcbiAgICB0aGVhZC5hbnQtdGFibGUtdGhlYWR7XG4gICAgICB0cntcbiAgICAgICAgdGh7XG4gICAgICAgICAgY29sb3I6ICM5ZTllOWU7XG4gICAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgICAgIGZvbnQtd2VpZ2h0OiAzMDBcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICB0Ym9keS5hbnQtdGFibGUtdGJvZHl7XG4gICAgICB0cntcbiAgICAgICAgdGR7XG4gICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICYuYW1vdW50LXJpZ2h0IHtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICAgIH1cbiAgICAgICAgICAmLmFtb3VudC1jZW50ZXIge1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgICAgIHRkIHNwYW4ge1xuICAgICAgICAgICBmb250LXdlaWdodDogYm9sZGVyICFpbXBvcnRhbnQ7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG4gIFxuYDtcbmV4cG9ydCBjb25zdCBUYWJsZVN0eWxlID0gU3R5bGVkLmRpdmBcbiAgdGFibGUgdGhlYWQuYW50LXRhYmxlLXRoZWFkIHRoIHtcbiAgICAvLyBib3JkZXItdG9wOiAxcHggc29saWQgI2U4ZThlODtcbiAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgIC8vIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2U4ZThlODtcbiAgICB9XG4gIH1cbmA7XG5leHBvcnQgY29uc3QgVGV4dEdyb3VwU3R5bGUgPSBTdHlsZWQuZGl2YFxuICAudGF4LW5vLWdyb3VwIHtcbiAgICAgIC5hbnQtZm9ybS1pdGVtIHtcbiAgICAgICAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgLmhhcy1lcnJvciB7XG4gICAgICAgICAgICAgICAgICAuYW50LWlucHV0e1xuICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2Y1MjIyZDtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAuYW50LWlucHV0e1xuICAgICAgICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjZDlkOWQ5O1xuICAgICAgICAgICAgICAgICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogNHB4O1xuICAgICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNHB4O1xuICAgICAgICAgICAgICAgICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDA7XG4gICAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgIH1cbiAgfVxuYDtcbi8vIOeZveiJsuiDjOaZr1xuZXhwb3J0IGNvbnN0IFN0eWxlQm94ID0gU3R5bGVkLmRpdmBcbmJhY2tncm91bmQtY29sb3I6ICNmZmZcbmA7XG5cbmV4cG9ydCBjb25zdCBEZXNrUGFnZUJveCA9IChwcm9wczogYW55KSA9PiA8U3R5bGVCb3g+XG4gIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCIgc3R5bGU9e3Byb3BzLnN0eWxlfT4ge3Byb3BzLmNoaWxkcmVufTwvZGl2PlxuPC9TdHlsZUJveD47XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVdBO0FBd0JBO0FBcUJBO0FBZ0NBO0FBUUE7QUFDQTtBQW9CQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/styles/global.tsx
