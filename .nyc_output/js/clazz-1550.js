/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var TYPE_DELIMITER = '.';
var IS_CONTAINER = '___EC__COMPONENT__CONTAINER___';
/**
 * Notice, parseClassType('') should returns {main: '', sub: ''}
 * @public
 */

function parseClassType(componentType) {
  var ret = {
    main: '',
    sub: ''
  };

  if (componentType) {
    componentType = componentType.split(TYPE_DELIMITER);
    ret.main = componentType[0] || '';
    ret.sub = componentType[1] || '';
  }

  return ret;
}
/**
 * @public
 */


function checkClassType(componentType) {
  zrUtil.assert(/^[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)?$/.test(componentType), 'componentType "' + componentType + '" illegal');
}
/**
 * @public
 */


function enableClassExtend(RootClass, mandatoryMethods) {
  RootClass.$constructor = RootClass;

  RootClass.extend = function (proto) {
    var superClass = this;

    var ExtendedClass = function ExtendedClass() {
      if (!proto.$constructor) {
        superClass.apply(this, arguments);
      } else {
        proto.$constructor.apply(this, arguments);
      }
    };

    zrUtil.extend(ExtendedClass.prototype, proto);
    ExtendedClass.extend = this.extend;
    ExtendedClass.superCall = superCall;
    ExtendedClass.superApply = superApply;
    zrUtil.inherits(ExtendedClass, this);
    ExtendedClass.superClass = superClass;
    return ExtendedClass;
  };
}

var classBase = 0;
/**
 * Can not use instanceof, consider different scope by
 * cross domain or es module import in ec extensions.
 * Mount a method "isInstance()" to Clz.
 */

function enableClassCheck(Clz) {
  var classAttr = ['__\0is_clz', classBase++, Math.random().toFixed(3)].join('_');
  Clz.prototype[classAttr] = true;

  Clz.isInstance = function (obj) {
    return !!(obj && obj[classAttr]);
  };
} // superCall should have class info, which can not be fetch from 'this'.
// Consider this case:
// class A has method f,
// class B inherits class A, overrides method f, f call superApply('f'),
// class C inherits class B, do not overrides method f,
// then when method of class C is called, dead loop occured.


function superCall(context, methodName) {
  var args = zrUtil.slice(arguments, 2);
  return this.superClass.prototype[methodName].apply(context, args);
}

function superApply(context, methodName, args) {
  return this.superClass.prototype[methodName].apply(context, args);
}
/**
 * @param {Object} entity
 * @param {Object} options
 * @param {boolean} [options.registerWhenExtend]
 * @public
 */


function enableClassManagement(entity, options) {
  options = options || {};
  /**
   * Component model classes
   * key: componentType,
   * value:
   *     componentClass, when componentType is 'xxx'
   *     or Object.<subKey, componentClass>, when componentType is 'xxx.yy'
   * @type {Object}
   */

  var storage = {};

  entity.registerClass = function (Clazz, componentType) {
    if (componentType) {
      checkClassType(componentType);
      componentType = parseClassType(componentType);

      if (!componentType.sub) {
        storage[componentType.main] = Clazz;
      } else if (componentType.sub !== IS_CONTAINER) {
        var container = makeContainer(componentType);
        container[componentType.sub] = Clazz;
      }
    }

    return Clazz;
  };

  entity.getClass = function (componentMainType, subType, throwWhenNotFound) {
    var Clazz = storage[componentMainType];

    if (Clazz && Clazz[IS_CONTAINER]) {
      Clazz = subType ? Clazz[subType] : null;
    }

    if (throwWhenNotFound && !Clazz) {
      throw new Error(!subType ? componentMainType + '.' + 'type should be specified.' : 'Component ' + componentMainType + '.' + (subType || '') + ' not exists. Load it first.');
    }

    return Clazz;
  };

  entity.getClassesByMainType = function (componentType) {
    componentType = parseClassType(componentType);
    var result = [];
    var obj = storage[componentType.main];

    if (obj && obj[IS_CONTAINER]) {
      zrUtil.each(obj, function (o, type) {
        type !== IS_CONTAINER && result.push(o);
      });
    } else {
      result.push(obj);
    }

    return result;
  };

  entity.hasClass = function (componentType) {
    // Just consider componentType.main.
    componentType = parseClassType(componentType);
    return !!storage[componentType.main];
  };
  /**
   * @return {Array.<string>} Like ['aa', 'bb'], but can not be ['aa.xx']
   */


  entity.getAllClassMainTypes = function () {
    var types = [];
    zrUtil.each(storage, function (obj, type) {
      types.push(type);
    });
    return types;
  };
  /**
   * If a main type is container and has sub types
   * @param  {string}  mainType
   * @return {boolean}
   */


  entity.hasSubTypes = function (componentType) {
    componentType = parseClassType(componentType);
    var obj = storage[componentType.main];
    return obj && obj[IS_CONTAINER];
  };

  entity.parseClassType = parseClassType;

  function makeContainer(componentType) {
    var container = storage[componentType.main];

    if (!container || !container[IS_CONTAINER]) {
      container = storage[componentType.main] = {};
      container[IS_CONTAINER] = true;
    }

    return container;
  }

  if (options.registerWhenExtend) {
    var originalExtend = entity.extend;

    if (originalExtend) {
      entity.extend = function (proto) {
        var ExtendedClass = originalExtend.call(this, proto);
        return entity.registerClass(ExtendedClass, proto.type);
      };
    }
  }

  return entity;
}
/**
 * @param {string|Array.<string>} properties
 */


function setReadOnly(obj, properties) {// FIXME It seems broken in IE8 simulation of IE11
  // if (!zrUtil.isArray(properties)) {
  //     properties = properties != null ? [properties] : [];
  // }
  // zrUtil.each(properties, function (prop) {
  //     var value = obj[prop];
  //     Object.defineProperty
  //         && Object.defineProperty(obj, prop, {
  //             value: value, writable: false
  //         });
  //     zrUtil.isArray(obj[prop])
  //         && Object.freeze
  //         && Object.freeze(obj[prop]);
  // });
}

exports.parseClassType = parseClassType;
exports.enableClassExtend = enableClassExtend;
exports.enableClassCheck = enableClassCheck;
exports.enableClassManagement = enableClassManagement;
exports.setReadOnly = setReadOnly;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdXRpbC9jbGF6ei5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3V0aWwvY2xhenouanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfY29uZmlnID0gcmVxdWlyZShcIi4uL2NvbmZpZ1wiKTtcblxudmFyIF9fREVWX18gPSBfY29uZmlnLl9fREVWX187XG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgVFlQRV9ERUxJTUlURVIgPSAnLic7XG52YXIgSVNfQ09OVEFJTkVSID0gJ19fX0VDX19DT01QT05FTlRfX0NPTlRBSU5FUl9fXyc7XG4vKipcbiAqIE5vdGljZSwgcGFyc2VDbGFzc1R5cGUoJycpIHNob3VsZCByZXR1cm5zIHttYWluOiAnJywgc3ViOiAnJ31cbiAqIEBwdWJsaWNcbiAqL1xuXG5mdW5jdGlvbiBwYXJzZUNsYXNzVHlwZShjb21wb25lbnRUeXBlKSB7XG4gIHZhciByZXQgPSB7XG4gICAgbWFpbjogJycsXG4gICAgc3ViOiAnJ1xuICB9O1xuXG4gIGlmIChjb21wb25lbnRUeXBlKSB7XG4gICAgY29tcG9uZW50VHlwZSA9IGNvbXBvbmVudFR5cGUuc3BsaXQoVFlQRV9ERUxJTUlURVIpO1xuICAgIHJldC5tYWluID0gY29tcG9uZW50VHlwZVswXSB8fCAnJztcbiAgICByZXQuc3ViID0gY29tcG9uZW50VHlwZVsxXSB8fCAnJztcbiAgfVxuXG4gIHJldHVybiByZXQ7XG59XG4vKipcbiAqIEBwdWJsaWNcbiAqL1xuXG5cbmZ1bmN0aW9uIGNoZWNrQ2xhc3NUeXBlKGNvbXBvbmVudFR5cGUpIHtcbiAgenJVdGlsLmFzc2VydCgvXlthLXpBLVowLTlfXSsoWy5dW2EtekEtWjAtOV9dKyk/JC8udGVzdChjb21wb25lbnRUeXBlKSwgJ2NvbXBvbmVudFR5cGUgXCInICsgY29tcG9uZW50VHlwZSArICdcIiBpbGxlZ2FsJyk7XG59XG4vKipcbiAqIEBwdWJsaWNcbiAqL1xuXG5cbmZ1bmN0aW9uIGVuYWJsZUNsYXNzRXh0ZW5kKFJvb3RDbGFzcywgbWFuZGF0b3J5TWV0aG9kcykge1xuICBSb290Q2xhc3MuJGNvbnN0cnVjdG9yID0gUm9vdENsYXNzO1xuXG4gIFJvb3RDbGFzcy5leHRlbmQgPSBmdW5jdGlvbiAocHJvdG8pIHtcbiAgICB2YXIgc3VwZXJDbGFzcyA9IHRoaXM7XG5cbiAgICB2YXIgRXh0ZW5kZWRDbGFzcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICghcHJvdG8uJGNvbnN0cnVjdG9yKSB7XG4gICAgICAgIHN1cGVyQ2xhc3MuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHByb3RvLiRjb25zdHJ1Y3Rvci5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICB6clV0aWwuZXh0ZW5kKEV4dGVuZGVkQ2xhc3MucHJvdG90eXBlLCBwcm90byk7XG4gICAgRXh0ZW5kZWRDbGFzcy5leHRlbmQgPSB0aGlzLmV4dGVuZDtcbiAgICBFeHRlbmRlZENsYXNzLnN1cGVyQ2FsbCA9IHN1cGVyQ2FsbDtcbiAgICBFeHRlbmRlZENsYXNzLnN1cGVyQXBwbHkgPSBzdXBlckFwcGx5O1xuICAgIHpyVXRpbC5pbmhlcml0cyhFeHRlbmRlZENsYXNzLCB0aGlzKTtcbiAgICBFeHRlbmRlZENsYXNzLnN1cGVyQ2xhc3MgPSBzdXBlckNsYXNzO1xuICAgIHJldHVybiBFeHRlbmRlZENsYXNzO1xuICB9O1xufVxuXG52YXIgY2xhc3NCYXNlID0gMDtcbi8qKlxuICogQ2FuIG5vdCB1c2UgaW5zdGFuY2VvZiwgY29uc2lkZXIgZGlmZmVyZW50IHNjb3BlIGJ5XG4gKiBjcm9zcyBkb21haW4gb3IgZXMgbW9kdWxlIGltcG9ydCBpbiBlYyBleHRlbnNpb25zLlxuICogTW91bnQgYSBtZXRob2QgXCJpc0luc3RhbmNlKClcIiB0byBDbHouXG4gKi9cblxuZnVuY3Rpb24gZW5hYmxlQ2xhc3NDaGVjayhDbHopIHtcbiAgdmFyIGNsYXNzQXR0ciA9IFsnX19cXDBpc19jbHonLCBjbGFzc0Jhc2UrKywgTWF0aC5yYW5kb20oKS50b0ZpeGVkKDMpXS5qb2luKCdfJyk7XG4gIENsei5wcm90b3R5cGVbY2xhc3NBdHRyXSA9IHRydWU7XG5cbiAgQ2x6LmlzSW5zdGFuY2UgPSBmdW5jdGlvbiAob2JqKSB7XG4gICAgcmV0dXJuICEhKG9iaiAmJiBvYmpbY2xhc3NBdHRyXSk7XG4gIH07XG59IC8vIHN1cGVyQ2FsbCBzaG91bGQgaGF2ZSBjbGFzcyBpbmZvLCB3aGljaCBjYW4gbm90IGJlIGZldGNoIGZyb20gJ3RoaXMnLlxuLy8gQ29uc2lkZXIgdGhpcyBjYXNlOlxuLy8gY2xhc3MgQSBoYXMgbWV0aG9kIGYsXG4vLyBjbGFzcyBCIGluaGVyaXRzIGNsYXNzIEEsIG92ZXJyaWRlcyBtZXRob2QgZiwgZiBjYWxsIHN1cGVyQXBwbHkoJ2YnKSxcbi8vIGNsYXNzIEMgaW5oZXJpdHMgY2xhc3MgQiwgZG8gbm90IG92ZXJyaWRlcyBtZXRob2QgZixcbi8vIHRoZW4gd2hlbiBtZXRob2Qgb2YgY2xhc3MgQyBpcyBjYWxsZWQsIGRlYWQgbG9vcCBvY2N1cmVkLlxuXG5cbmZ1bmN0aW9uIHN1cGVyQ2FsbChjb250ZXh0LCBtZXRob2ROYW1lKSB7XG4gIHZhciBhcmdzID0genJVdGlsLnNsaWNlKGFyZ3VtZW50cywgMik7XG4gIHJldHVybiB0aGlzLnN1cGVyQ2xhc3MucHJvdG90eXBlW21ldGhvZE5hbWVdLmFwcGx5KGNvbnRleHQsIGFyZ3MpO1xufVxuXG5mdW5jdGlvbiBzdXBlckFwcGx5KGNvbnRleHQsIG1ldGhvZE5hbWUsIGFyZ3MpIHtcbiAgcmV0dXJuIHRoaXMuc3VwZXJDbGFzcy5wcm90b3R5cGVbbWV0aG9kTmFtZV0uYXBwbHkoY29udGV4dCwgYXJncyk7XG59XG4vKipcbiAqIEBwYXJhbSB7T2JqZWN0fSBlbnRpdHlcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHRpb25zLnJlZ2lzdGVyV2hlbkV4dGVuZF1cbiAqIEBwdWJsaWNcbiAqL1xuXG5cbmZ1bmN0aW9uIGVuYWJsZUNsYXNzTWFuYWdlbWVudChlbnRpdHksIG9wdGlvbnMpIHtcbiAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG4gIC8qKlxuICAgKiBDb21wb25lbnQgbW9kZWwgY2xhc3Nlc1xuICAgKiBrZXk6IGNvbXBvbmVudFR5cGUsXG4gICAqIHZhbHVlOlxuICAgKiAgICAgY29tcG9uZW50Q2xhc3MsIHdoZW4gY29tcG9uZW50VHlwZSBpcyAneHh4J1xuICAgKiAgICAgb3IgT2JqZWN0LjxzdWJLZXksIGNvbXBvbmVudENsYXNzPiwgd2hlbiBjb21wb25lbnRUeXBlIGlzICd4eHgueXknXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuXG4gIHZhciBzdG9yYWdlID0ge307XG5cbiAgZW50aXR5LnJlZ2lzdGVyQ2xhc3MgPSBmdW5jdGlvbiAoQ2xhenosIGNvbXBvbmVudFR5cGUpIHtcbiAgICBpZiAoY29tcG9uZW50VHlwZSkge1xuICAgICAgY2hlY2tDbGFzc1R5cGUoY29tcG9uZW50VHlwZSk7XG4gICAgICBjb21wb25lbnRUeXBlID0gcGFyc2VDbGFzc1R5cGUoY29tcG9uZW50VHlwZSk7XG5cbiAgICAgIGlmICghY29tcG9uZW50VHlwZS5zdWIpIHtcbiAgICAgICAgc3RvcmFnZVtjb21wb25lbnRUeXBlLm1haW5dID0gQ2xheno7XG4gICAgICB9IGVsc2UgaWYgKGNvbXBvbmVudFR5cGUuc3ViICE9PSBJU19DT05UQUlORVIpIHtcbiAgICAgICAgdmFyIGNvbnRhaW5lciA9IG1ha2VDb250YWluZXIoY29tcG9uZW50VHlwZSk7XG4gICAgICAgIGNvbnRhaW5lcltjb21wb25lbnRUeXBlLnN1Yl0gPSBDbGF6ejtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gQ2xheno7XG4gIH07XG5cbiAgZW50aXR5LmdldENsYXNzID0gZnVuY3Rpb24gKGNvbXBvbmVudE1haW5UeXBlLCBzdWJUeXBlLCB0aHJvd1doZW5Ob3RGb3VuZCkge1xuICAgIHZhciBDbGF6eiA9IHN0b3JhZ2VbY29tcG9uZW50TWFpblR5cGVdO1xuXG4gICAgaWYgKENsYXp6ICYmIENsYXp6W0lTX0NPTlRBSU5FUl0pIHtcbiAgICAgIENsYXp6ID0gc3ViVHlwZSA/IENsYXp6W3N1YlR5cGVdIDogbnVsbDtcbiAgICB9XG5cbiAgICBpZiAodGhyb3dXaGVuTm90Rm91bmQgJiYgIUNsYXp6KSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoIXN1YlR5cGUgPyBjb21wb25lbnRNYWluVHlwZSArICcuJyArICd0eXBlIHNob3VsZCBiZSBzcGVjaWZpZWQuJyA6ICdDb21wb25lbnQgJyArIGNvbXBvbmVudE1haW5UeXBlICsgJy4nICsgKHN1YlR5cGUgfHwgJycpICsgJyBub3QgZXhpc3RzLiBMb2FkIGl0IGZpcnN0LicpO1xuICAgIH1cblxuICAgIHJldHVybiBDbGF6ejtcbiAgfTtcblxuICBlbnRpdHkuZ2V0Q2xhc3Nlc0J5TWFpblR5cGUgPSBmdW5jdGlvbiAoY29tcG9uZW50VHlwZSkge1xuICAgIGNvbXBvbmVudFR5cGUgPSBwYXJzZUNsYXNzVHlwZShjb21wb25lbnRUeXBlKTtcbiAgICB2YXIgcmVzdWx0ID0gW107XG4gICAgdmFyIG9iaiA9IHN0b3JhZ2VbY29tcG9uZW50VHlwZS5tYWluXTtcblxuICAgIGlmIChvYmogJiYgb2JqW0lTX0NPTlRBSU5FUl0pIHtcbiAgICAgIHpyVXRpbC5lYWNoKG9iaiwgZnVuY3Rpb24gKG8sIHR5cGUpIHtcbiAgICAgICAgdHlwZSAhPT0gSVNfQ09OVEFJTkVSICYmIHJlc3VsdC5wdXNoKG8pO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc3VsdC5wdXNoKG9iaik7XG4gICAgfVxuXG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfTtcblxuICBlbnRpdHkuaGFzQ2xhc3MgPSBmdW5jdGlvbiAoY29tcG9uZW50VHlwZSkge1xuICAgIC8vIEp1c3QgY29uc2lkZXIgY29tcG9uZW50VHlwZS5tYWluLlxuICAgIGNvbXBvbmVudFR5cGUgPSBwYXJzZUNsYXNzVHlwZShjb21wb25lbnRUeXBlKTtcbiAgICByZXR1cm4gISFzdG9yYWdlW2NvbXBvbmVudFR5cGUubWFpbl07XG4gIH07XG4gIC8qKlxuICAgKiBAcmV0dXJuIHtBcnJheS48c3RyaW5nPn0gTGlrZSBbJ2FhJywgJ2JiJ10sIGJ1dCBjYW4gbm90IGJlIFsnYWEueHgnXVxuICAgKi9cblxuXG4gIGVudGl0eS5nZXRBbGxDbGFzc01haW5UeXBlcyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdHlwZXMgPSBbXTtcbiAgICB6clV0aWwuZWFjaChzdG9yYWdlLCBmdW5jdGlvbiAob2JqLCB0eXBlKSB7XG4gICAgICB0eXBlcy5wdXNoKHR5cGUpO1xuICAgIH0pO1xuICAgIHJldHVybiB0eXBlcztcbiAgfTtcbiAgLyoqXG4gICAqIElmIGEgbWFpbiB0eXBlIGlzIGNvbnRhaW5lciBhbmQgaGFzIHN1YiB0eXBlc1xuICAgKiBAcGFyYW0gIHtzdHJpbmd9ICBtYWluVHlwZVxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cblxuXG4gIGVudGl0eS5oYXNTdWJUeXBlcyA9IGZ1bmN0aW9uIChjb21wb25lbnRUeXBlKSB7XG4gICAgY29tcG9uZW50VHlwZSA9IHBhcnNlQ2xhc3NUeXBlKGNvbXBvbmVudFR5cGUpO1xuICAgIHZhciBvYmogPSBzdG9yYWdlW2NvbXBvbmVudFR5cGUubWFpbl07XG4gICAgcmV0dXJuIG9iaiAmJiBvYmpbSVNfQ09OVEFJTkVSXTtcbiAgfTtcblxuICBlbnRpdHkucGFyc2VDbGFzc1R5cGUgPSBwYXJzZUNsYXNzVHlwZTtcblxuICBmdW5jdGlvbiBtYWtlQ29udGFpbmVyKGNvbXBvbmVudFR5cGUpIHtcbiAgICB2YXIgY29udGFpbmVyID0gc3RvcmFnZVtjb21wb25lbnRUeXBlLm1haW5dO1xuXG4gICAgaWYgKCFjb250YWluZXIgfHwgIWNvbnRhaW5lcltJU19DT05UQUlORVJdKSB7XG4gICAgICBjb250YWluZXIgPSBzdG9yYWdlW2NvbXBvbmVudFR5cGUubWFpbl0gPSB7fTtcbiAgICAgIGNvbnRhaW5lcltJU19DT05UQUlORVJdID0gdHJ1ZTtcbiAgICB9XG5cbiAgICByZXR1cm4gY29udGFpbmVyO1xuICB9XG5cbiAgaWYgKG9wdGlvbnMucmVnaXN0ZXJXaGVuRXh0ZW5kKSB7XG4gICAgdmFyIG9yaWdpbmFsRXh0ZW5kID0gZW50aXR5LmV4dGVuZDtcblxuICAgIGlmIChvcmlnaW5hbEV4dGVuZCkge1xuICAgICAgZW50aXR5LmV4dGVuZCA9IGZ1bmN0aW9uIChwcm90bykge1xuICAgICAgICB2YXIgRXh0ZW5kZWRDbGFzcyA9IG9yaWdpbmFsRXh0ZW5kLmNhbGwodGhpcywgcHJvdG8pO1xuICAgICAgICByZXR1cm4gZW50aXR5LnJlZ2lzdGVyQ2xhc3MoRXh0ZW5kZWRDbGFzcywgcHJvdG8udHlwZSk7XG4gICAgICB9O1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBlbnRpdHk7XG59XG4vKipcbiAqIEBwYXJhbSB7c3RyaW5nfEFycmF5LjxzdHJpbmc+fSBwcm9wZXJ0aWVzXG4gKi9cblxuXG5mdW5jdGlvbiBzZXRSZWFkT25seShvYmosIHByb3BlcnRpZXMpIHsvLyBGSVhNRSBJdCBzZWVtcyBicm9rZW4gaW4gSUU4IHNpbXVsYXRpb24gb2YgSUUxMVxuICAvLyBpZiAoIXpyVXRpbC5pc0FycmF5KHByb3BlcnRpZXMpKSB7XG4gIC8vICAgICBwcm9wZXJ0aWVzID0gcHJvcGVydGllcyAhPSBudWxsID8gW3Byb3BlcnRpZXNdIDogW107XG4gIC8vIH1cbiAgLy8genJVdGlsLmVhY2gocHJvcGVydGllcywgZnVuY3Rpb24gKHByb3ApIHtcbiAgLy8gICAgIHZhciB2YWx1ZSA9IG9ialtwcm9wXTtcbiAgLy8gICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eVxuICAvLyAgICAgICAgICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIHByb3AsIHtcbiAgLy8gICAgICAgICAgICAgdmFsdWU6IHZhbHVlLCB3cml0YWJsZTogZmFsc2VcbiAgLy8gICAgICAgICB9KTtcbiAgLy8gICAgIHpyVXRpbC5pc0FycmF5KG9ialtwcm9wXSlcbiAgLy8gICAgICAgICAmJiBPYmplY3QuZnJlZXplXG4gIC8vICAgICAgICAgJiYgT2JqZWN0LmZyZWV6ZShvYmpbcHJvcF0pO1xuICAvLyB9KTtcbn1cblxuZXhwb3J0cy5wYXJzZUNsYXNzVHlwZSA9IHBhcnNlQ2xhc3NUeXBlO1xuZXhwb3J0cy5lbmFibGVDbGFzc0V4dGVuZCA9IGVuYWJsZUNsYXNzRXh0ZW5kO1xuZXhwb3J0cy5lbmFibGVDbGFzc0NoZWNrID0gZW5hYmxlQ2xhc3NDaGVjaztcbmV4cG9ydHMuZW5hYmxlQ2xhc3NNYW5hZ2VtZW50ID0gZW5hYmxlQ2xhc3NNYW5hZ2VtZW50O1xuZXhwb3J0cy5zZXRSZWFkT25seSA9IHNldFJlYWRPbmx5OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/util/clazz.js
