__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calculateNodeStyling", function() { return calculateNodeStyling; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return calculateNodeHeight; });
// Thanks to https://github.com/andreypopp/react-textarea-autosize/

/**
 * calculateNodeHeight(uiTextNode, useCache = false)
 */
var HIDDEN_TEXTAREA_STYLE = "\n  min-height:0 !important;\n  max-height:none !important;\n  height:0 !important;\n  visibility:hidden !important;\n  overflow:hidden !important;\n  position:absolute !important;\n  z-index:-1000 !important;\n  top:0 !important;\n  right:0 !important\n";
var SIZING_STYLE = ['letter-spacing', 'line-height', 'padding-top', 'padding-bottom', 'font-family', 'font-weight', 'font-size', 'font-variant', 'text-rendering', 'text-transform', 'width', 'text-indent', 'padding-left', 'padding-right', 'border-width', 'box-sizing'];
var computedStyleCache = {};
var hiddenTextarea;
function calculateNodeStyling(node) {
  var useCache = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var nodeRef = node.getAttribute('id') || node.getAttribute('data-reactid') || node.getAttribute('name');

  if (useCache && computedStyleCache[nodeRef]) {
    return computedStyleCache[nodeRef];
  }

  var style = window.getComputedStyle(node);
  var boxSizing = style.getPropertyValue('box-sizing') || style.getPropertyValue('-moz-box-sizing') || style.getPropertyValue('-webkit-box-sizing');
  var paddingSize = parseFloat(style.getPropertyValue('padding-bottom')) + parseFloat(style.getPropertyValue('padding-top'));
  var borderSize = parseFloat(style.getPropertyValue('border-bottom-width')) + parseFloat(style.getPropertyValue('border-top-width'));
  var sizingStyle = SIZING_STYLE.map(function (name) {
    return "".concat(name, ":").concat(style.getPropertyValue(name));
  }).join(';');
  var nodeInfo = {
    sizingStyle: sizingStyle,
    paddingSize: paddingSize,
    borderSize: borderSize,
    boxSizing: boxSizing
  };

  if (useCache && nodeRef) {
    computedStyleCache[nodeRef] = nodeInfo;
  }

  return nodeInfo;
}
function calculateNodeHeight(uiTextNode) {
  var useCache = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var minRows = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var maxRows = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

  if (!hiddenTextarea) {
    hiddenTextarea = document.createElement('textarea');
    document.body.appendChild(hiddenTextarea);
  } // Fix wrap="off" issue
  // https://github.com/ant-design/ant-design/issues/6577


  if (uiTextNode.getAttribute('wrap')) {
    hiddenTextarea.setAttribute('wrap', uiTextNode.getAttribute('wrap'));
  } else {
    hiddenTextarea.removeAttribute('wrap');
  } // Copy all CSS properties that have an impact on the height of the content in
  // the textbox


  var _calculateNodeStyling = calculateNodeStyling(uiTextNode, useCache),
      paddingSize = _calculateNodeStyling.paddingSize,
      borderSize = _calculateNodeStyling.borderSize,
      boxSizing = _calculateNodeStyling.boxSizing,
      sizingStyle = _calculateNodeStyling.sizingStyle; // Need to have the overflow attribute to hide the scrollbar otherwise
  // text-lines will not calculated properly as the shadow will technically be
  // narrower for content


  hiddenTextarea.setAttribute('style', "".concat(sizingStyle, ";").concat(HIDDEN_TEXTAREA_STYLE));
  hiddenTextarea.value = uiTextNode.value || uiTextNode.placeholder || '';
  var minHeight = Number.MIN_SAFE_INTEGER;
  var maxHeight = Number.MAX_SAFE_INTEGER;
  var height = hiddenTextarea.scrollHeight;
  var overflowY;

  if (boxSizing === 'border-box') {
    // border-box: add border, since height = content + padding + border
    height += borderSize;
  } else if (boxSizing === 'content-box') {
    // remove padding, since height = content
    height -= paddingSize;
  }

  if (minRows !== null || maxRows !== null) {
    // measure height of a textarea with a single row
    hiddenTextarea.value = ' ';
    var singleRowHeight = hiddenTextarea.scrollHeight - paddingSize;

    if (minRows !== null) {
      minHeight = singleRowHeight * minRows;

      if (boxSizing === 'border-box') {
        minHeight = minHeight + paddingSize + borderSize;
      }

      height = Math.max(minHeight, height);
    }

    if (maxRows !== null) {
      maxHeight = singleRowHeight * maxRows;

      if (boxSizing === 'border-box') {
        maxHeight = maxHeight + paddingSize + borderSize;
      }

      overflowY = height > maxHeight ? '' : 'hidden';
      height = Math.min(maxHeight, height);
    }
  }

  return {
    height: height,
    minHeight: minHeight,
    maxHeight: maxHeight,
    overflowY: overflowY
  };
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9pbnB1dC9jYWxjdWxhdGVOb2RlSGVpZ2h0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9pbnB1dC9jYWxjdWxhdGVOb2RlSGVpZ2h0LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBUaGFua3MgdG8gaHR0cHM6Ly9naXRodWIuY29tL2FuZHJleXBvcHAvcmVhY3QtdGV4dGFyZWEtYXV0b3NpemUvXG4vKipcbiAqIGNhbGN1bGF0ZU5vZGVIZWlnaHQodWlUZXh0Tm9kZSwgdXNlQ2FjaGUgPSBmYWxzZSlcbiAqL1xuY29uc3QgSElEREVOX1RFWFRBUkVBX1NUWUxFID0gYFxuICBtaW4taGVpZ2h0OjAgIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDpub25lICFpbXBvcnRhbnQ7XG4gIGhlaWdodDowICFpbXBvcnRhbnQ7XG4gIHZpc2liaWxpdHk6aGlkZGVuICFpbXBvcnRhbnQ7XG4gIG92ZXJmbG93OmhpZGRlbiAhaW1wb3J0YW50O1xuICBwb3NpdGlvbjphYnNvbHV0ZSAhaW1wb3J0YW50O1xuICB6LWluZGV4Oi0xMDAwICFpbXBvcnRhbnQ7XG4gIHRvcDowICFpbXBvcnRhbnQ7XG4gIHJpZ2h0OjAgIWltcG9ydGFudFxuYDtcbmNvbnN0IFNJWklOR19TVFlMRSA9IFtcbiAgICAnbGV0dGVyLXNwYWNpbmcnLFxuICAgICdsaW5lLWhlaWdodCcsXG4gICAgJ3BhZGRpbmctdG9wJyxcbiAgICAncGFkZGluZy1ib3R0b20nLFxuICAgICdmb250LWZhbWlseScsXG4gICAgJ2ZvbnQtd2VpZ2h0JyxcbiAgICAnZm9udC1zaXplJyxcbiAgICAnZm9udC12YXJpYW50JyxcbiAgICAndGV4dC1yZW5kZXJpbmcnLFxuICAgICd0ZXh0LXRyYW5zZm9ybScsXG4gICAgJ3dpZHRoJyxcbiAgICAndGV4dC1pbmRlbnQnLFxuICAgICdwYWRkaW5nLWxlZnQnLFxuICAgICdwYWRkaW5nLXJpZ2h0JyxcbiAgICAnYm9yZGVyLXdpZHRoJyxcbiAgICAnYm94LXNpemluZycsXG5dO1xuY29uc3QgY29tcHV0ZWRTdHlsZUNhY2hlID0ge307XG5sZXQgaGlkZGVuVGV4dGFyZWE7XG5leHBvcnQgZnVuY3Rpb24gY2FsY3VsYXRlTm9kZVN0eWxpbmcobm9kZSwgdXNlQ2FjaGUgPSBmYWxzZSkge1xuICAgIGNvbnN0IG5vZGVSZWYgPSAobm9kZS5nZXRBdHRyaWJ1dGUoJ2lkJykgfHxcbiAgICAgICAgbm9kZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtcmVhY3RpZCcpIHx8XG4gICAgICAgIG5vZGUuZ2V0QXR0cmlidXRlKCduYW1lJykpO1xuICAgIGlmICh1c2VDYWNoZSAmJiBjb21wdXRlZFN0eWxlQ2FjaGVbbm9kZVJlZl0pIHtcbiAgICAgICAgcmV0dXJuIGNvbXB1dGVkU3R5bGVDYWNoZVtub2RlUmVmXTtcbiAgICB9XG4gICAgY29uc3Qgc3R5bGUgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShub2RlKTtcbiAgICBjb25zdCBib3hTaXppbmcgPSBzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdib3gtc2l6aW5nJykgfHxcbiAgICAgICAgc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnLW1vei1ib3gtc2l6aW5nJykgfHxcbiAgICAgICAgc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnLXdlYmtpdC1ib3gtc2l6aW5nJyk7XG4gICAgY29uc3QgcGFkZGluZ1NpemUgPSBwYXJzZUZsb2F0KHN0eWxlLmdldFByb3BlcnR5VmFsdWUoJ3BhZGRpbmctYm90dG9tJykpICtcbiAgICAgICAgcGFyc2VGbG9hdChzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdwYWRkaW5nLXRvcCcpKTtcbiAgICBjb25zdCBib3JkZXJTaXplID0gcGFyc2VGbG9hdChzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdib3JkZXItYm90dG9tLXdpZHRoJykpICtcbiAgICAgICAgcGFyc2VGbG9hdChzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdib3JkZXItdG9wLXdpZHRoJykpO1xuICAgIGNvbnN0IHNpemluZ1N0eWxlID0gU0laSU5HX1NUWUxFLm1hcChuYW1lID0+IGAke25hbWV9OiR7c3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShuYW1lKX1gKS5qb2luKCc7Jyk7XG4gICAgY29uc3Qgbm9kZUluZm8gPSB7XG4gICAgICAgIHNpemluZ1N0eWxlLFxuICAgICAgICBwYWRkaW5nU2l6ZSxcbiAgICAgICAgYm9yZGVyU2l6ZSxcbiAgICAgICAgYm94U2l6aW5nLFxuICAgIH07XG4gICAgaWYgKHVzZUNhY2hlICYmIG5vZGVSZWYpIHtcbiAgICAgICAgY29tcHV0ZWRTdHlsZUNhY2hlW25vZGVSZWZdID0gbm9kZUluZm87XG4gICAgfVxuICAgIHJldHVybiBub2RlSW5mbztcbn1cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGNhbGN1bGF0ZU5vZGVIZWlnaHQodWlUZXh0Tm9kZSwgdXNlQ2FjaGUgPSBmYWxzZSwgbWluUm93cyA9IG51bGwsIG1heFJvd3MgPSBudWxsKSB7XG4gICAgaWYgKCFoaWRkZW5UZXh0YXJlYSkge1xuICAgICAgICBoaWRkZW5UZXh0YXJlYSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3RleHRhcmVhJyk7XG4gICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoaGlkZGVuVGV4dGFyZWEpO1xuICAgIH1cbiAgICAvLyBGaXggd3JhcD1cIm9mZlwiIGlzc3VlXG4gICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvNjU3N1xuICAgIGlmICh1aVRleHROb2RlLmdldEF0dHJpYnV0ZSgnd3JhcCcpKSB7XG4gICAgICAgIGhpZGRlblRleHRhcmVhLnNldEF0dHJpYnV0ZSgnd3JhcCcsIHVpVGV4dE5vZGUuZ2V0QXR0cmlidXRlKCd3cmFwJykpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgaGlkZGVuVGV4dGFyZWEucmVtb3ZlQXR0cmlidXRlKCd3cmFwJyk7XG4gICAgfVxuICAgIC8vIENvcHkgYWxsIENTUyBwcm9wZXJ0aWVzIHRoYXQgaGF2ZSBhbiBpbXBhY3Qgb24gdGhlIGhlaWdodCBvZiB0aGUgY29udGVudCBpblxuICAgIC8vIHRoZSB0ZXh0Ym94XG4gICAgY29uc3QgeyBwYWRkaW5nU2l6ZSwgYm9yZGVyU2l6ZSwgYm94U2l6aW5nLCBzaXppbmdTdHlsZSB9ID0gY2FsY3VsYXRlTm9kZVN0eWxpbmcodWlUZXh0Tm9kZSwgdXNlQ2FjaGUpO1xuICAgIC8vIE5lZWQgdG8gaGF2ZSB0aGUgb3ZlcmZsb3cgYXR0cmlidXRlIHRvIGhpZGUgdGhlIHNjcm9sbGJhciBvdGhlcndpc2VcbiAgICAvLyB0ZXh0LWxpbmVzIHdpbGwgbm90IGNhbGN1bGF0ZWQgcHJvcGVybHkgYXMgdGhlIHNoYWRvdyB3aWxsIHRlY2huaWNhbGx5IGJlXG4gICAgLy8gbmFycm93ZXIgZm9yIGNvbnRlbnRcbiAgICBoaWRkZW5UZXh0YXJlYS5zZXRBdHRyaWJ1dGUoJ3N0eWxlJywgYCR7c2l6aW5nU3R5bGV9OyR7SElEREVOX1RFWFRBUkVBX1NUWUxFfWApO1xuICAgIGhpZGRlblRleHRhcmVhLnZhbHVlID0gdWlUZXh0Tm9kZS52YWx1ZSB8fCB1aVRleHROb2RlLnBsYWNlaG9sZGVyIHx8ICcnO1xuICAgIGxldCBtaW5IZWlnaHQgPSBOdW1iZXIuTUlOX1NBRkVfSU5URUdFUjtcbiAgICBsZXQgbWF4SGVpZ2h0ID0gTnVtYmVyLk1BWF9TQUZFX0lOVEVHRVI7XG4gICAgbGV0IGhlaWdodCA9IGhpZGRlblRleHRhcmVhLnNjcm9sbEhlaWdodDtcbiAgICBsZXQgb3ZlcmZsb3dZO1xuICAgIGlmIChib3hTaXppbmcgPT09ICdib3JkZXItYm94Jykge1xuICAgICAgICAvLyBib3JkZXItYm94OiBhZGQgYm9yZGVyLCBzaW5jZSBoZWlnaHQgPSBjb250ZW50ICsgcGFkZGluZyArIGJvcmRlclxuICAgICAgICBoZWlnaHQgKz0gYm9yZGVyU2l6ZTtcbiAgICB9XG4gICAgZWxzZSBpZiAoYm94U2l6aW5nID09PSAnY29udGVudC1ib3gnKSB7XG4gICAgICAgIC8vIHJlbW92ZSBwYWRkaW5nLCBzaW5jZSBoZWlnaHQgPSBjb250ZW50XG4gICAgICAgIGhlaWdodCAtPSBwYWRkaW5nU2l6ZTtcbiAgICB9XG4gICAgaWYgKG1pblJvd3MgIT09IG51bGwgfHwgbWF4Um93cyAhPT0gbnVsbCkge1xuICAgICAgICAvLyBtZWFzdXJlIGhlaWdodCBvZiBhIHRleHRhcmVhIHdpdGggYSBzaW5nbGUgcm93XG4gICAgICAgIGhpZGRlblRleHRhcmVhLnZhbHVlID0gJyAnO1xuICAgICAgICBjb25zdCBzaW5nbGVSb3dIZWlnaHQgPSBoaWRkZW5UZXh0YXJlYS5zY3JvbGxIZWlnaHQgLSBwYWRkaW5nU2l6ZTtcbiAgICAgICAgaWYgKG1pblJvd3MgIT09IG51bGwpIHtcbiAgICAgICAgICAgIG1pbkhlaWdodCA9IHNpbmdsZVJvd0hlaWdodCAqIG1pblJvd3M7XG4gICAgICAgICAgICBpZiAoYm94U2l6aW5nID09PSAnYm9yZGVyLWJveCcpIHtcbiAgICAgICAgICAgICAgICBtaW5IZWlnaHQgPSBtaW5IZWlnaHQgKyBwYWRkaW5nU2l6ZSArIGJvcmRlclNpemU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBoZWlnaHQgPSBNYXRoLm1heChtaW5IZWlnaHQsIGhlaWdodCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1heFJvd3MgIT09IG51bGwpIHtcbiAgICAgICAgICAgIG1heEhlaWdodCA9IHNpbmdsZVJvd0hlaWdodCAqIG1heFJvd3M7XG4gICAgICAgICAgICBpZiAoYm94U2l6aW5nID09PSAnYm9yZGVyLWJveCcpIHtcbiAgICAgICAgICAgICAgICBtYXhIZWlnaHQgPSBtYXhIZWlnaHQgKyBwYWRkaW5nU2l6ZSArIGJvcmRlclNpemU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvdmVyZmxvd1kgPSBoZWlnaHQgPiBtYXhIZWlnaHQgPyAnJyA6ICdoaWRkZW4nO1xuICAgICAgICAgICAgaGVpZ2h0ID0gTWF0aC5taW4obWF4SGVpZ2h0LCBoZWlnaHQpO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiB7IGhlaWdodCwgbWluSGVpZ2h0LCBtYXhIZWlnaHQsIG92ZXJmbG93WSB9O1xufVxuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7OztBQUdBO0FBV0E7QUFrQkE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFFQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBSUE7QUFYQTtBQWNBO0FBQ0E7QUFDQTtBQWhCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/input/calculateNodeHeight.js
