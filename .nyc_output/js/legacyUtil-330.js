__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMotion", function() { return getMotion; });
/* harmony import */ var rc_util_es_warning__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rc-util/es/warning */ "./node_modules/rc-util/es/warning.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}


function getMotion(_ref) {
  var prefixCls = _ref.prefixCls,
      motion = _ref.motion,
      openAnimation = _ref.openAnimation,
      openTransitionName = _ref.openTransitionName;

  if (motion) {
    return motion;
  }

  if (_typeof(openAnimation) === 'object' && openAnimation) {
    Object(rc_util_es_warning__WEBPACK_IMPORTED_MODULE_0__["default"])(false, 'Object type of `openAnimation` is removed. Please use `motion` instead.');
  } else if (typeof openAnimation === 'string') {
    return {
      motionName: "".concat(prefixCls, "-open-").concat(openAnimation)
    };
  }

  if (openTransitionName) {
    return {
      motionName: openTransitionName
    };
  }

  return null;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtbWVudS9lcy91dGlscy9sZWdhY3lVdGlsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtbWVudS9lcy91dGlscy9sZWdhY3lVdGlsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IGlmICh0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIikgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH07IH0gZWxzZSB7IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTsgfSByZXR1cm4gX3R5cGVvZihvYmopOyB9XG5cbmltcG9ydCB3YXJuaW5nIGZyb20gXCJyYy11dGlsL2VzL3dhcm5pbmdcIjtcbmV4cG9ydCBmdW5jdGlvbiBnZXRNb3Rpb24oX3JlZikge1xuICB2YXIgcHJlZml4Q2xzID0gX3JlZi5wcmVmaXhDbHMsXG4gICAgICBtb3Rpb24gPSBfcmVmLm1vdGlvbixcbiAgICAgIG9wZW5BbmltYXRpb24gPSBfcmVmLm9wZW5BbmltYXRpb24sXG4gICAgICBvcGVuVHJhbnNpdGlvbk5hbWUgPSBfcmVmLm9wZW5UcmFuc2l0aW9uTmFtZTtcblxuICBpZiAobW90aW9uKSB7XG4gICAgcmV0dXJuIG1vdGlvbjtcbiAgfVxuXG4gIGlmIChfdHlwZW9mKG9wZW5BbmltYXRpb24pID09PSAnb2JqZWN0JyAmJiBvcGVuQW5pbWF0aW9uKSB7XG4gICAgd2FybmluZyhmYWxzZSwgJ09iamVjdCB0eXBlIG9mIGBvcGVuQW5pbWF0aW9uYCBpcyByZW1vdmVkLiBQbGVhc2UgdXNlIGBtb3Rpb25gIGluc3RlYWQuJyk7XG4gIH0gZWxzZSBpZiAodHlwZW9mIG9wZW5BbmltYXRpb24gPT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG1vdGlvbk5hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItb3Blbi1cIikuY29uY2F0KG9wZW5BbmltYXRpb24pXG4gICAgfTtcbiAgfVxuXG4gIGlmIChvcGVuVHJhbnNpdGlvbk5hbWUpIHtcbiAgICByZXR1cm4ge1xuICAgICAgbW90aW9uTmFtZTogb3BlblRyYW5zaXRpb25OYW1lXG4gICAgfTtcbiAgfVxuXG4gIHJldHVybiBudWxsO1xufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-menu/es/utils/legacyUtil.js
