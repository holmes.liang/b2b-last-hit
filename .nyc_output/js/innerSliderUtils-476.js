

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.canUseDOM = exports.slidesOnLeft = exports.slidesOnRight = exports.siblingDirection = exports.getTotalSlides = exports.getPostClones = exports.getPreClones = exports.getTrackLeft = exports.getTrackAnimateCSS = exports.getTrackCSS = exports.checkSpecKeys = exports.getSlideCount = exports.checkNavigable = exports.getNavigableIndexes = exports.swipeEnd = exports.swipeMove = exports.swipeStart = exports.keyHandler = exports.changeSlide = exports.slideHandler = exports.initializedState = exports.extractObject = exports.canGoNext = exports.getSwipeDirection = exports.getHeight = exports.getWidth = exports.lazySlidesOnRight = exports.lazySlidesOnLeft = exports.lazyEndIndex = exports.lazyStartIndex = exports.getRequiredLazySlides = exports.getOnDemandLazySlides = void 0;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var _reactDom = _interopRequireDefault(__webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(source, true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(source).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var getOnDemandLazySlides = function getOnDemandLazySlides(spec) {
  var onDemandSlides = [];
  var startIndex = lazyStartIndex(spec);
  var endIndex = lazyEndIndex(spec);

  for (var slideIndex = startIndex; slideIndex < endIndex; slideIndex++) {
    if (spec.lazyLoadedList.indexOf(slideIndex) < 0) {
      onDemandSlides.push(slideIndex);
    }
  }

  return onDemandSlides;
}; // return list of slides that need to be present


exports.getOnDemandLazySlides = getOnDemandLazySlides;

var getRequiredLazySlides = function getRequiredLazySlides(spec) {
  var requiredSlides = [];
  var startIndex = lazyStartIndex(spec);
  var endIndex = lazyEndIndex(spec);

  for (var slideIndex = startIndex; slideIndex < endIndex; slideIndex++) {
    requiredSlides.push(slideIndex);
  }

  return requiredSlides;
}; // startIndex that needs to be present


exports.getRequiredLazySlides = getRequiredLazySlides;

var lazyStartIndex = function lazyStartIndex(spec) {
  return spec.currentSlide - lazySlidesOnLeft(spec);
};

exports.lazyStartIndex = lazyStartIndex;

var lazyEndIndex = function lazyEndIndex(spec) {
  return spec.currentSlide + lazySlidesOnRight(spec);
};

exports.lazyEndIndex = lazyEndIndex;

var lazySlidesOnLeft = function lazySlidesOnLeft(spec) {
  return spec.centerMode ? Math.floor(spec.slidesToShow / 2) + (parseInt(spec.centerPadding) > 0 ? 1 : 0) : 0;
};

exports.lazySlidesOnLeft = lazySlidesOnLeft;

var lazySlidesOnRight = function lazySlidesOnRight(spec) {
  return spec.centerMode ? Math.floor((spec.slidesToShow - 1) / 2) + 1 + (parseInt(spec.centerPadding) > 0 ? 1 : 0) : spec.slidesToShow;
}; // get width of an element


exports.lazySlidesOnRight = lazySlidesOnRight;

var getWidth = function getWidth(elem) {
  return elem && elem.offsetWidth || 0;
};

exports.getWidth = getWidth;

var getHeight = function getHeight(elem) {
  return elem && elem.offsetHeight || 0;
};

exports.getHeight = getHeight;

var getSwipeDirection = function getSwipeDirection(touchObject) {
  var verticalSwiping = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var xDist, yDist, r, swipeAngle;
  xDist = touchObject.startX - touchObject.curX;
  yDist = touchObject.startY - touchObject.curY;
  r = Math.atan2(yDist, xDist);
  swipeAngle = Math.round(r * 180 / Math.PI);

  if (swipeAngle < 0) {
    swipeAngle = 360 - Math.abs(swipeAngle);
  }

  if (swipeAngle <= 45 && swipeAngle >= 0 || swipeAngle <= 360 && swipeAngle >= 315) {
    return "left";
  }

  if (swipeAngle >= 135 && swipeAngle <= 225) {
    return "right";
  }

  if (verticalSwiping === true) {
    if (swipeAngle >= 35 && swipeAngle <= 135) {
      return "up";
    } else {
      return "down";
    }
  }

  return "vertical";
}; // whether or not we can go next


exports.getSwipeDirection = getSwipeDirection;

var canGoNext = function canGoNext(spec) {
  var canGo = true;

  if (!spec.infinite) {
    if (spec.centerMode && spec.currentSlide >= spec.slideCount - 1) {
      canGo = false;
    } else if (spec.slideCount <= spec.slidesToShow || spec.currentSlide >= spec.slideCount - spec.slidesToShow) {
      canGo = false;
    }
  }

  return canGo;
}; // given an object and a list of keys, return new object with given keys


exports.canGoNext = canGoNext;

var extractObject = function extractObject(spec, keys) {
  var newObject = {};
  keys.forEach(function (key) {
    return newObject[key] = spec[key];
  });
  return newObject;
}; // get initialized state


exports.extractObject = extractObject;

var initializedState = function initializedState(spec) {
  // spec also contains listRef, trackRef
  var slideCount = _react["default"].Children.count(spec.children);

  var listWidth = Math.ceil(getWidth(_reactDom["default"].findDOMNode(spec.listRef)));
  var trackWidth = Math.ceil(getWidth(_reactDom["default"].findDOMNode(spec.trackRef)));
  var slideWidth;

  if (!spec.vertical) {
    var centerPaddingAdj = spec.centerMode && parseInt(spec.centerPadding) * 2;

    if (typeof spec.centerPadding === "string" && spec.centerPadding.slice(-1) === "%") {
      centerPaddingAdj *= listWidth / 100;
    }

    slideWidth = Math.ceil((listWidth - centerPaddingAdj) / spec.slidesToShow);
  } else {
    slideWidth = listWidth;
  }

  var slideHeight = _reactDom["default"].findDOMNode(spec.listRef) && getHeight(_reactDom["default"].findDOMNode(spec.listRef).querySelector('[data-index="0"]'));
  var listHeight = slideHeight * spec.slidesToShow;
  var currentSlide = spec.currentSlide === undefined ? spec.initialSlide : spec.currentSlide;

  if (spec.rtl && spec.currentSlide === undefined) {
    currentSlide = slideCount - 1 - spec.initialSlide;
  }

  var lazyLoadedList = spec.lazyLoadedList || [];
  var slidesToLoad = getOnDemandLazySlides({
    currentSlide: currentSlide,
    lazyLoadedList: lazyLoadedList
  }, spec);
  lazyLoadedList.concat(slidesToLoad);
  var state = {
    slideCount: slideCount,
    slideWidth: slideWidth,
    listWidth: listWidth,
    trackWidth: trackWidth,
    currentSlide: currentSlide,
    slideHeight: slideHeight,
    listHeight: listHeight,
    lazyLoadedList: lazyLoadedList
  };

  if (spec.autoplaying === null && spec.autoplay) {
    state["autoplaying"] = "playing";
  }

  return state;
};

exports.initializedState = initializedState;

var slideHandler = function slideHandler(spec) {
  var waitForAnimate = spec.waitForAnimate,
      animating = spec.animating,
      fade = spec.fade,
      infinite = spec.infinite,
      index = spec.index,
      slideCount = spec.slideCount,
      lazyLoadedList = spec.lazyLoadedList,
      lazyLoad = spec.lazyLoad,
      currentSlide = spec.currentSlide,
      centerMode = spec.centerMode,
      slidesToScroll = spec.slidesToScroll,
      slidesToShow = spec.slidesToShow,
      useCSS = spec.useCSS;
  if (waitForAnimate && animating) return {};
  var animationSlide = index,
      finalSlide,
      animationLeft,
      finalLeft;
  var state = {},
      nextState = {};

  if (fade) {
    if (!infinite && (index < 0 || index >= slideCount)) return {};

    if (index < 0) {
      animationSlide = index + slideCount;
    } else if (index >= slideCount) {
      animationSlide = index - slideCount;
    }

    if (lazyLoad && lazyLoadedList.indexOf(animationSlide) < 0) {
      lazyLoadedList.push(animationSlide);
    }

    state = {
      animating: true,
      currentSlide: animationSlide,
      lazyLoadedList: lazyLoadedList
    };
    nextState = {
      animating: false
    };
  } else {
    finalSlide = animationSlide;

    if (animationSlide < 0) {
      finalSlide = animationSlide + slideCount;
      if (!infinite) finalSlide = 0;else if (slideCount % slidesToScroll !== 0) finalSlide = slideCount - slideCount % slidesToScroll;
    } else if (!canGoNext(spec) && animationSlide > currentSlide) {
      animationSlide = finalSlide = currentSlide;
    } else if (centerMode && animationSlide >= slideCount) {
      animationSlide = infinite ? slideCount : slideCount - 1;
      finalSlide = infinite ? 0 : slideCount - 1;
    } else if (animationSlide >= slideCount) {
      finalSlide = animationSlide - slideCount;
      if (!infinite) finalSlide = slideCount - slidesToShow;else if (slideCount % slidesToScroll !== 0) finalSlide = 0;
    }

    animationLeft = getTrackLeft(_objectSpread({}, spec, {
      slideIndex: animationSlide
    }));
    finalLeft = getTrackLeft(_objectSpread({}, spec, {
      slideIndex: finalSlide
    }));

    if (!infinite) {
      if (animationLeft === finalLeft) animationSlide = finalSlide;
      animationLeft = finalLeft;
    }

    lazyLoad && lazyLoadedList.concat(getOnDemandLazySlides(_objectSpread({}, spec, {
      currentSlide: animationSlide
    })));

    if (!useCSS) {
      state = {
        currentSlide: finalSlide,
        trackStyle: getTrackCSS(_objectSpread({}, spec, {
          left: finalLeft
        })),
        lazyLoadedList: lazyLoadedList
      };
    } else {
      state = {
        animating: true,
        currentSlide: finalSlide,
        trackStyle: getTrackAnimateCSS(_objectSpread({}, spec, {
          left: animationLeft
        })),
        lazyLoadedList: lazyLoadedList
      };
      nextState = {
        animating: false,
        currentSlide: finalSlide,
        trackStyle: getTrackCSS(_objectSpread({}, spec, {
          left: finalLeft
        })),
        swipeLeft: null
      };
    }
  }

  return {
    state: state,
    nextState: nextState
  };
};

exports.slideHandler = slideHandler;

var changeSlide = function changeSlide(spec, options) {
  var indexOffset, previousInt, slideOffset, unevenOffset, targetSlide;
  var slidesToScroll = spec.slidesToScroll,
      slidesToShow = spec.slidesToShow,
      slideCount = spec.slideCount,
      currentSlide = spec.currentSlide,
      lazyLoad = spec.lazyLoad,
      infinite = spec.infinite;
  unevenOffset = slideCount % slidesToScroll !== 0;
  indexOffset = unevenOffset ? 0 : (slideCount - currentSlide) % slidesToScroll;

  if (options.message === "previous") {
    slideOffset = indexOffset === 0 ? slidesToScroll : slidesToShow - indexOffset;
    targetSlide = currentSlide - slideOffset;

    if (lazyLoad && !infinite) {
      previousInt = currentSlide - slideOffset;
      targetSlide = previousInt === -1 ? slideCount - 1 : previousInt;
    }
  } else if (options.message === "next") {
    slideOffset = indexOffset === 0 ? slidesToScroll : indexOffset;
    targetSlide = currentSlide + slideOffset;

    if (lazyLoad && !infinite) {
      targetSlide = (currentSlide + slidesToScroll) % slideCount + indexOffset;
    }
  } else if (options.message === "dots") {
    // Click on dots
    targetSlide = options.index * options.slidesToScroll;

    if (targetSlide === options.currentSlide) {
      return null;
    }
  } else if (options.message === "children") {
    // Click on the slides
    targetSlide = options.index;

    if (targetSlide === options.currentSlide) {
      return null;
    }

    if (infinite) {
      var direction = siblingDirection(_objectSpread({}, spec, {
        targetSlide: targetSlide
      }));

      if (targetSlide > options.currentSlide && direction === "left") {
        targetSlide = targetSlide - slideCount;
      } else if (targetSlide < options.currentSlide && direction === "right") {
        targetSlide = targetSlide + slideCount;
      }
    }
  } else if (options.message === "index") {
    targetSlide = Number(options.index);

    if (targetSlide === options.currentSlide) {
      return null;
    }
  }

  return targetSlide;
};

exports.changeSlide = changeSlide;

var keyHandler = function keyHandler(e, accessibility, rtl) {
  if (e.target.tagName.match("TEXTAREA|INPUT|SELECT") || !accessibility) return "";
  if (e.keyCode === 37) return rtl ? "next" : "previous";
  if (e.keyCode === 39) return rtl ? "previous" : "next";
  return "";
};

exports.keyHandler = keyHandler;

var swipeStart = function swipeStart(e, swipe, draggable) {
  e.target.tagName === "IMG" && e.preventDefault();
  if (!swipe || !draggable && e.type.indexOf("mouse") !== -1) return "";
  return {
    dragging: true,
    touchObject: {
      startX: e.touches ? e.touches[0].pageX : e.clientX,
      startY: e.touches ? e.touches[0].pageY : e.clientY,
      curX: e.touches ? e.touches[0].pageX : e.clientX,
      curY: e.touches ? e.touches[0].pageY : e.clientY
    }
  };
};

exports.swipeStart = swipeStart;

var swipeMove = function swipeMove(e, spec) {
  // spec also contains, trackRef and slideIndex
  var scrolling = spec.scrolling,
      animating = spec.animating,
      vertical = spec.vertical,
      swipeToSlide = spec.swipeToSlide,
      verticalSwiping = spec.verticalSwiping,
      rtl = spec.rtl,
      currentSlide = spec.currentSlide,
      edgeFriction = spec.edgeFriction,
      edgeDragged = spec.edgeDragged,
      onEdge = spec.onEdge,
      swiped = spec.swiped,
      swiping = spec.swiping,
      slideCount = spec.slideCount,
      slidesToScroll = spec.slidesToScroll,
      infinite = spec.infinite,
      touchObject = spec.touchObject,
      swipeEvent = spec.swipeEvent,
      listHeight = spec.listHeight,
      listWidth = spec.listWidth;
  if (scrolling) return;
  if (animating) return e.preventDefault();
  if (vertical && swipeToSlide && verticalSwiping) e.preventDefault();
  var swipeLeft,
      state = {};
  var curLeft = getTrackLeft(spec);
  touchObject.curX = e.touches ? e.touches[0].pageX : e.clientX;
  touchObject.curY = e.touches ? e.touches[0].pageY : e.clientY;
  touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(touchObject.curX - touchObject.startX, 2)));
  var verticalSwipeLength = Math.round(Math.sqrt(Math.pow(touchObject.curY - touchObject.startY, 2)));

  if (!verticalSwiping && !swiping && verticalSwipeLength > 10) {
    return {
      scrolling: true
    };
  }

  if (verticalSwiping) touchObject.swipeLength = verticalSwipeLength;
  var positionOffset = (!rtl ? 1 : -1) * (touchObject.curX > touchObject.startX ? 1 : -1);
  if (verticalSwiping) positionOffset = touchObject.curY > touchObject.startY ? 1 : -1;
  var dotCount = Math.ceil(slideCount / slidesToScroll);
  var swipeDirection = getSwipeDirection(spec.touchObject, verticalSwiping);
  var touchSwipeLength = touchObject.swipeLength;

  if (!infinite) {
    if (currentSlide === 0 && swipeDirection === "right" || currentSlide + 1 >= dotCount && swipeDirection === "left" || !canGoNext(spec) && swipeDirection === "left") {
      touchSwipeLength = touchObject.swipeLength * edgeFriction;

      if (edgeDragged === false && onEdge) {
        onEdge(swipeDirection);
        state["edgeDragged"] = true;
      }
    }
  }

  if (!swiped && swipeEvent) {
    swipeEvent(swipeDirection);
    state["swiped"] = true;
  }

  if (!vertical) {
    if (!rtl) {
      swipeLeft = curLeft + touchSwipeLength * positionOffset;
    } else {
      swipeLeft = curLeft - touchSwipeLength * positionOffset;
    }
  } else {
    swipeLeft = curLeft + touchSwipeLength * (listHeight / listWidth) * positionOffset;
  }

  if (verticalSwiping) {
    swipeLeft = curLeft + touchSwipeLength * positionOffset;
  }

  state = _objectSpread({}, state, {
    touchObject: touchObject,
    swipeLeft: swipeLeft,
    trackStyle: getTrackCSS(_objectSpread({}, spec, {
      left: swipeLeft
    }))
  });

  if (Math.abs(touchObject.curX - touchObject.startX) < Math.abs(touchObject.curY - touchObject.startY) * 0.8) {
    return state;
  }

  if (touchObject.swipeLength > 10) {
    state["swiping"] = true;
    e.preventDefault();
  }

  return state;
};

exports.swipeMove = swipeMove;

var swipeEnd = function swipeEnd(e, spec) {
  var dragging = spec.dragging,
      swipe = spec.swipe,
      touchObject = spec.touchObject,
      listWidth = spec.listWidth,
      touchThreshold = spec.touchThreshold,
      verticalSwiping = spec.verticalSwiping,
      listHeight = spec.listHeight,
      currentSlide = spec.currentSlide,
      swipeToSlide = spec.swipeToSlide,
      scrolling = spec.scrolling,
      onSwipe = spec.onSwipe;

  if (!dragging) {
    if (swipe) e.preventDefault();
    return {};
  }

  var minSwipe = verticalSwiping ? listHeight / touchThreshold : listWidth / touchThreshold;
  var swipeDirection = getSwipeDirection(touchObject, verticalSwiping); // reset the state of touch related state variables.

  var state = {
    dragging: false,
    edgeDragged: false,
    scrolling: false,
    swiping: false,
    swiped: false,
    swipeLeft: null,
    touchObject: {}
  };

  if (scrolling) {
    return state;
  }

  if (!touchObject.swipeLength) {
    return state;
  }

  if (touchObject.swipeLength > minSwipe) {
    e.preventDefault();

    if (onSwipe) {
      onSwipe(swipeDirection);
    }

    var slideCount, newSlide;

    switch (swipeDirection) {
      case "left":
      case "up":
        newSlide = currentSlide + getSlideCount(spec);
        slideCount = swipeToSlide ? checkNavigable(spec, newSlide) : newSlide;
        state["currentDirection"] = 0;
        break;

      case "right":
      case "down":
        newSlide = currentSlide - getSlideCount(spec);
        slideCount = swipeToSlide ? checkNavigable(spec, newSlide) : newSlide;
        state["currentDirection"] = 1;
        break;

      default:
        slideCount = currentSlide;
    }

    state["triggerSlideHandler"] = slideCount;
  } else {
    // Adjust the track back to it's original position.
    var currentLeft = getTrackLeft(spec);
    state["trackStyle"] = getTrackAnimateCSS(_objectSpread({}, spec, {
      left: currentLeft
    }));
  }

  return state;
};

exports.swipeEnd = swipeEnd;

var getNavigableIndexes = function getNavigableIndexes(spec) {
  var max = spec.infinite ? spec.slideCount * 2 : spec.slideCount;
  var breakpoint = spec.infinite ? spec.slidesToShow * -1 : 0;
  var counter = spec.infinite ? spec.slidesToShow * -1 : 0;
  var indexes = [];

  while (breakpoint < max) {
    indexes.push(breakpoint);
    breakpoint = counter + spec.slidesToScroll;
    counter += Math.min(spec.slidesToScroll, spec.slidesToShow);
  }

  return indexes;
};

exports.getNavigableIndexes = getNavigableIndexes;

var checkNavigable = function checkNavigable(spec, index) {
  var navigables = getNavigableIndexes(spec);
  var prevNavigable = 0;

  if (index > navigables[navigables.length - 1]) {
    index = navigables[navigables.length - 1];
  } else {
    for (var n in navigables) {
      if (index < navigables[n]) {
        index = prevNavigable;
        break;
      }

      prevNavigable = navigables[n];
    }
  }

  return index;
};

exports.checkNavigable = checkNavigable;

var getSlideCount = function getSlideCount(spec) {
  var centerOffset = spec.centerMode ? spec.slideWidth * Math.floor(spec.slidesToShow / 2) : 0;

  if (spec.swipeToSlide) {
    var swipedSlide;

    var slickList = _reactDom["default"].findDOMNode(spec.listRef);

    var slides = slickList.querySelectorAll(".slick-slide");
    Array.from(slides).every(function (slide) {
      if (!spec.vertical) {
        if (slide.offsetLeft - centerOffset + getWidth(slide) / 2 > spec.swipeLeft * -1) {
          swipedSlide = slide;
          return false;
        }
      } else {
        if (slide.offsetTop + getHeight(slide) / 2 > spec.swipeLeft * -1) {
          swipedSlide = slide;
          return false;
        }
      }

      return true;
    });

    if (!swipedSlide) {
      return 0;
    }

    var currentIndex = spec.rtl === true ? spec.slideCount - spec.currentSlide : spec.currentSlide;
    var slidesTraversed = Math.abs(swipedSlide.dataset.index - currentIndex) || 1;
    return slidesTraversed;
  } else {
    return spec.slidesToScroll;
  }
};

exports.getSlideCount = getSlideCount;

var checkSpecKeys = function checkSpecKeys(spec, keysArray) {
  return keysArray.reduce(function (value, key) {
    return value && spec.hasOwnProperty(key);
  }, true) ? null : console.error("Keys Missing:", spec);
};

exports.checkSpecKeys = checkSpecKeys;

var getTrackCSS = function getTrackCSS(spec) {
  checkSpecKeys(spec, ["left", "variableWidth", "slideCount", "slidesToShow", "slideWidth"]);
  var trackWidth, trackHeight;
  var trackChildren = spec.slideCount + 2 * spec.slidesToShow;

  if (!spec.vertical) {
    trackWidth = getTotalSlides(spec) * spec.slideWidth;
  } else {
    trackHeight = trackChildren * spec.slideHeight;
  }

  var style = {
    opacity: 1,
    transition: "",
    WebkitTransition: ""
  };

  if (spec.useTransform) {
    var WebkitTransform = !spec.vertical ? "translate3d(" + spec.left + "px, 0px, 0px)" : "translate3d(0px, " + spec.left + "px, 0px)";
    var transform = !spec.vertical ? "translate3d(" + spec.left + "px, 0px, 0px)" : "translate3d(0px, " + spec.left + "px, 0px)";
    var msTransform = !spec.vertical ? "translateX(" + spec.left + "px)" : "translateY(" + spec.left + "px)";
    style = _objectSpread({}, style, {
      WebkitTransform: WebkitTransform,
      transform: transform,
      msTransform: msTransform
    });
  } else {
    if (spec.vertical) {
      style["top"] = spec.left;
    } else {
      style["left"] = spec.left;
    }
  }

  if (spec.fade) style = {
    opacity: 1
  };
  if (trackWidth) style.width = trackWidth;
  if (trackHeight) style.height = trackHeight; // Fallback for IE8

  if (window && !window.addEventListener && window.attachEvent) {
    if (!spec.vertical) {
      style.marginLeft = spec.left + "px";
    } else {
      style.marginTop = spec.left + "px";
    }
  }

  return style;
};

exports.getTrackCSS = getTrackCSS;

var getTrackAnimateCSS = function getTrackAnimateCSS(spec) {
  checkSpecKeys(spec, ["left", "variableWidth", "slideCount", "slidesToShow", "slideWidth", "speed", "cssEase"]);
  var style = getTrackCSS(spec); // useCSS is true by default so it can be undefined

  if (spec.useTransform) {
    style.WebkitTransition = "-webkit-transform " + spec.speed + "ms " + spec.cssEase;
    style.transition = "transform " + spec.speed + "ms " + spec.cssEase;
  } else {
    if (spec.vertical) {
      style.transition = "top " + spec.speed + "ms " + spec.cssEase;
    } else {
      style.transition = "left " + spec.speed + "ms " + spec.cssEase;
    }
  }

  return style;
};

exports.getTrackAnimateCSS = getTrackAnimateCSS;

var getTrackLeft = function getTrackLeft(spec) {
  if (spec.unslick) {
    return 0;
  }

  checkSpecKeys(spec, ["slideIndex", "trackRef", "infinite", "centerMode", "slideCount", "slidesToShow", "slidesToScroll", "slideWidth", "listWidth", "variableWidth", "slideHeight"]);
  var slideIndex = spec.slideIndex,
      trackRef = spec.trackRef,
      infinite = spec.infinite,
      centerMode = spec.centerMode,
      slideCount = spec.slideCount,
      slidesToShow = spec.slidesToShow,
      slidesToScroll = spec.slidesToScroll,
      slideWidth = spec.slideWidth,
      listWidth = spec.listWidth,
      variableWidth = spec.variableWidth,
      slideHeight = spec.slideHeight,
      fade = spec.fade,
      vertical = spec.vertical;
  var slideOffset = 0;
  var targetLeft;
  var targetSlide;
  var verticalOffset = 0;

  if (fade || spec.slideCount === 1) {
    return 0;
  }

  var slidesToOffset = 0;

  if (infinite) {
    slidesToOffset = -getPreClones(spec); // bring active slide to the beginning of visual area
    // if next scroll doesn't have enough children, just reach till the end of original slides instead of shifting slidesToScroll children

    if (slideCount % slidesToScroll !== 0 && slideIndex + slidesToScroll > slideCount) {
      slidesToOffset = -(slideIndex > slideCount ? slidesToShow - (slideIndex - slideCount) : slideCount % slidesToScroll);
    } // shift current slide to center of the frame


    if (centerMode) {
      slidesToOffset += parseInt(slidesToShow / 2);
    }
  } else {
    if (slideCount % slidesToScroll !== 0 && slideIndex + slidesToScroll > slideCount) {
      slidesToOffset = slidesToShow - slideCount % slidesToScroll;
    }

    if (centerMode) {
      slidesToOffset = parseInt(slidesToShow / 2);
    }
  }

  slideOffset = slidesToOffset * slideWidth;
  verticalOffset = slidesToOffset * slideHeight;

  if (!vertical) {
    targetLeft = slideIndex * slideWidth * -1 + slideOffset;
  } else {
    targetLeft = slideIndex * slideHeight * -1 + verticalOffset;
  }

  if (variableWidth === true) {
    var targetSlideIndex;

    var trackElem = _reactDom["default"].findDOMNode(trackRef);

    targetSlideIndex = slideIndex + getPreClones(spec);
    targetSlide = trackElem && trackElem.childNodes[targetSlideIndex];
    targetLeft = targetSlide ? targetSlide.offsetLeft * -1 : 0;

    if (centerMode === true) {
      targetSlideIndex = infinite ? slideIndex + getPreClones(spec) : slideIndex;
      targetSlide = trackElem && trackElem.children[targetSlideIndex];
      targetLeft = 0;

      for (var slide = 0; slide < targetSlideIndex; slide++) {
        targetLeft -= trackElem && trackElem.children[slide] && trackElem.children[slide].offsetWidth;
      }

      targetLeft -= parseInt(spec.centerPadding);
      targetLeft += targetSlide && (listWidth - targetSlide.offsetWidth) / 2;
    }
  }

  return targetLeft;
};

exports.getTrackLeft = getTrackLeft;

var getPreClones = function getPreClones(spec) {
  if (spec.unslick || !spec.infinite) {
    return 0;
  }

  if (spec.variableWidth) {
    return spec.slideCount;
  }

  return spec.slidesToShow + (spec.centerMode ? 1 : 0);
};

exports.getPreClones = getPreClones;

var getPostClones = function getPostClones(spec) {
  if (spec.unslick || !spec.infinite) {
    return 0;
  }

  return spec.slideCount;
};

exports.getPostClones = getPostClones;

var getTotalSlides = function getTotalSlides(spec) {
  return spec.slideCount === 1 ? 1 : getPreClones(spec) + spec.slideCount + getPostClones(spec);
};

exports.getTotalSlides = getTotalSlides;

var siblingDirection = function siblingDirection(spec) {
  if (spec.targetSlide > spec.currentSlide) {
    if (spec.targetSlide > spec.currentSlide + slidesOnRight(spec)) {
      return "left";
    }

    return "right";
  } else {
    if (spec.targetSlide < spec.currentSlide - slidesOnLeft(spec)) {
      return "right";
    }

    return "left";
  }
};

exports.siblingDirection = siblingDirection;

var slidesOnRight = function slidesOnRight(_ref) {
  var slidesToShow = _ref.slidesToShow,
      centerMode = _ref.centerMode,
      rtl = _ref.rtl,
      centerPadding = _ref.centerPadding; // returns no of slides on the right of active slide

  if (centerMode) {
    var right = (slidesToShow - 1) / 2 + 1;
    if (parseInt(centerPadding) > 0) right += 1;
    if (rtl && slidesToShow % 2 === 0) right += 1;
    return right;
  }

  if (rtl) {
    return 0;
  }

  return slidesToShow - 1;
};

exports.slidesOnRight = slidesOnRight;

var slidesOnLeft = function slidesOnLeft(_ref2) {
  var slidesToShow = _ref2.slidesToShow,
      centerMode = _ref2.centerMode,
      rtl = _ref2.rtl,
      centerPadding = _ref2.centerPadding; // returns no of slides on the left of active slide

  if (centerMode) {
    var left = (slidesToShow - 1) / 2 + 1;
    if (parseInt(centerPadding) > 0) left += 1;
    if (!rtl && slidesToShow % 2 === 0) left += 1;
    return left;
  }

  if (rtl) {
    return slidesToShow - 1;
  }

  return 0;
};

exports.slidesOnLeft = slidesOnLeft;

var canUseDOM = function canUseDOM() {
  return !!(typeof window !== "undefined" && window.document && window.document.createElement);
};

exports.canUseDOM = canUseDOM;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3Qtc2xpY2svbGliL3V0aWxzL2lubmVyU2xpZGVyVXRpbHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yZWFjdC1zbGljay9saWIvdXRpbHMvaW5uZXJTbGlkZXJVdGlscy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuY2FuVXNlRE9NID0gZXhwb3J0cy5zbGlkZXNPbkxlZnQgPSBleHBvcnRzLnNsaWRlc09uUmlnaHQgPSBleHBvcnRzLnNpYmxpbmdEaXJlY3Rpb24gPSBleHBvcnRzLmdldFRvdGFsU2xpZGVzID0gZXhwb3J0cy5nZXRQb3N0Q2xvbmVzID0gZXhwb3J0cy5nZXRQcmVDbG9uZXMgPSBleHBvcnRzLmdldFRyYWNrTGVmdCA9IGV4cG9ydHMuZ2V0VHJhY2tBbmltYXRlQ1NTID0gZXhwb3J0cy5nZXRUcmFja0NTUyA9IGV4cG9ydHMuY2hlY2tTcGVjS2V5cyA9IGV4cG9ydHMuZ2V0U2xpZGVDb3VudCA9IGV4cG9ydHMuY2hlY2tOYXZpZ2FibGUgPSBleHBvcnRzLmdldE5hdmlnYWJsZUluZGV4ZXMgPSBleHBvcnRzLnN3aXBlRW5kID0gZXhwb3J0cy5zd2lwZU1vdmUgPSBleHBvcnRzLnN3aXBlU3RhcnQgPSBleHBvcnRzLmtleUhhbmRsZXIgPSBleHBvcnRzLmNoYW5nZVNsaWRlID0gZXhwb3J0cy5zbGlkZUhhbmRsZXIgPSBleHBvcnRzLmluaXRpYWxpemVkU3RhdGUgPSBleHBvcnRzLmV4dHJhY3RPYmplY3QgPSBleHBvcnRzLmNhbkdvTmV4dCA9IGV4cG9ydHMuZ2V0U3dpcGVEaXJlY3Rpb24gPSBleHBvcnRzLmdldEhlaWdodCA9IGV4cG9ydHMuZ2V0V2lkdGggPSBleHBvcnRzLmxhenlTbGlkZXNPblJpZ2h0ID0gZXhwb3J0cy5sYXp5U2xpZGVzT25MZWZ0ID0gZXhwb3J0cy5sYXp5RW5kSW5kZXggPSBleHBvcnRzLmxhenlTdGFydEluZGV4ID0gZXhwb3J0cy5nZXRSZXF1aXJlZExhenlTbGlkZXMgPSBleHBvcnRzLmdldE9uRGVtYW5kTGF6eVNsaWRlcyA9IHZvaWQgMDtcblxudmFyIF9yZWFjdCA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQocmVxdWlyZShcInJlYWN0XCIpKTtcblxudmFyIF9yZWFjdERvbSA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQocmVxdWlyZShcInJlYWN0LWRvbVwiKSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IFwiZGVmYXVsdFwiOiBvYmogfTsgfVxuXG5mdW5jdGlvbiBvd25LZXlzKG9iamVjdCwgZW51bWVyYWJsZU9ubHkpIHsgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhvYmplY3QpOyBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykgeyB2YXIgc3ltYm9scyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMob2JqZWN0KTsgaWYgKGVudW1lcmFibGVPbmx5KSBzeW1ib2xzID0gc3ltYm9scy5maWx0ZXIoZnVuY3Rpb24gKHN5bSkgeyByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmplY3QsIHN5bSkuZW51bWVyYWJsZTsgfSk7IGtleXMucHVzaC5hcHBseShrZXlzLCBzeW1ib2xzKTsgfSByZXR1cm4ga2V5czsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0U3ByZWFkKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldICE9IG51bGwgPyBhcmd1bWVudHNbaV0gOiB7fTsgaWYgKGkgJSAyKSB7IG93bktleXMoc291cmNlLCB0cnVlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHsgX2RlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCBzb3VyY2Vba2V5XSk7IH0pOyB9IGVsc2UgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcnMoc291cmNlKSk7IH0gZWxzZSB7IG93bktleXMoc291cmNlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHNvdXJjZSwga2V5KSk7IH0pOyB9IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7IGlmIChrZXkgaW4gb2JqKSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgeyB2YWx1ZTogdmFsdWUsIGVudW1lcmFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSwgd3JpdGFibGU6IHRydWUgfSk7IH0gZWxzZSB7IG9ialtrZXldID0gdmFsdWU7IH0gcmV0dXJuIG9iajsgfVxuXG52YXIgZ2V0T25EZW1hbmRMYXp5U2xpZGVzID0gZnVuY3Rpb24gZ2V0T25EZW1hbmRMYXp5U2xpZGVzKHNwZWMpIHtcbiAgdmFyIG9uRGVtYW5kU2xpZGVzID0gW107XG4gIHZhciBzdGFydEluZGV4ID0gbGF6eVN0YXJ0SW5kZXgoc3BlYyk7XG4gIHZhciBlbmRJbmRleCA9IGxhenlFbmRJbmRleChzcGVjKTtcblxuICBmb3IgKHZhciBzbGlkZUluZGV4ID0gc3RhcnRJbmRleDsgc2xpZGVJbmRleCA8IGVuZEluZGV4OyBzbGlkZUluZGV4KyspIHtcbiAgICBpZiAoc3BlYy5sYXp5TG9hZGVkTGlzdC5pbmRleE9mKHNsaWRlSW5kZXgpIDwgMCkge1xuICAgICAgb25EZW1hbmRTbGlkZXMucHVzaChzbGlkZUluZGV4KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gb25EZW1hbmRTbGlkZXM7XG59OyAvLyByZXR1cm4gbGlzdCBvZiBzbGlkZXMgdGhhdCBuZWVkIHRvIGJlIHByZXNlbnRcblxuXG5leHBvcnRzLmdldE9uRGVtYW5kTGF6eVNsaWRlcyA9IGdldE9uRGVtYW5kTGF6eVNsaWRlcztcblxudmFyIGdldFJlcXVpcmVkTGF6eVNsaWRlcyA9IGZ1bmN0aW9uIGdldFJlcXVpcmVkTGF6eVNsaWRlcyhzcGVjKSB7XG4gIHZhciByZXF1aXJlZFNsaWRlcyA9IFtdO1xuICB2YXIgc3RhcnRJbmRleCA9IGxhenlTdGFydEluZGV4KHNwZWMpO1xuICB2YXIgZW5kSW5kZXggPSBsYXp5RW5kSW5kZXgoc3BlYyk7XG5cbiAgZm9yICh2YXIgc2xpZGVJbmRleCA9IHN0YXJ0SW5kZXg7IHNsaWRlSW5kZXggPCBlbmRJbmRleDsgc2xpZGVJbmRleCsrKSB7XG4gICAgcmVxdWlyZWRTbGlkZXMucHVzaChzbGlkZUluZGV4KTtcbiAgfVxuXG4gIHJldHVybiByZXF1aXJlZFNsaWRlcztcbn07IC8vIHN0YXJ0SW5kZXggdGhhdCBuZWVkcyB0byBiZSBwcmVzZW50XG5cblxuZXhwb3J0cy5nZXRSZXF1aXJlZExhenlTbGlkZXMgPSBnZXRSZXF1aXJlZExhenlTbGlkZXM7XG5cbnZhciBsYXp5U3RhcnRJbmRleCA9IGZ1bmN0aW9uIGxhenlTdGFydEluZGV4KHNwZWMpIHtcbiAgcmV0dXJuIHNwZWMuY3VycmVudFNsaWRlIC0gbGF6eVNsaWRlc09uTGVmdChzcGVjKTtcbn07XG5cbmV4cG9ydHMubGF6eVN0YXJ0SW5kZXggPSBsYXp5U3RhcnRJbmRleDtcblxudmFyIGxhenlFbmRJbmRleCA9IGZ1bmN0aW9uIGxhenlFbmRJbmRleChzcGVjKSB7XG4gIHJldHVybiBzcGVjLmN1cnJlbnRTbGlkZSArIGxhenlTbGlkZXNPblJpZ2h0KHNwZWMpO1xufTtcblxuZXhwb3J0cy5sYXp5RW5kSW5kZXggPSBsYXp5RW5kSW5kZXg7XG5cbnZhciBsYXp5U2xpZGVzT25MZWZ0ID0gZnVuY3Rpb24gbGF6eVNsaWRlc09uTGVmdChzcGVjKSB7XG4gIHJldHVybiBzcGVjLmNlbnRlck1vZGUgPyBNYXRoLmZsb29yKHNwZWMuc2xpZGVzVG9TaG93IC8gMikgKyAocGFyc2VJbnQoc3BlYy5jZW50ZXJQYWRkaW5nKSA+IDAgPyAxIDogMCkgOiAwO1xufTtcblxuZXhwb3J0cy5sYXp5U2xpZGVzT25MZWZ0ID0gbGF6eVNsaWRlc09uTGVmdDtcblxudmFyIGxhenlTbGlkZXNPblJpZ2h0ID0gZnVuY3Rpb24gbGF6eVNsaWRlc09uUmlnaHQoc3BlYykge1xuICByZXR1cm4gc3BlYy5jZW50ZXJNb2RlID8gTWF0aC5mbG9vcigoc3BlYy5zbGlkZXNUb1Nob3cgLSAxKSAvIDIpICsgMSArIChwYXJzZUludChzcGVjLmNlbnRlclBhZGRpbmcpID4gMCA/IDEgOiAwKSA6IHNwZWMuc2xpZGVzVG9TaG93O1xufTsgLy8gZ2V0IHdpZHRoIG9mIGFuIGVsZW1lbnRcblxuXG5leHBvcnRzLmxhenlTbGlkZXNPblJpZ2h0ID0gbGF6eVNsaWRlc09uUmlnaHQ7XG5cbnZhciBnZXRXaWR0aCA9IGZ1bmN0aW9uIGdldFdpZHRoKGVsZW0pIHtcbiAgcmV0dXJuIGVsZW0gJiYgZWxlbS5vZmZzZXRXaWR0aCB8fCAwO1xufTtcblxuZXhwb3J0cy5nZXRXaWR0aCA9IGdldFdpZHRoO1xuXG52YXIgZ2V0SGVpZ2h0ID0gZnVuY3Rpb24gZ2V0SGVpZ2h0KGVsZW0pIHtcbiAgcmV0dXJuIGVsZW0gJiYgZWxlbS5vZmZzZXRIZWlnaHQgfHwgMDtcbn07XG5cbmV4cG9ydHMuZ2V0SGVpZ2h0ID0gZ2V0SGVpZ2h0O1xuXG52YXIgZ2V0U3dpcGVEaXJlY3Rpb24gPSBmdW5jdGlvbiBnZXRTd2lwZURpcmVjdGlvbih0b3VjaE9iamVjdCkge1xuICB2YXIgdmVydGljYWxTd2lwaW5nID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiBmYWxzZTtcbiAgdmFyIHhEaXN0LCB5RGlzdCwgciwgc3dpcGVBbmdsZTtcbiAgeERpc3QgPSB0b3VjaE9iamVjdC5zdGFydFggLSB0b3VjaE9iamVjdC5jdXJYO1xuICB5RGlzdCA9IHRvdWNoT2JqZWN0LnN0YXJ0WSAtIHRvdWNoT2JqZWN0LmN1clk7XG4gIHIgPSBNYXRoLmF0YW4yKHlEaXN0LCB4RGlzdCk7XG4gIHN3aXBlQW5nbGUgPSBNYXRoLnJvdW5kKHIgKiAxODAgLyBNYXRoLlBJKTtcblxuICBpZiAoc3dpcGVBbmdsZSA8IDApIHtcbiAgICBzd2lwZUFuZ2xlID0gMzYwIC0gTWF0aC5hYnMoc3dpcGVBbmdsZSk7XG4gIH1cblxuICBpZiAoc3dpcGVBbmdsZSA8PSA0NSAmJiBzd2lwZUFuZ2xlID49IDAgfHwgc3dpcGVBbmdsZSA8PSAzNjAgJiYgc3dpcGVBbmdsZSA+PSAzMTUpIHtcbiAgICByZXR1cm4gXCJsZWZ0XCI7XG4gIH1cblxuICBpZiAoc3dpcGVBbmdsZSA+PSAxMzUgJiYgc3dpcGVBbmdsZSA8PSAyMjUpIHtcbiAgICByZXR1cm4gXCJyaWdodFwiO1xuICB9XG5cbiAgaWYgKHZlcnRpY2FsU3dpcGluZyA9PT0gdHJ1ZSkge1xuICAgIGlmIChzd2lwZUFuZ2xlID49IDM1ICYmIHN3aXBlQW5nbGUgPD0gMTM1KSB7XG4gICAgICByZXR1cm4gXCJ1cFwiO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gXCJkb3duXCI7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIFwidmVydGljYWxcIjtcbn07IC8vIHdoZXRoZXIgb3Igbm90IHdlIGNhbiBnbyBuZXh0XG5cblxuZXhwb3J0cy5nZXRTd2lwZURpcmVjdGlvbiA9IGdldFN3aXBlRGlyZWN0aW9uO1xuXG52YXIgY2FuR29OZXh0ID0gZnVuY3Rpb24gY2FuR29OZXh0KHNwZWMpIHtcbiAgdmFyIGNhbkdvID0gdHJ1ZTtcblxuICBpZiAoIXNwZWMuaW5maW5pdGUpIHtcbiAgICBpZiAoc3BlYy5jZW50ZXJNb2RlICYmIHNwZWMuY3VycmVudFNsaWRlID49IHNwZWMuc2xpZGVDb3VudCAtIDEpIHtcbiAgICAgIGNhbkdvID0gZmFsc2U7XG4gICAgfSBlbHNlIGlmIChzcGVjLnNsaWRlQ291bnQgPD0gc3BlYy5zbGlkZXNUb1Nob3cgfHwgc3BlYy5jdXJyZW50U2xpZGUgPj0gc3BlYy5zbGlkZUNvdW50IC0gc3BlYy5zbGlkZXNUb1Nob3cpIHtcbiAgICAgIGNhbkdvID0gZmFsc2U7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGNhbkdvO1xufTsgLy8gZ2l2ZW4gYW4gb2JqZWN0IGFuZCBhIGxpc3Qgb2Yga2V5cywgcmV0dXJuIG5ldyBvYmplY3Qgd2l0aCBnaXZlbiBrZXlzXG5cblxuZXhwb3J0cy5jYW5Hb05leHQgPSBjYW5Hb05leHQ7XG5cbnZhciBleHRyYWN0T2JqZWN0ID0gZnVuY3Rpb24gZXh0cmFjdE9iamVjdChzcGVjLCBrZXlzKSB7XG4gIHZhciBuZXdPYmplY3QgPSB7fTtcbiAga2V5cy5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICByZXR1cm4gbmV3T2JqZWN0W2tleV0gPSBzcGVjW2tleV07XG4gIH0pO1xuICByZXR1cm4gbmV3T2JqZWN0O1xufTsgLy8gZ2V0IGluaXRpYWxpemVkIHN0YXRlXG5cblxuZXhwb3J0cy5leHRyYWN0T2JqZWN0ID0gZXh0cmFjdE9iamVjdDtcblxudmFyIGluaXRpYWxpemVkU3RhdGUgPSBmdW5jdGlvbiBpbml0aWFsaXplZFN0YXRlKHNwZWMpIHtcbiAgLy8gc3BlYyBhbHNvIGNvbnRhaW5zIGxpc3RSZWYsIHRyYWNrUmVmXG4gIHZhciBzbGlkZUNvdW50ID0gX3JlYWN0W1wiZGVmYXVsdFwiXS5DaGlsZHJlbi5jb3VudChzcGVjLmNoaWxkcmVuKTtcblxuICB2YXIgbGlzdFdpZHRoID0gTWF0aC5jZWlsKGdldFdpZHRoKF9yZWFjdERvbVtcImRlZmF1bHRcIl0uZmluZERPTU5vZGUoc3BlYy5saXN0UmVmKSkpO1xuICB2YXIgdHJhY2tXaWR0aCA9IE1hdGguY2VpbChnZXRXaWR0aChfcmVhY3REb21bXCJkZWZhdWx0XCJdLmZpbmRET01Ob2RlKHNwZWMudHJhY2tSZWYpKSk7XG4gIHZhciBzbGlkZVdpZHRoO1xuXG4gIGlmICghc3BlYy52ZXJ0aWNhbCkge1xuICAgIHZhciBjZW50ZXJQYWRkaW5nQWRqID0gc3BlYy5jZW50ZXJNb2RlICYmIHBhcnNlSW50KHNwZWMuY2VudGVyUGFkZGluZykgKiAyO1xuXG4gICAgaWYgKHR5cGVvZiBzcGVjLmNlbnRlclBhZGRpbmcgPT09IFwic3RyaW5nXCIgJiYgc3BlYy5jZW50ZXJQYWRkaW5nLnNsaWNlKC0xKSA9PT0gXCIlXCIpIHtcbiAgICAgIGNlbnRlclBhZGRpbmdBZGogKj0gbGlzdFdpZHRoIC8gMTAwO1xuICAgIH1cblxuICAgIHNsaWRlV2lkdGggPSBNYXRoLmNlaWwoKGxpc3RXaWR0aCAtIGNlbnRlclBhZGRpbmdBZGopIC8gc3BlYy5zbGlkZXNUb1Nob3cpO1xuICB9IGVsc2Uge1xuICAgIHNsaWRlV2lkdGggPSBsaXN0V2lkdGg7XG4gIH1cblxuICB2YXIgc2xpZGVIZWlnaHQgPSBfcmVhY3REb21bXCJkZWZhdWx0XCJdLmZpbmRET01Ob2RlKHNwZWMubGlzdFJlZikgJiYgZ2V0SGVpZ2h0KF9yZWFjdERvbVtcImRlZmF1bHRcIl0uZmluZERPTU5vZGUoc3BlYy5saXN0UmVmKS5xdWVyeVNlbGVjdG9yKCdbZGF0YS1pbmRleD1cIjBcIl0nKSk7XG4gIHZhciBsaXN0SGVpZ2h0ID0gc2xpZGVIZWlnaHQgKiBzcGVjLnNsaWRlc1RvU2hvdztcbiAgdmFyIGN1cnJlbnRTbGlkZSA9IHNwZWMuY3VycmVudFNsaWRlID09PSB1bmRlZmluZWQgPyBzcGVjLmluaXRpYWxTbGlkZSA6IHNwZWMuY3VycmVudFNsaWRlO1xuXG4gIGlmIChzcGVjLnJ0bCAmJiBzcGVjLmN1cnJlbnRTbGlkZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgY3VycmVudFNsaWRlID0gc2xpZGVDb3VudCAtIDEgLSBzcGVjLmluaXRpYWxTbGlkZTtcbiAgfVxuXG4gIHZhciBsYXp5TG9hZGVkTGlzdCA9IHNwZWMubGF6eUxvYWRlZExpc3QgfHwgW107XG4gIHZhciBzbGlkZXNUb0xvYWQgPSBnZXRPbkRlbWFuZExhenlTbGlkZXMoe1xuICAgIGN1cnJlbnRTbGlkZTogY3VycmVudFNsaWRlLFxuICAgIGxhenlMb2FkZWRMaXN0OiBsYXp5TG9hZGVkTGlzdFxuICB9LCBzcGVjKTtcbiAgbGF6eUxvYWRlZExpc3QuY29uY2F0KHNsaWRlc1RvTG9hZCk7XG4gIHZhciBzdGF0ZSA9IHtcbiAgICBzbGlkZUNvdW50OiBzbGlkZUNvdW50LFxuICAgIHNsaWRlV2lkdGg6IHNsaWRlV2lkdGgsXG4gICAgbGlzdFdpZHRoOiBsaXN0V2lkdGgsXG4gICAgdHJhY2tXaWR0aDogdHJhY2tXaWR0aCxcbiAgICBjdXJyZW50U2xpZGU6IGN1cnJlbnRTbGlkZSxcbiAgICBzbGlkZUhlaWdodDogc2xpZGVIZWlnaHQsXG4gICAgbGlzdEhlaWdodDogbGlzdEhlaWdodCxcbiAgICBsYXp5TG9hZGVkTGlzdDogbGF6eUxvYWRlZExpc3RcbiAgfTtcblxuICBpZiAoc3BlYy5hdXRvcGxheWluZyA9PT0gbnVsbCAmJiBzcGVjLmF1dG9wbGF5KSB7XG4gICAgc3RhdGVbXCJhdXRvcGxheWluZ1wiXSA9IFwicGxheWluZ1wiO1xuICB9XG5cbiAgcmV0dXJuIHN0YXRlO1xufTtcblxuZXhwb3J0cy5pbml0aWFsaXplZFN0YXRlID0gaW5pdGlhbGl6ZWRTdGF0ZTtcblxudmFyIHNsaWRlSGFuZGxlciA9IGZ1bmN0aW9uIHNsaWRlSGFuZGxlcihzcGVjKSB7XG4gIHZhciB3YWl0Rm9yQW5pbWF0ZSA9IHNwZWMud2FpdEZvckFuaW1hdGUsXG4gICAgICBhbmltYXRpbmcgPSBzcGVjLmFuaW1hdGluZyxcbiAgICAgIGZhZGUgPSBzcGVjLmZhZGUsXG4gICAgICBpbmZpbml0ZSA9IHNwZWMuaW5maW5pdGUsXG4gICAgICBpbmRleCA9IHNwZWMuaW5kZXgsXG4gICAgICBzbGlkZUNvdW50ID0gc3BlYy5zbGlkZUNvdW50LFxuICAgICAgbGF6eUxvYWRlZExpc3QgPSBzcGVjLmxhenlMb2FkZWRMaXN0LFxuICAgICAgbGF6eUxvYWQgPSBzcGVjLmxhenlMb2FkLFxuICAgICAgY3VycmVudFNsaWRlID0gc3BlYy5jdXJyZW50U2xpZGUsXG4gICAgICBjZW50ZXJNb2RlID0gc3BlYy5jZW50ZXJNb2RlLFxuICAgICAgc2xpZGVzVG9TY3JvbGwgPSBzcGVjLnNsaWRlc1RvU2Nyb2xsLFxuICAgICAgc2xpZGVzVG9TaG93ID0gc3BlYy5zbGlkZXNUb1Nob3csXG4gICAgICB1c2VDU1MgPSBzcGVjLnVzZUNTUztcbiAgaWYgKHdhaXRGb3JBbmltYXRlICYmIGFuaW1hdGluZykgcmV0dXJuIHt9O1xuICB2YXIgYW5pbWF0aW9uU2xpZGUgPSBpbmRleCxcbiAgICAgIGZpbmFsU2xpZGUsXG4gICAgICBhbmltYXRpb25MZWZ0LFxuICAgICAgZmluYWxMZWZ0O1xuICB2YXIgc3RhdGUgPSB7fSxcbiAgICAgIG5leHRTdGF0ZSA9IHt9O1xuXG4gIGlmIChmYWRlKSB7XG4gICAgaWYgKCFpbmZpbml0ZSAmJiAoaW5kZXggPCAwIHx8IGluZGV4ID49IHNsaWRlQ291bnQpKSByZXR1cm4ge307XG5cbiAgICBpZiAoaW5kZXggPCAwKSB7XG4gICAgICBhbmltYXRpb25TbGlkZSA9IGluZGV4ICsgc2xpZGVDb3VudDtcbiAgICB9IGVsc2UgaWYgKGluZGV4ID49IHNsaWRlQ291bnQpIHtcbiAgICAgIGFuaW1hdGlvblNsaWRlID0gaW5kZXggLSBzbGlkZUNvdW50O1xuICAgIH1cblxuICAgIGlmIChsYXp5TG9hZCAmJiBsYXp5TG9hZGVkTGlzdC5pbmRleE9mKGFuaW1hdGlvblNsaWRlKSA8IDApIHtcbiAgICAgIGxhenlMb2FkZWRMaXN0LnB1c2goYW5pbWF0aW9uU2xpZGUpO1xuICAgIH1cblxuICAgIHN0YXRlID0ge1xuICAgICAgYW5pbWF0aW5nOiB0cnVlLFxuICAgICAgY3VycmVudFNsaWRlOiBhbmltYXRpb25TbGlkZSxcbiAgICAgIGxhenlMb2FkZWRMaXN0OiBsYXp5TG9hZGVkTGlzdFxuICAgIH07XG4gICAgbmV4dFN0YXRlID0ge1xuICAgICAgYW5pbWF0aW5nOiBmYWxzZVxuICAgIH07XG4gIH0gZWxzZSB7XG4gICAgZmluYWxTbGlkZSA9IGFuaW1hdGlvblNsaWRlO1xuXG4gICAgaWYgKGFuaW1hdGlvblNsaWRlIDwgMCkge1xuICAgICAgZmluYWxTbGlkZSA9IGFuaW1hdGlvblNsaWRlICsgc2xpZGVDb3VudDtcbiAgICAgIGlmICghaW5maW5pdGUpIGZpbmFsU2xpZGUgPSAwO2Vsc2UgaWYgKHNsaWRlQ291bnQgJSBzbGlkZXNUb1Njcm9sbCAhPT0gMCkgZmluYWxTbGlkZSA9IHNsaWRlQ291bnQgLSBzbGlkZUNvdW50ICUgc2xpZGVzVG9TY3JvbGw7XG4gICAgfSBlbHNlIGlmICghY2FuR29OZXh0KHNwZWMpICYmIGFuaW1hdGlvblNsaWRlID4gY3VycmVudFNsaWRlKSB7XG4gICAgICBhbmltYXRpb25TbGlkZSA9IGZpbmFsU2xpZGUgPSBjdXJyZW50U2xpZGU7XG4gICAgfSBlbHNlIGlmIChjZW50ZXJNb2RlICYmIGFuaW1hdGlvblNsaWRlID49IHNsaWRlQ291bnQpIHtcbiAgICAgIGFuaW1hdGlvblNsaWRlID0gaW5maW5pdGUgPyBzbGlkZUNvdW50IDogc2xpZGVDb3VudCAtIDE7XG4gICAgICBmaW5hbFNsaWRlID0gaW5maW5pdGUgPyAwIDogc2xpZGVDb3VudCAtIDE7XG4gICAgfSBlbHNlIGlmIChhbmltYXRpb25TbGlkZSA+PSBzbGlkZUNvdW50KSB7XG4gICAgICBmaW5hbFNsaWRlID0gYW5pbWF0aW9uU2xpZGUgLSBzbGlkZUNvdW50O1xuICAgICAgaWYgKCFpbmZpbml0ZSkgZmluYWxTbGlkZSA9IHNsaWRlQ291bnQgLSBzbGlkZXNUb1Nob3c7ZWxzZSBpZiAoc2xpZGVDb3VudCAlIHNsaWRlc1RvU2Nyb2xsICE9PSAwKSBmaW5hbFNsaWRlID0gMDtcbiAgICB9XG5cbiAgICBhbmltYXRpb25MZWZ0ID0gZ2V0VHJhY2tMZWZ0KF9vYmplY3RTcHJlYWQoe30sIHNwZWMsIHtcbiAgICAgIHNsaWRlSW5kZXg6IGFuaW1hdGlvblNsaWRlXG4gICAgfSkpO1xuICAgIGZpbmFsTGVmdCA9IGdldFRyYWNrTGVmdChfb2JqZWN0U3ByZWFkKHt9LCBzcGVjLCB7XG4gICAgICBzbGlkZUluZGV4OiBmaW5hbFNsaWRlXG4gICAgfSkpO1xuXG4gICAgaWYgKCFpbmZpbml0ZSkge1xuICAgICAgaWYgKGFuaW1hdGlvbkxlZnQgPT09IGZpbmFsTGVmdCkgYW5pbWF0aW9uU2xpZGUgPSBmaW5hbFNsaWRlO1xuICAgICAgYW5pbWF0aW9uTGVmdCA9IGZpbmFsTGVmdDtcbiAgICB9XG5cbiAgICBsYXp5TG9hZCAmJiBsYXp5TG9hZGVkTGlzdC5jb25jYXQoZ2V0T25EZW1hbmRMYXp5U2xpZGVzKF9vYmplY3RTcHJlYWQoe30sIHNwZWMsIHtcbiAgICAgIGN1cnJlbnRTbGlkZTogYW5pbWF0aW9uU2xpZGVcbiAgICB9KSkpO1xuXG4gICAgaWYgKCF1c2VDU1MpIHtcbiAgICAgIHN0YXRlID0ge1xuICAgICAgICBjdXJyZW50U2xpZGU6IGZpbmFsU2xpZGUsXG4gICAgICAgIHRyYWNrU3R5bGU6IGdldFRyYWNrQ1NTKF9vYmplY3RTcHJlYWQoe30sIHNwZWMsIHtcbiAgICAgICAgICBsZWZ0OiBmaW5hbExlZnRcbiAgICAgICAgfSkpLFxuICAgICAgICBsYXp5TG9hZGVkTGlzdDogbGF6eUxvYWRlZExpc3RcbiAgICAgIH07XG4gICAgfSBlbHNlIHtcbiAgICAgIHN0YXRlID0ge1xuICAgICAgICBhbmltYXRpbmc6IHRydWUsXG4gICAgICAgIGN1cnJlbnRTbGlkZTogZmluYWxTbGlkZSxcbiAgICAgICAgdHJhY2tTdHlsZTogZ2V0VHJhY2tBbmltYXRlQ1NTKF9vYmplY3RTcHJlYWQoe30sIHNwZWMsIHtcbiAgICAgICAgICBsZWZ0OiBhbmltYXRpb25MZWZ0XG4gICAgICAgIH0pKSxcbiAgICAgICAgbGF6eUxvYWRlZExpc3Q6IGxhenlMb2FkZWRMaXN0XG4gICAgICB9O1xuICAgICAgbmV4dFN0YXRlID0ge1xuICAgICAgICBhbmltYXRpbmc6IGZhbHNlLFxuICAgICAgICBjdXJyZW50U2xpZGU6IGZpbmFsU2xpZGUsXG4gICAgICAgIHRyYWNrU3R5bGU6IGdldFRyYWNrQ1NTKF9vYmplY3RTcHJlYWQoe30sIHNwZWMsIHtcbiAgICAgICAgICBsZWZ0OiBmaW5hbExlZnRcbiAgICAgICAgfSkpLFxuICAgICAgICBzd2lwZUxlZnQ6IG51bGxcbiAgICAgIH07XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBzdGF0ZTogc3RhdGUsXG4gICAgbmV4dFN0YXRlOiBuZXh0U3RhdGVcbiAgfTtcbn07XG5cbmV4cG9ydHMuc2xpZGVIYW5kbGVyID0gc2xpZGVIYW5kbGVyO1xuXG52YXIgY2hhbmdlU2xpZGUgPSBmdW5jdGlvbiBjaGFuZ2VTbGlkZShzcGVjLCBvcHRpb25zKSB7XG4gIHZhciBpbmRleE9mZnNldCwgcHJldmlvdXNJbnQsIHNsaWRlT2Zmc2V0LCB1bmV2ZW5PZmZzZXQsIHRhcmdldFNsaWRlO1xuICB2YXIgc2xpZGVzVG9TY3JvbGwgPSBzcGVjLnNsaWRlc1RvU2Nyb2xsLFxuICAgICAgc2xpZGVzVG9TaG93ID0gc3BlYy5zbGlkZXNUb1Nob3csXG4gICAgICBzbGlkZUNvdW50ID0gc3BlYy5zbGlkZUNvdW50LFxuICAgICAgY3VycmVudFNsaWRlID0gc3BlYy5jdXJyZW50U2xpZGUsXG4gICAgICBsYXp5TG9hZCA9IHNwZWMubGF6eUxvYWQsXG4gICAgICBpbmZpbml0ZSA9IHNwZWMuaW5maW5pdGU7XG4gIHVuZXZlbk9mZnNldCA9IHNsaWRlQ291bnQgJSBzbGlkZXNUb1Njcm9sbCAhPT0gMDtcbiAgaW5kZXhPZmZzZXQgPSB1bmV2ZW5PZmZzZXQgPyAwIDogKHNsaWRlQ291bnQgLSBjdXJyZW50U2xpZGUpICUgc2xpZGVzVG9TY3JvbGw7XG5cbiAgaWYgKG9wdGlvbnMubWVzc2FnZSA9PT0gXCJwcmV2aW91c1wiKSB7XG4gICAgc2xpZGVPZmZzZXQgPSBpbmRleE9mZnNldCA9PT0gMCA/IHNsaWRlc1RvU2Nyb2xsIDogc2xpZGVzVG9TaG93IC0gaW5kZXhPZmZzZXQ7XG4gICAgdGFyZ2V0U2xpZGUgPSBjdXJyZW50U2xpZGUgLSBzbGlkZU9mZnNldDtcblxuICAgIGlmIChsYXp5TG9hZCAmJiAhaW5maW5pdGUpIHtcbiAgICAgIHByZXZpb3VzSW50ID0gY3VycmVudFNsaWRlIC0gc2xpZGVPZmZzZXQ7XG4gICAgICB0YXJnZXRTbGlkZSA9IHByZXZpb3VzSW50ID09PSAtMSA/IHNsaWRlQ291bnQgLSAxIDogcHJldmlvdXNJbnQ7XG4gICAgfVxuICB9IGVsc2UgaWYgKG9wdGlvbnMubWVzc2FnZSA9PT0gXCJuZXh0XCIpIHtcbiAgICBzbGlkZU9mZnNldCA9IGluZGV4T2Zmc2V0ID09PSAwID8gc2xpZGVzVG9TY3JvbGwgOiBpbmRleE9mZnNldDtcbiAgICB0YXJnZXRTbGlkZSA9IGN1cnJlbnRTbGlkZSArIHNsaWRlT2Zmc2V0O1xuXG4gICAgaWYgKGxhenlMb2FkICYmICFpbmZpbml0ZSkge1xuICAgICAgdGFyZ2V0U2xpZGUgPSAoY3VycmVudFNsaWRlICsgc2xpZGVzVG9TY3JvbGwpICUgc2xpZGVDb3VudCArIGluZGV4T2Zmc2V0O1xuICAgIH1cbiAgfSBlbHNlIGlmIChvcHRpb25zLm1lc3NhZ2UgPT09IFwiZG90c1wiKSB7XG4gICAgLy8gQ2xpY2sgb24gZG90c1xuICAgIHRhcmdldFNsaWRlID0gb3B0aW9ucy5pbmRleCAqIG9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XG5cbiAgICBpZiAodGFyZ2V0U2xpZGUgPT09IG9wdGlvbnMuY3VycmVudFNsaWRlKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH0gZWxzZSBpZiAob3B0aW9ucy5tZXNzYWdlID09PSBcImNoaWxkcmVuXCIpIHtcbiAgICAvLyBDbGljayBvbiB0aGUgc2xpZGVzXG4gICAgdGFyZ2V0U2xpZGUgPSBvcHRpb25zLmluZGV4O1xuXG4gICAgaWYgKHRhcmdldFNsaWRlID09PSBvcHRpb25zLmN1cnJlbnRTbGlkZSkge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgaWYgKGluZmluaXRlKSB7XG4gICAgICB2YXIgZGlyZWN0aW9uID0gc2libGluZ0RpcmVjdGlvbihfb2JqZWN0U3ByZWFkKHt9LCBzcGVjLCB7XG4gICAgICAgIHRhcmdldFNsaWRlOiB0YXJnZXRTbGlkZVxuICAgICAgfSkpO1xuXG4gICAgICBpZiAodGFyZ2V0U2xpZGUgPiBvcHRpb25zLmN1cnJlbnRTbGlkZSAmJiBkaXJlY3Rpb24gPT09IFwibGVmdFwiKSB7XG4gICAgICAgIHRhcmdldFNsaWRlID0gdGFyZ2V0U2xpZGUgLSBzbGlkZUNvdW50O1xuICAgICAgfSBlbHNlIGlmICh0YXJnZXRTbGlkZSA8IG9wdGlvbnMuY3VycmVudFNsaWRlICYmIGRpcmVjdGlvbiA9PT0gXCJyaWdodFwiKSB7XG4gICAgICAgIHRhcmdldFNsaWRlID0gdGFyZ2V0U2xpZGUgKyBzbGlkZUNvdW50O1xuICAgICAgfVxuICAgIH1cbiAgfSBlbHNlIGlmIChvcHRpb25zLm1lc3NhZ2UgPT09IFwiaW5kZXhcIikge1xuICAgIHRhcmdldFNsaWRlID0gTnVtYmVyKG9wdGlvbnMuaW5kZXgpO1xuXG4gICAgaWYgKHRhcmdldFNsaWRlID09PSBvcHRpb25zLmN1cnJlbnRTbGlkZSkge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRhcmdldFNsaWRlO1xufTtcblxuZXhwb3J0cy5jaGFuZ2VTbGlkZSA9IGNoYW5nZVNsaWRlO1xuXG52YXIga2V5SGFuZGxlciA9IGZ1bmN0aW9uIGtleUhhbmRsZXIoZSwgYWNjZXNzaWJpbGl0eSwgcnRsKSB7XG4gIGlmIChlLnRhcmdldC50YWdOYW1lLm1hdGNoKFwiVEVYVEFSRUF8SU5QVVR8U0VMRUNUXCIpIHx8ICFhY2Nlc3NpYmlsaXR5KSByZXR1cm4gXCJcIjtcbiAgaWYgKGUua2V5Q29kZSA9PT0gMzcpIHJldHVybiBydGwgPyBcIm5leHRcIiA6IFwicHJldmlvdXNcIjtcbiAgaWYgKGUua2V5Q29kZSA9PT0gMzkpIHJldHVybiBydGwgPyBcInByZXZpb3VzXCIgOiBcIm5leHRcIjtcbiAgcmV0dXJuIFwiXCI7XG59O1xuXG5leHBvcnRzLmtleUhhbmRsZXIgPSBrZXlIYW5kbGVyO1xuXG52YXIgc3dpcGVTdGFydCA9IGZ1bmN0aW9uIHN3aXBlU3RhcnQoZSwgc3dpcGUsIGRyYWdnYWJsZSkge1xuICBlLnRhcmdldC50YWdOYW1lID09PSBcIklNR1wiICYmIGUucHJldmVudERlZmF1bHQoKTtcbiAgaWYgKCFzd2lwZSB8fCAhZHJhZ2dhYmxlICYmIGUudHlwZS5pbmRleE9mKFwibW91c2VcIikgIT09IC0xKSByZXR1cm4gXCJcIjtcbiAgcmV0dXJuIHtcbiAgICBkcmFnZ2luZzogdHJ1ZSxcbiAgICB0b3VjaE9iamVjdDoge1xuICAgICAgc3RhcnRYOiBlLnRvdWNoZXMgPyBlLnRvdWNoZXNbMF0ucGFnZVggOiBlLmNsaWVudFgsXG4gICAgICBzdGFydFk6IGUudG91Y2hlcyA/IGUudG91Y2hlc1swXS5wYWdlWSA6IGUuY2xpZW50WSxcbiAgICAgIGN1clg6IGUudG91Y2hlcyA/IGUudG91Y2hlc1swXS5wYWdlWCA6IGUuY2xpZW50WCxcbiAgICAgIGN1clk6IGUudG91Y2hlcyA/IGUudG91Y2hlc1swXS5wYWdlWSA6IGUuY2xpZW50WVxuICAgIH1cbiAgfTtcbn07XG5cbmV4cG9ydHMuc3dpcGVTdGFydCA9IHN3aXBlU3RhcnQ7XG5cbnZhciBzd2lwZU1vdmUgPSBmdW5jdGlvbiBzd2lwZU1vdmUoZSwgc3BlYykge1xuICAvLyBzcGVjIGFsc28gY29udGFpbnMsIHRyYWNrUmVmIGFuZCBzbGlkZUluZGV4XG4gIHZhciBzY3JvbGxpbmcgPSBzcGVjLnNjcm9sbGluZyxcbiAgICAgIGFuaW1hdGluZyA9IHNwZWMuYW5pbWF0aW5nLFxuICAgICAgdmVydGljYWwgPSBzcGVjLnZlcnRpY2FsLFxuICAgICAgc3dpcGVUb1NsaWRlID0gc3BlYy5zd2lwZVRvU2xpZGUsXG4gICAgICB2ZXJ0aWNhbFN3aXBpbmcgPSBzcGVjLnZlcnRpY2FsU3dpcGluZyxcbiAgICAgIHJ0bCA9IHNwZWMucnRsLFxuICAgICAgY3VycmVudFNsaWRlID0gc3BlYy5jdXJyZW50U2xpZGUsXG4gICAgICBlZGdlRnJpY3Rpb24gPSBzcGVjLmVkZ2VGcmljdGlvbixcbiAgICAgIGVkZ2VEcmFnZ2VkID0gc3BlYy5lZGdlRHJhZ2dlZCxcbiAgICAgIG9uRWRnZSA9IHNwZWMub25FZGdlLFxuICAgICAgc3dpcGVkID0gc3BlYy5zd2lwZWQsXG4gICAgICBzd2lwaW5nID0gc3BlYy5zd2lwaW5nLFxuICAgICAgc2xpZGVDb3VudCA9IHNwZWMuc2xpZGVDb3VudCxcbiAgICAgIHNsaWRlc1RvU2Nyb2xsID0gc3BlYy5zbGlkZXNUb1Njcm9sbCxcbiAgICAgIGluZmluaXRlID0gc3BlYy5pbmZpbml0ZSxcbiAgICAgIHRvdWNoT2JqZWN0ID0gc3BlYy50b3VjaE9iamVjdCxcbiAgICAgIHN3aXBlRXZlbnQgPSBzcGVjLnN3aXBlRXZlbnQsXG4gICAgICBsaXN0SGVpZ2h0ID0gc3BlYy5saXN0SGVpZ2h0LFxuICAgICAgbGlzdFdpZHRoID0gc3BlYy5saXN0V2lkdGg7XG4gIGlmIChzY3JvbGxpbmcpIHJldHVybjtcbiAgaWYgKGFuaW1hdGluZykgcmV0dXJuIGUucHJldmVudERlZmF1bHQoKTtcbiAgaWYgKHZlcnRpY2FsICYmIHN3aXBlVG9TbGlkZSAmJiB2ZXJ0aWNhbFN3aXBpbmcpIGUucHJldmVudERlZmF1bHQoKTtcbiAgdmFyIHN3aXBlTGVmdCxcbiAgICAgIHN0YXRlID0ge307XG4gIHZhciBjdXJMZWZ0ID0gZ2V0VHJhY2tMZWZ0KHNwZWMpO1xuICB0b3VjaE9iamVjdC5jdXJYID0gZS50b3VjaGVzID8gZS50b3VjaGVzWzBdLnBhZ2VYIDogZS5jbGllbnRYO1xuICB0b3VjaE9iamVjdC5jdXJZID0gZS50b3VjaGVzID8gZS50b3VjaGVzWzBdLnBhZ2VZIDogZS5jbGllbnRZO1xuICB0b3VjaE9iamVjdC5zd2lwZUxlbmd0aCA9IE1hdGgucm91bmQoTWF0aC5zcXJ0KE1hdGgucG93KHRvdWNoT2JqZWN0LmN1clggLSB0b3VjaE9iamVjdC5zdGFydFgsIDIpKSk7XG4gIHZhciB2ZXJ0aWNhbFN3aXBlTGVuZ3RoID0gTWF0aC5yb3VuZChNYXRoLnNxcnQoTWF0aC5wb3codG91Y2hPYmplY3QuY3VyWSAtIHRvdWNoT2JqZWN0LnN0YXJ0WSwgMikpKTtcblxuICBpZiAoIXZlcnRpY2FsU3dpcGluZyAmJiAhc3dpcGluZyAmJiB2ZXJ0aWNhbFN3aXBlTGVuZ3RoID4gMTApIHtcbiAgICByZXR1cm4ge1xuICAgICAgc2Nyb2xsaW5nOiB0cnVlXG4gICAgfTtcbiAgfVxuXG4gIGlmICh2ZXJ0aWNhbFN3aXBpbmcpIHRvdWNoT2JqZWN0LnN3aXBlTGVuZ3RoID0gdmVydGljYWxTd2lwZUxlbmd0aDtcbiAgdmFyIHBvc2l0aW9uT2Zmc2V0ID0gKCFydGwgPyAxIDogLTEpICogKHRvdWNoT2JqZWN0LmN1clggPiB0b3VjaE9iamVjdC5zdGFydFggPyAxIDogLTEpO1xuICBpZiAodmVydGljYWxTd2lwaW5nKSBwb3NpdGlvbk9mZnNldCA9IHRvdWNoT2JqZWN0LmN1clkgPiB0b3VjaE9iamVjdC5zdGFydFkgPyAxIDogLTE7XG4gIHZhciBkb3RDb3VudCA9IE1hdGguY2VpbChzbGlkZUNvdW50IC8gc2xpZGVzVG9TY3JvbGwpO1xuICB2YXIgc3dpcGVEaXJlY3Rpb24gPSBnZXRTd2lwZURpcmVjdGlvbihzcGVjLnRvdWNoT2JqZWN0LCB2ZXJ0aWNhbFN3aXBpbmcpO1xuICB2YXIgdG91Y2hTd2lwZUxlbmd0aCA9IHRvdWNoT2JqZWN0LnN3aXBlTGVuZ3RoO1xuXG4gIGlmICghaW5maW5pdGUpIHtcbiAgICBpZiAoY3VycmVudFNsaWRlID09PSAwICYmIHN3aXBlRGlyZWN0aW9uID09PSBcInJpZ2h0XCIgfHwgY3VycmVudFNsaWRlICsgMSA+PSBkb3RDb3VudCAmJiBzd2lwZURpcmVjdGlvbiA9PT0gXCJsZWZ0XCIgfHwgIWNhbkdvTmV4dChzcGVjKSAmJiBzd2lwZURpcmVjdGlvbiA9PT0gXCJsZWZ0XCIpIHtcbiAgICAgIHRvdWNoU3dpcGVMZW5ndGggPSB0b3VjaE9iamVjdC5zd2lwZUxlbmd0aCAqIGVkZ2VGcmljdGlvbjtcblxuICAgICAgaWYgKGVkZ2VEcmFnZ2VkID09PSBmYWxzZSAmJiBvbkVkZ2UpIHtcbiAgICAgICAgb25FZGdlKHN3aXBlRGlyZWN0aW9uKTtcbiAgICAgICAgc3RhdGVbXCJlZGdlRHJhZ2dlZFwiXSA9IHRydWU7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaWYgKCFzd2lwZWQgJiYgc3dpcGVFdmVudCkge1xuICAgIHN3aXBlRXZlbnQoc3dpcGVEaXJlY3Rpb24pO1xuICAgIHN0YXRlW1wic3dpcGVkXCJdID0gdHJ1ZTtcbiAgfVxuXG4gIGlmICghdmVydGljYWwpIHtcbiAgICBpZiAoIXJ0bCkge1xuICAgICAgc3dpcGVMZWZ0ID0gY3VyTGVmdCArIHRvdWNoU3dpcGVMZW5ndGggKiBwb3NpdGlvbk9mZnNldDtcbiAgICB9IGVsc2Uge1xuICAgICAgc3dpcGVMZWZ0ID0gY3VyTGVmdCAtIHRvdWNoU3dpcGVMZW5ndGggKiBwb3NpdGlvbk9mZnNldDtcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgc3dpcGVMZWZ0ID0gY3VyTGVmdCArIHRvdWNoU3dpcGVMZW5ndGggKiAobGlzdEhlaWdodCAvIGxpc3RXaWR0aCkgKiBwb3NpdGlvbk9mZnNldDtcbiAgfVxuXG4gIGlmICh2ZXJ0aWNhbFN3aXBpbmcpIHtcbiAgICBzd2lwZUxlZnQgPSBjdXJMZWZ0ICsgdG91Y2hTd2lwZUxlbmd0aCAqIHBvc2l0aW9uT2Zmc2V0O1xuICB9XG5cbiAgc3RhdGUgPSBfb2JqZWN0U3ByZWFkKHt9LCBzdGF0ZSwge1xuICAgIHRvdWNoT2JqZWN0OiB0b3VjaE9iamVjdCxcbiAgICBzd2lwZUxlZnQ6IHN3aXBlTGVmdCxcbiAgICB0cmFja1N0eWxlOiBnZXRUcmFja0NTUyhfb2JqZWN0U3ByZWFkKHt9LCBzcGVjLCB7XG4gICAgICBsZWZ0OiBzd2lwZUxlZnRcbiAgICB9KSlcbiAgfSk7XG5cbiAgaWYgKE1hdGguYWJzKHRvdWNoT2JqZWN0LmN1clggLSB0b3VjaE9iamVjdC5zdGFydFgpIDwgTWF0aC5hYnModG91Y2hPYmplY3QuY3VyWSAtIHRvdWNoT2JqZWN0LnN0YXJ0WSkgKiAwLjgpIHtcbiAgICByZXR1cm4gc3RhdGU7XG4gIH1cblxuICBpZiAodG91Y2hPYmplY3Quc3dpcGVMZW5ndGggPiAxMCkge1xuICAgIHN0YXRlW1wic3dpcGluZ1wiXSA9IHRydWU7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICB9XG5cbiAgcmV0dXJuIHN0YXRlO1xufTtcblxuZXhwb3J0cy5zd2lwZU1vdmUgPSBzd2lwZU1vdmU7XG5cbnZhciBzd2lwZUVuZCA9IGZ1bmN0aW9uIHN3aXBlRW5kKGUsIHNwZWMpIHtcbiAgdmFyIGRyYWdnaW5nID0gc3BlYy5kcmFnZ2luZyxcbiAgICAgIHN3aXBlID0gc3BlYy5zd2lwZSxcbiAgICAgIHRvdWNoT2JqZWN0ID0gc3BlYy50b3VjaE9iamVjdCxcbiAgICAgIGxpc3RXaWR0aCA9IHNwZWMubGlzdFdpZHRoLFxuICAgICAgdG91Y2hUaHJlc2hvbGQgPSBzcGVjLnRvdWNoVGhyZXNob2xkLFxuICAgICAgdmVydGljYWxTd2lwaW5nID0gc3BlYy52ZXJ0aWNhbFN3aXBpbmcsXG4gICAgICBsaXN0SGVpZ2h0ID0gc3BlYy5saXN0SGVpZ2h0LFxuICAgICAgY3VycmVudFNsaWRlID0gc3BlYy5jdXJyZW50U2xpZGUsXG4gICAgICBzd2lwZVRvU2xpZGUgPSBzcGVjLnN3aXBlVG9TbGlkZSxcbiAgICAgIHNjcm9sbGluZyA9IHNwZWMuc2Nyb2xsaW5nLFxuICAgICAgb25Td2lwZSA9IHNwZWMub25Td2lwZTtcblxuICBpZiAoIWRyYWdnaW5nKSB7XG4gICAgaWYgKHN3aXBlKSBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgcmV0dXJuIHt9O1xuICB9XG5cbiAgdmFyIG1pblN3aXBlID0gdmVydGljYWxTd2lwaW5nID8gbGlzdEhlaWdodCAvIHRvdWNoVGhyZXNob2xkIDogbGlzdFdpZHRoIC8gdG91Y2hUaHJlc2hvbGQ7XG4gIHZhciBzd2lwZURpcmVjdGlvbiA9IGdldFN3aXBlRGlyZWN0aW9uKHRvdWNoT2JqZWN0LCB2ZXJ0aWNhbFN3aXBpbmcpOyAvLyByZXNldCB0aGUgc3RhdGUgb2YgdG91Y2ggcmVsYXRlZCBzdGF0ZSB2YXJpYWJsZXMuXG5cbiAgdmFyIHN0YXRlID0ge1xuICAgIGRyYWdnaW5nOiBmYWxzZSxcbiAgICBlZGdlRHJhZ2dlZDogZmFsc2UsXG4gICAgc2Nyb2xsaW5nOiBmYWxzZSxcbiAgICBzd2lwaW5nOiBmYWxzZSxcbiAgICBzd2lwZWQ6IGZhbHNlLFxuICAgIHN3aXBlTGVmdDogbnVsbCxcbiAgICB0b3VjaE9iamVjdDoge31cbiAgfTtcblxuICBpZiAoc2Nyb2xsaW5nKSB7XG4gICAgcmV0dXJuIHN0YXRlO1xuICB9XG5cbiAgaWYgKCF0b3VjaE9iamVjdC5zd2lwZUxlbmd0aCkge1xuICAgIHJldHVybiBzdGF0ZTtcbiAgfVxuXG4gIGlmICh0b3VjaE9iamVjdC5zd2lwZUxlbmd0aCA+IG1pblN3aXBlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgaWYgKG9uU3dpcGUpIHtcbiAgICAgIG9uU3dpcGUoc3dpcGVEaXJlY3Rpb24pO1xuICAgIH1cblxuICAgIHZhciBzbGlkZUNvdW50LCBuZXdTbGlkZTtcblxuICAgIHN3aXRjaCAoc3dpcGVEaXJlY3Rpb24pIHtcbiAgICAgIGNhc2UgXCJsZWZ0XCI6XG4gICAgICBjYXNlIFwidXBcIjpcbiAgICAgICAgbmV3U2xpZGUgPSBjdXJyZW50U2xpZGUgKyBnZXRTbGlkZUNvdW50KHNwZWMpO1xuICAgICAgICBzbGlkZUNvdW50ID0gc3dpcGVUb1NsaWRlID8gY2hlY2tOYXZpZ2FibGUoc3BlYywgbmV3U2xpZGUpIDogbmV3U2xpZGU7XG4gICAgICAgIHN0YXRlW1wiY3VycmVudERpcmVjdGlvblwiXSA9IDA7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIFwicmlnaHRcIjpcbiAgICAgIGNhc2UgXCJkb3duXCI6XG4gICAgICAgIG5ld1NsaWRlID0gY3VycmVudFNsaWRlIC0gZ2V0U2xpZGVDb3VudChzcGVjKTtcbiAgICAgICAgc2xpZGVDb3VudCA9IHN3aXBlVG9TbGlkZSA/IGNoZWNrTmF2aWdhYmxlKHNwZWMsIG5ld1NsaWRlKSA6IG5ld1NsaWRlO1xuICAgICAgICBzdGF0ZVtcImN1cnJlbnREaXJlY3Rpb25cIl0gPSAxO1xuICAgICAgICBicmVhaztcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgc2xpZGVDb3VudCA9IGN1cnJlbnRTbGlkZTtcbiAgICB9XG5cbiAgICBzdGF0ZVtcInRyaWdnZXJTbGlkZUhhbmRsZXJcIl0gPSBzbGlkZUNvdW50O1xuICB9IGVsc2Uge1xuICAgIC8vIEFkanVzdCB0aGUgdHJhY2sgYmFjayB0byBpdCdzIG9yaWdpbmFsIHBvc2l0aW9uLlxuICAgIHZhciBjdXJyZW50TGVmdCA9IGdldFRyYWNrTGVmdChzcGVjKTtcbiAgICBzdGF0ZVtcInRyYWNrU3R5bGVcIl0gPSBnZXRUcmFja0FuaW1hdGVDU1MoX29iamVjdFNwcmVhZCh7fSwgc3BlYywge1xuICAgICAgbGVmdDogY3VycmVudExlZnRcbiAgICB9KSk7XG4gIH1cblxuICByZXR1cm4gc3RhdGU7XG59O1xuXG5leHBvcnRzLnN3aXBlRW5kID0gc3dpcGVFbmQ7XG5cbnZhciBnZXROYXZpZ2FibGVJbmRleGVzID0gZnVuY3Rpb24gZ2V0TmF2aWdhYmxlSW5kZXhlcyhzcGVjKSB7XG4gIHZhciBtYXggPSBzcGVjLmluZmluaXRlID8gc3BlYy5zbGlkZUNvdW50ICogMiA6IHNwZWMuc2xpZGVDb3VudDtcbiAgdmFyIGJyZWFrcG9pbnQgPSBzcGVjLmluZmluaXRlID8gc3BlYy5zbGlkZXNUb1Nob3cgKiAtMSA6IDA7XG4gIHZhciBjb3VudGVyID0gc3BlYy5pbmZpbml0ZSA/IHNwZWMuc2xpZGVzVG9TaG93ICogLTEgOiAwO1xuICB2YXIgaW5kZXhlcyA9IFtdO1xuXG4gIHdoaWxlIChicmVha3BvaW50IDwgbWF4KSB7XG4gICAgaW5kZXhlcy5wdXNoKGJyZWFrcG9pbnQpO1xuICAgIGJyZWFrcG9pbnQgPSBjb3VudGVyICsgc3BlYy5zbGlkZXNUb1Njcm9sbDtcbiAgICBjb3VudGVyICs9IE1hdGgubWluKHNwZWMuc2xpZGVzVG9TY3JvbGwsIHNwZWMuc2xpZGVzVG9TaG93KTtcbiAgfVxuXG4gIHJldHVybiBpbmRleGVzO1xufTtcblxuZXhwb3J0cy5nZXROYXZpZ2FibGVJbmRleGVzID0gZ2V0TmF2aWdhYmxlSW5kZXhlcztcblxudmFyIGNoZWNrTmF2aWdhYmxlID0gZnVuY3Rpb24gY2hlY2tOYXZpZ2FibGUoc3BlYywgaW5kZXgpIHtcbiAgdmFyIG5hdmlnYWJsZXMgPSBnZXROYXZpZ2FibGVJbmRleGVzKHNwZWMpO1xuICB2YXIgcHJldk5hdmlnYWJsZSA9IDA7XG5cbiAgaWYgKGluZGV4ID4gbmF2aWdhYmxlc1tuYXZpZ2FibGVzLmxlbmd0aCAtIDFdKSB7XG4gICAgaW5kZXggPSBuYXZpZ2FibGVzW25hdmlnYWJsZXMubGVuZ3RoIC0gMV07XG4gIH0gZWxzZSB7XG4gICAgZm9yICh2YXIgbiBpbiBuYXZpZ2FibGVzKSB7XG4gICAgICBpZiAoaW5kZXggPCBuYXZpZ2FibGVzW25dKSB7XG4gICAgICAgIGluZGV4ID0gcHJldk5hdmlnYWJsZTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIHByZXZOYXZpZ2FibGUgPSBuYXZpZ2FibGVzW25dO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBpbmRleDtcbn07XG5cbmV4cG9ydHMuY2hlY2tOYXZpZ2FibGUgPSBjaGVja05hdmlnYWJsZTtcblxudmFyIGdldFNsaWRlQ291bnQgPSBmdW5jdGlvbiBnZXRTbGlkZUNvdW50KHNwZWMpIHtcbiAgdmFyIGNlbnRlck9mZnNldCA9IHNwZWMuY2VudGVyTW9kZSA/IHNwZWMuc2xpZGVXaWR0aCAqIE1hdGguZmxvb3Ioc3BlYy5zbGlkZXNUb1Nob3cgLyAyKSA6IDA7XG5cbiAgaWYgKHNwZWMuc3dpcGVUb1NsaWRlKSB7XG4gICAgdmFyIHN3aXBlZFNsaWRlO1xuXG4gICAgdmFyIHNsaWNrTGlzdCA9IF9yZWFjdERvbVtcImRlZmF1bHRcIl0uZmluZERPTU5vZGUoc3BlYy5saXN0UmVmKTtcblxuICAgIHZhciBzbGlkZXMgPSBzbGlja0xpc3QucXVlcnlTZWxlY3RvckFsbChcIi5zbGljay1zbGlkZVwiKTtcbiAgICBBcnJheS5mcm9tKHNsaWRlcykuZXZlcnkoZnVuY3Rpb24gKHNsaWRlKSB7XG4gICAgICBpZiAoIXNwZWMudmVydGljYWwpIHtcbiAgICAgICAgaWYgKHNsaWRlLm9mZnNldExlZnQgLSBjZW50ZXJPZmZzZXQgKyBnZXRXaWR0aChzbGlkZSkgLyAyID4gc3BlYy5zd2lwZUxlZnQgKiAtMSkge1xuICAgICAgICAgIHN3aXBlZFNsaWRlID0gc2xpZGU7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoc2xpZGUub2Zmc2V0VG9wICsgZ2V0SGVpZ2h0KHNsaWRlKSAvIDIgPiBzcGVjLnN3aXBlTGVmdCAqIC0xKSB7XG4gICAgICAgICAgc3dpcGVkU2xpZGUgPSBzbGlkZTtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfSk7XG5cbiAgICBpZiAoIXN3aXBlZFNsaWRlKSB7XG4gICAgICByZXR1cm4gMDtcbiAgICB9XG5cbiAgICB2YXIgY3VycmVudEluZGV4ID0gc3BlYy5ydGwgPT09IHRydWUgPyBzcGVjLnNsaWRlQ291bnQgLSBzcGVjLmN1cnJlbnRTbGlkZSA6IHNwZWMuY3VycmVudFNsaWRlO1xuICAgIHZhciBzbGlkZXNUcmF2ZXJzZWQgPSBNYXRoLmFicyhzd2lwZWRTbGlkZS5kYXRhc2V0LmluZGV4IC0gY3VycmVudEluZGV4KSB8fCAxO1xuICAgIHJldHVybiBzbGlkZXNUcmF2ZXJzZWQ7XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIHNwZWMuc2xpZGVzVG9TY3JvbGw7XG4gIH1cbn07XG5cbmV4cG9ydHMuZ2V0U2xpZGVDb3VudCA9IGdldFNsaWRlQ291bnQ7XG5cbnZhciBjaGVja1NwZWNLZXlzID0gZnVuY3Rpb24gY2hlY2tTcGVjS2V5cyhzcGVjLCBrZXlzQXJyYXkpIHtcbiAgcmV0dXJuIGtleXNBcnJheS5yZWR1Y2UoZnVuY3Rpb24gKHZhbHVlLCBrZXkpIHtcbiAgICByZXR1cm4gdmFsdWUgJiYgc3BlYy5oYXNPd25Qcm9wZXJ0eShrZXkpO1xuICB9LCB0cnVlKSA/IG51bGwgOiBjb25zb2xlLmVycm9yKFwiS2V5cyBNaXNzaW5nOlwiLCBzcGVjKTtcbn07XG5cbmV4cG9ydHMuY2hlY2tTcGVjS2V5cyA9IGNoZWNrU3BlY0tleXM7XG5cbnZhciBnZXRUcmFja0NTUyA9IGZ1bmN0aW9uIGdldFRyYWNrQ1NTKHNwZWMpIHtcbiAgY2hlY2tTcGVjS2V5cyhzcGVjLCBbXCJsZWZ0XCIsIFwidmFyaWFibGVXaWR0aFwiLCBcInNsaWRlQ291bnRcIiwgXCJzbGlkZXNUb1Nob3dcIiwgXCJzbGlkZVdpZHRoXCJdKTtcbiAgdmFyIHRyYWNrV2lkdGgsIHRyYWNrSGVpZ2h0O1xuICB2YXIgdHJhY2tDaGlsZHJlbiA9IHNwZWMuc2xpZGVDb3VudCArIDIgKiBzcGVjLnNsaWRlc1RvU2hvdztcblxuICBpZiAoIXNwZWMudmVydGljYWwpIHtcbiAgICB0cmFja1dpZHRoID0gZ2V0VG90YWxTbGlkZXMoc3BlYykgKiBzcGVjLnNsaWRlV2lkdGg7XG4gIH0gZWxzZSB7XG4gICAgdHJhY2tIZWlnaHQgPSB0cmFja0NoaWxkcmVuICogc3BlYy5zbGlkZUhlaWdodDtcbiAgfVxuXG4gIHZhciBzdHlsZSA9IHtcbiAgICBvcGFjaXR5OiAxLFxuICAgIHRyYW5zaXRpb246IFwiXCIsXG4gICAgV2Via2l0VHJhbnNpdGlvbjogXCJcIlxuICB9O1xuXG4gIGlmIChzcGVjLnVzZVRyYW5zZm9ybSkge1xuICAgIHZhciBXZWJraXRUcmFuc2Zvcm0gPSAhc3BlYy52ZXJ0aWNhbCA/IFwidHJhbnNsYXRlM2QoXCIgKyBzcGVjLmxlZnQgKyBcInB4LCAwcHgsIDBweClcIiA6IFwidHJhbnNsYXRlM2QoMHB4LCBcIiArIHNwZWMubGVmdCArIFwicHgsIDBweClcIjtcbiAgICB2YXIgdHJhbnNmb3JtID0gIXNwZWMudmVydGljYWwgPyBcInRyYW5zbGF0ZTNkKFwiICsgc3BlYy5sZWZ0ICsgXCJweCwgMHB4LCAwcHgpXCIgOiBcInRyYW5zbGF0ZTNkKDBweCwgXCIgKyBzcGVjLmxlZnQgKyBcInB4LCAwcHgpXCI7XG4gICAgdmFyIG1zVHJhbnNmb3JtID0gIXNwZWMudmVydGljYWwgPyBcInRyYW5zbGF0ZVgoXCIgKyBzcGVjLmxlZnQgKyBcInB4KVwiIDogXCJ0cmFuc2xhdGVZKFwiICsgc3BlYy5sZWZ0ICsgXCJweClcIjtcbiAgICBzdHlsZSA9IF9vYmplY3RTcHJlYWQoe30sIHN0eWxlLCB7XG4gICAgICBXZWJraXRUcmFuc2Zvcm06IFdlYmtpdFRyYW5zZm9ybSxcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNmb3JtLFxuICAgICAgbXNUcmFuc2Zvcm06IG1zVHJhbnNmb3JtXG4gICAgfSk7XG4gIH0gZWxzZSB7XG4gICAgaWYgKHNwZWMudmVydGljYWwpIHtcbiAgICAgIHN0eWxlW1widG9wXCJdID0gc3BlYy5sZWZ0O1xuICAgIH0gZWxzZSB7XG4gICAgICBzdHlsZVtcImxlZnRcIl0gPSBzcGVjLmxlZnQ7XG4gICAgfVxuICB9XG5cbiAgaWYgKHNwZWMuZmFkZSkgc3R5bGUgPSB7XG4gICAgb3BhY2l0eTogMVxuICB9O1xuICBpZiAodHJhY2tXaWR0aCkgc3R5bGUud2lkdGggPSB0cmFja1dpZHRoO1xuICBpZiAodHJhY2tIZWlnaHQpIHN0eWxlLmhlaWdodCA9IHRyYWNrSGVpZ2h0OyAvLyBGYWxsYmFjayBmb3IgSUU4XG5cbiAgaWYgKHdpbmRvdyAmJiAhd2luZG93LmFkZEV2ZW50TGlzdGVuZXIgJiYgd2luZG93LmF0dGFjaEV2ZW50KSB7XG4gICAgaWYgKCFzcGVjLnZlcnRpY2FsKSB7XG4gICAgICBzdHlsZS5tYXJnaW5MZWZ0ID0gc3BlYy5sZWZ0ICsgXCJweFwiO1xuICAgIH0gZWxzZSB7XG4gICAgICBzdHlsZS5tYXJnaW5Ub3AgPSBzcGVjLmxlZnQgKyBcInB4XCI7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHN0eWxlO1xufTtcblxuZXhwb3J0cy5nZXRUcmFja0NTUyA9IGdldFRyYWNrQ1NTO1xuXG52YXIgZ2V0VHJhY2tBbmltYXRlQ1NTID0gZnVuY3Rpb24gZ2V0VHJhY2tBbmltYXRlQ1NTKHNwZWMpIHtcbiAgY2hlY2tTcGVjS2V5cyhzcGVjLCBbXCJsZWZ0XCIsIFwidmFyaWFibGVXaWR0aFwiLCBcInNsaWRlQ291bnRcIiwgXCJzbGlkZXNUb1Nob3dcIiwgXCJzbGlkZVdpZHRoXCIsIFwic3BlZWRcIiwgXCJjc3NFYXNlXCJdKTtcbiAgdmFyIHN0eWxlID0gZ2V0VHJhY2tDU1Moc3BlYyk7IC8vIHVzZUNTUyBpcyB0cnVlIGJ5IGRlZmF1bHQgc28gaXQgY2FuIGJlIHVuZGVmaW5lZFxuXG4gIGlmIChzcGVjLnVzZVRyYW5zZm9ybSkge1xuICAgIHN0eWxlLldlYmtpdFRyYW5zaXRpb24gPSBcIi13ZWJraXQtdHJhbnNmb3JtIFwiICsgc3BlYy5zcGVlZCArIFwibXMgXCIgKyBzcGVjLmNzc0Vhc2U7XG4gICAgc3R5bGUudHJhbnNpdGlvbiA9IFwidHJhbnNmb3JtIFwiICsgc3BlYy5zcGVlZCArIFwibXMgXCIgKyBzcGVjLmNzc0Vhc2U7XG4gIH0gZWxzZSB7XG4gICAgaWYgKHNwZWMudmVydGljYWwpIHtcbiAgICAgIHN0eWxlLnRyYW5zaXRpb24gPSBcInRvcCBcIiArIHNwZWMuc3BlZWQgKyBcIm1zIFwiICsgc3BlYy5jc3NFYXNlO1xuICAgIH0gZWxzZSB7XG4gICAgICBzdHlsZS50cmFuc2l0aW9uID0gXCJsZWZ0IFwiICsgc3BlYy5zcGVlZCArIFwibXMgXCIgKyBzcGVjLmNzc0Vhc2U7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHN0eWxlO1xufTtcblxuZXhwb3J0cy5nZXRUcmFja0FuaW1hdGVDU1MgPSBnZXRUcmFja0FuaW1hdGVDU1M7XG5cbnZhciBnZXRUcmFja0xlZnQgPSBmdW5jdGlvbiBnZXRUcmFja0xlZnQoc3BlYykge1xuICBpZiAoc3BlYy51bnNsaWNrKSB7XG4gICAgcmV0dXJuIDA7XG4gIH1cblxuICBjaGVja1NwZWNLZXlzKHNwZWMsIFtcInNsaWRlSW5kZXhcIiwgXCJ0cmFja1JlZlwiLCBcImluZmluaXRlXCIsIFwiY2VudGVyTW9kZVwiLCBcInNsaWRlQ291bnRcIiwgXCJzbGlkZXNUb1Nob3dcIiwgXCJzbGlkZXNUb1Njcm9sbFwiLCBcInNsaWRlV2lkdGhcIiwgXCJsaXN0V2lkdGhcIiwgXCJ2YXJpYWJsZVdpZHRoXCIsIFwic2xpZGVIZWlnaHRcIl0pO1xuICB2YXIgc2xpZGVJbmRleCA9IHNwZWMuc2xpZGVJbmRleCxcbiAgICAgIHRyYWNrUmVmID0gc3BlYy50cmFja1JlZixcbiAgICAgIGluZmluaXRlID0gc3BlYy5pbmZpbml0ZSxcbiAgICAgIGNlbnRlck1vZGUgPSBzcGVjLmNlbnRlck1vZGUsXG4gICAgICBzbGlkZUNvdW50ID0gc3BlYy5zbGlkZUNvdW50LFxuICAgICAgc2xpZGVzVG9TaG93ID0gc3BlYy5zbGlkZXNUb1Nob3csXG4gICAgICBzbGlkZXNUb1Njcm9sbCA9IHNwZWMuc2xpZGVzVG9TY3JvbGwsXG4gICAgICBzbGlkZVdpZHRoID0gc3BlYy5zbGlkZVdpZHRoLFxuICAgICAgbGlzdFdpZHRoID0gc3BlYy5saXN0V2lkdGgsXG4gICAgICB2YXJpYWJsZVdpZHRoID0gc3BlYy52YXJpYWJsZVdpZHRoLFxuICAgICAgc2xpZGVIZWlnaHQgPSBzcGVjLnNsaWRlSGVpZ2h0LFxuICAgICAgZmFkZSA9IHNwZWMuZmFkZSxcbiAgICAgIHZlcnRpY2FsID0gc3BlYy52ZXJ0aWNhbDtcbiAgdmFyIHNsaWRlT2Zmc2V0ID0gMDtcbiAgdmFyIHRhcmdldExlZnQ7XG4gIHZhciB0YXJnZXRTbGlkZTtcbiAgdmFyIHZlcnRpY2FsT2Zmc2V0ID0gMDtcblxuICBpZiAoZmFkZSB8fCBzcGVjLnNsaWRlQ291bnQgPT09IDEpIHtcbiAgICByZXR1cm4gMDtcbiAgfVxuXG4gIHZhciBzbGlkZXNUb09mZnNldCA9IDA7XG5cbiAgaWYgKGluZmluaXRlKSB7XG4gICAgc2xpZGVzVG9PZmZzZXQgPSAtZ2V0UHJlQ2xvbmVzKHNwZWMpOyAvLyBicmluZyBhY3RpdmUgc2xpZGUgdG8gdGhlIGJlZ2lubmluZyBvZiB2aXN1YWwgYXJlYVxuICAgIC8vIGlmIG5leHQgc2Nyb2xsIGRvZXNuJ3QgaGF2ZSBlbm91Z2ggY2hpbGRyZW4sIGp1c3QgcmVhY2ggdGlsbCB0aGUgZW5kIG9mIG9yaWdpbmFsIHNsaWRlcyBpbnN0ZWFkIG9mIHNoaWZ0aW5nIHNsaWRlc1RvU2Nyb2xsIGNoaWxkcmVuXG5cbiAgICBpZiAoc2xpZGVDb3VudCAlIHNsaWRlc1RvU2Nyb2xsICE9PSAwICYmIHNsaWRlSW5kZXggKyBzbGlkZXNUb1Njcm9sbCA+IHNsaWRlQ291bnQpIHtcbiAgICAgIHNsaWRlc1RvT2Zmc2V0ID0gLShzbGlkZUluZGV4ID4gc2xpZGVDb3VudCA/IHNsaWRlc1RvU2hvdyAtIChzbGlkZUluZGV4IC0gc2xpZGVDb3VudCkgOiBzbGlkZUNvdW50ICUgc2xpZGVzVG9TY3JvbGwpO1xuICAgIH0gLy8gc2hpZnQgY3VycmVudCBzbGlkZSB0byBjZW50ZXIgb2YgdGhlIGZyYW1lXG5cblxuICAgIGlmIChjZW50ZXJNb2RlKSB7XG4gICAgICBzbGlkZXNUb09mZnNldCArPSBwYXJzZUludChzbGlkZXNUb1Nob3cgLyAyKTtcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgaWYgKHNsaWRlQ291bnQgJSBzbGlkZXNUb1Njcm9sbCAhPT0gMCAmJiBzbGlkZUluZGV4ICsgc2xpZGVzVG9TY3JvbGwgPiBzbGlkZUNvdW50KSB7XG4gICAgICBzbGlkZXNUb09mZnNldCA9IHNsaWRlc1RvU2hvdyAtIHNsaWRlQ291bnQgJSBzbGlkZXNUb1Njcm9sbDtcbiAgICB9XG5cbiAgICBpZiAoY2VudGVyTW9kZSkge1xuICAgICAgc2xpZGVzVG9PZmZzZXQgPSBwYXJzZUludChzbGlkZXNUb1Nob3cgLyAyKTtcbiAgICB9XG4gIH1cblxuICBzbGlkZU9mZnNldCA9IHNsaWRlc1RvT2Zmc2V0ICogc2xpZGVXaWR0aDtcbiAgdmVydGljYWxPZmZzZXQgPSBzbGlkZXNUb09mZnNldCAqIHNsaWRlSGVpZ2h0O1xuXG4gIGlmICghdmVydGljYWwpIHtcbiAgICB0YXJnZXRMZWZ0ID0gc2xpZGVJbmRleCAqIHNsaWRlV2lkdGggKiAtMSArIHNsaWRlT2Zmc2V0O1xuICB9IGVsc2Uge1xuICAgIHRhcmdldExlZnQgPSBzbGlkZUluZGV4ICogc2xpZGVIZWlnaHQgKiAtMSArIHZlcnRpY2FsT2Zmc2V0O1xuICB9XG5cbiAgaWYgKHZhcmlhYmxlV2lkdGggPT09IHRydWUpIHtcbiAgICB2YXIgdGFyZ2V0U2xpZGVJbmRleDtcblxuICAgIHZhciB0cmFja0VsZW0gPSBfcmVhY3REb21bXCJkZWZhdWx0XCJdLmZpbmRET01Ob2RlKHRyYWNrUmVmKTtcblxuICAgIHRhcmdldFNsaWRlSW5kZXggPSBzbGlkZUluZGV4ICsgZ2V0UHJlQ2xvbmVzKHNwZWMpO1xuICAgIHRhcmdldFNsaWRlID0gdHJhY2tFbGVtICYmIHRyYWNrRWxlbS5jaGlsZE5vZGVzW3RhcmdldFNsaWRlSW5kZXhdO1xuICAgIHRhcmdldExlZnQgPSB0YXJnZXRTbGlkZSA/IHRhcmdldFNsaWRlLm9mZnNldExlZnQgKiAtMSA6IDA7XG5cbiAgICBpZiAoY2VudGVyTW9kZSA9PT0gdHJ1ZSkge1xuICAgICAgdGFyZ2V0U2xpZGVJbmRleCA9IGluZmluaXRlID8gc2xpZGVJbmRleCArIGdldFByZUNsb25lcyhzcGVjKSA6IHNsaWRlSW5kZXg7XG4gICAgICB0YXJnZXRTbGlkZSA9IHRyYWNrRWxlbSAmJiB0cmFja0VsZW0uY2hpbGRyZW5bdGFyZ2V0U2xpZGVJbmRleF07XG4gICAgICB0YXJnZXRMZWZ0ID0gMDtcblxuICAgICAgZm9yICh2YXIgc2xpZGUgPSAwOyBzbGlkZSA8IHRhcmdldFNsaWRlSW5kZXg7IHNsaWRlKyspIHtcbiAgICAgICAgdGFyZ2V0TGVmdCAtPSB0cmFja0VsZW0gJiYgdHJhY2tFbGVtLmNoaWxkcmVuW3NsaWRlXSAmJiB0cmFja0VsZW0uY2hpbGRyZW5bc2xpZGVdLm9mZnNldFdpZHRoO1xuICAgICAgfVxuXG4gICAgICB0YXJnZXRMZWZ0IC09IHBhcnNlSW50KHNwZWMuY2VudGVyUGFkZGluZyk7XG4gICAgICB0YXJnZXRMZWZ0ICs9IHRhcmdldFNsaWRlICYmIChsaXN0V2lkdGggLSB0YXJnZXRTbGlkZS5vZmZzZXRXaWR0aCkgLyAyO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0YXJnZXRMZWZ0O1xufTtcblxuZXhwb3J0cy5nZXRUcmFja0xlZnQgPSBnZXRUcmFja0xlZnQ7XG5cbnZhciBnZXRQcmVDbG9uZXMgPSBmdW5jdGlvbiBnZXRQcmVDbG9uZXMoc3BlYykge1xuICBpZiAoc3BlYy51bnNsaWNrIHx8ICFzcGVjLmluZmluaXRlKSB7XG4gICAgcmV0dXJuIDA7XG4gIH1cblxuICBpZiAoc3BlYy52YXJpYWJsZVdpZHRoKSB7XG4gICAgcmV0dXJuIHNwZWMuc2xpZGVDb3VudDtcbiAgfVxuXG4gIHJldHVybiBzcGVjLnNsaWRlc1RvU2hvdyArIChzcGVjLmNlbnRlck1vZGUgPyAxIDogMCk7XG59O1xuXG5leHBvcnRzLmdldFByZUNsb25lcyA9IGdldFByZUNsb25lcztcblxudmFyIGdldFBvc3RDbG9uZXMgPSBmdW5jdGlvbiBnZXRQb3N0Q2xvbmVzKHNwZWMpIHtcbiAgaWYgKHNwZWMudW5zbGljayB8fCAhc3BlYy5pbmZpbml0ZSkge1xuICAgIHJldHVybiAwO1xuICB9XG5cbiAgcmV0dXJuIHNwZWMuc2xpZGVDb3VudDtcbn07XG5cbmV4cG9ydHMuZ2V0UG9zdENsb25lcyA9IGdldFBvc3RDbG9uZXM7XG5cbnZhciBnZXRUb3RhbFNsaWRlcyA9IGZ1bmN0aW9uIGdldFRvdGFsU2xpZGVzKHNwZWMpIHtcbiAgcmV0dXJuIHNwZWMuc2xpZGVDb3VudCA9PT0gMSA/IDEgOiBnZXRQcmVDbG9uZXMoc3BlYykgKyBzcGVjLnNsaWRlQ291bnQgKyBnZXRQb3N0Q2xvbmVzKHNwZWMpO1xufTtcblxuZXhwb3J0cy5nZXRUb3RhbFNsaWRlcyA9IGdldFRvdGFsU2xpZGVzO1xuXG52YXIgc2libGluZ0RpcmVjdGlvbiA9IGZ1bmN0aW9uIHNpYmxpbmdEaXJlY3Rpb24oc3BlYykge1xuICBpZiAoc3BlYy50YXJnZXRTbGlkZSA+IHNwZWMuY3VycmVudFNsaWRlKSB7XG4gICAgaWYgKHNwZWMudGFyZ2V0U2xpZGUgPiBzcGVjLmN1cnJlbnRTbGlkZSArIHNsaWRlc09uUmlnaHQoc3BlYykpIHtcbiAgICAgIHJldHVybiBcImxlZnRcIjtcbiAgICB9XG5cbiAgICByZXR1cm4gXCJyaWdodFwiO1xuICB9IGVsc2Uge1xuICAgIGlmIChzcGVjLnRhcmdldFNsaWRlIDwgc3BlYy5jdXJyZW50U2xpZGUgLSBzbGlkZXNPbkxlZnQoc3BlYykpIHtcbiAgICAgIHJldHVybiBcInJpZ2h0XCI7XG4gICAgfVxuXG4gICAgcmV0dXJuIFwibGVmdFwiO1xuICB9XG59O1xuXG5leHBvcnRzLnNpYmxpbmdEaXJlY3Rpb24gPSBzaWJsaW5nRGlyZWN0aW9uO1xuXG52YXIgc2xpZGVzT25SaWdodCA9IGZ1bmN0aW9uIHNsaWRlc09uUmlnaHQoX3JlZikge1xuICB2YXIgc2xpZGVzVG9TaG93ID0gX3JlZi5zbGlkZXNUb1Nob3csXG4gICAgICBjZW50ZXJNb2RlID0gX3JlZi5jZW50ZXJNb2RlLFxuICAgICAgcnRsID0gX3JlZi5ydGwsXG4gICAgICBjZW50ZXJQYWRkaW5nID0gX3JlZi5jZW50ZXJQYWRkaW5nO1xuXG4gIC8vIHJldHVybnMgbm8gb2Ygc2xpZGVzIG9uIHRoZSByaWdodCBvZiBhY3RpdmUgc2xpZGVcbiAgaWYgKGNlbnRlck1vZGUpIHtcbiAgICB2YXIgcmlnaHQgPSAoc2xpZGVzVG9TaG93IC0gMSkgLyAyICsgMTtcbiAgICBpZiAocGFyc2VJbnQoY2VudGVyUGFkZGluZykgPiAwKSByaWdodCArPSAxO1xuICAgIGlmIChydGwgJiYgc2xpZGVzVG9TaG93ICUgMiA9PT0gMCkgcmlnaHQgKz0gMTtcbiAgICByZXR1cm4gcmlnaHQ7XG4gIH1cblxuICBpZiAocnRsKSB7XG4gICAgcmV0dXJuIDA7XG4gIH1cblxuICByZXR1cm4gc2xpZGVzVG9TaG93IC0gMTtcbn07XG5cbmV4cG9ydHMuc2xpZGVzT25SaWdodCA9IHNsaWRlc09uUmlnaHQ7XG5cbnZhciBzbGlkZXNPbkxlZnQgPSBmdW5jdGlvbiBzbGlkZXNPbkxlZnQoX3JlZjIpIHtcbiAgdmFyIHNsaWRlc1RvU2hvdyA9IF9yZWYyLnNsaWRlc1RvU2hvdyxcbiAgICAgIGNlbnRlck1vZGUgPSBfcmVmMi5jZW50ZXJNb2RlLFxuICAgICAgcnRsID0gX3JlZjIucnRsLFxuICAgICAgY2VudGVyUGFkZGluZyA9IF9yZWYyLmNlbnRlclBhZGRpbmc7XG5cbiAgLy8gcmV0dXJucyBubyBvZiBzbGlkZXMgb24gdGhlIGxlZnQgb2YgYWN0aXZlIHNsaWRlXG4gIGlmIChjZW50ZXJNb2RlKSB7XG4gICAgdmFyIGxlZnQgPSAoc2xpZGVzVG9TaG93IC0gMSkgLyAyICsgMTtcbiAgICBpZiAocGFyc2VJbnQoY2VudGVyUGFkZGluZykgPiAwKSBsZWZ0ICs9IDE7XG4gICAgaWYgKCFydGwgJiYgc2xpZGVzVG9TaG93ICUgMiA9PT0gMCkgbGVmdCArPSAxO1xuICAgIHJldHVybiBsZWZ0O1xuICB9XG5cbiAgaWYgKHJ0bCkge1xuICAgIHJldHVybiBzbGlkZXNUb1Nob3cgLSAxO1xuICB9XG5cbiAgcmV0dXJuIDA7XG59O1xuXG5leHBvcnRzLnNsaWRlc09uTGVmdCA9IHNsaWRlc09uTGVmdDtcblxudmFyIGNhblVzZURPTSA9IGZ1bmN0aW9uIGNhblVzZURPTSgpIHtcbiAgcmV0dXJuICEhKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgJiYgd2luZG93LmRvY3VtZW50ICYmIHdpbmRvdy5kb2N1bWVudC5jcmVhdGVFbGVtZW50KTtcbn07XG5cbmV4cG9ydHMuY2FuVXNlRE9NID0gY2FuVXNlRE9NOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBRkE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFIQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFDQTtBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/react-slick/lib/utils/innerSliderUtils.js
