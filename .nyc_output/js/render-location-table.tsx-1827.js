__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return RenderLocationTable; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _desk_styles_global__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @desk-styles/global */ "./src/app/desk/styles/global.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/component/render-location-table.tsx";




var RenderLocationTable =
/*#__PURE__*/
function (_Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(RenderLocationTable, _Component);

  function RenderLocationTable() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, RenderLocationTable);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(RenderLocationTable).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(RenderLocationTable, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          columns = _this$props.columns,
          locations = _this$props.locations,
          handleMember = _this$props.handleMember;
      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_5___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_6__["TableHeader"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 15
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Table"], {
        pagination: false,
        size: "default",
        bordered: true,
        columns: columns,
        dataSource: locations,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 16
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        style: {
          marginTop: 20
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 24
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Button"], {
        type: "primary",
        onClick: function onClick() {
          handleMember();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 27
        },
        __self: this
      }, "Add")));
    }
  }]);

  return RenderLocationTable;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3JlbmRlci1sb2NhdGlvbi10YWJsZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9TQUlDL2lhci9jb21wb25lbnQvcmVuZGVyLWxvY2F0aW9uLXRhYmxlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBUYWJsZUhlYWRlciB9IGZyb20gXCJAZGVzay1zdHlsZXMvZ2xvYmFsXCI7XG5pbXBvcnQgeyBCdXR0b24sIFRhYmxlIH0gZnJvbSBcImFudGRcIjtcblxuaW50ZXJmYWNlIElQcm9wcyB7XG4gIGNvbHVtbnM6IGFueSxcbiAgbG9jYXRpb25zOiBhbnksXG4gIGhhbmRsZU1lbWJlcjogYW55XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFJlbmRlckxvY2F0aW9uVGFibGUgZXh0ZW5kcyBDb21wb25lbnQ8SVByb3BzLCBhbnk+IHtcbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY29sdW1ucywgbG9jYXRpb25zLCBoYW5kbGVNZW1iZXIgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIDw+XG4gICAgICA8VGFibGVIZWFkZXI+XG4gICAgICAgIDxUYWJsZVxuICAgICAgICAgIHBhZ2luYXRpb249e2ZhbHNlfVxuICAgICAgICAgIHNpemU9XCJkZWZhdWx0XCJcbiAgICAgICAgICBib3JkZXJlZFxuICAgICAgICAgIGNvbHVtbnM9e2NvbHVtbnN9XG4gICAgICAgICAgZGF0YVNvdXJjZT17bG9jYXRpb25zfVxuICAgICAgICAvPlxuICAgICAgPC9UYWJsZUhlYWRlcj5cbiAgICAgIDxkaXYgc3R5bGU9e3tcbiAgICAgICAgbWFyZ2luVG9wOiAyMCxcbiAgICAgIH19PlxuICAgICAgICA8QnV0dG9uIHR5cGU9XCJwcmltYXJ5XCIgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgIGhhbmRsZU1lbWJlcigpO1xuICAgICAgICB9fT5BZGQ8L0J1dHRvbj5cbiAgICAgIDwvZGl2PlxuICAgIDwvPjtcbiAgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBT0E7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7OztBQXJCQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/component/render-location-table.tsx
