/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var ORIGIN_METHOD = '\0__throttleOriginMethod';
var RATE = '\0__throttleRate';
var THROTTLE_TYPE = '\0__throttleType';
/**
 * @public
 * @param {(Function)} fn
 * @param {number} [delay=0] Unit: ms.
 * @param {boolean} [debounce=false]
 *        true: If call interval less than `delay`, only the last call works.
 *        false: If call interval less than `delay, call works on fixed rate.
 * @return {(Function)} throttled fn.
 */

function throttle(fn, delay, debounce) {
  var currCall;
  var lastCall = 0;
  var lastExec = 0;
  var timer = null;
  var diff;
  var scope;
  var args;
  var debounceNextCall;
  delay = delay || 0;

  function exec() {
    lastExec = new Date().getTime();
    timer = null;
    fn.apply(scope, args || []);
  }

  var cb = function cb() {
    currCall = new Date().getTime();
    scope = this;
    args = arguments;
    var thisDelay = debounceNextCall || delay;
    var thisDebounce = debounceNextCall || debounce;
    debounceNextCall = null;
    diff = currCall - (thisDebounce ? lastCall : lastExec) - thisDelay;
    clearTimeout(timer); // Here we should make sure that: the `exec` SHOULD NOT be called later
    // than a new call of `cb`, that is, preserving the command order. Consider
    // calculating "scale rate" when roaming as an example. When a call of `cb`
    // happens, either the `exec` is called dierectly, or the call is delayed.
    // But the delayed call should never be later than next call of `cb`. Under
    // this assurance, we can simply update view state each time `dispatchAction`
    // triggered by user roaming, but not need to add extra code to avoid the
    // state being "rolled-back".

    if (thisDebounce) {
      timer = setTimeout(exec, thisDelay);
    } else {
      if (diff >= 0) {
        exec();
      } else {
        timer = setTimeout(exec, -diff);
      }
    }

    lastCall = currCall;
  };
  /**
   * Clear throttle.
   * @public
   */


  cb.clear = function () {
    if (timer) {
      clearTimeout(timer);
      timer = null;
    }
  };
  /**
   * Enable debounce once.
   */


  cb.debounceNextCall = function (debounceDelay) {
    debounceNextCall = debounceDelay;
  };

  return cb;
}
/**
 * Create throttle method or update throttle rate.
 *
 * @example
 * ComponentView.prototype.render = function () {
 *     ...
 *     throttle.createOrUpdate(
 *         this,
 *         '_dispatchAction',
 *         this.model.get('throttle'),
 *         'fixRate'
 *     );
 * };
 * ComponentView.prototype.remove = function () {
 *     throttle.clear(this, '_dispatchAction');
 * };
 * ComponentView.prototype.dispose = function () {
 *     throttle.clear(this, '_dispatchAction');
 * };
 *
 * @public
 * @param {Object} obj
 * @param {string} fnAttr
 * @param {number} [rate]
 * @param {string} [throttleType='fixRate'] 'fixRate' or 'debounce'
 * @return {Function} throttled function.
 */


function createOrUpdate(obj, fnAttr, rate, throttleType) {
  var fn = obj[fnAttr];

  if (!fn) {
    return;
  }

  var originFn = fn[ORIGIN_METHOD] || fn;
  var lastThrottleType = fn[THROTTLE_TYPE];
  var lastRate = fn[RATE];

  if (lastRate !== rate || lastThrottleType !== throttleType) {
    if (rate == null || !throttleType) {
      return obj[fnAttr] = originFn;
    }

    fn = obj[fnAttr] = throttle(originFn, rate, throttleType === 'debounce');
    fn[ORIGIN_METHOD] = originFn;
    fn[THROTTLE_TYPE] = throttleType;
    fn[RATE] = rate;
  }

  return fn;
}
/**
 * Clear throttle. Example see throttle.createOrUpdate.
 *
 * @public
 * @param {Object} obj
 * @param {string} fnAttr
 */


function clear(obj, fnAttr) {
  var fn = obj[fnAttr];

  if (fn && fn[ORIGIN_METHOD]) {
    obj[fnAttr] = fn[ORIGIN_METHOD];
  }
}

exports.throttle = throttle;
exports.createOrUpdate = createOrUpdate;
exports.clear = clear;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdXRpbC90aHJvdHRsZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3V0aWwvdGhyb3R0bGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBPUklHSU5fTUVUSE9EID0gJ1xcMF9fdGhyb3R0bGVPcmlnaW5NZXRob2QnO1xudmFyIFJBVEUgPSAnXFwwX190aHJvdHRsZVJhdGUnO1xudmFyIFRIUk9UVExFX1RZUEUgPSAnXFwwX190aHJvdHRsZVR5cGUnO1xuLyoqXG4gKiBAcHVibGljXG4gKiBAcGFyYW0geyhGdW5jdGlvbil9IGZuXG4gKiBAcGFyYW0ge251bWJlcn0gW2RlbGF5PTBdIFVuaXQ6IG1zLlxuICogQHBhcmFtIHtib29sZWFufSBbZGVib3VuY2U9ZmFsc2VdXG4gKiAgICAgICAgdHJ1ZTogSWYgY2FsbCBpbnRlcnZhbCBsZXNzIHRoYW4gYGRlbGF5YCwgb25seSB0aGUgbGFzdCBjYWxsIHdvcmtzLlxuICogICAgICAgIGZhbHNlOiBJZiBjYWxsIGludGVydmFsIGxlc3MgdGhhbiBgZGVsYXksIGNhbGwgd29ya3Mgb24gZml4ZWQgcmF0ZS5cbiAqIEByZXR1cm4geyhGdW5jdGlvbil9IHRocm90dGxlZCBmbi5cbiAqL1xuXG5mdW5jdGlvbiB0aHJvdHRsZShmbiwgZGVsYXksIGRlYm91bmNlKSB7XG4gIHZhciBjdXJyQ2FsbDtcbiAgdmFyIGxhc3RDYWxsID0gMDtcbiAgdmFyIGxhc3RFeGVjID0gMDtcbiAgdmFyIHRpbWVyID0gbnVsbDtcbiAgdmFyIGRpZmY7XG4gIHZhciBzY29wZTtcbiAgdmFyIGFyZ3M7XG4gIHZhciBkZWJvdW5jZU5leHRDYWxsO1xuICBkZWxheSA9IGRlbGF5IHx8IDA7XG5cbiAgZnVuY3Rpb24gZXhlYygpIHtcbiAgICBsYXN0RXhlYyA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgIHRpbWVyID0gbnVsbDtcbiAgICBmbi5hcHBseShzY29wZSwgYXJncyB8fCBbXSk7XG4gIH1cblxuICB2YXIgY2IgPSBmdW5jdGlvbiAoKSB7XG4gICAgY3VyckNhbGwgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICBzY29wZSA9IHRoaXM7XG4gICAgYXJncyA9IGFyZ3VtZW50cztcbiAgICB2YXIgdGhpc0RlbGF5ID0gZGVib3VuY2VOZXh0Q2FsbCB8fCBkZWxheTtcbiAgICB2YXIgdGhpc0RlYm91bmNlID0gZGVib3VuY2VOZXh0Q2FsbCB8fCBkZWJvdW5jZTtcbiAgICBkZWJvdW5jZU5leHRDYWxsID0gbnVsbDtcbiAgICBkaWZmID0gY3VyckNhbGwgLSAodGhpc0RlYm91bmNlID8gbGFzdENhbGwgOiBsYXN0RXhlYykgLSB0aGlzRGVsYXk7XG4gICAgY2xlYXJUaW1lb3V0KHRpbWVyKTsgLy8gSGVyZSB3ZSBzaG91bGQgbWFrZSBzdXJlIHRoYXQ6IHRoZSBgZXhlY2AgU0hPVUxEIE5PVCBiZSBjYWxsZWQgbGF0ZXJcbiAgICAvLyB0aGFuIGEgbmV3IGNhbGwgb2YgYGNiYCwgdGhhdCBpcywgcHJlc2VydmluZyB0aGUgY29tbWFuZCBvcmRlci4gQ29uc2lkZXJcbiAgICAvLyBjYWxjdWxhdGluZyBcInNjYWxlIHJhdGVcIiB3aGVuIHJvYW1pbmcgYXMgYW4gZXhhbXBsZS4gV2hlbiBhIGNhbGwgb2YgYGNiYFxuICAgIC8vIGhhcHBlbnMsIGVpdGhlciB0aGUgYGV4ZWNgIGlzIGNhbGxlZCBkaWVyZWN0bHksIG9yIHRoZSBjYWxsIGlzIGRlbGF5ZWQuXG4gICAgLy8gQnV0IHRoZSBkZWxheWVkIGNhbGwgc2hvdWxkIG5ldmVyIGJlIGxhdGVyIHRoYW4gbmV4dCBjYWxsIG9mIGBjYmAuIFVuZGVyXG4gICAgLy8gdGhpcyBhc3N1cmFuY2UsIHdlIGNhbiBzaW1wbHkgdXBkYXRlIHZpZXcgc3RhdGUgZWFjaCB0aW1lIGBkaXNwYXRjaEFjdGlvbmBcbiAgICAvLyB0cmlnZ2VyZWQgYnkgdXNlciByb2FtaW5nLCBidXQgbm90IG5lZWQgdG8gYWRkIGV4dHJhIGNvZGUgdG8gYXZvaWQgdGhlXG4gICAgLy8gc3RhdGUgYmVpbmcgXCJyb2xsZWQtYmFja1wiLlxuXG4gICAgaWYgKHRoaXNEZWJvdW5jZSkge1xuICAgICAgdGltZXIgPSBzZXRUaW1lb3V0KGV4ZWMsIHRoaXNEZWxheSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChkaWZmID49IDApIHtcbiAgICAgICAgZXhlYygpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGltZXIgPSBzZXRUaW1lb3V0KGV4ZWMsIC1kaWZmKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBsYXN0Q2FsbCA9IGN1cnJDYWxsO1xuICB9O1xuICAvKipcbiAgICogQ2xlYXIgdGhyb3R0bGUuXG4gICAqIEBwdWJsaWNcbiAgICovXG5cblxuICBjYi5jbGVhciA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAodGltZXIpIHtcbiAgICAgIGNsZWFyVGltZW91dCh0aW1lcik7XG4gICAgICB0aW1lciA9IG51bGw7XG4gICAgfVxuICB9O1xuICAvKipcbiAgICogRW5hYmxlIGRlYm91bmNlIG9uY2UuXG4gICAqL1xuXG5cbiAgY2IuZGVib3VuY2VOZXh0Q2FsbCA9IGZ1bmN0aW9uIChkZWJvdW5jZURlbGF5KSB7XG4gICAgZGVib3VuY2VOZXh0Q2FsbCA9IGRlYm91bmNlRGVsYXk7XG4gIH07XG5cbiAgcmV0dXJuIGNiO1xufVxuLyoqXG4gKiBDcmVhdGUgdGhyb3R0bGUgbWV0aG9kIG9yIHVwZGF0ZSB0aHJvdHRsZSByYXRlLlxuICpcbiAqIEBleGFtcGxlXG4gKiBDb21wb25lbnRWaWV3LnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiAoKSB7XG4gKiAgICAgLi4uXG4gKiAgICAgdGhyb3R0bGUuY3JlYXRlT3JVcGRhdGUoXG4gKiAgICAgICAgIHRoaXMsXG4gKiAgICAgICAgICdfZGlzcGF0Y2hBY3Rpb24nLFxuICogICAgICAgICB0aGlzLm1vZGVsLmdldCgndGhyb3R0bGUnKSxcbiAqICAgICAgICAgJ2ZpeFJhdGUnXG4gKiAgICAgKTtcbiAqIH07XG4gKiBDb21wb25lbnRWaWV3LnByb3RvdHlwZS5yZW1vdmUgPSBmdW5jdGlvbiAoKSB7XG4gKiAgICAgdGhyb3R0bGUuY2xlYXIodGhpcywgJ19kaXNwYXRjaEFjdGlvbicpO1xuICogfTtcbiAqIENvbXBvbmVudFZpZXcucHJvdG90eXBlLmRpc3Bvc2UgPSBmdW5jdGlvbiAoKSB7XG4gKiAgICAgdGhyb3R0bGUuY2xlYXIodGhpcywgJ19kaXNwYXRjaEFjdGlvbicpO1xuICogfTtcbiAqXG4gKiBAcHVibGljXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqXG4gKiBAcGFyYW0ge3N0cmluZ30gZm5BdHRyXG4gKiBAcGFyYW0ge251bWJlcn0gW3JhdGVdXG4gKiBAcGFyYW0ge3N0cmluZ30gW3Rocm90dGxlVHlwZT0nZml4UmF0ZSddICdmaXhSYXRlJyBvciAnZGVib3VuY2UnXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn0gdGhyb3R0bGVkIGZ1bmN0aW9uLlxuICovXG5cblxuZnVuY3Rpb24gY3JlYXRlT3JVcGRhdGUob2JqLCBmbkF0dHIsIHJhdGUsIHRocm90dGxlVHlwZSkge1xuICB2YXIgZm4gPSBvYmpbZm5BdHRyXTtcblxuICBpZiAoIWZuKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIG9yaWdpbkZuID0gZm5bT1JJR0lOX01FVEhPRF0gfHwgZm47XG4gIHZhciBsYXN0VGhyb3R0bGVUeXBlID0gZm5bVEhST1RUTEVfVFlQRV07XG4gIHZhciBsYXN0UmF0ZSA9IGZuW1JBVEVdO1xuXG4gIGlmIChsYXN0UmF0ZSAhPT0gcmF0ZSB8fCBsYXN0VGhyb3R0bGVUeXBlICE9PSB0aHJvdHRsZVR5cGUpIHtcbiAgICBpZiAocmF0ZSA9PSBudWxsIHx8ICF0aHJvdHRsZVR5cGUpIHtcbiAgICAgIHJldHVybiBvYmpbZm5BdHRyXSA9IG9yaWdpbkZuO1xuICAgIH1cblxuICAgIGZuID0gb2JqW2ZuQXR0cl0gPSB0aHJvdHRsZShvcmlnaW5GbiwgcmF0ZSwgdGhyb3R0bGVUeXBlID09PSAnZGVib3VuY2UnKTtcbiAgICBmbltPUklHSU5fTUVUSE9EXSA9IG9yaWdpbkZuO1xuICAgIGZuW1RIUk9UVExFX1RZUEVdID0gdGhyb3R0bGVUeXBlO1xuICAgIGZuW1JBVEVdID0gcmF0ZTtcbiAgfVxuXG4gIHJldHVybiBmbjtcbn1cbi8qKlxuICogQ2xlYXIgdGhyb3R0bGUuIEV4YW1wbGUgc2VlIHRocm90dGxlLmNyZWF0ZU9yVXBkYXRlLlxuICpcbiAqIEBwdWJsaWNcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmpcbiAqIEBwYXJhbSB7c3RyaW5nfSBmbkF0dHJcbiAqL1xuXG5cbmZ1bmN0aW9uIGNsZWFyKG9iaiwgZm5BdHRyKSB7XG4gIHZhciBmbiA9IG9ialtmbkF0dHJdO1xuXG4gIGlmIChmbiAmJiBmbltPUklHSU5fTUVUSE9EXSkge1xuICAgIG9ialtmbkF0dHJdID0gZm5bT1JJR0lOX01FVEhPRF07XG4gIH1cbn1cblxuZXhwb3J0cy50aHJvdHRsZSA9IHRocm90dGxlO1xuZXhwb3J0cy5jcmVhdGVPclVwZGF0ZSA9IGNyZWF0ZU9yVXBkYXRlO1xuZXhwb3J0cy5jbGVhciA9IGNsZWFyOyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/util/throttle.js
