__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  ZERO: 48,
  NINE: 57,
  NUMPAD_ZERO: 96,
  NUMPAD_NINE: 105,
  BACKSPACE: 8,
  DELETE: 46,
  ENTER: 13,
  ARROW_UP: 38,
  ARROW_DOWN: 40
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtcGFnaW5hdGlvbi9lcy9LZXlDb2RlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtcGFnaW5hdGlvbi9lcy9LZXlDb2RlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWZhdWx0IHtcbiAgWkVSTzogNDgsXG4gIE5JTkU6IDU3LFxuXG4gIE5VTVBBRF9aRVJPOiA5NixcbiAgTlVNUEFEX05JTkU6IDEwNSxcblxuICBCQUNLU1BBQ0U6IDgsXG4gIERFTEVURTogNDYsXG4gIEVOVEVSOiAxMyxcblxuICBBUlJPV19VUDogMzgsXG4gIEFSUk9XX0RPV046IDQwXG59OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQVpBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-pagination/es/KeyCode.js
