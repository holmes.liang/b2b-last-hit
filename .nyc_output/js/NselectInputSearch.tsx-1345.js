__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NSelectInputSearch", function() { return NSelectInputSearch; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _NSelect__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./NSelect */ "./src/app/component/NSelect.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/component/NselectInputSearch.tsx";




var NSelectInputSearch =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(NSelectInputSearch, _ModelWidget);

  function NSelectInputSearch(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, NSelectInputSearch);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(NSelectInputSearch).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(NSelectInputSearch, [{
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      var _this$props = this.props,
          _this$props$labelInVa = _this$props.labelInValue,
          labelInValue = _this$props$labelInVa === void 0 ? true : _this$props$labelInVa,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["labelInValue"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_NSelect__WEBPACK_IMPORTED_MODULE_8__["NSelect"], Object.assign({
        showSearch: true,
        labelInValue: labelInValue,
        filterOption: false,
        optionFilterProp: "children",
        onSearch: function onSearch(value) {
          // TODO
          if (!value) return;
          _this.props.onSearch && _this.props.onSearch(value);
        }
      }, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        },
        __self: this
      }));
    }
  }]);

  return NSelectInputSearch;
}(_component__WEBPACK_IMPORTED_MODULE_7__["ModelWidget"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2NvbXBvbmVudC9Oc2VsZWN0SW5wdXRTZWFyY2gudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2NvbXBvbmVudC9Oc2VsZWN0SW5wdXRTZWFyY2gudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBTZWxlY3RQcm9wcyB9IGZyb20gXCJhbnRkL2xpYi9zZWxlY3RcIjtcblxuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQ29kZUl0ZW0gfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBORm9ybUl0ZW1Qcm9wcyB9IGZyb20gXCJAY29tcG9uZW50L05Gb3JtSXRlbVwiO1xuXG5pbXBvcnQgeyBOU2VsZWN0IH0gZnJvbSBcIi4vTlNlbGVjdFwiO1xuXG5leHBvcnQgdHlwZSBOU2VsZWN0UHJvcHMgPSB7XG4gIGlzQ29tcGFyZT86IGJvb2xlYW47XG4gIHRhYmxlTmFtZT86IHN0cmluZztcbiAgb3B0aW9ucz86IENvZGVJdGVtW107XG4gIGZldGNoT3B0aW9ucz86IGFueTtcbiAgZmlsdGVyPzogKG9wdGlvbnM6IENvZGVJdGVtW10pID0+IENvZGVJdGVtW107XG59ICYgU2VsZWN0UHJvcHMgJiBORm9ybUl0ZW1Qcm9wcztcblxudHlwZSBOU2VsZWN0U3RhdGUgPSB7XG4gIG9wdGlvbnM6IGFueVtdXG59XG5cbmNsYXNzIE5TZWxlY3RJbnB1dFNlYXJjaDxQIGV4dGVuZHMgTlNlbGVjdFByb3BzLCBTIGV4dGVuZHMgTlNlbGVjdFN0YXRlLCBDPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IE5TZWxlY3RQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge30gYXMgQztcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuXG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBsYWJlbEluVmFsdWUgPSB0cnVlLCAuLi5yZXN0IH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8TlNlbGVjdFxuICAgICAgICBzaG93U2VhcmNoXG4gICAgICAgIGxhYmVsSW5WYWx1ZT17bGFiZWxJblZhbHVlfVxuICAgICAgICBmaWx0ZXJPcHRpb249e2ZhbHNlfVxuICAgICAgICBvcHRpb25GaWx0ZXJQcm9wPVwiY2hpbGRyZW5cIlxuICAgICAgICBvblNlYXJjaD17dmFsdWUgPT4ge1xuICAgICAgICAgIC8vIFRPRE9cbiAgICAgICAgICBpZiAoIXZhbHVlKSByZXR1cm47XG4gICAgICAgICAgdGhpcy5wcm9wcy5vblNlYXJjaCAmJiB0aGlzLnByb3BzLm9uU2VhcmNoKHZhbHVlKTtcbiAgICAgICAgfX1cbiAgICAgICAgey4uLnJlc3R9XG4gICAgICAvPlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IHsgTlNlbGVjdElucHV0U2VhcmNoIH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBR0E7QUFJQTtBQUNBO0FBYUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUVBOzs7QUFJQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBOzs7O0FBN0JBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/component/NselectInputSearch.tsx
