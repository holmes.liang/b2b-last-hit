/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var each = _util.each;
var map = _util.map;

var _number = __webpack_require__(/*! ../util/number */ "./node_modules/echarts/lib/util/number.js");

var linearMap = _number.linearMap;
var _getPixelPrecision = _number.getPixelPrecision;

var _axisTickLabelBuilder = __webpack_require__(/*! ./axisTickLabelBuilder */ "./node_modules/echarts/lib/coord/axisTickLabelBuilder.js");

var createAxisTicks = _axisTickLabelBuilder.createAxisTicks;
var createAxisLabels = _axisTickLabelBuilder.createAxisLabels;
var _calculateCategoryInterval = _axisTickLabelBuilder.calculateCategoryInterval;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var NORMALIZED_EXTENT = [0, 1];
/**
 * Base class of Axis.
 * @constructor
 */

var Axis = function Axis(dim, scale, extent) {
  /**
   * Axis dimension. Such as 'x', 'y', 'z', 'angle', 'radius'.
   * @type {string}
   */
  this.dim = dim;
  /**
   * Axis scale
   * @type {module:echarts/coord/scale/*}
   */

  this.scale = scale;
  /**
   * @type {Array.<number>}
   * @private
   */

  this._extent = extent || [0, 0];
  /**
   * @type {boolean}
   */

  this.inverse = false;
  /**
   * Usually true when axis has a ordinal scale
   * @type {boolean}
   */

  this.onBand = false;
};

Axis.prototype = {
  constructor: Axis,

  /**
   * If axis extent contain given coord
   * @param {number} coord
   * @return {boolean}
   */
  contain: function contain(coord) {
    var extent = this._extent;
    var min = Math.min(extent[0], extent[1]);
    var max = Math.max(extent[0], extent[1]);
    return coord >= min && coord <= max;
  },

  /**
   * If axis extent contain given data
   * @param {number} data
   * @return {boolean}
   */
  containData: function containData(data) {
    return this.contain(this.dataToCoord(data));
  },

  /**
   * Get coord extent.
   * @return {Array.<number>}
   */
  getExtent: function getExtent() {
    return this._extent.slice();
  },

  /**
   * Get precision used for formatting
   * @param {Array.<number>} [dataExtent]
   * @return {number}
   */
  getPixelPrecision: function getPixelPrecision(dataExtent) {
    return _getPixelPrecision(dataExtent || this.scale.getExtent(), this._extent);
  },

  /**
   * Set coord extent
   * @param {number} start
   * @param {number} end
   */
  setExtent: function setExtent(start, end) {
    var extent = this._extent;
    extent[0] = start;
    extent[1] = end;
  },

  /**
   * Convert data to coord. Data is the rank if it has an ordinal scale
   * @param {number} data
   * @param  {boolean} clamp
   * @return {number}
   */
  dataToCoord: function dataToCoord(data, clamp) {
    var extent = this._extent;
    var scale = this.scale;
    data = scale.normalize(data);

    if (this.onBand && scale.type === 'ordinal') {
      extent = extent.slice();
      fixExtentWithBands(extent, scale.count());
    }

    return linearMap(data, NORMALIZED_EXTENT, extent, clamp);
  },

  /**
   * Convert coord to data. Data is the rank if it has an ordinal scale
   * @param {number} coord
   * @param  {boolean} clamp
   * @return {number}
   */
  coordToData: function coordToData(coord, clamp) {
    var extent = this._extent;
    var scale = this.scale;

    if (this.onBand && scale.type === 'ordinal') {
      extent = extent.slice();
      fixExtentWithBands(extent, scale.count());
    }

    var t = linearMap(coord, extent, NORMALIZED_EXTENT, clamp);
    return this.scale.scale(t);
  },

  /**
   * Convert pixel point to data in axis
   * @param {Array.<number>} point
   * @param  {boolean} clamp
   * @return {number} data
   */
  pointToData: function pointToData(point, clamp) {// Should be implemented in derived class if necessary.
  },

  /**
   * Different from `zrUtil.map(axis.getTicks(), axis.dataToCoord, axis)`,
   * `axis.getTicksCoords` considers `onBand`, which is used by
   * `boundaryGap:true` of category axis and splitLine and splitArea.
   * @param {Object} [opt]
   * @param {number} [opt.tickModel=axis.model.getModel('axisTick')]
   * @param {boolean} [opt.clamp] If `true`, the first and the last
   *        tick must be at the axis end points. Otherwise, clip ticks
   *        that outside the axis extent.
   * @return {Array.<Object>} [{
   *     coord: ...,
   *     tickValue: ...
   * }, ...]
   */
  getTicksCoords: function getTicksCoords(opt) {
    opt = opt || {};
    var tickModel = opt.tickModel || this.getTickModel();
    var result = createAxisTicks(this, tickModel);
    var ticks = result.ticks;
    var ticksCoords = map(ticks, function (tickValue) {
      return {
        coord: this.dataToCoord(tickValue),
        tickValue: tickValue
      };
    }, this);
    var alignWithLabel = tickModel.get('alignWithLabel');
    fixOnBandTicksCoords(this, ticksCoords, result.tickCategoryInterval, alignWithLabel, opt.clamp);
    return ticksCoords;
  },

  /**
   * @return {Array.<Object>} [{
   *     formattedLabel: string,
   *     rawLabel: axis.scale.getLabel(tickValue)
   *     tickValue: number
   * }, ...]
   */
  getViewLabels: function getViewLabels() {
    return createAxisLabels(this).labels;
  },

  /**
   * @return {module:echarts/coord/model/Model}
   */
  getLabelModel: function getLabelModel() {
    return this.model.getModel('axisLabel');
  },

  /**
   * Notice here we only get the default tick model. For splitLine
   * or splitArea, we should pass the splitLineModel or splitAreaModel
   * manually when calling `getTicksCoords`.
   * In GL, this method may be overrided to:
   * `axisModel.getModel('axisTick', grid3DModel.getModel('axisTick'));`
   * @return {module:echarts/coord/model/Model}
   */
  getTickModel: function getTickModel() {
    return this.model.getModel('axisTick');
  },

  /**
   * Get width of band
   * @return {number}
   */
  getBandWidth: function getBandWidth() {
    var axisExtent = this._extent;
    var dataExtent = this.scale.getExtent();
    var len = dataExtent[1] - dataExtent[0] + (this.onBand ? 1 : 0); // Fix #2728, avoid NaN when only one data.

    len === 0 && (len = 1);
    var size = Math.abs(axisExtent[1] - axisExtent[0]);
    return Math.abs(size) / len;
  },

  /**
   * @abstract
   * @return {boolean} Is horizontal
   */
  isHorizontal: null,

  /**
   * @abstract
   * @return {number} Get axis rotate, by degree.
   */
  getRotate: null,

  /**
   * Only be called in category axis.
   * Can be overrided, consider other axes like in 3D.
   * @return {number} Auto interval for cateogry axis tick and label
   */
  calculateCategoryInterval: function calculateCategoryInterval() {
    return _calculateCategoryInterval(this);
  }
};

function fixExtentWithBands(extent, nTick) {
  var size = extent[1] - extent[0];
  var len = nTick;
  var margin = size / len / 2;
  extent[0] += margin;
  extent[1] -= margin;
} // If axis has labels [1, 2, 3, 4]. Bands on the axis are
// |---1---|---2---|---3---|---4---|.
// So the displayed ticks and splitLine/splitArea should between
// each data item, otherwise cause misleading (e.g., split tow bars
// of a single data item when there are two bar series).
// Also consider if tickCategoryInterval > 0 and onBand, ticks and
// splitLine/spliteArea should layout appropriately corresponding
// to displayed labels. (So we should not use `getBandWidth` in this
// case).


function fixOnBandTicksCoords(axis, ticksCoords, tickCategoryInterval, alignWithLabel, clamp) {
  var ticksLen = ticksCoords.length;

  if (!axis.onBand || alignWithLabel || !ticksLen) {
    return;
  }

  var axisExtent = axis.getExtent();
  var last;

  if (ticksLen === 1) {
    ticksCoords[0].coord = axisExtent[0];
    last = ticksCoords[1] = {
      coord: axisExtent[0]
    };
  } else {
    var shift = ticksCoords[1].coord - ticksCoords[0].coord;
    each(ticksCoords, function (ticksItem) {
      ticksItem.coord -= shift / 2;
      var tickCategoryInterval = tickCategoryInterval || 0; // Avoid split a single data item when odd interval.

      if (tickCategoryInterval % 2 > 0) {
        ticksItem.coord -= shift / ((tickCategoryInterval + 1) * 2);
      }
    });
    last = {
      coord: ticksCoords[ticksLen - 1].coord + shift
    };
    ticksCoords.push(last);
  }

  var inverse = axisExtent[0] > axisExtent[1];

  if (littleThan(ticksCoords[0].coord, axisExtent[0])) {
    clamp ? ticksCoords[0].coord = axisExtent[0] : ticksCoords.shift();
  }

  if (clamp && littleThan(axisExtent[0], ticksCoords[0].coord)) {
    ticksCoords.unshift({
      coord: axisExtent[0]
    });
  }

  if (littleThan(axisExtent[1], last.coord)) {
    clamp ? last.coord = axisExtent[1] : ticksCoords.pop();
  }

  if (clamp && littleThan(last.coord, axisExtent[1])) {
    ticksCoords.push({
      coord: axisExtent[1]
    });
  }

  function littleThan(a, b) {
    return inverse ? a > b : a < b;
  }
}

var _default = Axis;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvQXhpcy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2Nvb3JkL0F4aXMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBlYWNoID0gX3V0aWwuZWFjaDtcbnZhciBtYXAgPSBfdXRpbC5tYXA7XG5cbnZhciBfbnVtYmVyID0gcmVxdWlyZShcIi4uL3V0aWwvbnVtYmVyXCIpO1xuXG52YXIgbGluZWFyTWFwID0gX251bWJlci5saW5lYXJNYXA7XG52YXIgZ2V0UGl4ZWxQcmVjaXNpb24gPSBfbnVtYmVyLmdldFBpeGVsUHJlY2lzaW9uO1xuXG52YXIgX2F4aXNUaWNrTGFiZWxCdWlsZGVyID0gcmVxdWlyZShcIi4vYXhpc1RpY2tMYWJlbEJ1aWxkZXJcIik7XG5cbnZhciBjcmVhdGVBeGlzVGlja3MgPSBfYXhpc1RpY2tMYWJlbEJ1aWxkZXIuY3JlYXRlQXhpc1RpY2tzO1xudmFyIGNyZWF0ZUF4aXNMYWJlbHMgPSBfYXhpc1RpY2tMYWJlbEJ1aWxkZXIuY3JlYXRlQXhpc0xhYmVscztcbnZhciBjYWxjdWxhdGVDYXRlZ29yeUludGVydmFsID0gX2F4aXNUaWNrTGFiZWxCdWlsZGVyLmNhbGN1bGF0ZUNhdGVnb3J5SW50ZXJ2YWw7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBOT1JNQUxJWkVEX0VYVEVOVCA9IFswLCAxXTtcbi8qKlxuICogQmFzZSBjbGFzcyBvZiBBeGlzLlxuICogQGNvbnN0cnVjdG9yXG4gKi9cblxudmFyIEF4aXMgPSBmdW5jdGlvbiAoZGltLCBzY2FsZSwgZXh0ZW50KSB7XG4gIC8qKlxuICAgKiBBeGlzIGRpbWVuc2lvbi4gU3VjaCBhcyAneCcsICd5JywgJ3onLCAnYW5nbGUnLCAncmFkaXVzJy5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIHRoaXMuZGltID0gZGltO1xuICAvKipcbiAgICogQXhpcyBzY2FsZVxuICAgKiBAdHlwZSB7bW9kdWxlOmVjaGFydHMvY29vcmQvc2NhbGUvKn1cbiAgICovXG5cbiAgdGhpcy5zY2FsZSA9IHNjYWxlO1xuICAvKipcbiAgICogQHR5cGUge0FycmF5LjxudW1iZXI+fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB0aGlzLl9leHRlbnQgPSBleHRlbnQgfHwgWzAsIDBdO1xuICAvKipcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqL1xuXG4gIHRoaXMuaW52ZXJzZSA9IGZhbHNlO1xuICAvKipcbiAgICogVXN1YWxseSB0cnVlIHdoZW4gYXhpcyBoYXMgYSBvcmRpbmFsIHNjYWxlXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKi9cblxuICB0aGlzLm9uQmFuZCA9IGZhbHNlO1xufTtcblxuQXhpcy5wcm90b3R5cGUgPSB7XG4gIGNvbnN0cnVjdG9yOiBBeGlzLFxuXG4gIC8qKlxuICAgKiBJZiBheGlzIGV4dGVudCBjb250YWluIGdpdmVuIGNvb3JkXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBjb29yZFxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgY29udGFpbjogZnVuY3Rpb24gKGNvb3JkKSB7XG4gICAgdmFyIGV4dGVudCA9IHRoaXMuX2V4dGVudDtcbiAgICB2YXIgbWluID0gTWF0aC5taW4oZXh0ZW50WzBdLCBleHRlbnRbMV0pO1xuICAgIHZhciBtYXggPSBNYXRoLm1heChleHRlbnRbMF0sIGV4dGVudFsxXSk7XG4gICAgcmV0dXJuIGNvb3JkID49IG1pbiAmJiBjb29yZCA8PSBtYXg7XG4gIH0sXG5cbiAgLyoqXG4gICAqIElmIGF4aXMgZXh0ZW50IGNvbnRhaW4gZ2l2ZW4gZGF0YVxuICAgKiBAcGFyYW0ge251bWJlcn0gZGF0YVxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgY29udGFpbkRhdGE6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgcmV0dXJuIHRoaXMuY29udGFpbih0aGlzLmRhdGFUb0Nvb3JkKGRhdGEpKTtcbiAgfSxcblxuICAvKipcbiAgICogR2V0IGNvb3JkIGV4dGVudC5cbiAgICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59XG4gICAqL1xuICBnZXRFeHRlbnQ6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5fZXh0ZW50LnNsaWNlKCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEdldCBwcmVjaXNpb24gdXNlZCBmb3IgZm9ybWF0dGluZ1xuICAgKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBbZGF0YUV4dGVudF1cbiAgICogQHJldHVybiB7bnVtYmVyfVxuICAgKi9cbiAgZ2V0UGl4ZWxQcmVjaXNpb246IGZ1bmN0aW9uIChkYXRhRXh0ZW50KSB7XG4gICAgcmV0dXJuIGdldFBpeGVsUHJlY2lzaW9uKGRhdGFFeHRlbnQgfHwgdGhpcy5zY2FsZS5nZXRFeHRlbnQoKSwgdGhpcy5fZXh0ZW50KTtcbiAgfSxcblxuICAvKipcbiAgICogU2V0IGNvb3JkIGV4dGVudFxuICAgKiBAcGFyYW0ge251bWJlcn0gc3RhcnRcbiAgICogQHBhcmFtIHtudW1iZXJ9IGVuZFxuICAgKi9cbiAgc2V0RXh0ZW50OiBmdW5jdGlvbiAoc3RhcnQsIGVuZCkge1xuICAgIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7XG4gICAgZXh0ZW50WzBdID0gc3RhcnQ7XG4gICAgZXh0ZW50WzFdID0gZW5kO1xuICB9LFxuXG4gIC8qKlxuICAgKiBDb252ZXJ0IGRhdGEgdG8gY29vcmQuIERhdGEgaXMgdGhlIHJhbmsgaWYgaXQgaGFzIGFuIG9yZGluYWwgc2NhbGVcbiAgICogQHBhcmFtIHtudW1iZXJ9IGRhdGFcbiAgICogQHBhcmFtICB7Ym9vbGVhbn0gY2xhbXBcbiAgICogQHJldHVybiB7bnVtYmVyfVxuICAgKi9cbiAgZGF0YVRvQ29vcmQ6IGZ1bmN0aW9uIChkYXRhLCBjbGFtcCkge1xuICAgIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7XG4gICAgdmFyIHNjYWxlID0gdGhpcy5zY2FsZTtcbiAgICBkYXRhID0gc2NhbGUubm9ybWFsaXplKGRhdGEpO1xuXG4gICAgaWYgKHRoaXMub25CYW5kICYmIHNjYWxlLnR5cGUgPT09ICdvcmRpbmFsJykge1xuICAgICAgZXh0ZW50ID0gZXh0ZW50LnNsaWNlKCk7XG4gICAgICBmaXhFeHRlbnRXaXRoQmFuZHMoZXh0ZW50LCBzY2FsZS5jb3VudCgpKTtcbiAgICB9XG5cbiAgICByZXR1cm4gbGluZWFyTWFwKGRhdGEsIE5PUk1BTElaRURfRVhURU5ULCBleHRlbnQsIGNsYW1wKTtcbiAgfSxcblxuICAvKipcbiAgICogQ29udmVydCBjb29yZCB0byBkYXRhLiBEYXRhIGlzIHRoZSByYW5rIGlmIGl0IGhhcyBhbiBvcmRpbmFsIHNjYWxlXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBjb29yZFxuICAgKiBAcGFyYW0gIHtib29sZWFufSBjbGFtcFxuICAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICAqL1xuICBjb29yZFRvRGF0YTogZnVuY3Rpb24gKGNvb3JkLCBjbGFtcCkge1xuICAgIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7XG4gICAgdmFyIHNjYWxlID0gdGhpcy5zY2FsZTtcblxuICAgIGlmICh0aGlzLm9uQmFuZCAmJiBzY2FsZS50eXBlID09PSAnb3JkaW5hbCcpIHtcbiAgICAgIGV4dGVudCA9IGV4dGVudC5zbGljZSgpO1xuICAgICAgZml4RXh0ZW50V2l0aEJhbmRzKGV4dGVudCwgc2NhbGUuY291bnQoKSk7XG4gICAgfVxuXG4gICAgdmFyIHQgPSBsaW5lYXJNYXAoY29vcmQsIGV4dGVudCwgTk9STUFMSVpFRF9FWFRFTlQsIGNsYW1wKTtcbiAgICByZXR1cm4gdGhpcy5zY2FsZS5zY2FsZSh0KTtcbiAgfSxcblxuICAvKipcbiAgICogQ29udmVydCBwaXhlbCBwb2ludCB0byBkYXRhIGluIGF4aXNcbiAgICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gcG9pbnRcbiAgICogQHBhcmFtICB7Ym9vbGVhbn0gY2xhbXBcbiAgICogQHJldHVybiB7bnVtYmVyfSBkYXRhXG4gICAqL1xuICBwb2ludFRvRGF0YTogZnVuY3Rpb24gKHBvaW50LCBjbGFtcCkgey8vIFNob3VsZCBiZSBpbXBsZW1lbnRlZCBpbiBkZXJpdmVkIGNsYXNzIGlmIG5lY2Vzc2FyeS5cbiAgfSxcblxuICAvKipcbiAgICogRGlmZmVyZW50IGZyb20gYHpyVXRpbC5tYXAoYXhpcy5nZXRUaWNrcygpLCBheGlzLmRhdGFUb0Nvb3JkLCBheGlzKWAsXG4gICAqIGBheGlzLmdldFRpY2tzQ29vcmRzYCBjb25zaWRlcnMgYG9uQmFuZGAsIHdoaWNoIGlzIHVzZWQgYnlcbiAgICogYGJvdW5kYXJ5R2FwOnRydWVgIG9mIGNhdGVnb3J5IGF4aXMgYW5kIHNwbGl0TGluZSBhbmQgc3BsaXRBcmVhLlxuICAgKiBAcGFyYW0ge09iamVjdH0gW29wdF1cbiAgICogQHBhcmFtIHtudW1iZXJ9IFtvcHQudGlja01vZGVsPWF4aXMubW9kZWwuZ2V0TW9kZWwoJ2F4aXNUaWNrJyldXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdC5jbGFtcF0gSWYgYHRydWVgLCB0aGUgZmlyc3QgYW5kIHRoZSBsYXN0XG4gICAqICAgICAgICB0aWNrIG11c3QgYmUgYXQgdGhlIGF4aXMgZW5kIHBvaW50cy4gT3RoZXJ3aXNlLCBjbGlwIHRpY2tzXG4gICAqICAgICAgICB0aGF0IG91dHNpZGUgdGhlIGF4aXMgZXh0ZW50LlxuICAgKiBAcmV0dXJuIHtBcnJheS48T2JqZWN0Pn0gW3tcbiAgICogICAgIGNvb3JkOiAuLi4sXG4gICAqICAgICB0aWNrVmFsdWU6IC4uLlxuICAgKiB9LCAuLi5dXG4gICAqL1xuICBnZXRUaWNrc0Nvb3JkczogZnVuY3Rpb24gKG9wdCkge1xuICAgIG9wdCA9IG9wdCB8fCB7fTtcbiAgICB2YXIgdGlja01vZGVsID0gb3B0LnRpY2tNb2RlbCB8fCB0aGlzLmdldFRpY2tNb2RlbCgpO1xuICAgIHZhciByZXN1bHQgPSBjcmVhdGVBeGlzVGlja3ModGhpcywgdGlja01vZGVsKTtcbiAgICB2YXIgdGlja3MgPSByZXN1bHQudGlja3M7XG4gICAgdmFyIHRpY2tzQ29vcmRzID0gbWFwKHRpY2tzLCBmdW5jdGlvbiAodGlja1ZhbHVlKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBjb29yZDogdGhpcy5kYXRhVG9Db29yZCh0aWNrVmFsdWUpLFxuICAgICAgICB0aWNrVmFsdWU6IHRpY2tWYWx1ZVxuICAgICAgfTtcbiAgICB9LCB0aGlzKTtcbiAgICB2YXIgYWxpZ25XaXRoTGFiZWwgPSB0aWNrTW9kZWwuZ2V0KCdhbGlnbldpdGhMYWJlbCcpO1xuICAgIGZpeE9uQmFuZFRpY2tzQ29vcmRzKHRoaXMsIHRpY2tzQ29vcmRzLCByZXN1bHQudGlja0NhdGVnb3J5SW50ZXJ2YWwsIGFsaWduV2l0aExhYmVsLCBvcHQuY2xhbXApO1xuICAgIHJldHVybiB0aWNrc0Nvb3JkcztcbiAgfSxcblxuICAvKipcbiAgICogQHJldHVybiB7QXJyYXkuPE9iamVjdD59IFt7XG4gICAqICAgICBmb3JtYXR0ZWRMYWJlbDogc3RyaW5nLFxuICAgKiAgICAgcmF3TGFiZWw6IGF4aXMuc2NhbGUuZ2V0TGFiZWwodGlja1ZhbHVlKVxuICAgKiAgICAgdGlja1ZhbHVlOiBudW1iZXJcbiAgICogfSwgLi4uXVxuICAgKi9cbiAgZ2V0Vmlld0xhYmVsczogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBjcmVhdGVBeGlzTGFiZWxzKHRoaXMpLmxhYmVscztcbiAgfSxcblxuICAvKipcbiAgICogQHJldHVybiB7bW9kdWxlOmVjaGFydHMvY29vcmQvbW9kZWwvTW9kZWx9XG4gICAqL1xuICBnZXRMYWJlbE1vZGVsOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMubW9kZWwuZ2V0TW9kZWwoJ2F4aXNMYWJlbCcpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBOb3RpY2UgaGVyZSB3ZSBvbmx5IGdldCB0aGUgZGVmYXVsdCB0aWNrIG1vZGVsLiBGb3Igc3BsaXRMaW5lXG4gICAqIG9yIHNwbGl0QXJlYSwgd2Ugc2hvdWxkIHBhc3MgdGhlIHNwbGl0TGluZU1vZGVsIG9yIHNwbGl0QXJlYU1vZGVsXG4gICAqIG1hbnVhbGx5IHdoZW4gY2FsbGluZyBgZ2V0VGlja3NDb29yZHNgLlxuICAgKiBJbiBHTCwgdGhpcyBtZXRob2QgbWF5IGJlIG92ZXJyaWRlZCB0bzpcbiAgICogYGF4aXNNb2RlbC5nZXRNb2RlbCgnYXhpc1RpY2snLCBncmlkM0RNb2RlbC5nZXRNb2RlbCgnYXhpc1RpY2snKSk7YFxuICAgKiBAcmV0dXJuIHttb2R1bGU6ZWNoYXJ0cy9jb29yZC9tb2RlbC9Nb2RlbH1cbiAgICovXG4gIGdldFRpY2tNb2RlbDogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLm1vZGVsLmdldE1vZGVsKCdheGlzVGljaycpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBHZXQgd2lkdGggb2YgYmFuZFxuICAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICAqL1xuICBnZXRCYW5kV2lkdGg6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgYXhpc0V4dGVudCA9IHRoaXMuX2V4dGVudDtcbiAgICB2YXIgZGF0YUV4dGVudCA9IHRoaXMuc2NhbGUuZ2V0RXh0ZW50KCk7XG4gICAgdmFyIGxlbiA9IGRhdGFFeHRlbnRbMV0gLSBkYXRhRXh0ZW50WzBdICsgKHRoaXMub25CYW5kID8gMSA6IDApOyAvLyBGaXggIzI3MjgsIGF2b2lkIE5hTiB3aGVuIG9ubHkgb25lIGRhdGEuXG5cbiAgICBsZW4gPT09IDAgJiYgKGxlbiA9IDEpO1xuICAgIHZhciBzaXplID0gTWF0aC5hYnMoYXhpc0V4dGVudFsxXSAtIGF4aXNFeHRlbnRbMF0pO1xuICAgIHJldHVybiBNYXRoLmFicyhzaXplKSAvIGxlbjtcbiAgfSxcblxuICAvKipcbiAgICogQGFic3RyYWN0XG4gICAqIEByZXR1cm4ge2Jvb2xlYW59IElzIGhvcml6b250YWxcbiAgICovXG4gIGlzSG9yaXpvbnRhbDogbnVsbCxcblxuICAvKipcbiAgICogQGFic3RyYWN0XG4gICAqIEByZXR1cm4ge251bWJlcn0gR2V0IGF4aXMgcm90YXRlLCBieSBkZWdyZWUuXG4gICAqL1xuICBnZXRSb3RhdGU6IG51bGwsXG5cbiAgLyoqXG4gICAqIE9ubHkgYmUgY2FsbGVkIGluIGNhdGVnb3J5IGF4aXMuXG4gICAqIENhbiBiZSBvdmVycmlkZWQsIGNvbnNpZGVyIG90aGVyIGF4ZXMgbGlrZSBpbiAzRC5cbiAgICogQHJldHVybiB7bnVtYmVyfSBBdXRvIGludGVydmFsIGZvciBjYXRlb2dyeSBheGlzIHRpY2sgYW5kIGxhYmVsXG4gICAqL1xuICBjYWxjdWxhdGVDYXRlZ29yeUludGVydmFsOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGNhbGN1bGF0ZUNhdGVnb3J5SW50ZXJ2YWwodGhpcyk7XG4gIH1cbn07XG5cbmZ1bmN0aW9uIGZpeEV4dGVudFdpdGhCYW5kcyhleHRlbnQsIG5UaWNrKSB7XG4gIHZhciBzaXplID0gZXh0ZW50WzFdIC0gZXh0ZW50WzBdO1xuICB2YXIgbGVuID0gblRpY2s7XG4gIHZhciBtYXJnaW4gPSBzaXplIC8gbGVuIC8gMjtcbiAgZXh0ZW50WzBdICs9IG1hcmdpbjtcbiAgZXh0ZW50WzFdIC09IG1hcmdpbjtcbn0gLy8gSWYgYXhpcyBoYXMgbGFiZWxzIFsxLCAyLCAzLCA0XS4gQmFuZHMgb24gdGhlIGF4aXMgYXJlXG4vLyB8LS0tMS0tLXwtLS0yLS0tfC0tLTMtLS18LS0tNC0tLXwuXG4vLyBTbyB0aGUgZGlzcGxheWVkIHRpY2tzIGFuZCBzcGxpdExpbmUvc3BsaXRBcmVhIHNob3VsZCBiZXR3ZWVuXG4vLyBlYWNoIGRhdGEgaXRlbSwgb3RoZXJ3aXNlIGNhdXNlIG1pc2xlYWRpbmcgKGUuZy4sIHNwbGl0IHRvdyBiYXJzXG4vLyBvZiBhIHNpbmdsZSBkYXRhIGl0ZW0gd2hlbiB0aGVyZSBhcmUgdHdvIGJhciBzZXJpZXMpLlxuLy8gQWxzbyBjb25zaWRlciBpZiB0aWNrQ2F0ZWdvcnlJbnRlcnZhbCA+IDAgYW5kIG9uQmFuZCwgdGlja3MgYW5kXG4vLyBzcGxpdExpbmUvc3BsaXRlQXJlYSBzaG91bGQgbGF5b3V0IGFwcHJvcHJpYXRlbHkgY29ycmVzcG9uZGluZ1xuLy8gdG8gZGlzcGxheWVkIGxhYmVscy4gKFNvIHdlIHNob3VsZCBub3QgdXNlIGBnZXRCYW5kV2lkdGhgIGluIHRoaXNcbi8vIGNhc2UpLlxuXG5cbmZ1bmN0aW9uIGZpeE9uQmFuZFRpY2tzQ29vcmRzKGF4aXMsIHRpY2tzQ29vcmRzLCB0aWNrQ2F0ZWdvcnlJbnRlcnZhbCwgYWxpZ25XaXRoTGFiZWwsIGNsYW1wKSB7XG4gIHZhciB0aWNrc0xlbiA9IHRpY2tzQ29vcmRzLmxlbmd0aDtcblxuICBpZiAoIWF4aXMub25CYW5kIHx8IGFsaWduV2l0aExhYmVsIHx8ICF0aWNrc0xlbikge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBheGlzRXh0ZW50ID0gYXhpcy5nZXRFeHRlbnQoKTtcbiAgdmFyIGxhc3Q7XG5cbiAgaWYgKHRpY2tzTGVuID09PSAxKSB7XG4gICAgdGlja3NDb29yZHNbMF0uY29vcmQgPSBheGlzRXh0ZW50WzBdO1xuICAgIGxhc3QgPSB0aWNrc0Nvb3Jkc1sxXSA9IHtcbiAgICAgIGNvb3JkOiBheGlzRXh0ZW50WzBdXG4gICAgfTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgc2hpZnQgPSB0aWNrc0Nvb3Jkc1sxXS5jb29yZCAtIHRpY2tzQ29vcmRzWzBdLmNvb3JkO1xuICAgIGVhY2godGlja3NDb29yZHMsIGZ1bmN0aW9uICh0aWNrc0l0ZW0pIHtcbiAgICAgIHRpY2tzSXRlbS5jb29yZCAtPSBzaGlmdCAvIDI7XG4gICAgICB2YXIgdGlja0NhdGVnb3J5SW50ZXJ2YWwgPSB0aWNrQ2F0ZWdvcnlJbnRlcnZhbCB8fCAwOyAvLyBBdm9pZCBzcGxpdCBhIHNpbmdsZSBkYXRhIGl0ZW0gd2hlbiBvZGQgaW50ZXJ2YWwuXG5cbiAgICAgIGlmICh0aWNrQ2F0ZWdvcnlJbnRlcnZhbCAlIDIgPiAwKSB7XG4gICAgICAgIHRpY2tzSXRlbS5jb29yZCAtPSBzaGlmdCAvICgodGlja0NhdGVnb3J5SW50ZXJ2YWwgKyAxKSAqIDIpO1xuICAgICAgfVxuICAgIH0pO1xuICAgIGxhc3QgPSB7XG4gICAgICBjb29yZDogdGlja3NDb29yZHNbdGlja3NMZW4gLSAxXS5jb29yZCArIHNoaWZ0XG4gICAgfTtcbiAgICB0aWNrc0Nvb3Jkcy5wdXNoKGxhc3QpO1xuICB9XG5cbiAgdmFyIGludmVyc2UgPSBheGlzRXh0ZW50WzBdID4gYXhpc0V4dGVudFsxXTtcblxuICBpZiAobGl0dGxlVGhhbih0aWNrc0Nvb3Jkc1swXS5jb29yZCwgYXhpc0V4dGVudFswXSkpIHtcbiAgICBjbGFtcCA/IHRpY2tzQ29vcmRzWzBdLmNvb3JkID0gYXhpc0V4dGVudFswXSA6IHRpY2tzQ29vcmRzLnNoaWZ0KCk7XG4gIH1cblxuICBpZiAoY2xhbXAgJiYgbGl0dGxlVGhhbihheGlzRXh0ZW50WzBdLCB0aWNrc0Nvb3Jkc1swXS5jb29yZCkpIHtcbiAgICB0aWNrc0Nvb3Jkcy51bnNoaWZ0KHtcbiAgICAgIGNvb3JkOiBheGlzRXh0ZW50WzBdXG4gICAgfSk7XG4gIH1cblxuICBpZiAobGl0dGxlVGhhbihheGlzRXh0ZW50WzFdLCBsYXN0LmNvb3JkKSkge1xuICAgIGNsYW1wID8gbGFzdC5jb29yZCA9IGF4aXNFeHRlbnRbMV0gOiB0aWNrc0Nvb3Jkcy5wb3AoKTtcbiAgfVxuXG4gIGlmIChjbGFtcCAmJiBsaXR0bGVUaGFuKGxhc3QuY29vcmQsIGF4aXNFeHRlbnRbMV0pKSB7XG4gICAgdGlja3NDb29yZHMucHVzaCh7XG4gICAgICBjb29yZDogYXhpc0V4dGVudFsxXVxuICAgIH0pO1xuICB9XG5cbiAgZnVuY3Rpb24gbGl0dGxlVGhhbihhLCBiKSB7XG4gICAgcmV0dXJuIGludmVyc2UgPyBhID4gYiA6IGEgPCBiO1xuICB9XG59XG5cbnZhciBfZGVmYXVsdCA9IEF4aXM7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7QUFJQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7OztBQUlBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFoTUE7QUFDQTtBQWtNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/Axis.js
