/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule insertFragmentIntoContentState
 * @format
 * 
 */


var BlockMapBuilder = __webpack_require__(/*! ./BlockMapBuilder */ "./node_modules/draft-js/lib/BlockMapBuilder.js");

var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var insertIntoList = __webpack_require__(/*! ./insertIntoList */ "./node_modules/draft-js/lib/insertIntoList.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var randomizeBlockMapKeys = __webpack_require__(/*! ./randomizeBlockMapKeys */ "./node_modules/draft-js/lib/randomizeBlockMapKeys.js");

var List = Immutable.List;

var updateExistingBlock = function updateExistingBlock(contentState, selectionState, blockMap, fragmentBlock, targetKey, targetOffset) {
  var targetBlock = blockMap.get(targetKey);
  var text = targetBlock.getText();
  var chars = targetBlock.getCharacterList();
  var finalKey = targetKey;
  var finalOffset = targetOffset + fragmentBlock.getText().length;
  var newBlock = targetBlock.merge({
    text: text.slice(0, targetOffset) + fragmentBlock.getText() + text.slice(targetOffset),
    characterList: insertIntoList(chars, fragmentBlock.getCharacterList(), targetOffset),
    data: fragmentBlock.getData()
  });
  return contentState.merge({
    blockMap: blockMap.set(targetKey, newBlock),
    selectionBefore: selectionState,
    selectionAfter: selectionState.merge({
      anchorKey: finalKey,
      anchorOffset: finalOffset,
      focusKey: finalKey,
      focusOffset: finalOffset,
      isBackward: false
    })
  });
};
/**
 * Appends text/characterList from the fragment first block to
 * target block.
 */


var updateHead = function updateHead(block, targetOffset, fragment) {
  var text = block.getText();
  var chars = block.getCharacterList(); // Modify head portion of block.

  var headText = text.slice(0, targetOffset);
  var headCharacters = chars.slice(0, targetOffset);
  var appendToHead = fragment.first();
  return block.merge({
    text: headText + appendToHead.getText(),
    characterList: headCharacters.concat(appendToHead.getCharacterList()),
    type: headText ? block.getType() : appendToHead.getType(),
    data: appendToHead.getData()
  });
};
/**
 * Appends offset text/characterList from the target block to the last
 * fragment block.
 */


var updateTail = function updateTail(block, targetOffset, fragment) {
  // Modify tail portion of block.
  var text = block.getText();
  var chars = block.getCharacterList(); // Modify head portion of block.

  var blockSize = text.length;
  var tailText = text.slice(targetOffset, blockSize);
  var tailCharacters = chars.slice(targetOffset, blockSize);
  var prependToTail = fragment.last();
  return prependToTail.merge({
    text: prependToTail.getText() + tailText,
    characterList: prependToTail.getCharacterList().concat(tailCharacters),
    data: prependToTail.getData()
  });
};

var getRootBlocks = function getRootBlocks(block, blockMap) {
  var headKey = block.getKey();
  var rootBlock = block;
  var rootBlocks = []; // sometimes the fragment head block will not be part of the blockMap itself this can happen when
  // the fragment head is used to update the target block, however when this does not happen we need
  // to make sure that we include it on the rootBlocks since the first block of a fragment is always a
  // fragment root block

  if (blockMap.get(headKey)) {
    rootBlocks.push(headKey);
  }

  while (rootBlock && rootBlock.getNextSiblingKey()) {
    var lastSiblingKey = rootBlock.getNextSiblingKey();

    if (!lastSiblingKey) {
      break;
    }

    rootBlocks.push(lastSiblingKey);
    rootBlock = blockMap.get(lastSiblingKey);
  }

  return rootBlocks;
};

var updateBlockMapLinks = function updateBlockMapLinks(blockMap, originalBlockMap, targetBlock, fragmentHeadBlock) {
  return blockMap.withMutations(function (blockMapState) {
    var targetKey = targetBlock.getKey();
    var headKey = fragmentHeadBlock.getKey();
    var targetNextKey = targetBlock.getNextSiblingKey();
    var targetParentKey = targetBlock.getParentKey();
    var fragmentRootBlocks = getRootBlocks(fragmentHeadBlock, blockMap);
    var lastRootFragmentBlockKey = fragmentRootBlocks[fragmentRootBlocks.length - 1];

    if (blockMapState.get(headKey)) {
      // update the fragment head when it is part of the blockMap otherwise
      blockMapState.setIn([targetKey, 'nextSibling'], headKey);
      blockMapState.setIn([headKey, 'prevSibling'], targetKey);
    } else {
      // update the target block that had the fragment head contents merged into it
      blockMapState.setIn([targetKey, 'nextSibling'], fragmentHeadBlock.getNextSiblingKey());
      blockMapState.setIn([fragmentHeadBlock.getNextSiblingKey(), 'prevSibling'], targetKey);
    } // update the last root block fragment


    blockMapState.setIn([lastRootFragmentBlockKey, 'nextSibling'], targetNextKey); // update the original target next block

    if (targetNextKey) {
      blockMapState.setIn([targetNextKey, 'prevSibling'], lastRootFragmentBlockKey);
    } // update fragment parent links


    fragmentRootBlocks.forEach(function (blockKey) {
      return blockMapState.setIn([blockKey, 'parent'], targetParentKey);
    }); // update targetBlock parent child links

    if (targetParentKey) {
      var targetParent = blockMap.get(targetParentKey);
      var originalTargetParentChildKeys = targetParent.getChildKeys();
      var targetBlockIndex = originalTargetParentChildKeys.indexOf(targetKey);
      var insertionIndex = targetBlockIndex + 1;
      var newChildrenKeysArray = originalTargetParentChildKeys.toArray(); // insert fragment children

      newChildrenKeysArray.splice.apply(newChildrenKeysArray, [insertionIndex, 0].concat(fragmentRootBlocks));
      blockMapState.setIn([targetParentKey, 'children'], List(newChildrenKeysArray));
    }
  });
};

var insertFragment = function insertFragment(contentState, selectionState, blockMap, fragment, targetKey, targetOffset) {
  var isTreeBasedBlockMap = blockMap.first() instanceof ContentBlockNode;
  var newBlockArr = [];
  var fragmentSize = fragment.size;
  var target = blockMap.get(targetKey);
  var head = fragment.first();
  var tail = fragment.last();
  var finalOffset = tail.getLength();
  var finalKey = tail.getKey();
  var shouldNotUpdateFromFragmentBlock = isTreeBasedBlockMap && (!target.getChildKeys().isEmpty() || !head.getChildKeys().isEmpty());
  blockMap.forEach(function (block, blockKey) {
    if (blockKey !== targetKey) {
      newBlockArr.push(block);
      return;
    }

    if (shouldNotUpdateFromFragmentBlock) {
      newBlockArr.push(block);
    } else {
      newBlockArr.push(updateHead(block, targetOffset, fragment));
    } // Insert fragment blocks after the head and before the tail.


    fragment // when we are updating the target block with the head fragment block we skip the first fragment
    // head since its contents have already been merged with the target block otherwise we include
    // the whole fragment
    .slice(shouldNotUpdateFromFragmentBlock ? 0 : 1, fragmentSize - 1).forEach(function (fragmentBlock) {
      return newBlockArr.push(fragmentBlock);
    }); // update tail

    newBlockArr.push(updateTail(block, targetOffset, fragment));
  });
  var updatedBlockMap = BlockMapBuilder.createFromArray(newBlockArr);

  if (isTreeBasedBlockMap) {
    updatedBlockMap = updateBlockMapLinks(updatedBlockMap, blockMap, target, head);
  }

  return contentState.merge({
    blockMap: updatedBlockMap,
    selectionBefore: selectionState,
    selectionAfter: selectionState.merge({
      anchorKey: finalKey,
      anchorOffset: finalOffset,
      focusKey: finalKey,
      focusOffset: finalOffset,
      isBackward: false
    })
  });
};

var insertFragmentIntoContentState = function insertFragmentIntoContentState(contentState, selectionState, fragmentBlockMap) {
  !selectionState.isCollapsed() ?  true ? invariant(false, '`insertFragment` should only be called with a collapsed selection state.') : undefined : void 0;
  var blockMap = contentState.getBlockMap();
  var fragment = randomizeBlockMapKeys(fragmentBlockMap);
  var targetKey = selectionState.getStartKey();
  var targetOffset = selectionState.getStartOffset();
  var targetBlock = blockMap.get(targetKey);

  if (targetBlock instanceof ContentBlockNode) {
    !targetBlock.getChildKeys().isEmpty() ?  true ? invariant(false, '`insertFragment` should not be called when a container node is selected.') : undefined : void 0;
  } // When we insert a fragment with a single block we simply update the target block
  // with the contents of the inserted fragment block


  if (fragment.size === 1) {
    return updateExistingBlock(contentState, selectionState, blockMap, fragment.first(), targetKey, targetOffset);
  }

  return insertFragment(contentState, selectionState, blockMap, fragment, targetKey, targetOffset);
};

module.exports = insertFragmentIntoContentState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2luc2VydEZyYWdtZW50SW50b0NvbnRlbnRTdGF0ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9pbnNlcnRGcmFnbWVudEludG9Db250ZW50U3RhdGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBpbnNlcnRGcmFnbWVudEludG9Db250ZW50U3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEJsb2NrTWFwQnVpbGRlciA9IHJlcXVpcmUoJy4vQmxvY2tNYXBCdWlsZGVyJyk7XG52YXIgQ29udGVudEJsb2NrTm9kZSA9IHJlcXVpcmUoJy4vQ29udGVudEJsb2NrTm9kZScpO1xudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xuXG52YXIgaW5zZXJ0SW50b0xpc3QgPSByZXF1aXJlKCcuL2luc2VydEludG9MaXN0Jyk7XG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG52YXIgcmFuZG9taXplQmxvY2tNYXBLZXlzID0gcmVxdWlyZSgnLi9yYW5kb21pemVCbG9ja01hcEtleXMnKTtcblxudmFyIExpc3QgPSBJbW11dGFibGUuTGlzdDtcblxuXG52YXIgdXBkYXRlRXhpc3RpbmdCbG9jayA9IGZ1bmN0aW9uIHVwZGF0ZUV4aXN0aW5nQmxvY2soY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSwgYmxvY2tNYXAsIGZyYWdtZW50QmxvY2ssIHRhcmdldEtleSwgdGFyZ2V0T2Zmc2V0KSB7XG4gIHZhciB0YXJnZXRCbG9jayA9IGJsb2NrTWFwLmdldCh0YXJnZXRLZXkpO1xuICB2YXIgdGV4dCA9IHRhcmdldEJsb2NrLmdldFRleHQoKTtcbiAgdmFyIGNoYXJzID0gdGFyZ2V0QmxvY2suZ2V0Q2hhcmFjdGVyTGlzdCgpO1xuICB2YXIgZmluYWxLZXkgPSB0YXJnZXRLZXk7XG4gIHZhciBmaW5hbE9mZnNldCA9IHRhcmdldE9mZnNldCArIGZyYWdtZW50QmxvY2suZ2V0VGV4dCgpLmxlbmd0aDtcblxuICB2YXIgbmV3QmxvY2sgPSB0YXJnZXRCbG9jay5tZXJnZSh7XG4gICAgdGV4dDogdGV4dC5zbGljZSgwLCB0YXJnZXRPZmZzZXQpICsgZnJhZ21lbnRCbG9jay5nZXRUZXh0KCkgKyB0ZXh0LnNsaWNlKHRhcmdldE9mZnNldCksXG4gICAgY2hhcmFjdGVyTGlzdDogaW5zZXJ0SW50b0xpc3QoY2hhcnMsIGZyYWdtZW50QmxvY2suZ2V0Q2hhcmFjdGVyTGlzdCgpLCB0YXJnZXRPZmZzZXQpLFxuICAgIGRhdGE6IGZyYWdtZW50QmxvY2suZ2V0RGF0YSgpXG4gIH0pO1xuXG4gIHJldHVybiBjb250ZW50U3RhdGUubWVyZ2Uoe1xuICAgIGJsb2NrTWFwOiBibG9ja01hcC5zZXQodGFyZ2V0S2V5LCBuZXdCbG9jayksXG4gICAgc2VsZWN0aW9uQmVmb3JlOiBzZWxlY3Rpb25TdGF0ZSxcbiAgICBzZWxlY3Rpb25BZnRlcjogc2VsZWN0aW9uU3RhdGUubWVyZ2Uoe1xuICAgICAgYW5jaG9yS2V5OiBmaW5hbEtleSxcbiAgICAgIGFuY2hvck9mZnNldDogZmluYWxPZmZzZXQsXG4gICAgICBmb2N1c0tleTogZmluYWxLZXksXG4gICAgICBmb2N1c09mZnNldDogZmluYWxPZmZzZXQsXG4gICAgICBpc0JhY2t3YXJkOiBmYWxzZVxuICAgIH0pXG4gIH0pO1xufTtcblxuLyoqXG4gKiBBcHBlbmRzIHRleHQvY2hhcmFjdGVyTGlzdCBmcm9tIHRoZSBmcmFnbWVudCBmaXJzdCBibG9jayB0b1xuICogdGFyZ2V0IGJsb2NrLlxuICovXG52YXIgdXBkYXRlSGVhZCA9IGZ1bmN0aW9uIHVwZGF0ZUhlYWQoYmxvY2ssIHRhcmdldE9mZnNldCwgZnJhZ21lbnQpIHtcbiAgdmFyIHRleHQgPSBibG9jay5nZXRUZXh0KCk7XG4gIHZhciBjaGFycyA9IGJsb2NrLmdldENoYXJhY3Rlckxpc3QoKTtcblxuICAvLyBNb2RpZnkgaGVhZCBwb3J0aW9uIG9mIGJsb2NrLlxuICB2YXIgaGVhZFRleHQgPSB0ZXh0LnNsaWNlKDAsIHRhcmdldE9mZnNldCk7XG4gIHZhciBoZWFkQ2hhcmFjdGVycyA9IGNoYXJzLnNsaWNlKDAsIHRhcmdldE9mZnNldCk7XG4gIHZhciBhcHBlbmRUb0hlYWQgPSBmcmFnbWVudC5maXJzdCgpO1xuXG4gIHJldHVybiBibG9jay5tZXJnZSh7XG4gICAgdGV4dDogaGVhZFRleHQgKyBhcHBlbmRUb0hlYWQuZ2V0VGV4dCgpLFxuICAgIGNoYXJhY3Rlckxpc3Q6IGhlYWRDaGFyYWN0ZXJzLmNvbmNhdChhcHBlbmRUb0hlYWQuZ2V0Q2hhcmFjdGVyTGlzdCgpKSxcbiAgICB0eXBlOiBoZWFkVGV4dCA/IGJsb2NrLmdldFR5cGUoKSA6IGFwcGVuZFRvSGVhZC5nZXRUeXBlKCksXG4gICAgZGF0YTogYXBwZW5kVG9IZWFkLmdldERhdGEoKVxuICB9KTtcbn07XG5cbi8qKlxuICogQXBwZW5kcyBvZmZzZXQgdGV4dC9jaGFyYWN0ZXJMaXN0IGZyb20gdGhlIHRhcmdldCBibG9jayB0byB0aGUgbGFzdFxuICogZnJhZ21lbnQgYmxvY2suXG4gKi9cbnZhciB1cGRhdGVUYWlsID0gZnVuY3Rpb24gdXBkYXRlVGFpbChibG9jaywgdGFyZ2V0T2Zmc2V0LCBmcmFnbWVudCkge1xuICAvLyBNb2RpZnkgdGFpbCBwb3J0aW9uIG9mIGJsb2NrLlxuICB2YXIgdGV4dCA9IGJsb2NrLmdldFRleHQoKTtcbiAgdmFyIGNoYXJzID0gYmxvY2suZ2V0Q2hhcmFjdGVyTGlzdCgpO1xuXG4gIC8vIE1vZGlmeSBoZWFkIHBvcnRpb24gb2YgYmxvY2suXG4gIHZhciBibG9ja1NpemUgPSB0ZXh0Lmxlbmd0aDtcbiAgdmFyIHRhaWxUZXh0ID0gdGV4dC5zbGljZSh0YXJnZXRPZmZzZXQsIGJsb2NrU2l6ZSk7XG4gIHZhciB0YWlsQ2hhcmFjdGVycyA9IGNoYXJzLnNsaWNlKHRhcmdldE9mZnNldCwgYmxvY2tTaXplKTtcbiAgdmFyIHByZXBlbmRUb1RhaWwgPSBmcmFnbWVudC5sYXN0KCk7XG5cbiAgcmV0dXJuIHByZXBlbmRUb1RhaWwubWVyZ2Uoe1xuICAgIHRleHQ6IHByZXBlbmRUb1RhaWwuZ2V0VGV4dCgpICsgdGFpbFRleHQsXG4gICAgY2hhcmFjdGVyTGlzdDogcHJlcGVuZFRvVGFpbC5nZXRDaGFyYWN0ZXJMaXN0KCkuY29uY2F0KHRhaWxDaGFyYWN0ZXJzKSxcbiAgICBkYXRhOiBwcmVwZW5kVG9UYWlsLmdldERhdGEoKVxuICB9KTtcbn07XG5cbnZhciBnZXRSb290QmxvY2tzID0gZnVuY3Rpb24gZ2V0Um9vdEJsb2NrcyhibG9jaywgYmxvY2tNYXApIHtcbiAgdmFyIGhlYWRLZXkgPSBibG9jay5nZXRLZXkoKTtcbiAgdmFyIHJvb3RCbG9jayA9IGJsb2NrO1xuICB2YXIgcm9vdEJsb2NrcyA9IFtdO1xuXG4gIC8vIHNvbWV0aW1lcyB0aGUgZnJhZ21lbnQgaGVhZCBibG9jayB3aWxsIG5vdCBiZSBwYXJ0IG9mIHRoZSBibG9ja01hcCBpdHNlbGYgdGhpcyBjYW4gaGFwcGVuIHdoZW5cbiAgLy8gdGhlIGZyYWdtZW50IGhlYWQgaXMgdXNlZCB0byB1cGRhdGUgdGhlIHRhcmdldCBibG9jaywgaG93ZXZlciB3aGVuIHRoaXMgZG9lcyBub3QgaGFwcGVuIHdlIG5lZWRcbiAgLy8gdG8gbWFrZSBzdXJlIHRoYXQgd2UgaW5jbHVkZSBpdCBvbiB0aGUgcm9vdEJsb2NrcyBzaW5jZSB0aGUgZmlyc3QgYmxvY2sgb2YgYSBmcmFnbWVudCBpcyBhbHdheXMgYVxuICAvLyBmcmFnbWVudCByb290IGJsb2NrXG4gIGlmIChibG9ja01hcC5nZXQoaGVhZEtleSkpIHtcbiAgICByb290QmxvY2tzLnB1c2goaGVhZEtleSk7XG4gIH1cblxuICB3aGlsZSAocm9vdEJsb2NrICYmIHJvb3RCbG9jay5nZXROZXh0U2libGluZ0tleSgpKSB7XG4gICAgdmFyIGxhc3RTaWJsaW5nS2V5ID0gcm9vdEJsb2NrLmdldE5leHRTaWJsaW5nS2V5KCk7XG5cbiAgICBpZiAoIWxhc3RTaWJsaW5nS2V5KSB7XG4gICAgICBicmVhaztcbiAgICB9XG5cbiAgICByb290QmxvY2tzLnB1c2gobGFzdFNpYmxpbmdLZXkpO1xuICAgIHJvb3RCbG9jayA9IGJsb2NrTWFwLmdldChsYXN0U2libGluZ0tleSk7XG4gIH1cblxuICByZXR1cm4gcm9vdEJsb2Nrcztcbn07XG5cbnZhciB1cGRhdGVCbG9ja01hcExpbmtzID0gZnVuY3Rpb24gdXBkYXRlQmxvY2tNYXBMaW5rcyhibG9ja01hcCwgb3JpZ2luYWxCbG9ja01hcCwgdGFyZ2V0QmxvY2ssIGZyYWdtZW50SGVhZEJsb2NrKSB7XG4gIHJldHVybiBibG9ja01hcC53aXRoTXV0YXRpb25zKGZ1bmN0aW9uIChibG9ja01hcFN0YXRlKSB7XG4gICAgdmFyIHRhcmdldEtleSA9IHRhcmdldEJsb2NrLmdldEtleSgpO1xuICAgIHZhciBoZWFkS2V5ID0gZnJhZ21lbnRIZWFkQmxvY2suZ2V0S2V5KCk7XG4gICAgdmFyIHRhcmdldE5leHRLZXkgPSB0YXJnZXRCbG9jay5nZXROZXh0U2libGluZ0tleSgpO1xuICAgIHZhciB0YXJnZXRQYXJlbnRLZXkgPSB0YXJnZXRCbG9jay5nZXRQYXJlbnRLZXkoKTtcbiAgICB2YXIgZnJhZ21lbnRSb290QmxvY2tzID0gZ2V0Um9vdEJsb2NrcyhmcmFnbWVudEhlYWRCbG9jaywgYmxvY2tNYXApO1xuICAgIHZhciBsYXN0Um9vdEZyYWdtZW50QmxvY2tLZXkgPSBmcmFnbWVudFJvb3RCbG9ja3NbZnJhZ21lbnRSb290QmxvY2tzLmxlbmd0aCAtIDFdO1xuXG4gICAgaWYgKGJsb2NrTWFwU3RhdGUuZ2V0KGhlYWRLZXkpKSB7XG4gICAgICAvLyB1cGRhdGUgdGhlIGZyYWdtZW50IGhlYWQgd2hlbiBpdCBpcyBwYXJ0IG9mIHRoZSBibG9ja01hcCBvdGhlcndpc2VcbiAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW3RhcmdldEtleSwgJ25leHRTaWJsaW5nJ10sIGhlYWRLZXkpO1xuICAgICAgYmxvY2tNYXBTdGF0ZS5zZXRJbihbaGVhZEtleSwgJ3ByZXZTaWJsaW5nJ10sIHRhcmdldEtleSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIHVwZGF0ZSB0aGUgdGFyZ2V0IGJsb2NrIHRoYXQgaGFkIHRoZSBmcmFnbWVudCBoZWFkIGNvbnRlbnRzIG1lcmdlZCBpbnRvIGl0XG4gICAgICBibG9ja01hcFN0YXRlLnNldEluKFt0YXJnZXRLZXksICduZXh0U2libGluZyddLCBmcmFnbWVudEhlYWRCbG9jay5nZXROZXh0U2libGluZ0tleSgpKTtcbiAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW2ZyYWdtZW50SGVhZEJsb2NrLmdldE5leHRTaWJsaW5nS2V5KCksICdwcmV2U2libGluZyddLCB0YXJnZXRLZXkpO1xuICAgIH1cblxuICAgIC8vIHVwZGF0ZSB0aGUgbGFzdCByb290IGJsb2NrIGZyYWdtZW50XG4gICAgYmxvY2tNYXBTdGF0ZS5zZXRJbihbbGFzdFJvb3RGcmFnbWVudEJsb2NrS2V5LCAnbmV4dFNpYmxpbmcnXSwgdGFyZ2V0TmV4dEtleSk7XG5cbiAgICAvLyB1cGRhdGUgdGhlIG9yaWdpbmFsIHRhcmdldCBuZXh0IGJsb2NrXG4gICAgaWYgKHRhcmdldE5leHRLZXkpIHtcbiAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW3RhcmdldE5leHRLZXksICdwcmV2U2libGluZyddLCBsYXN0Um9vdEZyYWdtZW50QmxvY2tLZXkpO1xuICAgIH1cblxuICAgIC8vIHVwZGF0ZSBmcmFnbWVudCBwYXJlbnQgbGlua3NcbiAgICBmcmFnbWVudFJvb3RCbG9ja3MuZm9yRWFjaChmdW5jdGlvbiAoYmxvY2tLZXkpIHtcbiAgICAgIHJldHVybiBibG9ja01hcFN0YXRlLnNldEluKFtibG9ja0tleSwgJ3BhcmVudCddLCB0YXJnZXRQYXJlbnRLZXkpO1xuICAgIH0pO1xuXG4gICAgLy8gdXBkYXRlIHRhcmdldEJsb2NrIHBhcmVudCBjaGlsZCBsaW5rc1xuICAgIGlmICh0YXJnZXRQYXJlbnRLZXkpIHtcbiAgICAgIHZhciB0YXJnZXRQYXJlbnQgPSBibG9ja01hcC5nZXQodGFyZ2V0UGFyZW50S2V5KTtcbiAgICAgIHZhciBvcmlnaW5hbFRhcmdldFBhcmVudENoaWxkS2V5cyA9IHRhcmdldFBhcmVudC5nZXRDaGlsZEtleXMoKTtcblxuICAgICAgdmFyIHRhcmdldEJsb2NrSW5kZXggPSBvcmlnaW5hbFRhcmdldFBhcmVudENoaWxkS2V5cy5pbmRleE9mKHRhcmdldEtleSk7XG4gICAgICB2YXIgaW5zZXJ0aW9uSW5kZXggPSB0YXJnZXRCbG9ja0luZGV4ICsgMTtcblxuICAgICAgdmFyIG5ld0NoaWxkcmVuS2V5c0FycmF5ID0gb3JpZ2luYWxUYXJnZXRQYXJlbnRDaGlsZEtleXMudG9BcnJheSgpO1xuXG4gICAgICAvLyBpbnNlcnQgZnJhZ21lbnQgY2hpbGRyZW5cbiAgICAgIG5ld0NoaWxkcmVuS2V5c0FycmF5LnNwbGljZS5hcHBseShuZXdDaGlsZHJlbktleXNBcnJheSwgW2luc2VydGlvbkluZGV4LCAwXS5jb25jYXQoZnJhZ21lbnRSb290QmxvY2tzKSk7XG5cbiAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW3RhcmdldFBhcmVudEtleSwgJ2NoaWxkcmVuJ10sIExpc3QobmV3Q2hpbGRyZW5LZXlzQXJyYXkpKTtcbiAgICB9XG4gIH0pO1xufTtcblxudmFyIGluc2VydEZyYWdtZW50ID0gZnVuY3Rpb24gaW5zZXJ0RnJhZ21lbnQoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSwgYmxvY2tNYXAsIGZyYWdtZW50LCB0YXJnZXRLZXksIHRhcmdldE9mZnNldCkge1xuICB2YXIgaXNUcmVlQmFzZWRCbG9ja01hcCA9IGJsb2NrTWFwLmZpcnN0KCkgaW5zdGFuY2VvZiBDb250ZW50QmxvY2tOb2RlO1xuICB2YXIgbmV3QmxvY2tBcnIgPSBbXTtcbiAgdmFyIGZyYWdtZW50U2l6ZSA9IGZyYWdtZW50LnNpemU7XG4gIHZhciB0YXJnZXQgPSBibG9ja01hcC5nZXQodGFyZ2V0S2V5KTtcbiAgdmFyIGhlYWQgPSBmcmFnbWVudC5maXJzdCgpO1xuICB2YXIgdGFpbCA9IGZyYWdtZW50Lmxhc3QoKTtcbiAgdmFyIGZpbmFsT2Zmc2V0ID0gdGFpbC5nZXRMZW5ndGgoKTtcbiAgdmFyIGZpbmFsS2V5ID0gdGFpbC5nZXRLZXkoKTtcbiAgdmFyIHNob3VsZE5vdFVwZGF0ZUZyb21GcmFnbWVudEJsb2NrID0gaXNUcmVlQmFzZWRCbG9ja01hcCAmJiAoIXRhcmdldC5nZXRDaGlsZEtleXMoKS5pc0VtcHR5KCkgfHwgIWhlYWQuZ2V0Q2hpbGRLZXlzKCkuaXNFbXB0eSgpKTtcblxuICBibG9ja01hcC5mb3JFYWNoKGZ1bmN0aW9uIChibG9jaywgYmxvY2tLZXkpIHtcbiAgICBpZiAoYmxvY2tLZXkgIT09IHRhcmdldEtleSkge1xuICAgICAgbmV3QmxvY2tBcnIucHVzaChibG9jayk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKHNob3VsZE5vdFVwZGF0ZUZyb21GcmFnbWVudEJsb2NrKSB7XG4gICAgICBuZXdCbG9ja0Fyci5wdXNoKGJsb2NrKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbmV3QmxvY2tBcnIucHVzaCh1cGRhdGVIZWFkKGJsb2NrLCB0YXJnZXRPZmZzZXQsIGZyYWdtZW50KSk7XG4gICAgfVxuXG4gICAgLy8gSW5zZXJ0IGZyYWdtZW50IGJsb2NrcyBhZnRlciB0aGUgaGVhZCBhbmQgYmVmb3JlIHRoZSB0YWlsLlxuICAgIGZyYWdtZW50XG4gICAgLy8gd2hlbiB3ZSBhcmUgdXBkYXRpbmcgdGhlIHRhcmdldCBibG9jayB3aXRoIHRoZSBoZWFkIGZyYWdtZW50IGJsb2NrIHdlIHNraXAgdGhlIGZpcnN0IGZyYWdtZW50XG4gICAgLy8gaGVhZCBzaW5jZSBpdHMgY29udGVudHMgaGF2ZSBhbHJlYWR5IGJlZW4gbWVyZ2VkIHdpdGggdGhlIHRhcmdldCBibG9jayBvdGhlcndpc2Ugd2UgaW5jbHVkZVxuICAgIC8vIHRoZSB3aG9sZSBmcmFnbWVudFxuICAgIC5zbGljZShzaG91bGROb3RVcGRhdGVGcm9tRnJhZ21lbnRCbG9jayA/IDAgOiAxLCBmcmFnbWVudFNpemUgLSAxKS5mb3JFYWNoKGZ1bmN0aW9uIChmcmFnbWVudEJsb2NrKSB7XG4gICAgICByZXR1cm4gbmV3QmxvY2tBcnIucHVzaChmcmFnbWVudEJsb2NrKTtcbiAgICB9KTtcblxuICAgIC8vIHVwZGF0ZSB0YWlsXG4gICAgbmV3QmxvY2tBcnIucHVzaCh1cGRhdGVUYWlsKGJsb2NrLCB0YXJnZXRPZmZzZXQsIGZyYWdtZW50KSk7XG4gIH0pO1xuXG4gIHZhciB1cGRhdGVkQmxvY2tNYXAgPSBCbG9ja01hcEJ1aWxkZXIuY3JlYXRlRnJvbUFycmF5KG5ld0Jsb2NrQXJyKTtcblxuICBpZiAoaXNUcmVlQmFzZWRCbG9ja01hcCkge1xuICAgIHVwZGF0ZWRCbG9ja01hcCA9IHVwZGF0ZUJsb2NrTWFwTGlua3ModXBkYXRlZEJsb2NrTWFwLCBibG9ja01hcCwgdGFyZ2V0LCBoZWFkKTtcbiAgfVxuXG4gIHJldHVybiBjb250ZW50U3RhdGUubWVyZ2Uoe1xuICAgIGJsb2NrTWFwOiB1cGRhdGVkQmxvY2tNYXAsXG4gICAgc2VsZWN0aW9uQmVmb3JlOiBzZWxlY3Rpb25TdGF0ZSxcbiAgICBzZWxlY3Rpb25BZnRlcjogc2VsZWN0aW9uU3RhdGUubWVyZ2Uoe1xuICAgICAgYW5jaG9yS2V5OiBmaW5hbEtleSxcbiAgICAgIGFuY2hvck9mZnNldDogZmluYWxPZmZzZXQsXG4gICAgICBmb2N1c0tleTogZmluYWxLZXksXG4gICAgICBmb2N1c09mZnNldDogZmluYWxPZmZzZXQsXG4gICAgICBpc0JhY2t3YXJkOiBmYWxzZVxuICAgIH0pXG4gIH0pO1xufTtcblxudmFyIGluc2VydEZyYWdtZW50SW50b0NvbnRlbnRTdGF0ZSA9IGZ1bmN0aW9uIGluc2VydEZyYWdtZW50SW50b0NvbnRlbnRTdGF0ZShjb250ZW50U3RhdGUsIHNlbGVjdGlvblN0YXRlLCBmcmFnbWVudEJsb2NrTWFwKSB7XG4gICFzZWxlY3Rpb25TdGF0ZS5pc0NvbGxhcHNlZCgpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ2BpbnNlcnRGcmFnbWVudGAgc2hvdWxkIG9ubHkgYmUgY2FsbGVkIHdpdGggYSBjb2xsYXBzZWQgc2VsZWN0aW9uIHN0YXRlLicpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcblxuICB2YXIgYmxvY2tNYXAgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tNYXAoKTtcbiAgdmFyIGZyYWdtZW50ID0gcmFuZG9taXplQmxvY2tNYXBLZXlzKGZyYWdtZW50QmxvY2tNYXApO1xuICB2YXIgdGFyZ2V0S2V5ID0gc2VsZWN0aW9uU3RhdGUuZ2V0U3RhcnRLZXkoKTtcbiAgdmFyIHRhcmdldE9mZnNldCA9IHNlbGVjdGlvblN0YXRlLmdldFN0YXJ0T2Zmc2V0KCk7XG5cbiAgdmFyIHRhcmdldEJsb2NrID0gYmxvY2tNYXAuZ2V0KHRhcmdldEtleSk7XG5cbiAgaWYgKHRhcmdldEJsb2NrIGluc3RhbmNlb2YgQ29udGVudEJsb2NrTm9kZSkge1xuICAgICF0YXJnZXRCbG9jay5nZXRDaGlsZEtleXMoKS5pc0VtcHR5KCkgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAnYGluc2VydEZyYWdtZW50YCBzaG91bGQgbm90IGJlIGNhbGxlZCB3aGVuIGEgY29udGFpbmVyIG5vZGUgaXMgc2VsZWN0ZWQuJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuICB9XG5cbiAgLy8gV2hlbiB3ZSBpbnNlcnQgYSBmcmFnbWVudCB3aXRoIGEgc2luZ2xlIGJsb2NrIHdlIHNpbXBseSB1cGRhdGUgdGhlIHRhcmdldCBibG9ja1xuICAvLyB3aXRoIHRoZSBjb250ZW50cyBvZiB0aGUgaW5zZXJ0ZWQgZnJhZ21lbnQgYmxvY2tcbiAgaWYgKGZyYWdtZW50LnNpemUgPT09IDEpIHtcbiAgICByZXR1cm4gdXBkYXRlRXhpc3RpbmdCbG9jayhjb250ZW50U3RhdGUsIHNlbGVjdGlvblN0YXRlLCBibG9ja01hcCwgZnJhZ21lbnQuZmlyc3QoKSwgdGFyZ2V0S2V5LCB0YXJnZXRPZmZzZXQpO1xuICB9XG5cbiAgcmV0dXJuIGluc2VydEZyYWdtZW50KGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIGJsb2NrTWFwLCBmcmFnbWVudCwgdGFyZ2V0S2V5LCB0YXJnZXRPZmZzZXQpO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBpbnNlcnRGcmFnbWVudEludG9Db250ZW50U3RhdGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFIQTtBQVdBO0FBRUE7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFFQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUhBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/insertFragmentIntoContentState.js
