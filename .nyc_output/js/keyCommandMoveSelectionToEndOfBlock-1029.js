/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule keyCommandMoveSelectionToEndOfBlock
 * @format
 * 
 */


var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");
/**
 * See comment for `moveSelectionToStartOfBlock`.
 */


function keyCommandMoveSelectionToEndOfBlock(editorState) {
  var selection = editorState.getSelection();
  var endKey = selection.getEndKey();
  var content = editorState.getCurrentContent();
  var textLength = content.getBlockForKey(endKey).getLength();
  return EditorState.set(editorState, {
    selection: selection.merge({
      anchorKey: endKey,
      anchorOffset: textLength,
      focusKey: endKey,
      focusOffset: textLength,
      isBackward: false
    }),
    forceSelection: true
  });
}

module.exports = keyCommandMoveSelectionToEndOfBlock;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRNb3ZlU2VsZWN0aW9uVG9FbmRPZkJsb2NrLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRNb3ZlU2VsZWN0aW9uVG9FbmRPZkJsb2NrLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUga2V5Q29tbWFuZE1vdmVTZWxlY3Rpb25Ub0VuZE9mQmxvY2tcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xuXG4vKipcbiAqIFNlZSBjb21tZW50IGZvciBgbW92ZVNlbGVjdGlvblRvU3RhcnRPZkJsb2NrYC5cbiAqL1xuZnVuY3Rpb24ga2V5Q29tbWFuZE1vdmVTZWxlY3Rpb25Ub0VuZE9mQmxvY2soZWRpdG9yU3RhdGUpIHtcbiAgdmFyIHNlbGVjdGlvbiA9IGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpO1xuICB2YXIgZW5kS2V5ID0gc2VsZWN0aW9uLmdldEVuZEtleSgpO1xuICB2YXIgY29udGVudCA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gIHZhciB0ZXh0TGVuZ3RoID0gY29udGVudC5nZXRCbG9ja0ZvcktleShlbmRLZXkpLmdldExlbmd0aCgpO1xuICByZXR1cm4gRWRpdG9yU3RhdGUuc2V0KGVkaXRvclN0YXRlLCB7XG4gICAgc2VsZWN0aW9uOiBzZWxlY3Rpb24ubWVyZ2Uoe1xuICAgICAgYW5jaG9yS2V5OiBlbmRLZXksXG4gICAgICBhbmNob3JPZmZzZXQ6IHRleHRMZW5ndGgsXG4gICAgICBmb2N1c0tleTogZW5kS2V5LFxuICAgICAgZm9jdXNPZmZzZXQ6IHRleHRMZW5ndGgsXG4gICAgICBpc0JhY2t3YXJkOiBmYWxzZVxuICAgIH0pLFxuICAgIGZvcmNlU2VsZWN0aW9uOiB0cnVlXG4gIH0pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGtleUNvbW1hbmRNb3ZlU2VsZWN0aW9uVG9FbmRPZkJsb2NrOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFSQTtBQVVBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/keyCommandMoveSelectionToEndOfBlock.js
