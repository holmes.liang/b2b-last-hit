/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var Cartesian = __webpack_require__(/*! ./Cartesian */ "./node_modules/echarts/lib/coord/cartesian/Cartesian.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


function Cartesian2D(name) {
  Cartesian.call(this, name);
}

Cartesian2D.prototype = {
  constructor: Cartesian2D,
  type: 'cartesian2d',

  /**
   * @type {Array.<string>}
   * @readOnly
   */
  dimensions: ['x', 'y'],

  /**
   * Base axis will be used on stacking.
   *
   * @return {module:echarts/coord/cartesian/Axis2D}
   */
  getBaseAxis: function getBaseAxis() {
    return this.getAxesByScale('ordinal')[0] || this.getAxesByScale('time')[0] || this.getAxis('x');
  },

  /**
   * If contain point
   * @param {Array.<number>} point
   * @return {boolean}
   */
  containPoint: function containPoint(point) {
    var axisX = this.getAxis('x');
    var axisY = this.getAxis('y');
    return axisX.contain(axisX.toLocalCoord(point[0])) && axisY.contain(axisY.toLocalCoord(point[1]));
  },

  /**
   * If contain data
   * @param {Array.<number>} data
   * @return {boolean}
   */
  containData: function containData(data) {
    return this.getAxis('x').containData(data[0]) && this.getAxis('y').containData(data[1]);
  },

  /**
   * @param {Array.<number>} data
   * @param {Array.<number>} out
   * @return {Array.<number>}
   */
  dataToPoint: function dataToPoint(data, reserved, out) {
    var xAxis = this.getAxis('x');
    var yAxis = this.getAxis('y');
    out = out || [];
    out[0] = xAxis.toGlobalCoord(xAxis.dataToCoord(data[0]));
    out[1] = yAxis.toGlobalCoord(yAxis.dataToCoord(data[1]));
    return out;
  },

  /**
   * @param {Array.<number>} data
   * @param {Array.<number>} out
   * @return {Array.<number>}
   */
  clampData: function clampData(data, out) {
    var xScale = this.getAxis('x').scale;
    var yScale = this.getAxis('y').scale;
    var xAxisExtent = xScale.getExtent();
    var yAxisExtent = yScale.getExtent();
    var x = xScale.parse(data[0]);
    var y = yScale.parse(data[1]);
    out = out || [];
    out[0] = Math.min(Math.max(Math.min(xAxisExtent[0], xAxisExtent[1]), x), Math.max(xAxisExtent[0], xAxisExtent[1]));
    out[1] = Math.min(Math.max(Math.min(yAxisExtent[0], yAxisExtent[1]), y), Math.max(yAxisExtent[0], yAxisExtent[1]));
    return out;
  },

  /**
   * @param {Array.<number>} point
   * @param {Array.<number>} out
   * @return {Array.<number>}
   */
  pointToData: function pointToData(point, out) {
    var xAxis = this.getAxis('x');
    var yAxis = this.getAxis('y');
    out = out || [];
    out[0] = xAxis.coordToData(xAxis.toLocalCoord(point[0]));
    out[1] = yAxis.coordToData(yAxis.toLocalCoord(point[1]));
    return out;
  },

  /**
   * Get other axis
   * @param {module:echarts/coord/cartesian/Axis2D} axis
   */
  getOtherAxis: function getOtherAxis(axis) {
    return this.getAxis(axis.dim === 'x' ? 'y' : 'x');
  }
};
zrUtil.inherits(Cartesian2D, Cartesian);
var _default = Cartesian2D;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvY2FydGVzaWFuL0NhcnRlc2lhbjJELmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvY2FydGVzaWFuL0NhcnRlc2lhbjJELmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIENhcnRlc2lhbiA9IHJlcXVpcmUoXCIuL0NhcnRlc2lhblwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuZnVuY3Rpb24gQ2FydGVzaWFuMkQobmFtZSkge1xuICBDYXJ0ZXNpYW4uY2FsbCh0aGlzLCBuYW1lKTtcbn1cblxuQ2FydGVzaWFuMkQucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogQ2FydGVzaWFuMkQsXG4gIHR5cGU6ICdjYXJ0ZXNpYW4yZCcsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtBcnJheS48c3RyaW5nPn1cbiAgICogQHJlYWRPbmx5XG4gICAqL1xuICBkaW1lbnNpb25zOiBbJ3gnLCAneSddLFxuXG4gIC8qKlxuICAgKiBCYXNlIGF4aXMgd2lsbCBiZSB1c2VkIG9uIHN0YWNraW5nLlxuICAgKlxuICAgKiBAcmV0dXJuIHttb2R1bGU6ZWNoYXJ0cy9jb29yZC9jYXJ0ZXNpYW4vQXhpczJEfVxuICAgKi9cbiAgZ2V0QmFzZUF4aXM6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRBeGVzQnlTY2FsZSgnb3JkaW5hbCcpWzBdIHx8IHRoaXMuZ2V0QXhlc0J5U2NhbGUoJ3RpbWUnKVswXSB8fCB0aGlzLmdldEF4aXMoJ3gnKTtcbiAgfSxcblxuICAvKipcbiAgICogSWYgY29udGFpbiBwb2ludFxuICAgKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBwb2ludFxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgY29udGFpblBvaW50OiBmdW5jdGlvbiAocG9pbnQpIHtcbiAgICB2YXIgYXhpc1ggPSB0aGlzLmdldEF4aXMoJ3gnKTtcbiAgICB2YXIgYXhpc1kgPSB0aGlzLmdldEF4aXMoJ3knKTtcbiAgICByZXR1cm4gYXhpc1guY29udGFpbihheGlzWC50b0xvY2FsQ29vcmQocG9pbnRbMF0pKSAmJiBheGlzWS5jb250YWluKGF4aXNZLnRvTG9jYWxDb29yZChwb2ludFsxXSkpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBJZiBjb250YWluIGRhdGFcbiAgICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gZGF0YVxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgY29udGFpbkRhdGE6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0QXhpcygneCcpLmNvbnRhaW5EYXRhKGRhdGFbMF0pICYmIHRoaXMuZ2V0QXhpcygneScpLmNvbnRhaW5EYXRhKGRhdGFbMV0pO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBkYXRhXG4gICAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IG91dFxuICAgKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPn1cbiAgICovXG4gIGRhdGFUb1BvaW50OiBmdW5jdGlvbiAoZGF0YSwgcmVzZXJ2ZWQsIG91dCkge1xuICAgIHZhciB4QXhpcyA9IHRoaXMuZ2V0QXhpcygneCcpO1xuICAgIHZhciB5QXhpcyA9IHRoaXMuZ2V0QXhpcygneScpO1xuICAgIG91dCA9IG91dCB8fCBbXTtcbiAgICBvdXRbMF0gPSB4QXhpcy50b0dsb2JhbENvb3JkKHhBeGlzLmRhdGFUb0Nvb3JkKGRhdGFbMF0pKTtcbiAgICBvdXRbMV0gPSB5QXhpcy50b0dsb2JhbENvb3JkKHlBeGlzLmRhdGFUb0Nvb3JkKGRhdGFbMV0pKTtcbiAgICByZXR1cm4gb3V0O1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBkYXRhXG4gICAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IG91dFxuICAgKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPn1cbiAgICovXG4gIGNsYW1wRGF0YTogZnVuY3Rpb24gKGRhdGEsIG91dCkge1xuICAgIHZhciB4U2NhbGUgPSB0aGlzLmdldEF4aXMoJ3gnKS5zY2FsZTtcbiAgICB2YXIgeVNjYWxlID0gdGhpcy5nZXRBeGlzKCd5Jykuc2NhbGU7XG4gICAgdmFyIHhBeGlzRXh0ZW50ID0geFNjYWxlLmdldEV4dGVudCgpO1xuICAgIHZhciB5QXhpc0V4dGVudCA9IHlTY2FsZS5nZXRFeHRlbnQoKTtcbiAgICB2YXIgeCA9IHhTY2FsZS5wYXJzZShkYXRhWzBdKTtcbiAgICB2YXIgeSA9IHlTY2FsZS5wYXJzZShkYXRhWzFdKTtcbiAgICBvdXQgPSBvdXQgfHwgW107XG4gICAgb3V0WzBdID0gTWF0aC5taW4oTWF0aC5tYXgoTWF0aC5taW4oeEF4aXNFeHRlbnRbMF0sIHhBeGlzRXh0ZW50WzFdKSwgeCksIE1hdGgubWF4KHhBeGlzRXh0ZW50WzBdLCB4QXhpc0V4dGVudFsxXSkpO1xuICAgIG91dFsxXSA9IE1hdGgubWluKE1hdGgubWF4KE1hdGgubWluKHlBeGlzRXh0ZW50WzBdLCB5QXhpc0V4dGVudFsxXSksIHkpLCBNYXRoLm1heCh5QXhpc0V4dGVudFswXSwgeUF4aXNFeHRlbnRbMV0pKTtcbiAgICByZXR1cm4gb3V0O1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBwb2ludFxuICAgKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBvdXRcbiAgICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59XG4gICAqL1xuICBwb2ludFRvRGF0YTogZnVuY3Rpb24gKHBvaW50LCBvdXQpIHtcbiAgICB2YXIgeEF4aXMgPSB0aGlzLmdldEF4aXMoJ3gnKTtcbiAgICB2YXIgeUF4aXMgPSB0aGlzLmdldEF4aXMoJ3knKTtcbiAgICBvdXQgPSBvdXQgfHwgW107XG4gICAgb3V0WzBdID0geEF4aXMuY29vcmRUb0RhdGEoeEF4aXMudG9Mb2NhbENvb3JkKHBvaW50WzBdKSk7XG4gICAgb3V0WzFdID0geUF4aXMuY29vcmRUb0RhdGEoeUF4aXMudG9Mb2NhbENvb3JkKHBvaW50WzFdKSk7XG4gICAgcmV0dXJuIG91dDtcbiAgfSxcblxuICAvKipcbiAgICogR2V0IG90aGVyIGF4aXNcbiAgICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9jb29yZC9jYXJ0ZXNpYW4vQXhpczJEfSBheGlzXG4gICAqL1xuICBnZXRPdGhlckF4aXM6IGZ1bmN0aW9uIChheGlzKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0QXhpcyhheGlzLmRpbSA9PT0gJ3gnID8gJ3knIDogJ3gnKTtcbiAgfVxufTtcbnpyVXRpbC5pbmhlcml0cyhDYXJ0ZXNpYW4yRCwgQ2FydGVzaWFuKTtcbnZhciBfZGVmYXVsdCA9IENhcnRlc2lhbjJEO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQTNGQTtBQTZGQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/cartesian/Cartesian2D.js
