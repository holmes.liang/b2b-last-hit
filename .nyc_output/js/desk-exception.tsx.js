__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _index_less__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./index.less */ "./src/app/desk/component/exception/index.less");
/* harmony import */ var _index_less__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_index_less__WEBPACK_IMPORTED_MODULE_8__);






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/exception/desk-exception.tsx";


// @ts-ignore

var configs = {
  403: {
    img: "https://gw.alipayobjects.com/zos/rmsportal/wZcnGqRDyhPOEYFcZDnb.svg",
    title: "403",
    desc: "Sorry, you have no rights to access this page."
  },
  404: {
    img: "https://gw.alipayobjects.com/zos/rmsportal/KpnpchXsobRgLElEozzI.svg",
    title: "404",
    desc: "Sorry, page not found."
  },
  500: {
    img: "https://gw.alipayobjects.com/zos/rmsportal/RVRUAYdCGeYNBWoKiIwB.svg",
    title: "500",
    desc: "Sorry, service is unavaliable."
  }
};

var DeskException =
/*#__PURE__*/
function (_Widget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(DeskException, _Widget);

  function DeskException(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, DeskException);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(DeskException).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(DeskException, [{
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          type = _this$props.type,
          title = _this$props.title,
          desc = _this$props.desc,
          img = _this$props.img,
          actions = _this$props.actions,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["type", "title", "desc", "img", "actions"]);

      var pageType = type in configs ? type : "404";
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", Object.assign({
        className: _index_less__WEBPACK_IMPORTED_MODULE_8___default.a.exception
      }, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 56
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        className: _index_less__WEBPACK_IMPORTED_MODULE_8___default.a.imgBlock,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        className: _index_less__WEBPACK_IMPORTED_MODULE_8___default.a.imgEle,
        style: {
          backgroundImage: "url(".concat(img || configs[pageType], ")")
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        className: _index_less__WEBPACK_IMPORTED_MODULE_8___default.a.content,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 63
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("h1", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        },
        __self: this
      }, title || configs[pageType].title), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        className: _index_less__WEBPACK_IMPORTED_MODULE_8___default.a.desc,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        },
        __self: this
      }, configs[pageType].desc)));
    }
  }]);

  return DeskException;
}(_component__WEBPACK_IMPORTED_MODULE_7__["Widget"]);

/* harmony default export */ __webpack_exports__["default"] = (DeskException);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2V4Y2VwdGlvbi9kZXNrLWV4Y2VwdGlvbi50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvZXhjZXB0aW9uL2Rlc2stZXhjZXB0aW9uLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCIuLi8uLi8uLi8uLi9jb21tb24vM3JkXCI7XG5pbXBvcnQgeyBXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG4vLyBAdHMtaWdub3JlXG5pbXBvcnQgc3R5bGVzIGZyb20gXCIuL2luZGV4Lmxlc3NcIjtcblxuZXhwb3J0IHR5cGUgTkV4Y2VwdGlvblByb3BzID0ge1xuICB0eXBlPzogXCI0MDNcIiB8IFwiNDA0XCIgfCBcIjUwMFwiO1xuICB0aXRsZT86IFJlYWN0LlJlYWN0Tm9kZTtcbiAgZGVzYz86IFJlYWN0LlJlYWN0Tm9kZTtcbiAgaW1nPzogc3RyaW5nO1xuICBhY3Rpb25zPzogUmVhY3QuUmVhY3ROb2RlO1xuICBsaW5rRWxlbWVudD86IFJlYWN0LlJlYWN0Tm9kZTtcbiAgc3R5bGU/OiBSZWFjdC5DU1NQcm9wZXJ0aWVzO1xufSAmIFdpZGdldFByb3BzO1xuXG5pbnRlcmZhY2UgRXhjZXB0aW9uQ29uZmlnIHtcbiAgaW1nOiBzdHJpbmc7XG4gIHRpdGxlOiBzdHJpbmc7XG4gIGRlc2M6IHN0cmluZztcbn1cblxubGV0IGNvbmZpZ3M6IHsgW2tleTogc3RyaW5nXTogRXhjZXB0aW9uQ29uZmlnOyB9ID0ge1xuICA0MDM6IHtcbiAgICBpbWc6IFwiaHR0cHM6Ly9ndy5hbGlwYXlvYmplY3RzLmNvbS96b3Mvcm1zcG9ydGFsL3daY25HcVJEeWhQT0VZRmNaRG5iLnN2Z1wiLFxuICAgIHRpdGxlOiBcIjQwM1wiLFxuICAgIGRlc2M6IFwiU29ycnksIHlvdSBoYXZlIG5vIHJpZ2h0cyB0byBhY2Nlc3MgdGhpcyBwYWdlLlwiLFxuICB9LFxuICA0MDQ6IHtcbiAgICBpbWc6IFwiaHR0cHM6Ly9ndy5hbGlwYXlvYmplY3RzLmNvbS96b3Mvcm1zcG9ydGFsL0twbnBjaFhzb2JSZ0xFbEVvenpJLnN2Z1wiLFxuICAgIHRpdGxlOiBcIjQwNFwiLFxuICAgIGRlc2M6IFwiU29ycnksIHBhZ2Ugbm90IGZvdW5kLlwiLFxuICB9LFxuICA1MDA6IHtcbiAgICBpbWc6IFwiaHR0cHM6Ly9ndy5hbGlwYXlvYmplY3RzLmNvbS96b3Mvcm1zcG9ydGFsL1JWUlVBWWRDR2VZTkJXb0tpSXdCLnN2Z1wiLFxuICAgIHRpdGxlOiBcIjUwMFwiLFxuICAgIGRlc2M6IFwiU29ycnksIHNlcnZpY2UgaXMgdW5hdmFsaWFibGUuXCIsXG4gIH0sXG59O1xuXG5cbmNsYXNzIERlc2tFeGNlcHRpb248UCBleHRlbmRzIE5FeGNlcHRpb25Qcm9wcywgUywgQz4gZXh0ZW5kcyBXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogTkV4Y2VwdGlvblByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7fSBhcyBDO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgdHlwZSwgdGl0bGUsIGRlc2MsIGltZywgYWN0aW9ucywgLi4ucmVzdCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBwYWdlVHlwZSA9ICh0eXBlIGluIGNvbmZpZ3MgPyB0eXBlIDogXCI0MDRcIikgYXMgc3RyaW5nO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMuZXhjZXB0aW9ufSB7Li4ucmVzdH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMuaW1nQmxvY2t9PlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGNsYXNzTmFtZT17c3R5bGVzLmltZ0VsZX1cbiAgICAgICAgICAgIHN0eWxlPXt7IGJhY2tncm91bmRJbWFnZTogYHVybCgke2ltZyB8fCBjb25maWdzW3BhZ2VUeXBlXX0pYCB9fVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGVzLmNvbnRlbnR9PlxuICAgICAgICAgIDxoMT57dGl0bGUgfHwgY29uZmlnc1twYWdlVHlwZV0udGl0bGV9PC9oMT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGVzLmRlc2N9Pntjb25maWdzW3BhZ2VUeXBlXS5kZXNjfTwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgRGVza0V4Y2VwdGlvbjsiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBWEE7QUFDQTtBQWtCQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBOzs7O0FBM0JBO0FBQ0E7QUE2QkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/exception/desk-exception.tsx
