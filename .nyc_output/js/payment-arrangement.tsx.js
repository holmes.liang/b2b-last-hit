__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formItemLayout", function() { return formItemLayout; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _desk_quote_components_panyment_arramgement_component_operate_arrangement__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk/quote/components/panyment-arramgement/component/operate-arrangement */ "./src/app/desk/quote/components/panyment-arramgement/component/operate-arrangement.tsx");
/* harmony import */ var _component_view_arrangement__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./component/view-arrangement */ "./src/app/desk/quote/components/panyment-arramgement/component/view-arrangement.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/components/panyment-arramgement/payment-arrangement.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        .arrange-row {\n          .arrange-btn {\n            margin-right: 15px;\n          }\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}












var formItemLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};
var itemLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 24
    }
  }
};

var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_8__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();

var PaymentArrangement =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(PaymentArrangement, _ModelWidget);

  function PaymentArrangement(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, PaymentArrangement);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(PaymentArrangement).call(this, props, context));

    _this.operateArrange = function () {
      var option = _this.state.planOptions.find(function (item) {
        return item.code === "OTHERS";
      });

      var oldModel = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.cloneDeep(_this.props.model);

      var details = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(oldModel, _this.getProp("policy.paymentMethod.installment.details")) || [];
      _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_15__["default"].create({
        Component: function Component(_ref) {
          var onCancel = _ref.onCancel,
              onOk = _ref.onOk;
          return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(_desk_quote_components_panyment_arramgement_component_operate_arrangement__WEBPACK_IMPORTED_MODULE_16__["default"], {
            onCancel: onCancel,
            onOK: onOk,
            model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied({
              details: details
            }),
            oldModel: oldModel,
            min: lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(option, "minInstallments") || 1,
            max: lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(option, "maxInstallments") || 1,
            onSave: _this.onSave.bind(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this)),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 185
            },
            __self: this
          });
        }
      });
    };

    _this.viewArrange = function () {
      var oldModel = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.cloneDeep(_this.props.model);

      var details = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(oldModel, _this.getProp("policy.paymentMethod.installment.details")) || [];
      _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_15__["default"].create({
        Component: function Component(_ref2) {
          var onCancel = _ref2.onCancel,
              onOk = _ref2.onOk;
          return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(_component_view_arrangement__WEBPACK_IMPORTED_MODULE_17__["default"], {
            onCancel: onCancel,
            onOK: onOk,
            model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied({
              details: details
            }),
            dataFixed: "policy",
            oldModel: oldModel,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 203
            },
            __self: this
          });
        }
      });
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(PaymentArrangement, [{
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(PaymentArrangement.prototype), "initState", this).call(this), {
        planOptions: []
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      var layout = this.props.layout;
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_12__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var paymentPlan = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(this.props.model, this.getProp("policy.paymentMethod.installment.paymentPlan"));

      if (!paymentPlan) {
        this.setValueToModel("LUMPSUM", this.getProp("policy.paymentMethod.installment.paymentPlan"));
        this.setValueToModel("LUMPSUM", this.getProp("policy.paymentMethod.method"));
        this.getPlanDetails();
      }

      var model = this.props.model;

      var policyId = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, this.getProp("policy.policyId"));

      var policy = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, this.getProp("policy")) || {};
      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_10__["Apis"].PAYMENT_PLAN.replace(":policyId", policyId), policy).then(function (res) {
        var data = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(res, "body.respData") || [];

        _this2.setState({
          planOptions: data
        });
      });
    } // render

  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var C = this.getComponents();
      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          layout = _this$props.layout;

      var policyId = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, this.getProp("policy.policyId"));

      var policy = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, this.getProp("policy")) || {};

      var plan = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, this.getProp("policy.paymentMethod.installment.paymentPlan"));

      return _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(C.BoxContent, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_14__["NSelect"], {
        style: {
          width: "100%"
        },
        layoutCol: layout,
        size: "large",
        form: form,
        label: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Payment Plan").thai("Payment Plan").getMessage(),
        model: model,
        propName: this.getProp("policy.paymentMethod.installment.paymentPlan"),
        fetchOptions: _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_10__["Apis"].PAYMENT_PLAN.replace(":policyId", policyId), policy),
        filter: function filter(options) {
          return options.map(function (item) {
            return {
              id: item.code,
              text: item.name
            };
          });
        },
        onChange: function onChange(value) {
          if (value === "LUMPSUM") {
            _this3.setValueToModel("LUMPSUM", _this3.getProp("policy.paymentMethod.method"));
          } else {
            _this3.setValueToModel("INSTALLMENT", _this3.getProp("policy.paymentMethod.method"));
          }

          _this3.getPlanDetails();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 106
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Row"], {
        className: "arrange-row",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Col"], {
        xs: 8,
        sm: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 138
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Col"], {
        xs: 10,
        sm: 10,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      }, plan === "OTHERS" && _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("a", {
        className: "arrange-btn",
        onClick: function onClick() {
          return _this3.operateArrange();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }, "Arrange"), plan !== "LUMPSUM" && _common_3rd__WEBPACK_IMPORTED_MODULE_12__["React"].createElement("a", {
        onClick: this.viewArrange,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 147
        },
        __self: this
      }, "View payment plan"))));
    } // actions

  }, {
    key: "getPlanDetails",
    value: function getPlanDetails() {
      var _this4 = this;

      var model = this.props.model;
      var policy = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, this.getProp("policy")) || {};
      var policyId = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, this.getProp("policy.policyId")) || {};

      lodash__WEBPACK_IMPORTED_MODULE_9___default.a.set(policy, this.getProp("paymentMethod.installment.details"), []);

      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_10__["Apis"].PAYMENT_PLAN_DETAILS.replace(":policyId", policyId), policy).then(function (res) {
        var data = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(res, "body.respData") || [];

        _this4.setValueToModel(data, _this4.getProp("policy.paymentMethod.installment.details"));
      });
    }
  }, {
    key: "getProp",
    // methods
    value: function getProp(propName) {
      return propName;
    }
  }, {
    key: "onSave",
    value: function onSave(details) {
      this.setValueToModel(details, this.getProp("policy.paymentMethod.installment.details"));
    }
  }]);

  return PaymentArrangement;
}(_component__WEBPACK_IMPORTED_MODULE_11__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (PaymentArrangement);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcG9uZW50cy9wYW55bWVudC1hcnJhbWdlbWVudC9wYXltZW50LWFycmFuZ2VtZW50LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL2NvbXBvbmVudHMvcGFueW1lbnQtYXJyYW1nZW1lbnQvcGF5bWVudC1hcnJhbmdlbWVudC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFRoZW1lIGZyb20gXCJAc3R5bGVzXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBBamF4LCBBcGlzLCBMYW5ndWFnZSwgUnVsZXMsIFN0b3JhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCB7IEFqYXhSZXNwb25zZSwgTW9kZWxXaWRnZXRQcm9wcyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IEZvcm1Db21wb25lbnRQcm9wcyB9IGZyb20gXCJhbnRkL2xpYi9mb3JtXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCwgTkZvcm1JdGVtIH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IEJ1dHRvbiwgQ29sLCBGb3JtLCBJY29uLCBJbnB1dCwgTW9kYWwsIFJvdywgU3BpbiwgVXBsb2FkIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IE5TZWxlY3QsIE5UZXh0IH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgTWFzayBmcm9tIFwiQGRlc2stY29tcG9uZW50L2NyZWF0ZS1kaWFsb2dcIjtcbmltcG9ydCBPcGVyYXRlQXJyYW5nZW1lbnQgZnJvbSBcIkBkZXNrL3F1b3RlL2NvbXBvbmVudHMvcGFueW1lbnQtYXJyYW1nZW1lbnQvY29tcG9uZW50L29wZXJhdGUtYXJyYW5nZW1lbnRcIjtcbmltcG9ydCBWaWV3QXJyYW5nZW1lbnQgZnJvbSBcIi4vY29tcG9uZW50L3ZpZXctYXJyYW5nZW1lbnRcIjtcbmltcG9ydCB7IE1vZGVsbGVyIH0gZnJvbSBcIkBtb2RlbFwiO1xuXG5leHBvcnQgY29uc3QgZm9ybUl0ZW1MYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogOCB9LFxuICAgIHNtOiB7IHNwYW46IDYgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDE2IH0sXG4gICAgc206IHsgc3BhbjogMTMgfSxcbiAgfSxcbn07XG5cbmNvbnN0IGl0ZW1MYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogOCB9LFxuICAgIHNtOiB7IHNwYW46IDYgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDE2IH0sXG4gICAgc206IHsgc3BhbjogMjQgfSxcbiAgfSxcbn07XG5jb25zdCB7IENPTE9SX0EgfSA9IFRoZW1lLmdldFRoZW1lKCk7XG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG50eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xudHlwZSBJQ29tcG9uZW50cyA9IHtcbiAgQm94Q29udGVudDogU3R5bGVkRElWO1xufTtcblxudHlwZSBhcnJhbmdlbWVudFN0YXRlID0ge1xuICBwbGFuT3B0aW9uczogYW55O1xufTtcblxuZXhwb3J0IHR5cGUgYXJyYW5nZW1lbnRQcm9wcyA9IHtcbiAgbW9kZWw6IGFueTtcbiAgZm9ybTogYW55O1xuICBsYXlvdXQ/OiBhbnk7XG59ICYgTW9kZWxXaWRnZXRQcm9wcztcblxuY2xhc3MgUGF5bWVudEFycmFuZ2VtZW50PFAgZXh0ZW5kcyBhcnJhbmdlbWVudFByb3BzLCBTIGV4dGVuZHMgYXJyYW5nZW1lbnRTdGF0ZSwgQyBleHRlbmRzIElDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IGFycmFuZ2VtZW50UHJvcHMsIGNvbnRleHQ/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICBwbGFuT3B0aW9uczogW10sXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICBjb25zdCB7IGxheW91dCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4ge1xuICAgICAgQm94Q29udGVudDogU3R5bGVkLmRpdmBcbiAgICAgICAgLmFycmFuZ2Utcm93IHtcbiAgICAgICAgICAuYXJyYW5nZS1idG4ge1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBjb25zdCBwYXltZW50UGxhbiA9IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIHRoaXMuZ2V0UHJvcChcInBvbGljeS5wYXltZW50TWV0aG9kLmluc3RhbGxtZW50LnBheW1lbnRQbGFuXCIpKTtcbiAgICBpZiAoIXBheW1lbnRQbGFuKSB7XG4gICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChcIkxVTVBTVU1cIiwgdGhpcy5nZXRQcm9wKFwicG9saWN5LnBheW1lbnRNZXRob2QuaW5zdGFsbG1lbnQucGF5bWVudFBsYW5cIikpO1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoXCJMVU1QU1VNXCIsIHRoaXMuZ2V0UHJvcChcInBvbGljeS5wYXltZW50TWV0aG9kLm1ldGhvZFwiKSk7XG4gICAgICB0aGlzLmdldFBsYW5EZXRhaWxzKCk7XG4gICAgfVxuICAgIGNvbnN0IHsgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgcG9saWN5SWQgPSBfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwicG9saWN5LnBvbGljeUlkXCIpKTtcbiAgICBjb25zdCBwb2xpY3kgPSBfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwicG9saWN5XCIpKSB8fCB7fTtcbiAgICBBamF4LnBvc3QoQXBpcy5QQVlNRU5UX1BMQU4ucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBwb2xpY3lJZCksIHBvbGljeSlcbiAgICAgIC50aGVuKChyZXM6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICBjb25zdCBkYXRhID0gXy5nZXQocmVzLCBcImJvZHkucmVzcERhdGFcIikgfHwgW107XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIHBsYW5PcHRpb25zOiBkYXRhLFxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICB9XG5cbiAgLy8gcmVuZGVyXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgeyBtb2RlbCwgZm9ybSwgbGF5b3V0IH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHBvbGljeUlkID0gXy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcInBvbGljeS5wb2xpY3lJZFwiKSk7XG4gICAgY29uc3QgcG9saWN5ID0gXy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcInBvbGljeVwiKSkgfHwge307XG4gICAgY29uc3QgcGxhbiA9IF8uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJwb2xpY3kucGF5bWVudE1ldGhvZC5pbnN0YWxsbWVudC5wYXltZW50UGxhblwiKSk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDLkJveENvbnRlbnQ+XG4gICAgICAgIDxOU2VsZWN0XG4gICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIixcbiAgICAgICAgICB9fVxuICAgICAgICAgIGxheW91dENvbD17bGF5b3V0fVxuICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlBheW1lbnQgUGxhblwiKVxuICAgICAgICAgICAgLnRoYWkoXCJQYXltZW50IFBsYW5cIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdldFByb3AoXCJwb2xpY3kucGF5bWVudE1ldGhvZC5pbnN0YWxsbWVudC5wYXltZW50UGxhblwiKX1cbiAgICAgICAgICBmZXRjaE9wdGlvbnM9e0FqYXgucG9zdChBcGlzLlBBWU1FTlRfUExBTi5yZXBsYWNlKFwiOnBvbGljeUlkXCIsIHBvbGljeUlkKSwgcG9saWN5KX1cbiAgICAgICAgICBmaWx0ZXI9eyhvcHRpb25zOiBhbnkpID0+IHtcbiAgICAgICAgICAgIHJldHVybiBvcHRpb25zLm1hcCgoaXRlbTogYW55KSA9PiB7XG4gICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgaWQ6IGl0ZW0uY29kZSxcbiAgICAgICAgICAgICAgICB0ZXh0OiBpdGVtLm5hbWUsXG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9fVxuICAgICAgICAgIG9uQ2hhbmdlPXsodmFsdWU6IGFueSkgPT4ge1xuICAgICAgICAgICAgaWYgKHZhbHVlID09PSBcIkxVTVBTVU1cIikge1xuICAgICAgICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChcIkxVTVBTVU1cIiwgdGhpcy5nZXRQcm9wKFwicG9saWN5LnBheW1lbnRNZXRob2QubWV0aG9kXCIpKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKFwiSU5TVEFMTE1FTlRcIiwgdGhpcy5nZXRQcm9wKFwicG9saWN5LnBheW1lbnRNZXRob2QubWV0aG9kXCIpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuZ2V0UGxhbkRldGFpbHMoKTtcbiAgICAgICAgICB9fVxuICAgICAgICAvPlxuXG4gICAgICAgIDxSb3cgY2xhc3NOYW1lPXtcImFycmFuZ2Utcm93XCJ9PlxuICAgICAgICAgIDxDb2wgeHM9ezh9IHNtPXs2fS8+XG4gICAgICAgICAgPENvbCB4cz17MTB9IHNtPXsxMH0+XG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHBsYW4gPT09IFwiT1RIRVJTXCIgJiYgPGEgY2xhc3NOYW1lPXtcImFycmFuZ2UtYnRuXCJ9IG9uQ2xpY2s9eygpID0+IHRoaXMub3BlcmF0ZUFycmFuZ2UoKX0+XG4gICAgICAgICAgICAgICAgQXJyYW5nZVxuICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHBsYW4gIT09IFwiTFVNUFNVTVwiICYmXG4gICAgICAgICAgICAgIDxhIG9uQ2xpY2s9e3RoaXMudmlld0FycmFuZ2V9PlxuICAgICAgICAgICAgICAgIFZpZXcgcGF5bWVudCBwbGFuXG4gICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICA8L0NvbD5cbiAgICAgICAgPC9Sb3c+XG5cbiAgICAgICAgey8qeyovfVxuICAgICAgICB7LyogIHBsYW4gIT09IFwiTFVNUFNVTVwiICYmIDxSb3c+Ki99XG4gICAgICAgIHsvKiAgICA8Q29sIHhzPXs4fSBzbT17Nn0vPiovfVxuICAgICAgICB7LyogICAgPENvbCB4cz17MTB9IHNtPXsxMH0+Ki99XG4gICAgICAgIHsvKiAgICAgICovfVxuICAgICAgICB7LyogICAgPC9Db2w+Ki99XG4gICAgICAgIHsvKiAgPC9Sb3c+Ki99XG4gICAgICAgIHsvKn0qL31cblxuICAgICAgPC9DLkJveENvbnRlbnQ+XG4gICAgKTtcbiAgfVxuXG4gIC8vIGFjdGlvbnNcbiAgZ2V0UGxhbkRldGFpbHMoKSB7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBwb2xpY3kgPSBfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwicG9saWN5XCIpKSB8fCB7fTtcbiAgICBjb25zdCBwb2xpY3lJZCA9IF8uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJwb2xpY3kucG9saWN5SWRcIikpIHx8IHt9O1xuICAgIF8uc2V0KHBvbGljeSwgdGhpcy5nZXRQcm9wKFwicGF5bWVudE1ldGhvZC5pbnN0YWxsbWVudC5kZXRhaWxzXCIpLCBbXSk7XG4gICAgQWpheC5wb3N0KEFwaXMuUEFZTUVOVF9QTEFOX0RFVEFJTFMucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBwb2xpY3lJZCksIHBvbGljeSlcbiAgICAgIC50aGVuKChyZXM6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICBjb25zdCBkYXRhID0gXy5nZXQocmVzLCBcImJvZHkucmVzcERhdGFcIikgfHwgW107XG4gICAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKGRhdGEsIHRoaXMuZ2V0UHJvcChcInBvbGljeS5wYXltZW50TWV0aG9kLmluc3RhbGxtZW50LmRldGFpbHNcIikpO1xuICAgICAgfSk7XG4gIH1cblxuICBvcGVyYXRlQXJyYW5nZSA9ICgpID0+IHtcbiAgICBjb25zdCBvcHRpb24gPSB0aGlzLnN0YXRlLnBsYW5PcHRpb25zLmZpbmQoKGl0ZW06IGFueSkgPT4gaXRlbS5jb2RlID09PSBcIk9USEVSU1wiKTtcbiAgICBjb25zdCBvbGRNb2RlbCA9IF8uY2xvbmVEZWVwKHRoaXMucHJvcHMubW9kZWwpO1xuICAgIGNvbnN0IGRldGFpbHMgPSBfLmdldChvbGRNb2RlbCwgdGhpcy5nZXRQcm9wKFwicG9saWN5LnBheW1lbnRNZXRob2QuaW5zdGFsbG1lbnQuZGV0YWlsc1wiKSkgfHwgW107XG4gICAgTWFzay5jcmVhdGUoe1xuICAgICAgQ29tcG9uZW50OiAoeyBvbkNhbmNlbCwgb25PayB9OiBhbnkpID0+IDxPcGVyYXRlQXJyYW5nZW1lbnRcbiAgICAgICAgb25DYW5jZWw9e29uQ2FuY2VsfVxuICAgICAgICBvbk9LPXtvbk9rfVxuICAgICAgICBtb2RlbD17TW9kZWxsZXIuYXNQcm94aWVkKHtcbiAgICAgICAgICBkZXRhaWxzLFxuICAgICAgICB9KX1cbiAgICAgICAgb2xkTW9kZWw9e29sZE1vZGVsfVxuICAgICAgICBtaW49e18uZ2V0KG9wdGlvbiwgXCJtaW5JbnN0YWxsbWVudHNcIikgfHwgMX1cbiAgICAgICAgbWF4PXtfLmdldChvcHRpb24sIFwibWF4SW5zdGFsbG1lbnRzXCIpIHx8IDF9XG4gICAgICAgIG9uU2F2ZT17dGhpcy5vblNhdmUuYmluZCh0aGlzKX1cbiAgICAgIC8+LFxuICAgIH0pO1xuICB9O1xuXG4gIHZpZXdBcnJhbmdlID0gKCkgPT4ge1xuICAgIGNvbnN0IG9sZE1vZGVsID0gXy5jbG9uZURlZXAodGhpcy5wcm9wcy5tb2RlbCk7XG4gICAgY29uc3QgZGV0YWlscyA9IF8uZ2V0KG9sZE1vZGVsLCB0aGlzLmdldFByb3AoXCJwb2xpY3kucGF5bWVudE1ldGhvZC5pbnN0YWxsbWVudC5kZXRhaWxzXCIpKSB8fCBbXTtcbiAgICBNYXNrLmNyZWF0ZSh7XG4gICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsLCBvbk9rIH06IGFueSkgPT4gPFZpZXdBcnJhbmdlbWVudFxuICAgICAgICBvbkNhbmNlbD17b25DYW5jZWx9XG4gICAgICAgIG9uT0s9e29uT2t9XG4gICAgICAgIG1vZGVsPXtNb2RlbGxlci5hc1Byb3hpZWQoe1xuICAgICAgICAgIGRldGFpbHMsXG4gICAgICAgIH0pfVxuICAgICAgICBkYXRhRml4ZWQ9e1wicG9saWN5XCJ9XG4gICAgICAgIG9sZE1vZGVsPXtvbGRNb2RlbH1cbiAgICAgIC8+LFxuICAgIH0pO1xuICB9O1xuXG4gIC8vIG1ldGhvZHNcbiAgZ2V0UHJvcChwcm9wTmFtZTogYW55KSB7XG4gICAgcmV0dXJuIHByb3BOYW1lO1xuICB9XG5cbiAgb25TYXZlKGRldGFpbHM6IGFueSkge1xuICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKGRldGFpbHMsIHRoaXMuZ2V0UHJvcChcInBvbGljeS5wYXltZW50TWV0aG9kLmluc3RhbGxtZW50LmRldGFpbHNcIikpO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFBheW1lbnRBcnJhbmdlbWVudDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFMQTtBQVdBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFMQTtBQUNBO0FBU0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQWVBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUE4SEE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQWFBO0FBQ0E7QUEvSUE7QUFpSkE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVdBO0FBQ0E7QUEvSkE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQURBO0FBU0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUNBO0FBT0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQTVCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUErQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFrQkE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUFxQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7OztBQXhLQTtBQUNBO0FBMEtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/components/panyment-arramgement/payment-arrangement.tsx
