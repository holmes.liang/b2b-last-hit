__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ProgressDialog; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/ph/progress.tsx";






var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_7__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

var ProgressDialog =
/*#__PURE__*/
function (_React$Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(ProgressDialog, _React$Component);

  function ProgressDialog() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, ProgressDialog);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(ProgressDialog)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.interval = void 0;
    _this.smoothlyAddInterval = void 0;
    _this.usedtime = 0;
    _this.state = {
      progressNum: 0,
      isRes: false,
      isError: ""
    };
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(ProgressDialog, [{
    key: "isApiOver",
    value: function isApiOver(postData, overAction) {
      var _this2 = this;

      this.setState({
        isRes: false,
        isError: ""
      });
      _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].get("/ai/sgpnric", postData, {
        silent: true
      }, function (respMsg) {
        _this2.setState({
          isError: respMsg
        });
      }).then(function (response) {
        var respData = (response.body || {}).respData || {};

        var success = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(respData, "success");

        if (success) {
          overAction && overAction(respData);
        } else {
          _this2.setState({
            isError: lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(respData, "message", "")
          });
        }
      }).catch(function (error) {
        overAction && overAction("error");
      }).finally(function () {
        _this2.setState({
          isRes: true
        });
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this3 = this;

      var postData = this.props.postData;
      this.isApiOver(postData, function (endo) {
        _this3.handelOverAction(endo);
      });
      this.interval = setInterval(function () {
        _this3.usedtime = _this3.usedtime + 500;

        if (_this3.usedtime / 1000 >= 160) {
          _this3.handelOverAction();

          return;
        }

        if (_this3.state.progressNum >= 95 && !_this3.state.isRes) return;

        _this3.setState({
          progressNum: _this3.state.progressNum + lodash__WEBPACK_IMPORTED_MODULE_8___default.a.random(1, 2)
        });
      }, 500);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearInterval(this.interval);
      clearInterval(this.smoothlyAddInterval);
    }
  }, {
    key: "smoothlyAdd",
    value: function smoothlyAdd() {
      var _this4 = this;

      this.smoothlyAddInterval = setInterval(function () {
        if (_this4.state.progressNum === 100) {
          clearInterval(_this4.smoothlyAddInterval);
          return;
        }

        _this4.setState({
          progressNum: _this4.state.progressNum + 1
        });
      }, 250);
    }
  }, {
    key: "handelOverAction",
    value: function handelOverAction(endo) {
      clearInterval(this.interval);
      this.smoothlyAdd();
      this.props.overCallback && this.props.overCallback(endo || this.props.model);
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      var info = this.props.info;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 106
        },
        __self: this
      }, this.state.isError ? _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("p", {
        style: {
          color: "red"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 108
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
        style: {
          paddingRight: "5px"
        },
        type: "exclamation-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        __self: this
      }), this.state.isError), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        style: {
          textAlign: "right"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 113
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_5__["Button"], {
        key: "cancel",
        onClick: function onClick() {
          _this5.props.overCallback && _this5.props.overCallback("error-cancel");
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 114
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Cancel").thai("ยกเลิก").getMessage()))) : _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }, info || "Please hold on. We are processing identify..."), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_5__["Progress"], {
        percent: this.state.progressNum,
        strokeColor: COLOR_PRIMARY,
        status: "active",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124
        },
        __self: this
      })));
    }
  }]);

  return ProgressDialog;
}(_common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].Component);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BoL3Byb2dyZXNzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9waC9wcm9ncmVzcy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtCdXR0b24sIEljb24sIFByb2dyZXNzfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHtSZWFjdH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXMvaW5kZXhcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7QWpheCwgTGFuZ3VhZ2V9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQge0FqYXhSZXNwb25zZX0gZnJvbSBcIkBteS10eXBlc1wiO1xuXG5jb25zdCB7Q09MT1JfUFJJTUFSWX0gPSBUaGVtZS5nZXRUaGVtZSgpO1xuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgICBvdmVyQ2FsbGJhY2s/OiBGdW5jdGlvbjtcbiAgICBwb3N0RGF0YTogYW55O1xuICAgIG1vZGVsOiBhbnk7XG4gICAgaW5mbz86IGFueTtcbn1cblxuaW50ZXJmYWNlIElTdGF0ZSB7XG4gICAgcHJvZ3Jlc3NOdW06IG51bWJlcixcbiAgICBpc1JlczogYm9vbGVhbjtcbiAgICBpc0Vycm9yOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFByb2dyZXNzRGlhbG9nIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50PElQcm9wcywgSVN0YXRlPiB7XG5cbiAgICBpbnRlcnZhbDogYW55O1xuICAgIHNtb290aGx5QWRkSW50ZXJ2YWw6IGFueTtcbiAgICB1c2VkdGltZTogbnVtYmVyID0gMDtcblxuICAgIHN0YXRlOiBJU3RhdGUgPSB7XG4gICAgICAgIHByb2dyZXNzTnVtOiAwLFxuICAgICAgICBpc1JlczogZmFsc2UsXG4gICAgICAgIGlzRXJyb3I6IFwiXCIsXG4gICAgfTtcblxuICAgIGlzQXBpT3Zlcihwb3N0RGF0YTogYW55LCBvdmVyQWN0aW9uOiBGdW5jdGlvbikge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgIGlzUmVzOiBmYWxzZSxcbiAgICAgICAgICAgIGlzRXJyb3I6IFwiXCIsXG4gICAgICAgIH0pO1xuICAgICAgICBBamF4LmdldChgL2FpL3NncG5yaWNgLCBwb3N0RGF0YSwge1xuICAgICAgICAgICAgc2lsZW50OiB0cnVlLFxuICAgICAgICB9LCAocmVzcE1zZzogYW55KSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtpc0Vycm9yOiByZXNwTXNnfSk7XG4gICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IHJlc3BEYXRhID0gKHJlc3BvbnNlLmJvZHkgfHwge30pLnJlc3BEYXRhIHx8IHt9O1xuICAgICAgICAgICAgICAgIGNvbnN0IHN1Y2Nlc3MgPSBfLmdldChyZXNwRGF0YSwgXCJzdWNjZXNzXCIpO1xuICAgICAgICAgICAgICAgIGlmKHN1Y2Nlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgb3ZlckFjdGlvbiAmJiBvdmVyQWN0aW9uKHJlc3BEYXRhKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtpc0Vycm9yOiBfLmdldChyZXNwRGF0YSwgXCJtZXNzYWdlXCIsIFwiXCIpfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgb3ZlckFjdGlvbiAmJiBvdmVyQWN0aW9uKFwiZXJyb3JcIik7XG4gICAgICAgIH0pLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgaXNSZXM6IHRydWUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG5cbiAgICB9O1xuXG4gICAgY29tcG9uZW50RGlkTW91bnQoKTogdm9pZCB7XG4gICAgICAgIGNvbnN0IHtwb3N0RGF0YX0gPSB0aGlzLnByb3BzO1xuICAgICAgICB0aGlzLmlzQXBpT3Zlcihwb3N0RGF0YSwgKGVuZG86IGFueSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5oYW5kZWxPdmVyQWN0aW9uKGVuZG8pO1xuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5pbnRlcnZhbCA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMudXNlZHRpbWUgPSB0aGlzLnVzZWR0aW1lICsgNTAwO1xuICAgICAgICAgICAgaWYgKHRoaXMudXNlZHRpbWUgLyAxMDAwID49IDE2MCkge1xuICAgICAgICAgICAgICAgIHRoaXMuaGFuZGVsT3ZlckFjdGlvbigpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnByb2dyZXNzTnVtID49IDk1ICYmICF0aGlzLnN0YXRlLmlzUmVzKSByZXR1cm47XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBwcm9ncmVzc051bTogdGhpcy5zdGF0ZS5wcm9ncmVzc051bSArIF8ucmFuZG9tKDEsIDIpLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sIDUwMCk7XG4gICAgfVxuXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKTogdm9pZCB7XG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5pbnRlcnZhbCk7XG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5zbW9vdGhseUFkZEludGVydmFsKTtcbiAgICB9XG5cbiAgICBzbW9vdGhseUFkZCgpIHtcbiAgICAgICAgdGhpcy5zbW9vdGhseUFkZEludGVydmFsID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUucHJvZ3Jlc3NOdW0gPT09IDEwMCkge1xuICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5zbW9vdGhseUFkZEludGVydmFsKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBwcm9ncmVzc051bTogdGhpcy5zdGF0ZS5wcm9ncmVzc051bSArIDEsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSwgMjUwKTtcbiAgICB9XG5cbiAgICBoYW5kZWxPdmVyQWN0aW9uKGVuZG8/OiBhbnkpIHtcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsKTtcbiAgICAgICAgdGhpcy5zbW9vdGhseUFkZCgpO1xuICAgICAgICB0aGlzLnByb3BzLm92ZXJDYWxsYmFjayAmJiB0aGlzLnByb3BzLm92ZXJDYWxsYmFjayhlbmRvIHx8IHRoaXMucHJvcHMubW9kZWwpO1xuICAgIH1cblxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgY29uc3Qge2luZm99ID0gdGhpcy5wcm9wcztcbiAgICAgICAgcmV0dXJuIDxkaXY+XG4gICAgICAgICAgICB7dGhpcy5zdGF0ZS5pc0Vycm9yID8gPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgPHAgc3R5bGU9e3tjb2xvcjogXCJyZWRcIn19PlxuICAgICAgICAgICAgICAgICAgICAgICAgPEljb24gc3R5bGU9e3twYWRkaW5nUmlnaHQ6IFwiNXB4XCJ9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT17XCJleGNsYW1hdGlvbi1jaXJjbGVcIn0vPlxuICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMuc3RhdGUuaXNFcnJvcn1cbiAgICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7dGV4dEFsaWduOiBcInJpZ2h0XCJ9fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24ga2V5PVwiY2FuY2VsXCIgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub3ZlckNhbGxiYWNrICYmIHRoaXMucHJvcHMub3ZlckNhbGxiYWNrKFwiZXJyb3ItY2FuY2VsXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfX0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwiQ2FuY2VsXCIpLnRoYWkoXCLguKLguIHguYDguKXguLTguIFcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PiA6XG4gICAgICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICAgICAgPHA+e2luZm8gfHwgXCJQbGVhc2UgaG9sZCBvbi4gV2UgYXJlIHByb2Nlc3NpbmcgaWRlbnRpZnkuLi5cIn08L3A+XG5cbiAgICAgICAgICAgICAgICAgICAgPFByb2dyZXNzIHBlcmNlbnQ9e3RoaXMuc3RhdGUucHJvZ3Jlc3NOdW19IHN0cm9rZUNvbG9yPXtDT0xPUl9QUklNQVJZfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzPVwiYWN0aXZlXCIvPlxuICAgICAgICAgICAgICAgIDwvPn1cbiAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA7XG4gICAgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBY0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTs7Ozs7O0FBTUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUVBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7OztBQTFHQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/ph/progress.tsx
