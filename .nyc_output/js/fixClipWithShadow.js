var env = __webpack_require__(/*! ../../core/env */ "./node_modules/zrender/lib/core/env.js"); // Fix weird bug in some version of IE11 (like 11.0.9600.178**),
// where exception "unexpected call to method or property access"
// might be thrown when calling ctx.fill or ctx.stroke after a path
// whose area size is zero is drawn and ctx.clip() is called and
// shadowBlur is set. See #4572, #3112, #5777.
// (e.g.,
//  ctx.moveTo(10, 10);
//  ctx.lineTo(20, 10);
//  ctx.closePath();
//  ctx.clip();
//  ctx.shadowBlur = 10;
//  ...
//  ctx.fill();
// )


var shadowTemp = [['shadowBlur', 0], ['shadowColor', '#000'], ['shadowOffsetX', 0], ['shadowOffsetY', 0]];

function _default(orignalBrush) {
  // version string can be: '11.0'
  return env.browser.ie && env.browser.version >= 11 ? function () {
    var clipPaths = this.__clipPaths;
    var style = this.style;
    var modified;

    if (clipPaths) {
      for (var i = 0; i < clipPaths.length; i++) {
        var clipPath = clipPaths[i];
        var shape = clipPath && clipPath.shape;
        var type = clipPath && clipPath.type;

        if (shape && (type === 'sector' && shape.startAngle === shape.endAngle || type === 'rect' && (!shape.width || !shape.height))) {
          for (var j = 0; j < shadowTemp.length; j++) {
            // It is save to put shadowTemp static, because shadowTemp
            // will be all modified each item brush called.
            shadowTemp[j][2] = style[shadowTemp[j][0]];
            style[shadowTemp[j][0]] = shadowTemp[j][1];
          }

          modified = true;
          break;
        }
      }
    }

    orignalBrush.apply(this, arguments);

    if (modified) {
      for (var j = 0; j < shadowTemp.length; j++) {
        style[shadowTemp[j][0]] = shadowTemp[j][2];
      }
    }
  } : orignalBrush;
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvZml4Q2xpcFdpdGhTaGFkb3cuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9ncmFwaGljL2hlbHBlci9maXhDbGlwV2l0aFNoYWRvdy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgZW52ID0gcmVxdWlyZShcIi4uLy4uL2NvcmUvZW52XCIpO1xuXG4vLyBGaXggd2VpcmQgYnVnIGluIHNvbWUgdmVyc2lvbiBvZiBJRTExIChsaWtlIDExLjAuOTYwMC4xNzgqKiksXG4vLyB3aGVyZSBleGNlcHRpb24gXCJ1bmV4cGVjdGVkIGNhbGwgdG8gbWV0aG9kIG9yIHByb3BlcnR5IGFjY2Vzc1wiXG4vLyBtaWdodCBiZSB0aHJvd24gd2hlbiBjYWxsaW5nIGN0eC5maWxsIG9yIGN0eC5zdHJva2UgYWZ0ZXIgYSBwYXRoXG4vLyB3aG9zZSBhcmVhIHNpemUgaXMgemVybyBpcyBkcmF3biBhbmQgY3R4LmNsaXAoKSBpcyBjYWxsZWQgYW5kXG4vLyBzaGFkb3dCbHVyIGlzIHNldC4gU2VlICM0NTcyLCAjMzExMiwgIzU3NzcuXG4vLyAoZS5nLixcbi8vICBjdHgubW92ZVRvKDEwLCAxMCk7XG4vLyAgY3R4LmxpbmVUbygyMCwgMTApO1xuLy8gIGN0eC5jbG9zZVBhdGgoKTtcbi8vICBjdHguY2xpcCgpO1xuLy8gIGN0eC5zaGFkb3dCbHVyID0gMTA7XG4vLyAgLi4uXG4vLyAgY3R4LmZpbGwoKTtcbi8vIClcbnZhciBzaGFkb3dUZW1wID0gW1snc2hhZG93Qmx1cicsIDBdLCBbJ3NoYWRvd0NvbG9yJywgJyMwMDAnXSwgWydzaGFkb3dPZmZzZXRYJywgMF0sIFsnc2hhZG93T2Zmc2V0WScsIDBdXTtcblxuZnVuY3Rpb24gX2RlZmF1bHQob3JpZ25hbEJydXNoKSB7XG4gIC8vIHZlcnNpb24gc3RyaW5nIGNhbiBiZTogJzExLjAnXG4gIHJldHVybiBlbnYuYnJvd3Nlci5pZSAmJiBlbnYuYnJvd3Nlci52ZXJzaW9uID49IDExID8gZnVuY3Rpb24gKCkge1xuICAgIHZhciBjbGlwUGF0aHMgPSB0aGlzLl9fY2xpcFBhdGhzO1xuICAgIHZhciBzdHlsZSA9IHRoaXMuc3R5bGU7XG4gICAgdmFyIG1vZGlmaWVkO1xuXG4gICAgaWYgKGNsaXBQYXRocykge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjbGlwUGF0aHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIGNsaXBQYXRoID0gY2xpcFBhdGhzW2ldO1xuICAgICAgICB2YXIgc2hhcGUgPSBjbGlwUGF0aCAmJiBjbGlwUGF0aC5zaGFwZTtcbiAgICAgICAgdmFyIHR5cGUgPSBjbGlwUGF0aCAmJiBjbGlwUGF0aC50eXBlO1xuXG4gICAgICAgIGlmIChzaGFwZSAmJiAodHlwZSA9PT0gJ3NlY3RvcicgJiYgc2hhcGUuc3RhcnRBbmdsZSA9PT0gc2hhcGUuZW5kQW5nbGUgfHwgdHlwZSA9PT0gJ3JlY3QnICYmICghc2hhcGUud2lkdGggfHwgIXNoYXBlLmhlaWdodCkpKSB7XG4gICAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBzaGFkb3dUZW1wLmxlbmd0aDsgaisrKSB7XG4gICAgICAgICAgICAvLyBJdCBpcyBzYXZlIHRvIHB1dCBzaGFkb3dUZW1wIHN0YXRpYywgYmVjYXVzZSBzaGFkb3dUZW1wXG4gICAgICAgICAgICAvLyB3aWxsIGJlIGFsbCBtb2RpZmllZCBlYWNoIGl0ZW0gYnJ1c2ggY2FsbGVkLlxuICAgICAgICAgICAgc2hhZG93VGVtcFtqXVsyXSA9IHN0eWxlW3NoYWRvd1RlbXBbal1bMF1dO1xuICAgICAgICAgICAgc3R5bGVbc2hhZG93VGVtcFtqXVswXV0gPSBzaGFkb3dUZW1wW2pdWzFdO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIG1vZGlmaWVkID0gdHJ1ZTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIG9yaWduYWxCcnVzaC5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuXG4gICAgaWYgKG1vZGlmaWVkKSB7XG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IHNoYWRvd1RlbXAubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgc3R5bGVbc2hhZG93VGVtcFtqXVswXV0gPSBzaGFkb3dUZW1wW2pdWzJdO1xuICAgICAgfVxuICAgIH1cbiAgfSA6IG9yaWduYWxCcnVzaDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/helper/fixClipWithShadow.js
