__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BasicInfoComponent; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_view_item__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component/view-item */ "./src/app/desk/component/view-item/index.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_component_viewCurrency__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk/quote/SAIC/iar/component/viewCurrency */ "./src/app/desk/quote/SAIC/iar/component/viewCurrency.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _desk_reinsurance_treaty_definition_common_select_curreny__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk/reinsurance/treaty-definition/common/select-curreny */ "./src/app/desk/reinsurance/treaty-definition/common/select-curreny.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_component_period_of_insurance__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @desk/quote/SAIC/iar/component/period-of-insurance */ "./src/app/desk/quote/SAIC/iar/component/period-of-insurance.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/component/render-basic-info.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__["default"])(["\n  .ant-col.itemTitle {\n    padding-right: 14px !important\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}










var defaultLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};
var ViewDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_15__["Styled"].div(_templateObject());

var ViewItem = function ViewItem(props) {
  return react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(ViewDiv, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, Object(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_12__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_7__["default"])({}, props, {
    layout: defaultLayout
  })));
};

var BasicInfoComponent =
/*#__PURE__*/
function (_Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(BasicInfoComponent, _Component);

  function BasicInfoComponent(props) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, BasicInfoComponent);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicInfoComponent).call(this, props));
    _this.loadCodeTables =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var codeTables, that, arr;
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              codeTables = _this.state.codeTables;
              that = _this.props.that;
              arr = [];
              ["insurancefund", "gstrate", "currency"].forEach(function (item) {
                arr.push(new Promise(function (resolve, reject) {
                  _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].get("/mastertable", {
                    itntCode: that.getValueFromModel("itntCode"),
                    productCode: that.getValueFromModel("productCode"),
                    productVersion: that.getValueFromModel("productVersion"),
                    tableName: item
                  }, {}).then(function (response) {
                    var respData = response.body.respData;
                    codeTables[item] = respData.items || [];
                    resolve();
                  }).catch(function (error) {});
                }));
              });
              Promise.all(arr).then(function (values) {
                _this.setState({
                  codeTables: codeTables
                });
              }, function (reason) {
                console.log(reason);
              });

            case 5:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    _this.getMasterTableItemText = function (tableName, itemId) {
      var codeTables = _this.state.codeTables;

      if (itemId) {
        var item = (codeTables[tableName] || []).find(function (option) {
          return itemId.toString() === option.id.toString();
        }) || {};
        return item.text || "";
      } else {
        return "";
      }
    };

    _this.state = {
      codeTables: {}
    };
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(BasicInfoComponent, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var isEndo = this.props.isEndo;

      if (isEndo) {
        this.loadCodeTables();
      }
    }
  }, {
    key: "renderDate",
    value: function renderDate() {
      var effDate = lodash__WEBPACK_IMPORTED_MODULE_14___default.a.get(this.props.model, "ext.changes.effDate");

      var expDate = lodash__WEBPACK_IMPORTED_MODULE_14___default.a.get(this.props.model, "ext.changes.expDate");

      effDate = _common__WEBPACK_IMPORTED_MODULE_11__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_11__["DateUtils"].toDate(effDate));
      expDate = _common__WEBPACK_IMPORTED_MODULE_11__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_11__["DateUtils"].toDate(expDate));
      return "".concat(effDate, " ~ ").concat(expDate);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model,
          getProp = _this$props.getProp,
          formDateLayout = _this$props.formDateLayout,
          generatePropName = _this$props.generatePropName,
          that = _this$props.that,
          dataFixed = _this$props.dataFixed,
          endoDisabled = _this$props.endoDisabled,
          isEndo = _this$props.isEndo;
      return react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_9___default.a.Fragment, null, endoDisabled ? react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Period of Insurance").thai("Period of Insurance").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 142
        },
        __self: this
      }, this.renderDate()) : react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_quote_SAIC_iar_component_period_of_insurance__WEBPACK_IMPORTED_MODULE_17__["default"], {
        form: form,
        model: model,
        layout: formDateLayout,
        effPropName: getProp("effDate"),
        expPropName: getProp("expDate"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 149
        },
        __self: this
      }), isEndo ? react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_9___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Insurance Fund").thai("Insurance Fund").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 159
        },
        __self: this
      }, this.getMasterTableItemText("insurancefund", that.getValueFromModel(generatePropName("insuranceFund")))), react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("GST Rate").thai("GST Rate").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166
        },
        __self: this
      }, this.getMasterTableItemText("gstrate", that.getValueFromModel(generatePropName("gstRate")))), react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(ViewDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 173
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_quote_SAIC_iar_component_viewCurrency__WEBPACK_IMPORTED_MODULE_13__["ViewCurrencyItemStyle2"], {
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("SI Currency").thai("SI Currency").getMessage(),
        value: that.getValueFromModel(generatePropName("siCurrency")),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 174
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(ViewDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 181
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_quote_SAIC_iar_component_viewCurrency__WEBPACK_IMPORTED_MODULE_13__["ViewCurrencyItemStyle2"], {
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Premium Currency").thai("Premium Currency").getMessage(),
        value: that.getValueFromModel(generatePropName("premiumCurrency")),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 182
        },
        __self: this
      }))) : react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_9___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NRadio"], {
        model: model,
        form: form,
        required: true,
        tableName: "insurancefund",
        dataFixed: dataFixed,
        propName: generatePropName("insuranceFund"),
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Insurance Fund").thai("Insurance Fund").getMessage(),
        onChange: function onChange(value) {},
        __source: {
          fileName: _jsxFileName,
          lineNumber: 192
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NRadio"], {
        model: model,
        form: form,
        required: true,
        tableName: "gstrate",
        dataFixed: dataFixed,
        propName: generatePropName("gstRate"),
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("GST Rate").thai("GST Rate").getMessage(),
        onChange: function onChange(value) {},
        __source: {
          fileName: _jsxFileName,
          lineNumber: 205
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_reinsurance_treaty_definition_common_select_curreny__WEBPACK_IMPORTED_MODULE_16__["default"], {
        model: model,
        form: form,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("SI Currency").thai("SI Currency").getMessage(),
        style: {
          width: "100%"
        },
        propName: getProp("siCurrencyCode"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 218
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_desk_reinsurance_treaty_definition_common_select_curreny__WEBPACK_IMPORTED_MODULE_16__["default"], {
        model: model,
        form: form,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Premium Currency").thai("Premium Currency").getMessage(),
        style: {
          width: "100%"
        },
        propName: getProp("premCurrencyCode"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 223
        },
        __self: this
      })));
    }
  }]);

  return BasicInfoComponent;
}(react__WEBPACK_IMPORTED_MODULE_9__["Component"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3JlbmRlci1iYXNpYy1pbmZvLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL1NBSUMvaWFyL2NvbXBvbmVudC9yZW5kZXItYmFzaWMtaW5mby50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgRmllbGRHcm91cCB9IGZyb20gXCJAZGVzay1jb21wb25lbnRcIjtcbmltcG9ydCB7IE5EYXRlLCBORGF0ZUZpbHRlciwgTlBhbmVsLCBOUmFkaW8sIE5TZWxlY3QgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IEFqYXgsIENvbnN0cywgRGF0ZVV0aWxzLCBMYW5ndWFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgbW9tZW50LCB7IE1vbWVudCB9IGZyb20gXCJtb21lbnRcIjtcbmltcG9ydCBWaWV3SXRlbTEgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC92aWV3LWl0ZW1cIjtcbmltcG9ydCB7IFZpZXdDdXJyZW5jeUl0ZW1TdHlsZTIgfSBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3ZpZXdDdXJyZW5jeVwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBBamF4UmVzcG9uc2UgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgU2VsZWN0Q3VycmVuY3kgZnJvbSBcIkBkZXNrL3JlaW5zdXJhbmNlL3RyZWF0eS1kZWZpbml0aW9uL2NvbW1vbi9zZWxlY3QtY3VycmVueVwiO1xuaW1wb3J0IE5QT0lEYXRlIGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL2lhci9jb21wb25lbnQvcGVyaW9kLW9mLWluc3VyYW5jZVwiO1xuXG5jb25zdCBkZWZhdWx0TGF5b3V0ID0ge1xuICBsYWJlbENvbDoge1xuICAgIHhzOiB7IHNwYW46IDggfSxcbiAgICBzbTogeyBzcGFuOiA2IH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxNiB9LFxuICAgIHNtOiB7IHNwYW46IDEzIH0sXG4gIH0sXG59O1xuXG5jb25zdCBWaWV3RGl2ID0gU3R5bGVkLmRpdmBcbiAgLmFudC1jb2wuaXRlbVRpdGxlIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNHB4ICFpbXBvcnRhbnRcbiAgfVxuYDtcblxuY29uc3QgVmlld0l0ZW0gPSAocHJvcHM6IGFueSkgPT4gPFZpZXdEaXY+e1ZpZXdJdGVtMSh7IC4uLnByb3BzLCBsYXlvdXQ6IGRlZmF1bHRMYXlvdXQgfSl9PC9WaWV3RGl2PjtcblxuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgZm9ybTogYW55LFxuICBtb2RlbDogYW55LFxuICBnZXRQcm9wOiBhbnksXG4gIGZvcm1EYXRlTGF5b3V0OiBhbnksXG4gIGdlbmVyYXRlUHJvcE5hbWU6IGFueSxcbiAgdGhhdDogYW55LFxuICBkYXRhRml4ZWQ/OiBhbnlcbiAgZW5kb0Rpc2FibGVkPzogYm9vbGVhbjtcbiAgaXNFbmRvPzogYm9vbGVhblxufVxuXG5pbnRlcmZhY2UgSVN0YXRlIHtcbiAgY29kZVRhYmxlczogYW55XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJhc2ljSW5mb0NvbXBvbmVudCBleHRlbmRzIENvbXBvbmVudDxJUHJvcHMsIElTdGF0ZT4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogYW55KSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBjb2RlVGFibGVzOiB7fSxcbiAgICB9O1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKTogdm9pZCB7XG4gICAgY29uc3QgeyBpc0VuZG8gfSA9IHRoaXMucHJvcHM7XG4gICAgaWYgKGlzRW5kbykge1xuICAgICAgdGhpcy5sb2FkQ29kZVRhYmxlcygpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgbG9hZENvZGVUYWJsZXMgPSBhc3luYyAoKSA9PiB7XG4gICAgbGV0IHsgY29kZVRhYmxlcyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IHRoYXQgfSA9IHRoaXMucHJvcHM7XG4gICAgbGV0IGFycjogYW55ID0gW107XG4gICAgW1xuICAgICAgXCJpbnN1cmFuY2VmdW5kXCIsXG4gICAgICBcImdzdHJhdGVcIixcbiAgICAgIFwiY3VycmVuY3lcIixcbiAgICBdLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICBhcnIucHVzaChcbiAgICAgICAgbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgIEFqYXguZ2V0KFxuICAgICAgICAgICAgYC9tYXN0ZXJ0YWJsZWAsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGl0bnRDb2RlOiB0aGF0LmdldFZhbHVlRnJvbU1vZGVsKFwiaXRudENvZGVcIiksXG4gICAgICAgICAgICAgIHByb2R1Y3RDb2RlOiB0aGF0LmdldFZhbHVlRnJvbU1vZGVsKFwicHJvZHVjdENvZGVcIiksXG4gICAgICAgICAgICAgIHByb2R1Y3RWZXJzaW9uOiB0aGF0LmdldFZhbHVlRnJvbU1vZGVsKFwicHJvZHVjdFZlcnNpb25cIiksXG4gICAgICAgICAgICAgIHRhYmxlTmFtZTogaXRlbSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7fSxcbiAgICAgICAgICApXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5O1xuICAgICAgICAgICAgICBjb2RlVGFibGVzW2l0ZW1dID0gcmVzcERhdGEuaXRlbXMgfHwgW107XG4gICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pLFxuICAgICAgKTtcbiAgICB9KTtcbiAgICBQcm9taXNlLmFsbChhcnIpLnRoZW4oXG4gICAgICB2YWx1ZXMgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgY29kZVRhYmxlczogY29kZVRhYmxlcyB9KTtcbiAgICAgIH0sXG4gICAgICByZWFzb24gPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhyZWFzb24pO1xuICAgICAgfSxcbiAgICApO1xuICB9O1xuXG4gIHByaXZhdGUgZ2V0TWFzdGVyVGFibGVJdGVtVGV4dCA9ICh0YWJsZU5hbWU6IHN0cmluZywgaXRlbUlkOiBzdHJpbmcpID0+IHtcbiAgICBjb25zdCB7IGNvZGVUYWJsZXMgfSA9IHRoaXMuc3RhdGU7XG4gICAgaWYgKGl0ZW1JZCkge1xuICAgICAgY29uc3QgaXRlbSA9XG4gICAgICAgIChjb2RlVGFibGVzW3RhYmxlTmFtZV0gfHwgW10pLmZpbmQoKG9wdGlvbjogYW55KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIGl0ZW1JZC50b1N0cmluZygpID09PSBvcHRpb24uaWQudG9TdHJpbmcoKTtcbiAgICAgICAgfSkgfHwge307XG4gICAgICByZXR1cm4gaXRlbS50ZXh0IHx8IFwiXCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBcIlwiO1xuICAgIH1cbiAgfTtcblxuICByZW5kZXJEYXRlKCkge1xuICAgIGxldCBlZmZEYXRlID0gXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgXCJleHQuY2hhbmdlcy5lZmZEYXRlXCIpO1xuICAgIGxldCBleHBEYXRlID0gXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgXCJleHQuY2hhbmdlcy5leHBEYXRlXCIpO1xuICAgIGVmZkRhdGUgPSBEYXRlVXRpbHMuZm9ybWF0RGF0ZShEYXRlVXRpbHMudG9EYXRlKGVmZkRhdGUpKTtcbiAgICBleHBEYXRlID0gRGF0ZVV0aWxzLmZvcm1hdERhdGUoRGF0ZVV0aWxzLnRvRGF0ZShleHBEYXRlKSk7XG4gICAgcmV0dXJuIGAke2VmZkRhdGV9IH4gJHtleHBEYXRlfWA7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3Qge1xuICAgICAgZm9ybSxcbiAgICAgIG1vZGVsLFxuICAgICAgZ2V0UHJvcCxcbiAgICAgIGZvcm1EYXRlTGF5b3V0LFxuICAgICAgZ2VuZXJhdGVQcm9wTmFtZSxcbiAgICAgIHRoYXQsXG4gICAgICBkYXRhRml4ZWQsXG4gICAgICBlbmRvRGlzYWJsZWQsXG4gICAgICBpc0VuZG8sXG4gICAgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIDw+XG4gICAgICB7XG4gICAgICAgIGVuZG9EaXNhYmxlZCA/XG4gICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJQZXJpb2Qgb2YgSW5zdXJhbmNlXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwiUGVyaW9kIG9mIEluc3VyYW5jZVwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIHt0aGlzLnJlbmRlckRhdGUoKX1cbiAgICAgICAgICA8L1ZpZXdJdGVtPiA6XG4gICAgICAgICAgPE5QT0lEYXRlXG4gICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgbGF5b3V0PXtmb3JtRGF0ZUxheW91dH1cbiAgICAgICAgICAgIGVmZlByb3BOYW1lPXtnZXRQcm9wKFwiZWZmRGF0ZVwiKX1cbiAgICAgICAgICAgIGV4cFByb3BOYW1lPXtnZXRQcm9wKFwiZXhwRGF0ZVwiKX1cbiAgICAgICAgICAvPlxuICAgICAgfVxuICAgICAge2lzRW5kbyA/XG4gICAgICAgIDw+XG4gICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJJbnN1cmFuY2UgRnVuZFwiKVxuICAgICAgICAgICAgICAudGhhaShcIkluc3VyYW5jZSBGdW5kXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAge3RoaXMuZ2V0TWFzdGVyVGFibGVJdGVtVGV4dChcImluc3VyYW5jZWZ1bmRcIiwgdGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZW5lcmF0ZVByb3BOYW1lKFwiaW5zdXJhbmNlRnVuZFwiKSkpfVxuICAgICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJHU1QgUmF0ZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIkdTVCBSYXRlXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAge3RoaXMuZ2V0TWFzdGVyVGFibGVJdGVtVGV4dChcImdzdHJhdGVcIiwgdGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZW5lcmF0ZVByb3BOYW1lKFwiZ3N0UmF0ZVwiKSkpfVxuICAgICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgICAgPFZpZXdEaXY+XG4gICAgICAgICAgICA8Vmlld0N1cnJlbmN5SXRlbVN0eWxlMlxuICAgICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJTSSBDdXJyZW5jeVwiKVxuICAgICAgICAgICAgICAgIC50aGFpKFwiU0kgQ3VycmVuY3lcIilcbiAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICB2YWx1ZT17dGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZW5lcmF0ZVByb3BOYW1lKFwic2lDdXJyZW5jeVwiKSl9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvVmlld0Rpdj5cbiAgICAgICAgICA8Vmlld0Rpdj5cbiAgICAgICAgICAgIDxWaWV3Q3VycmVuY3lJdGVtU3R5bGUyXG4gICAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIlByZW1pdW0gQ3VycmVuY3lcIilcbiAgICAgICAgICAgICAgICAudGhhaShcIlByZW1pdW0gQ3VycmVuY3lcIilcbiAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICB2YWx1ZT17dGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZW5lcmF0ZVByb3BOYW1lKFwicHJlbWl1bUN1cnJlbmN5XCIpKX1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9WaWV3RGl2PlxuICAgICAgICA8Lz5cbiAgICAgICAgOlxuICAgICAgICA8PlxuICAgICAgICAgIDxOUmFkaW9cbiAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgIHRhYmxlTmFtZT17XCJpbnN1cmFuY2VmdW5kXCJ9XG4gICAgICAgICAgICBkYXRhRml4ZWQ9e2RhdGFGaXhlZH1cbiAgICAgICAgICAgIHByb3BOYW1lPXtnZW5lcmF0ZVByb3BOYW1lKFwiaW5zdXJhbmNlRnVuZFwiKX1cbiAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIkluc3VyYW5jZSBGdW5kXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwiSW5zdXJhbmNlIEZ1bmRcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsdWUpID0+IHtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8TlJhZGlvXG4gICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICB0YWJsZU5hbWU9e1wiZ3N0cmF0ZVwifVxuICAgICAgICAgICAgZGF0YUZpeGVkPXtkYXRhRml4ZWR9XG4gICAgICAgICAgICBwcm9wTmFtZT17Z2VuZXJhdGVQcm9wTmFtZShcImdzdFJhdGVcIil9XG4gICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJHU1QgUmF0ZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIkdTVCBSYXRlXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlKSA9PiB7XG4gICAgICAgICAgICB9fVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPFNlbGVjdEN1cnJlbmN5IG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiU0kgQ3VycmVuY3lcIikudGhhaShcIlNJIEN1cnJlbmN5XCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiMTAwJVwiIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXtnZXRQcm9wKFwic2lDdXJyZW5jeUNvZGVcIil9Lz5cbiAgICAgICAgICA8U2VsZWN0Q3VycmVuY3kgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJQcmVtaXVtIEN1cnJlbmN5XCIpLnRoYWkoXCJQcmVtaXVtIEN1cnJlbmN5XCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiMTAwJVwiIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXtnZXRQcm9wKFwicHJlbUN1cnJlbmN5Q29kZVwiKX0vPlxuICAgICAgICA8Lz5cbiAgICAgIH1cbiAgICA8Lz47XG4gIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFXQTtBQUNBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBa0JBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFjQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFLQTtBQUVBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBRUE7QUFDQTtBQUNBO0FBR0E7QUFFQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUF0Q0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQWZBO0FBdURBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpFQTtBQUNBO0FBREE7QUFGQTtBQUtBO0FBQ0E7OztBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUF3REE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFJQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFHQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBR0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQVZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTs7OztBQXJMQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/component/render-basic-info.tsx
