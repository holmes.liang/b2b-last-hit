/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var each = _util.each;
var isString = _util.isString;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Note that it is too complicated to support 3d stack by value
 * (have to create two-dimension inverted index), so in 3d case
 * we just support that stacked by index.
 *
 * @param {module:echarts/model/Series} seriesModel
 * @param {Array.<string|Object>} dimensionInfoList The same as the input of <module:echarts/data/List>.
 *        The input dimensionInfoList will be modified.
 * @param {Object} [opt]
 * @param {boolean} [opt.stackedCoordDimension=''] Specify a coord dimension if needed.
 * @param {boolean} [opt.byIndex=false]
 * @return {Object} calculationInfo
 * {
 *     stackedDimension: string
 *     stackedByDimension: string
 *     isStackedByIndex: boolean
 *     stackedOverDimension: string
 *     stackResultDimension: string
 * }
 */

function enableDataStack(seriesModel, dimensionInfoList, opt) {
  opt = opt || {};
  var byIndex = opt.byIndex;
  var stackedCoordDimension = opt.stackedCoordDimension; // Compatibal: when `stack` is set as '', do not stack.

  var mayStack = !!(seriesModel && seriesModel.get('stack'));
  var stackedByDimInfo;
  var stackedDimInfo;
  var stackResultDimension;
  var stackedOverDimension;
  each(dimensionInfoList, function (dimensionInfo, index) {
    if (isString(dimensionInfo)) {
      dimensionInfoList[index] = dimensionInfo = {
        name: dimensionInfo
      };
    }

    if (mayStack && !dimensionInfo.isExtraCoord) {
      // Find the first ordinal dimension as the stackedByDimInfo.
      if (!byIndex && !stackedByDimInfo && dimensionInfo.ordinalMeta) {
        stackedByDimInfo = dimensionInfo;
      } // Find the first stackable dimension as the stackedDimInfo.


      if (!stackedDimInfo && dimensionInfo.type !== 'ordinal' && dimensionInfo.type !== 'time' && (!stackedCoordDimension || stackedCoordDimension === dimensionInfo.coordDim)) {
        stackedDimInfo = dimensionInfo;
      }
    }
  });

  if (stackedDimInfo && !byIndex && !stackedByDimInfo) {
    // Compatible with previous design, value axis (time axis) only stack by index.
    // It may make sense if the user provides elaborately constructed data.
    byIndex = true;
  } // Add stack dimension, they can be both calculated by coordinate system in `unionExtent`.
  // That put stack logic in List is for using conveniently in echarts extensions, but it
  // might not be a good way.


  if (stackedDimInfo) {
    // Use a weird name that not duplicated with other names.
    stackResultDimension = '__\0ecstackresult';
    stackedOverDimension = '__\0ecstackedover'; // Create inverted index to fast query index by value.

    if (stackedByDimInfo) {
      stackedByDimInfo.createInvertedIndices = true;
    }

    var stackedDimCoordDim = stackedDimInfo.coordDim;
    var stackedDimType = stackedDimInfo.type;
    var stackedDimCoordIndex = 0;
    each(dimensionInfoList, function (dimensionInfo) {
      if (dimensionInfo.coordDim === stackedDimCoordDim) {
        stackedDimCoordIndex++;
      }
    });
    dimensionInfoList.push({
      name: stackResultDimension,
      coordDim: stackedDimCoordDim,
      coordDimIndex: stackedDimCoordIndex,
      type: stackedDimType,
      isExtraCoord: true,
      isCalculationCoord: true
    });
    stackedDimCoordIndex++;
    dimensionInfoList.push({
      name: stackedOverDimension,
      // This dimension contains stack base (generally, 0), so do not set it as
      // `stackedDimCoordDim` to avoid extent calculation, consider log scale.
      coordDim: stackedOverDimension,
      coordDimIndex: stackedDimCoordIndex,
      type: stackedDimType,
      isExtraCoord: true,
      isCalculationCoord: true
    });
  }

  return {
    stackedDimension: stackedDimInfo && stackedDimInfo.name,
    stackedByDimension: stackedByDimInfo && stackedByDimInfo.name,
    isStackedByIndex: byIndex,
    stackedOverDimension: stackedOverDimension,
    stackResultDimension: stackResultDimension
  };
}
/**
 * @param {module:echarts/data/List} data
 * @param {string} stackedDim
 */


function isDimensionStacked(data, stackedDim
/*, stackedByDim*/
) {
  // Each single series only maps to one pair of axis. So we do not need to
  // check stackByDim, whatever stacked by a dimension or stacked by index.
  return !!stackedDim && stackedDim === data.getCalculationInfo('stackedDimension'); // && (
  //     stackedByDim != null
  //         ? stackedByDim === data.getCalculationInfo('stackedByDimension')
  //         : data.getCalculationInfo('isStackedByIndex')
  // );
}
/**
 * @param {module:echarts/data/List} data
 * @param {string} targetDim
 * @param {string} [stackedByDim] If not input this parameter, check whether
 *                                stacked by index.
 * @return {string} dimension
 */


function getStackedDimension(data, targetDim) {
  return isDimensionStacked(data, targetDim) ? data.getCalculationInfo('stackResultDimension') : targetDim;
}

exports.enableDataStack = enableDataStack;
exports.isDimensionStacked = isDimensionStacked;
exports.getStackedDimension = getStackedDimension;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9oZWxwZXIvZGF0YVN0YWNrSGVscGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9oZWxwZXIvZGF0YVN0YWNrSGVscGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX3V0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgZWFjaCA9IF91dGlsLmVhY2g7XG52YXIgaXNTdHJpbmcgPSBfdXRpbC5pc1N0cmluZztcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIE5vdGUgdGhhdCBpdCBpcyB0b28gY29tcGxpY2F0ZWQgdG8gc3VwcG9ydCAzZCBzdGFjayBieSB2YWx1ZVxuICogKGhhdmUgdG8gY3JlYXRlIHR3by1kaW1lbnNpb24gaW52ZXJ0ZWQgaW5kZXgpLCBzbyBpbiAzZCBjYXNlXG4gKiB3ZSBqdXN0IHN1cHBvcnQgdGhhdCBzdGFja2VkIGJ5IGluZGV4LlxuICpcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvU2VyaWVzfSBzZXJpZXNNb2RlbFxuICogQHBhcmFtIHtBcnJheS48c3RyaW5nfE9iamVjdD59IGRpbWVuc2lvbkluZm9MaXN0IFRoZSBzYW1lIGFzIHRoZSBpbnB1dCBvZiA8bW9kdWxlOmVjaGFydHMvZGF0YS9MaXN0Pi5cbiAqICAgICAgICBUaGUgaW5wdXQgZGltZW5zaW9uSW5mb0xpc3Qgd2lsbCBiZSBtb2RpZmllZC5cbiAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0XVxuICogQHBhcmFtIHtib29sZWFufSBbb3B0LnN0YWNrZWRDb29yZERpbWVuc2lvbj0nJ10gU3BlY2lmeSBhIGNvb3JkIGRpbWVuc2lvbiBpZiBuZWVkZWQuXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHQuYnlJbmRleD1mYWxzZV1cbiAqIEByZXR1cm4ge09iamVjdH0gY2FsY3VsYXRpb25JbmZvXG4gKiB7XG4gKiAgICAgc3RhY2tlZERpbWVuc2lvbjogc3RyaW5nXG4gKiAgICAgc3RhY2tlZEJ5RGltZW5zaW9uOiBzdHJpbmdcbiAqICAgICBpc1N0YWNrZWRCeUluZGV4OiBib29sZWFuXG4gKiAgICAgc3RhY2tlZE92ZXJEaW1lbnNpb246IHN0cmluZ1xuICogICAgIHN0YWNrUmVzdWx0RGltZW5zaW9uOiBzdHJpbmdcbiAqIH1cbiAqL1xuZnVuY3Rpb24gZW5hYmxlRGF0YVN0YWNrKHNlcmllc01vZGVsLCBkaW1lbnNpb25JbmZvTGlzdCwgb3B0KSB7XG4gIG9wdCA9IG9wdCB8fCB7fTtcbiAgdmFyIGJ5SW5kZXggPSBvcHQuYnlJbmRleDtcbiAgdmFyIHN0YWNrZWRDb29yZERpbWVuc2lvbiA9IG9wdC5zdGFja2VkQ29vcmREaW1lbnNpb247IC8vIENvbXBhdGliYWw6IHdoZW4gYHN0YWNrYCBpcyBzZXQgYXMgJycsIGRvIG5vdCBzdGFjay5cblxuICB2YXIgbWF5U3RhY2sgPSAhIShzZXJpZXNNb2RlbCAmJiBzZXJpZXNNb2RlbC5nZXQoJ3N0YWNrJykpO1xuICB2YXIgc3RhY2tlZEJ5RGltSW5mbztcbiAgdmFyIHN0YWNrZWREaW1JbmZvO1xuICB2YXIgc3RhY2tSZXN1bHREaW1lbnNpb247XG4gIHZhciBzdGFja2VkT3ZlckRpbWVuc2lvbjtcbiAgZWFjaChkaW1lbnNpb25JbmZvTGlzdCwgZnVuY3Rpb24gKGRpbWVuc2lvbkluZm8sIGluZGV4KSB7XG4gICAgaWYgKGlzU3RyaW5nKGRpbWVuc2lvbkluZm8pKSB7XG4gICAgICBkaW1lbnNpb25JbmZvTGlzdFtpbmRleF0gPSBkaW1lbnNpb25JbmZvID0ge1xuICAgICAgICBuYW1lOiBkaW1lbnNpb25JbmZvXG4gICAgICB9O1xuICAgIH1cblxuICAgIGlmIChtYXlTdGFjayAmJiAhZGltZW5zaW9uSW5mby5pc0V4dHJhQ29vcmQpIHtcbiAgICAgIC8vIEZpbmQgdGhlIGZpcnN0IG9yZGluYWwgZGltZW5zaW9uIGFzIHRoZSBzdGFja2VkQnlEaW1JbmZvLlxuICAgICAgaWYgKCFieUluZGV4ICYmICFzdGFja2VkQnlEaW1JbmZvICYmIGRpbWVuc2lvbkluZm8ub3JkaW5hbE1ldGEpIHtcbiAgICAgICAgc3RhY2tlZEJ5RGltSW5mbyA9IGRpbWVuc2lvbkluZm87XG4gICAgICB9IC8vIEZpbmQgdGhlIGZpcnN0IHN0YWNrYWJsZSBkaW1lbnNpb24gYXMgdGhlIHN0YWNrZWREaW1JbmZvLlxuXG5cbiAgICAgIGlmICghc3RhY2tlZERpbUluZm8gJiYgZGltZW5zaW9uSW5mby50eXBlICE9PSAnb3JkaW5hbCcgJiYgZGltZW5zaW9uSW5mby50eXBlICE9PSAndGltZScgJiYgKCFzdGFja2VkQ29vcmREaW1lbnNpb24gfHwgc3RhY2tlZENvb3JkRGltZW5zaW9uID09PSBkaW1lbnNpb25JbmZvLmNvb3JkRGltKSkge1xuICAgICAgICBzdGFja2VkRGltSW5mbyA9IGRpbWVuc2lvbkluZm87XG4gICAgICB9XG4gICAgfVxuICB9KTtcblxuICBpZiAoc3RhY2tlZERpbUluZm8gJiYgIWJ5SW5kZXggJiYgIXN0YWNrZWRCeURpbUluZm8pIHtcbiAgICAvLyBDb21wYXRpYmxlIHdpdGggcHJldmlvdXMgZGVzaWduLCB2YWx1ZSBheGlzICh0aW1lIGF4aXMpIG9ubHkgc3RhY2sgYnkgaW5kZXguXG4gICAgLy8gSXQgbWF5IG1ha2Ugc2Vuc2UgaWYgdGhlIHVzZXIgcHJvdmlkZXMgZWxhYm9yYXRlbHkgY29uc3RydWN0ZWQgZGF0YS5cbiAgICBieUluZGV4ID0gdHJ1ZTtcbiAgfSAvLyBBZGQgc3RhY2sgZGltZW5zaW9uLCB0aGV5IGNhbiBiZSBib3RoIGNhbGN1bGF0ZWQgYnkgY29vcmRpbmF0ZSBzeXN0ZW0gaW4gYHVuaW9uRXh0ZW50YC5cbiAgLy8gVGhhdCBwdXQgc3RhY2sgbG9naWMgaW4gTGlzdCBpcyBmb3IgdXNpbmcgY29udmVuaWVudGx5IGluIGVjaGFydHMgZXh0ZW5zaW9ucywgYnV0IGl0XG4gIC8vIG1pZ2h0IG5vdCBiZSBhIGdvb2Qgd2F5LlxuXG5cbiAgaWYgKHN0YWNrZWREaW1JbmZvKSB7XG4gICAgLy8gVXNlIGEgd2VpcmQgbmFtZSB0aGF0IG5vdCBkdXBsaWNhdGVkIHdpdGggb3RoZXIgbmFtZXMuXG4gICAgc3RhY2tSZXN1bHREaW1lbnNpb24gPSAnX19cXDBlY3N0YWNrcmVzdWx0JztcbiAgICBzdGFja2VkT3ZlckRpbWVuc2lvbiA9ICdfX1xcMGVjc3RhY2tlZG92ZXInOyAvLyBDcmVhdGUgaW52ZXJ0ZWQgaW5kZXggdG8gZmFzdCBxdWVyeSBpbmRleCBieSB2YWx1ZS5cblxuICAgIGlmIChzdGFja2VkQnlEaW1JbmZvKSB7XG4gICAgICBzdGFja2VkQnlEaW1JbmZvLmNyZWF0ZUludmVydGVkSW5kaWNlcyA9IHRydWU7XG4gICAgfVxuXG4gICAgdmFyIHN0YWNrZWREaW1Db29yZERpbSA9IHN0YWNrZWREaW1JbmZvLmNvb3JkRGltO1xuICAgIHZhciBzdGFja2VkRGltVHlwZSA9IHN0YWNrZWREaW1JbmZvLnR5cGU7XG4gICAgdmFyIHN0YWNrZWREaW1Db29yZEluZGV4ID0gMDtcbiAgICBlYWNoKGRpbWVuc2lvbkluZm9MaXN0LCBmdW5jdGlvbiAoZGltZW5zaW9uSW5mbykge1xuICAgICAgaWYgKGRpbWVuc2lvbkluZm8uY29vcmREaW0gPT09IHN0YWNrZWREaW1Db29yZERpbSkge1xuICAgICAgICBzdGFja2VkRGltQ29vcmRJbmRleCsrO1xuICAgICAgfVxuICAgIH0pO1xuICAgIGRpbWVuc2lvbkluZm9MaXN0LnB1c2goe1xuICAgICAgbmFtZTogc3RhY2tSZXN1bHREaW1lbnNpb24sXG4gICAgICBjb29yZERpbTogc3RhY2tlZERpbUNvb3JkRGltLFxuICAgICAgY29vcmREaW1JbmRleDogc3RhY2tlZERpbUNvb3JkSW5kZXgsXG4gICAgICB0eXBlOiBzdGFja2VkRGltVHlwZSxcbiAgICAgIGlzRXh0cmFDb29yZDogdHJ1ZSxcbiAgICAgIGlzQ2FsY3VsYXRpb25Db29yZDogdHJ1ZVxuICAgIH0pO1xuICAgIHN0YWNrZWREaW1Db29yZEluZGV4Kys7XG4gICAgZGltZW5zaW9uSW5mb0xpc3QucHVzaCh7XG4gICAgICBuYW1lOiBzdGFja2VkT3ZlckRpbWVuc2lvbixcbiAgICAgIC8vIFRoaXMgZGltZW5zaW9uIGNvbnRhaW5zIHN0YWNrIGJhc2UgKGdlbmVyYWxseSwgMCksIHNvIGRvIG5vdCBzZXQgaXQgYXNcbiAgICAgIC8vIGBzdGFja2VkRGltQ29vcmREaW1gIHRvIGF2b2lkIGV4dGVudCBjYWxjdWxhdGlvbiwgY29uc2lkZXIgbG9nIHNjYWxlLlxuICAgICAgY29vcmREaW06IHN0YWNrZWRPdmVyRGltZW5zaW9uLFxuICAgICAgY29vcmREaW1JbmRleDogc3RhY2tlZERpbUNvb3JkSW5kZXgsXG4gICAgICB0eXBlOiBzdGFja2VkRGltVHlwZSxcbiAgICAgIGlzRXh0cmFDb29yZDogdHJ1ZSxcbiAgICAgIGlzQ2FsY3VsYXRpb25Db29yZDogdHJ1ZVxuICAgIH0pO1xuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBzdGFja2VkRGltZW5zaW9uOiBzdGFja2VkRGltSW5mbyAmJiBzdGFja2VkRGltSW5mby5uYW1lLFxuICAgIHN0YWNrZWRCeURpbWVuc2lvbjogc3RhY2tlZEJ5RGltSW5mbyAmJiBzdGFja2VkQnlEaW1JbmZvLm5hbWUsXG4gICAgaXNTdGFja2VkQnlJbmRleDogYnlJbmRleCxcbiAgICBzdGFja2VkT3ZlckRpbWVuc2lvbjogc3RhY2tlZE92ZXJEaW1lbnNpb24sXG4gICAgc3RhY2tSZXN1bHREaW1lbnNpb246IHN0YWNrUmVzdWx0RGltZW5zaW9uXG4gIH07XG59XG4vKipcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvZGF0YS9MaXN0fSBkYXRhXG4gKiBAcGFyYW0ge3N0cmluZ30gc3RhY2tlZERpbVxuICovXG5cblxuZnVuY3Rpb24gaXNEaW1lbnNpb25TdGFja2VkKGRhdGEsIHN0YWNrZWREaW1cbi8qLCBzdGFja2VkQnlEaW0qL1xuKSB7XG4gIC8vIEVhY2ggc2luZ2xlIHNlcmllcyBvbmx5IG1hcHMgdG8gb25lIHBhaXIgb2YgYXhpcy4gU28gd2UgZG8gbm90IG5lZWQgdG9cbiAgLy8gY2hlY2sgc3RhY2tCeURpbSwgd2hhdGV2ZXIgc3RhY2tlZCBieSBhIGRpbWVuc2lvbiBvciBzdGFja2VkIGJ5IGluZGV4LlxuICByZXR1cm4gISFzdGFja2VkRGltICYmIHN0YWNrZWREaW0gPT09IGRhdGEuZ2V0Q2FsY3VsYXRpb25JbmZvKCdzdGFja2VkRGltZW5zaW9uJyk7IC8vICYmIChcbiAgLy8gICAgIHN0YWNrZWRCeURpbSAhPSBudWxsXG4gIC8vICAgICAgICAgPyBzdGFja2VkQnlEaW0gPT09IGRhdGEuZ2V0Q2FsY3VsYXRpb25JbmZvKCdzdGFja2VkQnlEaW1lbnNpb24nKVxuICAvLyAgICAgICAgIDogZGF0YS5nZXRDYWxjdWxhdGlvbkluZm8oJ2lzU3RhY2tlZEJ5SW5kZXgnKVxuICAvLyApO1xufVxuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2RhdGEvTGlzdH0gZGF0YVxuICogQHBhcmFtIHtzdHJpbmd9IHRhcmdldERpbVxuICogQHBhcmFtIHtzdHJpbmd9IFtzdGFja2VkQnlEaW1dIElmIG5vdCBpbnB1dCB0aGlzIHBhcmFtZXRlciwgY2hlY2sgd2hldGhlclxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YWNrZWQgYnkgaW5kZXguXG4gKiBAcmV0dXJuIHtzdHJpbmd9IGRpbWVuc2lvblxuICovXG5cblxuZnVuY3Rpb24gZ2V0U3RhY2tlZERpbWVuc2lvbihkYXRhLCB0YXJnZXREaW0pIHtcbiAgcmV0dXJuIGlzRGltZW5zaW9uU3RhY2tlZChkYXRhLCB0YXJnZXREaW0pID8gZGF0YS5nZXRDYWxjdWxhdGlvbkluZm8oJ3N0YWNrUmVzdWx0RGltZW5zaW9uJykgOiB0YXJnZXREaW07XG59XG5cbmV4cG9ydHMuZW5hYmxlRGF0YVN0YWNrID0gZW5hYmxlRGF0YVN0YWNrO1xuZXhwb3J0cy5pc0RpbWVuc2lvblN0YWNrZWQgPSBpc0RpbWVuc2lvblN0YWNrZWQ7XG5leHBvcnRzLmdldFN0YWNrZWREaW1lbnNpb24gPSBnZXRTdGFja2VkRGltZW5zaW9uOyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/data/helper/dataStackHelper.js
