__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var rc_mentions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-mentions */ "./node_modules/rc-mentions/es/index.js");
/* harmony import */ var _spin__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../spin */ "./node_modules/antd/es/spin/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};








var Option = rc_mentions__WEBPACK_IMPORTED_MODULE_4__["default"].Option;

function loadingFilterOption() {
  return true;
}

var Mentions =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Mentions, _React$Component);

  function Mentions() {
    var _this;

    _classCallCheck(this, Mentions);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Mentions).apply(this, arguments));
    _this.state = {
      focused: false
    };

    _this.onFocus = function () {
      var onFocus = _this.props.onFocus;

      if (onFocus) {
        onFocus.apply(void 0, arguments);
      }

      _this.setState({
        focused: true
      });
    };

    _this.onBlur = function () {
      var onBlur = _this.props.onBlur;

      if (onBlur) {
        onBlur.apply(void 0, arguments);
      }

      _this.setState({
        focused: false
      });
    };

    _this.getOptions = function () {
      var _this$props = _this.props,
          children = _this$props.children,
          loading = _this$props.loading;

      if (loading) {
        return react__WEBPACK_IMPORTED_MODULE_2__["createElement"](Option, {
          value: "ANTD_SEARCHING",
          disabled: true
        }, react__WEBPACK_IMPORTED_MODULE_2__["createElement"](_spin__WEBPACK_IMPORTED_MODULE_5__["default"], {
          size: "small"
        }));
      }

      return children;
    };

    _this.getFilterOption = function () {
      var _this$props2 = _this.props,
          filterOption = _this$props2.filterOption,
          loading = _this$props2.loading;

      if (loading) {
        return loadingFilterOption;
      }

      return filterOption;
    };

    _this.saveMentions = function (node) {
      _this.rcMentions = node;
    };

    _this.renderMentions = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls,
          renderEmpty = _ref.renderEmpty;
      var focused = _this.state.focused;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          className = _a.className,
          disabled = _a.disabled,
          restProps = __rest(_a, ["prefixCls", "className", "disabled"]);

      var prefixCls = getPrefixCls('mentions', customizePrefixCls);
      var mentionsProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_1__["default"])(restProps, ['loading']);
      var mergedClassName = classnames__WEBPACK_IMPORTED_MODULE_0___default()(className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-disabled"), disabled), _defineProperty(_classNames, "".concat(prefixCls, "-focused"), focused), _classNames));
      return react__WEBPACK_IMPORTED_MODULE_2__["createElement"](rc_mentions__WEBPACK_IMPORTED_MODULE_4__["default"], _extends({
        prefixCls: prefixCls,
        notFoundContent: _this.getNotFoundContent(renderEmpty),
        className: mergedClassName,
        disabled: disabled
      }, mentionsProps, {
        filterOption: _this.getFilterOption(),
        onFocus: _this.onFocus,
        onBlur: _this.onBlur,
        ref: _this.saveMentions
      }), _this.getOptions());
    };

    return _this;
  }

  _createClass(Mentions, [{
    key: "getNotFoundContent",
    value: function getNotFoundContent(renderEmpty) {
      var notFoundContent = this.props.notFoundContent;

      if (notFoundContent !== undefined) {
        return notFoundContent;
      }

      return renderEmpty('Select');
    }
  }, {
    key: "focus",
    value: function focus() {
      this.rcMentions.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.rcMentions.blur();
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_2__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderMentions);
    }
  }]);

  return Mentions;
}(react__WEBPACK_IMPORTED_MODULE_2__["Component"]);

Mentions.Option = Option;

Mentions.getMentions = function () {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var config = arguments.length > 1 ? arguments[1] : undefined;

  var _ref2 = config || {},
      _ref2$prefix = _ref2.prefix,
      prefix = _ref2$prefix === void 0 ? '@' : _ref2$prefix,
      _ref2$split = _ref2.split,
      split = _ref2$split === void 0 ? ' ' : _ref2$split;

  var prefixList = Array.isArray(prefix) ? prefix : [prefix];
  return value.split(split).map(function () {
    var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var hitPrefix = null;
    prefixList.some(function (prefixStr) {
      var startStr = str.slice(0, prefixStr.length);

      if (startStr === prefixStr) {
        hitPrefix = prefixStr;
        return true;
      }

      return false;
    });

    if (hitPrefix !== null) {
      return {
        prefix: hitPrefix,
        value: str.slice(hitPrefix.length)
      };
    }

    return null;
  }).filter(function (entity) {
    return !!entity && !!entity.value;
  });
};

Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__["polyfill"])(Mentions);
/* harmony default export */ __webpack_exports__["default"] = (Mentions);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9tZW50aW9ucy9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbWVudGlvbnMvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbInZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IG9taXQgZnJvbSAnb21pdC5qcyc7XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBwb2x5ZmlsbCB9IGZyb20gJ3JlYWN0LWxpZmVjeWNsZXMtY29tcGF0JztcbmltcG9ydCBSY01lbnRpb25zIGZyb20gJ3JjLW1lbnRpb25zJztcbmltcG9ydCBTcGluIGZyb20gJy4uL3NwaW4nO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuY29uc3QgeyBPcHRpb24gfSA9IFJjTWVudGlvbnM7XG5mdW5jdGlvbiBsb2FkaW5nRmlsdGVyT3B0aW9uKCkge1xuICAgIHJldHVybiB0cnVlO1xufVxuY2xhc3MgTWVudGlvbnMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlciguLi5hcmd1bWVudHMpO1xuICAgICAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgICAgICAgZm9jdXNlZDogZmFsc2UsXG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25Gb2N1cyA9ICguLi5hcmdzKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IG9uRm9jdXMgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAob25Gb2N1cykge1xuICAgICAgICAgICAgICAgIG9uRm9jdXMoLi4uYXJncyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBmb2N1c2VkOiB0cnVlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25CbHVyID0gKC4uLmFyZ3MpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgb25CbHVyIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKG9uQmx1cikge1xuICAgICAgICAgICAgICAgIG9uQmx1ciguLi5hcmdzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIGZvY3VzZWQ6IGZhbHNlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuZ2V0T3B0aW9ucyA9ICgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgY2hpbGRyZW4sIGxvYWRpbmcgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAobG9hZGluZykge1xuICAgICAgICAgICAgICAgIHJldHVybiAoPE9wdGlvbiB2YWx1ZT1cIkFOVERfU0VBUkNISU5HXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgPFNwaW4gc2l6ZT1cInNtYWxsXCIvPlxuICAgICAgICA8L09wdGlvbj4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGNoaWxkcmVuO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmdldEZpbHRlck9wdGlvbiA9ICgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgZmlsdGVyT3B0aW9uLCBsb2FkaW5nIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKGxvYWRpbmcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbG9hZGluZ0ZpbHRlck9wdGlvbjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBmaWx0ZXJPcHRpb247XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc2F2ZU1lbnRpb25zID0gKG5vZGUpID0+IHtcbiAgICAgICAgICAgIHRoaXMucmNNZW50aW9ucyA9IG5vZGU7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyTWVudGlvbnMgPSAoeyBnZXRQcmVmaXhDbHMsIHJlbmRlckVtcHR5IH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgZm9jdXNlZCB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgICAgIGNvbnN0IF9hID0gdGhpcy5wcm9wcywgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgY2xhc3NOYW1lLCBkaXNhYmxlZCB9ID0gX2EsIHJlc3RQcm9wcyA9IF9fcmVzdChfYSwgW1wicHJlZml4Q2xzXCIsIFwiY2xhc3NOYW1lXCIsIFwiZGlzYWJsZWRcIl0pO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdtZW50aW9ucycsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBjb25zdCBtZW50aW9uc1Byb3BzID0gb21pdChyZXN0UHJvcHMsIFsnbG9hZGluZyddKTtcbiAgICAgICAgICAgIGNvbnN0IG1lcmdlZENsYXNzTmFtZSA9IGNsYXNzTmFtZXMoY2xhc3NOYW1lLCB7XG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tZGlzYWJsZWRgXTogZGlzYWJsZWQsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tZm9jdXNlZGBdOiBmb2N1c2VkLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gKDxSY01lbnRpb25zIHByZWZpeENscz17cHJlZml4Q2xzfSBub3RGb3VuZENvbnRlbnQ9e3RoaXMuZ2V0Tm90Rm91bmRDb250ZW50KHJlbmRlckVtcHR5KX0gY2xhc3NOYW1lPXttZXJnZWRDbGFzc05hbWV9IGRpc2FibGVkPXtkaXNhYmxlZH0gey4uLm1lbnRpb25zUHJvcHN9IGZpbHRlck9wdGlvbj17dGhpcy5nZXRGaWx0ZXJPcHRpb24oKX0gb25Gb2N1cz17dGhpcy5vbkZvY3VzfSBvbkJsdXI9e3RoaXMub25CbHVyfSByZWY9e3RoaXMuc2F2ZU1lbnRpb25zfT5cbiAgICAgICAge3RoaXMuZ2V0T3B0aW9ucygpfVxuICAgICAgPC9SY01lbnRpb25zPik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIGdldE5vdEZvdW5kQ29udGVudChyZW5kZXJFbXB0eSkge1xuICAgICAgICBjb25zdCB7IG5vdEZvdW5kQ29udGVudCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKG5vdEZvdW5kQ29udGVudCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gbm90Rm91bmRDb250ZW50O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZW5kZXJFbXB0eSgnU2VsZWN0Jyk7XG4gICAgfVxuICAgIGZvY3VzKCkge1xuICAgICAgICB0aGlzLnJjTWVudGlvbnMuZm9jdXMoKTtcbiAgICB9XG4gICAgYmx1cigpIHtcbiAgICAgICAgdGhpcy5yY01lbnRpb25zLmJsdXIoKTtcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gPENvbmZpZ0NvbnN1bWVyPnt0aGlzLnJlbmRlck1lbnRpb25zfTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cbk1lbnRpb25zLk9wdGlvbiA9IE9wdGlvbjtcbk1lbnRpb25zLmdldE1lbnRpb25zID0gKHZhbHVlID0gJycsIGNvbmZpZykgPT4ge1xuICAgIGNvbnN0IHsgcHJlZml4ID0gJ0AnLCBzcGxpdCA9ICcgJyB9ID0gY29uZmlnIHx8IHt9O1xuICAgIGNvbnN0IHByZWZpeExpc3QgPSBBcnJheS5pc0FycmF5KHByZWZpeCkgPyBwcmVmaXggOiBbcHJlZml4XTtcbiAgICByZXR1cm4gdmFsdWVcbiAgICAgICAgLnNwbGl0KHNwbGl0KVxuICAgICAgICAubWFwKChzdHIgPSAnJykgPT4ge1xuICAgICAgICBsZXQgaGl0UHJlZml4ID0gbnVsbDtcbiAgICAgICAgcHJlZml4TGlzdC5zb21lKHByZWZpeFN0ciA9PiB7XG4gICAgICAgICAgICBjb25zdCBzdGFydFN0ciA9IHN0ci5zbGljZSgwLCBwcmVmaXhTdHIubGVuZ3RoKTtcbiAgICAgICAgICAgIGlmIChzdGFydFN0ciA9PT0gcHJlZml4U3RyKSB7XG4gICAgICAgICAgICAgICAgaGl0UHJlZml4ID0gcHJlZml4U3RyO1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9KTtcbiAgICAgICAgaWYgKGhpdFByZWZpeCAhPT0gbnVsbCkge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICBwcmVmaXg6IGhpdFByZWZpeCxcbiAgICAgICAgICAgICAgICB2YWx1ZTogc3RyLnNsaWNlKGhpdFByZWZpeC5sZW5ndGgpLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9KVxuICAgICAgICAuZmlsdGVyKChlbnRpdHkpID0+ICEhZW50aXR5ICYmICEhZW50aXR5LnZhbHVlKTtcbn07XG5wb2x5ZmlsbChNZW50aW9ucyk7XG5leHBvcnQgZGVmYXVsdCBNZW50aW9ucztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQVRBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFMQTtBQUNBO0FBUUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFMQTtBQUNBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBUEE7QUFDQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBTEE7QUFDQTtBQU1BO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVEE7QUFDQTtBQTNDQTtBQXVEQTtBQUNBOzs7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7Ozs7QUF4RUE7QUFDQTtBQXlFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQU5BO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBbEJBO0FBb0JBO0FBcEJBO0FBSEE7QUFDQTtBQXdCQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/mentions/index.js
