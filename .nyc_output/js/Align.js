__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var dom_align__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! dom-align */ "./node_modules/dom-align/dist-web/index.js");
/* harmony import */ var rc_util_es_Dom_addEventListener__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rc-util/es/Dom/addEventListener */ "./node_modules/rc-util/es/Dom/addEventListener.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./util */ "./node_modules/rc-align/es/util.js");











function getElement(func) {
  if (typeof func !== 'function' || !func) return null;
  return func();
}

function getPoint(point) {
  if (typeof point !== 'object' || !point) return null;
  return point;
}

var Align = function (_Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default()(Align, _Component);

  function Align() {
    var _ref;

    var _temp, _this, _ret;

    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Align);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(this, (_ref = Align.__proto__ || Object.getPrototypeOf(Align)).call.apply(_ref, [this].concat(args))), _this), _this.forceAlign = function () {
      var _this$props = _this.props,
          disabled = _this$props.disabled,
          target = _this$props.target,
          align = _this$props.align,
          onAlign = _this$props.onAlign;

      if (!disabled && target) {
        var source = react_dom__WEBPACK_IMPORTED_MODULE_6___default.a.findDOMNode(_this);
        var result = void 0;
        var element = getElement(target);
        var point = getPoint(target); // IE lose focus after element realign
        // We should record activeElement and restore later

        var activeElement = document.activeElement;

        if (element) {
          result = Object(dom_align__WEBPACK_IMPORTED_MODULE_7__["alignElement"])(source, element, align);
        } else if (point) {
          result = Object(dom_align__WEBPACK_IMPORTED_MODULE_7__["alignPoint"])(source, point, align);
        }

        Object(_util__WEBPACK_IMPORTED_MODULE_9__["restoreFocus"])(activeElement, source);

        if (onAlign) {
          onAlign(source, result);
        }
      }
    }, _temp), babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(_this, _ret);
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Align, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var props = this.props; // if parent ref not attached .... use document.getElementById

      this.forceAlign();

      if (!props.disabled && props.monitorWindowResize) {
        this.startMonitorWindowResize();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      var reAlign = false;
      var props = this.props;

      if (!props.disabled) {
        var source = react_dom__WEBPACK_IMPORTED_MODULE_6___default.a.findDOMNode(this);
        var sourceRect = source ? source.getBoundingClientRect() : null;

        if (prevProps.disabled) {
          reAlign = true;
        } else {
          var lastElement = getElement(prevProps.target);
          var currentElement = getElement(props.target);
          var lastPoint = getPoint(prevProps.target);
          var currentPoint = getPoint(props.target);

          if (Object(_util__WEBPACK_IMPORTED_MODULE_9__["isWindow"])(lastElement) && Object(_util__WEBPACK_IMPORTED_MODULE_9__["isWindow"])(currentElement)) {
            // Skip if is window
            reAlign = false;
          } else if (lastElement !== currentElement || // Element change
          lastElement && !currentElement && currentPoint || // Change from element to point
          lastPoint && currentPoint && currentElement || // Change from point to element
          currentPoint && !Object(_util__WEBPACK_IMPORTED_MODULE_9__["isSamePoint"])(lastPoint, currentPoint)) {
            reAlign = true;
          } // If source element size changed


          var preRect = this.sourceRect || {};

          if (!reAlign && source && (!Object(_util__WEBPACK_IMPORTED_MODULE_9__["isSimilarValue"])(preRect.width, sourceRect.width) || !Object(_util__WEBPACK_IMPORTED_MODULE_9__["isSimilarValue"])(preRect.height, sourceRect.height))) {
            reAlign = true;
          }
        }

        this.sourceRect = sourceRect;
      }

      if (reAlign) {
        this.forceAlign();
      }

      if (props.monitorWindowResize && !props.disabled) {
        this.startMonitorWindowResize();
      } else {
        this.stopMonitorWindowResize();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.stopMonitorWindowResize();
    }
  }, {
    key: 'startMonitorWindowResize',
    value: function startMonitorWindowResize() {
      if (!this.resizeHandler) {
        this.bufferMonitor = Object(_util__WEBPACK_IMPORTED_MODULE_9__["buffer"])(this.forceAlign, this.props.monitorBufferTime);
        this.resizeHandler = Object(rc_util_es_Dom_addEventListener__WEBPACK_IMPORTED_MODULE_8__["default"])(window, 'resize', this.bufferMonitor);
      }
    }
  }, {
    key: 'stopMonitorWindowResize',
    value: function stopMonitorWindowResize() {
      if (this.resizeHandler) {
        this.bufferMonitor.clear();
        this.resizeHandler.remove();
        this.resizeHandler = null;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          childrenProps = _props.childrenProps,
          children = _props.children;
      var child = react__WEBPACK_IMPORTED_MODULE_4___default.a.Children.only(children);

      if (childrenProps) {
        var newProps = {};
        var propList = Object.keys(childrenProps);
        propList.forEach(function (prop) {
          newProps[prop] = _this2.props[childrenProps[prop]];
        });
        return react__WEBPACK_IMPORTED_MODULE_4___default.a.cloneElement(child, newProps);
      }

      return child;
    }
  }]);

  return Align;
}(react__WEBPACK_IMPORTED_MODULE_4__["Component"]);

Align.propTypes = {
  childrenProps: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object,
  align: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object.isRequired,
  target: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func, prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.shape({
    clientX: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.number,
    clientY: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.number,
    pageX: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.number,
    pageY: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.number
  })]),
  onAlign: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  monitorBufferTime: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.number,
  monitorWindowResize: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  children: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.any
};
Align.defaultProps = {
  target: function target() {
    return window;
  },
  monitorBufferTime: 50,
  monitorWindowResize: false,
  disabled: false
};
/* harmony default export */ __webpack_exports__["default"] = (Align);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtYWxpZ24vZXMvQWxpZ24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1hbGlnbi9lcy9BbGlnbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX2NyZWF0ZUNsYXNzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcyc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgUmVhY3RET00gZnJvbSAncmVhY3QtZG9tJztcbmltcG9ydCB7IGFsaWduRWxlbWVudCwgYWxpZ25Qb2ludCB9IGZyb20gJ2RvbS1hbGlnbic7XG5pbXBvcnQgYWRkRXZlbnRMaXN0ZW5lciBmcm9tICdyYy11dGlsL2VzL0RvbS9hZGRFdmVudExpc3RlbmVyJztcblxuaW1wb3J0IHsgaXNXaW5kb3csIGJ1ZmZlciwgaXNTYW1lUG9pbnQsIGlzU2ltaWxhclZhbHVlLCByZXN0b3JlRm9jdXMgfSBmcm9tICcuL3V0aWwnO1xuXG5mdW5jdGlvbiBnZXRFbGVtZW50KGZ1bmMpIHtcbiAgaWYgKHR5cGVvZiBmdW5jICE9PSAnZnVuY3Rpb24nIHx8ICFmdW5jKSByZXR1cm4gbnVsbDtcbiAgcmV0dXJuIGZ1bmMoKTtcbn1cblxuZnVuY3Rpb24gZ2V0UG9pbnQocG9pbnQpIHtcbiAgaWYgKHR5cGVvZiBwb2ludCAhPT0gJ29iamVjdCcgfHwgIXBvaW50KSByZXR1cm4gbnVsbDtcbiAgcmV0dXJuIHBvaW50O1xufVxuXG52YXIgQWxpZ24gPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoQWxpZ24sIF9Db21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEFsaWduKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBBbGlnbik7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIChfcmVmID0gQWxpZ24uX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihBbGlnbikpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLmZvcmNlQWxpZ24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBkaXNhYmxlZCA9IF90aGlzJHByb3BzLmRpc2FibGVkLFxuICAgICAgICAgIHRhcmdldCA9IF90aGlzJHByb3BzLnRhcmdldCxcbiAgICAgICAgICBhbGlnbiA9IF90aGlzJHByb3BzLmFsaWduLFxuICAgICAgICAgIG9uQWxpZ24gPSBfdGhpcyRwcm9wcy5vbkFsaWduO1xuXG4gICAgICBpZiAoIWRpc2FibGVkICYmIHRhcmdldCkge1xuICAgICAgICB2YXIgc291cmNlID0gUmVhY3RET00uZmluZERPTU5vZGUoX3RoaXMpO1xuXG4gICAgICAgIHZhciByZXN1bHQgPSB2b2lkIDA7XG4gICAgICAgIHZhciBlbGVtZW50ID0gZ2V0RWxlbWVudCh0YXJnZXQpO1xuICAgICAgICB2YXIgcG9pbnQgPSBnZXRQb2ludCh0YXJnZXQpO1xuXG4gICAgICAgIC8vIElFIGxvc2UgZm9jdXMgYWZ0ZXIgZWxlbWVudCByZWFsaWduXG4gICAgICAgIC8vIFdlIHNob3VsZCByZWNvcmQgYWN0aXZlRWxlbWVudCBhbmQgcmVzdG9yZSBsYXRlclxuICAgICAgICB2YXIgYWN0aXZlRWxlbWVudCA9IGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQ7XG5cbiAgICAgICAgaWYgKGVsZW1lbnQpIHtcbiAgICAgICAgICByZXN1bHQgPSBhbGlnbkVsZW1lbnQoc291cmNlLCBlbGVtZW50LCBhbGlnbik7XG4gICAgICAgIH0gZWxzZSBpZiAocG9pbnQpIHtcbiAgICAgICAgICByZXN1bHQgPSBhbGlnblBvaW50KHNvdXJjZSwgcG9pbnQsIGFsaWduKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlc3RvcmVGb2N1cyhhY3RpdmVFbGVtZW50LCBzb3VyY2UpO1xuXG4gICAgICAgIGlmIChvbkFsaWduKSB7XG4gICAgICAgICAgb25BbGlnbihzb3VyY2UsIHJlc3VsdCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBfdGVtcCksIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhBbGlnbiwgW3tcbiAgICBrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgdmFyIHByb3BzID0gdGhpcy5wcm9wcztcbiAgICAgIC8vIGlmIHBhcmVudCByZWYgbm90IGF0dGFjaGVkIC4uLi4gdXNlIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkXG4gICAgICB0aGlzLmZvcmNlQWxpZ24oKTtcbiAgICAgIGlmICghcHJvcHMuZGlzYWJsZWQgJiYgcHJvcHMubW9uaXRvcldpbmRvd1Jlc2l6ZSkge1xuICAgICAgICB0aGlzLnN0YXJ0TW9uaXRvcldpbmRvd1Jlc2l6ZSgpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMpIHtcbiAgICAgIHZhciByZUFsaWduID0gZmFsc2U7XG4gICAgICB2YXIgcHJvcHMgPSB0aGlzLnByb3BzO1xuXG4gICAgICBpZiAoIXByb3BzLmRpc2FibGVkKSB7XG4gICAgICAgIHZhciBzb3VyY2UgPSBSZWFjdERPTS5maW5kRE9NTm9kZSh0aGlzKTtcbiAgICAgICAgdmFyIHNvdXJjZVJlY3QgPSBzb3VyY2UgPyBzb3VyY2UuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkgOiBudWxsO1xuXG4gICAgICAgIGlmIChwcmV2UHJvcHMuZGlzYWJsZWQpIHtcbiAgICAgICAgICByZUFsaWduID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YXIgbGFzdEVsZW1lbnQgPSBnZXRFbGVtZW50KHByZXZQcm9wcy50YXJnZXQpO1xuICAgICAgICAgIHZhciBjdXJyZW50RWxlbWVudCA9IGdldEVsZW1lbnQocHJvcHMudGFyZ2V0KTtcbiAgICAgICAgICB2YXIgbGFzdFBvaW50ID0gZ2V0UG9pbnQocHJldlByb3BzLnRhcmdldCk7XG4gICAgICAgICAgdmFyIGN1cnJlbnRQb2ludCA9IGdldFBvaW50KHByb3BzLnRhcmdldCk7XG5cbiAgICAgICAgICBpZiAoaXNXaW5kb3cobGFzdEVsZW1lbnQpICYmIGlzV2luZG93KGN1cnJlbnRFbGVtZW50KSkge1xuICAgICAgICAgICAgLy8gU2tpcCBpZiBpcyB3aW5kb3dcbiAgICAgICAgICAgIHJlQWxpZ24gPSBmYWxzZTtcbiAgICAgICAgICB9IGVsc2UgaWYgKGxhc3RFbGVtZW50ICE9PSBjdXJyZW50RWxlbWVudCB8fCAvLyBFbGVtZW50IGNoYW5nZVxuICAgICAgICAgIGxhc3RFbGVtZW50ICYmICFjdXJyZW50RWxlbWVudCAmJiBjdXJyZW50UG9pbnQgfHwgLy8gQ2hhbmdlIGZyb20gZWxlbWVudCB0byBwb2ludFxuICAgICAgICAgIGxhc3RQb2ludCAmJiBjdXJyZW50UG9pbnQgJiYgY3VycmVudEVsZW1lbnQgfHwgLy8gQ2hhbmdlIGZyb20gcG9pbnQgdG8gZWxlbWVudFxuICAgICAgICAgIGN1cnJlbnRQb2ludCAmJiAhaXNTYW1lUG9pbnQobGFzdFBvaW50LCBjdXJyZW50UG9pbnQpKSB7XG4gICAgICAgICAgICByZUFsaWduID0gdHJ1ZTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyBJZiBzb3VyY2UgZWxlbWVudCBzaXplIGNoYW5nZWRcbiAgICAgICAgICB2YXIgcHJlUmVjdCA9IHRoaXMuc291cmNlUmVjdCB8fCB7fTtcbiAgICAgICAgICBpZiAoIXJlQWxpZ24gJiYgc291cmNlICYmICghaXNTaW1pbGFyVmFsdWUocHJlUmVjdC53aWR0aCwgc291cmNlUmVjdC53aWR0aCkgfHwgIWlzU2ltaWxhclZhbHVlKHByZVJlY3QuaGVpZ2h0LCBzb3VyY2VSZWN0LmhlaWdodCkpKSB7XG4gICAgICAgICAgICByZUFsaWduID0gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnNvdXJjZVJlY3QgPSBzb3VyY2VSZWN0O1xuICAgICAgfVxuXG4gICAgICBpZiAocmVBbGlnbikge1xuICAgICAgICB0aGlzLmZvcmNlQWxpZ24oKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHByb3BzLm1vbml0b3JXaW5kb3dSZXNpemUgJiYgIXByb3BzLmRpc2FibGVkKSB7XG4gICAgICAgIHRoaXMuc3RhcnRNb25pdG9yV2luZG93UmVzaXplKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnN0b3BNb25pdG9yV2luZG93UmVzaXplKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIHRoaXMuc3RvcE1vbml0b3JXaW5kb3dSZXNpemUoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdzdGFydE1vbml0b3JXaW5kb3dSZXNpemUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzdGFydE1vbml0b3JXaW5kb3dSZXNpemUoKSB7XG4gICAgICBpZiAoIXRoaXMucmVzaXplSGFuZGxlcikge1xuICAgICAgICB0aGlzLmJ1ZmZlck1vbml0b3IgPSBidWZmZXIodGhpcy5mb3JjZUFsaWduLCB0aGlzLnByb3BzLm1vbml0b3JCdWZmZXJUaW1lKTtcbiAgICAgICAgdGhpcy5yZXNpemVIYW5kbGVyID0gYWRkRXZlbnRMaXN0ZW5lcih3aW5kb3csICdyZXNpemUnLCB0aGlzLmJ1ZmZlck1vbml0b3IpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3N0b3BNb25pdG9yV2luZG93UmVzaXplJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gc3RvcE1vbml0b3JXaW5kb3dSZXNpemUoKSB7XG4gICAgICBpZiAodGhpcy5yZXNpemVIYW5kbGVyKSB7XG4gICAgICAgIHRoaXMuYnVmZmVyTW9uaXRvci5jbGVhcigpO1xuICAgICAgICB0aGlzLnJlc2l6ZUhhbmRsZXIucmVtb3ZlKCk7XG4gICAgICAgIHRoaXMucmVzaXplSGFuZGxlciA9IG51bGw7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuUHJvcHMgPSBfcHJvcHMuY2hpbGRyZW5Qcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbjtcblxuICAgICAgdmFyIGNoaWxkID0gUmVhY3QuQ2hpbGRyZW4ub25seShjaGlsZHJlbik7XG4gICAgICBpZiAoY2hpbGRyZW5Qcm9wcykge1xuICAgICAgICB2YXIgbmV3UHJvcHMgPSB7fTtcbiAgICAgICAgdmFyIHByb3BMaXN0ID0gT2JqZWN0LmtleXMoY2hpbGRyZW5Qcm9wcyk7XG4gICAgICAgIHByb3BMaXN0LmZvckVhY2goZnVuY3Rpb24gKHByb3ApIHtcbiAgICAgICAgICBuZXdQcm9wc1twcm9wXSA9IF90aGlzMi5wcm9wc1tjaGlsZHJlblByb3BzW3Byb3BdXTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuIFJlYWN0LmNsb25lRWxlbWVudChjaGlsZCwgbmV3UHJvcHMpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGNoaWxkO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBBbGlnbjtcbn0oQ29tcG9uZW50KTtcblxuQWxpZ24ucHJvcFR5cGVzID0ge1xuICBjaGlsZHJlblByb3BzOiBQcm9wVHlwZXMub2JqZWN0LFxuICBhbGlnbjogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICB0YXJnZXQ6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5mdW5jLCBQcm9wVHlwZXMuc2hhcGUoe1xuICAgIGNsaWVudFg6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgY2xpZW50WTogUHJvcFR5cGVzLm51bWJlcixcbiAgICBwYWdlWDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBwYWdlWTogUHJvcFR5cGVzLm51bWJlclxuICB9KV0pLFxuICBvbkFsaWduOiBQcm9wVHlwZXMuZnVuYyxcbiAgbW9uaXRvckJ1ZmZlclRpbWU6IFByb3BUeXBlcy5udW1iZXIsXG4gIG1vbml0b3JXaW5kb3dSZXNpemU6IFByb3BUeXBlcy5ib29sLFxuICBkaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gIGNoaWxkcmVuOiBQcm9wVHlwZXMuYW55XG59O1xuQWxpZ24uZGVmYXVsdFByb3BzID0ge1xuICB0YXJnZXQ6IGZ1bmN0aW9uIHRhcmdldCgpIHtcbiAgICByZXR1cm4gd2luZG93O1xuICB9LFxuICBtb25pdG9yQnVmZmVyVGltZTogNTAsXG4gIG1vbml0b3JXaW5kb3dSZXNpemU6IGZhbHNlLFxuICBkaXNhYmxlZDogZmFsc2Vcbn07XG5cblxuZXhwb3J0IGRlZmF1bHQgQWxpZ247Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFLQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUEvQ0E7QUFpREE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBcEJBO0FBQ0E7QUFzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVVBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-align/es/Align.js
