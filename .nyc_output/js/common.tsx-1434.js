__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleMaxLine", function() { return StyleMaxLine; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleAction", function() { return StyleAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultPageIcon", function() { return ResultPageIcon; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultPageParagraph", function() { return ResultPageParagraph; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleDateRange", function() { return StyleDateRange; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");


function _templateObject5() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        .date-group {\n            .ant-form-item-control-wrapper .ant-input {\n                border: 1px solid #ebebeb;\n                border-top-left-radius: 4px;\n                border-bottom-left-radius: 4px;\n            }\n            .ant-form-item {\n                width: 45%;\n            }\n            .to-date {\n                display: inline-block;\n                width: 10%;\n                text-align: center;\n                line-height: 42px;\n                font-size: 18px;\n            }\n            .group .ant-form-item:last-child {\n                width: 45%;\n            }\n         }\n    "]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    font-size: 16px;\n    margin: 0 0 15px 0;\n    text-align: center;\n    color: rgba(0, 0, 0, 0.85);\n    padding: 0 10px;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  padding: 44px 0 54px 0;\n  text-align: center;\n  color: ", ";\n  i {\n    font-size: 100px;\n  }\n  "]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n       \n          margin-bottom: 20px;\n          padding-top: 30px;\n          height: ", ";\n          button {\n              margin-left: .5em;\n              min-width: 7.5em;\n              overflow: hidden;\n              text-overflow: ellipsis;\n              white-space: nowrap;\n              float:right;\n              &:last-child {\n                  margin-right: .5em;\n              }\n              &:first-child {\n                  float: left;\n              }\n          }  \n       \n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    display: -webkit-box;\n    overflow: hidden;\n    white-space: normal!important;\n    text-overflow: ellipsis;\n    word-wrap: break-word;\n    -webkit-line-clamp: ", ";\n    -webkit-box-orient: vertical;\n  "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}





var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_2__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

var isMobile = _common__WEBPACK_IMPORTED_MODULE_3__["Utils"].getIsMobile();
var StyleMaxLine = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject(), function (props) {
  return props.maxLine ? props.maxLine : "1";
});
var StyleAction = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject2(), isMobile ? "auto" : "70px");
var ResultPageIcon = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject3(), COLOR_PRIMARY);
var ResultPageParagraph = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject4());
var StyleDateRange = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject5());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svc3R5bGVzL2NvbW1vbi50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9zdHlsZXMvY29tbW9uLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXNcIjtcbmltcG9ydCB7IFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcblxuY29uc3QgeyBDT0xPUl9QUklNQVJZIH0gPSBUaGVtZS5nZXRUaGVtZSgpO1xuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuZXhwb3J0IGNvbnN0IFN0eWxlTWF4TGluZTogYW55ID0gU3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHdoaXRlLXNwYWNlOiBub3JtYWwhaW1wb3J0YW50O1xuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgIHdvcmQtd3JhcDogYnJlYWstd29yZDtcbiAgICAtd2Via2l0LWxpbmUtY2xhbXA6ICR7KHByb3BzOiBhbnkpID0+IHByb3BzLm1heExpbmUgPyBwcm9wcy5tYXhMaW5lIDogXCIxXCJ9O1xuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XG4gIGA7XG5cblxuZXhwb3J0IGNvbnN0IFN0eWxlQWN0aW9uOiBhbnkgPSBTdHlsZWQuZGl2YFxuICAgICAgIFxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgICAgICAgcGFkZGluZy10b3A6IDMwcHg7XG4gICAgICAgICAgaGVpZ2h0OiAke2lzTW9iaWxlID8gXCJhdXRvXCIgOiBcIjcwcHhcIn07XG4gICAgICAgICAgYnV0dG9uIHtcbiAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC41ZW07XG4gICAgICAgICAgICAgIG1pbi13aWR0aDogNy41ZW07XG4gICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICAgICAgICBmbG9hdDpyaWdodDtcbiAgICAgICAgICAgICAgJjpsYXN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogLjVlbTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSAgXG4gICAgICAgXG5gO1xuXG5cbmV4cG9ydCBjb25zdCBSZXN1bHRQYWdlSWNvbiA9IFN0eWxlZC5kaXZgXG4gIHBhZGRpbmc6IDQ0cHggMCA1NHB4IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICR7Q09MT1JfUFJJTUFSWX07XG4gIGkge1xuICAgIGZvbnQtc2l6ZTogMTAwcHg7XG4gIH1cbiAgYDtcblxuZXhwb3J0IGNvbnN0IFJlc3VsdFBhZ2VQYXJhZ3JhcGggPSBTdHlsZWQuZGl2YFxuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBtYXJnaW46IDAgMCAxNXB4IDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuODUpO1xuICAgIHBhZGRpbmc6IDAgMTBweDtcbmA7XG5cblxuZXhwb3J0IGNvbnN0IFN0eWxlRGF0ZVJhbmdlID0gU3R5bGVkLmRpdmBcbiAgICAgICAgLmRhdGUtZ3JvdXAge1xuICAgICAgICAgICAgLmFudC1mb3JtLWl0ZW0tY29udHJvbC13cmFwcGVyIC5hbnQtaW5wdXQge1xuICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNlYmViZWI7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogNHB4O1xuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDRweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5hbnQtZm9ybS1pdGVtIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogNDUlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnRvLWRhdGUge1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAlO1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogNDJweDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuZ3JvdXAgLmFudC1mb3JtLWl0ZW06bGFzdC1jaGlsZCB7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDQ1JTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgIH1cbiAgICBgO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQU1BO0FBQUE7QUFLQTtBQXVCQTtBQVNBO0FBU0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/styles/common.tsx
