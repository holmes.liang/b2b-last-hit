/**
 * Module dependencies
 */
var matches = __webpack_require__(/*! dom-matches */ "./node_modules/dom-matches/index.js");
/**
 * @param element {Element}
 * @param selector {String}
 * @param context {Element}
 * @return {Element}
 */


module.exports = function (element, selector, context) {
  context = context || document; // guard against orphans

  element = {
    parentNode: element
  };

  while ((element = element.parentNode) && element !== context) {
    if (matches(element, selector)) {
      return element;
    }
  }
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZG9tLWNsb3Nlc3QvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kb20tY2xvc2VzdC9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIE1vZHVsZSBkZXBlbmRlbmNpZXNcbiAqL1xuXG52YXIgbWF0Y2hlcyA9IHJlcXVpcmUoJ2RvbS1tYXRjaGVzJyk7XG5cbi8qKlxuICogQHBhcmFtIGVsZW1lbnQge0VsZW1lbnR9XG4gKiBAcGFyYW0gc2VsZWN0b3Ige1N0cmluZ31cbiAqIEBwYXJhbSBjb250ZXh0IHtFbGVtZW50fVxuICogQHJldHVybiB7RWxlbWVudH1cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoZWxlbWVudCwgc2VsZWN0b3IsIGNvbnRleHQpIHtcbiAgY29udGV4dCA9IGNvbnRleHQgfHwgZG9jdW1lbnQ7XG4gIC8vIGd1YXJkIGFnYWluc3Qgb3JwaGFuc1xuICBlbGVtZW50ID0geyBwYXJlbnROb2RlOiBlbGVtZW50IH07XG5cbiAgd2hpbGUgKChlbGVtZW50ID0gZWxlbWVudC5wYXJlbnROb2RlKSAmJiBlbGVtZW50ICE9PSBjb250ZXh0KSB7XG4gICAgaWYgKG1hdGNoZXMoZWxlbWVudCwgc2VsZWN0b3IpKSB7XG4gICAgICByZXR1cm4gZWxlbWVudDtcbiAgICB9XG4gIH1cbn07XG4iXSwibWFwcGluZ3MiOiJBQUFBOzs7QUFJQTtBQUVBOzs7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/dom-closest/index.js
