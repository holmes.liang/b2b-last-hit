/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule removeRangeFromContentState
 * @format
 * 
 */


var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var getNextDelimiterBlockKey = __webpack_require__(/*! ./getNextDelimiterBlockKey */ "./node_modules/draft-js/lib/getNextDelimiterBlockKey.js");

var List = Immutable.List,
    Map = Immutable.Map;

var transformBlock = function transformBlock(key, blockMap, func) {
  if (!key) {
    return;
  }

  var block = blockMap.get(key);

  if (!block) {
    return;
  }

  blockMap.set(key, func(block));
};
/**
 * Ancestors needs to be preserved when there are non selected
 * children to make sure we do not leave any orphans behind
 */


var getAncestorsKeys = function getAncestorsKeys(blockKey, blockMap) {
  var parents = [];

  if (!blockKey) {
    return parents;
  }

  var blockNode = blockMap.get(blockKey);

  while (blockNode && blockNode.getParentKey()) {
    var parentKey = blockNode.getParentKey();

    if (parentKey) {
      parents.push(parentKey);
    }

    blockNode = parentKey ? blockMap.get(parentKey) : null;
  }

  return parents;
};
/**
 * Get all next delimiter keys until we hit a root delimiter and return
 * an array of key references
 */


var getNextDelimitersBlockKeys = function getNextDelimitersBlockKeys(block, blockMap) {
  var nextDelimiters = [];

  if (!block) {
    return nextDelimiters;
  }

  var nextDelimiter = getNextDelimiterBlockKey(block, blockMap);

  while (nextDelimiter && blockMap.get(nextDelimiter)) {
    var _block = blockMap.get(nextDelimiter);

    nextDelimiters.push(nextDelimiter); // we do not need to keep checking all root node siblings, just the first occurance

    nextDelimiter = _block.getParentKey() ? getNextDelimiterBlockKey(_block, blockMap) : null;
  }

  return nextDelimiters;
};

var getNextValidSibling = function getNextValidSibling(block, blockMap, originalBlockMap) {
  if (!block) {
    return null;
  } // note that we need to make sure we refer to the original block since this
  // function is called within a withMutations


  var nextValidSiblingKey = originalBlockMap.get(block.getKey()).getNextSiblingKey();

  while (nextValidSiblingKey && !blockMap.get(nextValidSiblingKey)) {
    nextValidSiblingKey = originalBlockMap.get(nextValidSiblingKey).getNextSiblingKey() || null;
  }

  return nextValidSiblingKey;
};

var getPrevValidSibling = function getPrevValidSibling(block, blockMap, originalBlockMap) {
  if (!block) {
    return null;
  } // note that we need to make sure we refer to the original block since this
  // function is called within a withMutations


  var prevValidSiblingKey = originalBlockMap.get(block.getKey()).getPrevSiblingKey();

  while (prevValidSiblingKey && !blockMap.get(prevValidSiblingKey)) {
    prevValidSiblingKey = originalBlockMap.get(prevValidSiblingKey).getPrevSiblingKey() || null;
  }

  return prevValidSiblingKey;
};

var updateBlockMapLinks = function updateBlockMapLinks(blockMap, startBlock, endBlock, originalBlockMap) {
  return blockMap.withMutations(function (blocks) {
    // update start block if its retained
    transformBlock(startBlock.getKey(), blocks, function (block) {
      return block.merge({
        nextSibling: getNextValidSibling(startBlock, blocks, originalBlockMap),
        prevSibling: getPrevValidSibling(startBlock, blocks, originalBlockMap)
      });
    }); // update endblock if its retained

    transformBlock(endBlock.getKey(), blocks, function (block) {
      return block.merge({
        nextSibling: getNextValidSibling(endBlock, blocks, originalBlockMap),
        prevSibling: getPrevValidSibling(endBlock, blocks, originalBlockMap)
      });
    }); // update start block parent ancestors

    getAncestorsKeys(startBlock.getKey(), originalBlockMap).forEach(function (parentKey) {
      return transformBlock(parentKey, blocks, function (block) {
        return block.merge({
          children: block.getChildKeys().filter(function (key) {
            return blocks.get(key);
          }),
          nextSibling: getNextValidSibling(block, blocks, originalBlockMap),
          prevSibling: getPrevValidSibling(block, blocks, originalBlockMap)
        });
      });
    }); // update start block next - can only happen if startBlock == endBlock

    transformBlock(startBlock.getNextSiblingKey(), blocks, function (block) {
      return block.merge({
        prevSibling: startBlock.getPrevSiblingKey()
      });
    }); // update start block prev

    transformBlock(startBlock.getPrevSiblingKey(), blocks, function (block) {
      return block.merge({
        nextSibling: getNextValidSibling(startBlock, blocks, originalBlockMap)
      });
    }); // update end block next

    transformBlock(endBlock.getNextSiblingKey(), blocks, function (block) {
      return block.merge({
        prevSibling: getPrevValidSibling(endBlock, blocks, originalBlockMap)
      });
    }); // update end block prev

    transformBlock(endBlock.getPrevSiblingKey(), blocks, function (block) {
      return block.merge({
        nextSibling: endBlock.getNextSiblingKey()
      });
    }); // update end block parent ancestors

    getAncestorsKeys(endBlock.getKey(), originalBlockMap).forEach(function (parentKey) {
      transformBlock(parentKey, blocks, function (block) {
        return block.merge({
          children: block.getChildKeys().filter(function (key) {
            return blocks.get(key);
          }),
          nextSibling: getNextValidSibling(block, blocks, originalBlockMap),
          prevSibling: getPrevValidSibling(block, blocks, originalBlockMap)
        });
      });
    }); // update next delimiters all the way to a root delimiter

    getNextDelimitersBlockKeys(endBlock, originalBlockMap).forEach(function (delimiterKey) {
      return transformBlock(delimiterKey, blocks, function (block) {
        return block.merge({
          nextSibling: getNextValidSibling(block, blocks, originalBlockMap),
          prevSibling: getPrevValidSibling(block, blocks, originalBlockMap)
        });
      });
    });
  });
};

var removeRangeFromContentState = function removeRangeFromContentState(contentState, selectionState) {
  if (selectionState.isCollapsed()) {
    return contentState;
  }

  var blockMap = contentState.getBlockMap();
  var startKey = selectionState.getStartKey();
  var startOffset = selectionState.getStartOffset();
  var endKey = selectionState.getEndKey();
  var endOffset = selectionState.getEndOffset();
  var startBlock = blockMap.get(startKey);
  var endBlock = blockMap.get(endKey); // we assume that ContentBlockNode and ContentBlocks are not mixed together

  var isExperimentalTreeBlock = startBlock instanceof ContentBlockNode; // used to retain blocks that should not be deleted to avoid orphan children

  var parentAncestors = [];

  if (isExperimentalTreeBlock) {
    var endBlockchildrenKeys = endBlock.getChildKeys();
    var endBlockAncestors = getAncestorsKeys(endKey, blockMap); // endBlock has unselected sibblings so we can not remove its ancestors parents

    if (endBlock.getNextSiblingKey()) {
      parentAncestors = parentAncestors.concat(endBlockAncestors);
    } // endBlock has children so can not remove this block or any of its ancestors


    if (!endBlockchildrenKeys.isEmpty()) {
      parentAncestors = parentAncestors.concat(endBlockAncestors.concat([endKey]));
    } // we need to retain all ancestors of the next delimiter block


    parentAncestors = parentAncestors.concat(getAncestorsKeys(getNextDelimiterBlockKey(endBlock, blockMap), blockMap));
  }

  var characterList = void 0;

  if (startBlock === endBlock) {
    characterList = removeFromList(startBlock.getCharacterList(), startOffset, endOffset);
  } else {
    characterList = startBlock.getCharacterList().slice(0, startOffset).concat(endBlock.getCharacterList().slice(endOffset));
  }

  var modifiedStart = startBlock.merge({
    text: startBlock.getText().slice(0, startOffset) + endBlock.getText().slice(endOffset),
    characterList: characterList
  });
  var newBlocks = blockMap.toSeq().skipUntil(function (_, k) {
    return k === startKey;
  }).takeUntil(function (_, k) {
    return k === endKey;
  }).filter(function (_, k) {
    return parentAncestors.indexOf(k) === -1;
  }).concat(Map([[endKey, null]])).map(function (_, k) {
    return k === startKey ? modifiedStart : null;
  });
  var updatedBlockMap = blockMap.merge(newBlocks).filter(function (block) {
    return !!block;
  });

  if (isExperimentalTreeBlock) {
    updatedBlockMap = updateBlockMapLinks(updatedBlockMap, startBlock, endBlock, blockMap);
  }

  return contentState.merge({
    blockMap: updatedBlockMap,
    selectionBefore: selectionState,
    selectionAfter: selectionState.merge({
      anchorKey: startKey,
      anchorOffset: startOffset,
      focusKey: startKey,
      focusOffset: startOffset,
      isBackward: false
    })
  });
};
/**
 * Maintain persistence for target list when removing characters on the
 * head and tail of the character list.
 */


var removeFromList = function removeFromList(targetList, startOffset, endOffset) {
  if (startOffset === 0) {
    while (startOffset < endOffset) {
      targetList = targetList.shift();
      startOffset++;
    }
  } else if (endOffset === targetList.count()) {
    while (endOffset > startOffset) {
      targetList = targetList.pop();
      endOffset--;
    }
  } else {
    var head = targetList.slice(0, startOffset);
    var tail = targetList.slice(endOffset);
    targetList = head.concat(tail).toList();
  }

  return targetList;
};

module.exports = removeRangeFromContentState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL3JlbW92ZVJhbmdlRnJvbUNvbnRlbnRTdGF0ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9yZW1vdmVSYW5nZUZyb21Db250ZW50U3RhdGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSByZW1vdmVSYW5nZUZyb21Db250ZW50U3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIENvbnRlbnRCbG9ja05vZGUgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9ja05vZGUnKTtcbnZhciBJbW11dGFibGUgPSByZXF1aXJlKCdpbW11dGFibGUnKTtcblxudmFyIGdldE5leHREZWxpbWl0ZXJCbG9ja0tleSA9IHJlcXVpcmUoJy4vZ2V0TmV4dERlbGltaXRlckJsb2NrS2V5Jyk7XG5cbnZhciBMaXN0ID0gSW1tdXRhYmxlLkxpc3QsXG4gICAgTWFwID0gSW1tdXRhYmxlLk1hcDtcblxuXG52YXIgdHJhbnNmb3JtQmxvY2sgPSBmdW5jdGlvbiB0cmFuc2Zvcm1CbG9jayhrZXksIGJsb2NrTWFwLCBmdW5jKSB7XG4gIGlmICgha2V5KSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIGJsb2NrID0gYmxvY2tNYXAuZ2V0KGtleSk7XG5cbiAgaWYgKCFibG9jaykge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGJsb2NrTWFwLnNldChrZXksIGZ1bmMoYmxvY2spKTtcbn07XG5cbi8qKlxuICogQW5jZXN0b3JzIG5lZWRzIHRvIGJlIHByZXNlcnZlZCB3aGVuIHRoZXJlIGFyZSBub24gc2VsZWN0ZWRcbiAqIGNoaWxkcmVuIHRvIG1ha2Ugc3VyZSB3ZSBkbyBub3QgbGVhdmUgYW55IG9ycGhhbnMgYmVoaW5kXG4gKi9cbnZhciBnZXRBbmNlc3RvcnNLZXlzID0gZnVuY3Rpb24gZ2V0QW5jZXN0b3JzS2V5cyhibG9ja0tleSwgYmxvY2tNYXApIHtcbiAgdmFyIHBhcmVudHMgPSBbXTtcblxuICBpZiAoIWJsb2NrS2V5KSB7XG4gICAgcmV0dXJuIHBhcmVudHM7XG4gIH1cblxuICB2YXIgYmxvY2tOb2RlID0gYmxvY2tNYXAuZ2V0KGJsb2NrS2V5KTtcbiAgd2hpbGUgKGJsb2NrTm9kZSAmJiBibG9ja05vZGUuZ2V0UGFyZW50S2V5KCkpIHtcbiAgICB2YXIgcGFyZW50S2V5ID0gYmxvY2tOb2RlLmdldFBhcmVudEtleSgpO1xuICAgIGlmIChwYXJlbnRLZXkpIHtcbiAgICAgIHBhcmVudHMucHVzaChwYXJlbnRLZXkpO1xuICAgIH1cbiAgICBibG9ja05vZGUgPSBwYXJlbnRLZXkgPyBibG9ja01hcC5nZXQocGFyZW50S2V5KSA6IG51bGw7XG4gIH1cblxuICByZXR1cm4gcGFyZW50cztcbn07XG5cbi8qKlxuICogR2V0IGFsbCBuZXh0IGRlbGltaXRlciBrZXlzIHVudGlsIHdlIGhpdCBhIHJvb3QgZGVsaW1pdGVyIGFuZCByZXR1cm5cbiAqIGFuIGFycmF5IG9mIGtleSByZWZlcmVuY2VzXG4gKi9cbnZhciBnZXROZXh0RGVsaW1pdGVyc0Jsb2NrS2V5cyA9IGZ1bmN0aW9uIGdldE5leHREZWxpbWl0ZXJzQmxvY2tLZXlzKGJsb2NrLCBibG9ja01hcCkge1xuICB2YXIgbmV4dERlbGltaXRlcnMgPSBbXTtcblxuICBpZiAoIWJsb2NrKSB7XG4gICAgcmV0dXJuIG5leHREZWxpbWl0ZXJzO1xuICB9XG5cbiAgdmFyIG5leHREZWxpbWl0ZXIgPSBnZXROZXh0RGVsaW1pdGVyQmxvY2tLZXkoYmxvY2ssIGJsb2NrTWFwKTtcbiAgd2hpbGUgKG5leHREZWxpbWl0ZXIgJiYgYmxvY2tNYXAuZ2V0KG5leHREZWxpbWl0ZXIpKSB7XG4gICAgdmFyIF9ibG9jayA9IGJsb2NrTWFwLmdldChuZXh0RGVsaW1pdGVyKTtcbiAgICBuZXh0RGVsaW1pdGVycy5wdXNoKG5leHREZWxpbWl0ZXIpO1xuXG4gICAgLy8gd2UgZG8gbm90IG5lZWQgdG8ga2VlcCBjaGVja2luZyBhbGwgcm9vdCBub2RlIHNpYmxpbmdzLCBqdXN0IHRoZSBmaXJzdCBvY2N1cmFuY2VcbiAgICBuZXh0RGVsaW1pdGVyID0gX2Jsb2NrLmdldFBhcmVudEtleSgpID8gZ2V0TmV4dERlbGltaXRlckJsb2NrS2V5KF9ibG9jaywgYmxvY2tNYXApIDogbnVsbDtcbiAgfVxuXG4gIHJldHVybiBuZXh0RGVsaW1pdGVycztcbn07XG5cbnZhciBnZXROZXh0VmFsaWRTaWJsaW5nID0gZnVuY3Rpb24gZ2V0TmV4dFZhbGlkU2libGluZyhibG9jaywgYmxvY2tNYXAsIG9yaWdpbmFsQmxvY2tNYXApIHtcbiAgaWYgKCFibG9jaykge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgLy8gbm90ZSB0aGF0IHdlIG5lZWQgdG8gbWFrZSBzdXJlIHdlIHJlZmVyIHRvIHRoZSBvcmlnaW5hbCBibG9jayBzaW5jZSB0aGlzXG4gIC8vIGZ1bmN0aW9uIGlzIGNhbGxlZCB3aXRoaW4gYSB3aXRoTXV0YXRpb25zXG4gIHZhciBuZXh0VmFsaWRTaWJsaW5nS2V5ID0gb3JpZ2luYWxCbG9ja01hcC5nZXQoYmxvY2suZ2V0S2V5KCkpLmdldE5leHRTaWJsaW5nS2V5KCk7XG5cbiAgd2hpbGUgKG5leHRWYWxpZFNpYmxpbmdLZXkgJiYgIWJsb2NrTWFwLmdldChuZXh0VmFsaWRTaWJsaW5nS2V5KSkge1xuICAgIG5leHRWYWxpZFNpYmxpbmdLZXkgPSBvcmlnaW5hbEJsb2NrTWFwLmdldChuZXh0VmFsaWRTaWJsaW5nS2V5KS5nZXROZXh0U2libGluZ0tleSgpIHx8IG51bGw7XG4gIH1cblxuICByZXR1cm4gbmV4dFZhbGlkU2libGluZ0tleTtcbn07XG5cbnZhciBnZXRQcmV2VmFsaWRTaWJsaW5nID0gZnVuY3Rpb24gZ2V0UHJldlZhbGlkU2libGluZyhibG9jaywgYmxvY2tNYXAsIG9yaWdpbmFsQmxvY2tNYXApIHtcbiAgaWYgKCFibG9jaykge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgLy8gbm90ZSB0aGF0IHdlIG5lZWQgdG8gbWFrZSBzdXJlIHdlIHJlZmVyIHRvIHRoZSBvcmlnaW5hbCBibG9jayBzaW5jZSB0aGlzXG4gIC8vIGZ1bmN0aW9uIGlzIGNhbGxlZCB3aXRoaW4gYSB3aXRoTXV0YXRpb25zXG4gIHZhciBwcmV2VmFsaWRTaWJsaW5nS2V5ID0gb3JpZ2luYWxCbG9ja01hcC5nZXQoYmxvY2suZ2V0S2V5KCkpLmdldFByZXZTaWJsaW5nS2V5KCk7XG5cbiAgd2hpbGUgKHByZXZWYWxpZFNpYmxpbmdLZXkgJiYgIWJsb2NrTWFwLmdldChwcmV2VmFsaWRTaWJsaW5nS2V5KSkge1xuICAgIHByZXZWYWxpZFNpYmxpbmdLZXkgPSBvcmlnaW5hbEJsb2NrTWFwLmdldChwcmV2VmFsaWRTaWJsaW5nS2V5KS5nZXRQcmV2U2libGluZ0tleSgpIHx8IG51bGw7XG4gIH1cblxuICByZXR1cm4gcHJldlZhbGlkU2libGluZ0tleTtcbn07XG5cbnZhciB1cGRhdGVCbG9ja01hcExpbmtzID0gZnVuY3Rpb24gdXBkYXRlQmxvY2tNYXBMaW5rcyhibG9ja01hcCwgc3RhcnRCbG9jaywgZW5kQmxvY2ssIG9yaWdpbmFsQmxvY2tNYXApIHtcbiAgcmV0dXJuIGJsb2NrTWFwLndpdGhNdXRhdGlvbnMoZnVuY3Rpb24gKGJsb2Nrcykge1xuICAgIC8vIHVwZGF0ZSBzdGFydCBibG9jayBpZiBpdHMgcmV0YWluZWRcbiAgICB0cmFuc2Zvcm1CbG9jayhzdGFydEJsb2NrLmdldEtleSgpLCBibG9ja3MsIGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHtcbiAgICAgICAgbmV4dFNpYmxpbmc6IGdldE5leHRWYWxpZFNpYmxpbmcoc3RhcnRCbG9jaywgYmxvY2tzLCBvcmlnaW5hbEJsb2NrTWFwKSxcbiAgICAgICAgcHJldlNpYmxpbmc6IGdldFByZXZWYWxpZFNpYmxpbmcoc3RhcnRCbG9jaywgYmxvY2tzLCBvcmlnaW5hbEJsb2NrTWFwKVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICAvLyB1cGRhdGUgZW5kYmxvY2sgaWYgaXRzIHJldGFpbmVkXG4gICAgdHJhbnNmb3JtQmxvY2soZW5kQmxvY2suZ2V0S2V5KCksIGJsb2NrcywgZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgICByZXR1cm4gYmxvY2subWVyZ2Uoe1xuICAgICAgICBuZXh0U2libGluZzogZ2V0TmV4dFZhbGlkU2libGluZyhlbmRCbG9jaywgYmxvY2tzLCBvcmlnaW5hbEJsb2NrTWFwKSxcbiAgICAgICAgcHJldlNpYmxpbmc6IGdldFByZXZWYWxpZFNpYmxpbmcoZW5kQmxvY2ssIGJsb2Nrcywgb3JpZ2luYWxCbG9ja01hcClcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgLy8gdXBkYXRlIHN0YXJ0IGJsb2NrIHBhcmVudCBhbmNlc3RvcnNcbiAgICBnZXRBbmNlc3RvcnNLZXlzKHN0YXJ0QmxvY2suZ2V0S2V5KCksIG9yaWdpbmFsQmxvY2tNYXApLmZvckVhY2goZnVuY3Rpb24gKHBhcmVudEtleSkge1xuICAgICAgcmV0dXJuIHRyYW5zZm9ybUJsb2NrKHBhcmVudEtleSwgYmxvY2tzLCBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHtcbiAgICAgICAgICBjaGlsZHJlbjogYmxvY2suZ2V0Q2hpbGRLZXlzKCkuZmlsdGVyKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIHJldHVybiBibG9ja3MuZ2V0KGtleSk7XG4gICAgICAgICAgfSksXG4gICAgICAgICAgbmV4dFNpYmxpbmc6IGdldE5leHRWYWxpZFNpYmxpbmcoYmxvY2ssIGJsb2Nrcywgb3JpZ2luYWxCbG9ja01hcCksXG4gICAgICAgICAgcHJldlNpYmxpbmc6IGdldFByZXZWYWxpZFNpYmxpbmcoYmxvY2ssIGJsb2Nrcywgb3JpZ2luYWxCbG9ja01hcClcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIC8vIHVwZGF0ZSBzdGFydCBibG9jayBuZXh0IC0gY2FuIG9ubHkgaGFwcGVuIGlmIHN0YXJ0QmxvY2sgPT0gZW5kQmxvY2tcbiAgICB0cmFuc2Zvcm1CbG9jayhzdGFydEJsb2NrLmdldE5leHRTaWJsaW5nS2V5KCksIGJsb2NrcywgZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgICByZXR1cm4gYmxvY2subWVyZ2Uoe1xuICAgICAgICBwcmV2U2libGluZzogc3RhcnRCbG9jay5nZXRQcmV2U2libGluZ0tleSgpXG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIC8vIHVwZGF0ZSBzdGFydCBibG9jayBwcmV2XG4gICAgdHJhbnNmb3JtQmxvY2soc3RhcnRCbG9jay5nZXRQcmV2U2libGluZ0tleSgpLCBibG9ja3MsIGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHtcbiAgICAgICAgbmV4dFNpYmxpbmc6IGdldE5leHRWYWxpZFNpYmxpbmcoc3RhcnRCbG9jaywgYmxvY2tzLCBvcmlnaW5hbEJsb2NrTWFwKVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICAvLyB1cGRhdGUgZW5kIGJsb2NrIG5leHRcbiAgICB0cmFuc2Zvcm1CbG9jayhlbmRCbG9jay5nZXROZXh0U2libGluZ0tleSgpLCBibG9ja3MsIGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHtcbiAgICAgICAgcHJldlNpYmxpbmc6IGdldFByZXZWYWxpZFNpYmxpbmcoZW5kQmxvY2ssIGJsb2Nrcywgb3JpZ2luYWxCbG9ja01hcClcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgLy8gdXBkYXRlIGVuZCBibG9jayBwcmV2XG4gICAgdHJhbnNmb3JtQmxvY2soZW5kQmxvY2suZ2V0UHJldlNpYmxpbmdLZXkoKSwgYmxvY2tzLCBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgIHJldHVybiBibG9jay5tZXJnZSh7XG4gICAgICAgIG5leHRTaWJsaW5nOiBlbmRCbG9jay5nZXROZXh0U2libGluZ0tleSgpXG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIC8vIHVwZGF0ZSBlbmQgYmxvY2sgcGFyZW50IGFuY2VzdG9yc1xuICAgIGdldEFuY2VzdG9yc0tleXMoZW5kQmxvY2suZ2V0S2V5KCksIG9yaWdpbmFsQmxvY2tNYXApLmZvckVhY2goZnVuY3Rpb24gKHBhcmVudEtleSkge1xuICAgICAgdHJhbnNmb3JtQmxvY2socGFyZW50S2V5LCBibG9ja3MsIGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgICByZXR1cm4gYmxvY2subWVyZ2Uoe1xuICAgICAgICAgIGNoaWxkcmVuOiBibG9jay5nZXRDaGlsZEtleXMoKS5maWx0ZXIoZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgICAgcmV0dXJuIGJsb2Nrcy5nZXQoa2V5KTtcbiAgICAgICAgICB9KSxcbiAgICAgICAgICBuZXh0U2libGluZzogZ2V0TmV4dFZhbGlkU2libGluZyhibG9jaywgYmxvY2tzLCBvcmlnaW5hbEJsb2NrTWFwKSxcbiAgICAgICAgICBwcmV2U2libGluZzogZ2V0UHJldlZhbGlkU2libGluZyhibG9jaywgYmxvY2tzLCBvcmlnaW5hbEJsb2NrTWFwKVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgLy8gdXBkYXRlIG5leHQgZGVsaW1pdGVycyBhbGwgdGhlIHdheSB0byBhIHJvb3QgZGVsaW1pdGVyXG4gICAgZ2V0TmV4dERlbGltaXRlcnNCbG9ja0tleXMoZW5kQmxvY2ssIG9yaWdpbmFsQmxvY2tNYXApLmZvckVhY2goZnVuY3Rpb24gKGRlbGltaXRlcktleSkge1xuICAgICAgcmV0dXJuIHRyYW5zZm9ybUJsb2NrKGRlbGltaXRlcktleSwgYmxvY2tzLCBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHtcbiAgICAgICAgICBuZXh0U2libGluZzogZ2V0TmV4dFZhbGlkU2libGluZyhibG9jaywgYmxvY2tzLCBvcmlnaW5hbEJsb2NrTWFwKSxcbiAgICAgICAgICBwcmV2U2libGluZzogZ2V0UHJldlZhbGlkU2libGluZyhibG9jaywgYmxvY2tzLCBvcmlnaW5hbEJsb2NrTWFwKVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9KTtcbn07XG5cbnZhciByZW1vdmVSYW5nZUZyb21Db250ZW50U3RhdGUgPSBmdW5jdGlvbiByZW1vdmVSYW5nZUZyb21Db250ZW50U3RhdGUoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSkge1xuICBpZiAoc2VsZWN0aW9uU3RhdGUuaXNDb2xsYXBzZWQoKSkge1xuICAgIHJldHVybiBjb250ZW50U3RhdGU7XG4gIH1cblxuICB2YXIgYmxvY2tNYXAgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tNYXAoKTtcbiAgdmFyIHN0YXJ0S2V5ID0gc2VsZWN0aW9uU3RhdGUuZ2V0U3RhcnRLZXkoKTtcbiAgdmFyIHN0YXJ0T2Zmc2V0ID0gc2VsZWN0aW9uU3RhdGUuZ2V0U3RhcnRPZmZzZXQoKTtcbiAgdmFyIGVuZEtleSA9IHNlbGVjdGlvblN0YXRlLmdldEVuZEtleSgpO1xuICB2YXIgZW5kT2Zmc2V0ID0gc2VsZWN0aW9uU3RhdGUuZ2V0RW5kT2Zmc2V0KCk7XG5cbiAgdmFyIHN0YXJ0QmxvY2sgPSBibG9ja01hcC5nZXQoc3RhcnRLZXkpO1xuICB2YXIgZW5kQmxvY2sgPSBibG9ja01hcC5nZXQoZW5kS2V5KTtcblxuICAvLyB3ZSBhc3N1bWUgdGhhdCBDb250ZW50QmxvY2tOb2RlIGFuZCBDb250ZW50QmxvY2tzIGFyZSBub3QgbWl4ZWQgdG9nZXRoZXJcbiAgdmFyIGlzRXhwZXJpbWVudGFsVHJlZUJsb2NrID0gc3RhcnRCbG9jayBpbnN0YW5jZW9mIENvbnRlbnRCbG9ja05vZGU7XG5cbiAgLy8gdXNlZCB0byByZXRhaW4gYmxvY2tzIHRoYXQgc2hvdWxkIG5vdCBiZSBkZWxldGVkIHRvIGF2b2lkIG9ycGhhbiBjaGlsZHJlblxuICB2YXIgcGFyZW50QW5jZXN0b3JzID0gW107XG5cbiAgaWYgKGlzRXhwZXJpbWVudGFsVHJlZUJsb2NrKSB7XG4gICAgdmFyIGVuZEJsb2NrY2hpbGRyZW5LZXlzID0gZW5kQmxvY2suZ2V0Q2hpbGRLZXlzKCk7XG4gICAgdmFyIGVuZEJsb2NrQW5jZXN0b3JzID0gZ2V0QW5jZXN0b3JzS2V5cyhlbmRLZXksIGJsb2NrTWFwKTtcblxuICAgIC8vIGVuZEJsb2NrIGhhcyB1bnNlbGVjdGVkIHNpYmJsaW5ncyBzbyB3ZSBjYW4gbm90IHJlbW92ZSBpdHMgYW5jZXN0b3JzIHBhcmVudHNcbiAgICBpZiAoZW5kQmxvY2suZ2V0TmV4dFNpYmxpbmdLZXkoKSkge1xuICAgICAgcGFyZW50QW5jZXN0b3JzID0gcGFyZW50QW5jZXN0b3JzLmNvbmNhdChlbmRCbG9ja0FuY2VzdG9ycyk7XG4gICAgfVxuXG4gICAgLy8gZW5kQmxvY2sgaGFzIGNoaWxkcmVuIHNvIGNhbiBub3QgcmVtb3ZlIHRoaXMgYmxvY2sgb3IgYW55IG9mIGl0cyBhbmNlc3RvcnNcbiAgICBpZiAoIWVuZEJsb2NrY2hpbGRyZW5LZXlzLmlzRW1wdHkoKSkge1xuICAgICAgcGFyZW50QW5jZXN0b3JzID0gcGFyZW50QW5jZXN0b3JzLmNvbmNhdChlbmRCbG9ja0FuY2VzdG9ycy5jb25jYXQoW2VuZEtleV0pKTtcbiAgICB9XG5cbiAgICAvLyB3ZSBuZWVkIHRvIHJldGFpbiBhbGwgYW5jZXN0b3JzIG9mIHRoZSBuZXh0IGRlbGltaXRlciBibG9ja1xuICAgIHBhcmVudEFuY2VzdG9ycyA9IHBhcmVudEFuY2VzdG9ycy5jb25jYXQoZ2V0QW5jZXN0b3JzS2V5cyhnZXROZXh0RGVsaW1pdGVyQmxvY2tLZXkoZW5kQmxvY2ssIGJsb2NrTWFwKSwgYmxvY2tNYXApKTtcbiAgfVxuXG4gIHZhciBjaGFyYWN0ZXJMaXN0ID0gdm9pZCAwO1xuXG4gIGlmIChzdGFydEJsb2NrID09PSBlbmRCbG9jaykge1xuICAgIGNoYXJhY3Rlckxpc3QgPSByZW1vdmVGcm9tTGlzdChzdGFydEJsb2NrLmdldENoYXJhY3Rlckxpc3QoKSwgc3RhcnRPZmZzZXQsIGVuZE9mZnNldCk7XG4gIH0gZWxzZSB7XG4gICAgY2hhcmFjdGVyTGlzdCA9IHN0YXJ0QmxvY2suZ2V0Q2hhcmFjdGVyTGlzdCgpLnNsaWNlKDAsIHN0YXJ0T2Zmc2V0KS5jb25jYXQoZW5kQmxvY2suZ2V0Q2hhcmFjdGVyTGlzdCgpLnNsaWNlKGVuZE9mZnNldCkpO1xuICB9XG5cbiAgdmFyIG1vZGlmaWVkU3RhcnQgPSBzdGFydEJsb2NrLm1lcmdlKHtcbiAgICB0ZXh0OiBzdGFydEJsb2NrLmdldFRleHQoKS5zbGljZSgwLCBzdGFydE9mZnNldCkgKyBlbmRCbG9jay5nZXRUZXh0KCkuc2xpY2UoZW5kT2Zmc2V0KSxcbiAgICBjaGFyYWN0ZXJMaXN0OiBjaGFyYWN0ZXJMaXN0XG4gIH0pO1xuXG4gIHZhciBuZXdCbG9ja3MgPSBibG9ja01hcC50b1NlcSgpLnNraXBVbnRpbChmdW5jdGlvbiAoXywgaykge1xuICAgIHJldHVybiBrID09PSBzdGFydEtleTtcbiAgfSkudGFrZVVudGlsKGZ1bmN0aW9uIChfLCBrKSB7XG4gICAgcmV0dXJuIGsgPT09IGVuZEtleTtcbiAgfSkuZmlsdGVyKGZ1bmN0aW9uIChfLCBrKSB7XG4gICAgcmV0dXJuIHBhcmVudEFuY2VzdG9ycy5pbmRleE9mKGspID09PSAtMTtcbiAgfSkuY29uY2F0KE1hcChbW2VuZEtleSwgbnVsbF1dKSkubWFwKGZ1bmN0aW9uIChfLCBrKSB7XG4gICAgcmV0dXJuIGsgPT09IHN0YXJ0S2V5ID8gbW9kaWZpZWRTdGFydCA6IG51bGw7XG4gIH0pO1xuXG4gIHZhciB1cGRhdGVkQmxvY2tNYXAgPSBibG9ja01hcC5tZXJnZShuZXdCbG9ja3MpLmZpbHRlcihmdW5jdGlvbiAoYmxvY2spIHtcbiAgICByZXR1cm4gISFibG9jaztcbiAgfSk7XG5cbiAgaWYgKGlzRXhwZXJpbWVudGFsVHJlZUJsb2NrKSB7XG4gICAgdXBkYXRlZEJsb2NrTWFwID0gdXBkYXRlQmxvY2tNYXBMaW5rcyh1cGRhdGVkQmxvY2tNYXAsIHN0YXJ0QmxvY2ssIGVuZEJsb2NrLCBibG9ja01hcCk7XG4gIH1cblxuICByZXR1cm4gY29udGVudFN0YXRlLm1lcmdlKHtcbiAgICBibG9ja01hcDogdXBkYXRlZEJsb2NrTWFwLFxuICAgIHNlbGVjdGlvbkJlZm9yZTogc2VsZWN0aW9uU3RhdGUsXG4gICAgc2VsZWN0aW9uQWZ0ZXI6IHNlbGVjdGlvblN0YXRlLm1lcmdlKHtcbiAgICAgIGFuY2hvcktleTogc3RhcnRLZXksXG4gICAgICBhbmNob3JPZmZzZXQ6IHN0YXJ0T2Zmc2V0LFxuICAgICAgZm9jdXNLZXk6IHN0YXJ0S2V5LFxuICAgICAgZm9jdXNPZmZzZXQ6IHN0YXJ0T2Zmc2V0LFxuICAgICAgaXNCYWNrd2FyZDogZmFsc2VcbiAgICB9KVxuICB9KTtcbn07XG5cbi8qKlxuICogTWFpbnRhaW4gcGVyc2lzdGVuY2UgZm9yIHRhcmdldCBsaXN0IHdoZW4gcmVtb3ZpbmcgY2hhcmFjdGVycyBvbiB0aGVcbiAqIGhlYWQgYW5kIHRhaWwgb2YgdGhlIGNoYXJhY3RlciBsaXN0LlxuICovXG52YXIgcmVtb3ZlRnJvbUxpc3QgPSBmdW5jdGlvbiByZW1vdmVGcm9tTGlzdCh0YXJnZXRMaXN0LCBzdGFydE9mZnNldCwgZW5kT2Zmc2V0KSB7XG4gIGlmIChzdGFydE9mZnNldCA9PT0gMCkge1xuICAgIHdoaWxlIChzdGFydE9mZnNldCA8IGVuZE9mZnNldCkge1xuICAgICAgdGFyZ2V0TGlzdCA9IHRhcmdldExpc3Quc2hpZnQoKTtcbiAgICAgIHN0YXJ0T2Zmc2V0Kys7XG4gICAgfVxuICB9IGVsc2UgaWYgKGVuZE9mZnNldCA9PT0gdGFyZ2V0TGlzdC5jb3VudCgpKSB7XG4gICAgd2hpbGUgKGVuZE9mZnNldCA+IHN0YXJ0T2Zmc2V0KSB7XG4gICAgICB0YXJnZXRMaXN0ID0gdGFyZ2V0TGlzdC5wb3AoKTtcbiAgICAgIGVuZE9mZnNldC0tO1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICB2YXIgaGVhZCA9IHRhcmdldExpc3Quc2xpY2UoMCwgc3RhcnRPZmZzZXQpO1xuICAgIHZhciB0YWlsID0gdGFyZ2V0TGlzdC5zbGljZShlbmRPZmZzZXQpO1xuICAgIHRhcmdldExpc3QgPSBoZWFkLmNvbmNhdCh0YWlsKS50b0xpc3QoKTtcbiAgfVxuICByZXR1cm4gdGFyZ2V0TGlzdDtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gcmVtb3ZlUmFuZ2VGcm9tQ29udGVudFN0YXRlOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBSEE7QUFXQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/removeRangeFromContentState.js
