__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _address__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./address */ "./src/app/desk/component/address.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/thai-address.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n              \n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var ThaiAddress =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(ThaiAddress, _ModelWidget);

  function ThaiAddress(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, ThaiAddress);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ThaiAddress).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(ThaiAddress, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        ThaiAddress: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ThaiAddress.prototype), "initState", this).call(this), {});
    }
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      var C = this.getComponents();
      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          prefix = _this$props.prefix,
          selectValue = _this$props.selectValue,
          extAddressType = _this$props.extAddressType,
          notAddressType = _this$props.notAddressType,
          _this$props$required = _this$props.required,
          required = _this$props$required === void 0 ? true : _this$props$required;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.ThaiAddress, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        },
        __self: this
      }, !notAddressType && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NRadio"], {
        form: form,
        model: model,
        tableName: "addresstype",
        required: required,
        propName: "".concat(extAddressType ? prefix + ".".concat(this.addressFix, ".ext") : prefix, ".addressType"),
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Address Type").thai("สถานที่ทำงาน (กรณีไม่มี ให้เลือกที่อยู่ปัจจุบัน)").my("လိပ်စာအမျိုးအစား").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_9__["NFormItem"], {
        form: form,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Address").thai("ที่อยู่").my("လိပ်စာ").getMessage(),
        model: model,
        required: required,
        propName: "".concat(prefix, "-").concat(this.addressFix),
        onChange: function onChange(address) {
          var _this$setValuesToMode;

          var postalCode = address.postalCode;

          _this.setValuesToModel((_this$setValuesToMode = {}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$setValuesToMode, "".concat(prefix, "-").concat(_this.addressFix), address), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$setValuesToMode, "".concat(prefix, "-").concat(_this.addressFix, "-street"), form.getFieldValue("".concat(prefix, "-address-street"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$setValuesToMode, "".concat(prefix, "-").concat(_this.addressFix, "-postalCode"), postalCode), _this$setValuesToMode));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_address__WEBPACK_IMPORTED_MODULE_13__["default"], {
        selectValue: selectValue || this.getValueFromModel("".concat(prefix, "-").concat(this.addressFix)),
        ltr: false,
        countryCode: this.props.countryCode,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NText"], {
        form: form,
        model: model,
        propName: "".concat(prefix, "-").concat(this.addressFix, "-street"),
        required: required,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Street").thai("ที่อยู่ถนน").my("လမ်းပေါ်မှ").getMessage(),
        placeholder: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("organization, building, village, street no. and street name etc.").thai("สังกัดหน่วยงาน, เลขที่, อาคาร, หมู่บ้าน, ถนน, อื่น ๆ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NText"], {
        form: form,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Postal Code").thai("รหัสไปรษณีย์").my("စာတိုက်သင်္ကေတ").getMessage(),
        required: required,
        model: model,
        propName: "".concat(prefix, "-").concat(this.addressFix, "-postalCode"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      }));
    }
  }, {
    key: "addressFix",
    get: function get() {
      return this.props.addressFix || "address";
    }
  }]);

  return ThaiAddress;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (ThaiAddress);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3RoYWktYWRkcmVzcy50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvdGhhaS1hZGRyZXNzLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCwgTkZvcm1JdGVtIH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0UHJvcHMsIFN0eWxlZERJViB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCB7IExhbmd1YWdlIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5SYWRpbywgTlRleHQgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCBBZGRyZXNzIGZyb20gXCIuL2FkZHJlc3NcIjtcblxuZXhwb3J0IHR5cGUgVGhhaUFkZHJlc3NQcm9wcyA9IHtcbiAgbW9kZWw6IGFueTtcbiAgZm9ybTogYW55O1xuICBwcmVmaXg/OiBhbnk7XG4gIGFkZHJlc3NGaXg/OiBzdHJpbmc7XG4gIGV4dEFkZHJlc3NUeXBlPzogYW55O1xuICBzZWxlY3RWYWx1ZT86IGFueTtcbiAgbm90QWRkcmVzc1R5cGU/OiBib29sZWFuO1xuICByZXF1aXJlZD86IGJvb2xlYW47XG4gIGNvdW50cnlDb2RlPzogc3RyaW5nO1xufSAmIE1vZGVsV2lkZ2V0UHJvcHM7XG5cbmV4cG9ydCB0eXBlIFRoYWlBZGRyZXNzU3RhdGUgPSB7XG4gIHNyYzogYW55O1xufTtcbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBUaGFpQWRkcmVzc0NvbXBvbmVudHMgPSB7XG4gIFRoYWlBZGRyZXNzOiBTdHlsZWRESVY7XG59O1xuXG5jbGFzcyBUaGFpQWRkcmVzczxQIGV4dGVuZHMgVGhhaUFkZHJlc3NQcm9wcyxcbiAgUyBleHRlbmRzIFRoYWlBZGRyZXNzU3RhdGUsXG4gIEMgZXh0ZW5kcyBUaGFpQWRkcmVzc0NvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogVGhhaUFkZHJlc3NQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgVGhhaUFkZHJlc3M6IFN0eWxlZC5kaXZgXG4gICAgICAgICAgICAgIFxuICAgICAgICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7fSkgYXMgUztcbiAgfVxuXG4gIGdldCBhZGRyZXNzRml4KCkge1xuICAgIHJldHVybiB0aGlzLnByb3BzLmFkZHJlc3NGaXggfHwgXCJhZGRyZXNzXCI7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIGNvbnN0IHsgbW9kZWwsIGZvcm0sIHByZWZpeCwgc2VsZWN0VmFsdWUsIGV4dEFkZHJlc3NUeXBlLCBub3RBZGRyZXNzVHlwZSwgcmVxdWlyZWQgPSB0cnVlIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5UaGFpQWRkcmVzcz5cbiAgICAgICAgeyFub3RBZGRyZXNzVHlwZSAmJiAoXG4gICAgICAgICAgPE5SYWRpb1xuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgIHRhYmxlTmFtZT1cImFkZHJlc3N0eXBlXCJcbiAgICAgICAgICAgIHJlcXVpcmVkPXtyZXF1aXJlZH1cbiAgICAgICAgICAgIHByb3BOYW1lPXtgJHtleHRBZGRyZXNzVHlwZSA/IHByZWZpeCArIGAuJHt0aGlzLmFkZHJlc3NGaXh9LmV4dGAgOiBwcmVmaXh9LmFkZHJlc3NUeXBlYH1cbiAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIkFkZHJlc3MgVHlwZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4quC4luC4suC4meC4l+C4teC5iOC4l+C4s+C4h+C4suC4mSAo4LiB4Lij4LiT4Li14LmE4Lih4LmI4Lih4Li1IOC5g+C4q+C5ieC5gOC4peC4t+C4reC4geC4l+C4teC5iOC4reC4ouC4ueC5iOC4m+C4seC4iOC4iOC4uOC4muC4seC4mSlcIilcbiAgICAgICAgICAgICAgLm15KFwi4YCc4YCt4YCV4YC64YCF4YCs4YCh4YCZ4YC74YCt4YCv4YC44YCh4YCF4YCs4YC4XCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgLz5cbiAgICAgICAgKX1cblxuICAgICAgICA8TkZvcm1JdGVtXG4gICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJBZGRyZXNzXCIpXG4gICAgICAgICAgICAudGhhaShcIuC4l+C4teC5iOC4reC4ouC4ueC5iFwiKVxuICAgICAgICAgICAgLm15KFwi4YCc4YCt4YCV4YC64YCF4YCsXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICByZXF1aXJlZD17cmVxdWlyZWR9XG4gICAgICAgICAgcHJvcE5hbWU9e2Ake3ByZWZpeH0tJHt0aGlzLmFkZHJlc3NGaXh9YH1cbiAgICAgICAgICBvbkNoYW5nZT17KGFkZHJlc3M6IGFueSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwb3N0YWxDb2RlIH0gPSBhZGRyZXNzO1xuICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZXNUb01vZGVsKHtcbiAgICAgICAgICAgICAgW2Ake3ByZWZpeH0tJHt0aGlzLmFkZHJlc3NGaXh9YF06IGFkZHJlc3MsXG4gICAgICAgICAgICAgIFtgJHtwcmVmaXh9LSR7dGhpcy5hZGRyZXNzRml4fS1zdHJlZXRgXTogZm9ybS5nZXRGaWVsZFZhbHVlKGAke3ByZWZpeH0tYWRkcmVzcy1zdHJlZXRgKSxcbiAgICAgICAgICAgICAgW2Ake3ByZWZpeH0tJHt0aGlzLmFkZHJlc3NGaXh9LXBvc3RhbENvZGVgXTogcG9zdGFsQ29kZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICA8QWRkcmVzc1xuICAgICAgICAgICAgc2VsZWN0VmFsdWU9e3NlbGVjdFZhbHVlIHx8IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoYCR7cHJlZml4fS0ke3RoaXMuYWRkcmVzc0ZpeH1gKX1cbiAgICAgICAgICAgIGx0cj17ZmFsc2V9XG4gICAgICAgICAgICBjb3VudHJ5Q29kZT17dGhpcy5wcm9wcy5jb3VudHJ5Q29kZSF9XG4gICAgICAgICAgLz5cbiAgICAgICAgPC9ORm9ybUl0ZW0+XG5cbiAgICAgICAgPE5UZXh0XG4gICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgcHJvcE5hbWU9e2Ake3ByZWZpeH0tJHt0aGlzLmFkZHJlc3NGaXh9LXN0cmVldGB9XG4gICAgICAgICAgcmVxdWlyZWQ9e3JlcXVpcmVkfVxuICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlN0cmVldFwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguJfguLXguYjguK3guKLguLnguYjguJbguJnguJlcIilcbiAgICAgICAgICAgIC5teShcIuGAnOGAmeGAuuGAuOGAleGAseGAq+GAuuGAmeGAvlwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICBwbGFjZWhvbGRlcj17TGFuZ3VhZ2UuZW4oXCJvcmdhbml6YXRpb24sIGJ1aWxkaW5nLCB2aWxsYWdlLCBzdHJlZXQgbm8uIGFuZCBzdHJlZXQgbmFtZSBldGMuXCIpXG4gICAgICAgICAgICAudGhhaShcIuC4quC4seC4h+C4geC4seC4lOC4q+C4meC5iOC4p+C4ouC4h+C4suC4mSwg4LmA4Lil4LiC4LiX4Li14LmILCDguK3guLLguITguLLguKMsIOC4q+C4oeC4ueC5iOC4muC5ieC4suC4mSwg4LiW4LiZ4LiZLCDguK3guLfguYjguJkg4LmGXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAvPlxuXG4gICAgICAgIDxOVGV4dFxuICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiUG9zdGFsIENvZGVcIilcbiAgICAgICAgICAgIC50aGFpKFwi4Lij4Lir4Lix4Liq4LmE4Lib4Lij4Lip4LiT4Li14Lii4LmMXCIpXG4gICAgICAgICAgICAubXkoXCLhgIXhgKzhgJDhgK3hgK/hgIDhgLrhgJ7hgIThgLrhgLnhgIDhgLHhgJBcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgcmVxdWlyZWQ9e3JlcXVpcmVkfVxuICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICBwcm9wTmFtZT17YCR7cHJlZml4fS0ke3RoaXMuYWRkcmVzc0ZpeH0tcG9zdGFsQ29kZWB9XG4gICAgICAgIC8+XG4gICAgICA8L0MuVGhhaUFkZHJlc3M+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBUaGFpQWRkcmVzcztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQXNCQTs7Ozs7QUFHQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQURBO0FBS0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBTUE7QUFBQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFLQTtBQWhCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFtQkE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBOzs7QUExRUE7QUFDQTtBQUNBOzs7O0FBbkJBO0FBQ0E7QUE2RkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/thai-address.tsx
