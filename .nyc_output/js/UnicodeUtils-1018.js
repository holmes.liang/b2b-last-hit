/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 */

/**
 * Unicode-enabled replacesments for basic String functions.
 *
 * All the functions in this module assume that the input string is a valid
 * UTF-16 encoding of a Unicode sequence. If it's not the case, the behavior
 * will be undefined.
 *
 * WARNING: Since this module is typechecks-enforced, you may find new bugs
 * when replacing normal String functions with ones provided here.
 */


var invariant = __webpack_require__(/*! ./invariant */ "./node_modules/fbjs/lib/invariant.js"); // These two ranges are consecutive so anything in [HIGH_START, LOW_END] is a
// surrogate code unit.


var SURROGATE_HIGH_START = 0xD800;
var SURROGATE_HIGH_END = 0xDBFF;
var SURROGATE_LOW_START = 0xDC00;
var SURROGATE_LOW_END = 0xDFFF;
var SURROGATE_UNITS_REGEX = /[\uD800-\uDFFF]/;
/**
 * @param {number} codeUnit   A Unicode code-unit, in range [0, 0x10FFFF]
 * @return {boolean}          Whether code-unit is in a surrogate (hi/low) range
 */

function isCodeUnitInSurrogateRange(codeUnit) {
  return SURROGATE_HIGH_START <= codeUnit && codeUnit <= SURROGATE_LOW_END;
}
/**
 * Returns whether the two characters starting at `index` form a surrogate pair.
 * For example, given the string s = "\uD83D\uDE0A", (s, 0) returns true and
 * (s, 1) returns false.
 *
 * @param {string} str
 * @param {number} index
 * @return {boolean}
 */


function isSurrogatePair(str, index) {
  !(0 <= index && index < str.length) ?  true ? invariant(false, 'isSurrogatePair: Invalid index %s for string length %s.', index, str.length) : undefined : void 0;

  if (index + 1 === str.length) {
    return false;
  }

  var first = str.charCodeAt(index);
  var second = str.charCodeAt(index + 1);
  return SURROGATE_HIGH_START <= first && first <= SURROGATE_HIGH_END && SURROGATE_LOW_START <= second && second <= SURROGATE_LOW_END;
}
/**
 * @param {string} str  Non-empty string
 * @return {boolean}    True if the input includes any surrogate code units
 */


function hasSurrogateUnit(str) {
  return SURROGATE_UNITS_REGEX.test(str);
}
/**
 * Return the length of the original Unicode character at given position in the
 * String by looking into the UTF-16 code unit; that is equal to 1 for any
 * non-surrogate characters in BMP ([U+0000..U+D7FF] and [U+E000, U+FFFF]); and
 * returns 2 for the hi/low surrogates ([U+D800..U+DFFF]), which are in fact
 * representing non-BMP characters ([U+10000..U+10FFFF]).
 *
 * Examples:
 * - '\u0020' => 1
 * - '\u3020' => 1
 * - '\uD835' => 2
 * - '\uD835\uDDEF' => 2
 * - '\uDDEF' => 2
 *
 * @param {string} str  Non-empty string
 * @param {number} pos  Position in the string to look for one code unit
 * @return {number}      Number 1 or 2
 */


function getUTF16Length(str, pos) {
  return 1 + isCodeUnitInSurrogateRange(str.charCodeAt(pos));
}
/**
 * Fully Unicode-enabled replacement for String#length
 *
 * @param {string} str  Valid Unicode string
 * @return {number}     The number of Unicode characters in the string
 */


function strlen(str) {
  // Call the native functions if there's no surrogate char
  if (!hasSurrogateUnit(str)) {
    return str.length;
  }

  var len = 0;

  for (var pos = 0; pos < str.length; pos += getUTF16Length(str, pos)) {
    len++;
  }

  return len;
}
/**
 * Fully Unicode-enabled replacement for String#substr()
 *
 * @param {string} str      Valid Unicode string
 * @param {number} start    Location in Unicode sequence to begin extracting
 * @param {?number} length  The number of Unicode characters to extract
 *                          (default: to the end of the string)
 * @return {string}         Extracted sub-string
 */


function substr(str, start, length) {
  start = start || 0;
  length = length === undefined ? Infinity : length || 0; // Call the native functions if there's no surrogate char

  if (!hasSurrogateUnit(str)) {
    return str.substr(start, length);
  } // Obvious cases


  var size = str.length;

  if (size <= 0 || start > size || length <= 0) {
    return '';
  } // Find the actual starting position


  var posA = 0;

  if (start > 0) {
    for (; start > 0 && posA < size; start--) {
      posA += getUTF16Length(str, posA);
    }

    if (posA >= size) {
      return '';
    }
  } else if (start < 0) {
    for (posA = size; start < 0 && 0 < posA; start++) {
      posA -= getUTF16Length(str, posA - 1);
    }

    if (posA < 0) {
      posA = 0;
    }
  } // Find the actual ending position


  var posB = size;

  if (length < size) {
    for (posB = posA; length > 0 && posB < size; length--) {
      posB += getUTF16Length(str, posB);
    }
  }

  return str.substring(posA, posB);
}
/**
 * Fully Unicode-enabled replacement for String#substring()
 *
 * @param {string} str    Valid Unicode string
 * @param {number} start  Location in Unicode sequence to begin extracting
 * @param {?number} end   Location in Unicode sequence to end extracting
 *                        (default: end of the string)
 * @return {string}       Extracted sub-string
 */


function substring(str, start, end) {
  start = start || 0;
  end = end === undefined ? Infinity : end || 0;

  if (start < 0) {
    start = 0;
  }

  if (end < 0) {
    end = 0;
  }

  var length = Math.abs(end - start);
  start = start < end ? start : end;
  return substr(str, start, length);
}
/**
 * Get a list of Unicode code-points from a String
 *
 * @param {string} str        Valid Unicode string
 * @return {array<number>}    A list of code-points in [0..0x10FFFF]
 */


function getCodePoints(str) {
  var codePoints = [];

  for (var pos = 0; pos < str.length; pos += getUTF16Length(str, pos)) {
    codePoints.push(str.codePointAt(pos));
  }

  return codePoints;
}

var UnicodeUtils = {
  getCodePoints: getCodePoints,
  getUTF16Length: getUTF16Length,
  hasSurrogateUnit: hasSurrogateUnit,
  isCodeUnitInSurrogateRange: isCodeUnitInSurrogateRange,
  isSurrogatePair: isSurrogatePair,
  strlen: strlen,
  substring: substring,
  substr: substr
};
module.exports = UnicodeUtils;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVW5pY29kZVV0aWxzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVW5pY29kZVV0aWxzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICogQHR5cGVjaGVja3NcbiAqL1xuXG4vKipcbiAqIFVuaWNvZGUtZW5hYmxlZCByZXBsYWNlc21lbnRzIGZvciBiYXNpYyBTdHJpbmcgZnVuY3Rpb25zLlxuICpcbiAqIEFsbCB0aGUgZnVuY3Rpb25zIGluIHRoaXMgbW9kdWxlIGFzc3VtZSB0aGF0IHRoZSBpbnB1dCBzdHJpbmcgaXMgYSB2YWxpZFxuICogVVRGLTE2IGVuY29kaW5nIG9mIGEgVW5pY29kZSBzZXF1ZW5jZS4gSWYgaXQncyBub3QgdGhlIGNhc2UsIHRoZSBiZWhhdmlvclxuICogd2lsbCBiZSB1bmRlZmluZWQuXG4gKlxuICogV0FSTklORzogU2luY2UgdGhpcyBtb2R1bGUgaXMgdHlwZWNoZWNrcy1lbmZvcmNlZCwgeW91IG1heSBmaW5kIG5ldyBidWdzXG4gKiB3aGVuIHJlcGxhY2luZyBub3JtYWwgU3RyaW5nIGZ1bmN0aW9ucyB3aXRoIG9uZXMgcHJvdmlkZWQgaGVyZS5cbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBpbnZhcmlhbnQgPSByZXF1aXJlKCcuL2ludmFyaWFudCcpO1xuXG4vLyBUaGVzZSB0d28gcmFuZ2VzIGFyZSBjb25zZWN1dGl2ZSBzbyBhbnl0aGluZyBpbiBbSElHSF9TVEFSVCwgTE9XX0VORF0gaXMgYVxuLy8gc3Vycm9nYXRlIGNvZGUgdW5pdC5cbnZhciBTVVJST0dBVEVfSElHSF9TVEFSVCA9IDB4RDgwMDtcbnZhciBTVVJST0dBVEVfSElHSF9FTkQgPSAweERCRkY7XG52YXIgU1VSUk9HQVRFX0xPV19TVEFSVCA9IDB4REMwMDtcbnZhciBTVVJST0dBVEVfTE9XX0VORCA9IDB4REZGRjtcbnZhciBTVVJST0dBVEVfVU5JVFNfUkVHRVggPSAvW1xcdUQ4MDAtXFx1REZGRl0vO1xuXG4vKipcbiAqIEBwYXJhbSB7bnVtYmVyfSBjb2RlVW5pdCAgIEEgVW5pY29kZSBjb2RlLXVuaXQsIGluIHJhbmdlIFswLCAweDEwRkZGRl1cbiAqIEByZXR1cm4ge2Jvb2xlYW59ICAgICAgICAgIFdoZXRoZXIgY29kZS11bml0IGlzIGluIGEgc3Vycm9nYXRlIChoaS9sb3cpIHJhbmdlXG4gKi9cbmZ1bmN0aW9uIGlzQ29kZVVuaXRJblN1cnJvZ2F0ZVJhbmdlKGNvZGVVbml0KSB7XG4gIHJldHVybiBTVVJST0dBVEVfSElHSF9TVEFSVCA8PSBjb2RlVW5pdCAmJiBjb2RlVW5pdCA8PSBTVVJST0dBVEVfTE9XX0VORDtcbn1cblxuLyoqXG4gKiBSZXR1cm5zIHdoZXRoZXIgdGhlIHR3byBjaGFyYWN0ZXJzIHN0YXJ0aW5nIGF0IGBpbmRleGAgZm9ybSBhIHN1cnJvZ2F0ZSBwYWlyLlxuICogRm9yIGV4YW1wbGUsIGdpdmVuIHRoZSBzdHJpbmcgcyA9IFwiXFx1RDgzRFxcdURFMEFcIiwgKHMsIDApIHJldHVybnMgdHJ1ZSBhbmRcbiAqIChzLCAxKSByZXR1cm5zIGZhbHNlLlxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSBzdHJcbiAqIEBwYXJhbSB7bnVtYmVyfSBpbmRleFxuICogQHJldHVybiB7Ym9vbGVhbn1cbiAqL1xuZnVuY3Rpb24gaXNTdXJyb2dhdGVQYWlyKHN0ciwgaW5kZXgpIHtcbiAgISgwIDw9IGluZGV4ICYmIGluZGV4IDwgc3RyLmxlbmd0aCkgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAnaXNTdXJyb2dhdGVQYWlyOiBJbnZhbGlkIGluZGV4ICVzIGZvciBzdHJpbmcgbGVuZ3RoICVzLicsIGluZGV4LCBzdHIubGVuZ3RoKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG4gIGlmIChpbmRleCArIDEgPT09IHN0ci5sZW5ndGgpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbiAgdmFyIGZpcnN0ID0gc3RyLmNoYXJDb2RlQXQoaW5kZXgpO1xuICB2YXIgc2Vjb25kID0gc3RyLmNoYXJDb2RlQXQoaW5kZXggKyAxKTtcbiAgcmV0dXJuIFNVUlJPR0FURV9ISUdIX1NUQVJUIDw9IGZpcnN0ICYmIGZpcnN0IDw9IFNVUlJPR0FURV9ISUdIX0VORCAmJiBTVVJST0dBVEVfTE9XX1NUQVJUIDw9IHNlY29uZCAmJiBzZWNvbmQgPD0gU1VSUk9HQVRFX0xPV19FTkQ7XG59XG5cbi8qKlxuICogQHBhcmFtIHtzdHJpbmd9IHN0ciAgTm9uLWVtcHR5IHN0cmluZ1xuICogQHJldHVybiB7Ym9vbGVhbn0gICAgVHJ1ZSBpZiB0aGUgaW5wdXQgaW5jbHVkZXMgYW55IHN1cnJvZ2F0ZSBjb2RlIHVuaXRzXG4gKi9cbmZ1bmN0aW9uIGhhc1N1cnJvZ2F0ZVVuaXQoc3RyKSB7XG4gIHJldHVybiBTVVJST0dBVEVfVU5JVFNfUkVHRVgudGVzdChzdHIpO1xufVxuXG4vKipcbiAqIFJldHVybiB0aGUgbGVuZ3RoIG9mIHRoZSBvcmlnaW5hbCBVbmljb2RlIGNoYXJhY3RlciBhdCBnaXZlbiBwb3NpdGlvbiBpbiB0aGVcbiAqIFN0cmluZyBieSBsb29raW5nIGludG8gdGhlIFVURi0xNiBjb2RlIHVuaXQ7IHRoYXQgaXMgZXF1YWwgdG8gMSBmb3IgYW55XG4gKiBub24tc3Vycm9nYXRlIGNoYXJhY3RlcnMgaW4gQk1QIChbVSswMDAwLi5VK0Q3RkZdIGFuZCBbVStFMDAwLCBVK0ZGRkZdKTsgYW5kXG4gKiByZXR1cm5zIDIgZm9yIHRoZSBoaS9sb3cgc3Vycm9nYXRlcyAoW1UrRDgwMC4uVStERkZGXSksIHdoaWNoIGFyZSBpbiBmYWN0XG4gKiByZXByZXNlbnRpbmcgbm9uLUJNUCBjaGFyYWN0ZXJzIChbVSsxMDAwMC4uVSsxMEZGRkZdKS5cbiAqXG4gKiBFeGFtcGxlczpcbiAqIC0gJ1xcdTAwMjAnID0+IDFcbiAqIC0gJ1xcdTMwMjAnID0+IDFcbiAqIC0gJ1xcdUQ4MzUnID0+IDJcbiAqIC0gJ1xcdUQ4MzVcXHVEREVGJyA9PiAyXG4gKiAtICdcXHVEREVGJyA9PiAyXG4gKlxuICogQHBhcmFtIHtzdHJpbmd9IHN0ciAgTm9uLWVtcHR5IHN0cmluZ1xuICogQHBhcmFtIHtudW1iZXJ9IHBvcyAgUG9zaXRpb24gaW4gdGhlIHN0cmluZyB0byBsb29rIGZvciBvbmUgY29kZSB1bml0XG4gKiBAcmV0dXJuIHtudW1iZXJ9ICAgICAgTnVtYmVyIDEgb3IgMlxuICovXG5mdW5jdGlvbiBnZXRVVEYxNkxlbmd0aChzdHIsIHBvcykge1xuICByZXR1cm4gMSArIGlzQ29kZVVuaXRJblN1cnJvZ2F0ZVJhbmdlKHN0ci5jaGFyQ29kZUF0KHBvcykpO1xufVxuXG4vKipcbiAqIEZ1bGx5IFVuaWNvZGUtZW5hYmxlZCByZXBsYWNlbWVudCBmb3IgU3RyaW5nI2xlbmd0aFxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSBzdHIgIFZhbGlkIFVuaWNvZGUgc3RyaW5nXG4gKiBAcmV0dXJuIHtudW1iZXJ9ICAgICBUaGUgbnVtYmVyIG9mIFVuaWNvZGUgY2hhcmFjdGVycyBpbiB0aGUgc3RyaW5nXG4gKi9cbmZ1bmN0aW9uIHN0cmxlbihzdHIpIHtcbiAgLy8gQ2FsbCB0aGUgbmF0aXZlIGZ1bmN0aW9ucyBpZiB0aGVyZSdzIG5vIHN1cnJvZ2F0ZSBjaGFyXG4gIGlmICghaGFzU3Vycm9nYXRlVW5pdChzdHIpKSB7XG4gICAgcmV0dXJuIHN0ci5sZW5ndGg7XG4gIH1cblxuICB2YXIgbGVuID0gMDtcbiAgZm9yICh2YXIgcG9zID0gMDsgcG9zIDwgc3RyLmxlbmd0aDsgcG9zICs9IGdldFVURjE2TGVuZ3RoKHN0ciwgcG9zKSkge1xuICAgIGxlbisrO1xuICB9XG4gIHJldHVybiBsZW47XG59XG5cbi8qKlxuICogRnVsbHkgVW5pY29kZS1lbmFibGVkIHJlcGxhY2VtZW50IGZvciBTdHJpbmcjc3Vic3RyKClcbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gc3RyICAgICAgVmFsaWQgVW5pY29kZSBzdHJpbmdcbiAqIEBwYXJhbSB7bnVtYmVyfSBzdGFydCAgICBMb2NhdGlvbiBpbiBVbmljb2RlIHNlcXVlbmNlIHRvIGJlZ2luIGV4dHJhY3RpbmdcbiAqIEBwYXJhbSB7P251bWJlcn0gbGVuZ3RoICBUaGUgbnVtYmVyIG9mIFVuaWNvZGUgY2hhcmFjdGVycyB0byBleHRyYWN0XG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgKGRlZmF1bHQ6IHRvIHRoZSBlbmQgb2YgdGhlIHN0cmluZylcbiAqIEByZXR1cm4ge3N0cmluZ30gICAgICAgICBFeHRyYWN0ZWQgc3ViLXN0cmluZ1xuICovXG5mdW5jdGlvbiBzdWJzdHIoc3RyLCBzdGFydCwgbGVuZ3RoKSB7XG4gIHN0YXJ0ID0gc3RhcnQgfHwgMDtcbiAgbGVuZ3RoID0gbGVuZ3RoID09PSB1bmRlZmluZWQgPyBJbmZpbml0eSA6IGxlbmd0aCB8fCAwO1xuXG4gIC8vIENhbGwgdGhlIG5hdGl2ZSBmdW5jdGlvbnMgaWYgdGhlcmUncyBubyBzdXJyb2dhdGUgY2hhclxuICBpZiAoIWhhc1N1cnJvZ2F0ZVVuaXQoc3RyKSkge1xuICAgIHJldHVybiBzdHIuc3Vic3RyKHN0YXJ0LCBsZW5ndGgpO1xuICB9XG5cbiAgLy8gT2J2aW91cyBjYXNlc1xuICB2YXIgc2l6ZSA9IHN0ci5sZW5ndGg7XG4gIGlmIChzaXplIDw9IDAgfHwgc3RhcnQgPiBzaXplIHx8IGxlbmd0aCA8PSAwKSB7XG4gICAgcmV0dXJuICcnO1xuICB9XG5cbiAgLy8gRmluZCB0aGUgYWN0dWFsIHN0YXJ0aW5nIHBvc2l0aW9uXG4gIHZhciBwb3NBID0gMDtcbiAgaWYgKHN0YXJ0ID4gMCkge1xuICAgIGZvciAoOyBzdGFydCA+IDAgJiYgcG9zQSA8IHNpemU7IHN0YXJ0LS0pIHtcbiAgICAgIHBvc0EgKz0gZ2V0VVRGMTZMZW5ndGgoc3RyLCBwb3NBKTtcbiAgICB9XG4gICAgaWYgKHBvc0EgPj0gc2l6ZSkge1xuICAgICAgcmV0dXJuICcnO1xuICAgIH1cbiAgfSBlbHNlIGlmIChzdGFydCA8IDApIHtcbiAgICBmb3IgKHBvc0EgPSBzaXplOyBzdGFydCA8IDAgJiYgMCA8IHBvc0E7IHN0YXJ0KyspIHtcbiAgICAgIHBvc0EgLT0gZ2V0VVRGMTZMZW5ndGgoc3RyLCBwb3NBIC0gMSk7XG4gICAgfVxuICAgIGlmIChwb3NBIDwgMCkge1xuICAgICAgcG9zQSA9IDA7XG4gICAgfVxuICB9XG5cbiAgLy8gRmluZCB0aGUgYWN0dWFsIGVuZGluZyBwb3NpdGlvblxuICB2YXIgcG9zQiA9IHNpemU7XG4gIGlmIChsZW5ndGggPCBzaXplKSB7XG4gICAgZm9yIChwb3NCID0gcG9zQTsgbGVuZ3RoID4gMCAmJiBwb3NCIDwgc2l6ZTsgbGVuZ3RoLS0pIHtcbiAgICAgIHBvc0IgKz0gZ2V0VVRGMTZMZW5ndGgoc3RyLCBwb3NCKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gc3RyLnN1YnN0cmluZyhwb3NBLCBwb3NCKTtcbn1cblxuLyoqXG4gKiBGdWxseSBVbmljb2RlLWVuYWJsZWQgcmVwbGFjZW1lbnQgZm9yIFN0cmluZyNzdWJzdHJpbmcoKVxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSBzdHIgICAgVmFsaWQgVW5pY29kZSBzdHJpbmdcbiAqIEBwYXJhbSB7bnVtYmVyfSBzdGFydCAgTG9jYXRpb24gaW4gVW5pY29kZSBzZXF1ZW5jZSB0byBiZWdpbiBleHRyYWN0aW5nXG4gKiBAcGFyYW0gez9udW1iZXJ9IGVuZCAgIExvY2F0aW9uIGluIFVuaWNvZGUgc2VxdWVuY2UgdG8gZW5kIGV4dHJhY3RpbmdcbiAqICAgICAgICAgICAgICAgICAgICAgICAgKGRlZmF1bHQ6IGVuZCBvZiB0aGUgc3RyaW5nKVxuICogQHJldHVybiB7c3RyaW5nfSAgICAgICBFeHRyYWN0ZWQgc3ViLXN0cmluZ1xuICovXG5mdW5jdGlvbiBzdWJzdHJpbmcoc3RyLCBzdGFydCwgZW5kKSB7XG4gIHN0YXJ0ID0gc3RhcnQgfHwgMDtcbiAgZW5kID0gZW5kID09PSB1bmRlZmluZWQgPyBJbmZpbml0eSA6IGVuZCB8fCAwO1xuXG4gIGlmIChzdGFydCA8IDApIHtcbiAgICBzdGFydCA9IDA7XG4gIH1cbiAgaWYgKGVuZCA8IDApIHtcbiAgICBlbmQgPSAwO1xuICB9XG5cbiAgdmFyIGxlbmd0aCA9IE1hdGguYWJzKGVuZCAtIHN0YXJ0KTtcbiAgc3RhcnQgPSBzdGFydCA8IGVuZCA/IHN0YXJ0IDogZW5kO1xuICByZXR1cm4gc3Vic3RyKHN0ciwgc3RhcnQsIGxlbmd0aCk7XG59XG5cbi8qKlxuICogR2V0IGEgbGlzdCBvZiBVbmljb2RlIGNvZGUtcG9pbnRzIGZyb20gYSBTdHJpbmdcbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gc3RyICAgICAgICBWYWxpZCBVbmljb2RlIHN0cmluZ1xuICogQHJldHVybiB7YXJyYXk8bnVtYmVyPn0gICAgQSBsaXN0IG9mIGNvZGUtcG9pbnRzIGluIFswLi4weDEwRkZGRl1cbiAqL1xuZnVuY3Rpb24gZ2V0Q29kZVBvaW50cyhzdHIpIHtcbiAgdmFyIGNvZGVQb2ludHMgPSBbXTtcbiAgZm9yICh2YXIgcG9zID0gMDsgcG9zIDwgc3RyLmxlbmd0aDsgcG9zICs9IGdldFVURjE2TGVuZ3RoKHN0ciwgcG9zKSkge1xuICAgIGNvZGVQb2ludHMucHVzaChzdHIuY29kZVBvaW50QXQocG9zKSk7XG4gIH1cbiAgcmV0dXJuIGNvZGVQb2ludHM7XG59XG5cbnZhciBVbmljb2RlVXRpbHMgPSB7XG4gIGdldENvZGVQb2ludHM6IGdldENvZGVQb2ludHMsXG4gIGdldFVURjE2TGVuZ3RoOiBnZXRVVEYxNkxlbmd0aCxcbiAgaGFzU3Vycm9nYXRlVW5pdDogaGFzU3Vycm9nYXRlVW5pdCxcbiAgaXNDb2RlVW5pdEluU3Vycm9nYXRlUmFuZ2U6IGlzQ29kZVVuaXRJblN1cnJvZ2F0ZVJhbmdlLFxuICBpc1N1cnJvZ2F0ZVBhaXI6IGlzU3Vycm9nYXRlUGFpcixcbiAgc3RybGVuOiBzdHJsZW4sXG4gIHN1YnN0cmluZzogc3Vic3RyaW5nLFxuICBzdWJzdHI6IHN1YnN0clxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBVbmljb2RlVXRpbHM7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7O0FBU0E7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBOzs7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFXQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/UnicodeUtils.js
