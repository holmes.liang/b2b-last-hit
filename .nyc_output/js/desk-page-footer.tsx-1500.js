__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/page/desk-page-footer.tsx";




var DeskPageFooter =
/*#__PURE__*/
function (_PageFooter) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(DeskPageFooter, _PageFooter);

  function DeskPageFooter() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, DeskPageFooter);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(DeskPageFooter).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(DeskPageFooter, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_4__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(DeskPageFooter.prototype), "initComponents", this).call(this), {});
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        ver: ""
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this = this;

      if (!_common__WEBPACK_IMPORTED_MODULE_8__["Storage"].TitleLogo.session().get("ver")) {
        _common__WEBPACK_IMPORTED_MODULE_8__["Ajax"].get("/version").then(function (res) {
          _this.setState({
            ver: res.body.respData
          });

          _common__WEBPACK_IMPORTED_MODULE_8__["Storage"].TitleLogo.session().set("ver", res.body.respData);
        });
        return;
      }

      this.setState({
        ver: _common__WEBPACK_IMPORTED_MODULE_8__["Storage"].TitleLogo.session().get("ver")
      });
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();

      if (_common__WEBPACK_IMPORTED_MODULE_8__["Utils"].getIsInApp()) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 40
          },
          __self: this
        });
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(C.Box, {
        style: {
          background: "#f0f2f5"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, "Powered by Bytesforce", this.state.ver && _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        style: {
          float: "right",
          fontSize: 12
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        },
        __self: this
      }, "v", this.state.ver)));
    }
  }]);

  return DeskPageFooter;
}(_component__WEBPACK_IMPORTED_MODULE_7__["PageFooter"]);

/* harmony default export */ __webpack_exports__["default"] = (DeskPageFooter);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BhZ2UvZGVzay1wYWdlLWZvb3Rlci50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcGFnZS9kZXNrLXBhZ2UtZm9vdGVyLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQYWdlRm9vdGVyQ29tcG9uZW50cyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIi4uLy4uLy4uLy4uL2NvbW1vbi8zcmRcIjtcbmltcG9ydCB7IFBhZ2VGb290ZXIgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheCwgU3RvcmFnZSwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuXG5pbnRlcmZhY2UgSVN0YXRlIHtcbiAgdmVyOiBzdHJpbmc7XG59XG5cbmNsYXNzIERlc2tQYWdlRm9vdGVyPFAsIFMgZXh0ZW5kcyBJU3RhdGUsIEMgZXh0ZW5kcyBQYWdlRm9vdGVyQ29tcG9uZW50cz4gZXh0ZW5kcyBQYWdlRm9vdGVyPFAsIFMsIEM+IHtcbiAgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdENvbXBvbmVudHMoKSwge30pO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4geyB2ZXI6IFwiXCIgfSBhcyBTO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgaWYgKCFTdG9yYWdlLlRpdGxlTG9nby5zZXNzaW9uKCkuZ2V0KFwidmVyXCIpKSB7XG4gICAgICBBamF4LmdldChcIi92ZXJzaW9uXCIpLnRoZW4ocmVzID0+IHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgdmVyOiByZXMuYm9keS5yZXNwRGF0YSxcbiAgICAgICAgfSk7XG4gICAgICAgIFN0b3JhZ2UuVGl0bGVMb2dvLnNlc3Npb24oKS5zZXQoXCJ2ZXJcIiwgcmVzLmJvZHkucmVzcERhdGEpO1xuICAgICAgfSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICB2ZXI6IFN0b3JhZ2UuVGl0bGVMb2dvLnNlc3Npb24oKS5nZXQoXCJ2ZXJcIiksXG4gICAgfSk7XG5cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG5cbiAgICBpZiAoVXRpbHMuZ2V0SXNJbkFwcCgpKSB7XG4gICAgICByZXR1cm4gPGRpdi8+O1xuICAgIH1cblxuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3ggc3R5bGU9e3sgYmFja2dyb3VuZDogXCIjZjBmMmY1XCIgfX0+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgUG93ZXJlZCBieSBCeXRlc2ZvcmNlXG5cbiAgICAgICAgICB7KHRoaXMuc3RhdGUudmVyKSAmJiA8ZGl2IHN0eWxlPXt7IGZsb2F0OiBcInJpZ2h0XCIsIGZvbnRTaXplOiAxMiB9fT52e3RoaXMuc3RhdGUudmVyfTwvZGl2Pn1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L0MuQm94PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgRGVza1BhZ2VGb290ZXI7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFLQTs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTs7OztBQTFDQTtBQUNBO0FBNENBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/page/desk-page-footer.tsx
