/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// import * as axisHelper from './axisHelper';


var _default = {
  /**
   * @param {boolean} origin
   * @return {number|string} min value or 'dataMin' or null/undefined (means auto) or NaN
   */
  getMin: function getMin(origin) {
    var option = this.option;
    var min = !origin && option.rangeStart != null ? option.rangeStart : option.min;

    if (this.axis && min != null && min !== 'dataMin' && typeof min !== 'function' && !zrUtil.eqNaN(min)) {
      min = this.axis.scale.parse(min);
    }

    return min;
  },

  /**
   * @param {boolean} origin
   * @return {number|string} max value or 'dataMax' or null/undefined (means auto) or NaN
   */
  getMax: function getMax(origin) {
    var option = this.option;
    var max = !origin && option.rangeEnd != null ? option.rangeEnd : option.max;

    if (this.axis && max != null && max !== 'dataMax' && typeof max !== 'function' && !zrUtil.eqNaN(max)) {
      max = this.axis.scale.parse(max);
    }

    return max;
  },

  /**
   * @return {boolean}
   */
  getNeedCrossZero: function getNeedCrossZero() {
    var option = this.option;
    return option.rangeStart != null || option.rangeEnd != null ? false : !option.scale;
  },

  /**
   * Should be implemented by each axis model if necessary.
   * @return {module:echarts/model/Component} coordinate system model
   */
  getCoordSysModel: zrUtil.noop,

  /**
   * @param {number} rangeStart Can only be finite number or null/undefined or NaN.
   * @param {number} rangeEnd Can only be finite number or null/undefined or NaN.
   */
  setRange: function setRange(rangeStart, rangeEnd) {
    this.option.rangeStart = rangeStart;
    this.option.rangeEnd = rangeEnd;
  },

  /**
   * Reset range
   */
  resetRange: function resetRange() {
    // rangeStart and rangeEnd is readonly.
    this.option.rangeStart = this.option.rangeEnd = null;
  }
};
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvYXhpc01vZGVsQ29tbW9uTWl4aW4uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb29yZC9heGlzTW9kZWxDb21tb25NaXhpbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbi8vIGltcG9ydCAqIGFzIGF4aXNIZWxwZXIgZnJvbSAnLi9heGlzSGVscGVyJztcbnZhciBfZGVmYXVsdCA9IHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gb3JpZ2luXG4gICAqIEByZXR1cm4ge251bWJlcnxzdHJpbmd9IG1pbiB2YWx1ZSBvciAnZGF0YU1pbicgb3IgbnVsbC91bmRlZmluZWQgKG1lYW5zIGF1dG8pIG9yIE5hTlxuICAgKi9cbiAgZ2V0TWluOiBmdW5jdGlvbiAob3JpZ2luKSB7XG4gICAgdmFyIG9wdGlvbiA9IHRoaXMub3B0aW9uO1xuICAgIHZhciBtaW4gPSAhb3JpZ2luICYmIG9wdGlvbi5yYW5nZVN0YXJ0ICE9IG51bGwgPyBvcHRpb24ucmFuZ2VTdGFydCA6IG9wdGlvbi5taW47XG5cbiAgICBpZiAodGhpcy5heGlzICYmIG1pbiAhPSBudWxsICYmIG1pbiAhPT0gJ2RhdGFNaW4nICYmIHR5cGVvZiBtaW4gIT09ICdmdW5jdGlvbicgJiYgIXpyVXRpbC5lcU5hTihtaW4pKSB7XG4gICAgICBtaW4gPSB0aGlzLmF4aXMuc2NhbGUucGFyc2UobWluKTtcbiAgICB9XG5cbiAgICByZXR1cm4gbWluO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge2Jvb2xlYW59IG9yaWdpblxuICAgKiBAcmV0dXJuIHtudW1iZXJ8c3RyaW5nfSBtYXggdmFsdWUgb3IgJ2RhdGFNYXgnIG9yIG51bGwvdW5kZWZpbmVkIChtZWFucyBhdXRvKSBvciBOYU5cbiAgICovXG4gIGdldE1heDogZnVuY3Rpb24gKG9yaWdpbikge1xuICAgIHZhciBvcHRpb24gPSB0aGlzLm9wdGlvbjtcbiAgICB2YXIgbWF4ID0gIW9yaWdpbiAmJiBvcHRpb24ucmFuZ2VFbmQgIT0gbnVsbCA/IG9wdGlvbi5yYW5nZUVuZCA6IG9wdGlvbi5tYXg7XG5cbiAgICBpZiAodGhpcy5heGlzICYmIG1heCAhPSBudWxsICYmIG1heCAhPT0gJ2RhdGFNYXgnICYmIHR5cGVvZiBtYXggIT09ICdmdW5jdGlvbicgJiYgIXpyVXRpbC5lcU5hTihtYXgpKSB7XG4gICAgICBtYXggPSB0aGlzLmF4aXMuc2NhbGUucGFyc2UobWF4KTtcbiAgICB9XG5cbiAgICByZXR1cm4gbWF4O1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgZ2V0TmVlZENyb3NzWmVybzogZnVuY3Rpb24gKCkge1xuICAgIHZhciBvcHRpb24gPSB0aGlzLm9wdGlvbjtcbiAgICByZXR1cm4gb3B0aW9uLnJhbmdlU3RhcnQgIT0gbnVsbCB8fCBvcHRpb24ucmFuZ2VFbmQgIT0gbnVsbCA/IGZhbHNlIDogIW9wdGlvbi5zY2FsZTtcbiAgfSxcblxuICAvKipcbiAgICogU2hvdWxkIGJlIGltcGxlbWVudGVkIGJ5IGVhY2ggYXhpcyBtb2RlbCBpZiBuZWNlc3NhcnkuXG4gICAqIEByZXR1cm4ge21vZHVsZTplY2hhcnRzL21vZGVsL0NvbXBvbmVudH0gY29vcmRpbmF0ZSBzeXN0ZW0gbW9kZWxcbiAgICovXG4gIGdldENvb3JkU3lzTW9kZWw6IHpyVXRpbC5ub29wLFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge251bWJlcn0gcmFuZ2VTdGFydCBDYW4gb25seSBiZSBmaW5pdGUgbnVtYmVyIG9yIG51bGwvdW5kZWZpbmVkIG9yIE5hTi5cbiAgICogQHBhcmFtIHtudW1iZXJ9IHJhbmdlRW5kIENhbiBvbmx5IGJlIGZpbml0ZSBudW1iZXIgb3IgbnVsbC91bmRlZmluZWQgb3IgTmFOLlxuICAgKi9cbiAgc2V0UmFuZ2U6IGZ1bmN0aW9uIChyYW5nZVN0YXJ0LCByYW5nZUVuZCkge1xuICAgIHRoaXMub3B0aW9uLnJhbmdlU3RhcnQgPSByYW5nZVN0YXJ0O1xuICAgIHRoaXMub3B0aW9uLnJhbmdlRW5kID0gcmFuZ2VFbmQ7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFJlc2V0IHJhbmdlXG4gICAqL1xuICByZXNldFJhbmdlOiBmdW5jdGlvbiAoKSB7XG4gICAgLy8gcmFuZ2VTdGFydCBhbmQgcmFuZ2VFbmQgaXMgcmVhZG9ubHkuXG4gICAgdGhpcy5vcHRpb24ucmFuZ2VTdGFydCA9IHRoaXMub3B0aW9uLnJhbmdlRW5kID0gbnVsbDtcbiAgfVxufTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFEQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBNURBO0FBOERBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/axisModelCommonMixin.js
