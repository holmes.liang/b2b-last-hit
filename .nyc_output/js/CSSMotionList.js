__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "genCSSMotionList", function() { return genCSSMotionList; });
/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _CSSMotion__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./CSSMotion */ "./node_modules/rc-animate/es/CSSMotion.js");
/* harmony import */ var _util_motion__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./util/motion */ "./node_modules/rc-animate/es/util/motion.js");
/* harmony import */ var _util_diff__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./util/diff */ "./node_modules/rc-animate/es/util/diff.js");












var MOTION_PROP_NAMES = Object.keys(_CSSMotion__WEBPACK_IMPORTED_MODULE_9__["MotionPropTypes"]);
function genCSSMotionList(transitionSupport) {
  var CSSMotion = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _CSSMotion__WEBPACK_IMPORTED_MODULE_9__["default"];

  var CSSMotionList = function (_React$Component) {
    babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(CSSMotionList, _React$Component);

    function CSSMotionList() {
      var _ref;

      var _temp, _this, _ret;

      babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, CSSMotionList);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default()(this, (_ref = CSSMotionList.__proto__ || Object.getPrototypeOf(CSSMotionList)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
        keyEntities: []
      }, _this.removeKey = function (removeKey) {
        _this.setState(function (_ref2) {
          var keyEntities = _ref2.keyEntities;
          return {
            keyEntities: keyEntities.map(function (entity) {
              if (entity.key !== removeKey) return entity;
              return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, entity, {
                status: _util_diff__WEBPACK_IMPORTED_MODULE_11__["STATUS_REMOVED"]
              });
            })
          };
        });
      }, _temp), babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default()(_this, _ret);
    }

    babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(CSSMotionList, [{
      key: 'render',
      value: function render() {
        var _this2 = this;

        var keyEntities = this.state.keyEntities;

        var _props = this.props,
            component = _props.component,
            children = _props.children,
            restProps = babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0___default()(_props, ['component', 'children']);

        var Component = component || react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment;
        var motionProps = {};
        MOTION_PROP_NAMES.forEach(function (prop) {
          motionProps[prop] = restProps[prop];
          delete restProps[prop];
        });
        delete restProps.keys;
        return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(Component, restProps, keyEntities.map(function (_ref3) {
          var status = _ref3.status,
              eventProps = babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0___default()(_ref3, ['status']);

          var visible = status === _util_diff__WEBPACK_IMPORTED_MODULE_11__["STATUS_ADD"] || status === _util_diff__WEBPACK_IMPORTED_MODULE_11__["STATUS_KEEP"];
          return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(CSSMotion, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, motionProps, {
            key: eventProps.key,
            visible: visible,
            eventProps: eventProps,
            onLeaveEnd: function onLeaveEnd() {
              if (motionProps.onLeaveEnd) {
                motionProps.onLeaveEnd.apply(motionProps, arguments);
              }

              _this2.removeKey(eventProps.key);
            }
          }), children);
        }));
      }
    }], [{
      key: 'getDerivedStateFromProps',
      value: function getDerivedStateFromProps(_ref4, _ref5) {
        var keys = _ref4.keys;
        var keyEntities = _ref5.keyEntities;
        var parsedKeyObjects = Object(_util_diff__WEBPACK_IMPORTED_MODULE_11__["parseKeys"])(keys); // Always as keep when motion not support

        if (!transitionSupport) {
          return {
            keyEntities: parsedKeyObjects.map(function (obj) {
              return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, obj, {
                status: _util_diff__WEBPACK_IMPORTED_MODULE_11__["STATUS_KEEP"]
              });
            })
          };
        }

        var mixedKeyEntities = Object(_util_diff__WEBPACK_IMPORTED_MODULE_11__["diffKeys"])(keyEntities, parsedKeyObjects);
        var keyEntitiesLen = keyEntities.length;
        return {
          keyEntities: mixedKeyEntities.filter(function (entity) {
            // IE 9 not support Array.prototype.find
            var prevEntity = null;

            for (var i = 0; i < keyEntitiesLen; i += 1) {
              var currentEntity = keyEntities[i];

              if (currentEntity.key === entity.key) {
                prevEntity = currentEntity;
                break;
              }
            } // Remove if already mark as removed


            if (prevEntity && prevEntity.status === _util_diff__WEBPACK_IMPORTED_MODULE_11__["STATUS_REMOVED"] && entity.status === _util_diff__WEBPACK_IMPORTED_MODULE_11__["STATUS_REMOVE"]) {
              return false;
            }

            return true;
          })
        };
      }
    }]);

    return CSSMotionList;
  }(react__WEBPACK_IMPORTED_MODULE_6___default.a.Component);

  CSSMotionList.propTypes = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, CSSMotion.propTypes, {
    component: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.bool]),
    keys: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.array
  });
  CSSMotionList.defaultProps = {
    component: 'div'
  };
  Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_7__["polyfill"])(CSSMotionList);
  return CSSMotionList;
}
/* harmony default export */ __webpack_exports__["default"] = (genCSSMotionList(_util_motion__WEBPACK_IMPORTED_MODULE_10__["supportTransition"]));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtYW5pbWF0ZS9lcy9DU1NNb3Rpb25MaXN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtYW5pbWF0ZS9lcy9DU1NNb3Rpb25MaXN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJztcbmltcG9ydCBfZXh0ZW5kcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcyc7XG5pbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX2NyZWF0ZUNsYXNzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcyc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBPcmlnaW5DU1NNb3Rpb24sIHsgTW90aW9uUHJvcFR5cGVzIH0gZnJvbSAnLi9DU1NNb3Rpb24nO1xuaW1wb3J0IHsgc3VwcG9ydFRyYW5zaXRpb24gfSBmcm9tICcuL3V0aWwvbW90aW9uJztcbmltcG9ydCB7IFNUQVRVU19BREQsIFNUQVRVU19LRUVQLCBTVEFUVVNfUkVNT1ZFLCBTVEFUVVNfUkVNT1ZFRCwgZGlmZktleXMsIHBhcnNlS2V5cyB9IGZyb20gJy4vdXRpbC9kaWZmJztcblxudmFyIE1PVElPTl9QUk9QX05BTUVTID0gT2JqZWN0LmtleXMoTW90aW9uUHJvcFR5cGVzKTtcblxuZXhwb3J0IGZ1bmN0aW9uIGdlbkNTU01vdGlvbkxpc3QodHJhbnNpdGlvblN1cHBvcnQpIHtcbiAgdmFyIENTU01vdGlvbiA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogT3JpZ2luQ1NTTW90aW9uO1xuXG4gIHZhciBDU1NNb3Rpb25MaXN0ID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgICBfaW5oZXJpdHMoQ1NTTW90aW9uTGlzdCwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgICBmdW5jdGlvbiBDU1NNb3Rpb25MaXN0KCkge1xuICAgICAgdmFyIF9yZWY7XG5cbiAgICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDU1NNb3Rpb25MaXN0KTtcblxuICAgICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoX3JlZiA9IENTU01vdGlvbkxpc3QuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihDU1NNb3Rpb25MaXN0KSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuc3RhdGUgPSB7XG4gICAgICAgIGtleUVudGl0aWVzOiBbXVxuICAgICAgfSwgX3RoaXMucmVtb3ZlS2V5ID0gZnVuY3Rpb24gKHJlbW92ZUtleSkge1xuICAgICAgICBfdGhpcy5zZXRTdGF0ZShmdW5jdGlvbiAoX3JlZjIpIHtcbiAgICAgICAgICB2YXIga2V5RW50aXRpZXMgPSBfcmVmMi5rZXlFbnRpdGllcztcbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAga2V5RW50aXRpZXM6IGtleUVudGl0aWVzLm1hcChmdW5jdGlvbiAoZW50aXR5KSB7XG4gICAgICAgICAgICAgIGlmIChlbnRpdHkua2V5ICE9PSByZW1vdmVLZXkpIHJldHVybiBlbnRpdHk7XG4gICAgICAgICAgICAgIHJldHVybiBfZXh0ZW5kcyh7fSwgZW50aXR5LCB7XG4gICAgICAgICAgICAgICAgc3RhdHVzOiBTVEFUVVNfUkVNT1ZFRFxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfTtcbiAgICAgICAgfSk7XG4gICAgICB9LCBfdGVtcCksIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKF90aGlzLCBfcmV0KTtcbiAgICB9XG5cbiAgICBfY3JlYXRlQ2xhc3MoQ1NTTW90aW9uTGlzdCwgW3tcbiAgICAgIGtleTogJ3JlbmRlcicsXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgICAgICB2YXIga2V5RW50aXRpZXMgPSB0aGlzLnN0YXRlLmtleUVudGl0aWVzO1xuXG4gICAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgICAgY29tcG9uZW50ID0gX3Byb3BzLmNvbXBvbmVudCxcbiAgICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgICAgcmVzdFByb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKF9wcm9wcywgWydjb21wb25lbnQnLCAnY2hpbGRyZW4nXSk7XG5cbiAgICAgICAgdmFyIENvbXBvbmVudCA9IGNvbXBvbmVudCB8fCBSZWFjdC5GcmFnbWVudDtcblxuICAgICAgICB2YXIgbW90aW9uUHJvcHMgPSB7fTtcbiAgICAgICAgTU9USU9OX1BST1BfTkFNRVMuZm9yRWFjaChmdW5jdGlvbiAocHJvcCkge1xuICAgICAgICAgIG1vdGlvblByb3BzW3Byb3BdID0gcmVzdFByb3BzW3Byb3BdO1xuICAgICAgICAgIGRlbGV0ZSByZXN0UHJvcHNbcHJvcF07XG4gICAgICAgIH0pO1xuICAgICAgICBkZWxldGUgcmVzdFByb3BzLmtleXM7XG5cbiAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgQ29tcG9uZW50LFxuICAgICAgICAgIHJlc3RQcm9wcyxcbiAgICAgICAgICBrZXlFbnRpdGllcy5tYXAoZnVuY3Rpb24gKF9yZWYzKSB7XG4gICAgICAgICAgICB2YXIgc3RhdHVzID0gX3JlZjMuc3RhdHVzLFxuICAgICAgICAgICAgICAgIGV2ZW50UHJvcHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3JlZjMsIFsnc3RhdHVzJ10pO1xuXG4gICAgICAgICAgICB2YXIgdmlzaWJsZSA9IHN0YXR1cyA9PT0gU1RBVFVTX0FERCB8fCBzdGF0dXMgPT09IFNUQVRVU19LRUVQO1xuICAgICAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICAgIENTU01vdGlvbixcbiAgICAgICAgICAgICAgX2V4dGVuZHMoe30sIG1vdGlvblByb3BzLCB7XG4gICAgICAgICAgICAgICAga2V5OiBldmVudFByb3BzLmtleSxcbiAgICAgICAgICAgICAgICB2aXNpYmxlOiB2aXNpYmxlLFxuICAgICAgICAgICAgICAgIGV2ZW50UHJvcHM6IGV2ZW50UHJvcHMsXG4gICAgICAgICAgICAgICAgb25MZWF2ZUVuZDogZnVuY3Rpb24gb25MZWF2ZUVuZCgpIHtcbiAgICAgICAgICAgICAgICAgIGlmIChtb3Rpb25Qcm9wcy5vbkxlYXZlRW5kKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vdGlvblByb3BzLm9uTGVhdmVFbmQuYXBwbHkobW90aW9uUHJvcHMsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBfdGhpczIucmVtb3ZlS2V5KGV2ZW50UHJvcHMua2V5KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICBjaGlsZHJlblxuICAgICAgICAgICAgKTtcbiAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgICAgfVxuICAgIH1dLCBbe1xuICAgICAga2V5OiAnZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzJyxcbiAgICAgIHZhbHVlOiBmdW5jdGlvbiBnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMoX3JlZjQsIF9yZWY1KSB7XG4gICAgICAgIHZhciBrZXlzID0gX3JlZjQua2V5cztcbiAgICAgICAgdmFyIGtleUVudGl0aWVzID0gX3JlZjUua2V5RW50aXRpZXM7XG5cbiAgICAgICAgdmFyIHBhcnNlZEtleU9iamVjdHMgPSBwYXJzZUtleXMoa2V5cyk7XG5cbiAgICAgICAgLy8gQWx3YXlzIGFzIGtlZXAgd2hlbiBtb3Rpb24gbm90IHN1cHBvcnRcbiAgICAgICAgaWYgKCF0cmFuc2l0aW9uU3VwcG9ydCkge1xuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBrZXlFbnRpdGllczogcGFyc2VkS2V5T2JqZWN0cy5tYXAoZnVuY3Rpb24gKG9iaikge1xuICAgICAgICAgICAgICByZXR1cm4gX2V4dGVuZHMoe30sIG9iaiwgeyBzdGF0dXM6IFNUQVRVU19LRUVQIH0pO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICB9O1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIG1peGVkS2V5RW50aXRpZXMgPSBkaWZmS2V5cyhrZXlFbnRpdGllcywgcGFyc2VkS2V5T2JqZWN0cyk7XG5cbiAgICAgICAgdmFyIGtleUVudGl0aWVzTGVuID0ga2V5RW50aXRpZXMubGVuZ3RoO1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGtleUVudGl0aWVzOiBtaXhlZEtleUVudGl0aWVzLmZpbHRlcihmdW5jdGlvbiAoZW50aXR5KSB7XG4gICAgICAgICAgICAvLyBJRSA5IG5vdCBzdXBwb3J0IEFycmF5LnByb3RvdHlwZS5maW5kXG4gICAgICAgICAgICB2YXIgcHJldkVudGl0eSA9IG51bGw7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGtleUVudGl0aWVzTGVuOyBpICs9IDEpIHtcbiAgICAgICAgICAgICAgdmFyIGN1cnJlbnRFbnRpdHkgPSBrZXlFbnRpdGllc1tpXTtcbiAgICAgICAgICAgICAgaWYgKGN1cnJlbnRFbnRpdHkua2V5ID09PSBlbnRpdHkua2V5KSB7XG4gICAgICAgICAgICAgICAgcHJldkVudGl0eSA9IGN1cnJlbnRFbnRpdHk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gUmVtb3ZlIGlmIGFscmVhZHkgbWFyayBhcyByZW1vdmVkXG4gICAgICAgICAgICBpZiAocHJldkVudGl0eSAmJiBwcmV2RW50aXR5LnN0YXR1cyA9PT0gU1RBVFVTX1JFTU9WRUQgJiYgZW50aXR5LnN0YXR1cyA9PT0gU1RBVFVTX1JFTU9WRSkge1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICB9KVxuICAgICAgICB9O1xuICAgICAgfVxuICAgIH1dKTtcblxuICAgIHJldHVybiBDU1NNb3Rpb25MaXN0O1xuICB9KFJlYWN0LkNvbXBvbmVudCk7XG5cbiAgQ1NTTW90aW9uTGlzdC5wcm9wVHlwZXMgPSBfZXh0ZW5kcyh7fSwgQ1NTTW90aW9uLnByb3BUeXBlcywge1xuICAgIGNvbXBvbmVudDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLmJvb2xdKSxcbiAgICBrZXlzOiBQcm9wVHlwZXMuYXJyYXlcbiAgfSk7XG4gIENTU01vdGlvbkxpc3QuZGVmYXVsdFByb3BzID0ge1xuICAgIGNvbXBvbmVudDogJ2RpdidcbiAgfTtcblxuXG4gIHBvbHlmaWxsKENTU01vdGlvbkxpc3QpO1xuXG4gIHJldHVybiBDU1NNb3Rpb25MaXN0O1xufVxuXG5leHBvcnQgZGVmYXVsdCBnZW5DU1NNb3Rpb25MaXN0KHN1cHBvcnRUcmFuc2l0aW9uKTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFJQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFUQTtBQWFBO0FBRUE7QUE5Q0E7QUFnREE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBakJBO0FBbUJBO0FBdkNBO0FBQ0E7QUF5Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFLQTtBQUVBO0FBQ0E7QUFFQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-animate/es/CSSMotionList.js
