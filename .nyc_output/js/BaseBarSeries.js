/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var SeriesModel = __webpack_require__(/*! ../../model/Series */ "./node_modules/echarts/lib/model/Series.js");

var createListFromArray = __webpack_require__(/*! ../helper/createListFromArray */ "./node_modules/echarts/lib/chart/helper/createListFromArray.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var _default = SeriesModel.extend({
  type: 'series.__base_bar__',
  getInitialData: function getInitialData(option, ecModel) {
    return createListFromArray(this.getSource(), this);
  },
  getMarkerPosition: function getMarkerPosition(value) {
    var coordSys = this.coordinateSystem;

    if (coordSys) {
      // PENDING if clamp ?
      var pt = coordSys.dataToPoint(coordSys.clampData(value));
      var data = this.getData();
      var offset = data.getLayout('offset');
      var size = data.getLayout('size');
      var offsetIndex = coordSys.getBaseAxis().isHorizontal() ? 0 : 1;
      pt[offsetIndex] += offset + size / 2;
      return pt;
    }

    return [NaN, NaN];
  },
  defaultOption: {
    zlevel: 0,
    // 一级层叠
    z: 2,
    // 二级层叠
    coordinateSystem: 'cartesian2d',
    legendHoverLink: true,
    // stack: null
    // Cartesian coordinate system
    // xAxisIndex: 0,
    // yAxisIndex: 0,
    // 最小高度改为0
    barMinHeight: 0,
    // 最小角度为0，仅对极坐标系下的柱状图有效
    barMinAngle: 0,
    // cursor: null,
    large: false,
    largeThreshold: 400,
    progressive: 3e3,
    progressiveChunkMode: 'mod',
    // barMaxWidth: null,
    // 默认自适应
    // barWidth: null,
    // 柱间距离，默认为柱形宽度的30%，可设固定值
    // barGap: '30%',
    // 类目间柱形距离，默认为类目间距的20%，可设固定值
    // barCategoryGap: '20%',
    // label: {
    //      show: false
    // },
    itemStyle: {},
    emphasis: {}
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvYmFyL0Jhc2VCYXJTZXJpZXMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jaGFydC9iYXIvQmFzZUJhclNlcmllcy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIFNlcmllc01vZGVsID0gcmVxdWlyZShcIi4uLy4uL21vZGVsL1Nlcmllc1wiKTtcblxudmFyIGNyZWF0ZUxpc3RGcm9tQXJyYXkgPSByZXF1aXJlKFwiLi4vaGVscGVyL2NyZWF0ZUxpc3RGcm9tQXJyYXlcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBfZGVmYXVsdCA9IFNlcmllc01vZGVsLmV4dGVuZCh7XG4gIHR5cGU6ICdzZXJpZXMuX19iYXNlX2Jhcl9fJyxcbiAgZ2V0SW5pdGlhbERhdGE6IGZ1bmN0aW9uIChvcHRpb24sIGVjTW9kZWwpIHtcbiAgICByZXR1cm4gY3JlYXRlTGlzdEZyb21BcnJheSh0aGlzLmdldFNvdXJjZSgpLCB0aGlzKTtcbiAgfSxcbiAgZ2V0TWFya2VyUG9zaXRpb246IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgIHZhciBjb29yZFN5cyA9IHRoaXMuY29vcmRpbmF0ZVN5c3RlbTtcblxuICAgIGlmIChjb29yZFN5cykge1xuICAgICAgLy8gUEVORElORyBpZiBjbGFtcCA/XG4gICAgICB2YXIgcHQgPSBjb29yZFN5cy5kYXRhVG9Qb2ludChjb29yZFN5cy5jbGFtcERhdGEodmFsdWUpKTtcbiAgICAgIHZhciBkYXRhID0gdGhpcy5nZXREYXRhKCk7XG4gICAgICB2YXIgb2Zmc2V0ID0gZGF0YS5nZXRMYXlvdXQoJ29mZnNldCcpO1xuICAgICAgdmFyIHNpemUgPSBkYXRhLmdldExheW91dCgnc2l6ZScpO1xuICAgICAgdmFyIG9mZnNldEluZGV4ID0gY29vcmRTeXMuZ2V0QmFzZUF4aXMoKS5pc0hvcml6b250YWwoKSA/IDAgOiAxO1xuICAgICAgcHRbb2Zmc2V0SW5kZXhdICs9IG9mZnNldCArIHNpemUgLyAyO1xuICAgICAgcmV0dXJuIHB0O1xuICAgIH1cblxuICAgIHJldHVybiBbTmFOLCBOYU5dO1xuICB9LFxuICBkZWZhdWx0T3B0aW9uOiB7XG4gICAgemxldmVsOiAwLFxuICAgIC8vIOS4gOe6p+WxguWPoFxuICAgIHo6IDIsXG4gICAgLy8g5LqM57qn5bGC5Y+gXG4gICAgY29vcmRpbmF0ZVN5c3RlbTogJ2NhcnRlc2lhbjJkJyxcbiAgICBsZWdlbmRIb3Zlckxpbms6IHRydWUsXG4gICAgLy8gc3RhY2s6IG51bGxcbiAgICAvLyBDYXJ0ZXNpYW4gY29vcmRpbmF0ZSBzeXN0ZW1cbiAgICAvLyB4QXhpc0luZGV4OiAwLFxuICAgIC8vIHlBeGlzSW5kZXg6IDAsXG4gICAgLy8g5pyA5bCP6auY5bqm5pS55Li6MFxuICAgIGJhck1pbkhlaWdodDogMCxcbiAgICAvLyDmnIDlsI/op5LluqbkuLow77yM5LuF5a+55p6B5Z2Q5qCH57O75LiL55qE5p+x54q25Zu+5pyJ5pWIXG4gICAgYmFyTWluQW5nbGU6IDAsXG4gICAgLy8gY3Vyc29yOiBudWxsLFxuICAgIGxhcmdlOiBmYWxzZSxcbiAgICBsYXJnZVRocmVzaG9sZDogNDAwLFxuICAgIHByb2dyZXNzaXZlOiAzZTMsXG4gICAgcHJvZ3Jlc3NpdmVDaHVua01vZGU6ICdtb2QnLFxuICAgIC8vIGJhck1heFdpZHRoOiBudWxsLFxuICAgIC8vIOm7mOiupOiHqumAguW6lFxuICAgIC8vIGJhcldpZHRoOiBudWxsLFxuICAgIC8vIOafsemXtOi3neemu++8jOm7mOiupOS4uuafseW9ouWuveW6pueahDMwJe+8jOWPr+iuvuWbuuWumuWAvFxuICAgIC8vIGJhckdhcDogJzMwJScsXG4gICAgLy8g57G755uu6Ze05p+x5b2i6Led56a777yM6buY6K6k5Li657G755uu6Ze06Led55qEMjAl77yM5Y+v6K6+5Zu65a6a5YC8XG4gICAgLy8gYmFyQ2F0ZWdvcnlHYXA6ICcyMCUnLFxuICAgIC8vIGxhYmVsOiB7XG4gICAgLy8gICAgICBzaG93OiBmYWxzZVxuICAgIC8vIH0sXG4gICAgaXRlbVN0eWxlOiB7fSxcbiAgICBlbXBoYXNpczoge31cbiAgfVxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBL0JBO0FBckJBO0FBQ0E7QUF1REEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/bar/BaseBarSeries.js
