__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Clause", function() { return Clause; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _basic_clause__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./basic-clause */ "./src/app/desk/quote/SAIC/iar/basic-clause.tsx");
/* harmony import */ var _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @desk/quote/SAIC/all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _component_render_clause__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./component/render-clause */ "./src/app/desk/quote/SAIC/iar/component/render-clause.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk/quote/SAIC/iar/index */ "./src/app/desk/quote/SAIC/iar/index.tsx");
/* harmony import */ var _desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk/quote/SAIC/phs/components/side-info */ "./src/app/desk/quote/SAIC/phs/components/side-info.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/clause.tsx";










var Clause =
/*#__PURE__*/
function (_BasicClause) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Clause, _BasicClause);

  function Clause() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Clause);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Clause).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Clause, [{
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "renderClause",
    value: function renderClause() {
      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_component_render_clause__WEBPACK_IMPORTED_MODULE_13__["default"], {
        that: this,
        form: form,
        model: model,
        getProp: this.getProp,
        generatePropName: this.generatePropName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 20
        },
        __self: this
      });
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      var model = this.props.model;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_15__["default"], {
        lineList: _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_14__["list"],
        premiumId: "cartPremium.totalPremium",
        model: model,
        dataFixed: "policy",
        activeStep: "Clauses",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 30
        },
        __self: this
      });
    } // action

  }, {
    key: "handleNext",
    value: function handleNext() {
      var _this = this;

      this.props.form.validateFields(function (err, fieldsValue) {
        if (err) {
          return;
        }

        var newCart = _this.props.mergePolicyToServiceModel();

        newCart["policy.ext._ui.step"] = _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_9__["AllSteps"].ATTACHMENTS;
        _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_10__["Apis"].CART_MERGE, newCart, {
          loading: true
        }).then(function (response) {
          _this.props.mergePolicyToUIModel(response.body.respData || {});

          if (response.body.respCode !== "0000") return;
          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_11__["pageTo"])(_this, _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_9__["AllSteps"].ATTACHMENTS);
        }).catch(function (error) {
          return console.error(error);
        });
      });
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      if (dataIdPrefix) return "policy.ext.".concat(dataIdPrefix, ".").concat(propName);
      return "policy.ext.".concat(propName);
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      if (lodash__WEBPACK_IMPORTED_MODULE_12___default.a.isEmpty(propName)) {
        return "policy";
      }

      return "policy.".concat(propName);
    }
  }]);

  return Clause;
}(_basic_clause__WEBPACK_IMPORTED_MODULE_8__["BasicClause"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY2xhdXNlLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL1NBSUMvaWFyL2NsYXVzZS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IEJhc2ljQ2xhdXNlIH0gZnJvbSBcIi4vYmFzaWMtY2xhdXNlXCI7XG5pbXBvcnQgeyBBbGxTdGVwcyB9IGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL2FsbC1zdGVwc1wiO1xuaW1wb3J0IHsgQWpheCwgQXBpcyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBwYWdlVG8gfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3dpemFyZFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IFJlbmRlckNsYXVzZSBmcm9tIFwiLi9jb21wb25lbnQvcmVuZGVyLWNsYXVzZVwiO1xuaW1wb3J0IHsgbGlzdCB9IGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL2lhci9pbmRleFwiO1xuaW1wb3J0IFNpZGVJbmZvUGhzIGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL3Bocy9jb21wb25lbnRzL3NpZGUtaW5mb1wiO1xuXG5jbGFzcyBDbGF1c2UgZXh0ZW5kcyBCYXNpY0NsYXVzZTxhbnksIGFueSwgYW55PiB7XG4gIGFzeW5jIGNvbXBvbmVudERpZE1vdW50KCkge1xuICB9XG5cbiAgcmVuZGVyQ2xhdXNlKCkge1xuICAgIGNvbnN0IHtcbiAgICAgIGZvcm0sXG4gICAgICBtb2RlbCxcbiAgICB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPFJlbmRlckNsYXVzZVxuICAgICAgdGhhdD17dGhpc31cbiAgICAgIGZvcm09e2Zvcm19XG4gICAgICBtb2RlbD17bW9kZWx9XG4gICAgICBnZXRQcm9wPXt0aGlzLmdldFByb3B9XG4gICAgICBnZW5lcmF0ZVByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWV9Lz47XG4gIH1cblxuICByZW5kZXJTaWRlSW5mbygpOiBhbnkge1xuICAgIGNvbnN0IHsgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIDxTaWRlSW5mb1Boc1xuICAgICAgbGluZUxpc3Q9e2xpc3R9XG4gICAgICBwcmVtaXVtSWQ9e1wiY2FydFByZW1pdW0udG90YWxQcmVtaXVtXCJ9XG4gICAgICBtb2RlbD17bW9kZWx9XG4gICAgICBkYXRhRml4ZWQ9e1wicG9saWN5XCJ9XG4gICAgICBhY3RpdmVTdGVwPXtcIkNsYXVzZXNcIn0vPjtcbiAgfVxuXG4gIC8vIGFjdGlvblxuICBoYW5kbGVOZXh0KCkge1xuICAgIHRoaXMucHJvcHMuZm9ybS52YWxpZGF0ZUZpZWxkcygoZXJyOiBhbnksIGZpZWxkc1ZhbHVlOiBhbnkpID0+IHtcbiAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgY29uc3QgbmV3Q2FydCA9IHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbCgpO1xuICAgICAgbmV3Q2FydFtcInBvbGljeS5leHQuX3VpLnN0ZXBcIl0gPSBBbGxTdGVwcy5BVFRBQ0hNRU5UUztcbiAgICAgIEFqYXgucG9zdChBcGlzLkNBUlRfTUVSR0UsIG5ld0NhcnQsIHsgbG9hZGluZzogdHJ1ZSB9KVxuICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvVUlNb2RlbChyZXNwb25zZS5ib2R5LnJlc3BEYXRhIHx8IHt9KTtcbiAgICAgICAgICBpZiAocmVzcG9uc2UuYm9keS5yZXNwQ29kZSAhPT0gXCIwMDAwXCIpIHJldHVybjtcbiAgICAgICAgICBwYWdlVG8odGhpcywgQWxsU3RlcHMuQVRUQUNITUVOVFMpO1xuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5lcnJvcihlcnJvcikpO1xuICAgIH0pO1xuICB9XG5cbiAgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZTogc3RyaW5nLCBkYXRhSWRQcmVmaXg/OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIGlmIChkYXRhSWRQcmVmaXgpIHJldHVybiBgcG9saWN5LmV4dC4ke2RhdGFJZFByZWZpeH0uJHtwcm9wTmFtZX1gO1xuICAgIHJldHVybiBgcG9saWN5LmV4dC4ke3Byb3BOYW1lfWA7XG4gIH1cblxuICBnZXRQcm9wKHByb3BOYW1lPzogc3RyaW5nKSB7XG4gICAgaWYgKF8uaXNFbXB0eShwcm9wTmFtZSkpIHtcbiAgICAgIHJldHVybiBcInBvbGljeVwiO1xuICAgIH1cbiAgICByZXR1cm4gYHBvbGljeS4ke3Byb3BOYW1lfWA7XG4gIH1cbn1cblxuZXhwb3J0IHsgQ2xhdXNlIH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BOzs7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7O0FBdkRBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/clause.tsx
