__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(clearImmediate, setImmediate) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! immutable */ "./node_modules/rc-editor-core/node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var setimmediate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! setimmediate */ "./node_modules/setimmediate/setImmediate.js");
/* harmony import */ var setimmediate__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(setimmediate__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _Toolbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Toolbar */ "./node_modules/rc-editor-core/es/Toolbar/index.js");
/* harmony import */ var _ConfigStore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ConfigStore */ "./node_modules/rc-editor-core/es/EditorCore/ConfigStore.js");
/* harmony import */ var _export_getHTML__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./export/getHTML */ "./node_modules/rc-editor-core/es/EditorCore/export/getHTML.js");
/* harmony import */ var _export_exportText__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./export/exportText */ "./node_modules/rc-editor-core/es/EditorCore/export/exportText.js");
/* harmony import */ var _customHTML2Content__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./customHTML2Content */ "./node_modules/rc-editor-core/es/EditorCore/customHTML2Content.js");
var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}
/* tslint:disable:member-ordering interface-name */













var hasCommandModifier = draft_js__WEBPACK_IMPORTED_MODULE_2__["KeyBindingUtil"].hasCommandModifier;

function noop() {}

;
var defaultPluginConfig = {};
var focusDummyStyle = {
  width: 0,
  opacity: 0,
  border: 0,
  position: 'absolute',
  left: 0,
  top: 0
};
var toolbar = Object(_Toolbar__WEBPACK_IMPORTED_MODULE_6__["createToolbar"])();
var configStore = new _ConfigStore__WEBPACK_IMPORTED_MODULE_7__["default"]();

var EditorCore = function (_React$Component) {
  _inherits(EditorCore, _React$Component);

  function EditorCore(props) {
    _classCallCheck(this, EditorCore);

    var _this = _possibleConstructorReturn(this, _React$Component.call(this, props));

    _this.cancelForceUpdateImmediate = function () {
      clearImmediate(_this.forceUpdateImmediate);
      _this.forceUpdateImmediate = null;
    };

    _this.handlePastedText = function (text, html) {
      var editorState = _this.state.editorState;

      if (html) {
        var contentState = editorState.getCurrentContent();
        var selection = editorState.getSelection();
        var fragment = Object(_customHTML2Content__WEBPACK_IMPORTED_MODULE_10__["default"])(html, contentState);
        var pastedContent = draft_js__WEBPACK_IMPORTED_MODULE_2__["Modifier"].replaceWithFragment(contentState, selection, fragment);
        var newContent = pastedContent.merge({
          selectionBefore: selection,
          selectionAfter: pastedContent.getSelectionAfter().set('hasFocus', true)
        });

        _this.setEditorState(draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].push(editorState, newContent, 'insert-fragment'), true);

        return 'handled';
      }

      return 'not-handled';
    };

    _this.plugins = Object(immutable__WEBPACK_IMPORTED_MODULE_3__["List"])(Object(immutable__WEBPACK_IMPORTED_MODULE_3__["List"])(props.plugins).flatten(true));
    var editorState = void 0;

    if (props.value !== undefined) {
      if (props.value instanceof draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"]) {
        editorState = props.value || draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].createEmpty();
      } else {
        editorState = draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].createEmpty();
      }
    } else {
      editorState = draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].createEmpty();
    }

    editorState = _this.generatorDefaultValue(editorState);
    _this.state = {
      plugins: _this.reloadPlugins(),
      editorState: editorState,
      customStyleMap: {},
      customBlockStyleMap: {},
      compositeDecorator: null
    };

    if (props.value !== undefined) {
      _this.controlledMode = true;
    }

    return _this;
  }

  EditorCore.ToEditorState = function ToEditorState(text) {
    var createEmptyContentState = draft_js__WEBPACK_IMPORTED_MODULE_2__["ContentState"].createFromText(Object(_export_exportText__WEBPACK_IMPORTED_MODULE_9__["decodeContent"])(text) || '');
    var editorState = draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].createWithContent(createEmptyContentState);
    return draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].forceSelection(editorState, createEmptyContentState.getSelectionAfter());
  };

  EditorCore.prototype.getDefaultValue = function getDefaultValue() {
    var _props = this.props,
        defaultValue = _props.defaultValue,
        value = _props.value;
    return value || defaultValue;
  };

  EditorCore.prototype.Reset = function Reset() {
    var defaultValue = this.getDefaultValue();
    var contentState = defaultValue ? defaultValue.getCurrentContent() : draft_js__WEBPACK_IMPORTED_MODULE_2__["ContentState"].createFromText('');
    var updatedEditorState = draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].push(this.state.editorState, contentState, 'remove-range');
    this.setEditorState(draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].forceSelection(updatedEditorState, contentState.getSelectionBefore()));
  };

  EditorCore.prototype.SetText = function SetText(text) {
    var createTextContentState = draft_js__WEBPACK_IMPORTED_MODULE_2__["ContentState"].createFromText(text || '');
    var editorState = draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].push(this.state.editorState, createTextContentState, 'change-block-data');
    this.setEditorState(draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].moveFocusToEnd(editorState), true);
  };

  EditorCore.prototype.getChildContext = function getChildContext() {
    return {
      getEditorState: this.getEditorState,
      setEditorState: this.setEditorState
    };
  };

  EditorCore.prototype.reloadPlugins = function reloadPlugins() {
    var _this2 = this;

    return this.plugins && this.plugins.size ? this.plugins.map(function (plugin) {
      // 如果插件有 callbacks 方法,则认为插件已经加载。
      if (plugin.callbacks) {
        return plugin;
      } // 如果插件有 constructor 方法,则构造插件


      if (plugin.hasOwnProperty('constructor')) {
        var pluginConfig = _extends(_this2.props.pluginConfig, plugin.config || {}, defaultPluginConfig);

        return plugin.constructor(pluginConfig);
      } // else 无效插件


      console.warn('>> 插件: [', plugin.name, '] 无效。插件或许已经过期。');
      return false;
    }).filter(function (plugin) {
      return plugin;
    }).toArray() : [];
  };

  EditorCore.prototype.componentWillMount = function componentWillMount() {
    var plugins = this.initPlugins().concat([toolbar]);
    var customStyleMap = {};
    var customBlockStyleMap = {};
    var customBlockRenderMap = Object(immutable__WEBPACK_IMPORTED_MODULE_3__["Map"])(draft_js__WEBPACK_IMPORTED_MODULE_2__["DefaultDraftBlockRenderMap"]);
    var toHTMLList = Object(immutable__WEBPACK_IMPORTED_MODULE_3__["List"])([]); // initialize compositeDecorator

    var compositeDecorator = new draft_js__WEBPACK_IMPORTED_MODULE_2__["CompositeDecorator"](plugins.filter(function (plugin) {
      return plugin.decorators !== undefined;
    }).map(function (plugin) {
      return plugin.decorators;
    }).reduce(function (prev, curr) {
      return prev.concat(curr);
    }, [])); // initialize Toolbar

    var toolbarPlugins = Object(immutable__WEBPACK_IMPORTED_MODULE_3__["List"])(plugins.filter(function (plugin) {
      return !!plugin.component && plugin.name !== 'toolbar';
    })); // load inline styles...

    plugins.forEach(function (plugin) {
      var styleMap = plugin.styleMap,
          blockStyleMap = plugin.blockStyleMap,
          blockRenderMap = plugin.blockRenderMap,
          toHtml = plugin.toHtml;

      if (styleMap) {
        for (var key in styleMap) {
          if (styleMap.hasOwnProperty(key)) {
            customStyleMap[key] = styleMap[key];
          }
        }
      }

      if (blockStyleMap) {
        for (var _key in blockStyleMap) {
          if (blockStyleMap.hasOwnProperty(_key)) {
            customBlockStyleMap[_key] = blockStyleMap[_key];
            customBlockRenderMap = customBlockRenderMap.set(_key, {
              element: null
            });
          }
        }
      }

      if (toHtml) {
        toHTMLList = toHTMLList.push(toHtml);
      }

      if (blockRenderMap) {
        for (var _key2 in blockRenderMap) {
          if (blockRenderMap.hasOwnProperty(_key2)) {
            customBlockRenderMap = customBlockRenderMap.set(_key2, blockRenderMap[_key2]);
          }
        }
      }
    });
    configStore.set('customStyleMap', customStyleMap);
    configStore.set('customBlockStyleMap', customBlockStyleMap);
    configStore.set('blockRenderMap', customBlockRenderMap);
    configStore.set('customStyleFn', this.customStyleFn.bind(this));
    configStore.set('toHTMLList', toHTMLList);
    this.setState({
      toolbarPlugins: toolbarPlugins,
      compositeDecorator: compositeDecorator
    });
    this.setEditorState(draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].set(this.state.editorState, {
      decorator: compositeDecorator
    }), false, false);
  };

  EditorCore.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
    if (this.forceUpdateImmediate) {
      this.cancelForceUpdateImmediate();
    }

    if (this.controlledMode) {
      var decorators = nextProps.value.getDecorator();
      var editorState = decorators ? nextProps.value : draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].set(nextProps.value, {
        decorator: this.state.compositeDecorator
      });
      this.setState({
        editorState: editorState
      });
    }
  };

  EditorCore.prototype.componentWillUnmount = function componentWillUnmount() {
    this.cancelForceUpdateImmediate();
  }; // 处理 value


  EditorCore.prototype.generatorDefaultValue = function generatorDefaultValue(editorState) {
    var defaultValue = this.getDefaultValue();

    if (defaultValue) {
      return defaultValue;
    }

    return editorState;
  };

  EditorCore.prototype.getStyleMap = function getStyleMap() {
    return configStore.get('customStyleMap');
  };

  EditorCore.prototype.setStyleMap = function setStyleMap(customStyleMap) {
    configStore.set('customStyleMap', customStyleMap);
    this.render();
  };

  EditorCore.prototype.initPlugins = function initPlugins() {
    var _this3 = this;

    var enableCallbacks = ['focus', 'getEditorState', 'setEditorState', 'getStyleMap', 'setStyleMap'];
    return this.getPlugins().map(function (plugin) {
      enableCallbacks.forEach(function (callbackName) {
        if (plugin.callbacks.hasOwnProperty(callbackName)) {
          plugin.callbacks[callbackName] = _this3[callbackName].bind(_this3);
        }
      });
      return plugin;
    });
  };

  EditorCore.prototype.focusEditor = function focusEditor(ev) {
    this.refs.editor.focus(ev);

    if (this.props.readOnly) {
      this._focusDummy.focus();
    }

    if (this.props.onFocus) {
      this.props.onFocus(ev);
    }
  };

  EditorCore.prototype._focus = function _focus(ev) {
    if (!ev || !ev.nativeEvent || !ev.nativeEvent.target) {
      return;
    }

    if (document.activeElement && document.activeElement.getAttribute('contenteditable') === 'true') {
      return;
    }

    return this.focus(ev);
  };

  EditorCore.prototype.focus = function focus(ev) {
    var _this4 = this;

    var event = ev && ev.nativeEvent;

    if (event && event.target === this._editorWrapper) {
      var editorState = this.state.editorState;
      var selection = editorState.getSelection();

      if (!selection.getHasFocus()) {
        if (selection.isCollapsed()) {
          return this.setState({
            editorState: draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"].moveSelectionToEnd(editorState)
          }, function () {
            _this4.focusEditor(ev);
          });
        }
      }
    }

    this.focusEditor(ev);
  };

  EditorCore.prototype.getPlugins = function getPlugins() {
    return this.state.plugins.slice();
  };

  EditorCore.prototype.getEventHandler = function getEventHandler() {
    var _this5 = this;

    var enabledEvents = ['onUpArrow', 'onDownArrow', 'handleReturn', 'onFocus', 'onBlur', 'onTab', 'handlePastedText'];
    var eventHandler = {};
    enabledEvents.forEach(function (event) {
      eventHandler[event] = _this5.generatorEventHandler(event);
    });
    return eventHandler;
  };

  EditorCore.prototype.getEditorState = function getEditorState() {
    var needFocus = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

    if (needFocus) {
      this.refs.editor.focus();
    }

    return this.state.editorState;
  };

  EditorCore.prototype.setEditorState = function setEditorState(editorState) {
    var _this6 = this;

    var focusEditor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var triggerChange = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
    var newEditorState = editorState;
    this.getPlugins().forEach(function (plugin) {
      if (plugin.onChange) {
        var updatedEditorState = plugin.onChange(newEditorState);

        if (updatedEditorState) {
          newEditorState = updatedEditorState;
        }
      }
    });

    if (this.props.onChange && triggerChange) {
      this.props.onChange(newEditorState); // close this issue https://github.com/ant-design/ant-design/issues/5788
      // when onChange not take any effect
      // `<Editor />` won't rerender cause no props is changed.
      // add an timeout here,
      // if props.onChange not trigger componentWillReceiveProps,
      // we will force render Editor with previous editorState,

      if (this.controlledMode) {
        this.forceUpdateImmediate = setImmediate(function () {
          return _this6.setState({
            editorState: new draft_js__WEBPACK_IMPORTED_MODULE_2__["EditorState"](_this6.state.editorState.getImmutable())
          });
        });
      }
    }

    if (!this.controlledMode) {
      this.setState({
        editorState: newEditorState
      }, focusEditor ? function () {
        return setImmediate(function () {
          return _this6.refs.editor.focus();
        });
      } : noop);
    }
  };

  EditorCore.prototype.handleKeyBinding = function handleKeyBinding(ev) {
    if (this.props.onKeyDown) {
      ev.ctrlKey = hasCommandModifier(ev);
      var keyDownResult = this.props.onKeyDown(ev);

      if (keyDownResult) {
        return keyDownResult;
      }

      return Object(draft_js__WEBPACK_IMPORTED_MODULE_2__["getDefaultKeyBinding"])(ev);
    }

    return Object(draft_js__WEBPACK_IMPORTED_MODULE_2__["getDefaultKeyBinding"])(ev);
  };

  EditorCore.prototype.handleKeyCommand = function handleKeyCommand(command) {
    if (this.props.multiLines) {
      return this.eventHandle('handleKeyBinding', command);
    }

    return command === 'split-block' ? 'handled' : 'not-handled';
  };

  EditorCore.prototype.getBlockStyle = function getBlockStyle(contentBlock) {
    var customBlockStyleMap = configStore.get('customBlockStyleMap');
    var type = contentBlock.getType();

    if (customBlockStyleMap.hasOwnProperty(type)) {
      return customBlockStyleMap[type];
    }

    return '';
  };

  EditorCore.prototype.blockRendererFn = function blockRendererFn(contentBlock) {
    var blockRenderResult = null;
    this.getPlugins().forEach(function (plugin) {
      if (plugin.blockRendererFn) {
        var result = plugin.blockRendererFn(contentBlock);

        if (result) {
          blockRenderResult = result;
        }
      }
    });
    return blockRenderResult;
  };

  EditorCore.prototype.eventHandle = function eventHandle(eventName) {
    var _props2;

    var plugins = this.getPlugins();

    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key3 = 1; _key3 < _len; _key3++) {
      args[_key3 - 1] = arguments[_key3];
    }

    for (var i = 0; i < plugins.length; i++) {
      var plugin = plugins[i];

      if (plugin.callbacks[eventName] && typeof plugin.callbacks[eventName] === 'function') {
        var _plugin$callbacks;

        var result = (_plugin$callbacks = plugin.callbacks)[eventName].apply(_plugin$callbacks, args);

        if (result === true) {
          return 'handled';
        }
      }
    }

    return this.props.hasOwnProperty(eventName) && (_props2 = this.props)[eventName].apply(_props2, args) === true ? 'handled' : 'not-handled';
  };

  EditorCore.prototype.generatorEventHandler = function generatorEventHandler(eventName) {
    var _this7 = this;

    return function () {
      for (var _len2 = arguments.length, args = Array(_len2), _key4 = 0; _key4 < _len2; _key4++) {
        args[_key4] = arguments[_key4];
      }

      return _this7.eventHandle.apply(_this7, [eventName].concat(args));
    };
  };

  EditorCore.prototype.customStyleFn = function customStyleFn(styleSet) {
    if (styleSet.size === 0) {
      return {};
    }

    var plugins = this.getPlugins();
    var resultStyle = {};

    for (var i = 0; i < plugins.length; i++) {
      if (plugins[i].customStyleFn) {
        var styled = plugins[i].customStyleFn(styleSet);

        if (styled) {
          _extends(resultStyle, styled);
        }
      }
    }

    return resultStyle;
  };

  EditorCore.prototype.render = function render() {
    var _classnames,
        _this8 = this;

    var _props3 = this.props,
        prefixCls = _props3.prefixCls,
        toolbars = _props3.toolbars,
        style = _props3.style,
        readOnly = _props3.readOnly,
        multiLines = _props3.multiLines;
    var _state = this.state,
        editorState = _state.editorState,
        toolbarPlugins = _state.toolbarPlugins;
    var customStyleMap = configStore.get('customStyleMap');
    var blockRenderMap = configStore.get('blockRenderMap');
    var eventHandler = this.getEventHandler();
    var Toolbar = toolbar.component;
    var cls = classnames__WEBPACK_IMPORTED_MODULE_5___default()((_classnames = {}, _classnames[prefixCls + '-editor'] = true, _classnames.readonly = readOnly, _classnames.oneline = !multiLines, _classnames));
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
      style: style,
      className: cls,
      onClick: this._focus.bind(this)
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Toolbar, {
      editorState: editorState,
      prefixCls: prefixCls,
      className: prefixCls + '-toolbar',
      plugins: toolbarPlugins,
      toolbars: toolbars
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
      className: prefixCls + '-editor-wrapper',
      ref: function ref(ele) {
        return _this8._editorWrapper = ele;
      },
      style: style,
      onClick: function onClick(ev) {
        return ev.preventDefault();
      }
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(draft_js__WEBPACK_IMPORTED_MODULE_2__["Editor"], _extends({}, this.props, eventHandler, {
      ref: 'editor',
      customStyleMap: customStyleMap,
      customStyleFn: this.customStyleFn.bind(this),
      editorState: editorState,
      handleKeyCommand: this.handleKeyCommand.bind(this),
      keyBindingFn: this.handleKeyBinding.bind(this),
      onChange: this.setEditorState.bind(this),
      blockStyleFn: this.getBlockStyle.bind(this),
      blockRenderMap: blockRenderMap,
      handlePastedText: this.handlePastedText,
      blockRendererFn: this.blockRendererFn.bind(this)
    })), readOnly ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('input', {
      style: focusDummyStyle,
      ref: function ref(ele) {
        return _this8._focusDummy = ele;
      },
      onBlur: eventHandler.onBlur
    }) : null, this.props.children));
  };

  return EditorCore;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

EditorCore.GetText = _export_exportText__WEBPACK_IMPORTED_MODULE_9__["default"];
EditorCore.GetHTML = Object(_export_getHTML__WEBPACK_IMPORTED_MODULE_8__["default"])(configStore);
EditorCore.defaultProps = {
  multiLines: true,
  plugins: [],
  prefixCls: 'rc-editor-core',
  pluginConfig: {},
  toolbars: [],
  spilitLine: 'enter'
};
EditorCore.childContextTypes = {
  getEditorState: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  setEditorState: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
};
/* harmony default export */ __webpack_exports__["default"] = (EditorCore);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../timers-browserify/main.js */ "./node_modules/timers-browserify/main.js").clearImmediate, __webpack_require__(/*! ./../../../timers-browserify/main.js */ "./node_modules/timers-browserify/main.js").setImmediate))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLWNvcmUvZXMvRWRpdG9yQ29yZS9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWVkaXRvci1jb3JlL2VzL0VkaXRvckNvcmUvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG4vKiB0c2xpbnQ6ZGlzYWJsZTptZW1iZXItb3JkZXJpbmcgaW50ZXJmYWNlLW5hbWUgKi9cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgRWRpdG9yLCBFZGl0b3JTdGF0ZSwgQ29udGVudFN0YXRlLCBDb21wb3NpdGVEZWNvcmF0b3IsIE1vZGlmaWVyLCBnZXREZWZhdWx0S2V5QmluZGluZywgS2V5QmluZGluZ1V0aWwsIERlZmF1bHREcmFmdEJsb2NrUmVuZGVyTWFwIH0gZnJvbSAnZHJhZnQtanMnO1xuaW1wb3J0IHsgTGlzdCwgTWFwIH0gZnJvbSAnaW1tdXRhYmxlJztcbmltcG9ydCAnc2V0aW1tZWRpYXRlJztcbmltcG9ydCBjbGFzc25hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHsgY3JlYXRlVG9vbGJhciB9IGZyb20gJy4uL1Rvb2xiYXInO1xuaW1wb3J0IENvbmZpZ1N0b3JlIGZyb20gJy4vQ29uZmlnU3RvcmUnO1xuaW1wb3J0IEdldEhUTUwgZnJvbSAnLi9leHBvcnQvZ2V0SFRNTCc7XG5pbXBvcnQgZXhwb3J0VGV4dCwgeyBkZWNvZGVDb250ZW50IH0gZnJvbSAnLi9leHBvcnQvZXhwb3J0VGV4dCc7XG5pbXBvcnQgY3VzdG9tSFRNTDJDb250ZW50IGZyb20gJy4vY3VzdG9tSFRNTDJDb250ZW50JztcbnZhciBoYXNDb21tYW5kTW9kaWZpZXIgPSBLZXlCaW5kaW5nVXRpbC5oYXNDb21tYW5kTW9kaWZpZXI7XG5cbmZ1bmN0aW9uIG5vb3AoKSB7fVxuO1xudmFyIGRlZmF1bHRQbHVnaW5Db25maWcgPSB7fTtcbnZhciBmb2N1c0R1bW15U3R5bGUgPSB7XG4gICAgd2lkdGg6IDAsXG4gICAgb3BhY2l0eTogMCxcbiAgICBib3JkZXI6IDAsXG4gICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gICAgbGVmdDogMCxcbiAgICB0b3A6IDBcbn07XG52YXIgdG9vbGJhciA9IGNyZWF0ZVRvb2xiYXIoKTtcbnZhciBjb25maWdTdG9yZSA9IG5ldyBDb25maWdTdG9yZSgpO1xuXG52YXIgRWRpdG9yQ29yZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICAgX2luaGVyaXRzKEVkaXRvckNvcmUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gICAgZnVuY3Rpb24gRWRpdG9yQ29yZShwcm9wcykge1xuICAgICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgRWRpdG9yQ29yZSk7XG5cbiAgICAgICAgdmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsKHRoaXMsIHByb3BzKSk7XG5cbiAgICAgICAgX3RoaXMuY2FuY2VsRm9yY2VVcGRhdGVJbW1lZGlhdGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBjbGVhckltbWVkaWF0ZShfdGhpcy5mb3JjZVVwZGF0ZUltbWVkaWF0ZSk7XG4gICAgICAgICAgICBfdGhpcy5mb3JjZVVwZGF0ZUltbWVkaWF0ZSA9IG51bGw7XG4gICAgICAgIH07XG4gICAgICAgIF90aGlzLmhhbmRsZVBhc3RlZFRleHQgPSBmdW5jdGlvbiAodGV4dCwgaHRtbCkge1xuICAgICAgICAgICAgdmFyIGVkaXRvclN0YXRlID0gX3RoaXMuc3RhdGUuZWRpdG9yU3RhdGU7XG5cbiAgICAgICAgICAgIGlmIChodG1sKSB7XG4gICAgICAgICAgICAgICAgdmFyIGNvbnRlbnRTdGF0ZSA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgICAgICAgICAgICAgdmFyIHNlbGVjdGlvbiA9IGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpO1xuICAgICAgICAgICAgICAgIHZhciBmcmFnbWVudCA9IGN1c3RvbUhUTUwyQ29udGVudChodG1sLCBjb250ZW50U3RhdGUpO1xuICAgICAgICAgICAgICAgIHZhciBwYXN0ZWRDb250ZW50ID0gTW9kaWZpZXIucmVwbGFjZVdpdGhGcmFnbWVudChjb250ZW50U3RhdGUsIHNlbGVjdGlvbiwgZnJhZ21lbnQpO1xuICAgICAgICAgICAgICAgIHZhciBuZXdDb250ZW50ID0gcGFzdGVkQ29udGVudC5tZXJnZSh7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGlvbkJlZm9yZTogc2VsZWN0aW9uLFxuICAgICAgICAgICAgICAgICAgICBzZWxlY3Rpb25BZnRlcjogcGFzdGVkQ29udGVudC5nZXRTZWxlY3Rpb25BZnRlcigpLnNldCgnaGFzRm9jdXMnLCB0cnVlKVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIF90aGlzLnNldEVkaXRvclN0YXRlKEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIG5ld0NvbnRlbnQsICdpbnNlcnQtZnJhZ21lbnQnKSwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuICdoYW5kbGVkJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAnbm90LWhhbmRsZWQnO1xuICAgICAgICB9O1xuICAgICAgICBfdGhpcy5wbHVnaW5zID0gTGlzdChMaXN0KHByb3BzLnBsdWdpbnMpLmZsYXR0ZW4odHJ1ZSkpO1xuICAgICAgICB2YXIgZWRpdG9yU3RhdGUgPSB2b2lkIDA7XG4gICAgICAgIGlmIChwcm9wcy52YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBpZiAocHJvcHMudmFsdWUgaW5zdGFuY2VvZiBFZGl0b3JTdGF0ZSkge1xuICAgICAgICAgICAgICAgIGVkaXRvclN0YXRlID0gcHJvcHMudmFsdWUgfHwgRWRpdG9yU3RhdGUuY3JlYXRlRW1wdHkoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgZWRpdG9yU3RhdGUgPSBFZGl0b3JTdGF0ZS5jcmVhdGVFbXB0eSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgZWRpdG9yU3RhdGUgPSBFZGl0b3JTdGF0ZS5jcmVhdGVFbXB0eSgpO1xuICAgICAgICB9XG4gICAgICAgIGVkaXRvclN0YXRlID0gX3RoaXMuZ2VuZXJhdG9yRGVmYXVsdFZhbHVlKGVkaXRvclN0YXRlKTtcbiAgICAgICAgX3RoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICBwbHVnaW5zOiBfdGhpcy5yZWxvYWRQbHVnaW5zKCksXG4gICAgICAgICAgICBlZGl0b3JTdGF0ZTogZWRpdG9yU3RhdGUsXG4gICAgICAgICAgICBjdXN0b21TdHlsZU1hcDoge30sXG4gICAgICAgICAgICBjdXN0b21CbG9ja1N0eWxlTWFwOiB7fSxcbiAgICAgICAgICAgIGNvbXBvc2l0ZURlY29yYXRvcjogbnVsbFxuICAgICAgICB9O1xuICAgICAgICBpZiAocHJvcHMudmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgX3RoaXMuY29udHJvbGxlZE1vZGUgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBfdGhpcztcbiAgICB9XG5cbiAgICBFZGl0b3JDb3JlLlRvRWRpdG9yU3RhdGUgPSBmdW5jdGlvbiBUb0VkaXRvclN0YXRlKHRleHQpIHtcbiAgICAgICAgdmFyIGNyZWF0ZUVtcHR5Q29udGVudFN0YXRlID0gQ29udGVudFN0YXRlLmNyZWF0ZUZyb21UZXh0KGRlY29kZUNvbnRlbnQodGV4dCkgfHwgJycpO1xuICAgICAgICB2YXIgZWRpdG9yU3RhdGUgPSBFZGl0b3JTdGF0ZS5jcmVhdGVXaXRoQ29udGVudChjcmVhdGVFbXB0eUNvbnRlbnRTdGF0ZSk7XG4gICAgICAgIHJldHVybiBFZGl0b3JTdGF0ZS5mb3JjZVNlbGVjdGlvbihlZGl0b3JTdGF0ZSwgY3JlYXRlRW1wdHlDb250ZW50U3RhdGUuZ2V0U2VsZWN0aW9uQWZ0ZXIoKSk7XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLmdldERlZmF1bHRWYWx1ZSA9IGZ1bmN0aW9uIGdldERlZmF1bHRWYWx1ZSgpIHtcbiAgICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgICBkZWZhdWx0VmFsdWUgPSBfcHJvcHMuZGVmYXVsdFZhbHVlLFxuICAgICAgICAgICAgdmFsdWUgPSBfcHJvcHMudmFsdWU7XG5cbiAgICAgICAgcmV0dXJuIHZhbHVlIHx8IGRlZmF1bHRWYWx1ZTtcbiAgICB9O1xuXG4gICAgRWRpdG9yQ29yZS5wcm90b3R5cGUuUmVzZXQgPSBmdW5jdGlvbiBSZXNldCgpIHtcbiAgICAgICAgdmFyIGRlZmF1bHRWYWx1ZSA9IHRoaXMuZ2V0RGVmYXVsdFZhbHVlKCk7XG4gICAgICAgIHZhciBjb250ZW50U3RhdGUgPSBkZWZhdWx0VmFsdWUgPyBkZWZhdWx0VmFsdWUuZ2V0Q3VycmVudENvbnRlbnQoKSA6IENvbnRlbnRTdGF0ZS5jcmVhdGVGcm9tVGV4dCgnJyk7XG4gICAgICAgIHZhciB1cGRhdGVkRWRpdG9yU3RhdGUgPSBFZGl0b3JTdGF0ZS5wdXNoKHRoaXMuc3RhdGUuZWRpdG9yU3RhdGUsIGNvbnRlbnRTdGF0ZSwgJ3JlbW92ZS1yYW5nZScpO1xuICAgICAgICB0aGlzLnNldEVkaXRvclN0YXRlKEVkaXRvclN0YXRlLmZvcmNlU2VsZWN0aW9uKHVwZGF0ZWRFZGl0b3JTdGF0ZSwgY29udGVudFN0YXRlLmdldFNlbGVjdGlvbkJlZm9yZSgpKSk7XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLlNldFRleHQgPSBmdW5jdGlvbiBTZXRUZXh0KHRleHQpIHtcbiAgICAgICAgdmFyIGNyZWF0ZVRleHRDb250ZW50U3RhdGUgPSBDb250ZW50U3RhdGUuY3JlYXRlRnJvbVRleHQodGV4dCB8fCAnJyk7XG4gICAgICAgIHZhciBlZGl0b3JTdGF0ZSA9IEVkaXRvclN0YXRlLnB1c2godGhpcy5zdGF0ZS5lZGl0b3JTdGF0ZSwgY3JlYXRlVGV4dENvbnRlbnRTdGF0ZSwgJ2NoYW5nZS1ibG9jay1kYXRhJyk7XG4gICAgICAgIHRoaXMuc2V0RWRpdG9yU3RhdGUoRWRpdG9yU3RhdGUubW92ZUZvY3VzVG9FbmQoZWRpdG9yU3RhdGUpLCB0cnVlKTtcbiAgICB9O1xuXG4gICAgRWRpdG9yQ29yZS5wcm90b3R5cGUuZ2V0Q2hpbGRDb250ZXh0ID0gZnVuY3Rpb24gZ2V0Q2hpbGRDb250ZXh0KCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgZ2V0RWRpdG9yU3RhdGU6IHRoaXMuZ2V0RWRpdG9yU3RhdGUsXG4gICAgICAgICAgICBzZXRFZGl0b3JTdGF0ZTogdGhpcy5zZXRFZGl0b3JTdGF0ZVxuICAgICAgICB9O1xuICAgIH07XG5cbiAgICBFZGl0b3JDb3JlLnByb3RvdHlwZS5yZWxvYWRQbHVnaW5zID0gZnVuY3Rpb24gcmVsb2FkUGx1Z2lucygpIHtcbiAgICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMucGx1Z2lucyAmJiB0aGlzLnBsdWdpbnMuc2l6ZSA/IHRoaXMucGx1Z2lucy5tYXAoZnVuY3Rpb24gKHBsdWdpbikge1xuICAgICAgICAgICAgLy8g5aaC5p6c5o+S5Lu25pyJIGNhbGxiYWNrcyDmlrnms5Us5YiZ6K6k5Li65o+S5Lu25bey57uP5Yqg6L2944CCXG4gICAgICAgICAgICBpZiAocGx1Z2luLmNhbGxiYWNrcykge1xuICAgICAgICAgICAgICAgIHJldHVybiBwbHVnaW47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyDlpoLmnpzmj5Lku7bmnIkgY29uc3RydWN0b3Ig5pa55rOVLOWImeaehOmAoOaPkuS7tlxuICAgICAgICAgICAgaWYgKHBsdWdpbi5oYXNPd25Qcm9wZXJ0eSgnY29uc3RydWN0b3InKSkge1xuICAgICAgICAgICAgICAgIHZhciBwbHVnaW5Db25maWcgPSBfZXh0ZW5kcyhfdGhpczIucHJvcHMucGx1Z2luQ29uZmlnLCBwbHVnaW4uY29uZmlnIHx8IHt9LCBkZWZhdWx0UGx1Z2luQ29uZmlnKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gcGx1Z2luLmNvbnN0cnVjdG9yKHBsdWdpbkNvbmZpZyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBlbHNlIOaXoOaViOaPkuS7tlxuICAgICAgICAgICAgY29uc29sZS53YXJuKCc+PiDmj5Lku7Y6IFsnLCBwbHVnaW4ubmFtZSwgJ10g5peg5pWI44CC5o+S5Lu25oiW6K645bey57uP6L+H5pyf44CCJyk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH0pLmZpbHRlcihmdW5jdGlvbiAocGx1Z2luKSB7XG4gICAgICAgICAgICByZXR1cm4gcGx1Z2luO1xuICAgICAgICB9KS50b0FycmF5KCkgOiBbXTtcbiAgICB9O1xuXG4gICAgRWRpdG9yQ29yZS5wcm90b3R5cGUuY29tcG9uZW50V2lsbE1vdW50ID0gZnVuY3Rpb24gY29tcG9uZW50V2lsbE1vdW50KCkge1xuICAgICAgICB2YXIgcGx1Z2lucyA9IHRoaXMuaW5pdFBsdWdpbnMoKS5jb25jYXQoW3Rvb2xiYXJdKTtcbiAgICAgICAgdmFyIGN1c3RvbVN0eWxlTWFwID0ge307XG4gICAgICAgIHZhciBjdXN0b21CbG9ja1N0eWxlTWFwID0ge307XG4gICAgICAgIHZhciBjdXN0b21CbG9ja1JlbmRlck1hcCA9IE1hcChEZWZhdWx0RHJhZnRCbG9ja1JlbmRlck1hcCk7XG4gICAgICAgIHZhciB0b0hUTUxMaXN0ID0gTGlzdChbXSk7XG4gICAgICAgIC8vIGluaXRpYWxpemUgY29tcG9zaXRlRGVjb3JhdG9yXG4gICAgICAgIHZhciBjb21wb3NpdGVEZWNvcmF0b3IgPSBuZXcgQ29tcG9zaXRlRGVjb3JhdG9yKHBsdWdpbnMuZmlsdGVyKGZ1bmN0aW9uIChwbHVnaW4pIHtcbiAgICAgICAgICAgIHJldHVybiBwbHVnaW4uZGVjb3JhdG9ycyAhPT0gdW5kZWZpbmVkO1xuICAgICAgICB9KS5tYXAoZnVuY3Rpb24gKHBsdWdpbikge1xuICAgICAgICAgICAgcmV0dXJuIHBsdWdpbi5kZWNvcmF0b3JzO1xuICAgICAgICB9KS5yZWR1Y2UoZnVuY3Rpb24gKHByZXYsIGN1cnIpIHtcbiAgICAgICAgICAgIHJldHVybiBwcmV2LmNvbmNhdChjdXJyKTtcbiAgICAgICAgfSwgW10pKTtcbiAgICAgICAgLy8gaW5pdGlhbGl6ZSBUb29sYmFyXG4gICAgICAgIHZhciB0b29sYmFyUGx1Z2lucyA9IExpc3QocGx1Z2lucy5maWx0ZXIoZnVuY3Rpb24gKHBsdWdpbikge1xuICAgICAgICAgICAgcmV0dXJuICEhcGx1Z2luLmNvbXBvbmVudCAmJiBwbHVnaW4ubmFtZSAhPT0gJ3Rvb2xiYXInO1xuICAgICAgICB9KSk7XG4gICAgICAgIC8vIGxvYWQgaW5saW5lIHN0eWxlcy4uLlxuICAgICAgICBwbHVnaW5zLmZvckVhY2goZnVuY3Rpb24gKHBsdWdpbikge1xuICAgICAgICAgICAgdmFyIHN0eWxlTWFwID0gcGx1Z2luLnN0eWxlTWFwLFxuICAgICAgICAgICAgICAgIGJsb2NrU3R5bGVNYXAgPSBwbHVnaW4uYmxvY2tTdHlsZU1hcCxcbiAgICAgICAgICAgICAgICBibG9ja1JlbmRlck1hcCA9IHBsdWdpbi5ibG9ja1JlbmRlck1hcCxcbiAgICAgICAgICAgICAgICB0b0h0bWwgPSBwbHVnaW4udG9IdG1sO1xuXG4gICAgICAgICAgICBpZiAoc3R5bGVNYXApIHtcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBrZXkgaW4gc3R5bGVNYXApIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHN0eWxlTWFwLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1c3RvbVN0eWxlTWFwW2tleV0gPSBzdHlsZU1hcFtrZXldO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGJsb2NrU3R5bGVNYXApIHtcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBfa2V5IGluIGJsb2NrU3R5bGVNYXApIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGJsb2NrU3R5bGVNYXAuaGFzT3duUHJvcGVydHkoX2tleSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1c3RvbUJsb2NrU3R5bGVNYXBbX2tleV0gPSBibG9ja1N0eWxlTWFwW19rZXldO1xuICAgICAgICAgICAgICAgICAgICAgICAgY3VzdG9tQmxvY2tSZW5kZXJNYXAgPSBjdXN0b21CbG9ja1JlbmRlck1hcC5zZXQoX2tleSwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQ6IG51bGxcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRvSHRtbCkge1xuICAgICAgICAgICAgICAgIHRvSFRNTExpc3QgPSB0b0hUTUxMaXN0LnB1c2godG9IdG1sKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChibG9ja1JlbmRlck1hcCkge1xuICAgICAgICAgICAgICAgIGZvciAodmFyIF9rZXkyIGluIGJsb2NrUmVuZGVyTWFwKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChibG9ja1JlbmRlck1hcC5oYXNPd25Qcm9wZXJ0eShfa2V5MikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1c3RvbUJsb2NrUmVuZGVyTWFwID0gY3VzdG9tQmxvY2tSZW5kZXJNYXAuc2V0KF9rZXkyLCBibG9ja1JlbmRlck1hcFtfa2V5Ml0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgY29uZmlnU3RvcmUuc2V0KCdjdXN0b21TdHlsZU1hcCcsIGN1c3RvbVN0eWxlTWFwKTtcbiAgICAgICAgY29uZmlnU3RvcmUuc2V0KCdjdXN0b21CbG9ja1N0eWxlTWFwJywgY3VzdG9tQmxvY2tTdHlsZU1hcCk7XG4gICAgICAgIGNvbmZpZ1N0b3JlLnNldCgnYmxvY2tSZW5kZXJNYXAnLCBjdXN0b21CbG9ja1JlbmRlck1hcCk7XG4gICAgICAgIGNvbmZpZ1N0b3JlLnNldCgnY3VzdG9tU3R5bGVGbicsIHRoaXMuY3VzdG9tU3R5bGVGbi5iaW5kKHRoaXMpKTtcbiAgICAgICAgY29uZmlnU3RvcmUuc2V0KCd0b0hUTUxMaXN0JywgdG9IVE1MTGlzdCk7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgdG9vbGJhclBsdWdpbnM6IHRvb2xiYXJQbHVnaW5zLFxuICAgICAgICAgICAgY29tcG9zaXRlRGVjb3JhdG9yOiBjb21wb3NpdGVEZWNvcmF0b3JcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuc2V0RWRpdG9yU3RhdGUoRWRpdG9yU3RhdGUuc2V0KHRoaXMuc3RhdGUuZWRpdG9yU3RhdGUsIHsgZGVjb3JhdG9yOiBjb21wb3NpdGVEZWNvcmF0b3IgfSksIGZhbHNlLCBmYWxzZSk7XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLmNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMgPSBmdW5jdGlvbiBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5leHRQcm9wcykge1xuICAgICAgICBpZiAodGhpcy5mb3JjZVVwZGF0ZUltbWVkaWF0ZSkge1xuICAgICAgICAgICAgdGhpcy5jYW5jZWxGb3JjZVVwZGF0ZUltbWVkaWF0ZSgpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmNvbnRyb2xsZWRNb2RlKSB7XG4gICAgICAgICAgICB2YXIgZGVjb3JhdG9ycyA9IG5leHRQcm9wcy52YWx1ZS5nZXREZWNvcmF0b3IoKTtcbiAgICAgICAgICAgIHZhciBlZGl0b3JTdGF0ZSA9IGRlY29yYXRvcnMgPyBuZXh0UHJvcHMudmFsdWUgOiBFZGl0b3JTdGF0ZS5zZXQobmV4dFByb3BzLnZhbHVlLCB7IGRlY29yYXRvcjogdGhpcy5zdGF0ZS5jb21wb3NpdGVEZWNvcmF0b3IgfSk7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBlZGl0b3JTdGF0ZTogZWRpdG9yU3RhdGVcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLmNvbXBvbmVudFdpbGxVbm1vdW50ID0gZnVuY3Rpb24gY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICAgIHRoaXMuY2FuY2VsRm9yY2VVcGRhdGVJbW1lZGlhdGUoKTtcbiAgICB9O1xuICAgIC8vIOWkhOeQhiB2YWx1ZVxuXG5cbiAgICBFZGl0b3JDb3JlLnByb3RvdHlwZS5nZW5lcmF0b3JEZWZhdWx0VmFsdWUgPSBmdW5jdGlvbiBnZW5lcmF0b3JEZWZhdWx0VmFsdWUoZWRpdG9yU3RhdGUpIHtcbiAgICAgICAgdmFyIGRlZmF1bHRWYWx1ZSA9IHRoaXMuZ2V0RGVmYXVsdFZhbHVlKCk7XG4gICAgICAgIGlmIChkZWZhdWx0VmFsdWUpIHtcbiAgICAgICAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGVkaXRvclN0YXRlO1xuICAgIH07XG5cbiAgICBFZGl0b3JDb3JlLnByb3RvdHlwZS5nZXRTdHlsZU1hcCA9IGZ1bmN0aW9uIGdldFN0eWxlTWFwKCkge1xuICAgICAgICByZXR1cm4gY29uZmlnU3RvcmUuZ2V0KCdjdXN0b21TdHlsZU1hcCcpO1xuICAgIH07XG5cbiAgICBFZGl0b3JDb3JlLnByb3RvdHlwZS5zZXRTdHlsZU1hcCA9IGZ1bmN0aW9uIHNldFN0eWxlTWFwKGN1c3RvbVN0eWxlTWFwKSB7XG4gICAgICAgIGNvbmZpZ1N0b3JlLnNldCgnY3VzdG9tU3R5bGVNYXAnLCBjdXN0b21TdHlsZU1hcCk7XG4gICAgICAgIHRoaXMucmVuZGVyKCk7XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLmluaXRQbHVnaW5zID0gZnVuY3Rpb24gaW5pdFBsdWdpbnMoKSB7XG4gICAgICAgIHZhciBfdGhpczMgPSB0aGlzO1xuXG4gICAgICAgIHZhciBlbmFibGVDYWxsYmFja3MgPSBbJ2ZvY3VzJywgJ2dldEVkaXRvclN0YXRlJywgJ3NldEVkaXRvclN0YXRlJywgJ2dldFN0eWxlTWFwJywgJ3NldFN0eWxlTWFwJ107XG4gICAgICAgIHJldHVybiB0aGlzLmdldFBsdWdpbnMoKS5tYXAoZnVuY3Rpb24gKHBsdWdpbikge1xuICAgICAgICAgICAgZW5hYmxlQ2FsbGJhY2tzLmZvckVhY2goZnVuY3Rpb24gKGNhbGxiYWNrTmFtZSkge1xuICAgICAgICAgICAgICAgIGlmIChwbHVnaW4uY2FsbGJhY2tzLmhhc093blByb3BlcnR5KGNhbGxiYWNrTmFtZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgcGx1Z2luLmNhbGxiYWNrc1tjYWxsYmFja05hbWVdID0gX3RoaXMzW2NhbGxiYWNrTmFtZV0uYmluZChfdGhpczMpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuIHBsdWdpbjtcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLmZvY3VzRWRpdG9yID0gZnVuY3Rpb24gZm9jdXNFZGl0b3IoZXYpIHtcbiAgICAgICAgdGhpcy5yZWZzLmVkaXRvci5mb2N1cyhldik7XG4gICAgICAgIGlmICh0aGlzLnByb3BzLnJlYWRPbmx5KSB7XG4gICAgICAgICAgICB0aGlzLl9mb2N1c0R1bW15LmZvY3VzKCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25Gb2N1cykge1xuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZvY3VzKGV2KTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBFZGl0b3JDb3JlLnByb3RvdHlwZS5fZm9jdXMgPSBmdW5jdGlvbiBfZm9jdXMoZXYpIHtcbiAgICAgICAgaWYgKCFldiB8fCAhZXYubmF0aXZlRXZlbnQgfHwgIWV2Lm5hdGl2ZUV2ZW50LnRhcmdldCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmIChkb2N1bWVudC5hY3RpdmVFbGVtZW50ICYmIGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQuZ2V0QXR0cmlidXRlKCdjb250ZW50ZWRpdGFibGUnKSA9PT0gJ3RydWUnKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuZm9jdXMoZXYpO1xuICAgIH07XG5cbiAgICBFZGl0b3JDb3JlLnByb3RvdHlwZS5mb2N1cyA9IGZ1bmN0aW9uIGZvY3VzKGV2KSB7XG4gICAgICAgIHZhciBfdGhpczQgPSB0aGlzO1xuXG4gICAgICAgIHZhciBldmVudCA9IGV2ICYmIGV2Lm5hdGl2ZUV2ZW50O1xuICAgICAgICBpZiAoZXZlbnQgJiYgZXZlbnQudGFyZ2V0ID09PSB0aGlzLl9lZGl0b3JXcmFwcGVyKSB7XG4gICAgICAgICAgICB2YXIgZWRpdG9yU3RhdGUgPSB0aGlzLnN0YXRlLmVkaXRvclN0YXRlO1xuXG4gICAgICAgICAgICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gICAgICAgICAgICBpZiAoIXNlbGVjdGlvbi5nZXRIYXNGb2N1cygpKSB7XG4gICAgICAgICAgICAgICAgaWYgKHNlbGVjdGlvbi5pc0NvbGxhcHNlZCgpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVkaXRvclN0YXRlOiBFZGl0b3JTdGF0ZS5tb3ZlU2VsZWN0aW9uVG9FbmQoZWRpdG9yU3RhdGUpXG4gICAgICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzNC5mb2N1c0VkaXRvcihldik7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLmZvY3VzRWRpdG9yKGV2KTtcbiAgICB9O1xuXG4gICAgRWRpdG9yQ29yZS5wcm90b3R5cGUuZ2V0UGx1Z2lucyA9IGZ1bmN0aW9uIGdldFBsdWdpbnMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0YXRlLnBsdWdpbnMuc2xpY2UoKTtcbiAgICB9O1xuXG4gICAgRWRpdG9yQ29yZS5wcm90b3R5cGUuZ2V0RXZlbnRIYW5kbGVyID0gZnVuY3Rpb24gZ2V0RXZlbnRIYW5kbGVyKCkge1xuICAgICAgICB2YXIgX3RoaXM1ID0gdGhpcztcblxuICAgICAgICB2YXIgZW5hYmxlZEV2ZW50cyA9IFsnb25VcEFycm93JywgJ29uRG93bkFycm93JywgJ2hhbmRsZVJldHVybicsICdvbkZvY3VzJywgJ29uQmx1cicsICdvblRhYicsICdoYW5kbGVQYXN0ZWRUZXh0J107XG4gICAgICAgIHZhciBldmVudEhhbmRsZXIgPSB7fTtcbiAgICAgICAgZW5hYmxlZEV2ZW50cy5mb3JFYWNoKGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnRIYW5kbGVyW2V2ZW50XSA9IF90aGlzNS5nZW5lcmF0b3JFdmVudEhhbmRsZXIoZXZlbnQpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIGV2ZW50SGFuZGxlcjtcbiAgICB9O1xuXG4gICAgRWRpdG9yQ29yZS5wcm90b3R5cGUuZ2V0RWRpdG9yU3RhdGUgPSBmdW5jdGlvbiBnZXRFZGl0b3JTdGF0ZSgpIHtcbiAgICAgICAgdmFyIG5lZWRGb2N1cyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogZmFsc2U7XG5cbiAgICAgICAgaWYgKG5lZWRGb2N1cykge1xuICAgICAgICAgICAgdGhpcy5yZWZzLmVkaXRvci5mb2N1cygpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLnN0YXRlLmVkaXRvclN0YXRlO1xuICAgIH07XG5cbiAgICBFZGl0b3JDb3JlLnByb3RvdHlwZS5zZXRFZGl0b3JTdGF0ZSA9IGZ1bmN0aW9uIHNldEVkaXRvclN0YXRlKGVkaXRvclN0YXRlKSB7XG4gICAgICAgIHZhciBfdGhpczYgPSB0aGlzO1xuXG4gICAgICAgIHZhciBmb2N1c0VkaXRvciA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogZmFsc2U7XG4gICAgICAgIHZhciB0cmlnZ2VyQ2hhbmdlID0gYXJndW1lbnRzLmxlbmd0aCA+IDIgJiYgYXJndW1lbnRzWzJdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMl0gOiB0cnVlO1xuXG4gICAgICAgIHZhciBuZXdFZGl0b3JTdGF0ZSA9IGVkaXRvclN0YXRlO1xuICAgICAgICB0aGlzLmdldFBsdWdpbnMoKS5mb3JFYWNoKGZ1bmN0aW9uIChwbHVnaW4pIHtcbiAgICAgICAgICAgIGlmIChwbHVnaW4ub25DaGFuZ2UpIHtcbiAgICAgICAgICAgICAgICB2YXIgdXBkYXRlZEVkaXRvclN0YXRlID0gcGx1Z2luLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgICAgICAgICAgICAgICBpZiAodXBkYXRlZEVkaXRvclN0YXRlKSB7XG4gICAgICAgICAgICAgICAgICAgIG5ld0VkaXRvclN0YXRlID0gdXBkYXRlZEVkaXRvclN0YXRlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uQ2hhbmdlICYmIHRyaWdnZXJDaGFuZ2UpIHtcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICAgICAgICAgICAgLy8gY2xvc2UgdGhpcyBpc3N1ZSBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy81Nzg4XG4gICAgICAgICAgICAvLyB3aGVuIG9uQ2hhbmdlIG5vdCB0YWtlIGFueSBlZmZlY3RcbiAgICAgICAgICAgIC8vIGA8RWRpdG9yIC8+YCB3b24ndCByZXJlbmRlciBjYXVzZSBubyBwcm9wcyBpcyBjaGFuZ2VkLlxuICAgICAgICAgICAgLy8gYWRkIGFuIHRpbWVvdXQgaGVyZSxcbiAgICAgICAgICAgIC8vIGlmIHByb3BzLm9uQ2hhbmdlIG5vdCB0cmlnZ2VyIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMsXG4gICAgICAgICAgICAvLyB3ZSB3aWxsIGZvcmNlIHJlbmRlciBFZGl0b3Igd2l0aCBwcmV2aW91cyBlZGl0b3JTdGF0ZSxcbiAgICAgICAgICAgIGlmICh0aGlzLmNvbnRyb2xsZWRNb2RlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZUltbWVkaWF0ZSA9IHNldEltbWVkaWF0ZShmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpczYuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgZWRpdG9yU3RhdGU6IG5ldyBFZGl0b3JTdGF0ZShfdGhpczYuc3RhdGUuZWRpdG9yU3RhdGUuZ2V0SW1tdXRhYmxlKCkpXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5jb250cm9sbGVkTW9kZSkge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGVkaXRvclN0YXRlOiBuZXdFZGl0b3JTdGF0ZSB9LCBmb2N1c0VkaXRvciA/IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gc2V0SW1tZWRpYXRlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzNi5yZWZzLmVkaXRvci5mb2N1cygpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSA6IG5vb3ApO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLmhhbmRsZUtleUJpbmRpbmcgPSBmdW5jdGlvbiBoYW5kbGVLZXlCaW5kaW5nKGV2KSB7XG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uS2V5RG93bikge1xuICAgICAgICAgICAgZXYuY3RybEtleSA9IGhhc0NvbW1hbmRNb2RpZmllcihldik7XG4gICAgICAgICAgICB2YXIga2V5RG93blJlc3VsdCA9IHRoaXMucHJvcHMub25LZXlEb3duKGV2KTtcbiAgICAgICAgICAgIGlmIChrZXlEb3duUmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGtleURvd25SZXN1bHQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZ2V0RGVmYXVsdEtleUJpbmRpbmcoZXYpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBnZXREZWZhdWx0S2V5QmluZGluZyhldik7XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLmhhbmRsZUtleUNvbW1hbmQgPSBmdW5jdGlvbiBoYW5kbGVLZXlDb21tYW5kKGNvbW1hbmQpIHtcbiAgICAgICAgaWYgKHRoaXMucHJvcHMubXVsdGlMaW5lcykge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZXZlbnRIYW5kbGUoJ2hhbmRsZUtleUJpbmRpbmcnLCBjb21tYW5kKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gY29tbWFuZCA9PT0gJ3NwbGl0LWJsb2NrJyA/ICdoYW5kbGVkJyA6ICdub3QtaGFuZGxlZCc7XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLmdldEJsb2NrU3R5bGUgPSBmdW5jdGlvbiBnZXRCbG9ja1N0eWxlKGNvbnRlbnRCbG9jaykge1xuICAgICAgICB2YXIgY3VzdG9tQmxvY2tTdHlsZU1hcCA9IGNvbmZpZ1N0b3JlLmdldCgnY3VzdG9tQmxvY2tTdHlsZU1hcCcpO1xuICAgICAgICB2YXIgdHlwZSA9IGNvbnRlbnRCbG9jay5nZXRUeXBlKCk7XG4gICAgICAgIGlmIChjdXN0b21CbG9ja1N0eWxlTWFwLmhhc093blByb3BlcnR5KHR5cGUpKSB7XG4gICAgICAgICAgICByZXR1cm4gY3VzdG9tQmxvY2tTdHlsZU1hcFt0eXBlXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gJyc7XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLmJsb2NrUmVuZGVyZXJGbiA9IGZ1bmN0aW9uIGJsb2NrUmVuZGVyZXJGbihjb250ZW50QmxvY2spIHtcbiAgICAgICAgdmFyIGJsb2NrUmVuZGVyUmVzdWx0ID0gbnVsbDtcbiAgICAgICAgdGhpcy5nZXRQbHVnaW5zKCkuZm9yRWFjaChmdW5jdGlvbiAocGx1Z2luKSB7XG4gICAgICAgICAgICBpZiAocGx1Z2luLmJsb2NrUmVuZGVyZXJGbikge1xuICAgICAgICAgICAgICAgIHZhciByZXN1bHQgPSBwbHVnaW4uYmxvY2tSZW5kZXJlckZuKGNvbnRlbnRCbG9jayk7XG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdCkge1xuICAgICAgICAgICAgICAgICAgICBibG9ja1JlbmRlclJlc3VsdCA9IHJlc3VsdDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gYmxvY2tSZW5kZXJSZXN1bHQ7XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLmV2ZW50SGFuZGxlID0gZnVuY3Rpb24gZXZlbnRIYW5kbGUoZXZlbnROYW1lKSB7XG4gICAgICAgIHZhciBfcHJvcHMyO1xuXG4gICAgICAgIHZhciBwbHVnaW5zID0gdGhpcy5nZXRQbHVnaW5zKCk7XG5cbiAgICAgICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuID4gMSA/IF9sZW4gLSAxIDogMCksIF9rZXkzID0gMTsgX2tleTMgPCBfbGVuOyBfa2V5MysrKSB7XG4gICAgICAgICAgICBhcmdzW19rZXkzIC0gMV0gPSBhcmd1bWVudHNbX2tleTNdO1xuICAgICAgICB9XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwbHVnaW5zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICB2YXIgcGx1Z2luID0gcGx1Z2luc1tpXTtcbiAgICAgICAgICAgIGlmIChwbHVnaW4uY2FsbGJhY2tzW2V2ZW50TmFtZV0gJiYgdHlwZW9mIHBsdWdpbi5jYWxsYmFja3NbZXZlbnROYW1lXSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIHZhciBfcGx1Z2luJGNhbGxiYWNrcztcblxuICAgICAgICAgICAgICAgIHZhciByZXN1bHQgPSAoX3BsdWdpbiRjYWxsYmFja3MgPSBwbHVnaW4uY2FsbGJhY2tzKVtldmVudE5hbWVdLmFwcGx5KF9wbHVnaW4kY2FsbGJhY2tzLCBhcmdzKTtcbiAgICAgICAgICAgICAgICBpZiAocmVzdWx0ID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnaGFuZGxlZCc7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLnByb3BzLmhhc093blByb3BlcnR5KGV2ZW50TmFtZSkgJiYgKF9wcm9wczIgPSB0aGlzLnByb3BzKVtldmVudE5hbWVdLmFwcGx5KF9wcm9wczIsIGFyZ3MpID09PSB0cnVlID8gJ2hhbmRsZWQnIDogJ25vdC1oYW5kbGVkJztcbiAgICB9O1xuXG4gICAgRWRpdG9yQ29yZS5wcm90b3R5cGUuZ2VuZXJhdG9yRXZlbnRIYW5kbGVyID0gZnVuY3Rpb24gZ2VuZXJhdG9yRXZlbnRIYW5kbGVyKGV2ZW50TmFtZSkge1xuICAgICAgICB2YXIgX3RoaXM3ID0gdGhpcztcblxuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgZm9yICh2YXIgX2xlbjIgPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbjIpLCBfa2V5NCA9IDA7IF9rZXk0IDwgX2xlbjI7IF9rZXk0KyspIHtcbiAgICAgICAgICAgICAgICBhcmdzW19rZXk0XSA9IGFyZ3VtZW50c1tfa2V5NF07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBfdGhpczcuZXZlbnRIYW5kbGUuYXBwbHkoX3RoaXM3LCBbZXZlbnROYW1lXS5jb25jYXQoYXJncykpO1xuICAgICAgICB9O1xuICAgIH07XG5cbiAgICBFZGl0b3JDb3JlLnByb3RvdHlwZS5jdXN0b21TdHlsZUZuID0gZnVuY3Rpb24gY3VzdG9tU3R5bGVGbihzdHlsZVNldCkge1xuICAgICAgICBpZiAoc3R5bGVTZXQuc2l6ZSA9PT0gMCkge1xuICAgICAgICAgICAgcmV0dXJuIHt9O1xuICAgICAgICB9XG4gICAgICAgIHZhciBwbHVnaW5zID0gdGhpcy5nZXRQbHVnaW5zKCk7XG4gICAgICAgIHZhciByZXN1bHRTdHlsZSA9IHt9O1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHBsdWdpbnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChwbHVnaW5zW2ldLmN1c3RvbVN0eWxlRm4pIHtcbiAgICAgICAgICAgICAgICB2YXIgc3R5bGVkID0gcGx1Z2luc1tpXS5jdXN0b21TdHlsZUZuKHN0eWxlU2V0KTtcbiAgICAgICAgICAgICAgICBpZiAoc3R5bGVkKSB7XG4gICAgICAgICAgICAgICAgICAgIF9leHRlbmRzKHJlc3VsdFN0eWxlLCBzdHlsZWQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzdWx0U3R5bGU7XG4gICAgfTtcblxuICAgIEVkaXRvckNvcmUucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgICAgdmFyIF9jbGFzc25hbWVzLFxuICAgICAgICAgICAgX3RoaXM4ID0gdGhpcztcblxuICAgICAgICB2YXIgX3Byb3BzMyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgICBwcmVmaXhDbHMgPSBfcHJvcHMzLnByZWZpeENscyxcbiAgICAgICAgICAgIHRvb2xiYXJzID0gX3Byb3BzMy50b29sYmFycyxcbiAgICAgICAgICAgIHN0eWxlID0gX3Byb3BzMy5zdHlsZSxcbiAgICAgICAgICAgIHJlYWRPbmx5ID0gX3Byb3BzMy5yZWFkT25seSxcbiAgICAgICAgICAgIG11bHRpTGluZXMgPSBfcHJvcHMzLm11bHRpTGluZXM7XG4gICAgICAgIHZhciBfc3RhdGUgPSB0aGlzLnN0YXRlLFxuICAgICAgICAgICAgZWRpdG9yU3RhdGUgPSBfc3RhdGUuZWRpdG9yU3RhdGUsXG4gICAgICAgICAgICB0b29sYmFyUGx1Z2lucyA9IF9zdGF0ZS50b29sYmFyUGx1Z2lucztcblxuICAgICAgICB2YXIgY3VzdG9tU3R5bGVNYXAgPSBjb25maWdTdG9yZS5nZXQoJ2N1c3RvbVN0eWxlTWFwJyk7XG4gICAgICAgIHZhciBibG9ja1JlbmRlck1hcCA9IGNvbmZpZ1N0b3JlLmdldCgnYmxvY2tSZW5kZXJNYXAnKTtcbiAgICAgICAgdmFyIGV2ZW50SGFuZGxlciA9IHRoaXMuZ2V0RXZlbnRIYW5kbGVyKCk7XG4gICAgICAgIHZhciBUb29sYmFyID0gdG9vbGJhci5jb21wb25lbnQ7XG4gICAgICAgIHZhciBjbHMgPSBjbGFzc25hbWVzKChfY2xhc3NuYW1lcyA9IHt9LCBfY2xhc3NuYW1lc1twcmVmaXhDbHMgKyAnLWVkaXRvciddID0gdHJ1ZSwgX2NsYXNzbmFtZXMucmVhZG9ubHkgPSByZWFkT25seSwgX2NsYXNzbmFtZXMub25lbGluZSA9ICFtdWx0aUxpbmVzLCBfY2xhc3NuYW1lcykpO1xuICAgICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICdkaXYnLFxuICAgICAgICAgICAgeyBzdHlsZTogc3R5bGUsIGNsYXNzTmFtZTogY2xzLCBvbkNsaWNrOiB0aGlzLl9mb2N1cy5iaW5kKHRoaXMpIH0sXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFRvb2xiYXIsIHsgZWRpdG9yU3RhdGU6IGVkaXRvclN0YXRlLCBwcmVmaXhDbHM6IHByZWZpeENscywgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLXRvb2xiYXInLCBwbHVnaW5zOiB0b29sYmFyUGx1Z2lucywgdG9vbGJhcnM6IHRvb2xiYXJzIH0pLFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICAgICAgICB7IGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy1lZGl0b3Itd3JhcHBlcicsIHJlZjogZnVuY3Rpb24gcmVmKGVsZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzOC5fZWRpdG9yV3JhcHBlciA9IGVsZTtcbiAgICAgICAgICAgICAgICAgICAgfSwgc3R5bGU6IHN0eWxlLCBvbkNsaWNrOiBmdW5jdGlvbiBvbkNsaWNrKGV2KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZXYucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgfSB9LFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRWRpdG9yLCBfZXh0ZW5kcyh7fSwgdGhpcy5wcm9wcywgZXZlbnRIYW5kbGVyLCB7IHJlZjogJ2VkaXRvcicsIGN1c3RvbVN0eWxlTWFwOiBjdXN0b21TdHlsZU1hcCwgY3VzdG9tU3R5bGVGbjogdGhpcy5jdXN0b21TdHlsZUZuLmJpbmQodGhpcyksIGVkaXRvclN0YXRlOiBlZGl0b3JTdGF0ZSwgaGFuZGxlS2V5Q29tbWFuZDogdGhpcy5oYW5kbGVLZXlDb21tYW5kLmJpbmQodGhpcyksIGtleUJpbmRpbmdGbjogdGhpcy5oYW5kbGVLZXlCaW5kaW5nLmJpbmQodGhpcyksIG9uQ2hhbmdlOiB0aGlzLnNldEVkaXRvclN0YXRlLmJpbmQodGhpcyksIGJsb2NrU3R5bGVGbjogdGhpcy5nZXRCbG9ja1N0eWxlLmJpbmQodGhpcyksIGJsb2NrUmVuZGVyTWFwOiBibG9ja1JlbmRlck1hcCwgaGFuZGxlUGFzdGVkVGV4dDogdGhpcy5oYW5kbGVQYXN0ZWRUZXh0LCBibG9ja1JlbmRlcmVyRm46IHRoaXMuYmxvY2tSZW5kZXJlckZuLmJpbmQodGhpcykgfSkpLFxuICAgICAgICAgICAgICAgIHJlYWRPbmx5ID8gUmVhY3QuY3JlYXRlRWxlbWVudCgnaW5wdXQnLCB7IHN0eWxlOiBmb2N1c0R1bW15U3R5bGUsIHJlZjogZnVuY3Rpb24gcmVmKGVsZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzOC5fZm9jdXNEdW1teSA9IGVsZTtcbiAgICAgICAgICAgICAgICAgICAgfSwgb25CbHVyOiBldmVudEhhbmRsZXIub25CbHVyIH0pIDogbnVsbCxcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmNoaWxkcmVuXG4gICAgICAgICAgICApXG4gICAgICAgICk7XG4gICAgfTtcblxuICAgIHJldHVybiBFZGl0b3JDb3JlO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5FZGl0b3JDb3JlLkdldFRleHQgPSBleHBvcnRUZXh0O1xuRWRpdG9yQ29yZS5HZXRIVE1MID0gR2V0SFRNTChjb25maWdTdG9yZSk7XG5FZGl0b3JDb3JlLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBtdWx0aUxpbmVzOiB0cnVlLFxuICAgIHBsdWdpbnM6IFtdLFxuICAgIHByZWZpeENsczogJ3JjLWVkaXRvci1jb3JlJyxcbiAgICBwbHVnaW5Db25maWc6IHt9LFxuICAgIHRvb2xiYXJzOiBbXSxcbiAgICBzcGlsaXRMaW5lOiAnZW50ZXInXG59O1xuRWRpdG9yQ29yZS5jaGlsZENvbnRleHRUeXBlcyA9IHtcbiAgICBnZXRFZGl0b3JTdGF0ZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc2V0RWRpdG9yU3RhdGU6IFByb3BUeXBlcy5mdW5jXG59O1xuZXhwb3J0IGRlZmF1bHQgRWRpdG9yQ29yZTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFGQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-core/es/EditorCore/index.js
