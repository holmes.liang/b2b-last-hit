

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

Object.defineProperty(exports, "__esModule", {
  value: true
});
/* eslint-disable no-underscore-dangle */

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var ColumnManager =
/*#__PURE__*/
function () {
  function ColumnManager(columns, elements) {
    _classCallCheck(this, ColumnManager);

    this._cached = {};
    this.columns = columns || this.normalize(elements);
  }

  _createClass(ColumnManager, [{
    key: "isAnyColumnsFixed",
    value: function isAnyColumnsFixed() {
      var _this = this;

      return this._cache('isAnyColumnsFixed', function () {
        return _this.columns.some(function (column) {
          return !!column.fixed;
        });
      });
    }
  }, {
    key: "isAnyColumnsLeftFixed",
    value: function isAnyColumnsLeftFixed() {
      var _this2 = this;

      return this._cache('isAnyColumnsLeftFixed', function () {
        return _this2.columns.some(function (column) {
          return column.fixed === 'left' || column.fixed === true;
        });
      });
    }
  }, {
    key: "isAnyColumnsRightFixed",
    value: function isAnyColumnsRightFixed() {
      var _this3 = this;

      return this._cache('isAnyColumnsRightFixed', function () {
        return _this3.columns.some(function (column) {
          return column.fixed === 'right';
        });
      });
    }
  }, {
    key: "leftColumns",
    value: function leftColumns() {
      var _this4 = this;

      return this._cache('leftColumns', function () {
        return _this4.groupedColumns().filter(function (column) {
          return column.fixed === 'left' || column.fixed === true;
        });
      });
    }
  }, {
    key: "rightColumns",
    value: function rightColumns() {
      var _this5 = this;

      return this._cache('rightColumns', function () {
        return _this5.groupedColumns().filter(function (column) {
          return column.fixed === 'right';
        });
      });
    }
  }, {
    key: "leafColumns",
    value: function leafColumns() {
      var _this6 = this;

      return this._cache('leafColumns', function () {
        return _this6._leafColumns(_this6.columns);
      });
    }
  }, {
    key: "leftLeafColumns",
    value: function leftLeafColumns() {
      var _this7 = this;

      return this._cache('leftLeafColumns', function () {
        return _this7._leafColumns(_this7.leftColumns());
      });
    }
  }, {
    key: "rightLeafColumns",
    value: function rightLeafColumns() {
      var _this8 = this;

      return this._cache('rightLeafColumns', function () {
        return _this8._leafColumns(_this8.rightColumns());
      });
    } // add appropriate rowspan and colspan to column

  }, {
    key: "groupedColumns",
    value: function groupedColumns() {
      var _this9 = this;

      return this._cache('groupedColumns', function () {
        var _groupColumns = function _groupColumns(columns) {
          var currentRow = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
          var parentColumn = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
          var rows = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
          /* eslint-disable no-param-reassign */
          // track how many rows we got

          rows[currentRow] = rows[currentRow] || [];
          var grouped = [];

          var setRowSpan = function setRowSpan(column) {
            var rowSpan = rows.length - currentRow;

            if (column && !column.children && // parent columns are supposed to be one row
            rowSpan > 1 && (!column.rowSpan || column.rowSpan < rowSpan)) {
              column.rowSpan = rowSpan;
            }
          };

          columns.forEach(function (column, index) {
            var newColumn = _objectSpread({}, column);

            rows[currentRow].push(newColumn);
            parentColumn.colSpan = parentColumn.colSpan || 0;

            if (newColumn.children && newColumn.children.length > 0) {
              newColumn.children = _groupColumns(newColumn.children, currentRow + 1, newColumn, rows);
              parentColumn.colSpan += newColumn.colSpan;
            } else {
              parentColumn.colSpan += 1;
            } // update rowspan to all same row columns


            for (var i = 0; i < rows[currentRow].length - 1; i += 1) {
              setRowSpan(rows[currentRow][i]);
            } // last column, update rowspan immediately


            if (index + 1 === columns.length) {
              setRowSpan(newColumn);
            }

            grouped.push(newColumn);
          });
          return grouped;
          /* eslint-enable no-param-reassign */
        };

        return _groupColumns(_this9.columns);
      });
    }
  }, {
    key: "normalize",
    value: function normalize(elements) {
      var _this10 = this;

      var columns = [];
      React.Children.forEach(elements, function (element) {
        if (!React.isValidElement(element)) {
          return;
        }

        var column = _objectSpread({}, element.props);

        if (element.key) {
          column.key = element.key;
        }

        if (element.type.isTableColumnGroup) {
          column.children = _this10.normalize(column.children);
        }

        columns.push(column);
      });
      return columns;
    }
  }, {
    key: "reset",
    value: function reset(columns, elements) {
      this.columns = columns || this.normalize(elements);
      this._cached = {};
    }
  }, {
    key: "_cache",
    value: function _cache(name, fn) {
      if (name in this._cached) {
        return this._cached[name];
      }

      this._cached[name] = fn();
      return this._cached[name];
    }
  }, {
    key: "_leafColumns",
    value: function _leafColumns(columns) {
      var _this11 = this;

      var leafColumns = [];
      columns.forEach(function (column) {
        if (!column.children) {
          leafColumns.push(column);
        } else {
          leafColumns.push.apply(leafColumns, _toConsumableArray(_this11._leafColumns(column.children)));
        }
      });
      return leafColumns;
    }
  }]);

  return ColumnManager;
}();

exports.default = ColumnManager;
/* eslint-enable *///# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvQ29sdW1uTWFuYWdlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXRhYmxlL2VzL0NvbHVtbk1hbmFnZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG5cbmZ1bmN0aW9uIF90b0NvbnN1bWFibGVBcnJheShhcnIpIHsgcmV0dXJuIF9hcnJheVdpdGhvdXRIb2xlcyhhcnIpIHx8IF9pdGVyYWJsZVRvQXJyYXkoYXJyKSB8fCBfbm9uSXRlcmFibGVTcHJlYWQoKTsgfVxuXG5mdW5jdGlvbiBfbm9uSXRlcmFibGVTcHJlYWQoKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJJbnZhbGlkIGF0dGVtcHQgdG8gc3ByZWFkIG5vbi1pdGVyYWJsZSBpbnN0YW5jZVwiKTsgfVxuXG5mdW5jdGlvbiBfaXRlcmFibGVUb0FycmF5KGl0ZXIpIHsgaWYgKFN5bWJvbC5pdGVyYXRvciBpbiBPYmplY3QoaXRlcikgfHwgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGl0ZXIpID09PSBcIltvYmplY3QgQXJndW1lbnRzXVwiKSByZXR1cm4gQXJyYXkuZnJvbShpdGVyKTsgfVxuXG5mdW5jdGlvbiBfYXJyYXlXaXRob3V0SG9sZXMoYXJyKSB7IGlmIChBcnJheS5pc0FycmF5KGFycikpIHsgZm9yICh2YXIgaSA9IDAsIGFycjIgPSBuZXcgQXJyYXkoYXJyLmxlbmd0aCk7IGkgPCBhcnIubGVuZ3RoOyBpKyspIHsgYXJyMltpXSA9IGFycltpXTsgfSByZXR1cm4gYXJyMjsgfSB9XG5cbmZ1bmN0aW9uIG93bktleXMob2JqZWN0LCBlbnVtZXJhYmxlT25seSkgeyB2YXIga2V5cyA9IE9iamVjdC5rZXlzKG9iamVjdCk7IGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKSB7IHZhciBzeW1ib2xzID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhvYmplY3QpOyBpZiAoZW51bWVyYWJsZU9ubHkpIHN5bWJvbHMgPSBzeW1ib2xzLmZpbHRlcihmdW5jdGlvbiAoc3ltKSB7IHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKG9iamVjdCwgc3ltKS5lbnVtZXJhYmxlOyB9KTsga2V5cy5wdXNoLmFwcGx5KGtleXMsIHN5bWJvbHMpOyB9IHJldHVybiBrZXlzOyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RTcHJlYWQodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV0gIT0gbnVsbCA/IGFyZ3VtZW50c1tpXSA6IHt9OyBpZiAoaSAlIDIpIHsgb3duS2V5cyhPYmplY3Qoc291cmNlKSwgdHJ1ZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IF9kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgc291cmNlW2tleV0pOyB9KTsgfSBlbHNlIGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycykgeyBPYmplY3QuZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKHNvdXJjZSkpOyB9IGVsc2UgeyBvd25LZXlzKE9iamVjdChzb3VyY2UpKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHNvdXJjZSwga2V5KSk7IH0pOyB9IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7IGlmIChrZXkgaW4gb2JqKSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgeyB2YWx1ZTogdmFsdWUsIGVudW1lcmFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSwgd3JpdGFibGU6IHRydWUgfSk7IH0gZWxzZSB7IG9ialtrZXldID0gdmFsdWU7IH0gcmV0dXJuIG9iajsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9XG5cbmZ1bmN0aW9uIF9jcmVhdGVDbGFzcyhDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9XG5cbnZhciBfX2ltcG9ydFN0YXIgPSB0aGlzICYmIHRoaXMuX19pbXBvcnRTdGFyIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcbiAgdmFyIHJlc3VsdCA9IHt9O1xuICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSB7XG4gICAgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcbiAgfVxuICByZXN1bHRbXCJkZWZhdWx0XCJdID0gbW9kO1xuICByZXR1cm4gcmVzdWx0O1xufTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbi8qIGVzbGludC1kaXNhYmxlIG5vLXVuZGVyc2NvcmUtZGFuZ2xlICovXG5cbnZhciBSZWFjdCA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicmVhY3RcIikpO1xuXG52YXIgQ29sdW1uTWFuYWdlciA9XG4vKiNfX1BVUkVfXyovXG5mdW5jdGlvbiAoKSB7XG4gIGZ1bmN0aW9uIENvbHVtbk1hbmFnZXIoY29sdW1ucywgZWxlbWVudHMpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQ29sdW1uTWFuYWdlcik7XG5cbiAgICB0aGlzLl9jYWNoZWQgPSB7fTtcbiAgICB0aGlzLmNvbHVtbnMgPSBjb2x1bW5zIHx8IHRoaXMubm9ybWFsaXplKGVsZW1lbnRzKTtcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhDb2x1bW5NYW5hZ2VyLCBbe1xuICAgIGtleTogXCJpc0FueUNvbHVtbnNGaXhlZFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0FueUNvbHVtbnNGaXhlZCgpIHtcbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG5cbiAgICAgIHJldHVybiB0aGlzLl9jYWNoZSgnaXNBbnlDb2x1bW5zRml4ZWQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBfdGhpcy5jb2x1bW5zLnNvbWUoZnVuY3Rpb24gKGNvbHVtbikge1xuICAgICAgICAgIHJldHVybiAhIWNvbHVtbi5maXhlZDtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiaXNBbnlDb2x1bW5zTGVmdEZpeGVkXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzQW55Q29sdW1uc0xlZnRGaXhlZCgpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICByZXR1cm4gdGhpcy5fY2FjaGUoJ2lzQW55Q29sdW1uc0xlZnRGaXhlZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIF90aGlzMi5jb2x1bW5zLnNvbWUoZnVuY3Rpb24gKGNvbHVtbikge1xuICAgICAgICAgIHJldHVybiBjb2x1bW4uZml4ZWQgPT09ICdsZWZ0JyB8fCBjb2x1bW4uZml4ZWQgPT09IHRydWU7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcImlzQW55Q29sdW1uc1JpZ2h0Rml4ZWRcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNBbnlDb2x1bW5zUmlnaHRGaXhlZCgpIHtcbiAgICAgIHZhciBfdGhpczMgPSB0aGlzO1xuXG4gICAgICByZXR1cm4gdGhpcy5fY2FjaGUoJ2lzQW55Q29sdW1uc1JpZ2h0Rml4ZWQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBfdGhpczMuY29sdW1ucy5zb21lKGZ1bmN0aW9uIChjb2x1bW4pIHtcbiAgICAgICAgICByZXR1cm4gY29sdW1uLmZpeGVkID09PSAncmlnaHQnO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJsZWZ0Q29sdW1uc1wiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBsZWZ0Q29sdW1ucygpIHtcbiAgICAgIHZhciBfdGhpczQgPSB0aGlzO1xuXG4gICAgICByZXR1cm4gdGhpcy5fY2FjaGUoJ2xlZnRDb2x1bW5zJywgZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gX3RoaXM0Lmdyb3VwZWRDb2x1bW5zKCkuZmlsdGVyKGZ1bmN0aW9uIChjb2x1bW4pIHtcbiAgICAgICAgICByZXR1cm4gY29sdW1uLmZpeGVkID09PSAnbGVmdCcgfHwgY29sdW1uLmZpeGVkID09PSB0cnVlO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJyaWdodENvbHVtbnNcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmlnaHRDb2x1bW5zKCkge1xuICAgICAgdmFyIF90aGlzNSA9IHRoaXM7XG5cbiAgICAgIHJldHVybiB0aGlzLl9jYWNoZSgncmlnaHRDb2x1bW5zJywgZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gX3RoaXM1Lmdyb3VwZWRDb2x1bW5zKCkuZmlsdGVyKGZ1bmN0aW9uIChjb2x1bW4pIHtcbiAgICAgICAgICByZXR1cm4gY29sdW1uLmZpeGVkID09PSAncmlnaHQnO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJsZWFmQ29sdW1uc1wiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBsZWFmQ29sdW1ucygpIHtcbiAgICAgIHZhciBfdGhpczYgPSB0aGlzO1xuXG4gICAgICByZXR1cm4gdGhpcy5fY2FjaGUoJ2xlYWZDb2x1bW5zJywgZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gX3RoaXM2Ll9sZWFmQ29sdW1ucyhfdGhpczYuY29sdW1ucyk7XG4gICAgICB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwibGVmdExlYWZDb2x1bW5zXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGxlZnRMZWFmQ29sdW1ucygpIHtcbiAgICAgIHZhciBfdGhpczcgPSB0aGlzO1xuXG4gICAgICByZXR1cm4gdGhpcy5fY2FjaGUoJ2xlZnRMZWFmQ29sdW1ucycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIF90aGlzNy5fbGVhZkNvbHVtbnMoX3RoaXM3LmxlZnRDb2x1bW5zKCkpO1xuICAgICAgfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInJpZ2h0TGVhZkNvbHVtbnNcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmlnaHRMZWFmQ29sdW1ucygpIHtcbiAgICAgIHZhciBfdGhpczggPSB0aGlzO1xuXG4gICAgICByZXR1cm4gdGhpcy5fY2FjaGUoJ3JpZ2h0TGVhZkNvbHVtbnMnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBfdGhpczguX2xlYWZDb2x1bW5zKF90aGlzOC5yaWdodENvbHVtbnMoKSk7XG4gICAgICB9KTtcbiAgICB9IC8vIGFkZCBhcHByb3ByaWF0ZSByb3dzcGFuIGFuZCBjb2xzcGFuIHRvIGNvbHVtblxuXG4gIH0sIHtcbiAgICBrZXk6IFwiZ3JvdXBlZENvbHVtbnNcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ3JvdXBlZENvbHVtbnMoKSB7XG4gICAgICB2YXIgX3RoaXM5ID0gdGhpcztcblxuICAgICAgcmV0dXJuIHRoaXMuX2NhY2hlKCdncm91cGVkQ29sdW1ucycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIF9ncm91cENvbHVtbnMgPSBmdW5jdGlvbiBfZ3JvdXBDb2x1bW5zKGNvbHVtbnMpIHtcbiAgICAgICAgICB2YXIgY3VycmVudFJvdyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogMDtcbiAgICAgICAgICB2YXIgcGFyZW50Q29sdW1uID0gYXJndW1lbnRzLmxlbmd0aCA+IDIgJiYgYXJndW1lbnRzWzJdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMl0gOiB7fTtcbiAgICAgICAgICB2YXIgcm93cyA9IGFyZ3VtZW50cy5sZW5ndGggPiAzICYmIGFyZ3VtZW50c1szXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzNdIDogW107XG5cbiAgICAgICAgICAvKiBlc2xpbnQtZGlzYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgICAgICAgIC8vIHRyYWNrIGhvdyBtYW55IHJvd3Mgd2UgZ290XG4gICAgICAgICAgcm93c1tjdXJyZW50Um93XSA9IHJvd3NbY3VycmVudFJvd10gfHwgW107XG4gICAgICAgICAgdmFyIGdyb3VwZWQgPSBbXTtcblxuICAgICAgICAgIHZhciBzZXRSb3dTcGFuID0gZnVuY3Rpb24gc2V0Um93U3Bhbihjb2x1bW4pIHtcbiAgICAgICAgICAgIHZhciByb3dTcGFuID0gcm93cy5sZW5ndGggLSBjdXJyZW50Um93O1xuXG4gICAgICAgICAgICBpZiAoY29sdW1uICYmICFjb2x1bW4uY2hpbGRyZW4gJiYgLy8gcGFyZW50IGNvbHVtbnMgYXJlIHN1cHBvc2VkIHRvIGJlIG9uZSByb3dcbiAgICAgICAgICAgIHJvd1NwYW4gPiAxICYmICghY29sdW1uLnJvd1NwYW4gfHwgY29sdW1uLnJvd1NwYW4gPCByb3dTcGFuKSkge1xuICAgICAgICAgICAgICBjb2x1bW4ucm93U3BhbiA9IHJvd1NwYW47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfTtcblxuICAgICAgICAgIGNvbHVtbnMuZm9yRWFjaChmdW5jdGlvbiAoY29sdW1uLCBpbmRleCkge1xuICAgICAgICAgICAgdmFyIG5ld0NvbHVtbiA9IF9vYmplY3RTcHJlYWQoe30sIGNvbHVtbik7XG5cbiAgICAgICAgICAgIHJvd3NbY3VycmVudFJvd10ucHVzaChuZXdDb2x1bW4pO1xuICAgICAgICAgICAgcGFyZW50Q29sdW1uLmNvbFNwYW4gPSBwYXJlbnRDb2x1bW4uY29sU3BhbiB8fCAwO1xuXG4gICAgICAgICAgICBpZiAobmV3Q29sdW1uLmNoaWxkcmVuICYmIG5ld0NvbHVtbi5jaGlsZHJlbi5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIG5ld0NvbHVtbi5jaGlsZHJlbiA9IF9ncm91cENvbHVtbnMobmV3Q29sdW1uLmNoaWxkcmVuLCBjdXJyZW50Um93ICsgMSwgbmV3Q29sdW1uLCByb3dzKTtcbiAgICAgICAgICAgICAgcGFyZW50Q29sdW1uLmNvbFNwYW4gKz0gbmV3Q29sdW1uLmNvbFNwYW47XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBwYXJlbnRDb2x1bW4uY29sU3BhbiArPSAxO1xuICAgICAgICAgICAgfSAvLyB1cGRhdGUgcm93c3BhbiB0byBhbGwgc2FtZSByb3cgY29sdW1uc1xuXG5cbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcm93c1tjdXJyZW50Um93XS5sZW5ndGggLSAxOyBpICs9IDEpIHtcbiAgICAgICAgICAgICAgc2V0Um93U3Bhbihyb3dzW2N1cnJlbnRSb3ddW2ldKTtcbiAgICAgICAgICAgIH0gLy8gbGFzdCBjb2x1bW4sIHVwZGF0ZSByb3dzcGFuIGltbWVkaWF0ZWx5XG5cblxuICAgICAgICAgICAgaWYgKGluZGV4ICsgMSA9PT0gY29sdW1ucy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgc2V0Um93U3BhbihuZXdDb2x1bW4pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBncm91cGVkLnB1c2gobmV3Q29sdW1uKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm4gZ3JvdXBlZDtcbiAgICAgICAgICAvKiBlc2xpbnQtZW5hYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgICAgIH07XG5cbiAgICAgICAgcmV0dXJuIF9ncm91cENvbHVtbnMoX3RoaXM5LmNvbHVtbnMpO1xuICAgICAgfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcIm5vcm1hbGl6ZVwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBub3JtYWxpemUoZWxlbWVudHMpIHtcbiAgICAgIHZhciBfdGhpczEwID0gdGhpcztcblxuICAgICAgdmFyIGNvbHVtbnMgPSBbXTtcbiAgICAgIFJlYWN0LkNoaWxkcmVuLmZvckVhY2goZWxlbWVudHMsIGZ1bmN0aW9uIChlbGVtZW50KSB7XG4gICAgICAgIGlmICghUmVhY3QuaXNWYWxpZEVsZW1lbnQoZWxlbWVudCkpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgY29sdW1uID0gX29iamVjdFNwcmVhZCh7fSwgZWxlbWVudC5wcm9wcyk7XG5cbiAgICAgICAgaWYgKGVsZW1lbnQua2V5KSB7XG4gICAgICAgICAgY29sdW1uLmtleSA9IGVsZW1lbnQua2V5O1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGVsZW1lbnQudHlwZS5pc1RhYmxlQ29sdW1uR3JvdXApIHtcbiAgICAgICAgICBjb2x1bW4uY2hpbGRyZW4gPSBfdGhpczEwLm5vcm1hbGl6ZShjb2x1bW4uY2hpbGRyZW4pO1xuICAgICAgICB9XG5cbiAgICAgICAgY29sdW1ucy5wdXNoKGNvbHVtbik7XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBjb2x1bW5zO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJyZXNldFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZXNldChjb2x1bW5zLCBlbGVtZW50cykge1xuICAgICAgdGhpcy5jb2x1bW5zID0gY29sdW1ucyB8fCB0aGlzLm5vcm1hbGl6ZShlbGVtZW50cyk7XG4gICAgICB0aGlzLl9jYWNoZWQgPSB7fTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiX2NhY2hlXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIF9jYWNoZShuYW1lLCBmbikge1xuICAgICAgaWYgKG5hbWUgaW4gdGhpcy5fY2FjaGVkKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9jYWNoZWRbbmFtZV07XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX2NhY2hlZFtuYW1lXSA9IGZuKCk7XG4gICAgICByZXR1cm4gdGhpcy5fY2FjaGVkW25hbWVdO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJfbGVhZkNvbHVtbnNcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gX2xlYWZDb2x1bW5zKGNvbHVtbnMpIHtcbiAgICAgIHZhciBfdGhpczExID0gdGhpcztcblxuICAgICAgdmFyIGxlYWZDb2x1bW5zID0gW107XG4gICAgICBjb2x1bW5zLmZvckVhY2goZnVuY3Rpb24gKGNvbHVtbikge1xuICAgICAgICBpZiAoIWNvbHVtbi5jaGlsZHJlbikge1xuICAgICAgICAgIGxlYWZDb2x1bW5zLnB1c2goY29sdW1uKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBsZWFmQ29sdW1ucy5wdXNoLmFwcGx5KGxlYWZDb2x1bW5zLCBfdG9Db25zdW1hYmxlQXJyYXkoX3RoaXMxMS5fbGVhZkNvbHVtbnMoY29sdW1uLmNoaWxkcmVuKSkpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBsZWFmQ29sdW1ucztcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gQ29sdW1uTWFuYWdlcjtcbn0oKTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gQ29sdW1uTWFuYWdlcjtcbi8qIGVzbGludC1lbmFibGUgKi8iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBeERBO0FBMERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXhCQTtBQTBCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFkQTtBQUNBO0FBZ0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/ColumnManager.js
