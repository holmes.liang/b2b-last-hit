/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var Axis = __webpack_require__(/*! ../Axis */ "./node_modules/echarts/lib/coord/Axis.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Extend axis 2d
 * @constructor module:echarts/coord/cartesian/Axis2D
 * @extends {module:echarts/coord/cartesian/Axis}
 * @param {string} dim
 * @param {*} scale
 * @param {Array.<number>} coordExtent
 * @param {string} axisType
 * @param {string} position
 */


var Axis2D = function Axis2D(dim, scale, coordExtent, axisType, position) {
  Axis.call(this, dim, scale, coordExtent);
  /**
   * Axis type
   *  - 'category'
   *  - 'value'
   *  - 'time'
   *  - 'log'
   * @type {string}
   */

  this.type = axisType || 'value';
  /**
   * Axis position
   *  - 'top'
   *  - 'bottom'
   *  - 'left'
   *  - 'right'
   */

  this.position = position || 'bottom';
};

Axis2D.prototype = {
  constructor: Axis2D,

  /**
   * Index of axis, can be used as key
   */
  index: 0,

  /**
   * Implemented in <module:echarts/coord/cartesian/Grid>.
   * @return {Array.<module:echarts/coord/cartesian/Axis2D>}
   *         If not on zero of other axis, return null/undefined.
   *         If no axes, return an empty array.
   */
  getAxesOnZeroOf: null,

  /**
   * Axis model
   * @param {module:echarts/coord/cartesian/AxisModel}
   */
  model: null,
  isHorizontal: function isHorizontal() {
    var position = this.position;
    return position === 'top' || position === 'bottom';
  },

  /**
   * Each item cooresponds to this.getExtent(), which
   * means globalExtent[0] may greater than globalExtent[1],
   * unless `asc` is input.
   *
   * @param {boolean} [asc]
   * @return {Array.<number>}
   */
  getGlobalExtent: function getGlobalExtent(asc) {
    var ret = this.getExtent();
    ret[0] = this.toGlobalCoord(ret[0]);
    ret[1] = this.toGlobalCoord(ret[1]);
    asc && ret[0] > ret[1] && ret.reverse();
    return ret;
  },
  getOtherAxis: function getOtherAxis() {
    this.grid.getOtherAxis();
  },

  /**
   * @override
   */
  pointToData: function pointToData(point, clamp) {
    return this.coordToData(this.toLocalCoord(point[this.dim === 'x' ? 0 : 1]), clamp);
  },

  /**
   * Transform global coord to local coord,
   * i.e. var localCoord = axis.toLocalCoord(80);
   * designate by module:echarts/coord/cartesian/Grid.
   * @type {Function}
   */
  toLocalCoord: null,

  /**
   * Transform global coord to local coord,
   * i.e. var globalCoord = axis.toLocalCoord(40);
   * designate by module:echarts/coord/cartesian/Grid.
   * @type {Function}
   */
  toGlobalCoord: null
};
zrUtil.inherits(Axis2D, Axis);
var _default = Axis2D;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvY2FydGVzaWFuL0F4aXMyRC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2Nvb3JkL2NhcnRlc2lhbi9BeGlzMkQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgQXhpcyA9IHJlcXVpcmUoXCIuLi9BeGlzXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qKlxuICogRXh0ZW5kIGF4aXMgMmRcbiAqIEBjb25zdHJ1Y3RvciBtb2R1bGU6ZWNoYXJ0cy9jb29yZC9jYXJ0ZXNpYW4vQXhpczJEXG4gKiBAZXh0ZW5kcyB7bW9kdWxlOmVjaGFydHMvY29vcmQvY2FydGVzaWFuL0F4aXN9XG4gKiBAcGFyYW0ge3N0cmluZ30gZGltXG4gKiBAcGFyYW0geyp9IHNjYWxlXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBjb29yZEV4dGVudFxuICogQHBhcmFtIHtzdHJpbmd9IGF4aXNUeXBlXG4gKiBAcGFyYW0ge3N0cmluZ30gcG9zaXRpb25cbiAqL1xudmFyIEF4aXMyRCA9IGZ1bmN0aW9uIChkaW0sIHNjYWxlLCBjb29yZEV4dGVudCwgYXhpc1R5cGUsIHBvc2l0aW9uKSB7XG4gIEF4aXMuY2FsbCh0aGlzLCBkaW0sIHNjYWxlLCBjb29yZEV4dGVudCk7XG4gIC8qKlxuICAgKiBBeGlzIHR5cGVcbiAgICogIC0gJ2NhdGVnb3J5J1xuICAgKiAgLSAndmFsdWUnXG4gICAqICAtICd0aW1lJ1xuICAgKiAgLSAnbG9nJ1xuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cblxuICB0aGlzLnR5cGUgPSBheGlzVHlwZSB8fCAndmFsdWUnO1xuICAvKipcbiAgICogQXhpcyBwb3NpdGlvblxuICAgKiAgLSAndG9wJ1xuICAgKiAgLSAnYm90dG9tJ1xuICAgKiAgLSAnbGVmdCdcbiAgICogIC0gJ3JpZ2h0J1xuICAgKi9cblxuICB0aGlzLnBvc2l0aW9uID0gcG9zaXRpb24gfHwgJ2JvdHRvbSc7XG59O1xuXG5BeGlzMkQucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogQXhpczJELFxuXG4gIC8qKlxuICAgKiBJbmRleCBvZiBheGlzLCBjYW4gYmUgdXNlZCBhcyBrZXlcbiAgICovXG4gIGluZGV4OiAwLFxuXG4gIC8qKlxuICAgKiBJbXBsZW1lbnRlZCBpbiA8bW9kdWxlOmVjaGFydHMvY29vcmQvY2FydGVzaWFuL0dyaWQ+LlxuICAgKiBAcmV0dXJuIHtBcnJheS48bW9kdWxlOmVjaGFydHMvY29vcmQvY2FydGVzaWFuL0F4aXMyRD59XG4gICAqICAgICAgICAgSWYgbm90IG9uIHplcm8gb2Ygb3RoZXIgYXhpcywgcmV0dXJuIG51bGwvdW5kZWZpbmVkLlxuICAgKiAgICAgICAgIElmIG5vIGF4ZXMsIHJldHVybiBhbiBlbXB0eSBhcnJheS5cbiAgICovXG4gIGdldEF4ZXNPblplcm9PZjogbnVsbCxcblxuICAvKipcbiAgICogQXhpcyBtb2RlbFxuICAgKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2Nvb3JkL2NhcnRlc2lhbi9BeGlzTW9kZWx9XG4gICAqL1xuICBtb2RlbDogbnVsbCxcbiAgaXNIb3Jpem9udGFsOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHBvc2l0aW9uID0gdGhpcy5wb3NpdGlvbjtcbiAgICByZXR1cm4gcG9zaXRpb24gPT09ICd0b3AnIHx8IHBvc2l0aW9uID09PSAnYm90dG9tJztcbiAgfSxcblxuICAvKipcbiAgICogRWFjaCBpdGVtIGNvb3Jlc3BvbmRzIHRvIHRoaXMuZ2V0RXh0ZW50KCksIHdoaWNoXG4gICAqIG1lYW5zIGdsb2JhbEV4dGVudFswXSBtYXkgZ3JlYXRlciB0aGFuIGdsb2JhbEV4dGVudFsxXSxcbiAgICogdW5sZXNzIGBhc2NgIGlzIGlucHV0LlxuICAgKlxuICAgKiBAcGFyYW0ge2Jvb2xlYW59IFthc2NdXG4gICAqIEByZXR1cm4ge0FycmF5LjxudW1iZXI+fVxuICAgKi9cbiAgZ2V0R2xvYmFsRXh0ZW50OiBmdW5jdGlvbiAoYXNjKSB7XG4gICAgdmFyIHJldCA9IHRoaXMuZ2V0RXh0ZW50KCk7XG4gICAgcmV0WzBdID0gdGhpcy50b0dsb2JhbENvb3JkKHJldFswXSk7XG4gICAgcmV0WzFdID0gdGhpcy50b0dsb2JhbENvb3JkKHJldFsxXSk7XG4gICAgYXNjICYmIHJldFswXSA+IHJldFsxXSAmJiByZXQucmV2ZXJzZSgpO1xuICAgIHJldHVybiByZXQ7XG4gIH0sXG4gIGdldE90aGVyQXhpczogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuZ3JpZC5nZXRPdGhlckF4aXMoKTtcbiAgfSxcblxuICAvKipcbiAgICogQG92ZXJyaWRlXG4gICAqL1xuICBwb2ludFRvRGF0YTogZnVuY3Rpb24gKHBvaW50LCBjbGFtcCkge1xuICAgIHJldHVybiB0aGlzLmNvb3JkVG9EYXRhKHRoaXMudG9Mb2NhbENvb3JkKHBvaW50W3RoaXMuZGltID09PSAneCcgPyAwIDogMV0pLCBjbGFtcCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFRyYW5zZm9ybSBnbG9iYWwgY29vcmQgdG8gbG9jYWwgY29vcmQsXG4gICAqIGkuZS4gdmFyIGxvY2FsQ29vcmQgPSBheGlzLnRvTG9jYWxDb29yZCg4MCk7XG4gICAqIGRlc2lnbmF0ZSBieSBtb2R1bGU6ZWNoYXJ0cy9jb29yZC9jYXJ0ZXNpYW4vR3JpZC5cbiAgICogQHR5cGUge0Z1bmN0aW9ufVxuICAgKi9cbiAgdG9Mb2NhbENvb3JkOiBudWxsLFxuXG4gIC8qKlxuICAgKiBUcmFuc2Zvcm0gZ2xvYmFsIGNvb3JkIHRvIGxvY2FsIGNvb3JkLFxuICAgKiBpLmUuIHZhciBnbG9iYWxDb29yZCA9IGF4aXMudG9Mb2NhbENvb3JkKDQwKTtcbiAgICogZGVzaWduYXRlIGJ5IG1vZHVsZTplY2hhcnRzL2Nvb3JkL2NhcnRlc2lhbi9HcmlkLlxuICAgKiBAdHlwZSB7RnVuY3Rpb259XG4gICAqL1xuICB0b0dsb2JhbENvb3JkOiBudWxsXG59O1xuenJVdGlsLmluaGVyaXRzKEF4aXMyRCwgQXhpcyk7XG52YXIgX2RlZmF1bHQgPSBBeGlzMkQ7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7Ozs7Ozs7O0FBVUE7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQWxFQTtBQW9FQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/cartesian/Axis2D.js
