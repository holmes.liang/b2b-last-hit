

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var classnames_1 = __importDefault(__webpack_require__(/*! classnames */ "./node_modules/classnames/index.js"));

var component_classes_1 = __importDefault(__webpack_require__(/*! component-classes */ "./node_modules/component-classes/index.js"));

var rc_animate_1 = __importDefault(__webpack_require__(/*! rc-animate */ "./node_modules/rc-animate/es/Animate.js"));

var rc_menu_1 = __webpack_require__(/*! rc-menu */ "./node_modules/rc-menu/es/index.js");

var toArray_1 = __importDefault(__webpack_require__(/*! rc-util/lib/Children/toArray */ "./node_modules/rc-util/lib/Children/toArray.js"));

var KeyCode_1 = __importDefault(__webpack_require__(/*! rc-util/lib/KeyCode */ "./node_modules/rc-util/lib/KeyCode.js"));

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var ReactDOM = __importStar(__webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js"));

var react_lifecycles_compat_1 = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");

var warning_1 = __importDefault(__webpack_require__(/*! warning */ "./node_modules/warning/warning.js"));

var Option_1 = __importDefault(__webpack_require__(/*! ./Option */ "./node_modules/rc-select/es/Option.js")); // Where el is the DOM element you'd like to test for visibility


function isHidden(node) {
  return !node || node.offsetParent === null;
}

var PropTypes_1 = __importDefault(__webpack_require__(/*! ./PropTypes */ "./node_modules/rc-select/es/PropTypes.js"));

var SelectTrigger_1 = __importDefault(__webpack_require__(/*! ./SelectTrigger */ "./node_modules/rc-select/es/SelectTrigger.js"));

var util_1 = __webpack_require__(/*! ./util */ "./node_modules/rc-select/es/util.js");

var SELECT_EMPTY_VALUE_KEY = 'RC_SELECT_EMPTY_VALUE_KEY';

var noop = function noop() {
  return null;
};

function chaining() {
  for (var _len = arguments.length, fns = new Array(_len), _key = 0; _key < _len; _key++) {
    fns[_key] = arguments[_key];
  }

  return function () {
    for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    } // tslint:disable-next-line:prefer-for-of


    for (var i = 0; i < fns.length; i++) {
      if (fns[i] && typeof fns[i] === 'function') {
        fns[i].apply(chaining, args);
      }
    }
  };
}

var Select =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Select, _React$Component);

  function Select(props) {
    var _this;

    _classCallCheck(this, Select);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Select).call(this, props));
    _this.inputRef = null;
    _this.inputMirrorRef = null;
    _this.topCtrlRef = null;
    _this.selectTriggerRef = null;
    _this.rootRef = null;
    _this.selectionRef = null;
    _this.dropdownContainer = null;
    _this.blurTimer = null;
    _this.focusTimer = null;
    _this.comboboxTimer = null; // tslint:disable-next-line:variable-name

    _this._focused = false; // tslint:disable-next-line:variable-name

    _this._mouseDown = false; // tslint:disable-next-line:variable-name

    _this._options = []; // tslint:disable-next-line:variable-name

    _this._empty = false;

    _this.onInputChange = function (event) {
      var tokenSeparators = _this.props.tokenSeparators;
      var val = event.target.value;

      if (util_1.isMultipleOrTags(_this.props) && tokenSeparators.length && util_1.includesSeparators(val, tokenSeparators)) {
        var nextValue = _this.getValueByInput(val);

        if (nextValue !== undefined) {
          _this.fireChange(nextValue);
        }

        _this.setOpenState(false, {
          needFocus: true
        });

        _this.setInputValue('', false);

        return;
      }

      _this.setInputValue(val);

      _this.setState({
        open: true
      });

      if (util_1.isCombobox(_this.props)) {
        _this.fireChange([val]);
      }
    };

    _this.onDropdownVisibleChange = function (open) {
      if (open && !_this._focused) {
        _this.clearBlurTime();

        _this.timeoutFocus();

        _this._focused = true;

        _this.updateFocusClassName();
      }

      _this.setOpenState(open);
    }; // combobox ignore


    _this.onKeyDown = function (event) {
      var open = _this.state.open;
      var disabled = _this.props.disabled;

      if (disabled) {
        return;
      }

      var keyCode = event.keyCode;

      if (open && !_this.getInputDOMNode()) {
        _this.onInputKeyDown(event);
      } else if (keyCode === KeyCode_1["default"].ENTER || keyCode === KeyCode_1["default"].DOWN) {
        if (!open) {
          _this.setOpenState(true);
        }

        event.preventDefault();
      } else if (keyCode === KeyCode_1["default"].SPACE) {
        // Not block space if popup is shown
        if (!open) {
          _this.setOpenState(true);

          event.preventDefault();
        }
      }
    };

    _this.onInputKeyDown = function (event) {
      var _this$props = _this.props,
          disabled = _this$props.disabled,
          combobox = _this$props.combobox,
          defaultActiveFirstOption = _this$props.defaultActiveFirstOption;

      if (disabled) {
        return;
      }

      var state = _this.state;

      var isRealOpen = _this.getRealOpenState(state); // magic code


      var keyCode = event.keyCode;

      if (util_1.isMultipleOrTags(_this.props) && !event.target.value && keyCode === KeyCode_1["default"].BACKSPACE) {
        event.preventDefault();
        var value = state.value;

        if (value.length) {
          _this.removeSelected(value[value.length - 1]);
        }

        return;
      }

      if (keyCode === KeyCode_1["default"].DOWN) {
        if (!state.open) {
          _this.openIfHasChildren();

          event.preventDefault();
          event.stopPropagation();
          return;
        }
      } else if (keyCode === KeyCode_1["default"].ENTER && state.open) {
        // Aviod trigger form submit when select item
        // https://github.com/ant-design/ant-design/issues/10861
        // https://github.com/ant-design/ant-design/issues/14544
        if (isRealOpen || !combobox) {
          event.preventDefault();
        } // Hard close popup to avoid lock of non option in combobox mode


        if (isRealOpen && combobox && defaultActiveFirstOption === false) {
          _this.comboboxTimer = setTimeout(function () {
            _this.setOpenState(false);
          });
        }
      } else if (keyCode === KeyCode_1["default"].ESC) {
        if (state.open) {
          _this.setOpenState(false);

          event.preventDefault();
          event.stopPropagation();
        }

        return;
      }

      if (isRealOpen && _this.selectTriggerRef) {
        var menu = _this.selectTriggerRef.getInnerMenu();

        if (menu && menu.onKeyDown(event, _this.handleBackfill)) {
          event.preventDefault();
          event.stopPropagation();
        }
      }
    };

    _this.onMenuSelect = function (_ref) {
      var item = _ref.item;

      if (!item) {
        return;
      }

      var value = _this.state.value;
      var props = _this.props;
      var selectedValue = util_1.getValuePropValue(item);
      var lastValue = value[value.length - 1];
      var skipTrigger = false;

      if (util_1.isMultipleOrTags(props)) {
        if (util_1.findIndexInValueBySingleValue(value, selectedValue) !== -1) {
          skipTrigger = true;
        } else {
          value = value.concat([selectedValue]);
        }
      } else {
        if (!util_1.isCombobox(props) && lastValue !== undefined && lastValue === selectedValue && selectedValue !== _this.state.backfillValue) {
          _this.setOpenState(false, {
            needFocus: true,
            fireSearch: false
          });

          skipTrigger = true;
        } else {
          value = [selectedValue];

          _this.setOpenState(false, {
            needFocus: true,
            fireSearch: false
          });
        }
      }

      if (!skipTrigger) {
        _this.fireChange(value);
      }

      _this.fireSelect(selectedValue);

      if (!skipTrigger) {
        var inputValue = util_1.isCombobox(props) ? util_1.getPropValue(item, props.optionLabelProp) : '';

        if (props.autoClearSearchValue) {
          _this.setInputValue(inputValue, false);
        }
      }
    };

    _this.onMenuDeselect = function (_ref2) {
      var item = _ref2.item,
          domEvent = _ref2.domEvent;

      if (domEvent.type === 'keydown' && domEvent.keyCode === KeyCode_1["default"].ENTER) {
        var menuItemDomNode = ReactDOM.findDOMNode(item); // https://github.com/ant-design/ant-design/issues/20465#issuecomment-569033796

        if (!isHidden(menuItemDomNode)) {
          _this.removeSelected(util_1.getValuePropValue(item));
        }

        return;
      }

      if (domEvent.type === 'click') {
        _this.removeSelected(util_1.getValuePropValue(item));
      }

      var props = _this.props;

      if (props.autoClearSearchValue) {
        _this.setInputValue('');
      }
    };

    _this.onArrowClick = function (e) {
      e.stopPropagation();
      e.preventDefault();

      if (!_this.props.disabled) {
        _this.setOpenState(!_this.state.open, {
          needFocus: !_this.state.open
        });
      }
    };

    _this.onPlaceholderClick = function () {
      if (_this.getInputDOMNode && _this.getInputDOMNode()) {
        _this.getInputDOMNode().focus();
      }
    };

    _this.onOuterFocus = function (e) {
      if (_this.props.disabled) {
        e.preventDefault();
        return;
      }

      _this.clearBlurTime(); // In IE11, onOuterFocus will be trigger twice when focus input
      // First one: e.target is div
      // Second one: e.target is input
      // other browser only trigger second one
      // https://github.com/ant-design/ant-design/issues/15942
      // Here we ignore the first one when e.target is div


      var inputNode = _this.getInputDOMNode();

      if (inputNode && e.target === _this.rootRef) {
        return;
      }

      if (!util_1.isMultipleOrTagsOrCombobox(_this.props) && e.target === inputNode) {
        return;
      }

      if (_this._focused) {
        return;
      }

      _this._focused = true;

      _this.updateFocusClassName(); // only effect multiple or tag mode


      if (!util_1.isMultipleOrTags(_this.props) || !_this._mouseDown) {
        _this.timeoutFocus();
      }
    };

    _this.onPopupFocus = function () {
      // fix ie scrollbar, focus element again
      _this.maybeFocus(true, true);
    };

    _this.onOuterBlur = function (e) {
      if (_this.props.disabled) {
        e.preventDefault();
        return;
      }

      _this.blurTimer = window.setTimeout(function () {
        _this._focused = false;

        _this.updateFocusClassName();

        var props = _this.props;
        var value = _this.state.value;
        var inputValue = _this.state.inputValue;

        if (util_1.isSingleMode(props) && props.showSearch && inputValue && props.defaultActiveFirstOption) {
          var options = _this._options || [];

          if (options.length) {
            var firstOption = util_1.findFirstMenuItem(options);

            if (firstOption) {
              value = [util_1.getValuePropValue(firstOption)];

              _this.fireChange(value);
            }
          }
        } else if (util_1.isMultipleOrTags(props) && inputValue) {
          if (_this._mouseDown) {
            // need update dropmenu when not blur
            _this.setInputValue('');
          } else {
            // why not use setState?
            // https://github.com/ant-design/ant-design/issues/14262
            _this.state.inputValue = '';

            if (_this.getInputDOMNode && _this.getInputDOMNode()) {
              _this.getInputDOMNode().value = '';
            }
          }

          var tmpValue = _this.getValueByInput(inputValue);

          if (tmpValue !== undefined) {
            value = tmpValue;

            _this.fireChange(value);
          }
        } // if click the rest space of Select in multiple mode


        if (util_1.isMultipleOrTags(props) && _this._mouseDown) {
          _this.maybeFocus(true, true);

          _this._mouseDown = false;
          return;
        }

        _this.setOpenState(false);

        if (props.onBlur) {
          props.onBlur(_this.getVLForOnChange(value));
        }
      }, 10);
    };

    _this.onClearSelection = function (event) {
      var props = _this.props;
      var state = _this.state;

      if (props.disabled) {
        return;
      }

      var inputValue = state.inputValue;
      var value = state.value;
      event.stopPropagation();

      if (inputValue || value.length) {
        if (value.length) {
          _this.fireChange([]);
        }

        _this.setOpenState(false, {
          needFocus: true
        });

        if (inputValue) {
          _this.setInputValue('');
        }
      }
    };

    _this.onChoiceAnimationLeave = function () {
      _this.forcePopupAlign();
    };

    _this.getOptionInfoBySingleValue = function (value, optionsInfo) {
      var info;
      optionsInfo = optionsInfo || _this.state.optionsInfo;

      if (optionsInfo[util_1.getMapKey(value)]) {
        info = optionsInfo[util_1.getMapKey(value)];
      }

      if (info) {
        return info;
      }

      var defaultLabel = value;

      if (_this.props.labelInValue) {
        var valueLabel = util_1.getLabelFromPropsValue(_this.props.value, value);
        var defaultValueLabel = util_1.getLabelFromPropsValue(_this.props.defaultValue, value);

        if (valueLabel !== undefined) {
          defaultLabel = valueLabel;
        } else if (defaultValueLabel !== undefined) {
          defaultLabel = defaultValueLabel;
        }
      }

      var defaultInfo = {
        option: React.createElement(Option_1["default"], {
          value: value,
          key: value
        }, value),
        value: value,
        label: defaultLabel
      };
      return defaultInfo;
    };

    _this.getOptionBySingleValue = function (value) {
      var _this$getOptionInfoBy = _this.getOptionInfoBySingleValue(value),
          option = _this$getOptionInfoBy.option;

      return option;
    };

    _this.getOptionsBySingleValue = function (values) {
      return values.map(function (value) {
        return _this.getOptionBySingleValue(value);
      });
    };

    _this.getValueByLabel = function (label) {
      if (label === undefined) {
        return null;
      }

      var value = null;
      Object.keys(_this.state.optionsInfo).forEach(function (key) {
        var info = _this.state.optionsInfo[key];
        var disabled = info.disabled;

        if (disabled) {
          return;
        }

        var oldLable = util_1.toArray(info.label);

        if (oldLable && oldLable.join('') === label) {
          value = info.value;
        }
      });
      return value;
    };

    _this.getVLBySingleValue = function (value) {
      if (_this.props.labelInValue) {
        return {
          key: value,
          label: _this.getLabelBySingleValue(value)
        };
      }

      return value;
    };

    _this.getVLForOnChange = function (vlsS) {
      var vls = vlsS;

      if (vls !== undefined) {
        if (!_this.props.labelInValue) {
          vls = vls.map(function (v) {
            return v;
          });
        } else {
          vls = vls.map(function (vl) {
            return {
              key: vl,
              label: _this.getLabelBySingleValue(vl)
            };
          });
        }

        return util_1.isMultipleOrTags(_this.props) ? vls : vls[0];
      }

      return vls;
    };

    _this.getLabelBySingleValue = function (value, optionsInfo) {
      var _this$getOptionInfoBy2 = _this.getOptionInfoBySingleValue(value, optionsInfo),
          label = _this$getOptionInfoBy2.label;

      return label;
    };

    _this.getDropdownContainer = function () {
      if (!_this.dropdownContainer) {
        _this.dropdownContainer = document.createElement('div');
        document.body.appendChild(_this.dropdownContainer);
      }

      return _this.dropdownContainer;
    };

    _this.getPlaceholderElement = function () {
      var props = _this.props;
      var state = _this.state;
      var hidden = false;

      if (state.inputValue) {
        hidden = true;
      }

      var value = state.value;

      if (value.length) {
        hidden = true;
      }

      if (util_1.isCombobox(props) && value.length === 1 && state.value && !state.value[0]) {
        hidden = false;
      }

      var placeholder = props.placeholder;

      if (placeholder) {
        return React.createElement("div", _extends({
          onMouseDown: util_1.preventDefaultEvent,
          style: _objectSpread({
            display: hidden ? 'none' : 'block'
          }, util_1.UNSELECTABLE_STYLE)
        }, util_1.UNSELECTABLE_ATTRIBUTE, {
          onClick: _this.onPlaceholderClick,
          className: "".concat(props.prefixCls, "-selection__placeholder")
        }), placeholder);
      }

      return null;
    };

    _this.getInputElement = function () {
      var props = _this.props;
      var defaultInput = React.createElement("input", {
        id: props.id,
        autoComplete: "off"
      }); // tslint:disable-next-line:typedef-whitespace

      var inputElement = props.getInputElement ? props.getInputElement() : defaultInput;
      var inputCls = classnames_1["default"](inputElement.props.className, _defineProperty({}, "".concat(props.prefixCls, "-search__field"), true)); // https://github.com/ant-design/ant-design/issues/4992#issuecomment-281542159
      // Add space to the end of the inputValue as the width measurement tolerance

      return React.createElement("div", {
        className: "".concat(props.prefixCls, "-search__field__wrap")
      }, React.cloneElement(inputElement, {
        ref: _this.saveInputRef,
        onChange: _this.onInputChange,
        onKeyDown: chaining(_this.onInputKeyDown, inputElement.props.onKeyDown, _this.props.onInputKeyDown),
        value: _this.state.inputValue,
        disabled: props.disabled,
        className: inputCls
      }), React.createElement("span", {
        ref: _this.saveInputMirrorRef,
        className: "".concat(props.prefixCls, "-search__field__mirror")
      }, _this.state.inputValue, "\xA0"));
    };

    _this.getInputDOMNode = function () {
      return _this.topCtrlRef ? _this.topCtrlRef.querySelector('input,textarea,div[contentEditable]') : _this.inputRef;
    };

    _this.getInputMirrorDOMNode = function () {
      return _this.inputMirrorRef;
    };

    _this.getPopupDOMNode = function () {
      if (_this.selectTriggerRef) {
        return _this.selectTriggerRef.getPopupDOMNode();
      }
    };

    _this.getPopupMenuComponent = function () {
      if (_this.selectTriggerRef) {
        return _this.selectTriggerRef.getInnerMenu();
      }
    };

    _this.setOpenState = function (open) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var needFocus = config.needFocus,
          fireSearch = config.fireSearch;
      var props = _this.props;
      var state = _this.state;

      if (state.open === open) {
        _this.maybeFocus(open, !!needFocus);

        return;
      }

      if (_this.props.onDropdownVisibleChange) {
        _this.props.onDropdownVisibleChange(open);
      }

      var nextState = {
        open: open,
        backfillValue: ''
      }; // clear search input value when open is false in singleMode.
      // https://github.com/ant-design/ant-design/issues/16572

      if (!open && util_1.isSingleMode(props) && props.showSearch) {
        _this.setInputValue('', fireSearch);
      }

      if (!open) {
        _this.maybeFocus(open, !!needFocus);
      }

      _this.setState(_objectSpread({
        open: open
      }, nextState), function () {
        if (open) {
          _this.maybeFocus(open, !!needFocus);
        }
      });
    };

    _this.setInputValue = function (inputValue) {
      var fireSearch = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
      var onSearch = _this.props.onSearch;

      if (inputValue !== _this.state.inputValue) {
        _this.setState(function (prevState) {
          // Additional check if `inputValue` changed in latest state.
          if (fireSearch && inputValue !== prevState.inputValue && onSearch) {
            onSearch(inputValue);
          }

          return {
            inputValue: inputValue
          };
        }, _this.forcePopupAlign);
      }
    };

    _this.getValueByInput = function (str) {
      var _this$props2 = _this.props,
          multiple = _this$props2.multiple,
          tokenSeparators = _this$props2.tokenSeparators;
      var nextValue = _this.state.value;
      var hasNewValue = false;
      util_1.splitBySeparators(str, tokenSeparators).forEach(function (label) {
        var selectedValue = [label];

        if (multiple) {
          var value = _this.getValueByLabel(label);

          if (value && util_1.findIndexInValueBySingleValue(nextValue, value) === -1) {
            nextValue = nextValue.concat(value);
            hasNewValue = true;

            _this.fireSelect(value);
          }
        } else if (util_1.findIndexInValueBySingleValue(nextValue, label) === -1) {
          nextValue = nextValue.concat(selectedValue);
          hasNewValue = true;

          _this.fireSelect(label);
        }
      });
      return hasNewValue ? nextValue : undefined;
    };

    _this.getRealOpenState = function (state) {
      // tslint:disable-next-line:variable-name
      var _open = _this.props.open;

      if (typeof _open === 'boolean') {
        return _open;
      }

      var open = (state || _this.state).open;
      var options = _this._options || [];

      if (util_1.isMultipleOrTagsOrCombobox(_this.props) || !_this.props.showSearch) {
        if (open && !options.length) {
          open = false;
        }
      }

      return open;
    };

    _this.markMouseDown = function () {
      _this._mouseDown = true;
    };

    _this.markMouseLeave = function () {
      _this._mouseDown = false;
    };

    _this.handleBackfill = function (item) {
      if (!_this.props.backfill || !(util_1.isSingleMode(_this.props) || util_1.isCombobox(_this.props))) {
        return;
      }

      var key = util_1.getValuePropValue(item);

      if (util_1.isCombobox(_this.props)) {
        _this.setInputValue(key, false);
      }

      _this.setState({
        value: [key],
        backfillValue: key
      });
    };

    _this.filterOption = function (input, child) {
      var defaultFilter = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : util_1.defaultFilterFn;
      var value = _this.state.value;
      var lastValue = value[value.length - 1];

      if (!input || lastValue && lastValue === _this.state.backfillValue) {
        return true;
      }

      var filterFn = _this.props.filterOption;

      if ('filterOption' in _this.props) {
        if (filterFn === true) {
          filterFn = defaultFilter.bind(_assertThisInitialized(_this));
        }
      } else {
        filterFn = defaultFilter.bind(_assertThisInitialized(_this));
      }

      if (!filterFn) {
        return true;
      } else if (typeof filterFn === 'function') {
        return filterFn.call(_assertThisInitialized(_this), input, child);
      } else if (child.props.disabled) {
        return false;
      }

      return true;
    };

    _this.timeoutFocus = function () {
      var onFocus = _this.props.onFocus;

      if (_this.focusTimer) {
        _this.clearFocusTime();
      }

      _this.focusTimer = window.setTimeout(function () {
        if (onFocus) {
          onFocus();
        }
      }, 10);
    };

    _this.clearFocusTime = function () {
      if (_this.focusTimer) {
        clearTimeout(_this.focusTimer);
        _this.focusTimer = null;
      }
    };

    _this.clearBlurTime = function () {
      if (_this.blurTimer) {
        clearTimeout(_this.blurTimer);
        _this.blurTimer = null;
      }
    };

    _this.clearComboboxTime = function () {
      if (_this.comboboxTimer) {
        clearTimeout(_this.comboboxTimer);
        _this.comboboxTimer = null;
      }
    };

    _this.updateFocusClassName = function () {
      var rootRef = _this.rootRef;
      var props = _this.props; // avoid setState and its side effect

      if (_this._focused) {
        component_classes_1["default"](rootRef).add("".concat(props.prefixCls, "-focused"));
      } else {
        component_classes_1["default"](rootRef).remove("".concat(props.prefixCls, "-focused"));
      }
    };

    _this.maybeFocus = function (open, needFocus) {
      if (needFocus || open) {
        var input = _this.getInputDOMNode();

        var _document = document,
            activeElement = _document.activeElement;

        if (input && (open || util_1.isMultipleOrTagsOrCombobox(_this.props))) {
          if (activeElement !== input) {
            input.focus();
            _this._focused = true;
          }
        } else if (activeElement !== _this.selectionRef && _this.selectionRef) {
          _this.selectionRef.focus();

          _this._focused = true;
        }
      }
    };

    _this.removeSelected = function (selectedKey, e) {
      var props = _this.props;

      if (props.disabled || _this.isChildDisabled(selectedKey)) {
        return;
      } // Do not trigger Trigger popup


      if (e && e.stopPropagation) {
        e.stopPropagation();
      }

      var oldValue = _this.state.value;
      var value = oldValue.filter(function (singleValue) {
        return singleValue !== selectedKey;
      });
      var canMultiple = util_1.isMultipleOrTags(props);

      if (canMultiple) {
        var event = selectedKey;

        if (props.labelInValue) {
          event = {
            key: selectedKey,
            label: _this.getLabelBySingleValue(selectedKey)
          };
        }

        if (props.onDeselect) {
          props.onDeselect(event, _this.getOptionBySingleValue(selectedKey));
        }
      }

      _this.fireChange(value);
    };

    _this.openIfHasChildren = function () {
      var props = _this.props;

      if (React.Children.count(props.children) || util_1.isSingleMode(props)) {
        _this.setOpenState(true);
      }
    };

    _this.fireSelect = function (value) {
      if (_this.props.onSelect) {
        _this.props.onSelect(_this.getVLBySingleValue(value), _this.getOptionBySingleValue(value));
      }
    };

    _this.fireChange = function (value) {
      var props = _this.props;

      if (!('value' in props)) {
        _this.setState({
          value: value
        }, _this.forcePopupAlign);
      }

      var vls = _this.getVLForOnChange(value);

      var options = _this.getOptionsBySingleValue(value);

      if (props.onChange) {
        props.onChange(vls, util_1.isMultipleOrTags(_this.props) ? options : options[0]);
      }
    };

    _this.isChildDisabled = function (key) {
      return toArray_1["default"](_this.props.children).some(function (child) {
        var childValue = util_1.getValuePropValue(child);
        return childValue === key && child.props && child.props.disabled;
      });
    };

    _this.forcePopupAlign = function () {
      if (!_this.state.open) {
        return;
      }

      if (_this.selectTriggerRef && _this.selectTriggerRef.triggerRef) {
        _this.selectTriggerRef.triggerRef.forcePopupAlign();
      }
    };

    _this.renderFilterOptions = function () {
      var inputValue = _this.state.inputValue;
      var _this$props3 = _this.props,
          children = _this$props3.children,
          tags = _this$props3.tags,
          notFoundContent = _this$props3.notFoundContent;
      var menuItems = [];
      var childrenKeys = [];
      var empty = false;

      var options = _this.renderFilterOptionsFromChildren(children, childrenKeys, menuItems);

      if (tags) {
        // tags value must be string
        var value = _this.state.value;
        value = value.filter(function (singleValue) {
          return childrenKeys.indexOf(singleValue) === -1 && (!inputValue || String(singleValue).indexOf(String(inputValue)) > -1);
        }); // sort by length

        value.sort(function (val1, val2) {
          return val1.length - val2.length;
        });
        value.forEach(function (singleValue) {
          var key = singleValue;
          var menuItem = React.createElement(rc_menu_1.Item, {
            style: util_1.UNSELECTABLE_STYLE,
            role: "option",
            attribute: util_1.UNSELECTABLE_ATTRIBUTE,
            value: key,
            key: key
          }, key);
          options.push(menuItem);
          menuItems.push(menuItem);
        }); // ref: https://github.com/ant-design/ant-design/issues/14090

        if (inputValue && menuItems.every(function (option) {
          return util_1.getValuePropValue(option) !== inputValue;
        })) {
          options.unshift(React.createElement(rc_menu_1.Item, {
            style: util_1.UNSELECTABLE_STYLE,
            role: "option",
            attribute: util_1.UNSELECTABLE_ATTRIBUTE,
            value: inputValue,
            key: inputValue
          }, inputValue));
        }
      }

      if (!options.length && notFoundContent) {
        empty = true;
        options = [React.createElement(rc_menu_1.Item, {
          style: util_1.UNSELECTABLE_STYLE,
          attribute: util_1.UNSELECTABLE_ATTRIBUTE,
          disabled: true,
          role: "option",
          value: "NOT_FOUND",
          key: "NOT_FOUND"
        }, notFoundContent)];
      }

      return {
        empty: empty,
        options: options
      };
    };

    _this.renderFilterOptionsFromChildren = function (children, childrenKeys, menuItems) {
      var sel = [];
      var props = _this.props;
      var inputValue = _this.state.inputValue;
      var tags = props.tags;
      React.Children.forEach(children, function (child) {
        if (!child) {
          return;
        }

        var type = child.type;

        if (type.isSelectOptGroup) {
          var label = child.props.label;
          var key = child.key;

          if (!key && typeof label === 'string') {
            key = label;
          } else if (!label && key) {
            label = key;
          } // Match option group label


          if (inputValue && _this.filterOption(inputValue, child)) {
            var innerItems = toArray_1["default"](child.props.children).map(function (subChild) {
              var childValueSub = util_1.getValuePropValue(subChild) || subChild.key;
              return React.createElement(rc_menu_1.Item, _extends({
                key: childValueSub,
                value: childValueSub
              }, subChild.props));
            });
            sel.push(React.createElement(rc_menu_1.ItemGroup, {
              key: key,
              title: label
            }, innerItems)); // Not match
          } else {
            var _innerItems = _this.renderFilterOptionsFromChildren(child.props.children, childrenKeys, menuItems);

            if (_innerItems.length) {
              sel.push(React.createElement(rc_menu_1.ItemGroup, {
                key: key,
                title: label
              }, _innerItems));
            }
          }

          return;
        }

        warning_1["default"](type.isSelectOption, 'the children of `Select` should be `Select.Option` or `Select.OptGroup`, ' + "instead of `".concat(type.name || type.displayName || child.type, "`."));
        var childValue = util_1.getValuePropValue(child);
        util_1.validateOptionValue(childValue, _this.props);

        if (_this.filterOption(inputValue, child)) {
          var menuItem = React.createElement(rc_menu_1.Item, _extends({
            style: util_1.UNSELECTABLE_STYLE,
            attribute: util_1.UNSELECTABLE_ATTRIBUTE,
            value: childValue,
            key: childValue,
            role: "option"
          }, child.props));
          sel.push(menuItem);
          menuItems.push(menuItem);
        }

        if (tags) {
          childrenKeys.push(childValue);
        }
      });
      return sel;
    };

    _this.renderTopControlNode = function () {
      var _this$state = _this.state,
          open = _this$state.open,
          inputValue = _this$state.inputValue;
      var value = _this.state.value;
      var props = _this.props;
      var choiceTransitionName = props.choiceTransitionName,
          prefixCls = props.prefixCls,
          maxTagTextLength = props.maxTagTextLength,
          maxTagCount = props.maxTagCount,
          showSearch = props.showSearch,
          removeIcon = props.removeIcon;
      var maxTagPlaceholder = props.maxTagPlaceholder;
      var className = "".concat(prefixCls, "-selection__rendered"); // search input is inside topControlNode in single, multiple & combobox. 2016/04/13

      var innerNode = null;

      if (util_1.isSingleMode(props)) {
        var selectedValue = null;

        if (value.length) {
          var showSelectedValue = false;
          var opacity = 1;

          if (!showSearch) {
            showSelectedValue = true;
          } else if (open) {
            showSelectedValue = !inputValue;

            if (showSelectedValue) {
              opacity = 0.4;
            }
          } else {
            showSelectedValue = true;
          }

          var singleValue = value[0];

          var _this$getOptionInfoBy3 = _this.getOptionInfoBySingleValue(singleValue),
              label = _this$getOptionInfoBy3.label,
              title = _this$getOptionInfoBy3.title;

          selectedValue = React.createElement("div", {
            key: "value",
            className: "".concat(prefixCls, "-selection-selected-value"),
            title: util_1.toTitle(title || label),
            style: {
              display: showSelectedValue ? 'block' : 'none',
              opacity: opacity
            }
          }, label);
        }

        if (!showSearch) {
          innerNode = [selectedValue];
        } else {
          innerNode = [selectedValue, React.createElement("div", {
            className: "".concat(prefixCls, "-search ").concat(prefixCls, "-search--inline"),
            key: "input",
            style: {
              display: open ? 'block' : 'none'
            }
          }, _this.getInputElement())];
        }
      } else {
        var selectedValueNodes = [];
        var limitedCountValue = value;
        var maxTagPlaceholderEl;

        if (maxTagCount !== undefined && value.length > maxTagCount) {
          limitedCountValue = limitedCountValue.slice(0, maxTagCount);

          var omittedValues = _this.getVLForOnChange(value.slice(maxTagCount, value.length));

          var content = "+ ".concat(value.length - maxTagCount, " ...");

          if (maxTagPlaceholder) {
            content = typeof maxTagPlaceholder === 'function' ? maxTagPlaceholder(omittedValues) : maxTagPlaceholder;
          }

          maxTagPlaceholderEl = React.createElement("li", _extends({
            style: util_1.UNSELECTABLE_STYLE
          }, util_1.UNSELECTABLE_ATTRIBUTE, {
            role: "presentation",
            onMouseDown: util_1.preventDefaultEvent,
            className: "".concat(prefixCls, "-selection__choice ").concat(prefixCls, "-selection__choice__disabled"),
            key: "maxTagPlaceholder",
            title: util_1.toTitle(content)
          }), React.createElement("div", {
            className: "".concat(prefixCls, "-selection__choice__content")
          }, content));
        }

        if (util_1.isMultipleOrTags(props)) {
          selectedValueNodes = limitedCountValue.map(function (singleValue) {
            var info = _this.getOptionInfoBySingleValue(singleValue);

            var content = info.label;
            var title = info.title || content;

            if (maxTagTextLength && typeof content === 'string' && content.length > maxTagTextLength) {
              content = "".concat(content.slice(0, maxTagTextLength), "...");
            }

            var disabled = _this.isChildDisabled(singleValue);

            var choiceClassName = disabled ? "".concat(prefixCls, "-selection__choice ").concat(prefixCls, "-selection__choice__disabled") : "".concat(prefixCls, "-selection__choice");
            return React.createElement("li", _extends({
              style: util_1.UNSELECTABLE_STYLE
            }, util_1.UNSELECTABLE_ATTRIBUTE, {
              onMouseDown: util_1.preventDefaultEvent,
              className: choiceClassName,
              role: "presentation",
              key: singleValue || SELECT_EMPTY_VALUE_KEY,
              title: util_1.toTitle(title)
            }), React.createElement("div", {
              className: "".concat(prefixCls, "-selection__choice__content")
            }, content), disabled ? null : React.createElement("span", {
              onClick: function onClick(event) {
                _this.removeSelected(singleValue, event);
              },
              className: "".concat(prefixCls, "-selection__choice__remove")
            }, removeIcon || React.createElement("i", {
              className: "".concat(prefixCls, "-selection__choice__remove-icon")
            }, "\xD7")));
          });
        }

        if (maxTagPlaceholderEl) {
          selectedValueNodes.push(maxTagPlaceholderEl);
        }

        selectedValueNodes.push(React.createElement("li", {
          className: "".concat(prefixCls, "-search ").concat(prefixCls, "-search--inline"),
          key: "__input"
        }, _this.getInputElement()));

        if (util_1.isMultipleOrTags(props) && choiceTransitionName) {
          innerNode = React.createElement(rc_animate_1["default"], {
            onLeave: _this.onChoiceAnimationLeave,
            component: "ul",
            transitionName: choiceTransitionName
          }, selectedValueNodes);
        } else {
          innerNode = React.createElement("ul", null, selectedValueNodes);
        }
      }

      return React.createElement("div", {
        className: className,
        ref: _this.saveTopCtrlRef
      }, _this.getPlaceholderElement(), innerNode);
    };

    var optionsInfo = Select.getOptionsInfoFromProps(props);

    if (props.tags && typeof props.filterOption !== 'function') {
      var isDisabledExist = Object.keys(optionsInfo).some(function (key) {
        return optionsInfo[key].disabled;
      });
      warning_1["default"](!isDisabledExist, 'Please avoid setting option to disabled in tags mode since user can always type text as tag.');
    }

    _this.state = {
      value: Select.getValueFromProps(props, true),
      inputValue: props.combobox ? Select.getInputValueForCombobox(props, optionsInfo, true) : '',
      open: props.defaultOpen,
      optionsInfo: optionsInfo,
      backfillValue: '',
      // a flag for aviod redundant getOptionsInfoFromProps call
      skipBuildOptionsInfo: true,
      ariaId: ''
    };
    _this.saveInputRef = util_1.saveRef(_assertThisInitialized(_this), 'inputRef');
    _this.saveInputMirrorRef = util_1.saveRef(_assertThisInitialized(_this), 'inputMirrorRef');
    _this.saveTopCtrlRef = util_1.saveRef(_assertThisInitialized(_this), 'topCtrlRef');
    _this.saveSelectTriggerRef = util_1.saveRef(_assertThisInitialized(_this), 'selectTriggerRef');
    _this.saveRootRef = util_1.saveRef(_assertThisInitialized(_this), 'rootRef');
    _this.saveSelectionRef = util_1.saveRef(_assertThisInitialized(_this), 'selectionRef');
    return _this;
  }

  _createClass(Select, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      // when defaultOpen is true, we should auto focus search input
      // https://github.com/ant-design/ant-design/issues/14254
      if (this.props.autoFocus || this.state.open) {
        this.focus();
      }

      this.setState({
        ariaId: util_1.generateUUID()
      });
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (util_1.isMultipleOrTags(this.props)) {
        var inputNode = this.getInputDOMNode();
        var mirrorNode = this.getInputMirrorDOMNode();

        if (inputNode && inputNode.value && mirrorNode) {
          inputNode.style.width = '';
          inputNode.style.width = "".concat(mirrorNode.clientWidth, "px");
        } else if (inputNode) {
          inputNode.style.width = '';
        }
      }

      this.forcePopupAlign();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.clearFocusTime();
      this.clearBlurTime();
      this.clearComboboxTime();

      if (this.dropdownContainer) {
        ReactDOM.unmountComponentAtNode(this.dropdownContainer);
        document.body.removeChild(this.dropdownContainer);
        this.dropdownContainer = null;
      }
    }
  }, {
    key: "focus",
    value: function focus() {
      if (util_1.isSingleMode(this.props) && this.selectionRef) {
        this.selectionRef.focus();
      } else if (this.getInputDOMNode()) {
        this.getInputDOMNode().focus();
      }
    }
  }, {
    key: "blur",
    value: function blur() {
      if (util_1.isSingleMode(this.props) && this.selectionRef) {
        this.selectionRef.blur();
      } else if (this.getInputDOMNode()) {
        this.getInputDOMNode().blur();
      }
    }
  }, {
    key: "renderArrow",
    value: function renderArrow(multiple) {
      // showArrow : Set to true if not multiple by default but keep set value.
      var _this$props4 = this.props,
          _this$props4$showArro = _this$props4.showArrow,
          showArrow = _this$props4$showArro === void 0 ? !multiple : _this$props4$showArro,
          loading = _this$props4.loading,
          inputIcon = _this$props4.inputIcon,
          prefixCls = _this$props4.prefixCls;

      if (!showArrow && !loading) {
        return null;
      } // if loading  have loading icon


      var defaultIcon = loading ? React.createElement("i", {
        className: "".concat(prefixCls, "-arrow-loading")
      }) : React.createElement("i", {
        className: "".concat(prefixCls, "-arrow-icon")
      });
      return React.createElement("span", _extends({
        key: "arrow",
        className: "".concat(prefixCls, "-arrow"),
        style: util_1.UNSELECTABLE_STYLE
      }, util_1.UNSELECTABLE_ATTRIBUTE, {
        onClick: this.onArrowClick
      }), inputIcon || defaultIcon);
    }
  }, {
    key: "renderClear",
    value: function renderClear() {
      var _this$props5 = this.props,
          prefixCls = _this$props5.prefixCls,
          allowClear = _this$props5.allowClear,
          clearIcon = _this$props5.clearIcon;
      var inputValue = this.state.inputValue;
      var value = this.state.value;
      var clear = React.createElement("span", _extends({
        key: "clear",
        className: "".concat(prefixCls, "-selection__clear"),
        onMouseDown: util_1.preventDefaultEvent,
        style: util_1.UNSELECTABLE_STYLE
      }, util_1.UNSELECTABLE_ATTRIBUTE, {
        onClick: this.onClearSelection
      }), clearIcon || React.createElement("i", {
        className: "".concat(prefixCls, "-selection__clear-icon")
      }, "\xD7"));

      if (!allowClear) {
        return null;
      }

      if (util_1.isCombobox(this.props)) {
        if (inputValue) {
          return clear;
        }

        return null;
      }

      if (inputValue || value.length) {
        return clear;
      }

      return null;
    }
  }, {
    key: "render",
    value: function render() {
      var _rootCls;

      var props = this.props;
      var multiple = util_1.isMultipleOrTags(props); // Default set showArrow to true if not set (not set directly in defaultProps to handle multiple case)

      var _props$showArrow = props.showArrow,
          showArrow = _props$showArrow === void 0 ? true : _props$showArrow;
      var state = this.state;
      var className = props.className,
          disabled = props.disabled,
          prefixCls = props.prefixCls,
          loading = props.loading;
      var ctrlNode = this.renderTopControlNode();
      var _this$state2 = this.state,
          open = _this$state2.open,
          ariaId = _this$state2.ariaId;

      if (open) {
        var filterOptions = this.renderFilterOptions();
        this._empty = filterOptions.empty;
        this._options = filterOptions.options;
      }

      var realOpen = this.getRealOpenState();
      var empty = this._empty;
      var options = this._options || [];
      var dataOrAriaAttributeProps = {};
      Object.keys(props).forEach(function (key) {
        if (Object.prototype.hasOwnProperty.call(props, key) && (key.substr(0, 5) === 'data-' || key.substr(0, 5) === 'aria-' || key === 'role')) {
          dataOrAriaAttributeProps[key] = props[key];
        }
      }); // for (const key in props) {
      //   if (
      //     Object.prototype.hasOwnProperty.call(props, key) &&
      //     (key.substr(0, 5) === 'data-' || key.substr(0, 5) === 'aria-' || key === 'role')
      //   ) {
      //     dataOrAriaAttributeProps[key] = props[key];
      //   }
      // }

      var extraSelectionProps = _objectSpread({}, dataOrAriaAttributeProps);

      if (!util_1.isMultipleOrTagsOrCombobox(props)) {
        extraSelectionProps = _objectSpread({}, extraSelectionProps, {
          onKeyDown: this.onKeyDown,
          tabIndex: props.disabled ? -1 : props.tabIndex
        });
      }

      var rootCls = (_rootCls = {}, _defineProperty(_rootCls, className, !!className), _defineProperty(_rootCls, prefixCls, 1), _defineProperty(_rootCls, "".concat(prefixCls, "-open"), open), _defineProperty(_rootCls, "".concat(prefixCls, "-focused"), open || !!this._focused), _defineProperty(_rootCls, "".concat(prefixCls, "-combobox"), util_1.isCombobox(props)), _defineProperty(_rootCls, "".concat(prefixCls, "-disabled"), disabled), _defineProperty(_rootCls, "".concat(prefixCls, "-enabled"), !disabled), _defineProperty(_rootCls, "".concat(prefixCls, "-allow-clear"), !!props.allowClear), _defineProperty(_rootCls, "".concat(prefixCls, "-no-arrow"), !showArrow), _defineProperty(_rootCls, "".concat(prefixCls, "-loading"), !!loading), _rootCls);
      return React.createElement(SelectTrigger_1["default"], {
        onPopupFocus: this.onPopupFocus,
        onMouseEnter: this.props.onMouseEnter,
        onMouseLeave: this.props.onMouseLeave,
        dropdownAlign: props.dropdownAlign,
        dropdownClassName: props.dropdownClassName,
        dropdownMatchSelectWidth: props.dropdownMatchSelectWidth,
        defaultActiveFirstOption: props.defaultActiveFirstOption,
        dropdownMenuStyle: props.dropdownMenuStyle,
        transitionName: props.transitionName,
        animation: props.animation,
        prefixCls: props.prefixCls,
        dropdownStyle: props.dropdownStyle,
        combobox: props.combobox,
        showSearch: props.showSearch,
        options: options,
        empty: empty,
        multiple: multiple,
        disabled: disabled,
        visible: realOpen,
        inputValue: state.inputValue,
        value: state.value,
        backfillValue: state.backfillValue,
        firstActiveValue: props.firstActiveValue,
        onDropdownVisibleChange: this.onDropdownVisibleChange,
        getPopupContainer: props.getPopupContainer,
        onMenuSelect: this.onMenuSelect,
        onMenuDeselect: this.onMenuDeselect,
        onPopupScroll: props.onPopupScroll,
        showAction: props.showAction,
        ref: this.saveSelectTriggerRef,
        menuItemSelectedIcon: props.menuItemSelectedIcon,
        dropdownRender: props.dropdownRender,
        ariaId: ariaId
      }, React.createElement("div", {
        id: props.id,
        style: props.style,
        ref: this.saveRootRef,
        onBlur: this.onOuterBlur,
        onFocus: this.onOuterFocus,
        className: classnames_1["default"](rootCls),
        onMouseDown: this.markMouseDown,
        onMouseUp: this.markMouseLeave,
        onMouseOut: this.markMouseLeave
      }, React.createElement("div", _extends({
        ref: this.saveSelectionRef,
        key: "selection",
        className: "".concat(prefixCls, "-selection\n            ").concat(prefixCls, "-selection--").concat(multiple ? 'multiple' : 'single'),
        role: "combobox",
        "aria-autocomplete": "list",
        "aria-haspopup": "true",
        "aria-controls": ariaId,
        "aria-expanded": realOpen
      }, extraSelectionProps), ctrlNode, this.renderClear(), this.renderArrow(!!multiple))));
    }
  }]);

  return Select;
}(React.Component);

Select.propTypes = PropTypes_1["default"];
Select.defaultProps = {
  prefixCls: 'rc-select',
  defaultOpen: false,
  labelInValue: false,
  defaultActiveFirstOption: true,
  showSearch: true,
  allowClear: false,
  placeholder: '',
  onChange: noop,
  onFocus: noop,
  onBlur: noop,
  onSelect: noop,
  onSearch: noop,
  onDeselect: noop,
  onInputKeyDown: noop,
  dropdownMatchSelectWidth: true,
  dropdownStyle: {},
  dropdownMenuStyle: {},
  optionFilterProp: 'value',
  optionLabelProp: 'value',
  notFoundContent: 'Not Found',
  backfill: false,
  showAction: ['click'],
  tokenSeparators: [],
  autoClearSearchValue: true,
  tabIndex: 0,
  dropdownRender: function dropdownRender(menu) {
    return menu;
  }
};

Select.getDerivedStateFromProps = function (nextProps, prevState) {
  var optionsInfo = prevState.skipBuildOptionsInfo ? prevState.optionsInfo : Select.getOptionsInfoFromProps(nextProps, prevState);
  var newState = {
    optionsInfo: optionsInfo,
    skipBuildOptionsInfo: false
  };

  if ('open' in nextProps) {
    newState.open = nextProps.open;
  }

  if ('value' in nextProps) {
    var value = Select.getValueFromProps(nextProps);
    newState.value = value;

    if (nextProps.combobox) {
      newState.inputValue = Select.getInputValueForCombobox(nextProps, optionsInfo);
    }
  }

  return newState;
};

Select.getOptionsFromChildren = function (children) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  React.Children.forEach(children, function (child) {
    if (!child) {
      return;
    }

    var type = child.type;

    if (type.isSelectOptGroup) {
      Select.getOptionsFromChildren(child.props.children, options);
    } else {
      options.push(child);
    }
  });
  return options;
};

Select.getInputValueForCombobox = function (props, optionsInfo, useDefaultValue) {
  var value = [];

  if ('value' in props && !useDefaultValue) {
    value = util_1.toArray(props.value);
  }

  if ('defaultValue' in props && useDefaultValue) {
    value = util_1.toArray(props.defaultValue);
  }

  if (value.length) {
    value = value[0];
  } else {
    return '';
  }

  var label = value;

  if (props.labelInValue) {
    label = value.label;
  } else if (optionsInfo[util_1.getMapKey(value)]) {
    label = optionsInfo[util_1.getMapKey(value)].label;
  }

  if (label === undefined) {
    label = '';
  }

  return label;
};

Select.getLabelFromOption = function (props, option) {
  return util_1.getPropValue(option, props.optionLabelProp);
};

Select.getOptionsInfoFromProps = function (props, preState) {
  var options = Select.getOptionsFromChildren(props.children);
  var optionsInfo = {};
  options.forEach(function (option) {
    var singleValue = util_1.getValuePropValue(option);
    optionsInfo[util_1.getMapKey(singleValue)] = {
      option: option,
      value: singleValue,
      label: Select.getLabelFromOption(props, option),
      title: option.props.title,
      disabled: option.props.disabled
    };
  });

  if (preState) {
    // keep option info in pre state value.
    var oldOptionsInfo = preState.optionsInfo;
    var value = preState.value;

    if (value) {
      value.forEach(function (v) {
        var key = util_1.getMapKey(v);

        if (!optionsInfo[key] && oldOptionsInfo[key] !== undefined) {
          optionsInfo[key] = oldOptionsInfo[key];
        }
      });
    }
  }

  return optionsInfo;
};

Select.getValueFromProps = function (props, useDefaultValue) {
  var value = [];

  if ('value' in props && !useDefaultValue) {
    value = util_1.toArray(props.value);
  }

  if ('defaultValue' in props && useDefaultValue) {
    value = util_1.toArray(props.defaultValue);
  }

  if (props.labelInValue) {
    value = value.map(function (v) {
      return v.key;
    });
  }

  return value;
};

Select.displayName = 'Select';
react_lifecycles_compat_1.polyfill(Select);
exports["default"] = Select;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtc2VsZWN0L2VzL1NlbGVjdC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXNlbGVjdC9lcy9TZWxlY3QuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG5cbmZ1bmN0aW9uIG93bktleXMob2JqZWN0LCBlbnVtZXJhYmxlT25seSkgeyB2YXIga2V5cyA9IE9iamVjdC5rZXlzKG9iamVjdCk7IGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKSB7IHZhciBzeW1ib2xzID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhvYmplY3QpOyBpZiAoZW51bWVyYWJsZU9ubHkpIHN5bWJvbHMgPSBzeW1ib2xzLmZpbHRlcihmdW5jdGlvbiAoc3ltKSB7IHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKG9iamVjdCwgc3ltKS5lbnVtZXJhYmxlOyB9KTsga2V5cy5wdXNoLmFwcGx5KGtleXMsIHN5bWJvbHMpOyB9IHJldHVybiBrZXlzOyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RTcHJlYWQodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV0gIT0gbnVsbCA/IGFyZ3VtZW50c1tpXSA6IHt9OyBpZiAoaSAlIDIpIHsgb3duS2V5cyhPYmplY3Qoc291cmNlKSwgdHJ1ZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IF9kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgc291cmNlW2tleV0pOyB9KTsgfSBlbHNlIGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycykgeyBPYmplY3QuZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKHNvdXJjZSkpOyB9IGVsc2UgeyBvd25LZXlzKE9iamVjdChzb3VyY2UpKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHNvdXJjZSwga2V5KSk7IH0pOyB9IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7IGlmIChrZXkgaW4gb2JqKSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgeyB2YWx1ZTogdmFsdWUsIGVudW1lcmFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSwgd3JpdGFibGU6IHRydWUgfSk7IH0gZWxzZSB7IG9ialtrZXldID0gdmFsdWU7IH0gcmV0dXJuIG9iajsgfVxuXG5mdW5jdGlvbiBfZXh0ZW5kcygpIHsgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9OyByZXR1cm4gX2V4dGVuZHMuYXBwbHkodGhpcywgYXJndW1lbnRzKTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9XG5cbmZ1bmN0aW9uIF9jcmVhdGVDbGFzcyhDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpKSB7IHJldHVybiBjYWxsOyB9IHJldHVybiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpOyB9XG5cbmZ1bmN0aW9uIF9nZXRQcm90b3R5cGVPZihvKSB7IF9nZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5nZXRQcm90b3R5cGVPZiA6IGZ1bmN0aW9uIF9nZXRQcm90b3R5cGVPZihvKSB7IHJldHVybiBvLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2Yobyk7IH07IHJldHVybiBfZ2V0UHJvdG90eXBlT2Yobyk7IH1cblxuZnVuY3Rpb24gX2Fzc2VydFRoaXNJbml0aWFsaXplZChzZWxmKSB7IGlmIChzZWxmID09PSB2b2lkIDApIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvblwiKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBfc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpOyB9XG5cbmZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7IF9zZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fCBmdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBvLl9fcHJvdG9fXyA9IHA7IHJldHVybiBvOyB9OyByZXR1cm4gX3NldFByb3RvdHlwZU9mKG8sIHApOyB9XG5cbnZhciBfX2ltcG9ydERlZmF1bHQgPSB0aGlzICYmIHRoaXMuX19pbXBvcnREZWZhdWx0IHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgcmV0dXJuIG1vZCAmJiBtb2QuX19lc01vZHVsZSA/IG1vZCA6IHtcbiAgICBcImRlZmF1bHRcIjogbW9kXG4gIH07XG59O1xuXG52YXIgX19pbXBvcnRTdGFyID0gdGhpcyAmJiB0aGlzLl9faW1wb3J0U3RhciB8fCBmdW5jdGlvbiAobW9kKSB7XG4gIGlmIChtb2QgJiYgbW9kLl9fZXNNb2R1bGUpIHJldHVybiBtb2Q7XG4gIHZhciByZXN1bHQgPSB7fTtcbiAgaWYgKG1vZCAhPSBudWxsKSBmb3IgKHZhciBrIGluIG1vZCkge1xuICAgIGlmIChPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtb2QsIGspKSByZXN1bHRba10gPSBtb2Rba107XG4gIH1cbiAgcmVzdWx0W1wiZGVmYXVsdFwiXSA9IG1vZDtcbiAgcmV0dXJuIHJlc3VsdDtcbn07XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBjbGFzc25hbWVzXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcImNsYXNzbmFtZXNcIikpO1xuXG52YXIgY29tcG9uZW50X2NsYXNzZXNfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwiY29tcG9uZW50LWNsYXNzZXNcIikpO1xuXG52YXIgcmNfYW5pbWF0ZV8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJyYy1hbmltYXRlXCIpKTtcblxudmFyIHJjX21lbnVfMSA9IHJlcXVpcmUoXCJyYy1tZW51XCIpO1xuXG52YXIgdG9BcnJheV8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJyYy11dGlsL2xpYi9DaGlsZHJlbi90b0FycmF5XCIpKTtcblxudmFyIEtleUNvZGVfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwicmMtdXRpbC9saWIvS2V5Q29kZVwiKSk7XG5cbnZhciBSZWFjdCA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicmVhY3RcIikpO1xuXG52YXIgUmVhY3RET00gPSBfX2ltcG9ydFN0YXIocmVxdWlyZShcInJlYWN0LWRvbVwiKSk7XG5cbnZhciByZWFjdF9saWZlY3ljbGVzX2NvbXBhdF8xID0gcmVxdWlyZShcInJlYWN0LWxpZmVjeWNsZXMtY29tcGF0XCIpO1xuXG52YXIgd2FybmluZ18xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJ3YXJuaW5nXCIpKTtcblxudmFyIE9wdGlvbl8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCIuL09wdGlvblwiKSk7IC8vIFdoZXJlIGVsIGlzIHRoZSBET00gZWxlbWVudCB5b3UnZCBsaWtlIHRvIHRlc3QgZm9yIHZpc2liaWxpdHlcblxuXG5mdW5jdGlvbiBpc0hpZGRlbihub2RlKSB7XG4gIHJldHVybiAhbm9kZSB8fCBub2RlLm9mZnNldFBhcmVudCA9PT0gbnVsbDtcbn1cblxudmFyIFByb3BUeXBlc18xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCIuL1Byb3BUeXBlc1wiKSk7XG5cbnZhciBTZWxlY3RUcmlnZ2VyXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcIi4vU2VsZWN0VHJpZ2dlclwiKSk7XG5cbnZhciB1dGlsXzEgPSByZXF1aXJlKFwiLi91dGlsXCIpO1xuXG52YXIgU0VMRUNUX0VNUFRZX1ZBTFVFX0tFWSA9ICdSQ19TRUxFQ1RfRU1QVFlfVkFMVUVfS0VZJztcblxudmFyIG5vb3AgPSBmdW5jdGlvbiBub29wKCkge1xuICByZXR1cm4gbnVsbDtcbn07XG5cbmZ1bmN0aW9uIGNoYWluaW5nKCkge1xuICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgZm5zID0gbmV3IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgIGZuc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgfVxuXG4gIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgZm9yICh2YXIgX2xlbjIgPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gbmV3IEFycmF5KF9sZW4yKSwgX2tleTIgPSAwOyBfa2V5MiA8IF9sZW4yOyBfa2V5MisrKSB7XG4gICAgICBhcmdzW19rZXkyXSA9IGFyZ3VtZW50c1tfa2V5Ml07XG4gICAgfVxuXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOnByZWZlci1mb3Itb2ZcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGZucy5sZW5ndGg7IGkrKykge1xuICAgICAgaWYgKGZuc1tpXSAmJiB0eXBlb2YgZm5zW2ldID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGZuc1tpXS5hcHBseShjaGFpbmluZywgYXJncyk7XG4gICAgICB9XG4gICAgfVxuICB9O1xufVxuXG52YXIgU2VsZWN0ID1cbi8qI19fUFVSRV9fKi9cbmZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhTZWxlY3QsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFNlbGVjdChwcm9wcykge1xuICAgIHZhciBfdGhpcztcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTZWxlY3QpO1xuXG4gICAgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfZ2V0UHJvdG90eXBlT2YoU2VsZWN0KS5jYWxsKHRoaXMsIHByb3BzKSk7XG4gICAgX3RoaXMuaW5wdXRSZWYgPSBudWxsO1xuICAgIF90aGlzLmlucHV0TWlycm9yUmVmID0gbnVsbDtcbiAgICBfdGhpcy50b3BDdHJsUmVmID0gbnVsbDtcbiAgICBfdGhpcy5zZWxlY3RUcmlnZ2VyUmVmID0gbnVsbDtcbiAgICBfdGhpcy5yb290UmVmID0gbnVsbDtcbiAgICBfdGhpcy5zZWxlY3Rpb25SZWYgPSBudWxsO1xuICAgIF90aGlzLmRyb3Bkb3duQ29udGFpbmVyID0gbnVsbDtcbiAgICBfdGhpcy5ibHVyVGltZXIgPSBudWxsO1xuICAgIF90aGlzLmZvY3VzVGltZXIgPSBudWxsO1xuICAgIF90aGlzLmNvbWJvYm94VGltZXIgPSBudWxsOyAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6dmFyaWFibGUtbmFtZVxuXG4gICAgX3RoaXMuX2ZvY3VzZWQgPSBmYWxzZTsgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOnZhcmlhYmxlLW5hbWVcblxuICAgIF90aGlzLl9tb3VzZURvd24gPSBmYWxzZTsgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOnZhcmlhYmxlLW5hbWVcblxuICAgIF90aGlzLl9vcHRpb25zID0gW107IC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTp2YXJpYWJsZS1uYW1lXG5cbiAgICBfdGhpcy5fZW1wdHkgPSBmYWxzZTtcblxuICAgIF90aGlzLm9uSW5wdXRDaGFuZ2UgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIHZhciB0b2tlblNlcGFyYXRvcnMgPSBfdGhpcy5wcm9wcy50b2tlblNlcGFyYXRvcnM7XG4gICAgICB2YXIgdmFsID0gZXZlbnQudGFyZ2V0LnZhbHVlO1xuXG4gICAgICBpZiAodXRpbF8xLmlzTXVsdGlwbGVPclRhZ3MoX3RoaXMucHJvcHMpICYmIHRva2VuU2VwYXJhdG9ycy5sZW5ndGggJiYgdXRpbF8xLmluY2x1ZGVzU2VwYXJhdG9ycyh2YWwsIHRva2VuU2VwYXJhdG9ycykpIHtcbiAgICAgICAgdmFyIG5leHRWYWx1ZSA9IF90aGlzLmdldFZhbHVlQnlJbnB1dCh2YWwpO1xuXG4gICAgICAgIGlmIChuZXh0VmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIF90aGlzLmZpcmVDaGFuZ2UobmV4dFZhbHVlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIF90aGlzLnNldE9wZW5TdGF0ZShmYWxzZSwge1xuICAgICAgICAgIG5lZWRGb2N1czogdHJ1ZVxuICAgICAgICB9KTtcblxuICAgICAgICBfdGhpcy5zZXRJbnB1dFZhbHVlKCcnLCBmYWxzZSk7XG5cbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBfdGhpcy5zZXRJbnB1dFZhbHVlKHZhbCk7XG5cbiAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgb3BlbjogdHJ1ZVxuICAgICAgfSk7XG5cbiAgICAgIGlmICh1dGlsXzEuaXNDb21ib2JveChfdGhpcy5wcm9wcykpIHtcbiAgICAgICAgX3RoaXMuZmlyZUNoYW5nZShbdmFsXSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLm9uRHJvcGRvd25WaXNpYmxlQ2hhbmdlID0gZnVuY3Rpb24gKG9wZW4pIHtcbiAgICAgIGlmIChvcGVuICYmICFfdGhpcy5fZm9jdXNlZCkge1xuICAgICAgICBfdGhpcy5jbGVhckJsdXJUaW1lKCk7XG5cbiAgICAgICAgX3RoaXMudGltZW91dEZvY3VzKCk7XG5cbiAgICAgICAgX3RoaXMuX2ZvY3VzZWQgPSB0cnVlO1xuXG4gICAgICAgIF90aGlzLnVwZGF0ZUZvY3VzQ2xhc3NOYW1lKCk7XG4gICAgICB9XG5cbiAgICAgIF90aGlzLnNldE9wZW5TdGF0ZShvcGVuKTtcbiAgICB9OyAvLyBjb21ib2JveCBpZ25vcmVcblxuXG4gICAgX3RoaXMub25LZXlEb3duID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICB2YXIgb3BlbiA9IF90aGlzLnN0YXRlLm9wZW47XG4gICAgICB2YXIgZGlzYWJsZWQgPSBfdGhpcy5wcm9wcy5kaXNhYmxlZDtcblxuICAgICAgaWYgKGRpc2FibGVkKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGtleUNvZGUgPSBldmVudC5rZXlDb2RlO1xuXG4gICAgICBpZiAob3BlbiAmJiAhX3RoaXMuZ2V0SW5wdXRET01Ob2RlKCkpIHtcbiAgICAgICAgX3RoaXMub25JbnB1dEtleURvd24oZXZlbnQpO1xuICAgICAgfSBlbHNlIGlmIChrZXlDb2RlID09PSBLZXlDb2RlXzFbXCJkZWZhdWx0XCJdLkVOVEVSIHx8IGtleUNvZGUgPT09IEtleUNvZGVfMVtcImRlZmF1bHRcIl0uRE9XTikge1xuICAgICAgICBpZiAoIW9wZW4pIHtcbiAgICAgICAgICBfdGhpcy5zZXRPcGVuU3RhdGUodHJ1ZSk7XG4gICAgICAgIH1cblxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfSBlbHNlIGlmIChrZXlDb2RlID09PSBLZXlDb2RlXzFbXCJkZWZhdWx0XCJdLlNQQUNFKSB7XG4gICAgICAgIC8vIE5vdCBibG9jayBzcGFjZSBpZiBwb3B1cCBpcyBzaG93blxuICAgICAgICBpZiAoIW9wZW4pIHtcbiAgICAgICAgICBfdGhpcy5zZXRPcGVuU3RhdGUodHJ1ZSk7XG5cbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLm9uSW5wdXRLZXlEb3duID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBkaXNhYmxlZCA9IF90aGlzJHByb3BzLmRpc2FibGVkLFxuICAgICAgICAgIGNvbWJvYm94ID0gX3RoaXMkcHJvcHMuY29tYm9ib3gsXG4gICAgICAgICAgZGVmYXVsdEFjdGl2ZUZpcnN0T3B0aW9uID0gX3RoaXMkcHJvcHMuZGVmYXVsdEFjdGl2ZUZpcnN0T3B0aW9uO1xuXG4gICAgICBpZiAoZGlzYWJsZWQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgc3RhdGUgPSBfdGhpcy5zdGF0ZTtcblxuICAgICAgdmFyIGlzUmVhbE9wZW4gPSBfdGhpcy5nZXRSZWFsT3BlblN0YXRlKHN0YXRlKTsgLy8gbWFnaWMgY29kZVxuXG5cbiAgICAgIHZhciBrZXlDb2RlID0gZXZlbnQua2V5Q29kZTtcblxuICAgICAgaWYgKHV0aWxfMS5pc011bHRpcGxlT3JUYWdzKF90aGlzLnByb3BzKSAmJiAhZXZlbnQudGFyZ2V0LnZhbHVlICYmIGtleUNvZGUgPT09IEtleUNvZGVfMVtcImRlZmF1bHRcIl0uQkFDS1NQQUNFKSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHZhciB2YWx1ZSA9IHN0YXRlLnZhbHVlO1xuXG4gICAgICAgIGlmICh2YWx1ZS5sZW5ndGgpIHtcbiAgICAgICAgICBfdGhpcy5yZW1vdmVTZWxlY3RlZCh2YWx1ZVt2YWx1ZS5sZW5ndGggLSAxXSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmIChrZXlDb2RlID09PSBLZXlDb2RlXzFbXCJkZWZhdWx0XCJdLkRPV04pIHtcbiAgICAgICAgaWYgKCFzdGF0ZS5vcGVuKSB7XG4gICAgICAgICAgX3RoaXMub3BlbklmSGFzQ2hpbGRyZW4oKTtcblxuICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKGtleUNvZGUgPT09IEtleUNvZGVfMVtcImRlZmF1bHRcIl0uRU5URVIgJiYgc3RhdGUub3Blbikge1xuICAgICAgICAvLyBBdmlvZCB0cmlnZ2VyIGZvcm0gc3VibWl0IHdoZW4gc2VsZWN0IGl0ZW1cbiAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTA4NjFcbiAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTQ1NDRcbiAgICAgICAgaWYgKGlzUmVhbE9wZW4gfHwgIWNvbWJvYm94KSB7XG4gICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgfSAvLyBIYXJkIGNsb3NlIHBvcHVwIHRvIGF2b2lkIGxvY2sgb2Ygbm9uIG9wdGlvbiBpbiBjb21ib2JveCBtb2RlXG5cblxuICAgICAgICBpZiAoaXNSZWFsT3BlbiAmJiBjb21ib2JveCAmJiBkZWZhdWx0QWN0aXZlRmlyc3RPcHRpb24gPT09IGZhbHNlKSB7XG4gICAgICAgICAgX3RoaXMuY29tYm9ib3hUaW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgX3RoaXMuc2V0T3BlblN0YXRlKGZhbHNlKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChrZXlDb2RlID09PSBLZXlDb2RlXzFbXCJkZWZhdWx0XCJdLkVTQykge1xuICAgICAgICBpZiAoc3RhdGUub3Blbikge1xuICAgICAgICAgIF90aGlzLnNldE9wZW5TdGF0ZShmYWxzZSk7XG5cbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAoaXNSZWFsT3BlbiAmJiBfdGhpcy5zZWxlY3RUcmlnZ2VyUmVmKSB7XG4gICAgICAgIHZhciBtZW51ID0gX3RoaXMuc2VsZWN0VHJpZ2dlclJlZi5nZXRJbm5lck1lbnUoKTtcblxuICAgICAgICBpZiAobWVudSAmJiBtZW51Lm9uS2V5RG93bihldmVudCwgX3RoaXMuaGFuZGxlQmFja2ZpbGwpKSB7XG4gICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5vbk1lbnVTZWxlY3QgPSBmdW5jdGlvbiAoX3JlZikge1xuICAgICAgdmFyIGl0ZW0gPSBfcmVmLml0ZW07XG5cbiAgICAgIGlmICghaXRlbSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciB2YWx1ZSA9IF90aGlzLnN0YXRlLnZhbHVlO1xuICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG4gICAgICB2YXIgc2VsZWN0ZWRWYWx1ZSA9IHV0aWxfMS5nZXRWYWx1ZVByb3BWYWx1ZShpdGVtKTtcbiAgICAgIHZhciBsYXN0VmFsdWUgPSB2YWx1ZVt2YWx1ZS5sZW5ndGggLSAxXTtcbiAgICAgIHZhciBza2lwVHJpZ2dlciA9IGZhbHNlO1xuXG4gICAgICBpZiAodXRpbF8xLmlzTXVsdGlwbGVPclRhZ3MocHJvcHMpKSB7XG4gICAgICAgIGlmICh1dGlsXzEuZmluZEluZGV4SW5WYWx1ZUJ5U2luZ2xlVmFsdWUodmFsdWUsIHNlbGVjdGVkVmFsdWUpICE9PSAtMSkge1xuICAgICAgICAgIHNraXBUcmlnZ2VyID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YWx1ZSA9IHZhbHVlLmNvbmNhdChbc2VsZWN0ZWRWYWx1ZV0pO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoIXV0aWxfMS5pc0NvbWJvYm94KHByb3BzKSAmJiBsYXN0VmFsdWUgIT09IHVuZGVmaW5lZCAmJiBsYXN0VmFsdWUgPT09IHNlbGVjdGVkVmFsdWUgJiYgc2VsZWN0ZWRWYWx1ZSAhPT0gX3RoaXMuc3RhdGUuYmFja2ZpbGxWYWx1ZSkge1xuICAgICAgICAgIF90aGlzLnNldE9wZW5TdGF0ZShmYWxzZSwge1xuICAgICAgICAgICAgbmVlZEZvY3VzOiB0cnVlLFxuICAgICAgICAgICAgZmlyZVNlYXJjaDogZmFsc2VcbiAgICAgICAgICB9KTtcblxuICAgICAgICAgIHNraXBUcmlnZ2VyID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YWx1ZSA9IFtzZWxlY3RlZFZhbHVlXTtcblxuICAgICAgICAgIF90aGlzLnNldE9wZW5TdGF0ZShmYWxzZSwge1xuICAgICAgICAgICAgbmVlZEZvY3VzOiB0cnVlLFxuICAgICAgICAgICAgZmlyZVNlYXJjaDogZmFsc2VcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoIXNraXBUcmlnZ2VyKSB7XG4gICAgICAgIF90aGlzLmZpcmVDaGFuZ2UodmFsdWUpO1xuICAgICAgfVxuXG4gICAgICBfdGhpcy5maXJlU2VsZWN0KHNlbGVjdGVkVmFsdWUpO1xuXG4gICAgICBpZiAoIXNraXBUcmlnZ2VyKSB7XG4gICAgICAgIHZhciBpbnB1dFZhbHVlID0gdXRpbF8xLmlzQ29tYm9ib3gocHJvcHMpID8gdXRpbF8xLmdldFByb3BWYWx1ZShpdGVtLCBwcm9wcy5vcHRpb25MYWJlbFByb3ApIDogJyc7XG5cbiAgICAgICAgaWYgKHByb3BzLmF1dG9DbGVhclNlYXJjaFZhbHVlKSB7XG4gICAgICAgICAgX3RoaXMuc2V0SW5wdXRWYWx1ZShpbnB1dFZhbHVlLCBmYWxzZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMub25NZW51RGVzZWxlY3QgPSBmdW5jdGlvbiAoX3JlZjIpIHtcbiAgICAgIHZhciBpdGVtID0gX3JlZjIuaXRlbSxcbiAgICAgICAgICBkb21FdmVudCA9IF9yZWYyLmRvbUV2ZW50O1xuXG4gICAgICBpZiAoZG9tRXZlbnQudHlwZSA9PT0gJ2tleWRvd24nICYmIGRvbUV2ZW50LmtleUNvZGUgPT09IEtleUNvZGVfMVtcImRlZmF1bHRcIl0uRU5URVIpIHtcbiAgICAgICAgdmFyIG1lbnVJdGVtRG9tTm9kZSA9IFJlYWN0RE9NLmZpbmRET01Ob2RlKGl0ZW0pOyAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8yMDQ2NSNpc3N1ZWNvbW1lbnQtNTY5MDMzNzk2XG5cbiAgICAgICAgaWYgKCFpc0hpZGRlbihtZW51SXRlbURvbU5vZGUpKSB7XG4gICAgICAgICAgX3RoaXMucmVtb3ZlU2VsZWN0ZWQodXRpbF8xLmdldFZhbHVlUHJvcFZhbHVlKGl0ZW0pKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKGRvbUV2ZW50LnR5cGUgPT09ICdjbGljaycpIHtcbiAgICAgICAgX3RoaXMucmVtb3ZlU2VsZWN0ZWQodXRpbF8xLmdldFZhbHVlUHJvcFZhbHVlKGl0ZW0pKTtcbiAgICAgIH1cblxuICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG5cbiAgICAgIGlmIChwcm9wcy5hdXRvQ2xlYXJTZWFyY2hWYWx1ZSkge1xuICAgICAgICBfdGhpcy5zZXRJbnB1dFZhbHVlKCcnKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMub25BcnJvd0NsaWNrID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgIGlmICghX3RoaXMucHJvcHMuZGlzYWJsZWQpIHtcbiAgICAgICAgX3RoaXMuc2V0T3BlblN0YXRlKCFfdGhpcy5zdGF0ZS5vcGVuLCB7XG4gICAgICAgICAgbmVlZEZvY3VzOiAhX3RoaXMuc3RhdGUub3BlblxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMub25QbGFjZWhvbGRlckNsaWNrID0gZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKF90aGlzLmdldElucHV0RE9NTm9kZSAmJiBfdGhpcy5nZXRJbnB1dERPTU5vZGUoKSkge1xuICAgICAgICBfdGhpcy5nZXRJbnB1dERPTU5vZGUoKS5mb2N1cygpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5vbk91dGVyRm9jdXMgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgaWYgKF90aGlzLnByb3BzLmRpc2FibGVkKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBfdGhpcy5jbGVhckJsdXJUaW1lKCk7IC8vIEluIElFMTEsIG9uT3V0ZXJGb2N1cyB3aWxsIGJlIHRyaWdnZXIgdHdpY2Ugd2hlbiBmb2N1cyBpbnB1dFxuICAgICAgLy8gRmlyc3Qgb25lOiBlLnRhcmdldCBpcyBkaXZcbiAgICAgIC8vIFNlY29uZCBvbmU6IGUudGFyZ2V0IGlzIGlucHV0XG4gICAgICAvLyBvdGhlciBicm93c2VyIG9ubHkgdHJpZ2dlciBzZWNvbmQgb25lXG4gICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xNTk0MlxuICAgICAgLy8gSGVyZSB3ZSBpZ25vcmUgdGhlIGZpcnN0IG9uZSB3aGVuIGUudGFyZ2V0IGlzIGRpdlxuXG5cbiAgICAgIHZhciBpbnB1dE5vZGUgPSBfdGhpcy5nZXRJbnB1dERPTU5vZGUoKTtcblxuICAgICAgaWYgKGlucHV0Tm9kZSAmJiBlLnRhcmdldCA9PT0gX3RoaXMucm9vdFJlZikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmICghdXRpbF8xLmlzTXVsdGlwbGVPclRhZ3NPckNvbWJvYm94KF90aGlzLnByb3BzKSAmJiBlLnRhcmdldCA9PT0gaW5wdXROb2RlKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKF90aGlzLl9mb2N1c2VkKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgX3RoaXMuX2ZvY3VzZWQgPSB0cnVlO1xuXG4gICAgICBfdGhpcy51cGRhdGVGb2N1c0NsYXNzTmFtZSgpOyAvLyBvbmx5IGVmZmVjdCBtdWx0aXBsZSBvciB0YWcgbW9kZVxuXG5cbiAgICAgIGlmICghdXRpbF8xLmlzTXVsdGlwbGVPclRhZ3MoX3RoaXMucHJvcHMpIHx8ICFfdGhpcy5fbW91c2VEb3duKSB7XG4gICAgICAgIF90aGlzLnRpbWVvdXRGb2N1cygpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5vblBvcHVwRm9jdXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBmaXggaWUgc2Nyb2xsYmFyLCBmb2N1cyBlbGVtZW50IGFnYWluXG4gICAgICBfdGhpcy5tYXliZUZvY3VzKHRydWUsIHRydWUpO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbk91dGVyQmx1ciA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICBpZiAoX3RoaXMucHJvcHMuZGlzYWJsZWQpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIF90aGlzLmJsdXJUaW1lciA9IHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgX3RoaXMuX2ZvY3VzZWQgPSBmYWxzZTtcblxuICAgICAgICBfdGhpcy51cGRhdGVGb2N1c0NsYXNzTmFtZSgpO1xuXG4gICAgICAgIHZhciBwcm9wcyA9IF90aGlzLnByb3BzO1xuICAgICAgICB2YXIgdmFsdWUgPSBfdGhpcy5zdGF0ZS52YWx1ZTtcbiAgICAgICAgdmFyIGlucHV0VmFsdWUgPSBfdGhpcy5zdGF0ZS5pbnB1dFZhbHVlO1xuXG4gICAgICAgIGlmICh1dGlsXzEuaXNTaW5nbGVNb2RlKHByb3BzKSAmJiBwcm9wcy5zaG93U2VhcmNoICYmIGlucHV0VmFsdWUgJiYgcHJvcHMuZGVmYXVsdEFjdGl2ZUZpcnN0T3B0aW9uKSB7XG4gICAgICAgICAgdmFyIG9wdGlvbnMgPSBfdGhpcy5fb3B0aW9ucyB8fCBbXTtcblxuICAgICAgICAgIGlmIChvcHRpb25zLmxlbmd0aCkge1xuICAgICAgICAgICAgdmFyIGZpcnN0T3B0aW9uID0gdXRpbF8xLmZpbmRGaXJzdE1lbnVJdGVtKG9wdGlvbnMpO1xuXG4gICAgICAgICAgICBpZiAoZmlyc3RPcHRpb24pIHtcbiAgICAgICAgICAgICAgdmFsdWUgPSBbdXRpbF8xLmdldFZhbHVlUHJvcFZhbHVlKGZpcnN0T3B0aW9uKV07XG5cbiAgICAgICAgICAgICAgX3RoaXMuZmlyZUNoYW5nZSh2YWx1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2UgaWYgKHV0aWxfMS5pc011bHRpcGxlT3JUYWdzKHByb3BzKSAmJiBpbnB1dFZhbHVlKSB7XG4gICAgICAgICAgaWYgKF90aGlzLl9tb3VzZURvd24pIHtcbiAgICAgICAgICAgIC8vIG5lZWQgdXBkYXRlIGRyb3BtZW51IHdoZW4gbm90IGJsdXJcbiAgICAgICAgICAgIF90aGlzLnNldElucHV0VmFsdWUoJycpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAvLyB3aHkgbm90IHVzZSBzZXRTdGF0ZT9cbiAgICAgICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzE0MjYyXG4gICAgICAgICAgICBfdGhpcy5zdGF0ZS5pbnB1dFZhbHVlID0gJyc7XG5cbiAgICAgICAgICAgIGlmIChfdGhpcy5nZXRJbnB1dERPTU5vZGUgJiYgX3RoaXMuZ2V0SW5wdXRET01Ob2RlKCkpIHtcbiAgICAgICAgICAgICAgX3RoaXMuZ2V0SW5wdXRET01Ob2RlKCkudmFsdWUgPSAnJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG5cbiAgICAgICAgICB2YXIgdG1wVmFsdWUgPSBfdGhpcy5nZXRWYWx1ZUJ5SW5wdXQoaW5wdXRWYWx1ZSk7XG5cbiAgICAgICAgICBpZiAodG1wVmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdmFsdWUgPSB0bXBWYWx1ZTtcblxuICAgICAgICAgICAgX3RoaXMuZmlyZUNoYW5nZSh2YWx1ZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9IC8vIGlmIGNsaWNrIHRoZSByZXN0IHNwYWNlIG9mIFNlbGVjdCBpbiBtdWx0aXBsZSBtb2RlXG5cblxuICAgICAgICBpZiAodXRpbF8xLmlzTXVsdGlwbGVPclRhZ3MocHJvcHMpICYmIF90aGlzLl9tb3VzZURvd24pIHtcbiAgICAgICAgICBfdGhpcy5tYXliZUZvY3VzKHRydWUsIHRydWUpO1xuXG4gICAgICAgICAgX3RoaXMuX21vdXNlRG93biA9IGZhbHNlO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIF90aGlzLnNldE9wZW5TdGF0ZShmYWxzZSk7XG5cbiAgICAgICAgaWYgKHByb3BzLm9uQmx1cikge1xuICAgICAgICAgIHByb3BzLm9uQmx1cihfdGhpcy5nZXRWTEZvck9uQ2hhbmdlKHZhbHVlKSk7XG4gICAgICAgIH1cbiAgICAgIH0sIDEwKTtcbiAgICB9O1xuXG4gICAgX3RoaXMub25DbGVhclNlbGVjdGlvbiA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG4gICAgICB2YXIgc3RhdGUgPSBfdGhpcy5zdGF0ZTtcblxuICAgICAgaWYgKHByb3BzLmRpc2FibGVkKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGlucHV0VmFsdWUgPSBzdGF0ZS5pbnB1dFZhbHVlO1xuICAgICAgdmFyIHZhbHVlID0gc3RhdGUudmFsdWU7XG4gICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblxuICAgICAgaWYgKGlucHV0VmFsdWUgfHwgdmFsdWUubGVuZ3RoKSB7XG4gICAgICAgIGlmICh2YWx1ZS5sZW5ndGgpIHtcbiAgICAgICAgICBfdGhpcy5maXJlQ2hhbmdlKFtdKTtcbiAgICAgICAgfVxuXG4gICAgICAgIF90aGlzLnNldE9wZW5TdGF0ZShmYWxzZSwge1xuICAgICAgICAgIG5lZWRGb2N1czogdHJ1ZVxuICAgICAgICB9KTtcblxuICAgICAgICBpZiAoaW5wdXRWYWx1ZSkge1xuICAgICAgICAgIF90aGlzLnNldElucHV0VmFsdWUoJycpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLm9uQ2hvaWNlQW5pbWF0aW9uTGVhdmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBfdGhpcy5mb3JjZVBvcHVwQWxpZ24oKTtcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0T3B0aW9uSW5mb0J5U2luZ2xlVmFsdWUgPSBmdW5jdGlvbiAodmFsdWUsIG9wdGlvbnNJbmZvKSB7XG4gICAgICB2YXIgaW5mbztcbiAgICAgIG9wdGlvbnNJbmZvID0gb3B0aW9uc0luZm8gfHwgX3RoaXMuc3RhdGUub3B0aW9uc0luZm87XG5cbiAgICAgIGlmIChvcHRpb25zSW5mb1t1dGlsXzEuZ2V0TWFwS2V5KHZhbHVlKV0pIHtcbiAgICAgICAgaW5mbyA9IG9wdGlvbnNJbmZvW3V0aWxfMS5nZXRNYXBLZXkodmFsdWUpXTtcbiAgICAgIH1cblxuICAgICAgaWYgKGluZm8pIHtcbiAgICAgICAgcmV0dXJuIGluZm87XG4gICAgICB9XG5cbiAgICAgIHZhciBkZWZhdWx0TGFiZWwgPSB2YWx1ZTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLmxhYmVsSW5WYWx1ZSkge1xuICAgICAgICB2YXIgdmFsdWVMYWJlbCA9IHV0aWxfMS5nZXRMYWJlbEZyb21Qcm9wc1ZhbHVlKF90aGlzLnByb3BzLnZhbHVlLCB2YWx1ZSk7XG4gICAgICAgIHZhciBkZWZhdWx0VmFsdWVMYWJlbCA9IHV0aWxfMS5nZXRMYWJlbEZyb21Qcm9wc1ZhbHVlKF90aGlzLnByb3BzLmRlZmF1bHRWYWx1ZSwgdmFsdWUpO1xuXG4gICAgICAgIGlmICh2YWx1ZUxhYmVsICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBkZWZhdWx0TGFiZWwgPSB2YWx1ZUxhYmVsO1xuICAgICAgICB9IGVsc2UgaWYgKGRlZmF1bHRWYWx1ZUxhYmVsICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBkZWZhdWx0TGFiZWwgPSBkZWZhdWx0VmFsdWVMYWJlbDtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB2YXIgZGVmYXVsdEluZm8gPSB7XG4gICAgICAgIG9wdGlvbjogUmVhY3QuY3JlYXRlRWxlbWVudChPcHRpb25fMVtcImRlZmF1bHRcIl0sIHtcbiAgICAgICAgICB2YWx1ZTogdmFsdWUsXG4gICAgICAgICAga2V5OiB2YWx1ZVxuICAgICAgICB9LCB2YWx1ZSksXG4gICAgICAgIHZhbHVlOiB2YWx1ZSxcbiAgICAgICAgbGFiZWw6IGRlZmF1bHRMYWJlbFxuICAgICAgfTtcbiAgICAgIHJldHVybiBkZWZhdWx0SW5mbztcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0T3B0aW9uQnlTaW5nbGVWYWx1ZSA9IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgdmFyIF90aGlzJGdldE9wdGlvbkluZm9CeSA9IF90aGlzLmdldE9wdGlvbkluZm9CeVNpbmdsZVZhbHVlKHZhbHVlKSxcbiAgICAgICAgICBvcHRpb24gPSBfdGhpcyRnZXRPcHRpb25JbmZvQnkub3B0aW9uO1xuXG4gICAgICByZXR1cm4gb3B0aW9uO1xuICAgIH07XG5cbiAgICBfdGhpcy5nZXRPcHRpb25zQnlTaW5nbGVWYWx1ZSA9IGZ1bmN0aW9uICh2YWx1ZXMpIHtcbiAgICAgIHJldHVybiB2YWx1ZXMubWFwKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICByZXR1cm4gX3RoaXMuZ2V0T3B0aW9uQnlTaW5nbGVWYWx1ZSh2YWx1ZSk7XG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0VmFsdWVCeUxhYmVsID0gZnVuY3Rpb24gKGxhYmVsKSB7XG4gICAgICBpZiAobGFiZWwgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cblxuICAgICAgdmFyIHZhbHVlID0gbnVsbDtcbiAgICAgIE9iamVjdC5rZXlzKF90aGlzLnN0YXRlLm9wdGlvbnNJbmZvKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgdmFyIGluZm8gPSBfdGhpcy5zdGF0ZS5vcHRpb25zSW5mb1trZXldO1xuICAgICAgICB2YXIgZGlzYWJsZWQgPSBpbmZvLmRpc2FibGVkO1xuXG4gICAgICAgIGlmIChkaXNhYmxlZCkge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBvbGRMYWJsZSA9IHV0aWxfMS50b0FycmF5KGluZm8ubGFiZWwpO1xuXG4gICAgICAgIGlmIChvbGRMYWJsZSAmJiBvbGRMYWJsZS5qb2luKCcnKSA9PT0gbGFiZWwpIHtcbiAgICAgICAgICB2YWx1ZSA9IGluZm8udmFsdWU7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIHZhbHVlO1xuICAgIH07XG5cbiAgICBfdGhpcy5nZXRWTEJ5U2luZ2xlVmFsdWUgPSBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgIGlmIChfdGhpcy5wcm9wcy5sYWJlbEluVmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBrZXk6IHZhbHVlLFxuICAgICAgICAgIGxhYmVsOiBfdGhpcy5nZXRMYWJlbEJ5U2luZ2xlVmFsdWUodmFsdWUpXG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0VkxGb3JPbkNoYW5nZSA9IGZ1bmN0aW9uICh2bHNTKSB7XG4gICAgICB2YXIgdmxzID0gdmxzUztcblxuICAgICAgaWYgKHZscyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGlmICghX3RoaXMucHJvcHMubGFiZWxJblZhbHVlKSB7XG4gICAgICAgICAgdmxzID0gdmxzLm1hcChmdW5jdGlvbiAodikge1xuICAgICAgICAgICAgcmV0dXJuIHY7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdmxzID0gdmxzLm1hcChmdW5jdGlvbiAodmwpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIGtleTogdmwsXG4gICAgICAgICAgICAgIGxhYmVsOiBfdGhpcy5nZXRMYWJlbEJ5U2luZ2xlVmFsdWUodmwpXG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHV0aWxfMS5pc011bHRpcGxlT3JUYWdzKF90aGlzLnByb3BzKSA/IHZscyA6IHZsc1swXTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHZscztcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0TGFiZWxCeVNpbmdsZVZhbHVlID0gZnVuY3Rpb24gKHZhbHVlLCBvcHRpb25zSW5mbykge1xuICAgICAgdmFyIF90aGlzJGdldE9wdGlvbkluZm9CeTIgPSBfdGhpcy5nZXRPcHRpb25JbmZvQnlTaW5nbGVWYWx1ZSh2YWx1ZSwgb3B0aW9uc0luZm8pLFxuICAgICAgICAgIGxhYmVsID0gX3RoaXMkZ2V0T3B0aW9uSW5mb0J5Mi5sYWJlbDtcblxuICAgICAgcmV0dXJuIGxhYmVsO1xuICAgIH07XG5cbiAgICBfdGhpcy5nZXREcm9wZG93bkNvbnRhaW5lciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICghX3RoaXMuZHJvcGRvd25Db250YWluZXIpIHtcbiAgICAgICAgX3RoaXMuZHJvcGRvd25Db250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChfdGhpcy5kcm9wZG93bkNvbnRhaW5lcik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfdGhpcy5kcm9wZG93bkNvbnRhaW5lcjtcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0UGxhY2Vob2xkZXJFbGVtZW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG4gICAgICB2YXIgc3RhdGUgPSBfdGhpcy5zdGF0ZTtcbiAgICAgIHZhciBoaWRkZW4gPSBmYWxzZTtcblxuICAgICAgaWYgKHN0YXRlLmlucHV0VmFsdWUpIHtcbiAgICAgICAgaGlkZGVuID0gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgdmFyIHZhbHVlID0gc3RhdGUudmFsdWU7XG5cbiAgICAgIGlmICh2YWx1ZS5sZW5ndGgpIHtcbiAgICAgICAgaGlkZGVuID0gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgaWYgKHV0aWxfMS5pc0NvbWJvYm94KHByb3BzKSAmJiB2YWx1ZS5sZW5ndGggPT09IDEgJiYgc3RhdGUudmFsdWUgJiYgIXN0YXRlLnZhbHVlWzBdKSB7XG4gICAgICAgIGhpZGRlbiA9IGZhbHNlO1xuICAgICAgfVxuXG4gICAgICB2YXIgcGxhY2Vob2xkZXIgPSBwcm9wcy5wbGFjZWhvbGRlcjtcblxuICAgICAgaWYgKHBsYWNlaG9sZGVyKSB7XG4gICAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIF9leHRlbmRzKHtcbiAgICAgICAgICBvbk1vdXNlRG93bjogdXRpbF8xLnByZXZlbnREZWZhdWx0RXZlbnQsXG4gICAgICAgICAgc3R5bGU6IF9vYmplY3RTcHJlYWQoe1xuICAgICAgICAgICAgZGlzcGxheTogaGlkZGVuID8gJ25vbmUnIDogJ2Jsb2NrJ1xuICAgICAgICAgIH0sIHV0aWxfMS5VTlNFTEVDVEFCTEVfU1RZTEUpXG4gICAgICAgIH0sIHV0aWxfMS5VTlNFTEVDVEFCTEVfQVRUUklCVVRFLCB7XG4gICAgICAgICAgb25DbGljazogX3RoaXMub25QbGFjZWhvbGRlckNsaWNrLFxuICAgICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJvcHMucHJlZml4Q2xzLCBcIi1zZWxlY3Rpb25fX3BsYWNlaG9sZGVyXCIpXG4gICAgICAgIH0pLCBwbGFjZWhvbGRlcik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICBfdGhpcy5nZXRJbnB1dEVsZW1lbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgcHJvcHMgPSBfdGhpcy5wcm9wcztcbiAgICAgIHZhciBkZWZhdWx0SW5wdXQgPSBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW5wdXRcIiwge1xuICAgICAgICBpZDogcHJvcHMuaWQsXG4gICAgICAgIGF1dG9Db21wbGV0ZTogXCJvZmZcIlxuICAgICAgfSk7IC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTp0eXBlZGVmLXdoaXRlc3BhY2VcblxuICAgICAgdmFyIGlucHV0RWxlbWVudCA9IHByb3BzLmdldElucHV0RWxlbWVudCA/IHByb3BzLmdldElucHV0RWxlbWVudCgpIDogZGVmYXVsdElucHV0O1xuICAgICAgdmFyIGlucHV0Q2xzID0gY2xhc3NuYW1lc18xW1wiZGVmYXVsdFwiXShpbnB1dEVsZW1lbnQucHJvcHMuY2xhc3NOYW1lLCBfZGVmaW5lUHJvcGVydHkoe30sIFwiXCIuY29uY2F0KHByb3BzLnByZWZpeENscywgXCItc2VhcmNoX19maWVsZFwiKSwgdHJ1ZSkpOyAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy80OTkyI2lzc3VlY29tbWVudC0yODE1NDIxNTlcbiAgICAgIC8vIEFkZCBzcGFjZSB0byB0aGUgZW5kIG9mIHRoZSBpbnB1dFZhbHVlIGFzIHRoZSB3aWR0aCBtZWFzdXJlbWVudCB0b2xlcmFuY2VcblxuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge1xuICAgICAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByb3BzLnByZWZpeENscywgXCItc2VhcmNoX19maWVsZF9fd3JhcFwiKVxuICAgICAgfSwgUmVhY3QuY2xvbmVFbGVtZW50KGlucHV0RWxlbWVudCwge1xuICAgICAgICByZWY6IF90aGlzLnNhdmVJbnB1dFJlZixcbiAgICAgICAgb25DaGFuZ2U6IF90aGlzLm9uSW5wdXRDaGFuZ2UsXG4gICAgICAgIG9uS2V5RG93bjogY2hhaW5pbmcoX3RoaXMub25JbnB1dEtleURvd24sIGlucHV0RWxlbWVudC5wcm9wcy5vbktleURvd24sIF90aGlzLnByb3BzLm9uSW5wdXRLZXlEb3duKSxcbiAgICAgICAgdmFsdWU6IF90aGlzLnN0YXRlLmlucHV0VmFsdWUsXG4gICAgICAgIGRpc2FibGVkOiBwcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgY2xhc3NOYW1lOiBpbnB1dENsc1xuICAgICAgfSksIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIHtcbiAgICAgICAgcmVmOiBfdGhpcy5zYXZlSW5wdXRNaXJyb3JSZWYsXG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJvcHMucHJlZml4Q2xzLCBcIi1zZWFyY2hfX2ZpZWxkX19taXJyb3JcIilcbiAgICAgIH0sIF90aGlzLnN0YXRlLmlucHV0VmFsdWUsIFwiXFx4QTBcIikpO1xuICAgIH07XG5cbiAgICBfdGhpcy5nZXRJbnB1dERPTU5vZGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gX3RoaXMudG9wQ3RybFJlZiA/IF90aGlzLnRvcEN0cmxSZWYucXVlcnlTZWxlY3RvcignaW5wdXQsdGV4dGFyZWEsZGl2W2NvbnRlbnRFZGl0YWJsZV0nKSA6IF90aGlzLmlucHV0UmVmO1xuICAgIH07XG5cbiAgICBfdGhpcy5nZXRJbnB1dE1pcnJvckRPTU5vZGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gX3RoaXMuaW5wdXRNaXJyb3JSZWY7XG4gICAgfTtcblxuICAgIF90aGlzLmdldFBvcHVwRE9NTm9kZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChfdGhpcy5zZWxlY3RUcmlnZ2VyUmVmKSB7XG4gICAgICAgIHJldHVybiBfdGhpcy5zZWxlY3RUcmlnZ2VyUmVmLmdldFBvcHVwRE9NTm9kZSgpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5nZXRQb3B1cE1lbnVDb21wb25lbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoX3RoaXMuc2VsZWN0VHJpZ2dlclJlZikge1xuICAgICAgICByZXR1cm4gX3RoaXMuc2VsZWN0VHJpZ2dlclJlZi5nZXRJbm5lck1lbnUoKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMuc2V0T3BlblN0YXRlID0gZnVuY3Rpb24gKG9wZW4pIHtcbiAgICAgIHZhciBjb25maWcgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IHt9O1xuICAgICAgdmFyIG5lZWRGb2N1cyA9IGNvbmZpZy5uZWVkRm9jdXMsXG4gICAgICAgICAgZmlyZVNlYXJjaCA9IGNvbmZpZy5maXJlU2VhcmNoO1xuICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG4gICAgICB2YXIgc3RhdGUgPSBfdGhpcy5zdGF0ZTtcblxuICAgICAgaWYgKHN0YXRlLm9wZW4gPT09IG9wZW4pIHtcbiAgICAgICAgX3RoaXMubWF5YmVGb2N1cyhvcGVuLCAhIW5lZWRGb2N1cyk7XG5cbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25Ecm9wZG93blZpc2libGVDaGFuZ2UpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25Ecm9wZG93blZpc2libGVDaGFuZ2Uob3Blbik7XG4gICAgICB9XG5cbiAgICAgIHZhciBuZXh0U3RhdGUgPSB7XG4gICAgICAgIG9wZW46IG9wZW4sXG4gICAgICAgIGJhY2tmaWxsVmFsdWU6ICcnXG4gICAgICB9OyAvLyBjbGVhciBzZWFyY2ggaW5wdXQgdmFsdWUgd2hlbiBvcGVuIGlzIGZhbHNlIGluIHNpbmdsZU1vZGUuXG4gICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xNjU3MlxuXG4gICAgICBpZiAoIW9wZW4gJiYgdXRpbF8xLmlzU2luZ2xlTW9kZShwcm9wcykgJiYgcHJvcHMuc2hvd1NlYXJjaCkge1xuICAgICAgICBfdGhpcy5zZXRJbnB1dFZhbHVlKCcnLCBmaXJlU2VhcmNoKTtcbiAgICAgIH1cblxuICAgICAgaWYgKCFvcGVuKSB7XG4gICAgICAgIF90aGlzLm1heWJlRm9jdXMob3BlbiwgISFuZWVkRm9jdXMpO1xuICAgICAgfVxuXG4gICAgICBfdGhpcy5zZXRTdGF0ZShfb2JqZWN0U3ByZWFkKHtcbiAgICAgICAgb3Blbjogb3BlblxuICAgICAgfSwgbmV4dFN0YXRlKSwgZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAob3Blbikge1xuICAgICAgICAgIF90aGlzLm1heWJlRm9jdXMob3BlbiwgISFuZWVkRm9jdXMpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgX3RoaXMuc2V0SW5wdXRWYWx1ZSA9IGZ1bmN0aW9uIChpbnB1dFZhbHVlKSB7XG4gICAgICB2YXIgZmlyZVNlYXJjaCA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogdHJ1ZTtcbiAgICAgIHZhciBvblNlYXJjaCA9IF90aGlzLnByb3BzLm9uU2VhcmNoO1xuXG4gICAgICBpZiAoaW5wdXRWYWx1ZSAhPT0gX3RoaXMuc3RhdGUuaW5wdXRWYWx1ZSkge1xuICAgICAgICBfdGhpcy5zZXRTdGF0ZShmdW5jdGlvbiAocHJldlN0YXRlKSB7XG4gICAgICAgICAgLy8gQWRkaXRpb25hbCBjaGVjayBpZiBgaW5wdXRWYWx1ZWAgY2hhbmdlZCBpbiBsYXRlc3Qgc3RhdGUuXG4gICAgICAgICAgaWYgKGZpcmVTZWFyY2ggJiYgaW5wdXRWYWx1ZSAhPT0gcHJldlN0YXRlLmlucHV0VmFsdWUgJiYgb25TZWFyY2gpIHtcbiAgICAgICAgICAgIG9uU2VhcmNoKGlucHV0VmFsdWUpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBpbnB1dFZhbHVlOiBpbnB1dFZhbHVlXG4gICAgICAgICAgfTtcbiAgICAgICAgfSwgX3RoaXMuZm9yY2VQb3B1cEFsaWduKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0VmFsdWVCeUlucHV0ID0gZnVuY3Rpb24gKHN0cikge1xuICAgICAgdmFyIF90aGlzJHByb3BzMiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIG11bHRpcGxlID0gX3RoaXMkcHJvcHMyLm11bHRpcGxlLFxuICAgICAgICAgIHRva2VuU2VwYXJhdG9ycyA9IF90aGlzJHByb3BzMi50b2tlblNlcGFyYXRvcnM7XG4gICAgICB2YXIgbmV4dFZhbHVlID0gX3RoaXMuc3RhdGUudmFsdWU7XG4gICAgICB2YXIgaGFzTmV3VmFsdWUgPSBmYWxzZTtcbiAgICAgIHV0aWxfMS5zcGxpdEJ5U2VwYXJhdG9ycyhzdHIsIHRva2VuU2VwYXJhdG9ycykuZm9yRWFjaChmdW5jdGlvbiAobGFiZWwpIHtcbiAgICAgICAgdmFyIHNlbGVjdGVkVmFsdWUgPSBbbGFiZWxdO1xuXG4gICAgICAgIGlmIChtdWx0aXBsZSkge1xuICAgICAgICAgIHZhciB2YWx1ZSA9IF90aGlzLmdldFZhbHVlQnlMYWJlbChsYWJlbCk7XG5cbiAgICAgICAgICBpZiAodmFsdWUgJiYgdXRpbF8xLmZpbmRJbmRleEluVmFsdWVCeVNpbmdsZVZhbHVlKG5leHRWYWx1ZSwgdmFsdWUpID09PSAtMSkge1xuICAgICAgICAgICAgbmV4dFZhbHVlID0gbmV4dFZhbHVlLmNvbmNhdCh2YWx1ZSk7XG4gICAgICAgICAgICBoYXNOZXdWYWx1ZSA9IHRydWU7XG5cbiAgICAgICAgICAgIF90aGlzLmZpcmVTZWxlY3QodmFsdWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmICh1dGlsXzEuZmluZEluZGV4SW5WYWx1ZUJ5U2luZ2xlVmFsdWUobmV4dFZhbHVlLCBsYWJlbCkgPT09IC0xKSB7XG4gICAgICAgICAgbmV4dFZhbHVlID0gbmV4dFZhbHVlLmNvbmNhdChzZWxlY3RlZFZhbHVlKTtcbiAgICAgICAgICBoYXNOZXdWYWx1ZSA9IHRydWU7XG5cbiAgICAgICAgICBfdGhpcy5maXJlU2VsZWN0KGxhYmVsKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICByZXR1cm4gaGFzTmV3VmFsdWUgPyBuZXh0VmFsdWUgOiB1bmRlZmluZWQ7XG4gICAgfTtcblxuICAgIF90aGlzLmdldFJlYWxPcGVuU3RhdGUgPSBmdW5jdGlvbiAoc3RhdGUpIHtcbiAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTp2YXJpYWJsZS1uYW1lXG4gICAgICB2YXIgX29wZW4gPSBfdGhpcy5wcm9wcy5vcGVuO1xuXG4gICAgICBpZiAodHlwZW9mIF9vcGVuID09PSAnYm9vbGVhbicpIHtcbiAgICAgICAgcmV0dXJuIF9vcGVuO1xuICAgICAgfVxuXG4gICAgICB2YXIgb3BlbiA9IChzdGF0ZSB8fCBfdGhpcy5zdGF0ZSkub3BlbjtcbiAgICAgIHZhciBvcHRpb25zID0gX3RoaXMuX29wdGlvbnMgfHwgW107XG5cbiAgICAgIGlmICh1dGlsXzEuaXNNdWx0aXBsZU9yVGFnc09yQ29tYm9ib3goX3RoaXMucHJvcHMpIHx8ICFfdGhpcy5wcm9wcy5zaG93U2VhcmNoKSB7XG4gICAgICAgIGlmIChvcGVuICYmICFvcHRpb25zLmxlbmd0aCkge1xuICAgICAgICAgIG9wZW4gPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gb3BlbjtcbiAgICB9O1xuXG4gICAgX3RoaXMubWFya01vdXNlRG93biA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIF90aGlzLl9tb3VzZURvd24gPSB0cnVlO1xuICAgIH07XG5cbiAgICBfdGhpcy5tYXJrTW91c2VMZWF2ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIF90aGlzLl9tb3VzZURvd24gPSBmYWxzZTtcbiAgICB9O1xuXG4gICAgX3RoaXMuaGFuZGxlQmFja2ZpbGwgPSBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgaWYgKCFfdGhpcy5wcm9wcy5iYWNrZmlsbCB8fCAhKHV0aWxfMS5pc1NpbmdsZU1vZGUoX3RoaXMucHJvcHMpIHx8IHV0aWxfMS5pc0NvbWJvYm94KF90aGlzLnByb3BzKSkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIga2V5ID0gdXRpbF8xLmdldFZhbHVlUHJvcFZhbHVlKGl0ZW0pO1xuXG4gICAgICBpZiAodXRpbF8xLmlzQ29tYm9ib3goX3RoaXMucHJvcHMpKSB7XG4gICAgICAgIF90aGlzLnNldElucHV0VmFsdWUoa2V5LCBmYWxzZSk7XG4gICAgICB9XG5cbiAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgdmFsdWU6IFtrZXldLFxuICAgICAgICBiYWNrZmlsbFZhbHVlOiBrZXlcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICBfdGhpcy5maWx0ZXJPcHRpb24gPSBmdW5jdGlvbiAoaW5wdXQsIGNoaWxkKSB7XG4gICAgICB2YXIgZGVmYXVsdEZpbHRlciA9IGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIGFyZ3VtZW50c1syXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzJdIDogdXRpbF8xLmRlZmF1bHRGaWx0ZXJGbjtcbiAgICAgIHZhciB2YWx1ZSA9IF90aGlzLnN0YXRlLnZhbHVlO1xuICAgICAgdmFyIGxhc3RWYWx1ZSA9IHZhbHVlW3ZhbHVlLmxlbmd0aCAtIDFdO1xuXG4gICAgICBpZiAoIWlucHV0IHx8IGxhc3RWYWx1ZSAmJiBsYXN0VmFsdWUgPT09IF90aGlzLnN0YXRlLmJhY2tmaWxsVmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG5cbiAgICAgIHZhciBmaWx0ZXJGbiA9IF90aGlzLnByb3BzLmZpbHRlck9wdGlvbjtcblxuICAgICAgaWYgKCdmaWx0ZXJPcHRpb24nIGluIF90aGlzLnByb3BzKSB7XG4gICAgICAgIGlmIChmaWx0ZXJGbiA9PT0gdHJ1ZSkge1xuICAgICAgICAgIGZpbHRlckZuID0gZGVmYXVsdEZpbHRlci5iaW5kKF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZmlsdGVyRm4gPSBkZWZhdWx0RmlsdGVyLmJpbmQoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcykpO1xuICAgICAgfVxuXG4gICAgICBpZiAoIWZpbHRlckZuKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfSBlbHNlIGlmICh0eXBlb2YgZmlsdGVyRm4gPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgcmV0dXJuIGZpbHRlckZuLmNhbGwoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIGlucHV0LCBjaGlsZCk7XG4gICAgICB9IGVsc2UgaWYgKGNoaWxkLnByb3BzLmRpc2FibGVkKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfTtcblxuICAgIF90aGlzLnRpbWVvdXRGb2N1cyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBvbkZvY3VzID0gX3RoaXMucHJvcHMub25Gb2N1cztcblxuICAgICAgaWYgKF90aGlzLmZvY3VzVGltZXIpIHtcbiAgICAgICAgX3RoaXMuY2xlYXJGb2N1c1RpbWUoKTtcbiAgICAgIH1cblxuICAgICAgX3RoaXMuZm9jdXNUaW1lciA9IHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKG9uRm9jdXMpIHtcbiAgICAgICAgICBvbkZvY3VzKCk7XG4gICAgICAgIH1cbiAgICAgIH0sIDEwKTtcbiAgICB9O1xuXG4gICAgX3RoaXMuY2xlYXJGb2N1c1RpbWUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoX3RoaXMuZm9jdXNUaW1lcikge1xuICAgICAgICBjbGVhclRpbWVvdXQoX3RoaXMuZm9jdXNUaW1lcik7XG4gICAgICAgIF90aGlzLmZvY3VzVGltZXIgPSBudWxsO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5jbGVhckJsdXJUaW1lID0gZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKF90aGlzLmJsdXJUaW1lcikge1xuICAgICAgICBjbGVhclRpbWVvdXQoX3RoaXMuYmx1clRpbWVyKTtcbiAgICAgICAgX3RoaXMuYmx1clRpbWVyID0gbnVsbDtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMuY2xlYXJDb21ib2JveFRpbWUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoX3RoaXMuY29tYm9ib3hUaW1lcikge1xuICAgICAgICBjbGVhclRpbWVvdXQoX3RoaXMuY29tYm9ib3hUaW1lcik7XG4gICAgICAgIF90aGlzLmNvbWJvYm94VGltZXIgPSBudWxsO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy51cGRhdGVGb2N1c0NsYXNzTmFtZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciByb290UmVmID0gX3RoaXMucm9vdFJlZjtcbiAgICAgIHZhciBwcm9wcyA9IF90aGlzLnByb3BzOyAvLyBhdm9pZCBzZXRTdGF0ZSBhbmQgaXRzIHNpZGUgZWZmZWN0XG5cbiAgICAgIGlmIChfdGhpcy5fZm9jdXNlZCkge1xuICAgICAgICBjb21wb25lbnRfY2xhc3Nlc18xW1wiZGVmYXVsdFwiXShyb290UmVmKS5hZGQoXCJcIi5jb25jYXQocHJvcHMucHJlZml4Q2xzLCBcIi1mb2N1c2VkXCIpKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbXBvbmVudF9jbGFzc2VzXzFbXCJkZWZhdWx0XCJdKHJvb3RSZWYpLnJlbW92ZShcIlwiLmNvbmNhdChwcm9wcy5wcmVmaXhDbHMsIFwiLWZvY3VzZWRcIikpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5tYXliZUZvY3VzID0gZnVuY3Rpb24gKG9wZW4sIG5lZWRGb2N1cykge1xuICAgICAgaWYgKG5lZWRGb2N1cyB8fCBvcGVuKSB7XG4gICAgICAgIHZhciBpbnB1dCA9IF90aGlzLmdldElucHV0RE9NTm9kZSgpO1xuXG4gICAgICAgIHZhciBfZG9jdW1lbnQgPSBkb2N1bWVudCxcbiAgICAgICAgICAgIGFjdGl2ZUVsZW1lbnQgPSBfZG9jdW1lbnQuYWN0aXZlRWxlbWVudDtcblxuICAgICAgICBpZiAoaW5wdXQgJiYgKG9wZW4gfHwgdXRpbF8xLmlzTXVsdGlwbGVPclRhZ3NPckNvbWJvYm94KF90aGlzLnByb3BzKSkpIHtcbiAgICAgICAgICBpZiAoYWN0aXZlRWxlbWVudCAhPT0gaW5wdXQpIHtcbiAgICAgICAgICAgIGlucHV0LmZvY3VzKCk7XG4gICAgICAgICAgICBfdGhpcy5fZm9jdXNlZCA9IHRydWU7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2UgaWYgKGFjdGl2ZUVsZW1lbnQgIT09IF90aGlzLnNlbGVjdGlvblJlZiAmJiBfdGhpcy5zZWxlY3Rpb25SZWYpIHtcbiAgICAgICAgICBfdGhpcy5zZWxlY3Rpb25SZWYuZm9jdXMoKTtcblxuICAgICAgICAgIF90aGlzLl9mb2N1c2VkID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5yZW1vdmVTZWxlY3RlZCA9IGZ1bmN0aW9uIChzZWxlY3RlZEtleSwgZSkge1xuICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG5cbiAgICAgIGlmIChwcm9wcy5kaXNhYmxlZCB8fCBfdGhpcy5pc0NoaWxkRGlzYWJsZWQoc2VsZWN0ZWRLZXkpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH0gLy8gRG8gbm90IHRyaWdnZXIgVHJpZ2dlciBwb3B1cFxuXG5cbiAgICAgIGlmIChlICYmIGUuc3RvcFByb3BhZ2F0aW9uKSB7XG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICB9XG5cbiAgICAgIHZhciBvbGRWYWx1ZSA9IF90aGlzLnN0YXRlLnZhbHVlO1xuICAgICAgdmFyIHZhbHVlID0gb2xkVmFsdWUuZmlsdGVyKGZ1bmN0aW9uIChzaW5nbGVWYWx1ZSkge1xuICAgICAgICByZXR1cm4gc2luZ2xlVmFsdWUgIT09IHNlbGVjdGVkS2V5O1xuICAgICAgfSk7XG4gICAgICB2YXIgY2FuTXVsdGlwbGUgPSB1dGlsXzEuaXNNdWx0aXBsZU9yVGFncyhwcm9wcyk7XG5cbiAgICAgIGlmIChjYW5NdWx0aXBsZSkge1xuICAgICAgICB2YXIgZXZlbnQgPSBzZWxlY3RlZEtleTtcblxuICAgICAgICBpZiAocHJvcHMubGFiZWxJblZhbHVlKSB7XG4gICAgICAgICAgZXZlbnQgPSB7XG4gICAgICAgICAgICBrZXk6IHNlbGVjdGVkS2V5LFxuICAgICAgICAgICAgbGFiZWw6IF90aGlzLmdldExhYmVsQnlTaW5nbGVWYWx1ZShzZWxlY3RlZEtleSlcbiAgICAgICAgICB9O1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHByb3BzLm9uRGVzZWxlY3QpIHtcbiAgICAgICAgICBwcm9wcy5vbkRlc2VsZWN0KGV2ZW50LCBfdGhpcy5nZXRPcHRpb25CeVNpbmdsZVZhbHVlKHNlbGVjdGVkS2V5KSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgX3RoaXMuZmlyZUNoYW5nZSh2YWx1ZSk7XG4gICAgfTtcblxuICAgIF90aGlzLm9wZW5JZkhhc0NoaWxkcmVuID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG5cbiAgICAgIGlmIChSZWFjdC5DaGlsZHJlbi5jb3VudChwcm9wcy5jaGlsZHJlbikgfHwgdXRpbF8xLmlzU2luZ2xlTW9kZShwcm9wcykpIHtcbiAgICAgICAgX3RoaXMuc2V0T3BlblN0YXRlKHRydWUpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5maXJlU2VsZWN0ID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICBpZiAoX3RoaXMucHJvcHMub25TZWxlY3QpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25TZWxlY3QoX3RoaXMuZ2V0VkxCeVNpbmdsZVZhbHVlKHZhbHVlKSwgX3RoaXMuZ2V0T3B0aW9uQnlTaW5nbGVWYWx1ZSh2YWx1ZSkpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5maXJlQ2hhbmdlID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICB2YXIgcHJvcHMgPSBfdGhpcy5wcm9wcztcblxuICAgICAgaWYgKCEoJ3ZhbHVlJyBpbiBwcm9wcykpIHtcbiAgICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIHZhbHVlOiB2YWx1ZVxuICAgICAgICB9LCBfdGhpcy5mb3JjZVBvcHVwQWxpZ24pO1xuICAgICAgfVxuXG4gICAgICB2YXIgdmxzID0gX3RoaXMuZ2V0VkxGb3JPbkNoYW5nZSh2YWx1ZSk7XG5cbiAgICAgIHZhciBvcHRpb25zID0gX3RoaXMuZ2V0T3B0aW9uc0J5U2luZ2xlVmFsdWUodmFsdWUpO1xuXG4gICAgICBpZiAocHJvcHMub25DaGFuZ2UpIHtcbiAgICAgICAgcHJvcHMub25DaGFuZ2UodmxzLCB1dGlsXzEuaXNNdWx0aXBsZU9yVGFncyhfdGhpcy5wcm9wcykgPyBvcHRpb25zIDogb3B0aW9uc1swXSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLmlzQ2hpbGREaXNhYmxlZCA9IGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHJldHVybiB0b0FycmF5XzFbXCJkZWZhdWx0XCJdKF90aGlzLnByb3BzLmNoaWxkcmVuKS5zb21lKGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgICB2YXIgY2hpbGRWYWx1ZSA9IHV0aWxfMS5nZXRWYWx1ZVByb3BWYWx1ZShjaGlsZCk7XG4gICAgICAgIHJldHVybiBjaGlsZFZhbHVlID09PSBrZXkgJiYgY2hpbGQucHJvcHMgJiYgY2hpbGQucHJvcHMuZGlzYWJsZWQ7XG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgX3RoaXMuZm9yY2VQb3B1cEFsaWduID0gZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKCFfdGhpcy5zdGF0ZS5vcGVuKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKF90aGlzLnNlbGVjdFRyaWdnZXJSZWYgJiYgX3RoaXMuc2VsZWN0VHJpZ2dlclJlZi50cmlnZ2VyUmVmKSB7XG4gICAgICAgIF90aGlzLnNlbGVjdFRyaWdnZXJSZWYudHJpZ2dlclJlZi5mb3JjZVBvcHVwQWxpZ24oKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMucmVuZGVyRmlsdGVyT3B0aW9ucyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBpbnB1dFZhbHVlID0gX3RoaXMuc3RhdGUuaW5wdXRWYWx1ZTtcbiAgICAgIHZhciBfdGhpcyRwcm9wczMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF90aGlzJHByb3BzMy5jaGlsZHJlbixcbiAgICAgICAgICB0YWdzID0gX3RoaXMkcHJvcHMzLnRhZ3MsXG4gICAgICAgICAgbm90Rm91bmRDb250ZW50ID0gX3RoaXMkcHJvcHMzLm5vdEZvdW5kQ29udGVudDtcbiAgICAgIHZhciBtZW51SXRlbXMgPSBbXTtcbiAgICAgIHZhciBjaGlsZHJlbktleXMgPSBbXTtcbiAgICAgIHZhciBlbXB0eSA9IGZhbHNlO1xuXG4gICAgICB2YXIgb3B0aW9ucyA9IF90aGlzLnJlbmRlckZpbHRlck9wdGlvbnNGcm9tQ2hpbGRyZW4oY2hpbGRyZW4sIGNoaWxkcmVuS2V5cywgbWVudUl0ZW1zKTtcblxuICAgICAgaWYgKHRhZ3MpIHtcbiAgICAgICAgLy8gdGFncyB2YWx1ZSBtdXN0IGJlIHN0cmluZ1xuICAgICAgICB2YXIgdmFsdWUgPSBfdGhpcy5zdGF0ZS52YWx1ZTtcbiAgICAgICAgdmFsdWUgPSB2YWx1ZS5maWx0ZXIoZnVuY3Rpb24gKHNpbmdsZVZhbHVlKSB7XG4gICAgICAgICAgcmV0dXJuIGNoaWxkcmVuS2V5cy5pbmRleE9mKHNpbmdsZVZhbHVlKSA9PT0gLTEgJiYgKCFpbnB1dFZhbHVlIHx8IFN0cmluZyhzaW5nbGVWYWx1ZSkuaW5kZXhPZihTdHJpbmcoaW5wdXRWYWx1ZSkpID4gLTEpO1xuICAgICAgICB9KTsgLy8gc29ydCBieSBsZW5ndGhcblxuICAgICAgICB2YWx1ZS5zb3J0KGZ1bmN0aW9uICh2YWwxLCB2YWwyKSB7XG4gICAgICAgICAgcmV0dXJuIHZhbDEubGVuZ3RoIC0gdmFsMi5sZW5ndGg7XG4gICAgICAgIH0pO1xuICAgICAgICB2YWx1ZS5mb3JFYWNoKGZ1bmN0aW9uIChzaW5nbGVWYWx1ZSkge1xuICAgICAgICAgIHZhciBrZXkgPSBzaW5nbGVWYWx1ZTtcbiAgICAgICAgICB2YXIgbWVudUl0ZW0gPSBSZWFjdC5jcmVhdGVFbGVtZW50KHJjX21lbnVfMS5JdGVtLCB7XG4gICAgICAgICAgICBzdHlsZTogdXRpbF8xLlVOU0VMRUNUQUJMRV9TVFlMRSxcbiAgICAgICAgICAgIHJvbGU6IFwib3B0aW9uXCIsXG4gICAgICAgICAgICBhdHRyaWJ1dGU6IHV0aWxfMS5VTlNFTEVDVEFCTEVfQVRUUklCVVRFLFxuICAgICAgICAgICAgdmFsdWU6IGtleSxcbiAgICAgICAgICAgIGtleToga2V5XG4gICAgICAgICAgfSwga2V5KTtcbiAgICAgICAgICBvcHRpb25zLnB1c2gobWVudUl0ZW0pO1xuICAgICAgICAgIG1lbnVJdGVtcy5wdXNoKG1lbnVJdGVtKTtcbiAgICAgICAgfSk7IC8vIHJlZjogaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTQwOTBcblxuICAgICAgICBpZiAoaW5wdXRWYWx1ZSAmJiBtZW51SXRlbXMuZXZlcnkoZnVuY3Rpb24gKG9wdGlvbikge1xuICAgICAgICAgIHJldHVybiB1dGlsXzEuZ2V0VmFsdWVQcm9wVmFsdWUob3B0aW9uKSAhPT0gaW5wdXRWYWx1ZTtcbiAgICAgICAgfSkpIHtcbiAgICAgICAgICBvcHRpb25zLnVuc2hpZnQoUmVhY3QuY3JlYXRlRWxlbWVudChyY19tZW51XzEuSXRlbSwge1xuICAgICAgICAgICAgc3R5bGU6IHV0aWxfMS5VTlNFTEVDVEFCTEVfU1RZTEUsXG4gICAgICAgICAgICByb2xlOiBcIm9wdGlvblwiLFxuICAgICAgICAgICAgYXR0cmlidXRlOiB1dGlsXzEuVU5TRUxFQ1RBQkxFX0FUVFJJQlVURSxcbiAgICAgICAgICAgIHZhbHVlOiBpbnB1dFZhbHVlLFxuICAgICAgICAgICAga2V5OiBpbnB1dFZhbHVlXG4gICAgICAgICAgfSwgaW5wdXRWYWx1ZSkpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmICghb3B0aW9ucy5sZW5ndGggJiYgbm90Rm91bmRDb250ZW50KSB7XG4gICAgICAgIGVtcHR5ID0gdHJ1ZTtcbiAgICAgICAgb3B0aW9ucyA9IFtSZWFjdC5jcmVhdGVFbGVtZW50KHJjX21lbnVfMS5JdGVtLCB7XG4gICAgICAgICAgc3R5bGU6IHV0aWxfMS5VTlNFTEVDVEFCTEVfU1RZTEUsXG4gICAgICAgICAgYXR0cmlidXRlOiB1dGlsXzEuVU5TRUxFQ1RBQkxFX0FUVFJJQlVURSxcbiAgICAgICAgICBkaXNhYmxlZDogdHJ1ZSxcbiAgICAgICAgICByb2xlOiBcIm9wdGlvblwiLFxuICAgICAgICAgIHZhbHVlOiBcIk5PVF9GT1VORFwiLFxuICAgICAgICAgIGtleTogXCJOT1RfRk9VTkRcIlxuICAgICAgICB9LCBub3RGb3VuZENvbnRlbnQpXTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZW1wdHk6IGVtcHR5LFxuICAgICAgICBvcHRpb25zOiBvcHRpb25zXG4gICAgICB9O1xuICAgIH07XG5cbiAgICBfdGhpcy5yZW5kZXJGaWx0ZXJPcHRpb25zRnJvbUNoaWxkcmVuID0gZnVuY3Rpb24gKGNoaWxkcmVuLCBjaGlsZHJlbktleXMsIG1lbnVJdGVtcykge1xuICAgICAgdmFyIHNlbCA9IFtdO1xuICAgICAgdmFyIHByb3BzID0gX3RoaXMucHJvcHM7XG4gICAgICB2YXIgaW5wdXRWYWx1ZSA9IF90aGlzLnN0YXRlLmlucHV0VmFsdWU7XG4gICAgICB2YXIgdGFncyA9IHByb3BzLnRhZ3M7XG4gICAgICBSZWFjdC5DaGlsZHJlbi5mb3JFYWNoKGNoaWxkcmVuLCBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgaWYgKCFjaGlsZCkge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciB0eXBlID0gY2hpbGQudHlwZTtcblxuICAgICAgICBpZiAodHlwZS5pc1NlbGVjdE9wdEdyb3VwKSB7XG4gICAgICAgICAgdmFyIGxhYmVsID0gY2hpbGQucHJvcHMubGFiZWw7XG4gICAgICAgICAgdmFyIGtleSA9IGNoaWxkLmtleTtcblxuICAgICAgICAgIGlmICgha2V5ICYmIHR5cGVvZiBsYWJlbCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgIGtleSA9IGxhYmVsO1xuICAgICAgICAgIH0gZWxzZSBpZiAoIWxhYmVsICYmIGtleSkge1xuICAgICAgICAgICAgbGFiZWwgPSBrZXk7XG4gICAgICAgICAgfSAvLyBNYXRjaCBvcHRpb24gZ3JvdXAgbGFiZWxcblxuXG4gICAgICAgICAgaWYgKGlucHV0VmFsdWUgJiYgX3RoaXMuZmlsdGVyT3B0aW9uKGlucHV0VmFsdWUsIGNoaWxkKSkge1xuICAgICAgICAgICAgdmFyIGlubmVySXRlbXMgPSB0b0FycmF5XzFbXCJkZWZhdWx0XCJdKGNoaWxkLnByb3BzLmNoaWxkcmVuKS5tYXAoZnVuY3Rpb24gKHN1YkNoaWxkKSB7XG4gICAgICAgICAgICAgIHZhciBjaGlsZFZhbHVlU3ViID0gdXRpbF8xLmdldFZhbHVlUHJvcFZhbHVlKHN1YkNoaWxkKSB8fCBzdWJDaGlsZC5rZXk7XG4gICAgICAgICAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KHJjX21lbnVfMS5JdGVtLCBfZXh0ZW5kcyh7XG4gICAgICAgICAgICAgICAga2V5OiBjaGlsZFZhbHVlU3ViLFxuICAgICAgICAgICAgICAgIHZhbHVlOiBjaGlsZFZhbHVlU3ViXG4gICAgICAgICAgICAgIH0sIHN1YkNoaWxkLnByb3BzKSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHNlbC5wdXNoKFJlYWN0LmNyZWF0ZUVsZW1lbnQocmNfbWVudV8xLkl0ZW1Hcm91cCwge1xuICAgICAgICAgICAgICBrZXk6IGtleSxcbiAgICAgICAgICAgICAgdGl0bGU6IGxhYmVsXG4gICAgICAgICAgICB9LCBpbm5lckl0ZW1zKSk7IC8vIE5vdCBtYXRjaFxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB2YXIgX2lubmVySXRlbXMgPSBfdGhpcy5yZW5kZXJGaWx0ZXJPcHRpb25zRnJvbUNoaWxkcmVuKGNoaWxkLnByb3BzLmNoaWxkcmVuLCBjaGlsZHJlbktleXMsIG1lbnVJdGVtcyk7XG5cbiAgICAgICAgICAgIGlmIChfaW5uZXJJdGVtcy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgc2VsLnB1c2goUmVhY3QuY3JlYXRlRWxlbWVudChyY19tZW51XzEuSXRlbUdyb3VwLCB7XG4gICAgICAgICAgICAgICAga2V5OiBrZXksXG4gICAgICAgICAgICAgICAgdGl0bGU6IGxhYmVsXG4gICAgICAgICAgICAgIH0sIF9pbm5lckl0ZW1zKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgd2FybmluZ18xW1wiZGVmYXVsdFwiXSh0eXBlLmlzU2VsZWN0T3B0aW9uLCAndGhlIGNoaWxkcmVuIG9mIGBTZWxlY3RgIHNob3VsZCBiZSBgU2VsZWN0Lk9wdGlvbmAgb3IgYFNlbGVjdC5PcHRHcm91cGAsICcgKyBcImluc3RlYWQgb2YgYFwiLmNvbmNhdCh0eXBlLm5hbWUgfHwgdHlwZS5kaXNwbGF5TmFtZSB8fCBjaGlsZC50eXBlLCBcImAuXCIpKTtcbiAgICAgICAgdmFyIGNoaWxkVmFsdWUgPSB1dGlsXzEuZ2V0VmFsdWVQcm9wVmFsdWUoY2hpbGQpO1xuICAgICAgICB1dGlsXzEudmFsaWRhdGVPcHRpb25WYWx1ZShjaGlsZFZhbHVlLCBfdGhpcy5wcm9wcyk7XG5cbiAgICAgICAgaWYgKF90aGlzLmZpbHRlck9wdGlvbihpbnB1dFZhbHVlLCBjaGlsZCkpIHtcbiAgICAgICAgICB2YXIgbWVudUl0ZW0gPSBSZWFjdC5jcmVhdGVFbGVtZW50KHJjX21lbnVfMS5JdGVtLCBfZXh0ZW5kcyh7XG4gICAgICAgICAgICBzdHlsZTogdXRpbF8xLlVOU0VMRUNUQUJMRV9TVFlMRSxcbiAgICAgICAgICAgIGF0dHJpYnV0ZTogdXRpbF8xLlVOU0VMRUNUQUJMRV9BVFRSSUJVVEUsXG4gICAgICAgICAgICB2YWx1ZTogY2hpbGRWYWx1ZSxcbiAgICAgICAgICAgIGtleTogY2hpbGRWYWx1ZSxcbiAgICAgICAgICAgIHJvbGU6IFwib3B0aW9uXCJcbiAgICAgICAgICB9LCBjaGlsZC5wcm9wcykpO1xuICAgICAgICAgIHNlbC5wdXNoKG1lbnVJdGVtKTtcbiAgICAgICAgICBtZW51SXRlbXMucHVzaChtZW51SXRlbSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGFncykge1xuICAgICAgICAgIGNoaWxkcmVuS2V5cy5wdXNoKGNoaWxkVmFsdWUpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBzZWw7XG4gICAgfTtcblxuICAgIF90aGlzLnJlbmRlclRvcENvbnRyb2xOb2RlID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIF90aGlzJHN0YXRlID0gX3RoaXMuc3RhdGUsXG4gICAgICAgICAgb3BlbiA9IF90aGlzJHN0YXRlLm9wZW4sXG4gICAgICAgICAgaW5wdXRWYWx1ZSA9IF90aGlzJHN0YXRlLmlucHV0VmFsdWU7XG4gICAgICB2YXIgdmFsdWUgPSBfdGhpcy5zdGF0ZS52YWx1ZTtcbiAgICAgIHZhciBwcm9wcyA9IF90aGlzLnByb3BzO1xuICAgICAgdmFyIGNob2ljZVRyYW5zaXRpb25OYW1lID0gcHJvcHMuY2hvaWNlVHJhbnNpdGlvbk5hbWUsXG4gICAgICAgICAgcHJlZml4Q2xzID0gcHJvcHMucHJlZml4Q2xzLFxuICAgICAgICAgIG1heFRhZ1RleHRMZW5ndGggPSBwcm9wcy5tYXhUYWdUZXh0TGVuZ3RoLFxuICAgICAgICAgIG1heFRhZ0NvdW50ID0gcHJvcHMubWF4VGFnQ291bnQsXG4gICAgICAgICAgc2hvd1NlYXJjaCA9IHByb3BzLnNob3dTZWFyY2gsXG4gICAgICAgICAgcmVtb3ZlSWNvbiA9IHByb3BzLnJlbW92ZUljb247XG4gICAgICB2YXIgbWF4VGFnUGxhY2Vob2xkZXIgPSBwcm9wcy5tYXhUYWdQbGFjZWhvbGRlcjtcbiAgICAgIHZhciBjbGFzc05hbWUgPSBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNlbGVjdGlvbl9fcmVuZGVyZWRcIik7IC8vIHNlYXJjaCBpbnB1dCBpcyBpbnNpZGUgdG9wQ29udHJvbE5vZGUgaW4gc2luZ2xlLCBtdWx0aXBsZSAmIGNvbWJvYm94LiAyMDE2LzA0LzEzXG5cbiAgICAgIHZhciBpbm5lck5vZGUgPSBudWxsO1xuXG4gICAgICBpZiAodXRpbF8xLmlzU2luZ2xlTW9kZShwcm9wcykpIHtcbiAgICAgICAgdmFyIHNlbGVjdGVkVmFsdWUgPSBudWxsO1xuXG4gICAgICAgIGlmICh2YWx1ZS5sZW5ndGgpIHtcbiAgICAgICAgICB2YXIgc2hvd1NlbGVjdGVkVmFsdWUgPSBmYWxzZTtcbiAgICAgICAgICB2YXIgb3BhY2l0eSA9IDE7XG5cbiAgICAgICAgICBpZiAoIXNob3dTZWFyY2gpIHtcbiAgICAgICAgICAgIHNob3dTZWxlY3RlZFZhbHVlID0gdHJ1ZTtcbiAgICAgICAgICB9IGVsc2UgaWYgKG9wZW4pIHtcbiAgICAgICAgICAgIHNob3dTZWxlY3RlZFZhbHVlID0gIWlucHV0VmFsdWU7XG5cbiAgICAgICAgICAgIGlmIChzaG93U2VsZWN0ZWRWYWx1ZSkge1xuICAgICAgICAgICAgICBvcGFjaXR5ID0gMC40O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBzaG93U2VsZWN0ZWRWYWx1ZSA9IHRydWU7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdmFyIHNpbmdsZVZhbHVlID0gdmFsdWVbMF07XG5cbiAgICAgICAgICB2YXIgX3RoaXMkZ2V0T3B0aW9uSW5mb0J5MyA9IF90aGlzLmdldE9wdGlvbkluZm9CeVNpbmdsZVZhbHVlKHNpbmdsZVZhbHVlKSxcbiAgICAgICAgICAgICAgbGFiZWwgPSBfdGhpcyRnZXRPcHRpb25JbmZvQnkzLmxhYmVsLFxuICAgICAgICAgICAgICB0aXRsZSA9IF90aGlzJGdldE9wdGlvbkluZm9CeTMudGl0bGU7XG5cbiAgICAgICAgICBzZWxlY3RlZFZhbHVlID0gUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgICAgICBrZXk6IFwidmFsdWVcIixcbiAgICAgICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zZWxlY3Rpb24tc2VsZWN0ZWQtdmFsdWVcIiksXG4gICAgICAgICAgICB0aXRsZTogdXRpbF8xLnRvVGl0bGUodGl0bGUgfHwgbGFiZWwpLFxuICAgICAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICAgICAgZGlzcGxheTogc2hvd1NlbGVjdGVkVmFsdWUgPyAnYmxvY2snIDogJ25vbmUnLFxuICAgICAgICAgICAgICBvcGFjaXR5OiBvcGFjaXR5XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSwgbGFiZWwpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFzaG93U2VhcmNoKSB7XG4gICAgICAgICAgaW5uZXJOb2RlID0gW3NlbGVjdGVkVmFsdWVdO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlubmVyTm9kZSA9IFtzZWxlY3RlZFZhbHVlLCBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtcbiAgICAgICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zZWFyY2ggXCIpLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNlYXJjaC0taW5saW5lXCIpLFxuICAgICAgICAgICAga2V5OiBcImlucHV0XCIsXG4gICAgICAgICAgICBzdHlsZToge1xuICAgICAgICAgICAgICBkaXNwbGF5OiBvcGVuID8gJ2Jsb2NrJyA6ICdub25lJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sIF90aGlzLmdldElucHV0RWxlbWVudCgpKV07XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBzZWxlY3RlZFZhbHVlTm9kZXMgPSBbXTtcbiAgICAgICAgdmFyIGxpbWl0ZWRDb3VudFZhbHVlID0gdmFsdWU7XG4gICAgICAgIHZhciBtYXhUYWdQbGFjZWhvbGRlckVsO1xuXG4gICAgICAgIGlmIChtYXhUYWdDb3VudCAhPT0gdW5kZWZpbmVkICYmIHZhbHVlLmxlbmd0aCA+IG1heFRhZ0NvdW50KSB7XG4gICAgICAgICAgbGltaXRlZENvdW50VmFsdWUgPSBsaW1pdGVkQ291bnRWYWx1ZS5zbGljZSgwLCBtYXhUYWdDb3VudCk7XG5cbiAgICAgICAgICB2YXIgb21pdHRlZFZhbHVlcyA9IF90aGlzLmdldFZMRm9yT25DaGFuZ2UodmFsdWUuc2xpY2UobWF4VGFnQ291bnQsIHZhbHVlLmxlbmd0aCkpO1xuXG4gICAgICAgICAgdmFyIGNvbnRlbnQgPSBcIisgXCIuY29uY2F0KHZhbHVlLmxlbmd0aCAtIG1heFRhZ0NvdW50LCBcIiAuLi5cIik7XG5cbiAgICAgICAgICBpZiAobWF4VGFnUGxhY2Vob2xkZXIpIHtcbiAgICAgICAgICAgIGNvbnRlbnQgPSB0eXBlb2YgbWF4VGFnUGxhY2Vob2xkZXIgPT09ICdmdW5jdGlvbicgPyBtYXhUYWdQbGFjZWhvbGRlcihvbWl0dGVkVmFsdWVzKSA6IG1heFRhZ1BsYWNlaG9sZGVyO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIG1heFRhZ1BsYWNlaG9sZGVyRWwgPSBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwgX2V4dGVuZHMoe1xuICAgICAgICAgICAgc3R5bGU6IHV0aWxfMS5VTlNFTEVDVEFCTEVfU1RZTEVcbiAgICAgICAgICB9LCB1dGlsXzEuVU5TRUxFQ1RBQkxFX0FUVFJJQlVURSwge1xuICAgICAgICAgICAgcm9sZTogXCJwcmVzZW50YXRpb25cIixcbiAgICAgICAgICAgIG9uTW91c2VEb3duOiB1dGlsXzEucHJldmVudERlZmF1bHRFdmVudCxcbiAgICAgICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zZWxlY3Rpb25fX2Nob2ljZSBcIikuY29uY2F0KHByZWZpeENscywgXCItc2VsZWN0aW9uX19jaG9pY2VfX2Rpc2FibGVkXCIpLFxuICAgICAgICAgICAga2V5OiBcIm1heFRhZ1BsYWNlaG9sZGVyXCIsXG4gICAgICAgICAgICB0aXRsZTogdXRpbF8xLnRvVGl0bGUoY29udGVudClcbiAgICAgICAgICB9KSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgICAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItc2VsZWN0aW9uX19jaG9pY2VfX2NvbnRlbnRcIilcbiAgICAgICAgICB9LCBjb250ZW50KSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodXRpbF8xLmlzTXVsdGlwbGVPclRhZ3MocHJvcHMpKSB7XG4gICAgICAgICAgc2VsZWN0ZWRWYWx1ZU5vZGVzID0gbGltaXRlZENvdW50VmFsdWUubWFwKGZ1bmN0aW9uIChzaW5nbGVWYWx1ZSkge1xuICAgICAgICAgICAgdmFyIGluZm8gPSBfdGhpcy5nZXRPcHRpb25JbmZvQnlTaW5nbGVWYWx1ZShzaW5nbGVWYWx1ZSk7XG5cbiAgICAgICAgICAgIHZhciBjb250ZW50ID0gaW5mby5sYWJlbDtcbiAgICAgICAgICAgIHZhciB0aXRsZSA9IGluZm8udGl0bGUgfHwgY29udGVudDtcblxuICAgICAgICAgICAgaWYgKG1heFRhZ1RleHRMZW5ndGggJiYgdHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnICYmIGNvbnRlbnQubGVuZ3RoID4gbWF4VGFnVGV4dExlbmd0aCkge1xuICAgICAgICAgICAgICBjb250ZW50ID0gXCJcIi5jb25jYXQoY29udGVudC5zbGljZSgwLCBtYXhUYWdUZXh0TGVuZ3RoKSwgXCIuLi5cIik7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHZhciBkaXNhYmxlZCA9IF90aGlzLmlzQ2hpbGREaXNhYmxlZChzaW5nbGVWYWx1ZSk7XG5cbiAgICAgICAgICAgIHZhciBjaG9pY2VDbGFzc05hbWUgPSBkaXNhYmxlZCA/IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItc2VsZWN0aW9uX19jaG9pY2UgXCIpLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNlbGVjdGlvbl9fY2hvaWNlX19kaXNhYmxlZFwiKSA6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItc2VsZWN0aW9uX19jaG9pY2VcIik7XG4gICAgICAgICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcImxpXCIsIF9leHRlbmRzKHtcbiAgICAgICAgICAgICAgc3R5bGU6IHV0aWxfMS5VTlNFTEVDVEFCTEVfU1RZTEVcbiAgICAgICAgICAgIH0sIHV0aWxfMS5VTlNFTEVDVEFCTEVfQVRUUklCVVRFLCB7XG4gICAgICAgICAgICAgIG9uTW91c2VEb3duOiB1dGlsXzEucHJldmVudERlZmF1bHRFdmVudCxcbiAgICAgICAgICAgICAgY2xhc3NOYW1lOiBjaG9pY2VDbGFzc05hbWUsXG4gICAgICAgICAgICAgIHJvbGU6IFwicHJlc2VudGF0aW9uXCIsXG4gICAgICAgICAgICAgIGtleTogc2luZ2xlVmFsdWUgfHwgU0VMRUNUX0VNUFRZX1ZBTFVFX0tFWSxcbiAgICAgICAgICAgICAgdGl0bGU6IHV0aWxfMS50b1RpdGxlKHRpdGxlKVxuICAgICAgICAgICAgfSksIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge1xuICAgICAgICAgICAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItc2VsZWN0aW9uX19jaG9pY2VfX2NvbnRlbnRcIilcbiAgICAgICAgICAgIH0sIGNvbnRlbnQpLCBkaXNhYmxlZCA/IG51bGwgOiBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7XG4gICAgICAgICAgICAgIG9uQ2xpY2s6IGZ1bmN0aW9uIG9uQ2xpY2soZXZlbnQpIHtcbiAgICAgICAgICAgICAgICBfdGhpcy5yZW1vdmVTZWxlY3RlZChzaW5nbGVWYWx1ZSwgZXZlbnQpO1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItc2VsZWN0aW9uX19jaG9pY2VfX3JlbW92ZVwiKVxuICAgICAgICAgICAgfSwgcmVtb3ZlSWNvbiB8fCBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaVwiLCB7XG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zZWxlY3Rpb25fX2Nob2ljZV9fcmVtb3ZlLWljb25cIilcbiAgICAgICAgICAgIH0sIFwiXFx4RDdcIikpKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChtYXhUYWdQbGFjZWhvbGRlckVsKSB7XG4gICAgICAgICAgc2VsZWN0ZWRWYWx1ZU5vZGVzLnB1c2gobWF4VGFnUGxhY2Vob2xkZXJFbCk7XG4gICAgICAgIH1cblxuICAgICAgICBzZWxlY3RlZFZhbHVlTm9kZXMucHVzaChSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwge1xuICAgICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zZWFyY2ggXCIpLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNlYXJjaC0taW5saW5lXCIpLFxuICAgICAgICAgIGtleTogXCJfX2lucHV0XCJcbiAgICAgICAgfSwgX3RoaXMuZ2V0SW5wdXRFbGVtZW50KCkpKTtcblxuICAgICAgICBpZiAodXRpbF8xLmlzTXVsdGlwbGVPclRhZ3MocHJvcHMpICYmIGNob2ljZVRyYW5zaXRpb25OYW1lKSB7XG4gICAgICAgICAgaW5uZXJOb2RlID0gUmVhY3QuY3JlYXRlRWxlbWVudChyY19hbmltYXRlXzFbXCJkZWZhdWx0XCJdLCB7XG4gICAgICAgICAgICBvbkxlYXZlOiBfdGhpcy5vbkNob2ljZUFuaW1hdGlvbkxlYXZlLFxuICAgICAgICAgICAgY29tcG9uZW50OiBcInVsXCIsXG4gICAgICAgICAgICB0cmFuc2l0aW9uTmFtZTogY2hvaWNlVHJhbnNpdGlvbk5hbWVcbiAgICAgICAgICB9LCBzZWxlY3RlZFZhbHVlTm9kZXMpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlubmVyTm9kZSA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJ1bFwiLCBudWxsLCBzZWxlY3RlZFZhbHVlTm9kZXMpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtcbiAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICAgIHJlZjogX3RoaXMuc2F2ZVRvcEN0cmxSZWZcbiAgICAgIH0sIF90aGlzLmdldFBsYWNlaG9sZGVyRWxlbWVudCgpLCBpbm5lck5vZGUpO1xuICAgIH07XG5cbiAgICB2YXIgb3B0aW9uc0luZm8gPSBTZWxlY3QuZ2V0T3B0aW9uc0luZm9Gcm9tUHJvcHMocHJvcHMpO1xuXG4gICAgaWYgKHByb3BzLnRhZ3MgJiYgdHlwZW9mIHByb3BzLmZpbHRlck9wdGlvbiAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgdmFyIGlzRGlzYWJsZWRFeGlzdCA9IE9iamVjdC5rZXlzKG9wdGlvbnNJbmZvKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgcmV0dXJuIG9wdGlvbnNJbmZvW2tleV0uZGlzYWJsZWQ7XG4gICAgICB9KTtcbiAgICAgIHdhcm5pbmdfMVtcImRlZmF1bHRcIl0oIWlzRGlzYWJsZWRFeGlzdCwgJ1BsZWFzZSBhdm9pZCBzZXR0aW5nIG9wdGlvbiB0byBkaXNhYmxlZCBpbiB0YWdzIG1vZGUgc2luY2UgdXNlciBjYW4gYWx3YXlzIHR5cGUgdGV4dCBhcyB0YWcuJyk7XG4gICAgfVxuXG4gICAgX3RoaXMuc3RhdGUgPSB7XG4gICAgICB2YWx1ZTogU2VsZWN0LmdldFZhbHVlRnJvbVByb3BzKHByb3BzLCB0cnVlKSxcbiAgICAgIGlucHV0VmFsdWU6IHByb3BzLmNvbWJvYm94ID8gU2VsZWN0LmdldElucHV0VmFsdWVGb3JDb21ib2JveChwcm9wcywgb3B0aW9uc0luZm8sIHRydWUpIDogJycsXG4gICAgICBvcGVuOiBwcm9wcy5kZWZhdWx0T3BlbixcbiAgICAgIG9wdGlvbnNJbmZvOiBvcHRpb25zSW5mbyxcbiAgICAgIGJhY2tmaWxsVmFsdWU6ICcnLFxuICAgICAgLy8gYSBmbGFnIGZvciBhdmlvZCByZWR1bmRhbnQgZ2V0T3B0aW9uc0luZm9Gcm9tUHJvcHMgY2FsbFxuICAgICAgc2tpcEJ1aWxkT3B0aW9uc0luZm86IHRydWUsXG4gICAgICBhcmlhSWQ6ICcnXG4gICAgfTtcbiAgICBfdGhpcy5zYXZlSW5wdXRSZWYgPSB1dGlsXzEuc2F2ZVJlZihfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgJ2lucHV0UmVmJyk7XG4gICAgX3RoaXMuc2F2ZUlucHV0TWlycm9yUmVmID0gdXRpbF8xLnNhdmVSZWYoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksICdpbnB1dE1pcnJvclJlZicpO1xuICAgIF90aGlzLnNhdmVUb3BDdHJsUmVmID0gdXRpbF8xLnNhdmVSZWYoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksICd0b3BDdHJsUmVmJyk7XG4gICAgX3RoaXMuc2F2ZVNlbGVjdFRyaWdnZXJSZWYgPSB1dGlsXzEuc2F2ZVJlZihfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgJ3NlbGVjdFRyaWdnZXJSZWYnKTtcbiAgICBfdGhpcy5zYXZlUm9vdFJlZiA9IHV0aWxfMS5zYXZlUmVmKF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCAncm9vdFJlZicpO1xuICAgIF90aGlzLnNhdmVTZWxlY3Rpb25SZWYgPSB1dGlsXzEuc2F2ZVJlZihfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgJ3NlbGVjdGlvblJlZicpO1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhTZWxlY3QsIFt7XG4gICAga2V5OiBcImNvbXBvbmVudERpZE1vdW50XCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgLy8gd2hlbiBkZWZhdWx0T3BlbiBpcyB0cnVlLCB3ZSBzaG91bGQgYXV0byBmb2N1cyBzZWFyY2ggaW5wdXRcbiAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzE0MjU0XG4gICAgICBpZiAodGhpcy5wcm9wcy5hdXRvRm9jdXMgfHwgdGhpcy5zdGF0ZS5vcGVuKSB7XG4gICAgICAgIHRoaXMuZm9jdXMoKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGFyaWFJZDogdXRpbF8xLmdlbmVyYXRlVVVJRCgpXG4gICAgICB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiY29tcG9uZW50RGlkVXBkYXRlXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICAgIGlmICh1dGlsXzEuaXNNdWx0aXBsZU9yVGFncyh0aGlzLnByb3BzKSkge1xuICAgICAgICB2YXIgaW5wdXROb2RlID0gdGhpcy5nZXRJbnB1dERPTU5vZGUoKTtcbiAgICAgICAgdmFyIG1pcnJvck5vZGUgPSB0aGlzLmdldElucHV0TWlycm9yRE9NTm9kZSgpO1xuXG4gICAgICAgIGlmIChpbnB1dE5vZGUgJiYgaW5wdXROb2RlLnZhbHVlICYmIG1pcnJvck5vZGUpIHtcbiAgICAgICAgICBpbnB1dE5vZGUuc3R5bGUud2lkdGggPSAnJztcbiAgICAgICAgICBpbnB1dE5vZGUuc3R5bGUud2lkdGggPSBcIlwiLmNvbmNhdChtaXJyb3JOb2RlLmNsaWVudFdpZHRoLCBcInB4XCIpO1xuICAgICAgICB9IGVsc2UgaWYgKGlucHV0Tm9kZSkge1xuICAgICAgICAgIGlucHV0Tm9kZS5zdHlsZS53aWR0aCA9ICcnO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHRoaXMuZm9yY2VQb3B1cEFsaWduKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcImNvbXBvbmVudFdpbGxVbm1vdW50XCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgdGhpcy5jbGVhckZvY3VzVGltZSgpO1xuICAgICAgdGhpcy5jbGVhckJsdXJUaW1lKCk7XG4gICAgICB0aGlzLmNsZWFyQ29tYm9ib3hUaW1lKCk7XG5cbiAgICAgIGlmICh0aGlzLmRyb3Bkb3duQ29udGFpbmVyKSB7XG4gICAgICAgIFJlYWN0RE9NLnVubW91bnRDb21wb25lbnRBdE5vZGUodGhpcy5kcm9wZG93bkNvbnRhaW5lcik7XG4gICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQodGhpcy5kcm9wZG93bkNvbnRhaW5lcik7XG4gICAgICAgIHRoaXMuZHJvcGRvd25Db250YWluZXIgPSBudWxsO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJmb2N1c1wiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBmb2N1cygpIHtcbiAgICAgIGlmICh1dGlsXzEuaXNTaW5nbGVNb2RlKHRoaXMucHJvcHMpICYmIHRoaXMuc2VsZWN0aW9uUmVmKSB7XG4gICAgICAgIHRoaXMuc2VsZWN0aW9uUmVmLmZvY3VzKCk7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMuZ2V0SW5wdXRET01Ob2RlKCkpIHtcbiAgICAgICAgdGhpcy5nZXRJbnB1dERPTU5vZGUoKS5mb2N1cygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJibHVyXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGJsdXIoKSB7XG4gICAgICBpZiAodXRpbF8xLmlzU2luZ2xlTW9kZSh0aGlzLnByb3BzKSAmJiB0aGlzLnNlbGVjdGlvblJlZikge1xuICAgICAgICB0aGlzLnNlbGVjdGlvblJlZi5ibHVyKCk7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMuZ2V0SW5wdXRET01Ob2RlKCkpIHtcbiAgICAgICAgdGhpcy5nZXRJbnB1dERPTU5vZGUoKS5ibHVyKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInJlbmRlckFycm93XCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlckFycm93KG11bHRpcGxlKSB7XG4gICAgICAvLyBzaG93QXJyb3cgOiBTZXQgdG8gdHJ1ZSBpZiBub3QgbXVsdGlwbGUgYnkgZGVmYXVsdCBidXQga2VlcCBzZXQgdmFsdWUuXG4gICAgICB2YXIgX3RoaXMkcHJvcHM0ID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBfdGhpcyRwcm9wczQkc2hvd0Fycm8gPSBfdGhpcyRwcm9wczQuc2hvd0Fycm93LFxuICAgICAgICAgIHNob3dBcnJvdyA9IF90aGlzJHByb3BzNCRzaG93QXJybyA9PT0gdm9pZCAwID8gIW11bHRpcGxlIDogX3RoaXMkcHJvcHM0JHNob3dBcnJvLFxuICAgICAgICAgIGxvYWRpbmcgPSBfdGhpcyRwcm9wczQubG9hZGluZyxcbiAgICAgICAgICBpbnB1dEljb24gPSBfdGhpcyRwcm9wczQuaW5wdXRJY29uLFxuICAgICAgICAgIHByZWZpeENscyA9IF90aGlzJHByb3BzNC5wcmVmaXhDbHM7XG5cbiAgICAgIGlmICghc2hvd0Fycm93ICYmICFsb2FkaW5nKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfSAvLyBpZiBsb2FkaW5nICBoYXZlIGxvYWRpbmcgaWNvblxuXG5cbiAgICAgIHZhciBkZWZhdWx0SWNvbiA9IGxvYWRpbmcgPyBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaVwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1hcnJvdy1sb2FkaW5nXCIpXG4gICAgICB9KSA6IFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpXCIsIHtcbiAgICAgICAgY2xhc3NOYW1lOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWFycm93LWljb25cIilcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIF9leHRlbmRzKHtcbiAgICAgICAga2V5OiBcImFycm93XCIsXG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1hcnJvd1wiKSxcbiAgICAgICAgc3R5bGU6IHV0aWxfMS5VTlNFTEVDVEFCTEVfU1RZTEVcbiAgICAgIH0sIHV0aWxfMS5VTlNFTEVDVEFCTEVfQVRUUklCVVRFLCB7XG4gICAgICAgIG9uQ2xpY2s6IHRoaXMub25BcnJvd0NsaWNrXG4gICAgICB9KSwgaW5wdXRJY29uIHx8IGRlZmF1bHRJY29uKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwicmVuZGVyQ2xlYXJcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyQ2xlYXIoKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHM1ID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBwcmVmaXhDbHMgPSBfdGhpcyRwcm9wczUucHJlZml4Q2xzLFxuICAgICAgICAgIGFsbG93Q2xlYXIgPSBfdGhpcyRwcm9wczUuYWxsb3dDbGVhcixcbiAgICAgICAgICBjbGVhckljb24gPSBfdGhpcyRwcm9wczUuY2xlYXJJY29uO1xuICAgICAgdmFyIGlucHV0VmFsdWUgPSB0aGlzLnN0YXRlLmlucHV0VmFsdWU7XG4gICAgICB2YXIgdmFsdWUgPSB0aGlzLnN0YXRlLnZhbHVlO1xuICAgICAgdmFyIGNsZWFyID0gUmVhY3QuY3JlYXRlRWxlbWVudChcInNwYW5cIiwgX2V4dGVuZHMoe1xuICAgICAgICBrZXk6IFwiY2xlYXJcIixcbiAgICAgICAgY2xhc3NOYW1lOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNlbGVjdGlvbl9fY2xlYXJcIiksXG4gICAgICAgIG9uTW91c2VEb3duOiB1dGlsXzEucHJldmVudERlZmF1bHRFdmVudCxcbiAgICAgICAgc3R5bGU6IHV0aWxfMS5VTlNFTEVDVEFCTEVfU1RZTEVcbiAgICAgIH0sIHV0aWxfMS5VTlNFTEVDVEFCTEVfQVRUUklCVVRFLCB7XG4gICAgICAgIG9uQ2xpY2s6IHRoaXMub25DbGVhclNlbGVjdGlvblxuICAgICAgfSksIGNsZWFySWNvbiB8fCBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaVwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zZWxlY3Rpb25fX2NsZWFyLWljb25cIilcbiAgICAgIH0sIFwiXFx4RDdcIikpO1xuXG4gICAgICBpZiAoIWFsbG93Q2xlYXIpIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG5cbiAgICAgIGlmICh1dGlsXzEuaXNDb21ib2JveCh0aGlzLnByb3BzKSkge1xuICAgICAgICBpZiAoaW5wdXRWYWx1ZSkge1xuICAgICAgICAgIHJldHVybiBjbGVhcjtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICBpZiAoaW5wdXRWYWx1ZSB8fCB2YWx1ZS5sZW5ndGgpIHtcbiAgICAgICAgcmV0dXJuIGNsZWFyO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwicmVuZGVyXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcm9vdENscztcblxuICAgICAgdmFyIHByb3BzID0gdGhpcy5wcm9wcztcbiAgICAgIHZhciBtdWx0aXBsZSA9IHV0aWxfMS5pc011bHRpcGxlT3JUYWdzKHByb3BzKTsgLy8gRGVmYXVsdCBzZXQgc2hvd0Fycm93IHRvIHRydWUgaWYgbm90IHNldCAobm90IHNldCBkaXJlY3RseSBpbiBkZWZhdWx0UHJvcHMgdG8gaGFuZGxlIG11bHRpcGxlIGNhc2UpXG5cbiAgICAgIHZhciBfcHJvcHMkc2hvd0Fycm93ID0gcHJvcHMuc2hvd0Fycm93LFxuICAgICAgICAgIHNob3dBcnJvdyA9IF9wcm9wcyRzaG93QXJyb3cgPT09IHZvaWQgMCA/IHRydWUgOiBfcHJvcHMkc2hvd0Fycm93O1xuICAgICAgdmFyIHN0YXRlID0gdGhpcy5zdGF0ZTtcbiAgICAgIHZhciBjbGFzc05hbWUgPSBwcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgZGlzYWJsZWQgPSBwcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICBwcmVmaXhDbHMgPSBwcm9wcy5wcmVmaXhDbHMsXG4gICAgICAgICAgbG9hZGluZyA9IHByb3BzLmxvYWRpbmc7XG4gICAgICB2YXIgY3RybE5vZGUgPSB0aGlzLnJlbmRlclRvcENvbnRyb2xOb2RlKCk7XG4gICAgICB2YXIgX3RoaXMkc3RhdGUyID0gdGhpcy5zdGF0ZSxcbiAgICAgICAgICBvcGVuID0gX3RoaXMkc3RhdGUyLm9wZW4sXG4gICAgICAgICAgYXJpYUlkID0gX3RoaXMkc3RhdGUyLmFyaWFJZDtcblxuICAgICAgaWYgKG9wZW4pIHtcbiAgICAgICAgdmFyIGZpbHRlck9wdGlvbnMgPSB0aGlzLnJlbmRlckZpbHRlck9wdGlvbnMoKTtcbiAgICAgICAgdGhpcy5fZW1wdHkgPSBmaWx0ZXJPcHRpb25zLmVtcHR5O1xuICAgICAgICB0aGlzLl9vcHRpb25zID0gZmlsdGVyT3B0aW9ucy5vcHRpb25zO1xuICAgICAgfVxuXG4gICAgICB2YXIgcmVhbE9wZW4gPSB0aGlzLmdldFJlYWxPcGVuU3RhdGUoKTtcbiAgICAgIHZhciBlbXB0eSA9IHRoaXMuX2VtcHR5O1xuICAgICAgdmFyIG9wdGlvbnMgPSB0aGlzLl9vcHRpb25zIHx8IFtdO1xuICAgICAgdmFyIGRhdGFPckFyaWFBdHRyaWJ1dGVQcm9wcyA9IHt9O1xuICAgICAgT2JqZWN0LmtleXMocHJvcHMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgICBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHByb3BzLCBrZXkpICYmIChrZXkuc3Vic3RyKDAsIDUpID09PSAnZGF0YS0nIHx8IGtleS5zdWJzdHIoMCwgNSkgPT09ICdhcmlhLScgfHwga2V5ID09PSAncm9sZScpKSB7XG4gICAgICAgICAgZGF0YU9yQXJpYUF0dHJpYnV0ZVByb3BzW2tleV0gPSBwcm9wc1trZXldO1xuICAgICAgICB9XG4gICAgICB9KTsgLy8gZm9yIChjb25zdCBrZXkgaW4gcHJvcHMpIHtcbiAgICAgIC8vICAgaWYgKFxuICAgICAgLy8gICAgIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChwcm9wcywga2V5KSAmJlxuICAgICAgLy8gICAgIChrZXkuc3Vic3RyKDAsIDUpID09PSAnZGF0YS0nIHx8IGtleS5zdWJzdHIoMCwgNSkgPT09ICdhcmlhLScgfHwga2V5ID09PSAncm9sZScpXG4gICAgICAvLyAgICkge1xuICAgICAgLy8gICAgIGRhdGFPckFyaWFBdHRyaWJ1dGVQcm9wc1trZXldID0gcHJvcHNba2V5XTtcbiAgICAgIC8vICAgfVxuICAgICAgLy8gfVxuXG4gICAgICB2YXIgZXh0cmFTZWxlY3Rpb25Qcm9wcyA9IF9vYmplY3RTcHJlYWQoe30sIGRhdGFPckFyaWFBdHRyaWJ1dGVQcm9wcyk7XG5cbiAgICAgIGlmICghdXRpbF8xLmlzTXVsdGlwbGVPclRhZ3NPckNvbWJvYm94KHByb3BzKSkge1xuICAgICAgICBleHRyYVNlbGVjdGlvblByb3BzID0gX29iamVjdFNwcmVhZCh7fSwgZXh0cmFTZWxlY3Rpb25Qcm9wcywge1xuICAgICAgICAgIG9uS2V5RG93bjogdGhpcy5vbktleURvd24sXG4gICAgICAgICAgdGFiSW5kZXg6IHByb3BzLmRpc2FibGVkID8gLTEgOiBwcm9wcy50YWJJbmRleFxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgdmFyIHJvb3RDbHMgPSAoX3Jvb3RDbHMgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9yb290Q2xzLCBjbGFzc05hbWUsICEhY2xhc3NOYW1lKSwgX2RlZmluZVByb3BlcnR5KF9yb290Q2xzLCBwcmVmaXhDbHMsIDEpLCBfZGVmaW5lUHJvcGVydHkoX3Jvb3RDbHMsIFwiXCIuY29uY2F0KHByZWZpeENscywgXCItb3BlblwiKSwgb3BlbiksIF9kZWZpbmVQcm9wZXJ0eShfcm9vdENscywgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1mb2N1c2VkXCIpLCBvcGVuIHx8ICEhdGhpcy5fZm9jdXNlZCksIF9kZWZpbmVQcm9wZXJ0eShfcm9vdENscywgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1jb21ib2JveFwiKSwgdXRpbF8xLmlzQ29tYm9ib3gocHJvcHMpKSwgX2RlZmluZVByb3BlcnR5KF9yb290Q2xzLCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWRpc2FibGVkXCIpLCBkaXNhYmxlZCksIF9kZWZpbmVQcm9wZXJ0eShfcm9vdENscywgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1lbmFibGVkXCIpLCAhZGlzYWJsZWQpLCBfZGVmaW5lUHJvcGVydHkoX3Jvb3RDbHMsIFwiXCIuY29uY2F0KHByZWZpeENscywgXCItYWxsb3ctY2xlYXJcIiksICEhcHJvcHMuYWxsb3dDbGVhciksIF9kZWZpbmVQcm9wZXJ0eShfcm9vdENscywgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1uby1hcnJvd1wiKSwgIXNob3dBcnJvdyksIF9kZWZpbmVQcm9wZXJ0eShfcm9vdENscywgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1sb2FkaW5nXCIpLCAhIWxvYWRpbmcpLCBfcm9vdENscyk7XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChTZWxlY3RUcmlnZ2VyXzFbXCJkZWZhdWx0XCJdLCB7XG4gICAgICAgIG9uUG9wdXBGb2N1czogdGhpcy5vblBvcHVwRm9jdXMsXG4gICAgICAgIG9uTW91c2VFbnRlcjogdGhpcy5wcm9wcy5vbk1vdXNlRW50ZXIsXG4gICAgICAgIG9uTW91c2VMZWF2ZTogdGhpcy5wcm9wcy5vbk1vdXNlTGVhdmUsXG4gICAgICAgIGRyb3Bkb3duQWxpZ246IHByb3BzLmRyb3Bkb3duQWxpZ24sXG4gICAgICAgIGRyb3Bkb3duQ2xhc3NOYW1lOiBwcm9wcy5kcm9wZG93bkNsYXNzTmFtZSxcbiAgICAgICAgZHJvcGRvd25NYXRjaFNlbGVjdFdpZHRoOiBwcm9wcy5kcm9wZG93bk1hdGNoU2VsZWN0V2lkdGgsXG4gICAgICAgIGRlZmF1bHRBY3RpdmVGaXJzdE9wdGlvbjogcHJvcHMuZGVmYXVsdEFjdGl2ZUZpcnN0T3B0aW9uLFxuICAgICAgICBkcm9wZG93bk1lbnVTdHlsZTogcHJvcHMuZHJvcGRvd25NZW51U3R5bGUsXG4gICAgICAgIHRyYW5zaXRpb25OYW1lOiBwcm9wcy50cmFuc2l0aW9uTmFtZSxcbiAgICAgICAgYW5pbWF0aW9uOiBwcm9wcy5hbmltYXRpb24sXG4gICAgICAgIHByZWZpeENsczogcHJvcHMucHJlZml4Q2xzLFxuICAgICAgICBkcm9wZG93blN0eWxlOiBwcm9wcy5kcm9wZG93blN0eWxlLFxuICAgICAgICBjb21ib2JveDogcHJvcHMuY29tYm9ib3gsXG4gICAgICAgIHNob3dTZWFyY2g6IHByb3BzLnNob3dTZWFyY2gsXG4gICAgICAgIG9wdGlvbnM6IG9wdGlvbnMsXG4gICAgICAgIGVtcHR5OiBlbXB0eSxcbiAgICAgICAgbXVsdGlwbGU6IG11bHRpcGxlLFxuICAgICAgICBkaXNhYmxlZDogZGlzYWJsZWQsXG4gICAgICAgIHZpc2libGU6IHJlYWxPcGVuLFxuICAgICAgICBpbnB1dFZhbHVlOiBzdGF0ZS5pbnB1dFZhbHVlLFxuICAgICAgICB2YWx1ZTogc3RhdGUudmFsdWUsXG4gICAgICAgIGJhY2tmaWxsVmFsdWU6IHN0YXRlLmJhY2tmaWxsVmFsdWUsXG4gICAgICAgIGZpcnN0QWN0aXZlVmFsdWU6IHByb3BzLmZpcnN0QWN0aXZlVmFsdWUsXG4gICAgICAgIG9uRHJvcGRvd25WaXNpYmxlQ2hhbmdlOiB0aGlzLm9uRHJvcGRvd25WaXNpYmxlQ2hhbmdlLFxuICAgICAgICBnZXRQb3B1cENvbnRhaW5lcjogcHJvcHMuZ2V0UG9wdXBDb250YWluZXIsXG4gICAgICAgIG9uTWVudVNlbGVjdDogdGhpcy5vbk1lbnVTZWxlY3QsXG4gICAgICAgIG9uTWVudURlc2VsZWN0OiB0aGlzLm9uTWVudURlc2VsZWN0LFxuICAgICAgICBvblBvcHVwU2Nyb2xsOiBwcm9wcy5vblBvcHVwU2Nyb2xsLFxuICAgICAgICBzaG93QWN0aW9uOiBwcm9wcy5zaG93QWN0aW9uLFxuICAgICAgICByZWY6IHRoaXMuc2F2ZVNlbGVjdFRyaWdnZXJSZWYsXG4gICAgICAgIG1lbnVJdGVtU2VsZWN0ZWRJY29uOiBwcm9wcy5tZW51SXRlbVNlbGVjdGVkSWNvbixcbiAgICAgICAgZHJvcGRvd25SZW5kZXI6IHByb3BzLmRyb3Bkb3duUmVuZGVyLFxuICAgICAgICBhcmlhSWQ6IGFyaWFJZFxuICAgICAgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgIGlkOiBwcm9wcy5pZCxcbiAgICAgICAgc3R5bGU6IHByb3BzLnN0eWxlLFxuICAgICAgICByZWY6IHRoaXMuc2F2ZVJvb3RSZWYsXG4gICAgICAgIG9uQmx1cjogdGhpcy5vbk91dGVyQmx1cixcbiAgICAgICAgb25Gb2N1czogdGhpcy5vbk91dGVyRm9jdXMsXG4gICAgICAgIGNsYXNzTmFtZTogY2xhc3NuYW1lc18xW1wiZGVmYXVsdFwiXShyb290Q2xzKSxcbiAgICAgICAgb25Nb3VzZURvd246IHRoaXMubWFya01vdXNlRG93bixcbiAgICAgICAgb25Nb3VzZVVwOiB0aGlzLm1hcmtNb3VzZUxlYXZlLFxuICAgICAgICBvbk1vdXNlT3V0OiB0aGlzLm1hcmtNb3VzZUxlYXZlXG4gICAgICB9LCBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIF9leHRlbmRzKHtcbiAgICAgICAgcmVmOiB0aGlzLnNhdmVTZWxlY3Rpb25SZWYsXG4gICAgICAgIGtleTogXCJzZWxlY3Rpb25cIixcbiAgICAgICAgY2xhc3NOYW1lOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNlbGVjdGlvblxcbiAgICAgICAgICAgIFwiKS5jb25jYXQocHJlZml4Q2xzLCBcIi1zZWxlY3Rpb24tLVwiKS5jb25jYXQobXVsdGlwbGUgPyAnbXVsdGlwbGUnIDogJ3NpbmdsZScpLFxuICAgICAgICByb2xlOiBcImNvbWJvYm94XCIsXG4gICAgICAgIFwiYXJpYS1hdXRvY29tcGxldGVcIjogXCJsaXN0XCIsXG4gICAgICAgIFwiYXJpYS1oYXNwb3B1cFwiOiBcInRydWVcIixcbiAgICAgICAgXCJhcmlhLWNvbnRyb2xzXCI6IGFyaWFJZCxcbiAgICAgICAgXCJhcmlhLWV4cGFuZGVkXCI6IHJlYWxPcGVuXG4gICAgICB9LCBleHRyYVNlbGVjdGlvblByb3BzKSwgY3RybE5vZGUsIHRoaXMucmVuZGVyQ2xlYXIoKSwgdGhpcy5yZW5kZXJBcnJvdyghIW11bHRpcGxlKSkpKTtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gU2VsZWN0O1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5TZWxlY3QucHJvcFR5cGVzID0gUHJvcFR5cGVzXzFbXCJkZWZhdWx0XCJdO1xuU2VsZWN0LmRlZmF1bHRQcm9wcyA9IHtcbiAgcHJlZml4Q2xzOiAncmMtc2VsZWN0JyxcbiAgZGVmYXVsdE9wZW46IGZhbHNlLFxuICBsYWJlbEluVmFsdWU6IGZhbHNlLFxuICBkZWZhdWx0QWN0aXZlRmlyc3RPcHRpb246IHRydWUsXG4gIHNob3dTZWFyY2g6IHRydWUsXG4gIGFsbG93Q2xlYXI6IGZhbHNlLFxuICBwbGFjZWhvbGRlcjogJycsXG4gIG9uQ2hhbmdlOiBub29wLFxuICBvbkZvY3VzOiBub29wLFxuICBvbkJsdXI6IG5vb3AsXG4gIG9uU2VsZWN0OiBub29wLFxuICBvblNlYXJjaDogbm9vcCxcbiAgb25EZXNlbGVjdDogbm9vcCxcbiAgb25JbnB1dEtleURvd246IG5vb3AsXG4gIGRyb3Bkb3duTWF0Y2hTZWxlY3RXaWR0aDogdHJ1ZSxcbiAgZHJvcGRvd25TdHlsZToge30sXG4gIGRyb3Bkb3duTWVudVN0eWxlOiB7fSxcbiAgb3B0aW9uRmlsdGVyUHJvcDogJ3ZhbHVlJyxcbiAgb3B0aW9uTGFiZWxQcm9wOiAndmFsdWUnLFxuICBub3RGb3VuZENvbnRlbnQ6ICdOb3QgRm91bmQnLFxuICBiYWNrZmlsbDogZmFsc2UsXG4gIHNob3dBY3Rpb246IFsnY2xpY2snXSxcbiAgdG9rZW5TZXBhcmF0b3JzOiBbXSxcbiAgYXV0b0NsZWFyU2VhcmNoVmFsdWU6IHRydWUsXG4gIHRhYkluZGV4OiAwLFxuICBkcm9wZG93blJlbmRlcjogZnVuY3Rpb24gZHJvcGRvd25SZW5kZXIobWVudSkge1xuICAgIHJldHVybiBtZW51O1xuICB9XG59O1xuXG5TZWxlY3QuZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzID0gZnVuY3Rpb24gKG5leHRQcm9wcywgcHJldlN0YXRlKSB7XG4gIHZhciBvcHRpb25zSW5mbyA9IHByZXZTdGF0ZS5za2lwQnVpbGRPcHRpb25zSW5mbyA/IHByZXZTdGF0ZS5vcHRpb25zSW5mbyA6IFNlbGVjdC5nZXRPcHRpb25zSW5mb0Zyb21Qcm9wcyhuZXh0UHJvcHMsIHByZXZTdGF0ZSk7XG4gIHZhciBuZXdTdGF0ZSA9IHtcbiAgICBvcHRpb25zSW5mbzogb3B0aW9uc0luZm8sXG4gICAgc2tpcEJ1aWxkT3B0aW9uc0luZm86IGZhbHNlXG4gIH07XG5cbiAgaWYgKCdvcGVuJyBpbiBuZXh0UHJvcHMpIHtcbiAgICBuZXdTdGF0ZS5vcGVuID0gbmV4dFByb3BzLm9wZW47XG4gIH1cblxuICBpZiAoJ3ZhbHVlJyBpbiBuZXh0UHJvcHMpIHtcbiAgICB2YXIgdmFsdWUgPSBTZWxlY3QuZ2V0VmFsdWVGcm9tUHJvcHMobmV4dFByb3BzKTtcbiAgICBuZXdTdGF0ZS52YWx1ZSA9IHZhbHVlO1xuXG4gICAgaWYgKG5leHRQcm9wcy5jb21ib2JveCkge1xuICAgICAgbmV3U3RhdGUuaW5wdXRWYWx1ZSA9IFNlbGVjdC5nZXRJbnB1dFZhbHVlRm9yQ29tYm9ib3gobmV4dFByb3BzLCBvcHRpb25zSW5mbyk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIG5ld1N0YXRlO1xufTtcblxuU2VsZWN0LmdldE9wdGlvbnNGcm9tQ2hpbGRyZW4gPSBmdW5jdGlvbiAoY2hpbGRyZW4pIHtcbiAgdmFyIG9wdGlvbnMgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IFtdO1xuICBSZWFjdC5DaGlsZHJlbi5mb3JFYWNoKGNoaWxkcmVuLCBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICBpZiAoIWNoaWxkKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIHR5cGUgPSBjaGlsZC50eXBlO1xuXG4gICAgaWYgKHR5cGUuaXNTZWxlY3RPcHRHcm91cCkge1xuICAgICAgU2VsZWN0LmdldE9wdGlvbnNGcm9tQ2hpbGRyZW4oY2hpbGQucHJvcHMuY2hpbGRyZW4sIG9wdGlvbnMpO1xuICAgIH0gZWxzZSB7XG4gICAgICBvcHRpb25zLnB1c2goY2hpbGQpO1xuICAgIH1cbiAgfSk7XG4gIHJldHVybiBvcHRpb25zO1xufTtcblxuU2VsZWN0LmdldElucHV0VmFsdWVGb3JDb21ib2JveCA9IGZ1bmN0aW9uIChwcm9wcywgb3B0aW9uc0luZm8sIHVzZURlZmF1bHRWYWx1ZSkge1xuICB2YXIgdmFsdWUgPSBbXTtcblxuICBpZiAoJ3ZhbHVlJyBpbiBwcm9wcyAmJiAhdXNlRGVmYXVsdFZhbHVlKSB7XG4gICAgdmFsdWUgPSB1dGlsXzEudG9BcnJheShwcm9wcy52YWx1ZSk7XG4gIH1cblxuICBpZiAoJ2RlZmF1bHRWYWx1ZScgaW4gcHJvcHMgJiYgdXNlRGVmYXVsdFZhbHVlKSB7XG4gICAgdmFsdWUgPSB1dGlsXzEudG9BcnJheShwcm9wcy5kZWZhdWx0VmFsdWUpO1xuICB9XG5cbiAgaWYgKHZhbHVlLmxlbmd0aCkge1xuICAgIHZhbHVlID0gdmFsdWVbMF07XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuICcnO1xuICB9XG5cbiAgdmFyIGxhYmVsID0gdmFsdWU7XG5cbiAgaWYgKHByb3BzLmxhYmVsSW5WYWx1ZSkge1xuICAgIGxhYmVsID0gdmFsdWUubGFiZWw7XG4gIH0gZWxzZSBpZiAob3B0aW9uc0luZm9bdXRpbF8xLmdldE1hcEtleSh2YWx1ZSldKSB7XG4gICAgbGFiZWwgPSBvcHRpb25zSW5mb1t1dGlsXzEuZ2V0TWFwS2V5KHZhbHVlKV0ubGFiZWw7XG4gIH1cblxuICBpZiAobGFiZWwgPT09IHVuZGVmaW5lZCkge1xuICAgIGxhYmVsID0gJyc7XG4gIH1cblxuICByZXR1cm4gbGFiZWw7XG59O1xuXG5TZWxlY3QuZ2V0TGFiZWxGcm9tT3B0aW9uID0gZnVuY3Rpb24gKHByb3BzLCBvcHRpb24pIHtcbiAgcmV0dXJuIHV0aWxfMS5nZXRQcm9wVmFsdWUob3B0aW9uLCBwcm9wcy5vcHRpb25MYWJlbFByb3ApO1xufTtcblxuU2VsZWN0LmdldE9wdGlvbnNJbmZvRnJvbVByb3BzID0gZnVuY3Rpb24gKHByb3BzLCBwcmVTdGF0ZSkge1xuICB2YXIgb3B0aW9ucyA9IFNlbGVjdC5nZXRPcHRpb25zRnJvbUNoaWxkcmVuKHByb3BzLmNoaWxkcmVuKTtcbiAgdmFyIG9wdGlvbnNJbmZvID0ge307XG4gIG9wdGlvbnMuZm9yRWFjaChmdW5jdGlvbiAob3B0aW9uKSB7XG4gICAgdmFyIHNpbmdsZVZhbHVlID0gdXRpbF8xLmdldFZhbHVlUHJvcFZhbHVlKG9wdGlvbik7XG4gICAgb3B0aW9uc0luZm9bdXRpbF8xLmdldE1hcEtleShzaW5nbGVWYWx1ZSldID0ge1xuICAgICAgb3B0aW9uOiBvcHRpb24sXG4gICAgICB2YWx1ZTogc2luZ2xlVmFsdWUsXG4gICAgICBsYWJlbDogU2VsZWN0LmdldExhYmVsRnJvbU9wdGlvbihwcm9wcywgb3B0aW9uKSxcbiAgICAgIHRpdGxlOiBvcHRpb24ucHJvcHMudGl0bGUsXG4gICAgICBkaXNhYmxlZDogb3B0aW9uLnByb3BzLmRpc2FibGVkXG4gICAgfTtcbiAgfSk7XG5cbiAgaWYgKHByZVN0YXRlKSB7XG4gICAgLy8ga2VlcCBvcHRpb24gaW5mbyBpbiBwcmUgc3RhdGUgdmFsdWUuXG4gICAgdmFyIG9sZE9wdGlvbnNJbmZvID0gcHJlU3RhdGUub3B0aW9uc0luZm87XG4gICAgdmFyIHZhbHVlID0gcHJlU3RhdGUudmFsdWU7XG5cbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHZhbHVlLmZvckVhY2goZnVuY3Rpb24gKHYpIHtcbiAgICAgICAgdmFyIGtleSA9IHV0aWxfMS5nZXRNYXBLZXkodik7XG5cbiAgICAgICAgaWYgKCFvcHRpb25zSW5mb1trZXldICYmIG9sZE9wdGlvbnNJbmZvW2tleV0gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIG9wdGlvbnNJbmZvW2tleV0gPSBvbGRPcHRpb25zSW5mb1trZXldO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gb3B0aW9uc0luZm87XG59O1xuXG5TZWxlY3QuZ2V0VmFsdWVGcm9tUHJvcHMgPSBmdW5jdGlvbiAocHJvcHMsIHVzZURlZmF1bHRWYWx1ZSkge1xuICB2YXIgdmFsdWUgPSBbXTtcblxuICBpZiAoJ3ZhbHVlJyBpbiBwcm9wcyAmJiAhdXNlRGVmYXVsdFZhbHVlKSB7XG4gICAgdmFsdWUgPSB1dGlsXzEudG9BcnJheShwcm9wcy52YWx1ZSk7XG4gIH1cblxuICBpZiAoJ2RlZmF1bHRWYWx1ZScgaW4gcHJvcHMgJiYgdXNlRGVmYXVsdFZhbHVlKSB7XG4gICAgdmFsdWUgPSB1dGlsXzEudG9BcnJheShwcm9wcy5kZWZhdWx0VmFsdWUpO1xuICB9XG5cbiAgaWYgKHByb3BzLmxhYmVsSW5WYWx1ZSkge1xuICAgIHZhbHVlID0gdmFsdWUubWFwKGZ1bmN0aW9uICh2KSB7XG4gICAgICByZXR1cm4gdi5rZXk7XG4gICAgfSk7XG4gIH1cblxuICByZXR1cm4gdmFsdWU7XG59O1xuXG5TZWxlY3QuZGlzcGxheU5hbWUgPSAnU2VsZWN0JztcbnJlYWN0X2xpZmVjeWNsZXNfY29tcGF0XzEucG9seWZpbGwoU2VsZWN0KTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gU2VsZWN0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBRkE7QUFNQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSkE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFaQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaEJBO0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQURBO0FBR0E7QUE1QkE7QUE4QkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBREE7QUFHQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFyQ0E7QUF1Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpDQTtBQW1DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVVBO0FBM0dBO0FBQ0E7QUE2R0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBNUJBO0FBQ0E7QUE4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-select/es/Select.js
