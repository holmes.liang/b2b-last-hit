/**
 * 线段包含判断
 * @param  {number}  x0
 * @param  {number}  y0
 * @param  {number}  x1
 * @param  {number}  y1
 * @param  {number}  lineWidth
 * @param  {number}  x
 * @param  {number}  y
 * @return {boolean}
 */
function containStroke(x0, y0, x1, y1, lineWidth, x, y) {
  if (lineWidth === 0) {
    return false;
  }

  var _l = lineWidth;
  var _a = 0;
  var _b = x0; // Quick reject

  if (y > y0 + _l && y > y1 + _l || y < y0 - _l && y < y1 - _l || x > x0 + _l && x > x1 + _l || x < x0 - _l && x < x1 - _l) {
    return false;
  }

  if (x0 !== x1) {
    _a = (y0 - y1) / (x0 - x1);
    _b = (x0 * y1 - x1 * y0) / (x0 - x1);
  } else {
    return Math.abs(x - x0) <= _l / 2;
  }

  var tmp = _a * x - y + _b;

  var _s = tmp * tmp / (_a * _a + 1);

  return _s <= _l / 2 * _l / 2;
}

exports.containStroke = containStroke;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi9saW5lLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi9saW5lLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICog57q/5q615YyF5ZCr5Yik5patXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB4MFxuICogQHBhcmFtICB7bnVtYmVyfSAgeTBcbiAqIEBwYXJhbSAge251bWJlcn0gIHgxXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB5MVxuICogQHBhcmFtICB7bnVtYmVyfSAgbGluZVdpZHRoXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB4XG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB5XG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5mdW5jdGlvbiBjb250YWluU3Ryb2tlKHgwLCB5MCwgeDEsIHkxLCBsaW5lV2lkdGgsIHgsIHkpIHtcbiAgaWYgKGxpbmVXaWR0aCA9PT0gMCkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIHZhciBfbCA9IGxpbmVXaWR0aDtcbiAgdmFyIF9hID0gMDtcbiAgdmFyIF9iID0geDA7IC8vIFF1aWNrIHJlamVjdFxuXG4gIGlmICh5ID4geTAgKyBfbCAmJiB5ID4geTEgKyBfbCB8fCB5IDwgeTAgLSBfbCAmJiB5IDwgeTEgLSBfbCB8fCB4ID4geDAgKyBfbCAmJiB4ID4geDEgKyBfbCB8fCB4IDwgeDAgLSBfbCAmJiB4IDwgeDEgLSBfbCkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGlmICh4MCAhPT0geDEpIHtcbiAgICBfYSA9ICh5MCAtIHkxKSAvICh4MCAtIHgxKTtcbiAgICBfYiA9ICh4MCAqIHkxIC0geDEgKiB5MCkgLyAoeDAgLSB4MSk7XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIE1hdGguYWJzKHggLSB4MCkgPD0gX2wgLyAyO1xuICB9XG5cbiAgdmFyIHRtcCA9IF9hICogeCAtIHkgKyBfYjtcblxuICB2YXIgX3MgPSB0bXAgKiB0bXAgLyAoX2EgKiBfYSArIDEpO1xuXG4gIHJldHVybiBfcyA8PSBfbCAvIDIgKiBfbCAvIDI7XG59XG5cbmV4cG9ydHMuY29udGFpblN0cm9rZSA9IGNvbnRhaW5TdHJva2U7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/contain/line.js
