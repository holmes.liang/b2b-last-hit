__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @assets/attachment.svg */ "./src/assets/attachment.svg");
/* harmony import */ var _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_assets_attachment_svg__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var react_images__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react-images */ "./node_modules/react-images/lib/Lightbox.js");
/* harmony import */ var react_images__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_images__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @desk-component/attachment/doc-types */ "./src/app/desk/component/attachment/doc-types.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/attachment/attachment-view.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        .operate {\n          position: absolute;\n          z-index: 22;\n          width: 100%; \n          height: 23px; \n          bottom: 2px;\n          margin-left: -5px;\n          display: none;\n          .bg {\n            background: #fff;\n            height: 23px; \n            zoom: 1; \n          }\n          .con {\n            position: relative;\n          }\n          a {\n            display: inline-block !important;\n            height: 22px !important;\n            line-height: 22px !important;\n            .anticon {\n              font-size: 20px;\n            }\n            &.a-pl {\n              padding-left: 48px;\n            }\n          }\n        }\n        .attachment-preview-list {\n            display: -ms-flexbox;\n            display: flex;\n            -ms-flex-wrap: wrap;\n            flex-wrap: wrap;\n        }\n        .attachment-preview-item {\n            position: relative;\n            display: -ms-flexbox;\n            display: flex;\n            -ms-flex-direction: column;\n            flex-direction: column;\n            margin: 0 15px 15px 0;\n            padding: 5px;\n            width: 135px;\n            height: 135px;\n            text-align: center;\n            border: 1px solid #ebebeb;\n            border-radius: 2px;\n            cursor: pointer;\n            img {\n                flex: 1 1 0%;\n                max-height: 100px;\n            }\n            a {\n                display: block;\n                height: 23px;\n                line-height: 23px;\n                white-space: nowrap;\n                overflow: hidden;\n                text-overflow: ellipsis;\n                color: #878787;\n                &.img-a {\n                  height: auto;\n                }\n            }\n          \n            .attachment-preview-item__oper {\n                position: absolute;\n                top: -9px;\n                right: -10px;\n                display: none;\n                z-index: 22;\n                img {\n                  width: 23px;\n                }\n                i{\n                    position: relative;\n                    color: #5fbfec;\n                    z-index: 25;\n                }\n            }\n            .file-name {\n              display: block;\n            }\n            &:hover {\n                .attachment-preview-item__oper, .operate {\n                    display: block;\n                }\n                .file-name {\n                  display: none;\n                }\n            }\n         }\n        .attachment-uploader-list {\n            display: -ms-flexbox;\n            display: flex;\n            -ms-flex-wrap: wrap;\n            flex-wrap: wrap;\n            overflow-x: auto;\n            overflow-y: hidden;\n            .attachment-uploader-item {\n                margin-right: 30px;\n                text-align: center;\n                a,img {\n                    display: block;\n                    height: 65px;\n                    width: 80px;\n                    margin: 0 auto;\n                    opacity: .6;\n                    background-size: contain;\n                    background-repeat: no-repeat;\n                    background-position: center bottom;\n                }\n                 p {\n                    font-size: 13px;\n                    margin-top: 5px;\n                    color: #9e9e9e;\n                    text-transform: uppercase;\n                }\n            }\n        } \n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}












var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_16__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

var isImage = function isImage(mimeType) {
  return mimeType && mimeType.includes("image");
};

var Attachment =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(Attachment, _ModelWidget);

  function Attachment(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Attachment);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Attachment).call(this, props, context));

    _this.setLightbox = function (nextLightbox) {
      _this.setState(function (_ref) {
        var lightbox = _ref.lightbox;
        return {
          lightbox: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, lightbox, {}, nextLightbox)
        };
      });
    };

    _this.gotoPrevLightboxImage = function () {
      var index = _this.state.lightbox.index;

      if (index === 0) {
        return;
      }

      _this.setLightbox({
        index: index - 1
      });
    };

    _this.gotoNextLightboxImage = function () {
      var _this$state = _this.state,
          index = _this$state.lightbox.index,
          _this$state$attaches = _this$state.attaches,
          attaches = _this$state$attaches === void 0 ? [] : _this$state$attaches;

      if (index === attaches.length - 1) {
        return;
      }

      _this.setLightbox({
        index: index + 1
      });
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Attachment, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (!this.props.isGetApis) {
        this.getAttaches();
      }
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Attachment: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Attachment.prototype), "initState", this).call(this), {
        attaches: [],
        lightbox: {
          index: -1,
          visible: false
        }
      });
    }
  }, {
    key: "getAttaches",
    value: function getAttaches() {
      var _this2 = this;

      var getUrl = this.props.getUrl;
      this.getHelpers().getAjax().get(getUrl ? "".concat(getUrl) : "/policies/".concat(this.props.policyId, "/attaches"), {}, {}).then(function (response) {
        var respData = response.body.respData;

        if (_this2.props.afterGetData) {
          _this2.props.afterGetData(respData);
        }

        _this2.setState({
          attaches: respData
        });
      }).catch(function (error) {});
    }
  }, {
    key: "renderImageLightbox",
    value: function renderImageLightbox() {
      var _this3 = this;

      var _this$props = this.props,
          isGetApis = _this$props.isGetApis,
          policyId = _this$props.policyId;
      var authKey = _common__WEBPACK_IMPORTED_MODULE_13__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_13__["Consts"].AUTH_KEY);
      var _this$state2 = this.state,
          _this$state2$attaches = _this$state2.attaches,
          attaches = _this$state2$attaches === void 0 ? [] : _this$state2$attaches,
          _this$state2$lightbox = _this$state2.lightbox,
          visible = _this$state2$lightbox.visible,
          index = _this$state2$lightbox.index;
      var imageList;
      var getDownUrl = this.props.getDownUrl;

      if (!isGetApis) {
        imageList = (this.getImgList(attaches) || []).map(function (item) {
          var fileName = item.fileName,
              attachDetails = item.attachDetails,
              attachId = item.attachId;
          return {
            caption: fileName,
            src: isImage(attachDetails.mimeType) ? _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].getServiceLocation(getDownUrl ? "".concat(getDownUrl, "/").concat(attachId, "/file?access_token=").concat(authKey, "&resize=md") : "/policies/".concat(policyId, "/attaches/").concat(attachId, "/file?access_token=").concat(authKey, "&resize=md")) : _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_14___default.a
          };
        });
      } else {
        imageList = (this.getImgList(this.props.attaches) || []).map(function (item) {
          var fileName = item.fileName,
              fid = item.fid,
              mimeType = item.mimeType;
          return {
            caption: fileName,
            src: isImage(mimeType) ? _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].getServiceLocation("/storage/".concat(fid, "?access_token=").concat(authKey, "&resize=md")) : _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_14___default.a
          };
        });
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(react_images__WEBPACK_IMPORTED_MODULE_15___default.a, {
        images: imageList,
        currentImage: index,
        backdropClosesModal: true,
        isOpen: visible && imageList.length > 0,
        onClickPrev: this.gotoPrevLightboxImage,
        onClickNext: this.gotoNextLightboxImage,
        onClose: function onClose() {
          return _this3.setLightbox({
            visible: false
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 271
        },
        __self: this
      });
    }
  }, {
    key: "getImgList",
    value: function getImgList(fileList) {
      var _this4 = this;

      var _this$props2 = this.props,
          isGetApis = _this$props2.isGetApis,
          attaches = _this$props2.attaches;
      var imgList = [];
      if (!fileList) return [];

      if (!isGetApis) {
        fileList.map(function (every) {
          var _ref2 = every || {
            attachDetails: {
              originalFileName: "",
              mimeType: ""
            }
          },
              _ref2$attachDetails = _ref2.attachDetails,
              originalFileName = _ref2$attachDetails.originalFileName,
              mimeType = _ref2$attachDetails.mimeType;

          if (_this4.isPicture(originalFileName) || isImage(mimeType)) {
            imgList.push(every);
          }
        });
      } else {
        (attaches || []).map(function (every) {
          if (isImage(every.mimeType)) {
            imgList.push(every);
          }
        });
      }

      return imgList;
    }
  }, {
    key: "isPicture",
    value: function isPicture(fileName) {
      if (!fileName) return;
      var type = fileName.split(".")[fileName.split(".").length - 1];
      return /gif|jpg|jpeg|png|gif|jpg|png$/.test(type);
    }
  }, {
    key: "attachesRender",
    value: function attachesRender() {
      var _this5 = this;

      var attachesItems;
      var attaches = this.state.attaches;
      var isGetApis = this.props.isGetApis;
      attachesItems = !isGetApis ? attaches : this.props.attaches;
      var authKey = _common__WEBPACK_IMPORTED_MODULE_13__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_13__["Consts"].AUTH_KEY);
      var policyId = this.props.policyId;
      var getDownUrl = this.props.getDownUrl;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        className: "attachment-preview-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 326
        },
        __self: this
      }, (attachesItems || []).map(function (attach, index) {
        var url, hrefUrl, fileName, attachId, mimeType, docName;
        var findImgIndex;

        if (!isGetApis) {
          var attachDetails = attach.attachDetails;
          attachId = attach.attachId;
          mimeType = attachDetails.mimeType;
          url = isImage(mimeType) ? _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].getServiceLocation(getDownUrl ? "".concat(getDownUrl, "/").concat(attachId, "/file?access_token=").concat(authKey, "&resize=sm") : "/policies/".concat(policyId, "/attaches/").concat(attachId, "/file?access_token=").concat(authKey, "&resize=sm")) : _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_14___default.a;
          hrefUrl = _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].getServiceLocation(getDownUrl ? "".concat(getDownUrl, "/").concat(attachId, "/file?access_token=").concat(authKey) : "/policies/".concat(policyId, "/attaches/").concat(attachId, "/file?access_token=").concat(authKey));
          fileName = !attachDetails ? "" : attachDetails.originalFileName;
          docName = !attachDetails ? "" : attachDetails.docName;

          _this5.getImgList(attaches).forEach(function (imageItem, imageIndex) {
            if (imageItem.attachId === attachId) {
              findImgIndex = imageIndex;
            }
          });
        } else {
          var fid = attach.fid;
          attachId = fid;
          mimeType = attach.mimeType; // note: storage => blob/files/

          url = isImage(mimeType) ? _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].getServiceLocation("/blob/files/".concat(fid, "?access_token=").concat(authKey, "&resize=sm")) : _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_14___default.a;
          hrefUrl = _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].getServiceLocation("/blob/files/".concat(fid, "?access_token=").concat(authKey));
          fileName = attach.fileName;

          _this5.getImgList(attachesItems).forEach(function (imageItem, imageIndex) {
            if (imageItem.fid === fid) {
              findImgIndex = imageIndex;
            }
          });
        }

        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 369
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Tooltip"], {
          key: "".concat(index, "-attach"),
          overlayClassName: "file-name-tip",
          placement: "bottomRight",
          title: fileName || docName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 370
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          className: "attachment-preview-item",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 376
          },
          __self: this
        }, url && (_this5.isPicture(fileName) || isImage(mimeType)) && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("img", {
          onClick: function onClick() {
            return _this5.setLightbox({
              index: findImgIndex,
              visible: true
            });
          },
          src: "".concat(url),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 378
          },
          __self: this
        }), url && !(_this5.isPicture(fileName) || isImage(mimeType)) && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          className: "img-a",
          href: hrefUrl + "&resize=md",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 381
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("img", {
          alt: attachId,
          src: url,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 382
          },
          __self: this
        })), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          href: hrefUrl + "&resize=md",
          title: fileName || docName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 385
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 386
          },
          __self: this
        }, fileName || docName)), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          className: "operate",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 388
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          className: "bg",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 389
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          className: "con",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 390
          },
          __self: this
        }, (_this5.isPicture(fileName) || isImage(mimeType)) && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          onClick: function onClick() {
            return _this5.setLightbox({
              index: findImgIndex,
              visible: true
            });
          },
          href: "javascript:void(0)",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 392
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Icon"], {
          type: "eye",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 396
          },
          __self: this
        })), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          className: _this5.isPicture(fileName) || isImage(mimeType) ? "a-pl" : "",
          style: {
            color: COLOR_PRIMARY
          },
          href: hrefUrl + "&resize=orig",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 399
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Icon"], {
          type: "download",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 404
          },
          __self: this
        }))))))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          style: {
            marginTop: -8,
            marginRight: 15,
            textAlign: "center",
            hyphens: "auto",
            width: 135
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 413
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("p", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 420
          },
          __self: this
        }, Object(_desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_17__["transTypeToName"])(lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(attach, "attachDetails.docType")))));
      }), this.renderImageLightbox());
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.Attachment, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 433
        },
        __self: this
      }, this.attachesRender());
    }
  }]);

  return Attachment;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Attachment);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2F0dGFjaG1lbnQvYXR0YWNobWVudC12aWV3LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9hdHRhY2htZW50L2F0dGFjaG1lbnQtdmlldy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlLCBNb2RlbFdpZGdldFByb3BzLCBTdHlsZWRESVYgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBJY29uLCBUb29sdGlwIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IEFqYXgsIENvbnN0cywgU3RvcmFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgYXR0YWNobWVudEltYWdlVXJsIGZyb20gXCJAYXNzZXRzL2F0dGFjaG1lbnQuc3ZnXCI7XG5pbXBvcnQgTGlnaHRib3ggZnJvbSBcInJlYWN0LWltYWdlc1wiO1xuaW1wb3J0IFRoZW1lIGZyb20gXCJAc3R5bGVzXCI7XG5pbXBvcnQgeyB0cmFuc1R5cGVUb05hbWUgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L2F0dGFjaG1lbnQvZG9jLXR5cGVzXCI7XG5cbmNvbnN0IHsgQ09MT1JfUFJJTUFSWSB9ID0gVGhlbWUuZ2V0VGhlbWUoKTtcbmNvbnN0IGlzSW1hZ2UgPSAobWltZVR5cGU6IHN0cmluZykgPT4gbWltZVR5cGUgJiYgbWltZVR5cGUuaW5jbHVkZXMoXCJpbWFnZVwiKTtcblxuZXhwb3J0IHR5cGUgQXR0YWNobWVudFByb3BzID0ge1xuICBwb2xpY3lJZD86IHN0cmluZztcbiAgaXNHZXRBcGlzPzogYm9vbGVhbjtcbiAgYXR0YWNoZXM/OiBhbnk7XG4gIGdldFVybD86IHN0cmluZztcbiAgZ2V0RG93blVybD86IHN0cmluZztcbiAgYWZ0ZXJHZXREYXRhPzogYW55LFxufSAmIE1vZGVsV2lkZ2V0UHJvcHM7XG5cbmV4cG9ydCB0eXBlIEF0dGFjaG1lbnRTdGF0ZSA9IHtcbiAgYXR0YWNoZXM6IGFueTtcbiAgbGlnaHRib3g6IGFueTtcbn07XG5leHBvcnQgdHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxuZXhwb3J0IHR5cGUgQXR0YWNobWVudENvbXBvbmVudHMgPSB7XG4gIEF0dGFjaG1lbnQ6IFN0eWxlZERJVjtcbn07XG5cbmNsYXNzIEF0dGFjaG1lbnQ8UCBleHRlbmRzIEF0dGFjaG1lbnRQcm9wcyxcbiAgUyBleHRlbmRzIEF0dGFjaG1lbnRTdGF0ZSxcbiAgQyBleHRlbmRzIEF0dGFjaG1lbnRDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IEF0dGFjaG1lbnRQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGlmICghdGhpcy5wcm9wcy5pc0dldEFwaXMpIHtcbiAgICAgIHRoaXMuZ2V0QXR0YWNoZXMoKTtcbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIEF0dGFjaG1lbnQ6IFN0eWxlZC5kaXZgXG4gICAgICAgIC5vcGVyYXRlIHtcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgei1pbmRleDogMjI7XG4gICAgICAgICAgd2lkdGg6IDEwMCU7IFxuICAgICAgICAgIGhlaWdodDogMjNweDsgXG4gICAgICAgICAgYm90dG9tOiAycHg7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IC01cHg7XG4gICAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgICAuYmcge1xuICAgICAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICAgICAgICAgIGhlaWdodDogMjNweDsgXG4gICAgICAgICAgICB6b29tOiAxOyBcbiAgICAgICAgICB9XG4gICAgICAgICAgLmNvbiB7XG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGEge1xuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBoZWlnaHQ6IDIycHggIWltcG9ydGFudDtcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMnB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAuYW50aWNvbiB7XG4gICAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICYuYS1wbCB7XG4gICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogNDhweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmF0dGFjaG1lbnQtcHJldmlldy1saXN0IHtcbiAgICAgICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIC1tcy1mbGV4LXdyYXA6IHdyYXA7XG4gICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgICAgIH1cbiAgICAgICAgLmF0dGFjaG1lbnQtcHJldmlldy1pdGVtIHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgIG1hcmdpbjogMCAxNXB4IDE1cHggMDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMzVweDtcbiAgICAgICAgICAgIGhlaWdodDogMTM1cHg7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZWJlYmViO1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgaW1nIHtcbiAgICAgICAgICAgICAgICBmbGV4OiAxIDEgMCU7XG4gICAgICAgICAgICAgICAgbWF4LWhlaWdodDogMTAwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBhIHtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDIzcHg7XG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIzcHg7XG4gICAgICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjODc4Nzg3O1xuICAgICAgICAgICAgICAgICYuaW1nLWEge1xuICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICBcbiAgICAgICAgICAgIC5hdHRhY2htZW50LXByZXZpZXctaXRlbV9fb3BlciB7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgIHRvcDogLTlweDtcbiAgICAgICAgICAgICAgICByaWdodDogLTEwcHg7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgICAgICAgICB6LWluZGV4OiAyMjtcbiAgICAgICAgICAgICAgICBpbWcge1xuICAgICAgICAgICAgICAgICAgd2lkdGg6IDIzcHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGl7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM1ZmJmZWM7XG4gICAgICAgICAgICAgICAgICAgIHotaW5kZXg6IDI1O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5maWxlLW5hbWUge1xuICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICY6aG92ZXIge1xuICAgICAgICAgICAgICAgIC5hdHRhY2htZW50LXByZXZpZXctaXRlbV9fb3BlciwgLm9wZXJhdGUge1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmZpbGUtbmFtZSB7XG4gICAgICAgICAgICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgIH1cbiAgICAgICAgLmF0dGFjaG1lbnQtdXBsb2FkZXItbGlzdCB7XG4gICAgICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xuICAgICAgICAgICAgZmxleC13cmFwOiB3cmFwO1xuICAgICAgICAgICAgb3ZlcmZsb3cteDogYXV0bztcbiAgICAgICAgICAgIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgICAgICAgICAgIC5hdHRhY2htZW50LXVwbG9hZGVyLWl0ZW0ge1xuICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMzBweDtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYSxpbWcge1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA2NXB4O1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogODBweDtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IC42O1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBib3R0b207XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICBwIHtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjOWU5ZTllO1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBcbiAgICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgYXR0YWNoZXM6IFtdLFxuICAgICAgbGlnaHRib3g6IHsgaW5kZXg6IC0xLCB2aXNpYmxlOiBmYWxzZSB9LFxuICAgIH0pIGFzIFM7XG4gIH1cblxuICBwcml2YXRlIGdldEF0dGFjaGVzKCkge1xuICAgIGxldCBnZXRVcmw6IGFueSA9IHRoaXMucHJvcHMuZ2V0VXJsO1xuICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAuZ2V0QWpheCgpXG4gICAgICAuZ2V0KFxuICAgICAgICBnZXRVcmwgPyBgJHtnZXRVcmx9YCA6XG4gICAgICAgICAgYC9wb2xpY2llcy8ke3RoaXMucHJvcHMucG9saWN5SWR9L2F0dGFjaGVzYCwge30sIHt9KVxuICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzcG9uc2UuYm9keTtcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuYWZ0ZXJHZXREYXRhKSB7XG4gICAgICAgICAgdGhpcy5wcm9wcy5hZnRlckdldERhdGEocmVzcERhdGEpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGF0dGFjaGVzOiByZXNwRGF0YSxcbiAgICAgICAgfSk7XG4gICAgICB9KVxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRMaWdodGJveCA9IChuZXh0TGlnaHRib3g6IGFueSkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoKHsgbGlnaHRib3ggfSkgPT4ge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbGlnaHRib3g6IHtcbiAgICAgICAgICAuLi5saWdodGJveCxcbiAgICAgICAgICAuLi5uZXh0TGlnaHRib3gsXG4gICAgICAgIH0sXG4gICAgICB9O1xuICAgIH0pO1xuICB9O1xuICBwcml2YXRlIGdvdG9QcmV2TGlnaHRib3hJbWFnZSA9ICgpID0+IHtcbiAgICBjb25zdCB7XG4gICAgICBsaWdodGJveDogeyBpbmRleCB9LFxuICAgIH0gPSB0aGlzLnN0YXRlO1xuICAgIGlmIChpbmRleCA9PT0gMCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuc2V0TGlnaHRib3goe1xuICAgICAgaW5kZXg6IGluZGV4IC0gMSxcbiAgICB9KTtcbiAgfTtcblxuICBwcml2YXRlIGdvdG9OZXh0TGlnaHRib3hJbWFnZSA9ICgpID0+IHtcbiAgICBjb25zdCB7XG4gICAgICBsaWdodGJveDogeyBpbmRleCB9LFxuICAgICAgYXR0YWNoZXMgPSBbXSxcbiAgICB9ID0gdGhpcy5zdGF0ZTtcbiAgICBpZiAoaW5kZXggPT09IGF0dGFjaGVzLmxlbmd0aCAtIDEpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5zZXRMaWdodGJveCh7XG4gICAgICBpbmRleDogaW5kZXggKyAxLFxuICAgIH0pO1xuICB9O1xuXG4gIHByaXZhdGUgcmVuZGVySW1hZ2VMaWdodGJveCgpIHtcbiAgICBjb25zdCB7IGlzR2V0QXBpcywgcG9saWN5SWQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgYXV0aEtleSA9IFN0b3JhZ2UuQXV0aC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BVVRIX0tFWSk7XG4gICAgY29uc3Qge1xuICAgICAgYXR0YWNoZXMgPSBbXSxcbiAgICAgIGxpZ2h0Ym94OiB7IHZpc2libGUsIGluZGV4IH0sXG4gICAgfSA9IHRoaXMuc3RhdGU7XG4gICAgbGV0IGltYWdlTGlzdDogYW55W107XG4gICAgbGV0IGdldERvd25Vcmw6IGFueSA9IHRoaXMucHJvcHMuZ2V0RG93blVybDtcbiAgICBpZiAoIWlzR2V0QXBpcykge1xuICAgICAgaW1hZ2VMaXN0ID0gKHRoaXMuZ2V0SW1nTGlzdChhdHRhY2hlcykgfHwgW10pLm1hcCgoaXRlbTogYW55KSA9PiB7XG4gICAgICAgIGNvbnN0IHsgZmlsZU5hbWUsIGF0dGFjaERldGFpbHMsIGF0dGFjaElkIH0gPSBpdGVtO1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGNhcHRpb246IGZpbGVOYW1lLFxuICAgICAgICAgIHNyYzogaXNJbWFnZShhdHRhY2hEZXRhaWxzLm1pbWVUeXBlKVxuICAgICAgICAgICAgPyBBamF4LmdldFNlcnZpY2VMb2NhdGlvbihnZXREb3duVXJsID9cbiAgICAgICAgICAgICAgYCR7Z2V0RG93blVybH0vJHthdHRhY2hJZH0vZmlsZT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fSZyZXNpemU9bWRgIDpcbiAgICAgICAgICAgICAgYC9wb2xpY2llcy8ke3BvbGljeUlkfS9hdHRhY2hlcy8ke2F0dGFjaElkfS9maWxlP2FjY2Vzc190b2tlbj0ke2F1dGhLZXl9JnJlc2l6ZT1tZGApXG4gICAgICAgICAgICA6IGF0dGFjaG1lbnRJbWFnZVVybCxcbiAgICAgICAgfTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBpbWFnZUxpc3QgPSAodGhpcy5nZXRJbWdMaXN0KHRoaXMucHJvcHMuYXR0YWNoZXMpIHx8IFtdKS5tYXAoKGl0ZW06IGFueSkgPT4ge1xuICAgICAgICBjb25zdCB7IGZpbGVOYW1lLCBmaWQsIG1pbWVUeXBlIH0gPSBpdGVtO1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGNhcHRpb246IGZpbGVOYW1lLFxuICAgICAgICAgIHNyYzogaXNJbWFnZShtaW1lVHlwZSlcbiAgICAgICAgICAgID8gQWpheC5nZXRTZXJ2aWNlTG9jYXRpb24oYC9zdG9yYWdlLyR7ZmlkfT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fSZyZXNpemU9bWRgKVxuICAgICAgICAgICAgOiBhdHRhY2htZW50SW1hZ2VVcmwsXG4gICAgICAgIH07XG4gICAgICB9KTtcbiAgICB9XG4gICAgcmV0dXJuIChcbiAgICAgIDxMaWdodGJveFxuICAgICAgICBpbWFnZXM9e2ltYWdlTGlzdH1cbiAgICAgICAgY3VycmVudEltYWdlPXtpbmRleH1cbiAgICAgICAgYmFja2Ryb3BDbG9zZXNNb2RhbFxuICAgICAgICBpc09wZW49e3Zpc2libGUgJiYgaW1hZ2VMaXN0Lmxlbmd0aCA+IDB9XG4gICAgICAgIG9uQ2xpY2tQcmV2PXt0aGlzLmdvdG9QcmV2TGlnaHRib3hJbWFnZX1cbiAgICAgICAgb25DbGlja05leHQ9e3RoaXMuZ290b05leHRMaWdodGJveEltYWdlfVxuICAgICAgICBvbkNsb3NlPXsoKSA9PiB0aGlzLnNldExpZ2h0Ym94KHsgdmlzaWJsZTogZmFsc2UgfSl9XG4gICAgICAvPlxuICAgICk7XG4gIH1cblxuICBwcml2YXRlIGdldEltZ0xpc3QoZmlsZUxpc3Q6IGFueSk6IGFueVtdIHtcbiAgICBjb25zdCB7IGlzR2V0QXBpcywgYXR0YWNoZXMgfSA9IHRoaXMucHJvcHM7XG4gICAgbGV0IGltZ0xpc3Q6IGFueSA9IFtdO1xuICAgIGlmICghZmlsZUxpc3QpIHJldHVybiBbXTtcbiAgICBpZiAoIWlzR2V0QXBpcykge1xuICAgICAgZmlsZUxpc3QubWFwKChldmVyeTogYW55KSA9PiB7XG4gICAgICAgIGNvbnN0IHtcbiAgICAgICAgICBhdHRhY2hEZXRhaWxzOiB7IG9yaWdpbmFsRmlsZU5hbWUsIG1pbWVUeXBlIH0sXG4gICAgICAgIH0gPSBldmVyeSB8fCB7XG4gICAgICAgICAgYXR0YWNoRGV0YWlsczoge1xuICAgICAgICAgICAgb3JpZ2luYWxGaWxlTmFtZTogXCJcIixcbiAgICAgICAgICAgIG1pbWVUeXBlOiBcIlwiLFxuICAgICAgICAgIH0sXG4gICAgICAgIH07XG4gICAgICAgIGlmICh0aGlzLmlzUGljdHVyZShvcmlnaW5hbEZpbGVOYW1lKSB8fCBpc0ltYWdlKG1pbWVUeXBlKSkge1xuICAgICAgICAgIGltZ0xpc3QucHVzaChldmVyeSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICAoYXR0YWNoZXMgfHwgW10pLm1hcCgoZXZlcnk6IGFueSkgPT4ge1xuICAgICAgICBpZiAoaXNJbWFnZShldmVyeS5taW1lVHlwZSkpIHtcbiAgICAgICAgICBpbWdMaXN0LnB1c2goZXZlcnkpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gICAgcmV0dXJuIGltZ0xpc3Q7XG4gIH1cblxuICBwcml2YXRlIGlzUGljdHVyZShmaWxlTmFtZTogc3RyaW5nKSB7XG4gICAgaWYgKCFmaWxlTmFtZSkgcmV0dXJuO1xuICAgIGNvbnN0IHR5cGU6IHN0cmluZyA9IGZpbGVOYW1lLnNwbGl0KFwiLlwiKVtmaWxlTmFtZS5zcGxpdChcIi5cIikubGVuZ3RoIC0gMV07XG4gICAgcmV0dXJuIC9naWZ8anBnfGpwZWd8cG5nfGdpZnxqcGd8cG5nJC8udGVzdCh0eXBlKTtcbiAgfVxuXG4gIHByaXZhdGUgYXR0YWNoZXNSZW5kZXIoKSB7XG4gICAgbGV0IGF0dGFjaGVzSXRlbXM6IGFueTtcbiAgICBjb25zdCB7IGF0dGFjaGVzIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHsgaXNHZXRBcGlzIH0gPSB0aGlzLnByb3BzO1xuICAgIGF0dGFjaGVzSXRlbXMgPSAhaXNHZXRBcGlzID8gYXR0YWNoZXMgOiB0aGlzLnByb3BzLmF0dGFjaGVzO1xuICAgIGNvbnN0IGF1dGhLZXkgPSBTdG9yYWdlLkF1dGguc2Vzc2lvbigpLmdldChDb25zdHMuQVVUSF9LRVkpO1xuICAgIGNvbnN0IHBvbGljeUlkID0gdGhpcy5wcm9wcy5wb2xpY3lJZDtcbiAgICBsZXQgZ2V0RG93blVybDogYW55ID0gdGhpcy5wcm9wcy5nZXREb3duVXJsO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImF0dGFjaG1lbnQtcHJldmlldy1saXN0XCI+XG4gICAgICAgIHsoYXR0YWNoZXNJdGVtcyB8fCBbXSkubWFwKChhdHRhY2g6IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgIGxldCB1cmwsIGhyZWZVcmwsIGZpbGVOYW1lLCBhdHRhY2hJZDogYW55LCBtaW1lVHlwZSwgZG9jTmFtZTtcbiAgICAgICAgICBsZXQgZmluZEltZ0luZGV4OiBudW1iZXI7XG4gICAgICAgICAgaWYgKCFpc0dldEFwaXMpIHtcbiAgICAgICAgICAgIGNvbnN0IHsgYXR0YWNoRGV0YWlscyB9ID0gYXR0YWNoO1xuICAgICAgICAgICAgYXR0YWNoSWQgPSBhdHRhY2guYXR0YWNoSWQ7XG4gICAgICAgICAgICBtaW1lVHlwZSA9IGF0dGFjaERldGFpbHMubWltZVR5cGU7XG4gICAgICAgICAgICB1cmwgPSBpc0ltYWdlKG1pbWVUeXBlKVxuICAgICAgICAgICAgICA/IEFqYXguZ2V0U2VydmljZUxvY2F0aW9uKFxuICAgICAgICAgICAgICAgIGdldERvd25VcmwgP1xuICAgICAgICAgICAgICAgICAgYCR7Z2V0RG93blVybH0vJHthdHRhY2hJZH0vZmlsZT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fSZyZXNpemU9c21gIDpcbiAgICAgICAgICAgICAgICAgIGAvcG9saWNpZXMvJHtwb2xpY3lJZH0vYXR0YWNoZXMvJHthdHRhY2hJZH0vZmlsZT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fSZyZXNpemU9c21gKVxuICAgICAgICAgICAgICA6IGF0dGFjaG1lbnRJbWFnZVVybDtcbiAgICAgICAgICAgIGhyZWZVcmwgPSBBamF4LmdldFNlcnZpY2VMb2NhdGlvbihcbiAgICAgICAgICAgICAgZ2V0RG93blVybCA/XG4gICAgICAgICAgICAgICAgYCR7Z2V0RG93blVybH0vJHthdHRhY2hJZH0vZmlsZT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fWAgOlxuICAgICAgICAgICAgICAgIGAvcG9saWNpZXMvJHtwb2xpY3lJZH0vYXR0YWNoZXMvJHthdHRhY2hJZH0vZmlsZT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fWAsXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgZmlsZU5hbWUgPSAhYXR0YWNoRGV0YWlscyA/IFwiXCIgOiBhdHRhY2hEZXRhaWxzLm9yaWdpbmFsRmlsZU5hbWU7XG4gICAgICAgICAgICBkb2NOYW1lID0gIWF0dGFjaERldGFpbHMgPyBcIlwiIDogYXR0YWNoRGV0YWlscy5kb2NOYW1lO1xuICAgICAgICAgICAgdGhpcy5nZXRJbWdMaXN0KGF0dGFjaGVzKS5mb3JFYWNoKChpbWFnZUl0ZW06IGFueSwgaW1hZ2VJbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICAgICAgICAgIGlmIChpbWFnZUl0ZW0uYXR0YWNoSWQgPT09IGF0dGFjaElkKSB7XG4gICAgICAgICAgICAgICAgZmluZEltZ0luZGV4ID0gaW1hZ2VJbmRleDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbnN0IHsgZmlkIH0gPSBhdHRhY2g7XG4gICAgICAgICAgICBhdHRhY2hJZCA9IGZpZDtcbiAgICAgICAgICAgIG1pbWVUeXBlID0gYXR0YWNoLm1pbWVUeXBlO1xuICAgICAgICAgICAgLy8gbm90ZTogc3RvcmFnZSA9PiBibG9iL2ZpbGVzL1xuICAgICAgICAgICAgdXJsID0gaXNJbWFnZShtaW1lVHlwZSlcbiAgICAgICAgICAgICAgPyBBamF4LmdldFNlcnZpY2VMb2NhdGlvbihgL2Jsb2IvZmlsZXMvJHtmaWR9P2FjY2Vzc190b2tlbj0ke2F1dGhLZXl9JnJlc2l6ZT1zbWApXG4gICAgICAgICAgICAgIDogYXR0YWNobWVudEltYWdlVXJsO1xuICAgICAgICAgICAgaHJlZlVybCA9IEFqYXguZ2V0U2VydmljZUxvY2F0aW9uKGAvYmxvYi9maWxlcy8ke2ZpZH0/YWNjZXNzX3Rva2VuPSR7YXV0aEtleX1gKTtcbiAgICAgICAgICAgIGZpbGVOYW1lID0gYXR0YWNoLmZpbGVOYW1lO1xuICAgICAgICAgICAgdGhpcy5nZXRJbWdMaXN0KGF0dGFjaGVzSXRlbXMpLmZvckVhY2goKGltYWdlSXRlbTogYW55LCBpbWFnZUluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgICAgaWYgKGltYWdlSXRlbS5maWQgPT09IGZpZCkge1xuICAgICAgICAgICAgICAgIGZpbmRJbWdJbmRleCA9IGltYWdlSW5kZXg7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgICBrZXk9e2Ake2luZGV4fS1hdHRhY2hgfVxuICAgICAgICAgICAgICAgIG92ZXJsYXlDbGFzc05hbWU9XCJmaWxlLW5hbWUtdGlwXCJcbiAgICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21SaWdodFwiXG4gICAgICAgICAgICAgICAgdGl0bGU9e2ZpbGVOYW1lIHx8IGRvY05hbWV9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImF0dGFjaG1lbnQtcHJldmlldy1pdGVtXCI+XG4gICAgICAgICAgICAgICAgICB7dXJsICYmICh0aGlzLmlzUGljdHVyZShmaWxlTmFtZSkgfHwgaXNJbWFnZShtaW1lVHlwZSkpICYmIChcbiAgICAgICAgICAgICAgICAgICAgPGltZyBvbkNsaWNrPXsoKSA9PiB0aGlzLnNldExpZ2h0Ym94KHsgaW5kZXg6IGZpbmRJbWdJbmRleCwgdmlzaWJsZTogdHJ1ZSB9KX0gc3JjPXtgJHt1cmx9YH0vPlxuICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgIHt1cmwgJiYgISh0aGlzLmlzUGljdHVyZShmaWxlTmFtZSkgfHwgaXNJbWFnZShtaW1lVHlwZSkpICYmIChcbiAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiaW1nLWFcIiBocmVmPXtocmVmVXJsICsgXCImcmVzaXplPW1kXCJ9PlxuICAgICAgICAgICAgICAgICAgICAgIDxpbWcgYWx0PXthdHRhY2hJZH0gc3JjPXt1cmx9Lz5cbiAgICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgIDxhIGhyZWY9e2hyZWZVcmwgKyBcIiZyZXNpemU9bWRcIn0gdGl0bGU9e2ZpbGVOYW1lIHx8IGRvY05hbWV9PlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj57ZmlsZU5hbWUgfHwgZG9jTmFtZX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm9wZXJhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJiZ1wiPlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICB7KHRoaXMuaXNQaWN0dXJlKGZpbGVOYW1lKSB8fCBpc0ltYWdlKG1pbWVUeXBlKSkgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMuc2V0TGlnaHRib3goeyBpbmRleDogZmluZEltZ0luZGV4LCB2aXNpYmxlOiB0cnVlIH0pfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT1cImV5ZVwiLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17dGhpcy5pc1BpY3R1cmUoZmlsZU5hbWUpIHx8IGlzSW1hZ2UobWltZVR5cGUpID8gXCJhLXBsXCIgOiBcIlwifVxuICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBjb2xvcjogQ09MT1JfUFJJTUFSWSB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICBocmVmPXtocmVmVXJsICsgXCImcmVzaXplPW9yaWdcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT1cImRvd25sb2FkXCIvPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvVG9vbHRpcD5cblxuICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgbWFyZ2luVG9wOiAtOCxcbiAgICAgICAgICAgICAgICBtYXJnaW5SaWdodDogMTUsXG4gICAgICAgICAgICAgICAgdGV4dEFsaWduOiBcImNlbnRlclwiLFxuICAgICAgICAgICAgICAgIGh5cGhlbnM6IFwiYXV0b1wiLFxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMzUsXG4gICAgICAgICAgICAgIH19PlxuICAgICAgICAgICAgICAgIDxwPnt0cmFuc1R5cGVUb05hbWUoXy5nZXQoYXR0YWNoLCBcImF0dGFjaERldGFpbHMuZG9jVHlwZVwiKSl9PC9wPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICk7XG4gICAgICAgIH0pfVxuICAgICAgICB7dGhpcy5yZW5kZXJJbWFnZUxpZ2h0Ym94KCl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcblxuICAgIHJldHVybiA8Qy5BdHRhY2htZW50PlxuXG4gICAgICB7dGhpcy5hdHRhY2hlc1JlbmRlcigpfVxuXG4gICAgPC9DLkF0dGFjaG1lbnQ+O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEF0dGFjaG1lbnQ7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBb0JBOzs7OztBQUdBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUFxS0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQU1BO0FBQ0E7QUFDQTtBQTlLQTtBQThLQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBMUxBO0FBMkxBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUF2TUE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQTRIQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQUlBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFHQTs7O0FBc0NBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUVBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFNQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQUhBO0FBQUE7QUFBQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQU1BO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUlBOzs7QUFFQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7OztBQWpaQTtBQUNBO0FBbVpBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/attachment/attachment-view.tsx
