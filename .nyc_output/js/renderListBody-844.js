__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OmitProps", function() { return OmitProps; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_animate__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-animate */ "./node_modules/rc-animate/es/Animate.js");
/* harmony import */ var _util_raf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_util/raf */ "./node_modules/antd/es/_util/raf.js");
/* harmony import */ var _util_type__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/es/_util/type.js");
/* harmony import */ var _ListItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ListItem */ "./node_modules/antd/es/transfer/ListItem.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}







var OmitProps = Object(_util_type__WEBPACK_IMPORTED_MODULE_4__["tuple"])('handleFilter', 'handleSelect', 'handleSelectAll', 'handleClear', 'body', 'checkedKeys');

var ListBody =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ListBody, _React$Component);

  function ListBody() {
    var _this;

    _classCallCheck(this, ListBody);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ListBody).apply(this, arguments));
    _this.state = {
      mounted: false
    };

    _this.onItemSelect = function (item) {
      var _this$props = _this.props,
          onItemSelect = _this$props.onItemSelect,
          selectedKeys = _this$props.selectedKeys;
      var checked = selectedKeys.indexOf(item.key) >= 0;
      onItemSelect(item.key, !checked);
    };

    return _this;
  }

  _createClass(ListBody, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.mountId = Object(_util_raf__WEBPACK_IMPORTED_MODULE_3__["default"])(function () {
        _this2.setState({
          mounted: true
        });
      });
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var _this$props2 = this.props,
          filteredRenderItems = _this$props2.filteredRenderItems,
          lazy = _this$props2.lazy;

      if (prevProps.filteredRenderItems.length !== filteredRenderItems.length && lazy !== false) {
        // TODO: Replace this with ref when react 15 support removed.
        var container = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(this);
        _util_raf__WEBPACK_IMPORTED_MODULE_3__["default"].cancel(this.lazyId);
        this.lazyId = Object(_util_raf__WEBPACK_IMPORTED_MODULE_3__["default"])(function () {
          if (container) {
            var scrollEvent = new Event('scroll', {
              bubbles: true
            });
            container.dispatchEvent(scrollEvent);
          }
        });
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      _util_raf__WEBPACK_IMPORTED_MODULE_3__["default"].cancel(this.mountId);
      _util_raf__WEBPACK_IMPORTED_MODULE_3__["default"].cancel(this.lazyId);
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var mounted = this.state.mounted;
      var _this$props3 = this.props,
          prefixCls = _this$props3.prefixCls,
          onScroll = _this$props3.onScroll,
          filteredRenderItems = _this$props3.filteredRenderItems,
          lazy = _this$props3.lazy,
          selectedKeys = _this$props3.selectedKeys,
          globalDisabled = _this$props3.disabled;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_animate__WEBPACK_IMPORTED_MODULE_2__["default"], {
        component: "ul",
        componentProps: {
          onScroll: onScroll
        },
        className: "".concat(prefixCls, "-content"),
        transitionName: mounted ? "".concat(prefixCls, "-content-item-highlight") : '',
        transitionLeave: false
      }, filteredRenderItems.map(function (_ref) {
        var renderedEl = _ref.renderedEl,
            renderedText = _ref.renderedText,
            item = _ref.item;
        var disabled = item.disabled;
        var checked = selectedKeys.indexOf(item.key) >= 0;
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ListItem__WEBPACK_IMPORTED_MODULE_5__["default"], {
          disabled: globalDisabled || disabled,
          key: item.key,
          item: item,
          lazy: lazy,
          renderedText: renderedText,
          renderedEl: renderedEl,
          checked: checked,
          prefixCls: prefixCls,
          onClick: _this3.onItemSelect
        });
      }));
    }
  }]);

  return ListBody;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var ListBodyWrapper = function ListBodyWrapper(props) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](ListBody, props);
};

/* harmony default export */ __webpack_exports__["default"] = (ListBodyWrapper);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90cmFuc2Zlci9yZW5kZXJMaXN0Qm9keS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvdHJhbnNmZXIvcmVuZGVyTGlzdEJvZHkuanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IGZpbmRET01Ob2RlIH0gZnJvbSAncmVhY3QtZG9tJztcbmltcG9ydCBBbmltYXRlIGZyb20gJ3JjLWFuaW1hdGUnO1xuaW1wb3J0IHJhZiBmcm9tICcuLi9fdXRpbC9yYWYnO1xuaW1wb3J0IHsgdHVwbGUgfSBmcm9tICcuLi9fdXRpbC90eXBlJztcbmltcG9ydCBMaXN0SXRlbSBmcm9tICcuL0xpc3RJdGVtJztcbmV4cG9ydCBjb25zdCBPbWl0UHJvcHMgPSB0dXBsZSgnaGFuZGxlRmlsdGVyJywgJ2hhbmRsZVNlbGVjdCcsICdoYW5kbGVTZWxlY3RBbGwnLCAnaGFuZGxlQ2xlYXInLCAnYm9keScsICdjaGVja2VkS2V5cycpO1xuY2xhc3MgTGlzdEJvZHkgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlciguLi5hcmd1bWVudHMpO1xuICAgICAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgICAgICAgbW91bnRlZDogZmFsc2UsXG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25JdGVtU2VsZWN0ID0gKGl0ZW0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgb25JdGVtU2VsZWN0LCBzZWxlY3RlZEtleXMgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCBjaGVja2VkID0gc2VsZWN0ZWRLZXlzLmluZGV4T2YoaXRlbS5rZXkpID49IDA7XG4gICAgICAgICAgICBvbkl0ZW1TZWxlY3QoaXRlbS5rZXksICFjaGVja2VkKTtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIHRoaXMubW91bnRJZCA9IHJhZigoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgbW91bnRlZDogdHJ1ZSB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMpIHtcbiAgICAgICAgY29uc3QgeyBmaWx0ZXJlZFJlbmRlckl0ZW1zLCBsYXp5IH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBpZiAocHJldlByb3BzLmZpbHRlcmVkUmVuZGVySXRlbXMubGVuZ3RoICE9PSBmaWx0ZXJlZFJlbmRlckl0ZW1zLmxlbmd0aCAmJiBsYXp5ICE9PSBmYWxzZSkge1xuICAgICAgICAgICAgLy8gVE9ETzogUmVwbGFjZSB0aGlzIHdpdGggcmVmIHdoZW4gcmVhY3QgMTUgc3VwcG9ydCByZW1vdmVkLlxuICAgICAgICAgICAgY29uc3QgY29udGFpbmVyID0gZmluZERPTU5vZGUodGhpcyk7XG4gICAgICAgICAgICByYWYuY2FuY2VsKHRoaXMubGF6eUlkKTtcbiAgICAgICAgICAgIHRoaXMubGF6eUlkID0gcmFmKCgpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoY29udGFpbmVyKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHNjcm9sbEV2ZW50ID0gbmV3IEV2ZW50KCdzY3JvbGwnLCB7IGJ1YmJsZXM6IHRydWUgfSk7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRhaW5lci5kaXNwYXRjaEV2ZW50KHNjcm9sbEV2ZW50KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgICAgcmFmLmNhbmNlbCh0aGlzLm1vdW50SWQpO1xuICAgICAgICByYWYuY2FuY2VsKHRoaXMubGF6eUlkKTtcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICBjb25zdCB7IG1vdW50ZWQgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGNvbnN0IHsgcHJlZml4Q2xzLCBvblNjcm9sbCwgZmlsdGVyZWRSZW5kZXJJdGVtcywgbGF6eSwgc2VsZWN0ZWRLZXlzLCBkaXNhYmxlZDogZ2xvYmFsRGlzYWJsZWQsIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICByZXR1cm4gKDxBbmltYXRlIGNvbXBvbmVudD1cInVsXCIgY29tcG9uZW50UHJvcHM9e3sgb25TY3JvbGwgfX0gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbnRlbnRgfSB0cmFuc2l0aW9uTmFtZT17bW91bnRlZCA/IGAke3ByZWZpeENsc30tY29udGVudC1pdGVtLWhpZ2hsaWdodGAgOiAnJ30gdHJhbnNpdGlvbkxlYXZlPXtmYWxzZX0+XG4gICAgICAgIHtmaWx0ZXJlZFJlbmRlckl0ZW1zLm1hcCgoeyByZW5kZXJlZEVsLCByZW5kZXJlZFRleHQsIGl0ZW0gfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBkaXNhYmxlZCB9ID0gaXRlbTtcbiAgICAgICAgICAgIGNvbnN0IGNoZWNrZWQgPSBzZWxlY3RlZEtleXMuaW5kZXhPZihpdGVtLmtleSkgPj0gMDtcbiAgICAgICAgICAgIHJldHVybiAoPExpc3RJdGVtIGRpc2FibGVkPXtnbG9iYWxEaXNhYmxlZCB8fCBkaXNhYmxlZH0ga2V5PXtpdGVtLmtleX0gaXRlbT17aXRlbX0gbGF6eT17bGF6eX0gcmVuZGVyZWRUZXh0PXtyZW5kZXJlZFRleHR9IHJlbmRlcmVkRWw9e3JlbmRlcmVkRWx9IGNoZWNrZWQ9e2NoZWNrZWR9IHByZWZpeENscz17cHJlZml4Q2xzfSBvbkNsaWNrPXt0aGlzLm9uSXRlbVNlbGVjdH0vPik7XG4gICAgICAgIH0pfVxuICAgICAgPC9BbmltYXRlPik7XG4gICAgfVxufVxuY29uc3QgTGlzdEJvZHlXcmFwcGVyID0gKHByb3BzKSA9PiA8TGlzdEJvZHkgey4uLnByb3BzfS8+O1xuZXhwb3J0IGRlZmF1bHQgTGlzdEJvZHlXcmFwcGVyO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFIQTtBQUNBO0FBTkE7QUFVQTtBQUNBOzs7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBR0E7OztBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkE7QUFPQTs7OztBQTdDQTtBQUNBO0FBOENBO0FBQUE7QUFBQTtBQUNBO0FBQUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/transfer/renderListBody.js
