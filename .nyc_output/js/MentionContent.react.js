__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var MentionContent = function MentionContent(_ref) {
  var children = _ref.children;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', {
    style: {
      backgroundColor: '#e6f3ff'
    }
  }, children);
};

/* harmony default export */ __webpack_exports__["default"] = (MentionContent);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvY29tcG9uZW50L01lbnRpb25Db250ZW50LnJlYWN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvY29tcG9uZW50L01lbnRpb25Db250ZW50LnJlYWN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5cbnZhciBNZW50aW9uQ29udGVudCA9IGZ1bmN0aW9uIE1lbnRpb25Db250ZW50KF9yZWYpIHtcbiAgdmFyIGNoaWxkcmVuID0gX3JlZi5jaGlsZHJlbjtcbiAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgJ3NwYW4nLFxuICAgIHsgc3R5bGU6IHsgYmFja2dyb3VuZENvbG9yOiAnI2U2ZjNmZicgfSB9LFxuICAgIGNoaWxkcmVuXG4gICk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBNZW50aW9uQ29udGVudDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/component/MentionContent.react.js
