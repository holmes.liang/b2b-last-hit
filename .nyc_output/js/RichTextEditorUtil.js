/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule RichTextEditorUtil
 * @format
 * 
 */


var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var SelectionState = __webpack_require__(/*! ./SelectionState */ "./node_modules/draft-js/lib/SelectionState.js");

var adjustBlockDepthForContentState = __webpack_require__(/*! ./adjustBlockDepthForContentState */ "./node_modules/draft-js/lib/adjustBlockDepthForContentState.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");

var RichTextEditorUtil = {
  currentBlockContainsLink: function currentBlockContainsLink(editorState) {
    var selection = editorState.getSelection();
    var contentState = editorState.getCurrentContent();
    var entityMap = contentState.getEntityMap();
    return contentState.getBlockForKey(selection.getAnchorKey()).getCharacterList().slice(selection.getStartOffset(), selection.getEndOffset()).some(function (v) {
      var entity = v.getEntity();
      return !!entity && entityMap.__get(entity).getType() === 'LINK';
    });
  },
  getCurrentBlockType: function getCurrentBlockType(editorState) {
    var selection = editorState.getSelection();
    return editorState.getCurrentContent().getBlockForKey(selection.getStartKey()).getType();
  },
  getDataObjectForLinkURL: function getDataObjectForLinkURL(uri) {
    return {
      url: uri.toString()
    };
  },
  handleKeyCommand: function handleKeyCommand(editorState, command) {
    switch (command) {
      case 'bold':
        return RichTextEditorUtil.toggleInlineStyle(editorState, 'BOLD');

      case 'italic':
        return RichTextEditorUtil.toggleInlineStyle(editorState, 'ITALIC');

      case 'underline':
        return RichTextEditorUtil.toggleInlineStyle(editorState, 'UNDERLINE');

      case 'code':
        return RichTextEditorUtil.toggleCode(editorState);

      case 'backspace':
      case 'backspace-word':
      case 'backspace-to-start-of-line':
        return RichTextEditorUtil.onBackspace(editorState);

      case 'delete':
      case 'delete-word':
      case 'delete-to-end-of-block':
        return RichTextEditorUtil.onDelete(editorState);

      default:
        // they may have custom editor commands; ignore those
        return null;
    }
  },
  insertSoftNewline: function insertSoftNewline(editorState) {
    var contentState = DraftModifier.insertText(editorState.getCurrentContent(), editorState.getSelection(), '\n', editorState.getCurrentInlineStyle(), null);
    var newEditorState = EditorState.push(editorState, contentState, 'insert-characters');
    return EditorState.forceSelection(newEditorState, contentState.getSelectionAfter());
  },

  /**
   * For collapsed selections at the start of styled blocks, backspace should
   * just remove the existing style.
   */
  onBackspace: function onBackspace(editorState) {
    var selection = editorState.getSelection();

    if (!selection.isCollapsed() || selection.getAnchorOffset() || selection.getFocusOffset()) {
      return null;
    } // First, try to remove a preceding atomic block.


    var content = editorState.getCurrentContent();
    var startKey = selection.getStartKey();
    var blockBefore = content.getBlockBefore(startKey);

    if (blockBefore && blockBefore.getType() === 'atomic') {
      var blockMap = content.getBlockMap()['delete'](blockBefore.getKey());
      var withoutAtomicBlock = content.merge({
        blockMap: blockMap,
        selectionAfter: selection
      });

      if (withoutAtomicBlock !== content) {
        return EditorState.push(editorState, withoutAtomicBlock, 'remove-range');
      }
    } // If that doesn't succeed, try to remove the current block style.


    var withoutBlockStyle = RichTextEditorUtil.tryToRemoveBlockStyle(editorState);

    if (withoutBlockStyle) {
      return EditorState.push(editorState, withoutBlockStyle, 'change-block-type');
    }

    return null;
  },
  onDelete: function onDelete(editorState) {
    var selection = editorState.getSelection();

    if (!selection.isCollapsed()) {
      return null;
    }

    var content = editorState.getCurrentContent();
    var startKey = selection.getStartKey();
    var block = content.getBlockForKey(startKey);
    var length = block.getLength(); // The cursor is somewhere within the text. Behave normally.

    if (selection.getStartOffset() < length) {
      return null;
    }

    var blockAfter = content.getBlockAfter(startKey);

    if (!blockAfter || blockAfter.getType() !== 'atomic') {
      return null;
    }

    var atomicBlockTarget = selection.merge({
      focusKey: blockAfter.getKey(),
      focusOffset: blockAfter.getLength()
    });
    var withoutAtomicBlock = DraftModifier.removeRange(content, atomicBlockTarget, 'forward');

    if (withoutAtomicBlock !== content) {
      return EditorState.push(editorState, withoutAtomicBlock, 'remove-range');
    }

    return null;
  },
  onTab: function onTab(event, editorState, maxDepth) {
    var selection = editorState.getSelection();
    var key = selection.getAnchorKey();

    if (key !== selection.getFocusKey()) {
      return editorState;
    }

    var content = editorState.getCurrentContent();
    var block = content.getBlockForKey(key);
    var type = block.getType();

    if (type !== 'unordered-list-item' && type !== 'ordered-list-item') {
      return editorState;
    }

    event.preventDefault(); // Only allow indenting one level beyond the block above, and only if
    // the block above is a list item as well.

    var blockAbove = content.getBlockBefore(key);

    if (!blockAbove) {
      return editorState;
    }

    var typeAbove = blockAbove.getType();

    if (typeAbove !== 'unordered-list-item' && typeAbove !== 'ordered-list-item') {
      return editorState;
    }

    var depth = block.getDepth();

    if (!event.shiftKey && depth === maxDepth) {
      return editorState;
    }

    maxDepth = Math.min(blockAbove.getDepth() + 1, maxDepth);
    var withAdjustment = adjustBlockDepthForContentState(content, selection, event.shiftKey ? -1 : 1, maxDepth);
    return EditorState.push(editorState, withAdjustment, 'adjust-depth');
  },
  toggleBlockType: function toggleBlockType(editorState, blockType) {
    var selection = editorState.getSelection();
    var startKey = selection.getStartKey();
    var endKey = selection.getEndKey();
    var content = editorState.getCurrentContent();
    var target = selection; // Triple-click can lead to a selection that includes offset 0 of the
    // following block. The `SelectionState` for this case is accurate, but
    // we should avoid toggling block type for the trailing block because it
    // is a confusing interaction.

    if (startKey !== endKey && selection.getEndOffset() === 0) {
      var blockBefore = nullthrows(content.getBlockBefore(endKey));
      endKey = blockBefore.getKey();
      target = target.merge({
        anchorKey: startKey,
        anchorOffset: selection.getStartOffset(),
        focusKey: endKey,
        focusOffset: blockBefore.getLength(),
        isBackward: false
      });
    }

    var hasAtomicBlock = content.getBlockMap().skipWhile(function (_, k) {
      return k !== startKey;
    }).reverse().skipWhile(function (_, k) {
      return k !== endKey;
    }).some(function (v) {
      return v.getType() === 'atomic';
    });

    if (hasAtomicBlock) {
      return editorState;
    }

    var typeToSet = content.getBlockForKey(startKey).getType() === blockType ? 'unstyled' : blockType;
    return EditorState.push(editorState, DraftModifier.setBlockType(content, target, typeToSet), 'change-block-type');
  },
  toggleCode: function toggleCode(editorState) {
    var selection = editorState.getSelection();
    var anchorKey = selection.getAnchorKey();
    var focusKey = selection.getFocusKey();

    if (selection.isCollapsed() || anchorKey !== focusKey) {
      return RichTextEditorUtil.toggleBlockType(editorState, 'code-block');
    }

    return RichTextEditorUtil.toggleInlineStyle(editorState, 'CODE');
  },

  /**
   * Toggle the specified inline style for the selection. If the
   * user's selection is collapsed, apply or remove the style for the
   * internal state. If it is not collapsed, apply the change directly
   * to the document state.
   */
  toggleInlineStyle: function toggleInlineStyle(editorState, inlineStyle) {
    var selection = editorState.getSelection();
    var currentStyle = editorState.getCurrentInlineStyle(); // If the selection is collapsed, toggle the specified style on or off and
    // set the result as the new inline style override. This will then be
    // used as the inline style for the next character to be inserted.

    if (selection.isCollapsed()) {
      return EditorState.setInlineStyleOverride(editorState, currentStyle.has(inlineStyle) ? currentStyle.remove(inlineStyle) : currentStyle.add(inlineStyle));
    } // If characters are selected, immediately apply or remove the
    // inline style on the document state itself.


    var content = editorState.getCurrentContent();
    var newContent; // If the style is already present for the selection range, remove it.
    // Otherwise, apply it.

    if (currentStyle.has(inlineStyle)) {
      newContent = DraftModifier.removeInlineStyle(content, selection, inlineStyle);
    } else {
      newContent = DraftModifier.applyInlineStyle(content, selection, inlineStyle);
    }

    return EditorState.push(editorState, newContent, 'change-inline-style');
  },
  toggleLink: function toggleLink(editorState, targetSelection, entityKey) {
    var withoutLink = DraftModifier.applyEntity(editorState.getCurrentContent(), targetSelection, entityKey);
    return EditorState.push(editorState, withoutLink, 'apply-entity');
  },

  /**
   * When a collapsed cursor is at the start of the first styled block, or
   * an empty styled block, changes block to 'unstyled'. Returns null if
   * block or selection does not meet that criteria.
   */
  tryToRemoveBlockStyle: function tryToRemoveBlockStyle(editorState) {
    var selection = editorState.getSelection();
    var offset = selection.getAnchorOffset();

    if (selection.isCollapsed() && offset === 0) {
      var key = selection.getAnchorKey();
      var content = editorState.getCurrentContent();
      var block = content.getBlockForKey(key);
      var firstBlock = content.getFirstBlock();

      if (block.getLength() > 0 && block !== firstBlock) {
        return null;
      }

      var type = block.getType();
      var blockBefore = content.getBlockBefore(key);

      if (type === 'code-block' && blockBefore && blockBefore.getType() === 'code-block' && blockBefore.getLength() !== 0) {
        return null;
      }

      if (type !== 'unstyled') {
        return DraftModifier.setBlockType(content, selection, 'unstyled');
      }
    }

    return null;
  }
};
module.exports = RichTextEditorUtil;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL1JpY2hUZXh0RWRpdG9yVXRpbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9SaWNoVGV4dEVkaXRvclV0aWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBSaWNoVGV4dEVkaXRvclV0aWxcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcbnZhciBTZWxlY3Rpb25TdGF0ZSA9IHJlcXVpcmUoJy4vU2VsZWN0aW9uU3RhdGUnKTtcblxudmFyIGFkanVzdEJsb2NrRGVwdGhGb3JDb250ZW50U3RhdGUgPSByZXF1aXJlKCcuL2FkanVzdEJsb2NrRGVwdGhGb3JDb250ZW50U3RhdGUnKTtcbnZhciBudWxsdGhyb3dzID0gcmVxdWlyZSgnZmJqcy9saWIvbnVsbHRocm93cycpO1xuXG52YXIgUmljaFRleHRFZGl0b3JVdGlsID0ge1xuICBjdXJyZW50QmxvY2tDb250YWluc0xpbms6IGZ1bmN0aW9uIGN1cnJlbnRCbG9ja0NvbnRhaW5zTGluayhlZGl0b3JTdGF0ZSkge1xuICAgIHZhciBzZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcbiAgICB2YXIgY29udGVudFN0YXRlID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgICB2YXIgZW50aXR5TWFwID0gY29udGVudFN0YXRlLmdldEVudGl0eU1hcCgpO1xuICAgIHJldHVybiBjb250ZW50U3RhdGUuZ2V0QmxvY2tGb3JLZXkoc2VsZWN0aW9uLmdldEFuY2hvcktleSgpKS5nZXRDaGFyYWN0ZXJMaXN0KCkuc2xpY2Uoc2VsZWN0aW9uLmdldFN0YXJ0T2Zmc2V0KCksIHNlbGVjdGlvbi5nZXRFbmRPZmZzZXQoKSkuc29tZShmdW5jdGlvbiAodikge1xuICAgICAgdmFyIGVudGl0eSA9IHYuZ2V0RW50aXR5KCk7XG4gICAgICByZXR1cm4gISFlbnRpdHkgJiYgZW50aXR5TWFwLl9fZ2V0KGVudGl0eSkuZ2V0VHlwZSgpID09PSAnTElOSyc7XG4gICAgfSk7XG4gIH0sXG5cbiAgZ2V0Q3VycmVudEJsb2NrVHlwZTogZnVuY3Rpb24gZ2V0Q3VycmVudEJsb2NrVHlwZShlZGl0b3JTdGF0ZSkge1xuICAgIHZhciBzZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcbiAgICByZXR1cm4gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKS5nZXRCbG9ja0ZvcktleShzZWxlY3Rpb24uZ2V0U3RhcnRLZXkoKSkuZ2V0VHlwZSgpO1xuICB9LFxuXG4gIGdldERhdGFPYmplY3RGb3JMaW5rVVJMOiBmdW5jdGlvbiBnZXREYXRhT2JqZWN0Rm9yTGlua1VSTCh1cmkpIHtcbiAgICByZXR1cm4geyB1cmw6IHVyaS50b1N0cmluZygpIH07XG4gIH0sXG5cbiAgaGFuZGxlS2V5Q29tbWFuZDogZnVuY3Rpb24gaGFuZGxlS2V5Q29tbWFuZChlZGl0b3JTdGF0ZSwgY29tbWFuZCkge1xuICAgIHN3aXRjaCAoY29tbWFuZCkge1xuICAgICAgY2FzZSAnYm9sZCc6XG4gICAgICAgIHJldHVybiBSaWNoVGV4dEVkaXRvclV0aWwudG9nZ2xlSW5saW5lU3R5bGUoZWRpdG9yU3RhdGUsICdCT0xEJyk7XG4gICAgICBjYXNlICdpdGFsaWMnOlxuICAgICAgICByZXR1cm4gUmljaFRleHRFZGl0b3JVdGlsLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnSVRBTElDJyk7XG4gICAgICBjYXNlICd1bmRlcmxpbmUnOlxuICAgICAgICByZXR1cm4gUmljaFRleHRFZGl0b3JVdGlsLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnVU5ERVJMSU5FJyk7XG4gICAgICBjYXNlICdjb2RlJzpcbiAgICAgICAgcmV0dXJuIFJpY2hUZXh0RWRpdG9yVXRpbC50b2dnbGVDb2RlKGVkaXRvclN0YXRlKTtcbiAgICAgIGNhc2UgJ2JhY2tzcGFjZSc6XG4gICAgICBjYXNlICdiYWNrc3BhY2Utd29yZCc6XG4gICAgICBjYXNlICdiYWNrc3BhY2UtdG8tc3RhcnQtb2YtbGluZSc6XG4gICAgICAgIHJldHVybiBSaWNoVGV4dEVkaXRvclV0aWwub25CYWNrc3BhY2UoZWRpdG9yU3RhdGUpO1xuICAgICAgY2FzZSAnZGVsZXRlJzpcbiAgICAgIGNhc2UgJ2RlbGV0ZS13b3JkJzpcbiAgICAgIGNhc2UgJ2RlbGV0ZS10by1lbmQtb2YtYmxvY2snOlxuICAgICAgICByZXR1cm4gUmljaFRleHRFZGl0b3JVdGlsLm9uRGVsZXRlKGVkaXRvclN0YXRlKTtcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIC8vIHRoZXkgbWF5IGhhdmUgY3VzdG9tIGVkaXRvciBjb21tYW5kczsgaWdub3JlIHRob3NlXG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfSxcblxuICBpbnNlcnRTb2Z0TmV3bGluZTogZnVuY3Rpb24gaW5zZXJ0U29mdE5ld2xpbmUoZWRpdG9yU3RhdGUpIHtcbiAgICB2YXIgY29udGVudFN0YXRlID0gRHJhZnRNb2RpZmllci5pbnNlcnRUZXh0KGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCksIGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpLCAnXFxuJywgZWRpdG9yU3RhdGUuZ2V0Q3VycmVudElubGluZVN0eWxlKCksIG51bGwpO1xuXG4gICAgdmFyIG5ld0VkaXRvclN0YXRlID0gRWRpdG9yU3RhdGUucHVzaChlZGl0b3JTdGF0ZSwgY29udGVudFN0YXRlLCAnaW5zZXJ0LWNoYXJhY3RlcnMnKTtcblxuICAgIHJldHVybiBFZGl0b3JTdGF0ZS5mb3JjZVNlbGVjdGlvbihuZXdFZGl0b3JTdGF0ZSwgY29udGVudFN0YXRlLmdldFNlbGVjdGlvbkFmdGVyKCkpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBGb3IgY29sbGFwc2VkIHNlbGVjdGlvbnMgYXQgdGhlIHN0YXJ0IG9mIHN0eWxlZCBibG9ja3MsIGJhY2tzcGFjZSBzaG91bGRcbiAgICoganVzdCByZW1vdmUgdGhlIGV4aXN0aW5nIHN0eWxlLlxuICAgKi9cbiAgb25CYWNrc3BhY2U6IGZ1bmN0aW9uIG9uQmFja3NwYWNlKGVkaXRvclN0YXRlKSB7XG4gICAgdmFyIHNlbGVjdGlvbiA9IGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpO1xuICAgIGlmICghc2VsZWN0aW9uLmlzQ29sbGFwc2VkKCkgfHwgc2VsZWN0aW9uLmdldEFuY2hvck9mZnNldCgpIHx8IHNlbGVjdGlvbi5nZXRGb2N1c09mZnNldCgpKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICAvLyBGaXJzdCwgdHJ5IHRvIHJlbW92ZSBhIHByZWNlZGluZyBhdG9taWMgYmxvY2suXG4gICAgdmFyIGNvbnRlbnQgPSBlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpO1xuICAgIHZhciBzdGFydEtleSA9IHNlbGVjdGlvbi5nZXRTdGFydEtleSgpO1xuICAgIHZhciBibG9ja0JlZm9yZSA9IGNvbnRlbnQuZ2V0QmxvY2tCZWZvcmUoc3RhcnRLZXkpO1xuXG4gICAgaWYgKGJsb2NrQmVmb3JlICYmIGJsb2NrQmVmb3JlLmdldFR5cGUoKSA9PT0gJ2F0b21pYycpIHtcbiAgICAgIHZhciBibG9ja01hcCA9IGNvbnRlbnQuZ2V0QmxvY2tNYXAoKVsnZGVsZXRlJ10oYmxvY2tCZWZvcmUuZ2V0S2V5KCkpO1xuICAgICAgdmFyIHdpdGhvdXRBdG9taWNCbG9jayA9IGNvbnRlbnQubWVyZ2Uoe1xuICAgICAgICBibG9ja01hcDogYmxvY2tNYXAsXG4gICAgICAgIHNlbGVjdGlvbkFmdGVyOiBzZWxlY3Rpb25cbiAgICAgIH0pO1xuICAgICAgaWYgKHdpdGhvdXRBdG9taWNCbG9jayAhPT0gY29udGVudCkge1xuICAgICAgICByZXR1cm4gRWRpdG9yU3RhdGUucHVzaChlZGl0b3JTdGF0ZSwgd2l0aG91dEF0b21pY0Jsb2NrLCAncmVtb3ZlLXJhbmdlJyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gSWYgdGhhdCBkb2Vzbid0IHN1Y2NlZWQsIHRyeSB0byByZW1vdmUgdGhlIGN1cnJlbnQgYmxvY2sgc3R5bGUuXG4gICAgdmFyIHdpdGhvdXRCbG9ja1N0eWxlID0gUmljaFRleHRFZGl0b3JVdGlsLnRyeVRvUmVtb3ZlQmxvY2tTdHlsZShlZGl0b3JTdGF0ZSk7XG5cbiAgICBpZiAod2l0aG91dEJsb2NrU3R5bGUpIHtcbiAgICAgIHJldHVybiBFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCB3aXRob3V0QmxvY2tTdHlsZSwgJ2NoYW5nZS1ibG9jay10eXBlJyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG51bGw7XG4gIH0sXG5cbiAgb25EZWxldGU6IGZ1bmN0aW9uIG9uRGVsZXRlKGVkaXRvclN0YXRlKSB7XG4gICAgdmFyIHNlbGVjdGlvbiA9IGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpO1xuICAgIGlmICghc2VsZWN0aW9uLmlzQ29sbGFwc2VkKCkpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHZhciBjb250ZW50ID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgICB2YXIgc3RhcnRLZXkgPSBzZWxlY3Rpb24uZ2V0U3RhcnRLZXkoKTtcbiAgICB2YXIgYmxvY2sgPSBjb250ZW50LmdldEJsb2NrRm9yS2V5KHN0YXJ0S2V5KTtcbiAgICB2YXIgbGVuZ3RoID0gYmxvY2suZ2V0TGVuZ3RoKCk7XG5cbiAgICAvLyBUaGUgY3Vyc29yIGlzIHNvbWV3aGVyZSB3aXRoaW4gdGhlIHRleHQuIEJlaGF2ZSBub3JtYWxseS5cbiAgICBpZiAoc2VsZWN0aW9uLmdldFN0YXJ0T2Zmc2V0KCkgPCBsZW5ndGgpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHZhciBibG9ja0FmdGVyID0gY29udGVudC5nZXRCbG9ja0FmdGVyKHN0YXJ0S2V5KTtcblxuICAgIGlmICghYmxvY2tBZnRlciB8fCBibG9ja0FmdGVyLmdldFR5cGUoKSAhPT0gJ2F0b21pYycpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHZhciBhdG9taWNCbG9ja1RhcmdldCA9IHNlbGVjdGlvbi5tZXJnZSh7XG4gICAgICBmb2N1c0tleTogYmxvY2tBZnRlci5nZXRLZXkoKSxcbiAgICAgIGZvY3VzT2Zmc2V0OiBibG9ja0FmdGVyLmdldExlbmd0aCgpXG4gICAgfSk7XG5cbiAgICB2YXIgd2l0aG91dEF0b21pY0Jsb2NrID0gRHJhZnRNb2RpZmllci5yZW1vdmVSYW5nZShjb250ZW50LCBhdG9taWNCbG9ja1RhcmdldCwgJ2ZvcndhcmQnKTtcblxuICAgIGlmICh3aXRob3V0QXRvbWljQmxvY2sgIT09IGNvbnRlbnQpIHtcbiAgICAgIHJldHVybiBFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCB3aXRob3V0QXRvbWljQmxvY2ssICdyZW1vdmUtcmFuZ2UnKTtcbiAgICB9XG5cbiAgICByZXR1cm4gbnVsbDtcbiAgfSxcblxuICBvblRhYjogZnVuY3Rpb24gb25UYWIoZXZlbnQsIGVkaXRvclN0YXRlLCBtYXhEZXB0aCkge1xuICAgIHZhciBzZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcbiAgICB2YXIga2V5ID0gc2VsZWN0aW9uLmdldEFuY2hvcktleSgpO1xuICAgIGlmIChrZXkgIT09IHNlbGVjdGlvbi5nZXRGb2N1c0tleSgpKSB7XG4gICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgfVxuXG4gICAgdmFyIGNvbnRlbnQgPSBlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpO1xuICAgIHZhciBibG9jayA9IGNvbnRlbnQuZ2V0QmxvY2tGb3JLZXkoa2V5KTtcbiAgICB2YXIgdHlwZSA9IGJsb2NrLmdldFR5cGUoKTtcbiAgICBpZiAodHlwZSAhPT0gJ3Vub3JkZXJlZC1saXN0LWl0ZW0nICYmIHR5cGUgIT09ICdvcmRlcmVkLWxpc3QtaXRlbScpIHtcbiAgICAgIHJldHVybiBlZGl0b3JTdGF0ZTtcbiAgICB9XG5cbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgLy8gT25seSBhbGxvdyBpbmRlbnRpbmcgb25lIGxldmVsIGJleW9uZCB0aGUgYmxvY2sgYWJvdmUsIGFuZCBvbmx5IGlmXG4gICAgLy8gdGhlIGJsb2NrIGFib3ZlIGlzIGEgbGlzdCBpdGVtIGFzIHdlbGwuXG4gICAgdmFyIGJsb2NrQWJvdmUgPSBjb250ZW50LmdldEJsb2NrQmVmb3JlKGtleSk7XG4gICAgaWYgKCFibG9ja0Fib3ZlKSB7XG4gICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgfVxuXG4gICAgdmFyIHR5cGVBYm92ZSA9IGJsb2NrQWJvdmUuZ2V0VHlwZSgpO1xuICAgIGlmICh0eXBlQWJvdmUgIT09ICd1bm9yZGVyZWQtbGlzdC1pdGVtJyAmJiB0eXBlQWJvdmUgIT09ICdvcmRlcmVkLWxpc3QtaXRlbScpIHtcbiAgICAgIHJldHVybiBlZGl0b3JTdGF0ZTtcbiAgICB9XG5cbiAgICB2YXIgZGVwdGggPSBibG9jay5nZXREZXB0aCgpO1xuICAgIGlmICghZXZlbnQuc2hpZnRLZXkgJiYgZGVwdGggPT09IG1heERlcHRoKSB7XG4gICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgfVxuXG4gICAgbWF4RGVwdGggPSBNYXRoLm1pbihibG9ja0Fib3ZlLmdldERlcHRoKCkgKyAxLCBtYXhEZXB0aCk7XG5cbiAgICB2YXIgd2l0aEFkanVzdG1lbnQgPSBhZGp1c3RCbG9ja0RlcHRoRm9yQ29udGVudFN0YXRlKGNvbnRlbnQsIHNlbGVjdGlvbiwgZXZlbnQuc2hpZnRLZXkgPyAtMSA6IDEsIG1heERlcHRoKTtcblxuICAgIHJldHVybiBFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCB3aXRoQWRqdXN0bWVudCwgJ2FkanVzdC1kZXB0aCcpO1xuICB9LFxuXG4gIHRvZ2dsZUJsb2NrVHlwZTogZnVuY3Rpb24gdG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCBibG9ja1R5cGUpIHtcbiAgICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gICAgdmFyIHN0YXJ0S2V5ID0gc2VsZWN0aW9uLmdldFN0YXJ0S2V5KCk7XG4gICAgdmFyIGVuZEtleSA9IHNlbGVjdGlvbi5nZXRFbmRLZXkoKTtcbiAgICB2YXIgY29udGVudCA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdmFyIHRhcmdldCA9IHNlbGVjdGlvbjtcblxuICAgIC8vIFRyaXBsZS1jbGljayBjYW4gbGVhZCB0byBhIHNlbGVjdGlvbiB0aGF0IGluY2x1ZGVzIG9mZnNldCAwIG9mIHRoZVxuICAgIC8vIGZvbGxvd2luZyBibG9jay4gVGhlIGBTZWxlY3Rpb25TdGF0ZWAgZm9yIHRoaXMgY2FzZSBpcyBhY2N1cmF0ZSwgYnV0XG4gICAgLy8gd2Ugc2hvdWxkIGF2b2lkIHRvZ2dsaW5nIGJsb2NrIHR5cGUgZm9yIHRoZSB0cmFpbGluZyBibG9jayBiZWNhdXNlIGl0XG4gICAgLy8gaXMgYSBjb25mdXNpbmcgaW50ZXJhY3Rpb24uXG4gICAgaWYgKHN0YXJ0S2V5ICE9PSBlbmRLZXkgJiYgc2VsZWN0aW9uLmdldEVuZE9mZnNldCgpID09PSAwKSB7XG4gICAgICB2YXIgYmxvY2tCZWZvcmUgPSBudWxsdGhyb3dzKGNvbnRlbnQuZ2V0QmxvY2tCZWZvcmUoZW5kS2V5KSk7XG4gICAgICBlbmRLZXkgPSBibG9ja0JlZm9yZS5nZXRLZXkoKTtcbiAgICAgIHRhcmdldCA9IHRhcmdldC5tZXJnZSh7XG4gICAgICAgIGFuY2hvcktleTogc3RhcnRLZXksXG4gICAgICAgIGFuY2hvck9mZnNldDogc2VsZWN0aW9uLmdldFN0YXJ0T2Zmc2V0KCksXG4gICAgICAgIGZvY3VzS2V5OiBlbmRLZXksXG4gICAgICAgIGZvY3VzT2Zmc2V0OiBibG9ja0JlZm9yZS5nZXRMZW5ndGgoKSxcbiAgICAgICAgaXNCYWNrd2FyZDogZmFsc2VcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIHZhciBoYXNBdG9taWNCbG9jayA9IGNvbnRlbnQuZ2V0QmxvY2tNYXAoKS5za2lwV2hpbGUoZnVuY3Rpb24gKF8sIGspIHtcbiAgICAgIHJldHVybiBrICE9PSBzdGFydEtleTtcbiAgICB9KS5yZXZlcnNlKCkuc2tpcFdoaWxlKGZ1bmN0aW9uIChfLCBrKSB7XG4gICAgICByZXR1cm4gayAhPT0gZW5kS2V5O1xuICAgIH0pLnNvbWUoZnVuY3Rpb24gKHYpIHtcbiAgICAgIHJldHVybiB2LmdldFR5cGUoKSA9PT0gJ2F0b21pYyc7XG4gICAgfSk7XG5cbiAgICBpZiAoaGFzQXRvbWljQmxvY2spIHtcbiAgICAgIHJldHVybiBlZGl0b3JTdGF0ZTtcbiAgICB9XG5cbiAgICB2YXIgdHlwZVRvU2V0ID0gY29udGVudC5nZXRCbG9ja0ZvcktleShzdGFydEtleSkuZ2V0VHlwZSgpID09PSBibG9ja1R5cGUgPyAndW5zdHlsZWQnIDogYmxvY2tUeXBlO1xuXG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIERyYWZ0TW9kaWZpZXIuc2V0QmxvY2tUeXBlKGNvbnRlbnQsIHRhcmdldCwgdHlwZVRvU2V0KSwgJ2NoYW5nZS1ibG9jay10eXBlJyk7XG4gIH0sXG5cbiAgdG9nZ2xlQ29kZTogZnVuY3Rpb24gdG9nZ2xlQ29kZShlZGl0b3JTdGF0ZSkge1xuICAgIHZhciBzZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcbiAgICB2YXIgYW5jaG9yS2V5ID0gc2VsZWN0aW9uLmdldEFuY2hvcktleSgpO1xuICAgIHZhciBmb2N1c0tleSA9IHNlbGVjdGlvbi5nZXRGb2N1c0tleSgpO1xuXG4gICAgaWYgKHNlbGVjdGlvbi5pc0NvbGxhcHNlZCgpIHx8IGFuY2hvcktleSAhPT0gZm9jdXNLZXkpIHtcbiAgICAgIHJldHVybiBSaWNoVGV4dEVkaXRvclV0aWwudG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCAnY29kZS1ibG9jaycpO1xuICAgIH1cblxuICAgIHJldHVybiBSaWNoVGV4dEVkaXRvclV0aWwudG9nZ2xlSW5saW5lU3R5bGUoZWRpdG9yU3RhdGUsICdDT0RFJyk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFRvZ2dsZSB0aGUgc3BlY2lmaWVkIGlubGluZSBzdHlsZSBmb3IgdGhlIHNlbGVjdGlvbi4gSWYgdGhlXG4gICAqIHVzZXIncyBzZWxlY3Rpb24gaXMgY29sbGFwc2VkLCBhcHBseSBvciByZW1vdmUgdGhlIHN0eWxlIGZvciB0aGVcbiAgICogaW50ZXJuYWwgc3RhdGUuIElmIGl0IGlzIG5vdCBjb2xsYXBzZWQsIGFwcGx5IHRoZSBjaGFuZ2UgZGlyZWN0bHlcbiAgICogdG8gdGhlIGRvY3VtZW50IHN0YXRlLlxuICAgKi9cbiAgdG9nZ2xlSW5saW5lU3R5bGU6IGZ1bmN0aW9uIHRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCBpbmxpbmVTdHlsZSkge1xuICAgIHZhciBzZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcbiAgICB2YXIgY3VycmVudFN0eWxlID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudElubGluZVN0eWxlKCk7XG5cbiAgICAvLyBJZiB0aGUgc2VsZWN0aW9uIGlzIGNvbGxhcHNlZCwgdG9nZ2xlIHRoZSBzcGVjaWZpZWQgc3R5bGUgb24gb3Igb2ZmIGFuZFxuICAgIC8vIHNldCB0aGUgcmVzdWx0IGFzIHRoZSBuZXcgaW5saW5lIHN0eWxlIG92ZXJyaWRlLiBUaGlzIHdpbGwgdGhlbiBiZVxuICAgIC8vIHVzZWQgYXMgdGhlIGlubGluZSBzdHlsZSBmb3IgdGhlIG5leHQgY2hhcmFjdGVyIHRvIGJlIGluc2VydGVkLlxuICAgIGlmIChzZWxlY3Rpb24uaXNDb2xsYXBzZWQoKSkge1xuICAgICAgcmV0dXJuIEVkaXRvclN0YXRlLnNldElubGluZVN0eWxlT3ZlcnJpZGUoZWRpdG9yU3RhdGUsIGN1cnJlbnRTdHlsZS5oYXMoaW5saW5lU3R5bGUpID8gY3VycmVudFN0eWxlLnJlbW92ZShpbmxpbmVTdHlsZSkgOiBjdXJyZW50U3R5bGUuYWRkKGlubGluZVN0eWxlKSk7XG4gICAgfVxuXG4gICAgLy8gSWYgY2hhcmFjdGVycyBhcmUgc2VsZWN0ZWQsIGltbWVkaWF0ZWx5IGFwcGx5IG9yIHJlbW92ZSB0aGVcbiAgICAvLyBpbmxpbmUgc3R5bGUgb24gdGhlIGRvY3VtZW50IHN0YXRlIGl0c2VsZi5cbiAgICB2YXIgY29udGVudCA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdmFyIG5ld0NvbnRlbnQ7XG5cbiAgICAvLyBJZiB0aGUgc3R5bGUgaXMgYWxyZWFkeSBwcmVzZW50IGZvciB0aGUgc2VsZWN0aW9uIHJhbmdlLCByZW1vdmUgaXQuXG4gICAgLy8gT3RoZXJ3aXNlLCBhcHBseSBpdC5cbiAgICBpZiAoY3VycmVudFN0eWxlLmhhcyhpbmxpbmVTdHlsZSkpIHtcbiAgICAgIG5ld0NvbnRlbnQgPSBEcmFmdE1vZGlmaWVyLnJlbW92ZUlubGluZVN0eWxlKGNvbnRlbnQsIHNlbGVjdGlvbiwgaW5saW5lU3R5bGUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBuZXdDb250ZW50ID0gRHJhZnRNb2RpZmllci5hcHBseUlubGluZVN0eWxlKGNvbnRlbnQsIHNlbGVjdGlvbiwgaW5saW5lU3R5bGUpO1xuICAgIH1cblxuICAgIHJldHVybiBFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCBuZXdDb250ZW50LCAnY2hhbmdlLWlubGluZS1zdHlsZScpO1xuICB9LFxuXG4gIHRvZ2dsZUxpbms6IGZ1bmN0aW9uIHRvZ2dsZUxpbmsoZWRpdG9yU3RhdGUsIHRhcmdldFNlbGVjdGlvbiwgZW50aXR5S2V5KSB7XG4gICAgdmFyIHdpdGhvdXRMaW5rID0gRHJhZnRNb2RpZmllci5hcHBseUVudGl0eShlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpLCB0YXJnZXRTZWxlY3Rpb24sIGVudGl0eUtleSk7XG5cbiAgICByZXR1cm4gRWRpdG9yU3RhdGUucHVzaChlZGl0b3JTdGF0ZSwgd2l0aG91dExpbmssICdhcHBseS1lbnRpdHknKTtcbiAgfSxcblxuICAvKipcbiAgICogV2hlbiBhIGNvbGxhcHNlZCBjdXJzb3IgaXMgYXQgdGhlIHN0YXJ0IG9mIHRoZSBmaXJzdCBzdHlsZWQgYmxvY2ssIG9yXG4gICAqIGFuIGVtcHR5IHN0eWxlZCBibG9jaywgY2hhbmdlcyBibG9jayB0byAndW5zdHlsZWQnLiBSZXR1cm5zIG51bGwgaWZcbiAgICogYmxvY2sgb3Igc2VsZWN0aW9uIGRvZXMgbm90IG1lZXQgdGhhdCBjcml0ZXJpYS5cbiAgICovXG4gIHRyeVRvUmVtb3ZlQmxvY2tTdHlsZTogZnVuY3Rpb24gdHJ5VG9SZW1vdmVCbG9ja1N0eWxlKGVkaXRvclN0YXRlKSB7XG4gICAgdmFyIHNlbGVjdGlvbiA9IGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpO1xuICAgIHZhciBvZmZzZXQgPSBzZWxlY3Rpb24uZ2V0QW5jaG9yT2Zmc2V0KCk7XG4gICAgaWYgKHNlbGVjdGlvbi5pc0NvbGxhcHNlZCgpICYmIG9mZnNldCA9PT0gMCkge1xuICAgICAgdmFyIGtleSA9IHNlbGVjdGlvbi5nZXRBbmNob3JLZXkoKTtcbiAgICAgIHZhciBjb250ZW50ID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgICAgIHZhciBibG9jayA9IGNvbnRlbnQuZ2V0QmxvY2tGb3JLZXkoa2V5KTtcblxuICAgICAgdmFyIGZpcnN0QmxvY2sgPSBjb250ZW50LmdldEZpcnN0QmxvY2soKTtcbiAgICAgIGlmIChibG9jay5nZXRMZW5ndGgoKSA+IDAgJiYgYmxvY2sgIT09IGZpcnN0QmxvY2spIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG5cbiAgICAgIHZhciB0eXBlID0gYmxvY2suZ2V0VHlwZSgpO1xuICAgICAgdmFyIGJsb2NrQmVmb3JlID0gY29udGVudC5nZXRCbG9ja0JlZm9yZShrZXkpO1xuICAgICAgaWYgKHR5cGUgPT09ICdjb2RlLWJsb2NrJyAmJiBibG9ja0JlZm9yZSAmJiBibG9ja0JlZm9yZS5nZXRUeXBlKCkgPT09ICdjb2RlLWJsb2NrJyAmJiBibG9ja0JlZm9yZS5nZXRMZW5ndGgoKSAhPT0gMCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cblxuICAgICAgaWYgKHR5cGUgIT09ICd1bnN0eWxlZCcpIHtcbiAgICAgICAgcmV0dXJuIERyYWZ0TW9kaWZpZXIuc2V0QmxvY2tUeXBlKGNvbnRlbnQsIHNlbGVjdGlvbiwgJ3Vuc3R5bGVkJyk7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IFJpY2hUZXh0RWRpdG9yVXRpbDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQW5CQTtBQXFCQTtBQUVBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBNVJBO0FBK1JBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/RichTextEditorUtil.js
