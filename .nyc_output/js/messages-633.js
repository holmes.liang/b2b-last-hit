

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.newMessages = newMessages;

function newMessages() {
  return {
    'default': 'Validation error on field %s',
    required: '%s is required',
    'enum': '%s must be one of %s',
    whitespace: '%s cannot be empty',
    date: {
      format: '%s date %s is invalid for format %s',
      parse: '%s date could not be parsed, %s is invalid ',
      invalid: '%s date %s is invalid'
    },
    types: {
      string: '%s is not a %s',
      method: '%s is not a %s (function)',
      array: '%s is not an %s',
      object: '%s is not an %s',
      number: '%s is not a %s',
      date: '%s is not a %s',
      boolean: '%s is not a %s',
      integer: '%s is not an %s',
      float: '%s is not a %s',
      regexp: '%s is not a valid %s',
      email: '%s is not a valid %s',
      url: '%s is not a valid %s',
      hex: '%s is not a valid %s'
    },
    string: {
      len: '%s must be exactly %s characters',
      min: '%s must be at least %s characters',
      max: '%s cannot be longer than %s characters',
      range: '%s must be between %s and %s characters'
    },
    number: {
      len: '%s must equal %s',
      min: '%s cannot be less than %s',
      max: '%s cannot be greater than %s',
      range: '%s must be between %s and %s'
    },
    array: {
      len: '%s must be exactly %s in length',
      min: '%s cannot be less than %s in length',
      max: '%s cannot be greater than %s in length',
      range: '%s must be between %s and %s in length'
    },
    pattern: {
      mismatch: '%s value %s does not match pattern %s'
    },
    clone: function clone() {
      var cloned = JSON.parse(JSON.stringify(this));
      cloned.clone = this.clone;
      return cloned;
    }
  };
}

var messages = exports.messages = newMessages();//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYXN5bmMtdmFsaWRhdG9yL2VzL21lc3NhZ2VzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvYXN5bmMtdmFsaWRhdG9yL2VzL21lc3NhZ2VzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMubmV3TWVzc2FnZXMgPSBuZXdNZXNzYWdlcztcbmZ1bmN0aW9uIG5ld01lc3NhZ2VzKCkge1xuICByZXR1cm4ge1xuICAgICdkZWZhdWx0JzogJ1ZhbGlkYXRpb24gZXJyb3Igb24gZmllbGQgJXMnLFxuICAgIHJlcXVpcmVkOiAnJXMgaXMgcmVxdWlyZWQnLFxuICAgICdlbnVtJzogJyVzIG11c3QgYmUgb25lIG9mICVzJyxcbiAgICB3aGl0ZXNwYWNlOiAnJXMgY2Fubm90IGJlIGVtcHR5JyxcbiAgICBkYXRlOiB7XG4gICAgICBmb3JtYXQ6ICclcyBkYXRlICVzIGlzIGludmFsaWQgZm9yIGZvcm1hdCAlcycsXG4gICAgICBwYXJzZTogJyVzIGRhdGUgY291bGQgbm90IGJlIHBhcnNlZCwgJXMgaXMgaW52YWxpZCAnLFxuICAgICAgaW52YWxpZDogJyVzIGRhdGUgJXMgaXMgaW52YWxpZCdcbiAgICB9LFxuICAgIHR5cGVzOiB7XG4gICAgICBzdHJpbmc6ICclcyBpcyBub3QgYSAlcycsXG4gICAgICBtZXRob2Q6ICclcyBpcyBub3QgYSAlcyAoZnVuY3Rpb24pJyxcbiAgICAgIGFycmF5OiAnJXMgaXMgbm90IGFuICVzJyxcbiAgICAgIG9iamVjdDogJyVzIGlzIG5vdCBhbiAlcycsXG4gICAgICBudW1iZXI6ICclcyBpcyBub3QgYSAlcycsXG4gICAgICBkYXRlOiAnJXMgaXMgbm90IGEgJXMnLFxuICAgICAgYm9vbGVhbjogJyVzIGlzIG5vdCBhICVzJyxcbiAgICAgIGludGVnZXI6ICclcyBpcyBub3QgYW4gJXMnLFxuICAgICAgZmxvYXQ6ICclcyBpcyBub3QgYSAlcycsXG4gICAgICByZWdleHA6ICclcyBpcyBub3QgYSB2YWxpZCAlcycsXG4gICAgICBlbWFpbDogJyVzIGlzIG5vdCBhIHZhbGlkICVzJyxcbiAgICAgIHVybDogJyVzIGlzIG5vdCBhIHZhbGlkICVzJyxcbiAgICAgIGhleDogJyVzIGlzIG5vdCBhIHZhbGlkICVzJ1xuICAgIH0sXG4gICAgc3RyaW5nOiB7XG4gICAgICBsZW46ICclcyBtdXN0IGJlIGV4YWN0bHkgJXMgY2hhcmFjdGVycycsXG4gICAgICBtaW46ICclcyBtdXN0IGJlIGF0IGxlYXN0ICVzIGNoYXJhY3RlcnMnLFxuICAgICAgbWF4OiAnJXMgY2Fubm90IGJlIGxvbmdlciB0aGFuICVzIGNoYXJhY3RlcnMnLFxuICAgICAgcmFuZ2U6ICclcyBtdXN0IGJlIGJldHdlZW4gJXMgYW5kICVzIGNoYXJhY3RlcnMnXG4gICAgfSxcbiAgICBudW1iZXI6IHtcbiAgICAgIGxlbjogJyVzIG11c3QgZXF1YWwgJXMnLFxuICAgICAgbWluOiAnJXMgY2Fubm90IGJlIGxlc3MgdGhhbiAlcycsXG4gICAgICBtYXg6ICclcyBjYW5ub3QgYmUgZ3JlYXRlciB0aGFuICVzJyxcbiAgICAgIHJhbmdlOiAnJXMgbXVzdCBiZSBiZXR3ZWVuICVzIGFuZCAlcydcbiAgICB9LFxuICAgIGFycmF5OiB7XG4gICAgICBsZW46ICclcyBtdXN0IGJlIGV4YWN0bHkgJXMgaW4gbGVuZ3RoJyxcbiAgICAgIG1pbjogJyVzIGNhbm5vdCBiZSBsZXNzIHRoYW4gJXMgaW4gbGVuZ3RoJyxcbiAgICAgIG1heDogJyVzIGNhbm5vdCBiZSBncmVhdGVyIHRoYW4gJXMgaW4gbGVuZ3RoJyxcbiAgICAgIHJhbmdlOiAnJXMgbXVzdCBiZSBiZXR3ZWVuICVzIGFuZCAlcyBpbiBsZW5ndGgnXG4gICAgfSxcbiAgICBwYXR0ZXJuOiB7XG4gICAgICBtaXNtYXRjaDogJyVzIHZhbHVlICVzIGRvZXMgbm90IG1hdGNoIHBhdHRlcm4gJXMnXG4gICAgfSxcbiAgICBjbG9uZTogZnVuY3Rpb24gY2xvbmUoKSB7XG4gICAgICB2YXIgY2xvbmVkID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeSh0aGlzKSk7XG4gICAgICBjbG9uZWQuY2xvbmUgPSB0aGlzLmNsb25lO1xuICAgICAgcmV0dXJuIGNsb25lZDtcbiAgICB9XG4gIH07XG59XG5cbnZhciBtZXNzYWdlcyA9IGV4cG9ydHMubWVzc2FnZXMgPSBuZXdNZXNzYWdlcygpOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWxEQTtBQW9EQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/async-validator/es/messages.js
