var _event = __webpack_require__(/*! ../core/event */ "./node_modules/zrender/lib/core/event.js");

var addEventListener = _event.addEventListener;
var removeEventListener = _event.removeEventListener;
var normalizeEvent = _event.normalizeEvent;

var zrUtil = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var Eventful = __webpack_require__(/*! ../mixin/Eventful */ "./node_modules/zrender/lib/mixin/Eventful.js");

var env = __webpack_require__(/*! ../core/env */ "./node_modules/zrender/lib/core/env.js");

var TOUCH_CLICK_DELAY = 300;
var mouseHandlerNames = ['click', 'dblclick', 'mousewheel', 'mouseout', 'mouseup', 'mousedown', 'mousemove', 'contextmenu'];
var touchHandlerNames = ['touchstart', 'touchend', 'touchmove'];
var pointerEventNames = {
  pointerdown: 1,
  pointerup: 1,
  pointermove: 1,
  pointerout: 1
};
var pointerHandlerNames = zrUtil.map(mouseHandlerNames, function (name) {
  var nm = name.replace('mouse', 'pointer');
  return pointerEventNames[nm] ? nm : name;
});

function eventNameFix(name) {
  return name === 'mousewheel' && env.browser.firefox ? 'DOMMouseScroll' : name;
} // function onMSGestureChange(proxy, event) {
//     if (event.translationX || event.translationY) {
//         // mousemove is carried by MSGesture to reduce the sensitivity.
//         proxy.handler.dispatchToElement(event.target, 'mousemove', event);
//     }
//     if (event.scale !== 1) {
//         event.pinchX = event.offsetX;
//         event.pinchY = event.offsetY;
//         event.pinchScale = event.scale;
//         proxy.handler.dispatchToElement(event.target, 'pinch', event);
//     }
// }

/**
 * Prevent mouse event from being dispatched after Touch Events action
 * @see <https://github.com/deltakosh/handjs/blob/master/src/hand.base.js>
 * 1. Mobile browsers dispatch mouse events 300ms after touchend.
 * 2. Chrome for Android dispatch mousedown for long-touch about 650ms
 * Result: Blocking Mouse Events for 700ms.
 */


function setTouchTimer(instance) {
  instance._touching = true;
  clearTimeout(instance._touchTimer);
  instance._touchTimer = setTimeout(function () {
    instance._touching = false;
  }, 700);
}

var domHandlers = {
  /**
   * Mouse move handler
   * @inner
   * @param {Event} event
   */
  mousemove: function mousemove(event) {
    event = normalizeEvent(this.dom, event);
    this.trigger('mousemove', event);
  },

  /**
   * Mouse out handler
   * @inner
   * @param {Event} event
   */
  mouseout: function mouseout(event) {
    event = normalizeEvent(this.dom, event);
    var element = event.toElement || event.relatedTarget;

    if (element !== this.dom) {
      while (element && element.nodeType !== 9) {
        // 忽略包含在root中的dom引起的mouseOut
        if (element === this.dom) {
          return;
        }

        element = element.parentNode;
      }
    }

    this.trigger('mouseout', event);
  },

  /**
   * Touch开始响应函数
   * @inner
   * @param {Event} event
   */
  touchstart: function touchstart(event) {
    // Default mouse behaviour should not be disabled here.
    // For example, page may needs to be slided.
    event = normalizeEvent(this.dom, event); // Mark touch, which is useful in distinguish touch and
    // mouse event in upper applicatoin.

    event.zrByTouch = true;
    this._lastTouchMoment = new Date();
    this.handler.processGesture(this, event, 'start'); // In touch device, trigger `mousemove`(`mouseover`) should
    // be triggered, and must before `mousedown` triggered.

    domHandlers.mousemove.call(this, event);
    domHandlers.mousedown.call(this, event);
    setTouchTimer(this);
  },

  /**
   * Touch移动响应函数
   * @inner
   * @param {Event} event
   */
  touchmove: function touchmove(event) {
    event = normalizeEvent(this.dom, event); // Mark touch, which is useful in distinguish touch and
    // mouse event in upper applicatoin.

    event.zrByTouch = true;
    this.handler.processGesture(this, event, 'change'); // Mouse move should always be triggered no matter whether
    // there is gestrue event, because mouse move and pinch may
    // be used at the same time.

    domHandlers.mousemove.call(this, event);
    setTouchTimer(this);
  },

  /**
   * Touch结束响应函数
   * @inner
   * @param {Event} event
   */
  touchend: function touchend(event) {
    event = normalizeEvent(this.dom, event); // Mark touch, which is useful in distinguish touch and
    // mouse event in upper applicatoin.

    event.zrByTouch = true;
    this.handler.processGesture(this, event, 'end');
    domHandlers.mouseup.call(this, event); // Do not trigger `mouseout` here, in spite of `mousemove`(`mouseover`) is
    // triggered in `touchstart`. This seems to be illogical, but by this mechanism,
    // we can conveniently implement "hover style" in both PC and touch device just
    // by listening to `mouseover` to add "hover style" and listening to `mouseout`
    // to remove "hover style" on an element, without any additional code for
    // compatibility. (`mouseout` will not be triggered in `touchend`, so "hover
    // style" will remain for user view)
    // click event should always be triggered no matter whether
    // there is gestrue event. System click can not be prevented.

    if (+new Date() - this._lastTouchMoment < TOUCH_CLICK_DELAY) {
      domHandlers.click.call(this, event);
    }

    setTouchTimer(this);
  },
  pointerdown: function pointerdown(event) {
    domHandlers.mousedown.call(this, event); // if (useMSGuesture(this, event)) {
    //     this._msGesture.addPointer(event.pointerId);
    // }
  },
  pointermove: function pointermove(event) {
    // FIXME
    // pointermove is so sensitive that it always triggered when
    // tap(click) on touch screen, which affect some judgement in
    // upper application. So, we dont support mousemove on MS touch
    // device yet.
    if (!isPointerFromTouch(event)) {
      domHandlers.mousemove.call(this, event);
    }
  },
  pointerup: function pointerup(event) {
    domHandlers.mouseup.call(this, event);
  },
  pointerout: function pointerout(event) {
    // pointerout will be triggered when tap on touch screen
    // (IE11+/Edge on MS Surface) after click event triggered,
    // which is inconsistent with the mousout behavior we defined
    // in touchend. So we unify them.
    // (check domHandlers.touchend for detailed explanation)
    if (!isPointerFromTouch(event)) {
      domHandlers.mouseout.call(this, event);
    }
  }
};

function isPointerFromTouch(event) {
  var pointerType = event.pointerType;
  return pointerType === 'pen' || pointerType === 'touch';
} // function useMSGuesture(handlerProxy, event) {
//     return isPointerFromTouch(event) && !!handlerProxy._msGesture;
// }
// Common handlers


zrUtil.each(['click', 'mousedown', 'mouseup', 'mousewheel', 'dblclick', 'contextmenu'], function (name) {
  domHandlers[name] = function (event) {
    event = normalizeEvent(this.dom, event);
    this.trigger(name, event);
  };
});
/**
 * 为控制类实例初始化dom 事件处理函数
 *
 * @inner
 * @param {module:zrender/Handler} instance 控制类实例
 */

function initDomHandler(instance) {
  zrUtil.each(touchHandlerNames, function (name) {
    instance._handlers[name] = zrUtil.bind(domHandlers[name], instance);
  });
  zrUtil.each(pointerHandlerNames, function (name) {
    instance._handlers[name] = zrUtil.bind(domHandlers[name], instance);
  });
  zrUtil.each(mouseHandlerNames, function (name) {
    instance._handlers[name] = makeMouseHandler(domHandlers[name], instance);
  });

  function makeMouseHandler(fn, instance) {
    return function () {
      if (instance._touching) {
        return;
      }

      return fn.apply(instance, arguments);
    };
  }
}

function HandlerDomProxy(dom) {
  Eventful.call(this);
  this.dom = dom;
  /**
   * @private
   * @type {boolean}
   */

  this._touching = false;
  /**
   * @private
   * @type {number}
   */

  this._touchTimer;
  this._handlers = {};
  initDomHandler(this);

  if (env.pointerEventsSupported) {
    // Only IE11+/Edge
    // 1. On devices that both enable touch and mouse (e.g., MS Surface and lenovo X240),
    // IE11+/Edge do not trigger touch event, but trigger pointer event and mouse event
    // at the same time.
    // 2. On MS Surface, it probablely only trigger mousedown but no mouseup when tap on
    // screen, which do not occurs in pointer event.
    // So we use pointer event to both detect touch gesture and mouse behavior.
    mountHandlers(pointerHandlerNames, this); // FIXME
    // Note: MS Gesture require CSS touch-action set. But touch-action is not reliable,
    // which does not prevent defuault behavior occasionally (which may cause view port
    // zoomed in but use can not zoom it back). And event.preventDefault() does not work.
    // So we have to not to use MSGesture and not to support touchmove and pinch on MS
    // touch screen. And we only support click behavior on MS touch screen now.
    // MS Gesture Event is only supported on IE11+/Edge and on Windows 8+.
    // We dont support touch on IE on win7.
    // See <https://msdn.microsoft.com/en-us/library/dn433243(v=vs.85).aspx>
    // if (typeof MSGesture === 'function') {
    //     (this._msGesture = new MSGesture()).target = dom; // jshint ignore:line
    //     dom.addEventListener('MSGestureChange', onMSGestureChange);
    // }
  } else {
    if (env.touchEventsSupported) {
      mountHandlers(touchHandlerNames, this); // Handler of 'mouseout' event is needed in touch mode, which will be mounted below.
      // addEventListener(root, 'mouseout', this._mouseoutHandler);
    } // 1. Considering some devices that both enable touch and mouse event (like on MS Surface
    // and lenovo X240, @see #2350), we make mouse event be always listened, otherwise
    // mouse event can not be handle in those devices.
    // 2. On MS Surface, Chrome will trigger both touch event and mouse event. How to prevent
    // mouseevent after touch event triggered, see `setTouchTimer`.


    mountHandlers(mouseHandlerNames, this);
  }

  function mountHandlers(handlerNames, instance) {
    zrUtil.each(handlerNames, function (name) {
      addEventListener(dom, eventNameFix(name), instance._handlers[name]);
    }, instance);
  }
}

var handlerDomProxyProto = HandlerDomProxy.prototype;

handlerDomProxyProto.dispose = function () {
  var handlerNames = mouseHandlerNames.concat(touchHandlerNames);

  for (var i = 0; i < handlerNames.length; i++) {
    var name = handlerNames[i];
    removeEventListener(this.dom, eventNameFix(name), this._handlers[name]);
  }
};

handlerDomProxyProto.setCursor = function (cursorStyle) {
  this.dom.style && (this.dom.style.cursor = cursorStyle || 'default');
};

zrUtil.mixin(HandlerDomProxy, Eventful);
var _default = HandlerDomProxy;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZG9tL0hhbmRsZXJQcm94eS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2RvbS9IYW5kbGVyUHJveHkuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9ldmVudCA9IHJlcXVpcmUoXCIuLi9jb3JlL2V2ZW50XCIpO1xuXG52YXIgYWRkRXZlbnRMaXN0ZW5lciA9IF9ldmVudC5hZGRFdmVudExpc3RlbmVyO1xudmFyIHJlbW92ZUV2ZW50TGlzdGVuZXIgPSBfZXZlbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcjtcbnZhciBub3JtYWxpemVFdmVudCA9IF9ldmVudC5ub3JtYWxpemVFdmVudDtcblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCIuLi9jb3JlL3V0aWxcIik7XG5cbnZhciBFdmVudGZ1bCA9IHJlcXVpcmUoXCIuLi9taXhpbi9FdmVudGZ1bFwiKTtcblxudmFyIGVudiA9IHJlcXVpcmUoXCIuLi9jb3JlL2VudlwiKTtcblxudmFyIFRPVUNIX0NMSUNLX0RFTEFZID0gMzAwO1xudmFyIG1vdXNlSGFuZGxlck5hbWVzID0gWydjbGljaycsICdkYmxjbGljaycsICdtb3VzZXdoZWVsJywgJ21vdXNlb3V0JywgJ21vdXNldXAnLCAnbW91c2Vkb3duJywgJ21vdXNlbW92ZScsICdjb250ZXh0bWVudSddO1xudmFyIHRvdWNoSGFuZGxlck5hbWVzID0gWyd0b3VjaHN0YXJ0JywgJ3RvdWNoZW5kJywgJ3RvdWNobW92ZSddO1xudmFyIHBvaW50ZXJFdmVudE5hbWVzID0ge1xuICBwb2ludGVyZG93bjogMSxcbiAgcG9pbnRlcnVwOiAxLFxuICBwb2ludGVybW92ZTogMSxcbiAgcG9pbnRlcm91dDogMVxufTtcbnZhciBwb2ludGVySGFuZGxlck5hbWVzID0genJVdGlsLm1hcChtb3VzZUhhbmRsZXJOYW1lcywgZnVuY3Rpb24gKG5hbWUpIHtcbiAgdmFyIG5tID0gbmFtZS5yZXBsYWNlKCdtb3VzZScsICdwb2ludGVyJyk7XG4gIHJldHVybiBwb2ludGVyRXZlbnROYW1lc1tubV0gPyBubSA6IG5hbWU7XG59KTtcblxuZnVuY3Rpb24gZXZlbnROYW1lRml4KG5hbWUpIHtcbiAgcmV0dXJuIG5hbWUgPT09ICdtb3VzZXdoZWVsJyAmJiBlbnYuYnJvd3Nlci5maXJlZm94ID8gJ0RPTU1vdXNlU2Nyb2xsJyA6IG5hbWU7XG59IC8vIGZ1bmN0aW9uIG9uTVNHZXN0dXJlQ2hhbmdlKHByb3h5LCBldmVudCkge1xuLy8gICAgIGlmIChldmVudC50cmFuc2xhdGlvblggfHwgZXZlbnQudHJhbnNsYXRpb25ZKSB7XG4vLyAgICAgICAgIC8vIG1vdXNlbW92ZSBpcyBjYXJyaWVkIGJ5IE1TR2VzdHVyZSB0byByZWR1Y2UgdGhlIHNlbnNpdGl2aXR5LlxuLy8gICAgICAgICBwcm94eS5oYW5kbGVyLmRpc3BhdGNoVG9FbGVtZW50KGV2ZW50LnRhcmdldCwgJ21vdXNlbW92ZScsIGV2ZW50KTtcbi8vICAgICB9XG4vLyAgICAgaWYgKGV2ZW50LnNjYWxlICE9PSAxKSB7XG4vLyAgICAgICAgIGV2ZW50LnBpbmNoWCA9IGV2ZW50Lm9mZnNldFg7XG4vLyAgICAgICAgIGV2ZW50LnBpbmNoWSA9IGV2ZW50Lm9mZnNldFk7XG4vLyAgICAgICAgIGV2ZW50LnBpbmNoU2NhbGUgPSBldmVudC5zY2FsZTtcbi8vICAgICAgICAgcHJveHkuaGFuZGxlci5kaXNwYXRjaFRvRWxlbWVudChldmVudC50YXJnZXQsICdwaW5jaCcsIGV2ZW50KTtcbi8vICAgICB9XG4vLyB9XG5cbi8qKlxuICogUHJldmVudCBtb3VzZSBldmVudCBmcm9tIGJlaW5nIGRpc3BhdGNoZWQgYWZ0ZXIgVG91Y2ggRXZlbnRzIGFjdGlvblxuICogQHNlZSA8aHR0cHM6Ly9naXRodWIuY29tL2RlbHRha29zaC9oYW5kanMvYmxvYi9tYXN0ZXIvc3JjL2hhbmQuYmFzZS5qcz5cbiAqIDEuIE1vYmlsZSBicm93c2VycyBkaXNwYXRjaCBtb3VzZSBldmVudHMgMzAwbXMgYWZ0ZXIgdG91Y2hlbmQuXG4gKiAyLiBDaHJvbWUgZm9yIEFuZHJvaWQgZGlzcGF0Y2ggbW91c2Vkb3duIGZvciBsb25nLXRvdWNoIGFib3V0IDY1MG1zXG4gKiBSZXN1bHQ6IEJsb2NraW5nIE1vdXNlIEV2ZW50cyBmb3IgNzAwbXMuXG4gKi9cblxuXG5mdW5jdGlvbiBzZXRUb3VjaFRpbWVyKGluc3RhbmNlKSB7XG4gIGluc3RhbmNlLl90b3VjaGluZyA9IHRydWU7XG4gIGNsZWFyVGltZW91dChpbnN0YW5jZS5fdG91Y2hUaW1lcik7XG4gIGluc3RhbmNlLl90b3VjaFRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgaW5zdGFuY2UuX3RvdWNoaW5nID0gZmFsc2U7XG4gIH0sIDcwMCk7XG59XG5cbnZhciBkb21IYW5kbGVycyA9IHtcbiAgLyoqXG4gICAqIE1vdXNlIG1vdmUgaGFuZGxlclxuICAgKiBAaW5uZXJcbiAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICovXG4gIG1vdXNlbW92ZTogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgZXZlbnQgPSBub3JtYWxpemVFdmVudCh0aGlzLmRvbSwgZXZlbnQpO1xuICAgIHRoaXMudHJpZ2dlcignbW91c2Vtb3ZlJywgZXZlbnQpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBNb3VzZSBvdXQgaGFuZGxlclxuICAgKiBAaW5uZXJcbiAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICovXG4gIG1vdXNlb3V0OiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBldmVudCA9IG5vcm1hbGl6ZUV2ZW50KHRoaXMuZG9tLCBldmVudCk7XG4gICAgdmFyIGVsZW1lbnQgPSBldmVudC50b0VsZW1lbnQgfHwgZXZlbnQucmVsYXRlZFRhcmdldDtcblxuICAgIGlmIChlbGVtZW50ICE9PSB0aGlzLmRvbSkge1xuICAgICAgd2hpbGUgKGVsZW1lbnQgJiYgZWxlbWVudC5ub2RlVHlwZSAhPT0gOSkge1xuICAgICAgICAvLyDlv73nlaXljIXlkKvlnKhyb2905Lit55qEZG9t5byV6LW355qEbW91c2VPdXRcbiAgICAgICAgaWYgKGVsZW1lbnQgPT09IHRoaXMuZG9tKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgZWxlbWVudCA9IGVsZW1lbnQucGFyZW50Tm9kZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLnRyaWdnZXIoJ21vdXNlb3V0JywgZXZlbnQpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBUb3VjaOW8gOWni+WTjeW6lOWHveaVsFxuICAgKiBAaW5uZXJcbiAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICovXG4gIHRvdWNoc3RhcnQ6IGZ1bmN0aW9uIChldmVudCkge1xuICAgIC8vIERlZmF1bHQgbW91c2UgYmVoYXZpb3VyIHNob3VsZCBub3QgYmUgZGlzYWJsZWQgaGVyZS5cbiAgICAvLyBGb3IgZXhhbXBsZSwgcGFnZSBtYXkgbmVlZHMgdG8gYmUgc2xpZGVkLlxuICAgIGV2ZW50ID0gbm9ybWFsaXplRXZlbnQodGhpcy5kb20sIGV2ZW50KTsgLy8gTWFyayB0b3VjaCwgd2hpY2ggaXMgdXNlZnVsIGluIGRpc3Rpbmd1aXNoIHRvdWNoIGFuZFxuICAgIC8vIG1vdXNlIGV2ZW50IGluIHVwcGVyIGFwcGxpY2F0b2luLlxuXG4gICAgZXZlbnQuenJCeVRvdWNoID0gdHJ1ZTtcbiAgICB0aGlzLl9sYXN0VG91Y2hNb21lbnQgPSBuZXcgRGF0ZSgpO1xuICAgIHRoaXMuaGFuZGxlci5wcm9jZXNzR2VzdHVyZSh0aGlzLCBldmVudCwgJ3N0YXJ0Jyk7IC8vIEluIHRvdWNoIGRldmljZSwgdHJpZ2dlciBgbW91c2Vtb3ZlYChgbW91c2VvdmVyYCkgc2hvdWxkXG4gICAgLy8gYmUgdHJpZ2dlcmVkLCBhbmQgbXVzdCBiZWZvcmUgYG1vdXNlZG93bmAgdHJpZ2dlcmVkLlxuXG4gICAgZG9tSGFuZGxlcnMubW91c2Vtb3ZlLmNhbGwodGhpcywgZXZlbnQpO1xuICAgIGRvbUhhbmRsZXJzLm1vdXNlZG93bi5jYWxsKHRoaXMsIGV2ZW50KTtcbiAgICBzZXRUb3VjaFRpbWVyKHRoaXMpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBUb3VjaOenu+WKqOWTjeW6lOWHveaVsFxuICAgKiBAaW5uZXJcbiAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICovXG4gIHRvdWNobW92ZTogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgZXZlbnQgPSBub3JtYWxpemVFdmVudCh0aGlzLmRvbSwgZXZlbnQpOyAvLyBNYXJrIHRvdWNoLCB3aGljaCBpcyB1c2VmdWwgaW4gZGlzdGluZ3Vpc2ggdG91Y2ggYW5kXG4gICAgLy8gbW91c2UgZXZlbnQgaW4gdXBwZXIgYXBwbGljYXRvaW4uXG5cbiAgICBldmVudC56ckJ5VG91Y2ggPSB0cnVlO1xuICAgIHRoaXMuaGFuZGxlci5wcm9jZXNzR2VzdHVyZSh0aGlzLCBldmVudCwgJ2NoYW5nZScpOyAvLyBNb3VzZSBtb3ZlIHNob3VsZCBhbHdheXMgYmUgdHJpZ2dlcmVkIG5vIG1hdHRlciB3aGV0aGVyXG4gICAgLy8gdGhlcmUgaXMgZ2VzdHJ1ZSBldmVudCwgYmVjYXVzZSBtb3VzZSBtb3ZlIGFuZCBwaW5jaCBtYXlcbiAgICAvLyBiZSB1c2VkIGF0IHRoZSBzYW1lIHRpbWUuXG5cbiAgICBkb21IYW5kbGVycy5tb3VzZW1vdmUuY2FsbCh0aGlzLCBldmVudCk7XG4gICAgc2V0VG91Y2hUaW1lcih0aGlzKTtcbiAgfSxcblxuICAvKipcbiAgICogVG91Y2jnu5PmnZ/lk43lupTlh73mlbBcbiAgICogQGlubmVyXG4gICAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50XG4gICAqL1xuICB0b3VjaGVuZDogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgZXZlbnQgPSBub3JtYWxpemVFdmVudCh0aGlzLmRvbSwgZXZlbnQpOyAvLyBNYXJrIHRvdWNoLCB3aGljaCBpcyB1c2VmdWwgaW4gZGlzdGluZ3Vpc2ggdG91Y2ggYW5kXG4gICAgLy8gbW91c2UgZXZlbnQgaW4gdXBwZXIgYXBwbGljYXRvaW4uXG5cbiAgICBldmVudC56ckJ5VG91Y2ggPSB0cnVlO1xuICAgIHRoaXMuaGFuZGxlci5wcm9jZXNzR2VzdHVyZSh0aGlzLCBldmVudCwgJ2VuZCcpO1xuICAgIGRvbUhhbmRsZXJzLm1vdXNldXAuY2FsbCh0aGlzLCBldmVudCk7IC8vIERvIG5vdCB0cmlnZ2VyIGBtb3VzZW91dGAgaGVyZSwgaW4gc3BpdGUgb2YgYG1vdXNlbW92ZWAoYG1vdXNlb3ZlcmApIGlzXG4gICAgLy8gdHJpZ2dlcmVkIGluIGB0b3VjaHN0YXJ0YC4gVGhpcyBzZWVtcyB0byBiZSBpbGxvZ2ljYWwsIGJ1dCBieSB0aGlzIG1lY2hhbmlzbSxcbiAgICAvLyB3ZSBjYW4gY29udmVuaWVudGx5IGltcGxlbWVudCBcImhvdmVyIHN0eWxlXCIgaW4gYm90aCBQQyBhbmQgdG91Y2ggZGV2aWNlIGp1c3RcbiAgICAvLyBieSBsaXN0ZW5pbmcgdG8gYG1vdXNlb3ZlcmAgdG8gYWRkIFwiaG92ZXIgc3R5bGVcIiBhbmQgbGlzdGVuaW5nIHRvIGBtb3VzZW91dGBcbiAgICAvLyB0byByZW1vdmUgXCJob3ZlciBzdHlsZVwiIG9uIGFuIGVsZW1lbnQsIHdpdGhvdXQgYW55IGFkZGl0aW9uYWwgY29kZSBmb3JcbiAgICAvLyBjb21wYXRpYmlsaXR5LiAoYG1vdXNlb3V0YCB3aWxsIG5vdCBiZSB0cmlnZ2VyZWQgaW4gYHRvdWNoZW5kYCwgc28gXCJob3ZlclxuICAgIC8vIHN0eWxlXCIgd2lsbCByZW1haW4gZm9yIHVzZXIgdmlldylcbiAgICAvLyBjbGljayBldmVudCBzaG91bGQgYWx3YXlzIGJlIHRyaWdnZXJlZCBubyBtYXR0ZXIgd2hldGhlclxuICAgIC8vIHRoZXJlIGlzIGdlc3RydWUgZXZlbnQuIFN5c3RlbSBjbGljayBjYW4gbm90IGJlIHByZXZlbnRlZC5cblxuICAgIGlmICgrbmV3IERhdGUoKSAtIHRoaXMuX2xhc3RUb3VjaE1vbWVudCA8IFRPVUNIX0NMSUNLX0RFTEFZKSB7XG4gICAgICBkb21IYW5kbGVycy5jbGljay5jYWxsKHRoaXMsIGV2ZW50KTtcbiAgICB9XG5cbiAgICBzZXRUb3VjaFRpbWVyKHRoaXMpO1xuICB9LFxuICBwb2ludGVyZG93bjogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgZG9tSGFuZGxlcnMubW91c2Vkb3duLmNhbGwodGhpcywgZXZlbnQpOyAvLyBpZiAodXNlTVNHdWVzdHVyZSh0aGlzLCBldmVudCkpIHtcbiAgICAvLyAgICAgdGhpcy5fbXNHZXN0dXJlLmFkZFBvaW50ZXIoZXZlbnQucG9pbnRlcklkKTtcbiAgICAvLyB9XG4gIH0sXG4gIHBvaW50ZXJtb3ZlOiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAvLyBGSVhNRVxuICAgIC8vIHBvaW50ZXJtb3ZlIGlzIHNvIHNlbnNpdGl2ZSB0aGF0IGl0IGFsd2F5cyB0cmlnZ2VyZWQgd2hlblxuICAgIC8vIHRhcChjbGljaykgb24gdG91Y2ggc2NyZWVuLCB3aGljaCBhZmZlY3Qgc29tZSBqdWRnZW1lbnQgaW5cbiAgICAvLyB1cHBlciBhcHBsaWNhdGlvbi4gU28sIHdlIGRvbnQgc3VwcG9ydCBtb3VzZW1vdmUgb24gTVMgdG91Y2hcbiAgICAvLyBkZXZpY2UgeWV0LlxuICAgIGlmICghaXNQb2ludGVyRnJvbVRvdWNoKGV2ZW50KSkge1xuICAgICAgZG9tSGFuZGxlcnMubW91c2Vtb3ZlLmNhbGwodGhpcywgZXZlbnQpO1xuICAgIH1cbiAgfSxcbiAgcG9pbnRlcnVwOiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBkb21IYW5kbGVycy5tb3VzZXVwLmNhbGwodGhpcywgZXZlbnQpO1xuICB9LFxuICBwb2ludGVyb3V0OiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAvLyBwb2ludGVyb3V0IHdpbGwgYmUgdHJpZ2dlcmVkIHdoZW4gdGFwIG9uIHRvdWNoIHNjcmVlblxuICAgIC8vIChJRTExKy9FZGdlIG9uIE1TIFN1cmZhY2UpIGFmdGVyIGNsaWNrIGV2ZW50IHRyaWdnZXJlZCxcbiAgICAvLyB3aGljaCBpcyBpbmNvbnNpc3RlbnQgd2l0aCB0aGUgbW91c291dCBiZWhhdmlvciB3ZSBkZWZpbmVkXG4gICAgLy8gaW4gdG91Y2hlbmQuIFNvIHdlIHVuaWZ5IHRoZW0uXG4gICAgLy8gKGNoZWNrIGRvbUhhbmRsZXJzLnRvdWNoZW5kIGZvciBkZXRhaWxlZCBleHBsYW5hdGlvbilcbiAgICBpZiAoIWlzUG9pbnRlckZyb21Ub3VjaChldmVudCkpIHtcbiAgICAgIGRvbUhhbmRsZXJzLm1vdXNlb3V0LmNhbGwodGhpcywgZXZlbnQpO1xuICAgIH1cbiAgfVxufTtcblxuZnVuY3Rpb24gaXNQb2ludGVyRnJvbVRvdWNoKGV2ZW50KSB7XG4gIHZhciBwb2ludGVyVHlwZSA9IGV2ZW50LnBvaW50ZXJUeXBlO1xuICByZXR1cm4gcG9pbnRlclR5cGUgPT09ICdwZW4nIHx8IHBvaW50ZXJUeXBlID09PSAndG91Y2gnO1xufSAvLyBmdW5jdGlvbiB1c2VNU0d1ZXN0dXJlKGhhbmRsZXJQcm94eSwgZXZlbnQpIHtcbi8vICAgICByZXR1cm4gaXNQb2ludGVyRnJvbVRvdWNoKGV2ZW50KSAmJiAhIWhhbmRsZXJQcm94eS5fbXNHZXN0dXJlO1xuLy8gfVxuLy8gQ29tbW9uIGhhbmRsZXJzXG5cblxuenJVdGlsLmVhY2goWydjbGljaycsICdtb3VzZWRvd24nLCAnbW91c2V1cCcsICdtb3VzZXdoZWVsJywgJ2RibGNsaWNrJywgJ2NvbnRleHRtZW51J10sIGZ1bmN0aW9uIChuYW1lKSB7XG4gIGRvbUhhbmRsZXJzW25hbWVdID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgZXZlbnQgPSBub3JtYWxpemVFdmVudCh0aGlzLmRvbSwgZXZlbnQpO1xuICAgIHRoaXMudHJpZ2dlcihuYW1lLCBldmVudCk7XG4gIH07XG59KTtcbi8qKlxuICog5Li65o6n5Yi257G75a6e5L6L5Yid5aeL5YyWZG9tIOS6i+S7tuWkhOeQhuWHveaVsFxuICpcbiAqIEBpbm5lclxuICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9IYW5kbGVyfSBpbnN0YW5jZSDmjqfliLbnsbvlrp7kvotcbiAqL1xuXG5mdW5jdGlvbiBpbml0RG9tSGFuZGxlcihpbnN0YW5jZSkge1xuICB6clV0aWwuZWFjaCh0b3VjaEhhbmRsZXJOYW1lcywgZnVuY3Rpb24gKG5hbWUpIHtcbiAgICBpbnN0YW5jZS5faGFuZGxlcnNbbmFtZV0gPSB6clV0aWwuYmluZChkb21IYW5kbGVyc1tuYW1lXSwgaW5zdGFuY2UpO1xuICB9KTtcbiAgenJVdGlsLmVhY2gocG9pbnRlckhhbmRsZXJOYW1lcywgZnVuY3Rpb24gKG5hbWUpIHtcbiAgICBpbnN0YW5jZS5faGFuZGxlcnNbbmFtZV0gPSB6clV0aWwuYmluZChkb21IYW5kbGVyc1tuYW1lXSwgaW5zdGFuY2UpO1xuICB9KTtcbiAgenJVdGlsLmVhY2gobW91c2VIYW5kbGVyTmFtZXMsIGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgaW5zdGFuY2UuX2hhbmRsZXJzW25hbWVdID0gbWFrZU1vdXNlSGFuZGxlcihkb21IYW5kbGVyc1tuYW1lXSwgaW5zdGFuY2UpO1xuICB9KTtcblxuICBmdW5jdGlvbiBtYWtlTW91c2VIYW5kbGVyKGZuLCBpbnN0YW5jZSkge1xuICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoaW5zdGFuY2UuX3RvdWNoaW5nKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGZuLmFwcGx5KGluc3RhbmNlLCBhcmd1bWVudHMpO1xuICAgIH07XG4gIH1cbn1cblxuZnVuY3Rpb24gSGFuZGxlckRvbVByb3h5KGRvbSkge1xuICBFdmVudGZ1bC5jYWxsKHRoaXMpO1xuICB0aGlzLmRvbSA9IGRvbTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKi9cblxuICB0aGlzLl90b3VjaGluZyA9IGZhbHNlO1xuICAvKipcbiAgICogQHByaXZhdGVcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG5cbiAgdGhpcy5fdG91Y2hUaW1lcjtcbiAgdGhpcy5faGFuZGxlcnMgPSB7fTtcbiAgaW5pdERvbUhhbmRsZXIodGhpcyk7XG5cbiAgaWYgKGVudi5wb2ludGVyRXZlbnRzU3VwcG9ydGVkKSB7XG4gICAgLy8gT25seSBJRTExKy9FZGdlXG4gICAgLy8gMS4gT24gZGV2aWNlcyB0aGF0IGJvdGggZW5hYmxlIHRvdWNoIGFuZCBtb3VzZSAoZS5nLiwgTVMgU3VyZmFjZSBhbmQgbGVub3ZvIFgyNDApLFxuICAgIC8vIElFMTErL0VkZ2UgZG8gbm90IHRyaWdnZXIgdG91Y2ggZXZlbnQsIGJ1dCB0cmlnZ2VyIHBvaW50ZXIgZXZlbnQgYW5kIG1vdXNlIGV2ZW50XG4gICAgLy8gYXQgdGhlIHNhbWUgdGltZS5cbiAgICAvLyAyLiBPbiBNUyBTdXJmYWNlLCBpdCBwcm9iYWJsZWx5IG9ubHkgdHJpZ2dlciBtb3VzZWRvd24gYnV0IG5vIG1vdXNldXAgd2hlbiB0YXAgb25cbiAgICAvLyBzY3JlZW4sIHdoaWNoIGRvIG5vdCBvY2N1cnMgaW4gcG9pbnRlciBldmVudC5cbiAgICAvLyBTbyB3ZSB1c2UgcG9pbnRlciBldmVudCB0byBib3RoIGRldGVjdCB0b3VjaCBnZXN0dXJlIGFuZCBtb3VzZSBiZWhhdmlvci5cbiAgICBtb3VudEhhbmRsZXJzKHBvaW50ZXJIYW5kbGVyTmFtZXMsIHRoaXMpOyAvLyBGSVhNRVxuICAgIC8vIE5vdGU6IE1TIEdlc3R1cmUgcmVxdWlyZSBDU1MgdG91Y2gtYWN0aW9uIHNldC4gQnV0IHRvdWNoLWFjdGlvbiBpcyBub3QgcmVsaWFibGUsXG4gICAgLy8gd2hpY2ggZG9lcyBub3QgcHJldmVudCBkZWZ1YXVsdCBiZWhhdmlvciBvY2Nhc2lvbmFsbHkgKHdoaWNoIG1heSBjYXVzZSB2aWV3IHBvcnRcbiAgICAvLyB6b29tZWQgaW4gYnV0IHVzZSBjYW4gbm90IHpvb20gaXQgYmFjaykuIEFuZCBldmVudC5wcmV2ZW50RGVmYXVsdCgpIGRvZXMgbm90IHdvcmsuXG4gICAgLy8gU28gd2UgaGF2ZSB0byBub3QgdG8gdXNlIE1TR2VzdHVyZSBhbmQgbm90IHRvIHN1cHBvcnQgdG91Y2htb3ZlIGFuZCBwaW5jaCBvbiBNU1xuICAgIC8vIHRvdWNoIHNjcmVlbi4gQW5kIHdlIG9ubHkgc3VwcG9ydCBjbGljayBiZWhhdmlvciBvbiBNUyB0b3VjaCBzY3JlZW4gbm93LlxuICAgIC8vIE1TIEdlc3R1cmUgRXZlbnQgaXMgb25seSBzdXBwb3J0ZWQgb24gSUUxMSsvRWRnZSBhbmQgb24gV2luZG93cyA4Ky5cbiAgICAvLyBXZSBkb250IHN1cHBvcnQgdG91Y2ggb24gSUUgb24gd2luNy5cbiAgICAvLyBTZWUgPGh0dHBzOi8vbXNkbi5taWNyb3NvZnQuY29tL2VuLXVzL2xpYnJhcnkvZG40MzMyNDModj12cy44NSkuYXNweD5cbiAgICAvLyBpZiAodHlwZW9mIE1TR2VzdHVyZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIC8vICAgICAodGhpcy5fbXNHZXN0dXJlID0gbmV3IE1TR2VzdHVyZSgpKS50YXJnZXQgPSBkb207IC8vIGpzaGludCBpZ25vcmU6bGluZVxuICAgIC8vICAgICBkb20uYWRkRXZlbnRMaXN0ZW5lcignTVNHZXN0dXJlQ2hhbmdlJywgb25NU0dlc3R1cmVDaGFuZ2UpO1xuICAgIC8vIH1cbiAgfSBlbHNlIHtcbiAgICBpZiAoZW52LnRvdWNoRXZlbnRzU3VwcG9ydGVkKSB7XG4gICAgICBtb3VudEhhbmRsZXJzKHRvdWNoSGFuZGxlck5hbWVzLCB0aGlzKTsgLy8gSGFuZGxlciBvZiAnbW91c2VvdXQnIGV2ZW50IGlzIG5lZWRlZCBpbiB0b3VjaCBtb2RlLCB3aGljaCB3aWxsIGJlIG1vdW50ZWQgYmVsb3cuXG4gICAgICAvLyBhZGRFdmVudExpc3RlbmVyKHJvb3QsICdtb3VzZW91dCcsIHRoaXMuX21vdXNlb3V0SGFuZGxlcik7XG4gICAgfSAvLyAxLiBDb25zaWRlcmluZyBzb21lIGRldmljZXMgdGhhdCBib3RoIGVuYWJsZSB0b3VjaCBhbmQgbW91c2UgZXZlbnQgKGxpa2Ugb24gTVMgU3VyZmFjZVxuICAgIC8vIGFuZCBsZW5vdm8gWDI0MCwgQHNlZSAjMjM1MCksIHdlIG1ha2UgbW91c2UgZXZlbnQgYmUgYWx3YXlzIGxpc3RlbmVkLCBvdGhlcndpc2VcbiAgICAvLyBtb3VzZSBldmVudCBjYW4gbm90IGJlIGhhbmRsZSBpbiB0aG9zZSBkZXZpY2VzLlxuICAgIC8vIDIuIE9uIE1TIFN1cmZhY2UsIENocm9tZSB3aWxsIHRyaWdnZXIgYm90aCB0b3VjaCBldmVudCBhbmQgbW91c2UgZXZlbnQuIEhvdyB0byBwcmV2ZW50XG4gICAgLy8gbW91c2VldmVudCBhZnRlciB0b3VjaCBldmVudCB0cmlnZ2VyZWQsIHNlZSBgc2V0VG91Y2hUaW1lcmAuXG5cblxuICAgIG1vdW50SGFuZGxlcnMobW91c2VIYW5kbGVyTmFtZXMsIHRoaXMpO1xuICB9XG5cbiAgZnVuY3Rpb24gbW91bnRIYW5kbGVycyhoYW5kbGVyTmFtZXMsIGluc3RhbmNlKSB7XG4gICAgenJVdGlsLmVhY2goaGFuZGxlck5hbWVzLCBmdW5jdGlvbiAobmFtZSkge1xuICAgICAgYWRkRXZlbnRMaXN0ZW5lcihkb20sIGV2ZW50TmFtZUZpeChuYW1lKSwgaW5zdGFuY2UuX2hhbmRsZXJzW25hbWVdKTtcbiAgICB9LCBpbnN0YW5jZSk7XG4gIH1cbn1cblxudmFyIGhhbmRsZXJEb21Qcm94eVByb3RvID0gSGFuZGxlckRvbVByb3h5LnByb3RvdHlwZTtcblxuaGFuZGxlckRvbVByb3h5UHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhhbmRsZXJOYW1lcyA9IG1vdXNlSGFuZGxlck5hbWVzLmNvbmNhdCh0b3VjaEhhbmRsZXJOYW1lcyk7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBoYW5kbGVyTmFtZXMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgbmFtZSA9IGhhbmRsZXJOYW1lc1tpXTtcbiAgICByZW1vdmVFdmVudExpc3RlbmVyKHRoaXMuZG9tLCBldmVudE5hbWVGaXgobmFtZSksIHRoaXMuX2hhbmRsZXJzW25hbWVdKTtcbiAgfVxufTtcblxuaGFuZGxlckRvbVByb3h5UHJvdG8uc2V0Q3Vyc29yID0gZnVuY3Rpb24gKGN1cnNvclN0eWxlKSB7XG4gIHRoaXMuZG9tLnN0eWxlICYmICh0aGlzLmRvbS5zdHlsZS5jdXJzb3IgPSBjdXJzb3JTdHlsZSB8fCAnZGVmYXVsdCcpO1xufTtcblxuenJVdGlsLm1peGluKEhhbmRsZXJEb21Qcm94eSwgRXZlbnRmdWwpO1xudmFyIF9kZWZhdWx0ID0gSGFuZGxlckRvbVByb3h5O1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBL0hBO0FBQ0E7QUFpSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/dom/HandlerProxy.js
