__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _CheckableTag__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./CheckableTag */ "./node_modules/antd/es/tag/CheckableTag.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_colors__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_util/colors */ "./node_modules/antd/es/_util/colors.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
/* harmony import */ var _util_wave__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../_util/wave */ "./node_modules/antd/es/_util/wave.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};











var PresetColorRegex = new RegExp("^(".concat(_util_colors__WEBPACK_IMPORTED_MODULE_7__["PresetColorTypes"].join('|'), ")(-inverse)?$"));

var Tag =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Tag, _React$Component);

  function Tag(props) {
    var _this;

    _classCallCheck(this, Tag);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Tag).call(this, props));
    _this.state = {
      visible: true
    };

    _this.handleIconClick = function (e) {
      e.stopPropagation();

      _this.setVisible(false, e);
    };

    _this.renderTag = function (configProps) {
      var _a = _this.props,
          children = _a.children,
          otherProps = __rest(_a, ["children"]);

      var isNeedWave = 'onClick' in otherProps || children && children.type === 'a';
      var tagProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_2__["default"])(otherProps, ['onClose', 'afterClose', 'color', 'visible', 'closable', 'prefixCls']);
      return isNeedWave ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_util_wave__WEBPACK_IMPORTED_MODULE_9__["default"], null, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", _extends({}, tagProps, {
        className: _this.getTagClassName(configProps),
        style: _this.getTagStyle()
      }), children, _this.renderCloseIcon())) : react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", _extends({}, tagProps, {
        className: _this.getTagClassName(configProps),
        style: _this.getTagStyle()
      }), children, _this.renderCloseIcon());
    };

    Object(_util_warning__WEBPACK_IMPORTED_MODULE_8__["default"])(!('afterClose' in props), 'Tag', "'afterClose' will be deprecated, please use 'onClose', we will remove this in the next version.");
    return _this;
  }

  _createClass(Tag, [{
    key: "getTagStyle",
    value: function getTagStyle() {
      var _this$props = this.props,
          color = _this$props.color,
          style = _this$props.style;
      var isPresetColor = this.isPresetColor();
      return _extends({
        backgroundColor: color && !isPresetColor ? color : undefined
      }, style);
    }
  }, {
    key: "getTagClassName",
    value: function getTagClassName(_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;
      var _this$props2 = this.props,
          customizePrefixCls = _this$props2.prefixCls,
          className = _this$props2.className,
          color = _this$props2.color;
      var visible = this.state.visible;
      var isPresetColor = this.isPresetColor();
      var prefixCls = getPrefixCls('tag', customizePrefixCls);
      return classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-").concat(color), isPresetColor), _defineProperty(_classNames, "".concat(prefixCls, "-has-color"), color && !isPresetColor), _defineProperty(_classNames, "".concat(prefixCls, "-hidden"), !visible), _classNames), className);
    }
  }, {
    key: "setVisible",
    value: function setVisible(visible, e) {
      var _this$props3 = this.props,
          onClose = _this$props3.onClose,
          afterClose = _this$props3.afterClose;

      if (onClose) {
        onClose(e);
      }

      if (afterClose && !onClose) {
        // next version remove.
        afterClose();
      }

      if (e.defaultPrevented) {
        return;
      }

      if (!('visible' in this.props)) {
        this.setState({
          visible: visible
        });
      }
    }
  }, {
    key: "isPresetColor",
    value: function isPresetColor() {
      var color = this.props.color;

      if (!color) {
        return false;
      }

      return PresetColorRegex.test(color);
    }
  }, {
    key: "renderCloseIcon",
    value: function renderCloseIcon() {
      var closable = this.props.closable;
      return closable ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
        type: "close",
        onClick: this.handleIconClick
      }) : null;
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderTag);
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps) {
      if ('visible' in nextProps) {
        return {
          visible: nextProps.visible
        };
      }

      return null;
    }
  }]);

  return Tag;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Tag.CheckableTag = _CheckableTag__WEBPACK_IMPORTED_MODULE_5__["default"];
Tag.defaultProps = {
  closable: false
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__["polyfill"])(Tag);
/* harmony default export */ __webpack_exports__["default"] = (Tag);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90YWcvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3RhZy9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IEljb24gZnJvbSAnLi4vaWNvbic7XG5pbXBvcnQgQ2hlY2thYmxlVGFnIGZyb20gJy4vQ2hlY2thYmxlVGFnJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmltcG9ydCB7IFByZXNldENvbG9yVHlwZXMgfSBmcm9tICcuLi9fdXRpbC9jb2xvcnMnO1xuaW1wb3J0IHdhcm5pbmcgZnJvbSAnLi4vX3V0aWwvd2FybmluZyc7XG5pbXBvcnQgV2F2ZSBmcm9tICcuLi9fdXRpbC93YXZlJztcbmNvbnN0IFByZXNldENvbG9yUmVnZXggPSBuZXcgUmVnRXhwKGBeKCR7UHJlc2V0Q29sb3JUeXBlcy5qb2luKCd8Jyl9KSgtaW52ZXJzZSk/JGApO1xuY2xhc3MgVGFnIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxuICAgICAgICB9O1xuICAgICAgICB0aGlzLmhhbmRsZUljb25DbGljayA9IChlKSA9PiB7XG4gICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgdGhpcy5zZXRWaXNpYmxlKGZhbHNlLCBlKTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJUYWcgPSAoY29uZmlnUHJvcHMpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IF9hID0gdGhpcy5wcm9wcywgeyBjaGlsZHJlbiB9ID0gX2EsIG90aGVyUHJvcHMgPSBfX3Jlc3QoX2EsIFtcImNoaWxkcmVuXCJdKTtcbiAgICAgICAgICAgIGNvbnN0IGlzTmVlZFdhdmUgPSAnb25DbGljaycgaW4gb3RoZXJQcm9wcyB8fCAoY2hpbGRyZW4gJiYgY2hpbGRyZW4udHlwZSA9PT0gJ2EnKTtcbiAgICAgICAgICAgIGNvbnN0IHRhZ1Byb3BzID0gb21pdChvdGhlclByb3BzLCBbXG4gICAgICAgICAgICAgICAgJ29uQ2xvc2UnLFxuICAgICAgICAgICAgICAgICdhZnRlckNsb3NlJyxcbiAgICAgICAgICAgICAgICAnY29sb3InLFxuICAgICAgICAgICAgICAgICd2aXNpYmxlJyxcbiAgICAgICAgICAgICAgICAnY2xvc2FibGUnLFxuICAgICAgICAgICAgICAgICdwcmVmaXhDbHMnLFxuICAgICAgICAgICAgXSk7XG4gICAgICAgICAgICByZXR1cm4gaXNOZWVkV2F2ZSA/ICg8V2F2ZT5cbiAgICAgICAgPHNwYW4gey4uLnRhZ1Byb3BzfSBjbGFzc05hbWU9e3RoaXMuZ2V0VGFnQ2xhc3NOYW1lKGNvbmZpZ1Byb3BzKX0gc3R5bGU9e3RoaXMuZ2V0VGFnU3R5bGUoKX0+XG4gICAgICAgICAge2NoaWxkcmVufVxuICAgICAgICAgIHt0aGlzLnJlbmRlckNsb3NlSWNvbigpfVxuICAgICAgICA8L3NwYW4+XG4gICAgICA8L1dhdmU+KSA6ICg8c3BhbiB7Li4udGFnUHJvcHN9IGNsYXNzTmFtZT17dGhpcy5nZXRUYWdDbGFzc05hbWUoY29uZmlnUHJvcHMpfSBzdHlsZT17dGhpcy5nZXRUYWdTdHlsZSgpfT5cbiAgICAgICAge2NoaWxkcmVufVxuICAgICAgICB7dGhpcy5yZW5kZXJDbG9zZUljb24oKX1cbiAgICAgIDwvc3Bhbj4pO1xuICAgICAgICB9O1xuICAgICAgICB3YXJuaW5nKCEoJ2FmdGVyQ2xvc2UnIGluIHByb3BzKSwgJ1RhZycsIFwiJ2FmdGVyQ2xvc2UnIHdpbGwgYmUgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSAnb25DbG9zZScsIHdlIHdpbGwgcmVtb3ZlIHRoaXMgaW4gdGhlIG5leHQgdmVyc2lvbi5cIik7XG4gICAgfVxuICAgIHN0YXRpYyBnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMobmV4dFByb3BzKSB7XG4gICAgICAgIGlmICgndmlzaWJsZScgaW4gbmV4dFByb3BzKSB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHZpc2libGU6IG5leHRQcm9wcy52aXNpYmxlLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgZ2V0VGFnU3R5bGUoKSB7XG4gICAgICAgIGNvbnN0IHsgY29sb3IsIHN0eWxlIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBjb25zdCBpc1ByZXNldENvbG9yID0gdGhpcy5pc1ByZXNldENvbG9yKCk7XG4gICAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKHsgYmFja2dyb3VuZENvbG9yOiBjb2xvciAmJiAhaXNQcmVzZXRDb2xvciA/IGNvbG9yIDogdW5kZWZpbmVkIH0sIHN0eWxlKTtcbiAgICB9XG4gICAgZ2V0VGFnQ2xhc3NOYW1lKHsgZ2V0UHJlZml4Q2xzIH0pIHtcbiAgICAgICAgY29uc3QgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgY2xhc3NOYW1lLCBjb2xvciB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgY29uc3QgeyB2aXNpYmxlIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBjb25zdCBpc1ByZXNldENvbG9yID0gdGhpcy5pc1ByZXNldENvbG9yKCk7XG4gICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygndGFnJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgcmV0dXJuIGNsYXNzTmFtZXMocHJlZml4Q2xzLCB7XG4gICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS0ke2NvbG9yfWBdOiBpc1ByZXNldENvbG9yLFxuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30taGFzLWNvbG9yYF06IGNvbG9yICYmICFpc1ByZXNldENvbG9yLFxuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30taGlkZGVuYF06ICF2aXNpYmxlLFxuICAgICAgICB9LCBjbGFzc05hbWUpO1xuICAgIH1cbiAgICBzZXRWaXNpYmxlKHZpc2libGUsIGUpIHtcbiAgICAgICAgY29uc3QgeyBvbkNsb3NlLCBhZnRlckNsb3NlIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBpZiAob25DbG9zZSkge1xuICAgICAgICAgICAgb25DbG9zZShlKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoYWZ0ZXJDbG9zZSAmJiAhb25DbG9zZSkge1xuICAgICAgICAgICAgLy8gbmV4dCB2ZXJzaW9uIHJlbW92ZS5cbiAgICAgICAgICAgIGFmdGVyQ2xvc2UoKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoZS5kZWZhdWx0UHJldmVudGVkKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCEoJ3Zpc2libGUnIGluIHRoaXMucHJvcHMpKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgdmlzaWJsZSB9KTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpc1ByZXNldENvbG9yKCkge1xuICAgICAgICBjb25zdCB7IGNvbG9yIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBpZiAoIWNvbG9yKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIFByZXNldENvbG9yUmVnZXgudGVzdChjb2xvcik7XG4gICAgfVxuICAgIHJlbmRlckNsb3NlSWNvbigpIHtcbiAgICAgICAgY29uc3QgeyBjbG9zYWJsZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgcmV0dXJuIGNsb3NhYmxlID8gPEljb24gdHlwZT1cImNsb3NlXCIgb25DbGljaz17dGhpcy5oYW5kbGVJY29uQ2xpY2t9Lz4gOiBudWxsO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyVGFnfTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cblRhZy5DaGVja2FibGVUYWcgPSBDaGVja2FibGVUYWc7XG5UYWcuZGVmYXVsdFByb3BzID0ge1xuICAgIGNsb3NhYmxlOiBmYWxzZSxcbn07XG5wb2x5ZmlsbChUYWcpO1xuZXhwb3J0IGRlZmF1bHQgVGFnO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFGQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFRQTtBQUNBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQWhCQTtBQUNBO0FBb0JBO0FBOUJBO0FBK0JBO0FBQ0E7OztBQVFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBS0E7OztBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFyREE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBOzs7O0FBeENBO0FBQ0E7QUF1RkE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/tag/index.js
