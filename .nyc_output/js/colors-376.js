__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PresetColorTypes", function() { return PresetColorTypes; });
/* harmony import */ var _type__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./type */ "./node_modules/antd/es/_util/type.js");
 // eslint-disable-next-line import/prefer-default-export

var PresetColorTypes = Object(_type__WEBPACK_IMPORTED_MODULE_0__["tuple"])('pink', 'red', 'yellow', 'orange', 'cyan', 'green', 'blue', 'purple', 'geekblue', 'magenta', 'volcano', 'gold', 'lime');//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9fdXRpbC9jb2xvcnMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL191dGlsL2NvbG9ycy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB0dXBsZSB9IGZyb20gJy4vdHlwZSc7XG4vLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgaW1wb3J0L3ByZWZlci1kZWZhdWx0LWV4cG9ydFxuZXhwb3J0IGNvbnN0IFByZXNldENvbG9yVHlwZXMgPSB0dXBsZSgncGluaycsICdyZWQnLCAneWVsbG93JywgJ29yYW5nZScsICdjeWFuJywgJ2dyZWVuJywgJ2JsdWUnLCAncHVycGxlJywgJ2dlZWtibHVlJywgJ21hZ2VudGEnLCAndm9sY2FubycsICdnb2xkJywgJ2xpbWUnKTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/_util/colors.js
