__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @component/page */ "./src/component/page/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _desk_page_header_style__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./desk-page-header-style */ "./src/app/desk/component/page/desk-page-header-style.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_menu__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common/menu */ "./src/common/menu.tsx");
/* harmony import */ var _component_index__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../component/index */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _components_email_table__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/email-table */ "./src/app/desk/component/page/components/email-table.tsx");
/* harmony import */ var _badge_bell__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./badge-bell */ "./src/app/desk/component/page/badge-bell.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/page/desk-page-header.tsx";










var SubMenu = antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].SubMenu;

var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_16__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

function getValidatUser(user) {
  if (!!user) {
    user.profile = user.profile || {};
  }

  return user || {};
}

var isMobile = _common__WEBPACK_IMPORTED_MODULE_11__["Utils"].getIsMobile();
/**
 * page header
 */

var DeskPageHeader =
/*#__PURE__*/
function (_PageHeader) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(DeskPageHeader, _PageHeader);

  function DeskPageHeader(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, DeskPageHeader);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(DeskPageHeader).call(this, props, context));
    _this.authorizedMenus = [];
    _this.activeFirstMenusKey = "";
    _this.activeSecondMenusKey = "";

    _this.visibleChange = function (visible) {
      _this.setState({
        showEmail: visible,
        emailVisible: visible
      });
    };

    _this.changeVisible = function () {
      _this.setState({
        showEmail: false,
        emailVisible: false
      });
    };

    _this.authorizedMenus = _this.buildAuthorizedMenus();
    _this.activeFirstMenusKey = (_this.authorizedMenus.find(function (item) {
      return window.location.pathname.startsWith(item.path);
    }) || {}).path || "/home";
    _this.activeSecondMenusKey = (_this.getChildrenMenus(_this.activeFirstMenusKey).find(function (item) {
      if (item.path.split("/").length === 3) {
        return window.location.pathname.startsWith(item.path);
      }

      return false;
    }) || {}).path || "/home";
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(DeskPageHeader, [{
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(DeskPageHeader.prototype), "initState", this).call(this), {
        collapsed: false,
        openKey: "",
        showEmail: false,
        emailVisible: false
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(DeskPageHeader.prototype), "initComponents", this).call(this), _desk_page_header_style__WEBPACK_IMPORTED_MODULE_10__["default"]);
    }
  }, {
    key: "buildAuthorizedMenus",
    value: function buildAuthorizedMenus() {
      var mainMenus = _common__WEBPACK_IMPORTED_MODULE_11__["Storage"].Account.session().get(_common__WEBPACK_IMPORTED_MODULE_11__["Consts"].MAIN_MENU_ITEM_AUTHORITIES) || [];
      var definedMenuList = Object(_common_menu__WEBPACK_IMPORTED_MODULE_12__["getMenuData"])();
      var authorizedMenuMap = mainMenus.reduce(function (acc, menuItem) {
        var authorized = menuItem.authorized,
            itemName = menuItem.itemName;
        acc[menuItem.itemCode] = {
          authorized: authorized,
          itemName: itemName
        };
        return acc;
      }, {});

      var filterMenu = function filterMenu(menusList) {
        var authorizedMenus = [];
        menusList.forEach(function (item) {
          var menuItem = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item);

          var itemCode = menuItem.itemCode,
              children = menuItem.children;
          var hasChildren = children && children.length;

          var _ref = authorizedMenuMap[itemCode] || {},
              _ref$authorized = _ref.authorized,
              authorized = _ref$authorized === void 0 ? false : _ref$authorized,
              _ref$itemName = _ref.itemName,
              itemName = _ref$itemName === void 0 ? "" : _ref$itemName;

          if (authorized) {
            menuItem.name = itemName;
            menuItem.authorized = authorized;
            authorizedMenus.push(menuItem);
          }

          if (hasChildren) {
            var subMenuItems = filterMenu(children);
            menuItem.children = subMenuItems || [];
          }
        });
        return authorizedMenus;
      };

      return filterMenu(definedMenuList);
    }
  }, {
    key: "renderFirstLevelMenuItem",
    value: function renderFirstLevelMenuItem() {
      var _this2 = this;

      return this.authorizedMenus.map(function (item, index) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
          key: item.path,
          onClick: function onClick() {
            var childrenMenus = _this2.getChildrenMenus(item.path);

            var childrenMenusTwo = childrenMenus.length > 0 ? _this2.getChildrenNextMenus(item.path, childrenMenus[0].path) : [];

            if (childrenMenus.length > 0) {
              var filter = childrenMenus.filter(function (item) {
                return item.itemCode === "MI0401";
              });
              setTimeout(function () {
                _this2.getHelpers().getRouter().pushRedirect(filter.length > 0 ? "/transactions/policyList" : childrenMenusTwo.length > 0 ? childrenMenusTwo[0].path : childrenMenus[0].path);
              }, 0);
            } else {
              setTimeout(function () {
                _this2.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"].NO_PAGE);
              }, 0);
            }
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 133
          },
          __self: this
        }, ["configuration", "kyc"].includes(item.icon) ? _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-".concat(item.icon),
          style: {
            marginRight: 8
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 164
          },
          __self: this
        }) : _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
          type: item.icon,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 165
          },
          __self: this
        }), item.name);
      });
    }
  }, {
    key: "isChildrenNext",
    value: function isChildrenNext(parentKey, parentNextKey) {
      var menuAll = Object(_common_menu__WEBPACK_IMPORTED_MODULE_12__["getMenuData"])();
      var childrenList = menuAll.filter(function (item) {
        return item.path === parentKey;
      });
      var childrenNextList = childrenList.length > 0 ? childrenList[0].children : [];
      var nextChildrenListFilter = childrenNextList.filter(function (item) {
        return item.path === parentNextKey;
      });
      return nextChildrenListFilter.length > 0 ? !!nextChildrenListFilter[0].children : false;
    }
  }, {
    key: "renderSecondLevelMenuItem",
    value: function renderSecondLevelMenuItem(parentKey) {
      var _this3 = this;

      return this.getChildrenMenus(parentKey).map(function (item, index) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
          key: item.path,
          onClick: function onClick() {
            var childrenMenusTwo = _this3.getChildrenNextMenus(parentKey, item.path);

            if (childrenMenusTwo.length > 0) {
              _this3.getHelpers().getRouter().pushRedirect(childrenMenusTwo[0].path);
            } else {
              if (_this3.isChildrenNext(parentKey, item.path) || !Object.values(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"]).includes(item.path)) {
                _this3.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"].NO_PAGE);
              } else {
                _this3.getHelpers().getRouter().pushRedirect(item.path);
              }
            }
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 183
          },
          __self: this
        }, item.children && item.children.length > 0 ? _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Dropdown"], {
          overlay: _this3.showThirdMenu(item),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 206
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          onClick: function onClick(e) {
            return e.stopPropagation();
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 207
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
          type: item.icon,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 208
          },
          __self: this
        }), item.name, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
          style: {
            fontSize: "18px",
            verticalAlign: "middle",
            marginLeft: "5px"
          },
          type: "down",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 210
          },
          __self: this
        }))) : _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
          type: item.icon,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 219
          },
          __self: this
        }), item.name));
      });
    }
  }, {
    key: "isPaths",
    value: function isPaths(path) {
      var count = 0;
      var pathArr = Object.values(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"]);
      pathArr.forEach(function (item) {
        if (typeof item === "string") {
          if (item.indexOf(path) === 0) count++;
        }
      });
      return count > 0;
    }
  }, {
    key: "showThirdMenu",
    value: function showThirdMenu(item) {
      var _this4 = this;

      return item.children ? _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 239
        },
        __self: this
      }, (item.children || []).map(function (every, index) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
          key: every.path,
          className: window.location.pathname === every.path ? "active-menu" : "",
          onClick: function onClick(e) {
            e.domEvent.stopPropagation();

            if (!_this4.isPaths(every.path)) {
              _this4.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"].NO_PAGE);
            } else {
              _this4.getHelpers().getRouter().pushRedirect(every.path);
            }
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 242
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("a", {
          target: "_blank",
          rel: "noopener noreferrer",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 257
          },
          __self: this
        }, every.name));
      })) : "";
    }
  }, {
    key: "getChildrenNextMenus",
    value: function getChildrenNextMenus(parentKey, parentNextKey) {
      var childrenMenus = this.getChildrenMenus(parentKey);

      if (childrenMenus.length > 0) {
        return (childrenMenus.find(function (item) {
          return item.path === parentNextKey;
        }) || {}).children || [];
      }

      return [];
    }
  }, {
    key: "getChildrenMenus",
    value: function getChildrenMenus(parentKey) {
      return (this.authorizedMenus.find(function (item) {
        return item.path === parentKey;
      }) || {}).children || [];
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      var C = this.getComponents();
      var user = getValidatUser(_common__WEBPACK_IMPORTED_MODULE_11__["Storage"].Account.session().get(_common__WEBPACK_IMPORTED_MODULE_11__["Consts"].ACCOUNT_KEY));

      if (!isMobile) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Box, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 297
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 298
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Header, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 299
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Container, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 300
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "head-container",
          style: {
            display: "flex",
            height: "55px"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 301
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Logo, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 302
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("img", {
          alt: "logo",
          height: "20",
          src: _common__WEBPACK_IMPORTED_MODULE_11__["Storage"].TitleLogo.session().get(_common__WEBPACK_IMPORTED_MODULE_11__["Consts"].LOGO_KEY),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 303
          },
          __self: this
        })), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Slider, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 306
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"], {
          className: "menu-first",
          mode: "horizontal",
          theme: "dark",
          selectedKeys: [this.activeFirstMenusKey],
          __source: {
            fileName: _jsxFileName,
            lineNumber: 307
          },
          __self: this
        }, this.renderFirstLevelMenuItem())), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Right, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 317
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Dropdown"], {
          visible: this.state.emailVisible,
          placement: "bottomRight",
          onVisibleChange: this.visibleChange,
          trigger: ["click"],
          overlay: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"], {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 321
            },
            __self: this
          }, this.state.showEmail && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_components_email_table__WEBPACK_IMPORTED_MODULE_14__["default"], {
            changeVisible: this.changeVisible,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 323
            },
            __self: this
          })),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 318
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          style: {
            height: "100%"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 326
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_badge_bell__WEBPACK_IMPORTED_MODULE_15__["default"], {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 327
          },
          __self: this
        }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Dropdown"], {
          overlayClassName: "header-menu",
          overlay: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"], {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 334
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
            key: "settings",
            onClick: function onClick() {
              _this5.getHelpers().getRouter().replaceRedirect(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"].ACCOUNT_PROFILE);
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 335
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
            type: "setting",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 343
            },
            __self: this
          }), _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Settings").thai("การตั้งค่า").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Divider, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 348
            },
            __self: this
          }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
            key: "logout",
            onClick: function onClick() {
              _this5.getHelpers().getAjax().get(_common__WEBPACK_IMPORTED_MODULE_11__["Apis"].SIGN_OUT, {}, {
                loading: false
              }).then(function (response) {}).finally(function () {
                _common__WEBPACK_IMPORTED_MODULE_11__["Storage"].clear();

                _this5.getHelpers().getRouter().replaceRedirect(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"].SIGN_IN);
              });
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 349
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
            type: "logout",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 371
            },
            __self: this
          }), _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Sign out").thai("ออกจากระบบ").getMessage())),
          trigger: ["click", "hover"],
          __source: {
            fileName: _jsxFileName,
            lineNumber: 331
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Action, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 380
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_component_index__WEBPACK_IMPORTED_MODULE_13__["Avatar"], {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 381
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 382
          },
          __self: this
        }, (user || {}).realName), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
          type: "down",
          "data-open": false,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 383
          },
          __self: this
        }))))))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.SubMenu, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 391
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "head-container sub-menu",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 392
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"], {
          mode: "horizontal",
          theme: "dark",
          selectedKeys: [this.activeSecondMenusKey],
          __source: {
            fileName: _jsxFileName,
            lineNumber: 393
          },
          __self: this
        }, this.renderSecondLevelMenuItem(this.activeFirstMenusKey))))));
      }

      if (!_common__WEBPACK_IMPORTED_MODULE_11__["Utils"].getIsInApp()) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Box, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 406
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.MobileHeader, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 407
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "top",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 408
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Dropdown"], {
          overlayClassName: "header-menu",
          overlay: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"], {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 412
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
            key: "settings",
            onClick: function onClick() {
              _this5.getHelpers().getRouter().replaceRedirect(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"].ACCOUNT_PROFILE);
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 413
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
            type: "setting",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 421
            },
            __self: this
          }), _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Settings").thai("การตั้งค่า").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Divider, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 426
            },
            __self: this
          }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
            key: "logout",
            onClick: function onClick() {
              _this5.getHelpers().getAjax().get(_common__WEBPACK_IMPORTED_MODULE_11__["Apis"].SIGN_OUT, {}, {
                loading: false
              }).then(function (response) {}).finally(function () {
                _common__WEBPACK_IMPORTED_MODULE_11__["Storage"].clear();

                _this5.getHelpers().getRouter().replaceRedirect(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"].SIGN_IN);
              });
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 427
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
            type: "logout",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 449
            },
            __self: this
          }), _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Sign out").thai("ออกจากระบบ").getMessage())),
          trigger: ["click", "hover"],
          __source: {
            fileName: _jsxFileName,
            lineNumber: 409
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Action, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 458
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_component_index__WEBPACK_IMPORTED_MODULE_13__["Avatar"], {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 459
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 460
          },
          __self: this
        }, (user || {}).realName), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
          type: "down",
          "data-open": false,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 461
          },
          __self: this
        }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Dropdown"], {
          visible: this.state.emailVisible,
          placement: "bottomRight",
          onVisibleChange: this.visibleChange,
          trigger: ["click"],
          overlay: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"], {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 467
            },
            __self: this
          }, this.state.showEmail && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_components_email_table__WEBPACK_IMPORTED_MODULE_14__["default"], {
            changeVisible: this.changeVisible,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 469
            },
            __self: this
          })),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 464
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          style: {
            height: "100%"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 472
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_badge_bell__WEBPACK_IMPORTED_MODULE_15__["default"], {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 473
          },
          __self: this
        })))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: this.state.collapsed ? "top-menu-icon blue" : "top-menu-icon back",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 477
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
          type: "bars",
          onClick: function onClick() {
            _this5.setState({
              collapsed: !_this5.state.collapsed
            });
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 478
          },
          __self: this
        })), this.state.collapsed && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 488
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"] // defaultSelectedKeys={['1']}
        // defaultOpenKeys={['sub1']}
        , {
          theme: "dark",
          key: "sub-menu",
          mode: "inline",
          className: "sub-menu",
          selectedKeys: [window.location.pathname],
          openKeys: [this.state.openKey] // inlineCollapsed={this.state.collapsed}
          ,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 489
          },
          __self: this
        }, this.authorizedMenus.map(function (item) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(SubMenu, {
            key: item.path,
            onTitleClick: function onTitleClick(_ref2) {
              var key = _ref2.key,
                  domEvent = _ref2.domEvent;

              _this5.setState({
                openKey: key
              });
            },
            title: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 510
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
              type: item.icon,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 511
              },
              __self: this
            }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 512
              },
              __self: this
            }, " ", item.name)),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 502
            },
            __self: this
          }, (item.children || []).map(function (child) {
            return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
              key: child.path,
              onClick: function onClick() {
                _this5.getHelpers().getRouter().pushRedirect(child.path);

                _this5.setState({
                  collapsed: !_this5.state.collapsed
                });
              },
              __source: {
                fileName: _jsxFileName,
                lineNumber: 518
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
              type: child.icon,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 529
              },
              __self: this
            }), child.name);
          }));
        })))));
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        style: {
          marginTop: -36
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 544
        },
        __self: this
      });
    }
  }]);

  return DeskPageHeader;
}(_component_page__WEBPACK_IMPORTED_MODULE_8__["PageHeader"]);

/* harmony default export */ __webpack_exports__["default"] = (DeskPageHeader);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BhZ2UvZGVzay1wYWdlLWhlYWRlci50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcGFnZS9kZXNrLXBhZ2UtaGVhZGVyLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBamF4UmVzcG9uc2UsIFBhZ2VDb21wb25lbnRzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IFBhZ2VIZWFkZXIgfSBmcm9tIFwiQGNvbXBvbmVudC9wYWdlXCI7XG5pbXBvcnQgeyBEcm9wZG93biwgSWNvbiwgTWVudSB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgRGVza1BhZ2VIZWFkZXJTdHlsZSBmcm9tIFwiLi9kZXNrLXBhZ2UtaGVhZGVyLXN0eWxlXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBBcGlzLCBDb25zdHMsIExhbmd1YWdlLCBQQVRILCBTdG9yYWdlLCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBnZXRNZW51RGF0YSB9IGZyb20gXCJAY29tbW9uL21lbnVcIjtcbmltcG9ydCB7IEF2YXRhciB9IGZyb20gXCIuLi8uLi9jb21wb25lbnQvaW5kZXhcIjtcbmltcG9ydCBFbWFpbFRhYmxlIGZyb20gXCIuL2NvbXBvbmVudHMvZW1haWwtdGFibGVcIjtcbmltcG9ydCBCZWxsQ291bnQgZnJvbSBcIi4vYmFkZ2UtYmVsbFwiO1xuaW1wb3J0IFRoZW1lIGZyb20gXCJAc3R5bGVzXCI7XG5cbmNvbnN0IHsgU3ViTWVudSB9ID0gTWVudTtcbmNvbnN0IHsgQ09MT1JfQSB9ID0gVGhlbWUuZ2V0VGhlbWUoKTtcbmZ1bmN0aW9uIGdldFZhbGlkYXRVc2VyKHVzZXI6IGFueSkge1xuICBpZiAoISF1c2VyKSB7XG4gICAgdXNlci5wcm9maWxlID0gdXNlci5wcm9maWxlIHx8IHt9O1xuICB9XG4gIHJldHVybiB1c2VyIHx8IHt9O1xufVxuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG4vKipcbiAqIHBhZ2UgaGVhZGVyXG4gKi9cbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuZXhwb3J0IHR5cGUgU3R5bGVkQSA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJhXCIsIGFueSwge30sIG5ldmVyPjtcbmV4cG9ydCB0eXBlIFN0eWxlZFNwYW4gPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwic3BhblwiLCBhbnksIHt9LCBuZXZlcj47XG5leHBvcnQgdHlwZSBEZXNrUGFnZUhlYWRlckNvbXBvbmVudHMgPSB7XG4gIEhlYWRlcjogU3R5bGVkRElWO1xuICBDb250YWluZXI6IFN0eWxlZERJVjtcbiAgUmlnaHQ6IFN0eWxlZERJVjtcbiAgTG9nbzogU3R5bGVkQTtcbiAgQWN0aW9uOiBTdHlsZWRTcGFuO1xuICBBdmF0YXI6IFN0eWxlZERJVjtcbiAgU2xpZGVyOiBTdHlsZWRESVY7XG4gIFN1Yk1lbnU6IFN0eWxlZERJVjtcbiAgTW9iaWxlSGVhZGVyOiBTdHlsZWRESVY7XG59ICYgUGFnZUNvbXBvbmVudHM7XG5cbmV4cG9ydCB0eXBlIEhlYWRlclN0YXRlID0ge1xuICBjb2xsYXBzZWQ6IGJvb2xlYW47XG4gIG9wZW5LZXk6IHN0cmluZztcbiAgc2hvd0VtYWlsOiBib29sZWFuO1xuICBlbWFpbFZpc2libGU6IGJvb2xlYW47XG59O1xuXG50eXBlIE1lbnVJdGVtID0ge1xuICBuYW1lOiBzdHJpbmc7XG4gIGl0ZW1Db2RlOiBzdHJpbmc7XG4gIGljb24/OiBhbnk7XG4gIHBhdGg6IHN0cmluZztcbiAgY2hpbGRyZW4/OiBNZW51SXRlbVtdO1xuICBhdXRob3JpemVkOiBib29sZWFuO1xufTtcblxuY2xhc3MgRGVza1BhZ2VIZWFkZXI8UCwgUyBleHRlbmRzIEhlYWRlclN0YXRlLCBDIGV4dGVuZHMgRGVza1BhZ2VIZWFkZXJDb21wb25lbnRzPiBleHRlbmRzIFBhZ2VIZWFkZXI8UCwgUywgQz4ge1xuICBwcm90ZWN0ZWQgYXV0aG9yaXplZE1lbnVzOiBNZW51SXRlbVtdID0gW107XG4gIHByb3RlY3RlZCBhY3RpdmVGaXJzdE1lbnVzS2V5OiBzdHJpbmcgPSBcIlwiO1xuICBwcm90ZWN0ZWQgYWN0aXZlU2Vjb25kTWVudXNLZXk6IHN0cmluZyA9IFwiXCI7XG5cbiAgY29uc3RydWN0b3IocHJvcHM6IGFueSwgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcblxuICAgIHRoaXMuYXV0aG9yaXplZE1lbnVzID0gdGhpcy5idWlsZEF1dGhvcml6ZWRNZW51cygpO1xuICAgIHRoaXMuYWN0aXZlRmlyc3RNZW51c0tleSA9XG4gICAgICAoXG4gICAgICAgIHRoaXMuYXV0aG9yaXplZE1lbnVzLmZpbmQoKGl0ZW06IE1lbnVJdGVtKTogYm9vbGVhbiA9PiB7XG4gICAgICAgICAgcmV0dXJuIHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZS5zdGFydHNXaXRoKGl0ZW0ucGF0aCk7XG4gICAgICAgIH0pIHx8ICh7fSBhcyBNZW51SXRlbSlcbiAgICAgICkucGF0aCB8fCBcIi9ob21lXCI7XG4gICAgdGhpcy5hY3RpdmVTZWNvbmRNZW51c0tleSA9IChcbiAgICAgICh0aGlzLmdldENoaWxkcmVuTWVudXModGhpcy5hY3RpdmVGaXJzdE1lbnVzS2V5KS5maW5kKChpdGVtOiBhbnkpOiBib29sZWFuID0+IHtcbiAgICAgICAgaWYgKGl0ZW0ucGF0aC5zcGxpdChcIi9cIikubGVuZ3RoID09PSAzKSB7XG4gICAgICAgICAgcmV0dXJuIHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZS5zdGFydHNXaXRoKGl0ZW0ucGF0aCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfSkgfHwge30pIGFzIGFueVxuICAgICkucGF0aCB8fCBcIi9ob21lXCI7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICBjb2xsYXBzZWQ6IGZhbHNlLFxuICAgICAgb3BlbktleTogXCJcIixcbiAgICAgIHNob3dFbWFpbDogZmFsc2UsXG4gICAgICBlbWFpbFZpc2libGU6IGZhbHNlLFxuICAgIH0pIGFzIFM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdENvbXBvbmVudHMoKSwgRGVza1BhZ2VIZWFkZXJTdHlsZSkgYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBidWlsZEF1dGhvcml6ZWRNZW51cygpOiBNZW51SXRlbVtdIHtcbiAgICBjb25zdCBtYWluTWVudXMgPSBTdG9yYWdlLkFjY291bnQuc2Vzc2lvbigpLmdldChDb25zdHMuTUFJTl9NRU5VX0lURU1fQVVUSE9SSVRJRVMpIHx8IFtdO1xuICAgIGNvbnN0IGRlZmluZWRNZW51TGlzdCA9IGdldE1lbnVEYXRhKCk7XG4gICAgY29uc3QgYXV0aG9yaXplZE1lbnVNYXAgPSBtYWluTWVudXMucmVkdWNlKChhY2M6IGFueSwgbWVudUl0ZW06IGFueSkgPT4ge1xuICAgICAgY29uc3QgeyBhdXRob3JpemVkLCBpdGVtTmFtZSB9ID0gbWVudUl0ZW07XG4gICAgICBhY2NbbWVudUl0ZW0uaXRlbUNvZGVdID0geyBhdXRob3JpemVkLCBpdGVtTmFtZSB9O1xuICAgICAgcmV0dXJuIGFjYztcbiAgICB9LCB7fSk7XG5cbiAgICBjb25zdCBmaWx0ZXJNZW51ID0gKG1lbnVzTGlzdDogYW55KSA9PiB7XG4gICAgICBjb25zdCBhdXRob3JpemVkTWVudXM6IE1lbnVJdGVtW10gPSBbXTtcblxuICAgICAgbWVudXNMaXN0LmZvckVhY2goKGl0ZW06IGFueSkgPT4ge1xuICAgICAgICBjb25zdCBtZW51SXRlbTogTWVudUl0ZW0gPSB7IC4uLml0ZW0gfTtcbiAgICAgICAgY29uc3QgeyBpdGVtQ29kZSwgY2hpbGRyZW4gfSA9IG1lbnVJdGVtO1xuICAgICAgICBjb25zdCBoYXNDaGlsZHJlbiA9IGNoaWxkcmVuICYmIGNoaWxkcmVuLmxlbmd0aDtcbiAgICAgICAgbGV0IHsgYXV0aG9yaXplZCA9IGZhbHNlLCBpdGVtTmFtZSA9IFwiXCIgfSA9IGF1dGhvcml6ZWRNZW51TWFwW2l0ZW1Db2RlXSB8fCB7fTtcblxuICAgICAgICBpZiAoYXV0aG9yaXplZCkge1xuICAgICAgICAgIG1lbnVJdGVtLm5hbWUgPSBpdGVtTmFtZTtcbiAgICAgICAgICBtZW51SXRlbS5hdXRob3JpemVkID0gYXV0aG9yaXplZDtcbiAgICAgICAgICBhdXRob3JpemVkTWVudXMucHVzaChtZW51SXRlbSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaGFzQ2hpbGRyZW4pIHtcbiAgICAgICAgICBjb25zdCBzdWJNZW51SXRlbXMgPSBmaWx0ZXJNZW51KGNoaWxkcmVuKTtcbiAgICAgICAgICBtZW51SXRlbS5jaGlsZHJlbiA9IHN1Yk1lbnVJdGVtcyB8fCBbXTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICByZXR1cm4gYXV0aG9yaXplZE1lbnVzO1xuICAgIH07XG4gICAgcmV0dXJuIGZpbHRlck1lbnUoZGVmaW5lZE1lbnVMaXN0KTtcbiAgfVxuXG4gIHJlbmRlckZpcnN0TGV2ZWxNZW51SXRlbSgpIHtcbiAgICByZXR1cm4gdGhpcy5hdXRob3JpemVkTWVudXMubWFwKChpdGVtOiBNZW51SXRlbSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgcmV0dXJuIChcbiAgICAgICAgPE1lbnUuSXRlbVxuICAgICAgICAgIGtleT17aXRlbS5wYXRofVxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGNoaWxkcmVuTWVudXMgPSB0aGlzLmdldENoaWxkcmVuTWVudXMoaXRlbS5wYXRoKTtcbiAgICAgICAgICAgIGNvbnN0IGNoaWxkcmVuTWVudXNUd28gPSBjaGlsZHJlbk1lbnVzLmxlbmd0aCA+IDBcbiAgICAgICAgICAgICAgPyB0aGlzLmdldENoaWxkcmVuTmV4dE1lbnVzKGl0ZW0ucGF0aCwgY2hpbGRyZW5NZW51c1swXS5wYXRoKVxuICAgICAgICAgICAgICA6IFtdO1xuICAgICAgICAgICAgaWYgKGNoaWxkcmVuTWVudXMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBsZXQgZmlsdGVyID0gY2hpbGRyZW5NZW51cy5maWx0ZXIoKGl0ZW06IGFueSkgPT4gaXRlbS5pdGVtQ29kZSA9PT0gXCJNSTA0MDFcIik7XG4gICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAgICAgICAuZ2V0Um91dGVyKClcbiAgICAgICAgICAgICAgICAgIC5wdXNoUmVkaXJlY3QoZmlsdGVyLmxlbmd0aCA+IDBcbiAgICAgICAgICAgICAgICAgICAgPyBcIi90cmFuc2FjdGlvbnMvcG9saWN5TGlzdFwiXG4gICAgICAgICAgICAgICAgICAgIDogKGNoaWxkcmVuTWVudXNUd28ubGVuZ3RoID4gMFxuICAgICAgICAgICAgICAgICAgICAgID8gY2hpbGRyZW5NZW51c1R3b1swXS5wYXRoXG4gICAgICAgICAgICAgICAgICAgICAgOiBjaGlsZHJlbk1lbnVzWzBdLnBhdGgpKTtcbiAgICAgICAgICAgICAgfSwgMCk7XG5cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAgICAgICAuZ2V0Um91dGVyKClcbiAgICAgICAgICAgICAgICAgIC5wdXNoUmVkaXJlY3QoUEFUSC5OT19QQUdFKTtcbiAgICAgICAgICAgICAgfSwgMCk7XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9fVxuICAgICAgICA+XG5cbiAgICAgICAgICB7W1wiY29uZmlndXJhdGlvblwiLCBcImt5Y1wiXS5pbmNsdWRlcyhpdGVtLmljb24pID9cbiAgICAgICAgICAgIDxpIGNsYXNzTmFtZT17YGljb25mb250IGljb24tJHtpdGVtLmljb259YH0gc3R5bGU9e3sgbWFyZ2luUmlnaHQ6IDggfX0vPiA6XG4gICAgICAgICAgICA8SWNvbiB0eXBlPXtpdGVtLmljb259Lz59XG4gICAgICAgICAge2l0ZW0ubmFtZX1cbiAgICAgICAgPC9NZW51Lkl0ZW0+XG4gICAgICApO1xuICAgIH0pO1xuICB9XG5cbiAgaXNDaGlsZHJlbk5leHQocGFyZW50S2V5OiBzdHJpbmcsIHBhcmVudE5leHRLZXk6IHN0cmluZykge1xuICAgIGNvbnN0IG1lbnVBbGwgPSBnZXRNZW51RGF0YSgpO1xuICAgIGNvbnN0IGNoaWxkcmVuTGlzdCA9IG1lbnVBbGwuZmlsdGVyKChpdGVtOiBhbnkpID0+IGl0ZW0ucGF0aCA9PT0gcGFyZW50S2V5KTtcbiAgICBjb25zdCBjaGlsZHJlbk5leHRMaXN0ID0gY2hpbGRyZW5MaXN0Lmxlbmd0aCA+IDAgPyBjaGlsZHJlbkxpc3RbMF0uY2hpbGRyZW4gOiBbXTtcbiAgICBjb25zdCBuZXh0Q2hpbGRyZW5MaXN0RmlsdGVyID0gY2hpbGRyZW5OZXh0TGlzdC5maWx0ZXIoKGl0ZW06IGFueSkgPT4gaXRlbS5wYXRoID09PSBwYXJlbnROZXh0S2V5KTtcbiAgICByZXR1cm4gbmV4dENoaWxkcmVuTGlzdEZpbHRlci5sZW5ndGggPiAwID8gISFuZXh0Q2hpbGRyZW5MaXN0RmlsdGVyWzBdLmNoaWxkcmVuIDogZmFsc2U7XG4gIH1cblxuICByZW5kZXJTZWNvbmRMZXZlbE1lbnVJdGVtKHBhcmVudEtleTogc3RyaW5nKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0Q2hpbGRyZW5NZW51cyhwYXJlbnRLZXkpLm1hcCgoaXRlbTogTWVudUl0ZW0sIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxNZW51Lkl0ZW1cbiAgICAgICAgICBrZXk9e2l0ZW0ucGF0aH1cbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBjaGlsZHJlbk1lbnVzVHdvID0gdGhpcy5nZXRDaGlsZHJlbk5leHRNZW51cyhwYXJlbnRLZXksIGl0ZW0ucGF0aCk7XG4gICAgICAgICAgICBpZiAoY2hpbGRyZW5NZW51c1R3by5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAgICAgLmdldFJvdXRlcigpXG4gICAgICAgICAgICAgICAgLnB1c2hSZWRpcmVjdChjaGlsZHJlbk1lbnVzVHdvWzBdLnBhdGgpO1xuXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBpZiAodGhpcy5pc0NoaWxkcmVuTmV4dChwYXJlbnRLZXksIGl0ZW0ucGF0aCkgfHwgIU9iamVjdC52YWx1ZXMoUEFUSCkuaW5jbHVkZXMoaXRlbS5wYXRoKSkge1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAgICAgICAuZ2V0Um91dGVyKClcbiAgICAgICAgICAgICAgICAgIC5wdXNoUmVkaXJlY3QoUEFUSC5OT19QQUdFKTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgICAgICAgICAgICAgLmdldFJvdXRlcigpXG4gICAgICAgICAgICAgICAgICAucHVzaFJlZGlyZWN0KGl0ZW0ucGF0aCk7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICB7KGl0ZW0uY2hpbGRyZW4gJiYgaXRlbS5jaGlsZHJlbiEubGVuZ3RoID4gMCkgPyA8RHJvcGRvd24gb3ZlcmxheT17dGhpcy5zaG93VGhpcmRNZW51KGl0ZW0pfT5cbiAgICAgICAgICAgICAgPGRpdiBvbkNsaWNrPXtlID0+IGUuc3RvcFByb3BhZ2F0aW9uKCl9PlxuICAgICAgICAgICAgICAgIDxJY29uIHR5cGU9e2l0ZW0uaWNvbn0vPlxuICAgICAgICAgICAgICAgIHtpdGVtLm5hbWV9XG4gICAgICAgICAgICAgICAgPEljb24gc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgIGZvbnRTaXplOiBcIjE4cHhcIixcbiAgICAgICAgICAgICAgICAgIHZlcnRpY2FsQWxpZ246IFwibWlkZGxlXCIsXG4gICAgICAgICAgICAgICAgICBtYXJnaW5MZWZ0OiBcIjVweFwiLFxuICAgICAgICAgICAgICAgIH19IHR5cGU9XCJkb3duXCIvPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvRHJvcGRvd24+XG4gICAgICAgICAgICA6XG4gICAgICAgICAgICA8PlxuICAgICAgICAgICAgICA8SWNvbiB0eXBlPXtpdGVtLmljb259Lz5cbiAgICAgICAgICAgICAge2l0ZW0ubmFtZX1cbiAgICAgICAgICAgIDwvPn1cbiAgICAgICAgPC9NZW51Lkl0ZW0+XG4gICAgICApO1xuICAgIH0pO1xuICB9XG5cbiAgaXNQYXRocyhwYXRoOiBhbnkpIHtcbiAgICBsZXQgY291bnQ6IG51bWJlciA9IDA7XG4gICAgY29uc3QgcGF0aEFycjogYW55W10gPSBPYmplY3QudmFsdWVzKFBBVEgpO1xuICAgIHBhdGhBcnIuZm9yRWFjaCgoaXRlbTogYW55KSA9PiB7XG4gICAgICBpZiAodHlwZW9mIChpdGVtKSA9PT0gXCJzdHJpbmdcIikge1xuICAgICAgICBpZiAoaXRlbS5pbmRleE9mKHBhdGgpID09PSAwKSBjb3VudCsrO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBjb3VudCA+IDA7XG4gIH1cblxuICBzaG93VGhpcmRNZW51KGl0ZW06IE1lbnVJdGVtKSB7XG4gICAgcmV0dXJuIChpdGVtLmNoaWxkcmVuID8gPE1lbnU+XG4gICAgICB7XG4gICAgICAgIChpdGVtLmNoaWxkcmVuIHx8IFtdKS5tYXAoKGV2ZXJ5OiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICByZXR1cm4gKDxNZW51Lkl0ZW0ga2V5PXtldmVyeS5wYXRofVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3dpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSA9PT0gZXZlcnkucGF0aCA/IFwiYWN0aXZlLW1lbnVcIiA6IFwiXCJ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eyhlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLmRvbUV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghdGhpcy5pc1BhdGhzKGV2ZXJ5LnBhdGgpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0Um91dGVyKClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnB1c2hSZWRpcmVjdChQQVRILk5PX1BBR0UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0Um91dGVyKClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnB1c2hSZWRpcmVjdChldmVyeS5wYXRoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0+XG4gICAgICAgICAgICA8YSB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub29wZW5lciBub3JlZmVycmVyXCI+XG4gICAgICAgICAgICAgIHtldmVyeS5uYW1lfVxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgIDwvTWVudS5JdGVtPik7XG4gICAgICAgIH0pXG4gICAgICB9XG4gICAgPC9NZW51PiA6IFwiXCIpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRDaGlsZHJlbk5leHRNZW51cyhwYXJlbnRLZXk6IHN0cmluZywgcGFyZW50TmV4dEtleTogc3RyaW5nKTogTWVudUl0ZW1bXSB7XG4gICAgY29uc3QgY2hpbGRyZW5NZW51cyA9IHRoaXMuZ2V0Q2hpbGRyZW5NZW51cyhwYXJlbnRLZXkpO1xuICAgIGlmIChjaGlsZHJlbk1lbnVzLmxlbmd0aCA+IDApIHtcbiAgICAgIHJldHVybiAoY2hpbGRyZW5NZW51cy5maW5kKChpdGVtOiBNZW51SXRlbSkgPT4gaXRlbS5wYXRoID09PSBwYXJlbnROZXh0S2V5KSB8fCAoe30gYXMgTWVudUl0ZW0pKS5jaGlsZHJlbiB8fCBbXTtcbiAgICB9XG4gICAgcmV0dXJuIFtdO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRDaGlsZHJlbk1lbnVzKHBhcmVudEtleTogc3RyaW5nKTogTWVudUl0ZW1bXSB7XG4gICAgcmV0dXJuICh0aGlzLmF1dGhvcml6ZWRNZW51cy5maW5kKChpdGVtOiBNZW51SXRlbSkgPT4gaXRlbS5wYXRoID09PSBwYXJlbnRLZXkpIHx8ICh7fSBhcyBNZW51SXRlbSkpLmNoaWxkcmVuIHx8IFtdO1xuICB9XG5cbiAgdmlzaWJsZUNoYW5nZSA9ICh2aXNpYmxlOiBhbnkpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHNob3dFbWFpbDogdmlzaWJsZSxcbiAgICAgIGVtYWlsVmlzaWJsZTogdmlzaWJsZSxcbiAgICB9KTtcbiAgfTtcblxuICBjaGFuZ2VWaXNpYmxlID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc2hvd0VtYWlsOiBmYWxzZSxcbiAgICAgIGVtYWlsVmlzaWJsZTogZmFsc2UsXG4gICAgfSk7XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCB1c2VyID0gZ2V0VmFsaWRhdFVzZXIoU3RvcmFnZS5BY2NvdW50LnNlc3Npb24oKS5nZXQoQ29uc3RzLkFDQ09VTlRfS0VZKSk7XG4gICAgaWYgKCFpc01vYmlsZSkge1xuICAgICAgcmV0dXJuIChcbiAgICAgICAgPEMuQm94PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8Qy5IZWFkZXI+XG4gICAgICAgICAgICAgIDxDLkNvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlYWQtY29udGFpbmVyXCIgc3R5bGU9e3sgZGlzcGxheTogXCJmbGV4XCIsIGhlaWdodDogXCI1NXB4XCIgfX0+XG4gICAgICAgICAgICAgICAgICA8Qy5Mb2dvPlxuICAgICAgICAgICAgICAgICAgICA8aW1nIGFsdD1cImxvZ29cIiBoZWlnaHQ9XCIyMFwiIHNyYz17U3RvcmFnZS5UaXRsZUxvZ28uc2Vzc2lvbigpLmdldChDb25zdHMuTE9HT19LRVkpfS8+XG4gICAgICAgICAgICAgICAgICA8L0MuTG9nbz5cblxuICAgICAgICAgICAgICAgICAgPEMuU2xpZGVyPlxuICAgICAgICAgICAgICAgICAgICA8TWVudVxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XCJtZW51LWZpcnN0XCJ9XG4gICAgICAgICAgICAgICAgICAgICAgbW9kZT1cImhvcml6b250YWxcIlxuICAgICAgICAgICAgICAgICAgICAgIHRoZW1lPVwiZGFya1wiXG4gICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRLZXlzPXtbdGhpcy5hY3RpdmVGaXJzdE1lbnVzS2V5XX1cbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnJlbmRlckZpcnN0TGV2ZWxNZW51SXRlbSgpfVxuICAgICAgICAgICAgICAgICAgICA8L01lbnU+XG4gICAgICAgICAgICAgICAgICA8L0MuU2xpZGVyPlxuXG4gICAgICAgICAgICAgICAgICA8Qy5SaWdodD5cbiAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duIHZpc2libGU9e3RoaXMuc3RhdGUuZW1haWxWaXNpYmxlfSBwbGFjZW1lbnQ9e1wiYm90dG9tUmlnaHRcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uVmlzaWJsZUNoYW5nZT17dGhpcy52aXNpYmxlQ2hhbmdlfSB0cmlnZ2VyPXtbXCJjbGlja1wiXX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJsYXk9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TWVudT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5zdGF0ZS5zaG93RW1haWwgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RW1haWxUYWJsZSBjaGFuZ2VWaXNpYmxlPXt0aGlzLmNoYW5nZVZpc2libGV9Lz59XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvTWVudT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBoZWlnaHQ6IFwiMTAwJVwiIH19PlxuICAgICAgICAgICAgICAgICAgICAgICAgPEJlbGxDb3VudC8+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvRHJvcGRvd24+XG5cbiAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duXG4gICAgICAgICAgICAgICAgICAgICAgb3ZlcmxheUNsYXNzTmFtZT1cImhlYWRlci1tZW51XCJcbiAgICAgICAgICAgICAgICAgICAgICBvdmVybGF5PXtcbiAgICAgICAgICAgICAgICAgICAgICAgIDxNZW51PlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8TWVudS5JdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PVwic2V0dGluZ3NcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRSb3V0ZXIoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZVJlZGlyZWN0KFBBVEguQUNDT1VOVF9QUk9GSUxFKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT1cInNldHRpbmdcIi8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwiU2V0dGluZ3NcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4LiB4Liy4Lij4LiV4Lix4LmJ4LiH4LiE4LmI4LiyXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L01lbnUuSXRlbT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPE1lbnUuRGl2aWRlci8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxNZW51Lkl0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9XCJsb2dvdXRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRBamF4KClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmdldChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBBcGlzLlNJR05fT1VULFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFN0b3JhZ2UuY2xlYXIoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmdldFJvdXRlcigpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZVJlZGlyZWN0KFBBVEguU0lHTl9JTik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SWNvbiB0eXBlPVwibG9nb3V0XCIvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIlNpZ24gb3V0XCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGhhaShcIuC4reC4reC4geC4iOC4suC4geC4o+C4sOC4muC4mlwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9NZW51Lkl0ZW0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L01lbnU+XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIHRyaWdnZXI9e1tcImNsaWNrXCIsIFwiaG92ZXJcIl19XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICA8Qy5BY3Rpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICA8QXZhdGFyLz5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPnsodXNlciB8fCB7fSkucmVhbE5hbWV9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT1cImRvd25cIiBkYXRhLW9wZW49e2ZhbHNlfS8+XG4gICAgICAgICAgICAgICAgICAgICAgPC9DLkFjdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgPC9Ecm9wZG93bj5cbiAgICAgICAgICAgICAgICAgIDwvQy5SaWdodD5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9DLkNvbnRhaW5lcj5cbiAgICAgICAgICAgIDwvQy5IZWFkZXI+XG5cbiAgICAgICAgICAgIDxDLlN1Yk1lbnU+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZC1jb250YWluZXIgc3ViLW1lbnVcIj5cbiAgICAgICAgICAgICAgICA8TWVudSBtb2RlPVwiaG9yaXpvbnRhbFwiXG4gICAgICAgICAgICAgICAgICAgICAgdGhlbWU9XCJkYXJrXCJcbiAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZEtleXM9e1t0aGlzLmFjdGl2ZVNlY29uZE1lbnVzS2V5XX0+XG4gICAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJTZWNvbmRMZXZlbE1lbnVJdGVtKHRoaXMuYWN0aXZlRmlyc3RNZW51c0tleSl9XG4gICAgICAgICAgICAgICAgPC9NZW51PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvQy5TdWJNZW51PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L0MuQm94PlxuICAgICAgKTtcbiAgICB9XG5cbiAgICBpZiAoIVV0aWxzLmdldElzSW5BcHAoKSkge1xuICAgICAgcmV0dXJuIDxDLkJveD5cbiAgICAgICAgPEMuTW9iaWxlSGVhZGVyPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidG9wXCI+XG4gICAgICAgICAgICA8RHJvcGRvd25cbiAgICAgICAgICAgICAgb3ZlcmxheUNsYXNzTmFtZT1cImhlYWRlci1tZW51XCJcbiAgICAgICAgICAgICAgb3ZlcmxheT17XG4gICAgICAgICAgICAgICAgPE1lbnU+XG4gICAgICAgICAgICAgICAgICA8TWVudS5JdGVtXG4gICAgICAgICAgICAgICAgICAgIGtleT1cInNldHRpbmdzXCJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAgICAgICAgICAgICAuZ2V0Um91dGVyKClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5yZXBsYWNlUmVkaXJlY3QoUEFUSC5BQ0NPVU5UX1BST0ZJTEUpO1xuICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8SWNvbiB0eXBlPVwic2V0dGluZ1wiLz5cbiAgICAgICAgICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwiU2V0dGluZ3NcIilcbiAgICAgICAgICAgICAgICAgICAgICAudGhhaShcIuC4geC4suC4o+C4leC4seC5ieC4h+C4hOC5iOC4slwiKVxuICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICA8L01lbnUuSXRlbT5cbiAgICAgICAgICAgICAgICAgIDxNZW51LkRpdmlkZXIvPlxuICAgICAgICAgICAgICAgICAgPE1lbnUuSXRlbVxuICAgICAgICAgICAgICAgICAgICBrZXk9XCJsb2dvdXRcIlxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRBamF4KClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5nZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgIEFwaXMuU0lHTl9PVVQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHt9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgU3RvcmFnZS5jbGVhcigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRSb3V0ZXIoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5yZXBsYWNlUmVkaXJlY3QoUEFUSC5TSUdOX0lOKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8SWNvbiB0eXBlPVwibG9nb3V0XCIvPlxuICAgICAgICAgICAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJTaWduIG91dFwiKVxuICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Lit4Lit4LiB4LiI4Liy4LiB4Lij4Liw4Lia4LiaXCIpXG4gICAgICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgIDwvTWVudS5JdGVtPlxuICAgICAgICAgICAgICAgIDwvTWVudT5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB0cmlnZ2VyPXtbXCJjbGlja1wiLCBcImhvdmVyXCJdfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8Qy5BY3Rpb24+XG4gICAgICAgICAgICAgICAgPEF2YXRhci8+XG4gICAgICAgICAgICAgICAgPHNwYW4+eyh1c2VyIHx8IHt9KS5yZWFsTmFtZX08L3NwYW4+XG4gICAgICAgICAgICAgICAgPEljb24gdHlwZT1cImRvd25cIiBkYXRhLW9wZW49e2ZhbHNlfS8+XG4gICAgICAgICAgICAgIDwvQy5BY3Rpb24+XG4gICAgICAgICAgICA8L0Ryb3Bkb3duPlxuICAgICAgICAgICAgPERyb3Bkb3duIHZpc2libGU9e3RoaXMuc3RhdGUuZW1haWxWaXNpYmxlfSBwbGFjZW1lbnQ9e1wiYm90dG9tUmlnaHRcIn1cbiAgICAgICAgICAgICAgICAgICAgICBvblZpc2libGVDaGFuZ2U9e3RoaXMudmlzaWJsZUNoYW5nZX0gdHJpZ2dlcj17W1wiY2xpY2tcIl19XG4gICAgICAgICAgICAgICAgICAgICAgb3ZlcmxheT17XG4gICAgICAgICAgICAgICAgICAgICAgICA8TWVudT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMuc3RhdGUuc2hvd0VtYWlsICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxFbWFpbFRhYmxlIGNoYW5nZVZpc2libGU9e3RoaXMuY2hhbmdlVmlzaWJsZX0vPn1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvTWVudT5cbiAgICAgICAgICAgICAgICAgICAgICB9PlxuICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IGhlaWdodDogXCIxMDAlXCIgfX0+XG4gICAgICAgICAgICAgICAgPEJlbGxDb3VudC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9Ecm9wZG93bj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17dGhpcy5zdGF0ZS5jb2xsYXBzZWQgPyBgdG9wLW1lbnUtaWNvbiBibHVlYCA6IGB0b3AtbWVudS1pY29uIGJhY2tgfT5cbiAgICAgICAgICAgIDxJY29uXG4gICAgICAgICAgICAgIHR5cGU9XCJiYXJzXCJcbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgY29sbGFwc2VkOiAhdGhpcy5zdGF0ZS5jb2xsYXBzZWQsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIHt0aGlzLnN0YXRlLmNvbGxhcHNlZCAmJiAoXG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICA8TWVudVxuICAgICAgICAgICAgICAgIC8vIGRlZmF1bHRTZWxlY3RlZEtleXM9e1snMSddfVxuICAgICAgICAgICAgICAgIC8vIGRlZmF1bHRPcGVuS2V5cz17WydzdWIxJ119XG4gICAgICAgICAgICAgICAgdGhlbWU9XCJkYXJrXCJcbiAgICAgICAgICAgICAgICBrZXk9XCJzdWItbWVudVwiXG4gICAgICAgICAgICAgICAgbW9kZT1cImlubGluZVwiXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic3ViLW1lbnVcIlxuICAgICAgICAgICAgICAgIHNlbGVjdGVkS2V5cz17W3dpbmRvdy5sb2NhdGlvbi5wYXRobmFtZV19XG4gICAgICAgICAgICAgICAgb3BlbktleXM9e1t0aGlzLnN0YXRlLm9wZW5LZXldfVxuICAgICAgICAgICAgICAgIC8vIGlubGluZUNvbGxhcHNlZD17dGhpcy5zdGF0ZS5jb2xsYXBzZWR9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7dGhpcy5hdXRob3JpemVkTWVudXMubWFwKChpdGVtOiBNZW51SXRlbSkgPT4ge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgPFN1Yk1lbnVcbiAgICAgICAgICAgICAgICAgICAgICBrZXk9e2l0ZW0ucGF0aH1cbiAgICAgICAgICAgICAgICAgICAgICBvblRpdGxlQ2xpY2s9eyh7IGtleSwgZG9tRXZlbnQgfSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIG9wZW5LZXk6IGtleSxcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e1xuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT17aXRlbS5pY29ufS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+IHtpdGVtLm5hbWV9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgeyhpdGVtLmNoaWxkcmVuIHx8IFtdKS5tYXAoKGNoaWxkOiBNZW51SXRlbSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPE1lbnUuSXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17Y2hpbGQucGF0aH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0Um91dGVyKClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnB1c2hSZWRpcmVjdChjaGlsZC5wYXRoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xsYXBzZWQ6ICF0aGlzLnN0YXRlLmNvbGxhcHNlZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SWNvbiB0eXBlPXtjaGlsZC5pY29ufS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge2NoaWxkLm5hbWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvTWVudS5JdGVtPlxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICB9KX1cbiAgICAgICAgICAgICAgICAgICAgPC9TdWJNZW51PlxuICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICB9KX1cbiAgICAgICAgICAgICAgPC9NZW51PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKX1cbiAgICAgICAgPC9DLk1vYmlsZUhlYWRlcj5cbiAgICAgIDwvQy5Cb3g+O1xuICAgIH1cblxuICAgIHJldHVybiA8ZGl2IHN0eWxlPXt7IG1hcmdpblRvcDogLTM2IH19PjwvZGl2PjtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBEZXNrUGFnZUhlYWRlcjtcblxuXG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFrQ0E7Ozs7O0FBS0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQXdOQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUE3TkE7QUErTkE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBak9BO0FBQ0E7QUFHQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFoQkE7QUFrQkE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBR0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBT0E7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQUVBO0FBQ0E7QUEzQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBK0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUdBO0FBRUE7QUFDQTtBQXJCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF1QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFJQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFHQTtBQUVBO0FBZEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBR0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7OztBQWdCQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUdBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFNQTtBQURBO0FBT0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQXBCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFzQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQS9DQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFpREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBTUE7QUFEQTtBQU9BO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFwQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBc0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUEvQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBRkE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFHQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFHQTtBQU1BO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7O0FBdmVBO0FBQ0E7QUF5ZUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/page/desk-page-header.tsx
