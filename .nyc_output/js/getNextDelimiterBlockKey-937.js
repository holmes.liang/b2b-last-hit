
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getNextDelimiterBlockKey
 * @format
 * 
 *
 * This is unstable and not part of the public API and should not be used by
 * production systems. This file may be update/removed without notice.
 */

var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var getNextDelimiterBlockKey = function getNextDelimiterBlockKey(block, blockMap) {
  var isExperimentalTreeBlock = block instanceof ContentBlockNode;

  if (!isExperimentalTreeBlock) {
    return null;
  }

  var nextSiblingKey = block.getNextSiblingKey();

  if (nextSiblingKey) {
    return nextSiblingKey;
  }

  var parent = block.getParentKey();

  if (!parent) {
    return null;
  }

  var nextNonDescendantBlock = blockMap.get(parent);

  while (nextNonDescendantBlock && !nextNonDescendantBlock.getNextSiblingKey()) {
    var parentKey = nextNonDescendantBlock.getParentKey();
    nextNonDescendantBlock = parentKey ? blockMap.get(parentKey) : null;
  }

  if (!nextNonDescendantBlock) {
    return null;
  }

  return nextNonDescendantBlock.getNextSiblingKey();
};

module.exports = getNextDelimiterBlockKey;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldE5leHREZWxpbWl0ZXJCbG9ja0tleS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9nZXROZXh0RGVsaW1pdGVyQmxvY2tLZXkuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGdldE5leHREZWxpbWl0ZXJCbG9ja0tleVxuICogQGZvcm1hdFxuICogXG4gKlxuICogVGhpcyBpcyB1bnN0YWJsZSBhbmQgbm90IHBhcnQgb2YgdGhlIHB1YmxpYyBBUEkgYW5kIHNob3VsZCBub3QgYmUgdXNlZCBieVxuICogcHJvZHVjdGlvbiBzeXN0ZW1zLiBUaGlzIGZpbGUgbWF5IGJlIHVwZGF0ZS9yZW1vdmVkIHdpdGhvdXQgbm90aWNlLlxuICovXG5cbnZhciBDb250ZW50QmxvY2tOb2RlID0gcmVxdWlyZSgnLi9Db250ZW50QmxvY2tOb2RlJyk7XG5cbnZhciBnZXROZXh0RGVsaW1pdGVyQmxvY2tLZXkgPSBmdW5jdGlvbiBnZXROZXh0RGVsaW1pdGVyQmxvY2tLZXkoYmxvY2ssIGJsb2NrTWFwKSB7XG4gIHZhciBpc0V4cGVyaW1lbnRhbFRyZWVCbG9jayA9IGJsb2NrIGluc3RhbmNlb2YgQ29udGVudEJsb2NrTm9kZTtcblxuICBpZiAoIWlzRXhwZXJpbWVudGFsVHJlZUJsb2NrKSB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICB2YXIgbmV4dFNpYmxpbmdLZXkgPSBibG9jay5nZXROZXh0U2libGluZ0tleSgpO1xuXG4gIGlmIChuZXh0U2libGluZ0tleSkge1xuICAgIHJldHVybiBuZXh0U2libGluZ0tleTtcbiAgfVxuXG4gIHZhciBwYXJlbnQgPSBibG9jay5nZXRQYXJlbnRLZXkoKTtcblxuICBpZiAoIXBhcmVudCkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgdmFyIG5leHROb25EZXNjZW5kYW50QmxvY2sgPSBibG9ja01hcC5nZXQocGFyZW50KTtcbiAgd2hpbGUgKG5leHROb25EZXNjZW5kYW50QmxvY2sgJiYgIW5leHROb25EZXNjZW5kYW50QmxvY2suZ2V0TmV4dFNpYmxpbmdLZXkoKSkge1xuICAgIHZhciBwYXJlbnRLZXkgPSBuZXh0Tm9uRGVzY2VuZGFudEJsb2NrLmdldFBhcmVudEtleSgpO1xuICAgIG5leHROb25EZXNjZW5kYW50QmxvY2sgPSBwYXJlbnRLZXkgPyBibG9ja01hcC5nZXQocGFyZW50S2V5KSA6IG51bGw7XG4gIH1cblxuICBpZiAoIW5leHROb25EZXNjZW5kYW50QmxvY2spIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHJldHVybiBuZXh0Tm9uRGVzY2VuZGFudEJsb2NrLmdldE5leHRTaWJsaW5nS2V5KCk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGdldE5leHREZWxpbWl0ZXJCbG9ja0tleTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getNextDelimiterBlockKey.js
