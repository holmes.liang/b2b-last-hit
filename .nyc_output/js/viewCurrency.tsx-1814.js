__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewCurrencyItemStyle2", function() { return ViewCurrencyItemStyle2; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _desk_component_view_item_style__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @desk-component/view-item/style */ "./src/app/desk/component/view-item/style.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);



var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/component/viewCurrency.tsx";






var isMobile = _common__WEBPACK_IMPORTED_MODULE_7__["Utils"].getIsMobile();
var defaultLayout = {
  labelCol: {
    xs: {
      span: 6
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 13
    },
    sm: {
      span: 16
    }
  }
};

var ViewCurrencyItem = function ViewCurrencyItem(_ref) {
  var title = _ref.title,
      value = _ref.value,
      _ref$layout = _ref.layout,
      layout = _ref$layout === void 0 ? defaultLayout : _ref$layout,
      rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__["default"])(_ref, ["title", "value", "layout"]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_8__["useState"])({}),
      _useState2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_useState, 2),
      currencyTable = _useState2[0],
      setCurrency = _useState2[1];

  var getMasterTableItemText = function getMasterTableItemText(tableName, itemId, currencyTable) {
    var currencyTables = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.cloneDeep(currencyTable);

    if (itemId) {
      var item = (currencyTables[tableName] || []).find(function (option) {
        return itemId.toString() === option.currencyCode.toString();
      }) || {};
      return item.currencyName || "";
    } else {
      return "";
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_8__["useEffect"])(function () {
    _common__WEBPACK_IMPORTED_MODULE_7__["Ajax"].get("/currencies").then(function (res) {
      setCurrency({
        currency: (res.body || {}).respData || []
      });
    });
  }, []);
  return _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(_desk_component_view_item_style__WEBPACK_IMPORTED_MODULE_6__["default"].box, Object.assign({}, rest, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Row"], {
    className: "item",
    type: "flex",
    gutter: 24,
    align: "middle",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Col"], Object.assign({
    className: "itemTitle ".concat(isMobile ? "mobile-item-title" : "")
  }, layout.labelCol, {
    style: {
      paddingRight: 1
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }), title), _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Col"], Object.assign({
    className: "itemContent"
  }, layout.wrapperCol, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: this
  }), getMasterTableItemText("currency", value, currencyTable))));
};

var defaultLayout2 = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};

var ViewCurrencyItemStyle2 = function ViewCurrencyItemStyle2(props) {
  return ViewCurrencyItem(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, props, {
    layout: defaultLayout2
  }));
};


/* harmony default export */ __webpack_exports__["default"] = (ViewCurrencyItem);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3ZpZXdDdXJyZW5jeS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9TQUlDL2lhci9jb21wb25lbnQvdmlld0N1cnJlbmN5LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgQ29sLCBSb3cgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IFN0eWxlIGZyb20gXCJAZGVzay1jb21wb25lbnQvdmlldy1pdGVtL3N0eWxlXCI7XG5pbXBvcnQgeyBBamF4LCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyB1c2VFZmZlY3QsIHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XG5cbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcbmNvbnN0IGRlZmF1bHRMYXlvdXQ6IGFueSA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiA2IH0sXG4gICAgc206IHsgc3BhbjogOCB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTMgfSxcbiAgICBzbTogeyBzcGFuOiAxNiB9LFxuICB9LFxufTtcblxuY29uc3QgVmlld0N1cnJlbmN5SXRlbSA9ICh7IHRpdGxlLCB2YWx1ZSwgbGF5b3V0ID0gZGVmYXVsdExheW91dCwgLi4ucmVzdCB9OiBhbnkpID0+IHtcbiAgY29uc3QgW2N1cnJlbmN5VGFibGUsIHNldEN1cnJlbmN5XSA9IHVzZVN0YXRlKHt9KTtcbiAgY29uc3QgZ2V0TWFzdGVyVGFibGVJdGVtVGV4dCA9ICh0YWJsZU5hbWU6IHN0cmluZywgaXRlbUlkOiBzdHJpbmcsIGN1cnJlbmN5VGFibGU6IG9iamVjdCkgPT4ge1xuICAgIGNvbnN0IGN1cnJlbmN5VGFibGVzOiBhbnkgPSBfLmNsb25lRGVlcChjdXJyZW5jeVRhYmxlKTtcbiAgICBpZiAoaXRlbUlkKSB7XG4gICAgICBjb25zdCBpdGVtID1cbiAgICAgICAgKGN1cnJlbmN5VGFibGVzW3RhYmxlTmFtZV0gfHwgW10pLmZpbmQoKG9wdGlvbjogYW55KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIGl0ZW1JZC50b1N0cmluZygpID09PSBvcHRpb24uY3VycmVuY3lDb2RlLnRvU3RyaW5nKCk7XG4gICAgICAgIH0pIHx8IHt9O1xuICAgICAgcmV0dXJuIGl0ZW0uY3VycmVuY3lOYW1lIHx8IFwiXCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBcIlwiO1xuICAgIH1cbiAgfTtcbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBBamF4LmdldChcIi9jdXJyZW5jaWVzXCIpLnRoZW4oKHJlcykgPT4ge1xuICAgICAgc2V0Q3VycmVuY3koXG4gICAgICAgIHtcbiAgICAgICAgICBjdXJyZW5jeTogKChyZXMuYm9keSB8fCB7fSkucmVzcERhdGEgfHwgW10pLFxuICAgICAgICB9LFxuICAgICAgKTtcbiAgICB9KTtcbiAgfSwgW10pO1xuICByZXR1cm4gKFxuICAgIDxTdHlsZS5ib3ggey4uLnJlc3R9PlxuICAgICAgPFJvdyBjbGFzc05hbWU9XCJpdGVtXCIgdHlwZT1cImZsZXhcIiBndXR0ZXI9ezI0fSBhbGlnbj1cIm1pZGRsZVwiPlxuICAgICAgICA8Q29sXG4gICAgICAgICAgY2xhc3NOYW1lPXtgaXRlbVRpdGxlICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1pdGVtLXRpdGxlXCIgOiBcIlwifWB9XG4gICAgICAgICAgey4uLmxheW91dC5sYWJlbENvbH1cbiAgICAgICAgICBzdHlsZT17eyBwYWRkaW5nUmlnaHQ6IDEgfX1cbiAgICAgICAgPlxuICAgICAgICAgIHt0aXRsZX1cbiAgICAgICAgPC9Db2w+XG4gICAgICAgIDxDb2wgY2xhc3NOYW1lPVwiaXRlbUNvbnRlbnRcIiB7Li4ubGF5b3V0LndyYXBwZXJDb2x9PlxuICAgICAgICAgIHtnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0KFwiY3VycmVuY3lcIiwgdmFsdWUsIGN1cnJlbmN5VGFibGUpfVxuICAgICAgICA8L0NvbD5cbiAgICAgIDwvUm93PlxuICAgIDwvU3R5bGUuYm94PlxuICApO1xufTtcblxuY29uc3QgZGVmYXVsdExheW91dDIgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogOCB9LFxuICAgIHNtOiB7IHNwYW46IDYgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDE2IH0sXG4gICAgc206IHsgc3BhbjogMTMgfSxcbiAgfSxcbn07XG5cbmNvbnN0IFZpZXdDdXJyZW5jeUl0ZW1TdHlsZTIgPSAocHJvcHM6IGFueSkgPT4gVmlld0N1cnJlbmN5SXRlbSh7IC4uLnByb3BzLCBsYXlvdXQ6IGRlZmF1bHRMYXlvdXQyIH0pO1xuXG5leHBvcnQgeyBWaWV3Q3VycmVuY3lJdGVtU3R5bGUyIH07XG5cbmV4cG9ydCBkZWZhdWx0IFZpZXdDdXJyZW5jeUl0ZW07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFEQTtBQUdBO0FBQUE7QUFBQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBQ0E7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/component/viewCurrency.tsx
