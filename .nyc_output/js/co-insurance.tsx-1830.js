__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CoInsurance; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var number_precision__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! number-precision */ "./node_modules/number-precision/build/index.js");
/* harmony import */ var number_precision__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(number_precision__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_services_model__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk/services/model */ "./src/app/desk/services/model.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_table_action_bars__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-component/table/action-bars */ "./src/app/desk/component/table/action-bars.tsx");
/* harmony import */ var _desk_claims_handing_details_SAIC_components_settlement_components_style_table__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk/claims/handing/details/SAIC/components/settlement/components/style/table */ "./src/app/desk/claims/handing/details/SAIC/components/settlement/components/style/table.tsx");
/* harmony import */ var _desk_styles_global__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk-styles/global */ "./src/app/desk/styles/global.tsx");
/* harmony import */ var _app_component_NPrice__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @app-component/NPrice */ "./src/app/component/NPrice.tsx");
/* harmony import */ var _desk_component_table_actions_under_table__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @desk-component/table/actions-under-table */ "./src/app/desk/component/table/actions-under-table.tsx");
/* harmony import */ var _service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../service */ "./src/app/desk/quote/SAIC/iar/service.ts");
/* harmony import */ var _desk_component_useful_form_item_utils_master_table_item__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @desk-component/useful-form-item/utils/master-table-item */ "./src/app/desk/component/useful-form-item/utils/master-table-item.tsx");
/* harmony import */ var _common_utils_string_utils__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @common/utils/string-utils */ "./src/common/utils/string-utils.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_location__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @desk/quote/SAIC/iar/location */ "./src/app/desk/quote/SAIC/iar/location.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/component/co-insurance.tsx";
















var Column = antd__WEBPACK_IMPORTED_MODULE_9__["Table"].Column;

var CoInsurance =
/*#__PURE__*/
function (_Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(CoInsurance, _Component);

  function CoInsurance() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, CoInsurance);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(CoInsurance)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.calcFollowerTotal = function () {
      var _income = _this.getDataFromModelByPropName("coFeeExpense", "leader");

      var _gst = _this.getDataFromModelByPropName("coFeeExpenseTurnoverTax", "leader");

      if (_common__WEBPACK_IMPORTED_MODULE_13__["Rules"].isNum(_income) && _common__WEBPACK_IMPORTED_MODULE_13__["Rules"].isNum(_gst)) {
        var premium = number_precision__WEBPACK_IMPORTED_MODULE_10___default.a.plus(_income, _gst);
        premium = number_precision__WEBPACK_IMPORTED_MODULE_10___default.a.round(premium, 2);

        var _propName = _this.generatePropName("totalCoFeeExpense", "leader");

        _this.props.that.setValueToModel(premium, _propName);

        _this.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, _propName, premium));

        var data = _common__WEBPACK_IMPORTED_MODULE_13__["Utils"].formatCurrencyInfo(premium, "");
        return "".concat(data.amount);
      }

      return "";
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(CoInsurance, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      _desk_services_model__WEBPACK_IMPORTED_MODULE_12__["ModelSevice"].setValueIfUndefined({
        propsName: this.generatePropName("coinsuranceInvolved"),
        value: "N",
        // todo N
        that: this.props.that
      });
    } // renders

  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        },
        __self: this
      }, this.renderInvolved(), this.isInvolved && this.renderInvolvedBox());
    }
  }, {
    key: "renderInvolved",
    value: function renderInvolved() {
      var _this2 = this;

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NRadio"], {
        form: form,
        model: model,
        propName: this.generatePropName("coinsuranceInvolved"),
        label: "Co-insurance Involved",
        options: _common__WEBPACK_IMPORTED_MODULE_13__["Codes"].BOOLEAN_YN,
        onChange: function onChange() {
          _this2.mergeForCalc(); //  todo ? 清空


          var newCo = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.pick(_this2.getDataFromModelByPropName(""), ["coinsuranceInvolved"]);

          _this2.props.that.setValueToModel(newCo, _this2.generatePropName(""));

          _this2.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, _this2.generatePropName(""), newCo));

          console.log(_this2.getDataFromModelByPropName(""), "=======", _this2.generatePropName(""));
          console.log(_this2.props.model);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        },
        __self: this
      });
    }
  }, {
    key: "renderInvolvedBox",
    value: function renderInvolvedBox() {
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 73
        },
        __self: this
      }, this.renderPersonType(), this.renderSharePercent(), this.renderPersonBox());
    }
  }, {
    key: "renderPersonBox",
    value: function renderPersonBox() {
      if (this.isLeader === null) return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        },
        __self: this
      });
      if (this.isLeader) return this.renderLeaderBox();
      return this.renderFollowersBox();
    }
  }, {
    key: "renderLeaderBox",
    value: function renderLeaderBox() {
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87
        },
        __self: this
      }, this.renderFollowers());
    }
  }, {
    key: "renderFollowersBox",
    value: function renderFollowersBox() {
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 93
        },
        __self: this
      }, this.renderLeader(), this.renderFee(), this.renderGst(), this.renderTotal());
    }
  }, {
    key: "renderPersonType",
    value: function renderPersonType() {
      var _this3 = this;

      var _this$props2 = this.props,
          form = _this$props2.form,
          model = _this$props2.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NSelect"], {
        form: form,
        model: model,
        propName: this.generatePropName("isLeader"),
        label: "We Are",
        options: [{
          id: "Y",
          text: "Leader"
        }, {
          id: "N",
          text: "Follower"
        }],
        size: "large",
        onSelect: function onSelect() {
          console.log(_this3.generatePropName("isLeader"), "=====a==s isisiisisi");
          console.log(_this3.props.model, "=====a==s");
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 103
        },
        __self: this
      });
    }
  }, {
    key: "renderSharePercent",
    value: function renderSharePercent() {
      var _this4 = this;

      var _this$props3 = this.props,
          form = _this$props3.form,
          model = _this$props3.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NRate"], {
        form: form,
        model: model,
        size: "large",
        propName: this.generatePropName("ourShareRate"),
        label: "Our Share",
        onBlur: function onBlur() {
          _this4.mergeForCalc();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 119
        },
        __self: this
      }));
    }
  }, {
    key: "renderFollowers",
    value: function renderFollowers() {
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 132
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(CoInsuranceFollowers, Object.assign({}, this.props, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133
        },
        __self: this
      })));
    }
  }, {
    key: "renderLeader",
    value: function renderLeader() {
      var _this$props4 = this.props,
          form = _this$props4.form,
          model = _this$props4.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NSelect"], {
        form: form,
        model: model,
        propName: this.generatePropName("leaderInsurerId", "leader"),
        label: "Leader",
        fetchFunction: function fetchFunction() {
          return _service__WEBPACK_IMPORTED_MODULE_19__["QuoteIarService"].fetchLeaderList();
        },
        size: "large",
        onSelect: function onSelect() {},
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      });
    }
  }, {
    key: "renderFee",
    value: function renderFee() {
      var _this$props5 = this.props,
          form = _this$props5.form,
          model = _this$props5.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 153
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPrice"], {
        form: form,
        model: model,
        size: "large",
        propName: this.generatePropName("coFeeExpense", "leader"),
        label: "Co Fee Expense",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 154
        },
        __self: this
      }));
    }
  }, {
    key: "renderGst",
    value: function renderGst() {
      var _this$props6 = this.props,
          form = _this$props6.form,
          model = _this$props6.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 165
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPrice"], {
        form: form,
        model: model,
        size: "large",
        propName: this.generatePropName("coFeeExpenseTurnoverTax", "leader"),
        label: "Co Fee Expense GST",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166
        },
        __self: this
      }));
    }
  }, {
    key: "renderTotal",
    value: function renderTotal() {
      var _this5 = this;

      var _this$props7 = this.props,
          form = _this$props7.form,
          model = _this$props7.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 177
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Popover"], {
        content: react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("a", {
          onClick: function onClick() {
            _this5.calcFollowerTotal();
          },
          style: {
            cursor: "pointer"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 179
          },
          __self: this
        }, "Calculate"),
        trigger: "focus",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 178
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPrice"], {
        form: form,
        model: model,
        size: "large",
        propName: this.generatePropName("totalCoFeeExpense", "leader"),
        label: "Co Fee TOTAL",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 187
        },
        __self: this
      })));
    } // getters

  }, {
    key: "mergeForCalc",
    // actions
    value: function mergeForCalc() {
      var that = this.props.that;

      var newCart = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.cloneDeep(that.props.mergePolicyToServiceModel());

      _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_13__["Apis"].CART_MERGE, newCart, {
        loading: true
      }).then(function (response) {
        that.props.mergePolicyToUIModel((response.body || {}).respData);
      }).catch(function (error) {
        console.log(error);
      });
    }
  }, {
    key: "generatePropName",
    //methods
    value: function generatePropName(propName, dataIdPrefix, rootPrefix) {
      return _desk_services_model__WEBPACK_IMPORTED_MODULE_12__["ModelSevice"].generatePropName(propName, dataIdPrefix, "policy.co");
    }
  }, {
    key: "getDataFromModel",
    value: function getDataFromModel(key) {
      return this.props.that.getValueFromModel(key);
    }
  }, {
    key: "getDataFromModelByPropName",
    value: function getDataFromModelByPropName(propName, dataIdPrefix) {
      return this.getDataFromModel(this.generatePropName(propName, dataIdPrefix));
    }
  }, {
    key: "isLeader",
    get: function get() {
      var that = this.props.that;
      if (!that) return null;

      var _isLeader = this.getDataFromModelByPropName("isLeader");

      console.log(_isLeader, "=====00d0sa0s0a");
      console.log(this.props.model, "=====00d0sa0s0a222");
      if (_isLeader === undefined || _isLeader === null) return null;
      return _isLeader === "Y";
    }
  }, {
    key: "isInvolved",
    get: function get() {
      var that = this.props.that;
      if (!that) return null;
      return that.getValueFromModel(this.generatePropName("coinsuranceInvolved")) === "Y";
    }
  }]);

  return CoInsurance;
}(react__WEBPACK_IMPORTED_MODULE_7__["Component"]);



var CoInsuranceFollowers =
/*#__PURE__*/
function (_Component2) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(CoInsuranceFollowers, _Component2);

  function CoInsuranceFollowers() {
    var _getPrototypeOf3;

    var _this6;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, CoInsuranceFollowers);

    for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      args[_key3] = arguments[_key3];
    }

    _this6 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf3 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(CoInsuranceFollowers)).call.apply(_getPrototypeOf3, [this].concat(args)));
    _this6.state = {
      editIndex: 0
    };

    _this6.toggleEdit = function (index) {
      _this6.setState({
        editIndex: index
      }, function () {});
    };

    _this6.calcPre = function (dataIndex) {
      var _income = _this6.getDataFromModel(_this6.generatePropName("coFeeExpense", "".concat(dataIndex)));

      var _gst = _this6.getDataFromModel(_this6.generatePropName("coFeeExpenseTurnoverTax", "".concat(dataIndex)));

      if (_common__WEBPACK_IMPORTED_MODULE_13__["Rules"].isNum(_income) && _common__WEBPACK_IMPORTED_MODULE_13__["Rules"].isNum(_gst)) {
        var premium = number_precision__WEBPACK_IMPORTED_MODULE_10___default.a.plus(_income, _gst);
        premium = number_precision__WEBPACK_IMPORTED_MODULE_10___default.a.round(premium, 2);

        var _propName = _this6.generatePropName("totalCoFeeExpense", "".concat(dataIndex));

        _this6.props.that.setValueToModel(premium, _propName);

        _this6.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, _propName, premium));

        var data = _common__WEBPACK_IMPORTED_MODULE_13__["Utils"].formatCurrencyInfo(premium, "");
        return "".concat(data.amount);
      }

      return "";
    };

    return _this6;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(CoInsuranceFollowers, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.tableData && this.tableData.length == 0) {
        this.handleAdd();
      }
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 272
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_claims_handing_details_SAIC_components_settlement_components_style_table__WEBPACK_IMPORTED_MODULE_15__["TableStyleForDefaultSize"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 273
        },
        __self: this
      }, this.renderTable(), this.renderFormAction()));
    }
  }, {
    key: "renderTable",
    value: function renderTable() {
      var _this7 = this;

      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Table"], {
        dataSource: this.tableData,
        pagination: false,
        scroll: {
          x: "100 %"
        },
        onRow: function onRow(record, index) {
          return {
            onClick: function onClick(event) {
              console.log(record, index);

              _this7.toggleEdit(index);
            } // 点击行

          };
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 282
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Column, {
        title: "Co-insurer",
        key: "coInsurerId",
        dataIndex: "coInsurerId",
        width: 225,
        render: function render(text, record, dataIndex) {
          return _this7.renderCoInsurer(dataIndex);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 294
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Column, {
        title: "Share",
        key: "shareRate",
        width: 110,
        className: "amount-right",
        render: function render(text, record, dataIndex) {
          return _this7.renderShare(dataIndex);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 303
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Column, {
        title: "Premium",
        key: "premium",
        className: "amount-right",
        render: function render(text, record, dataIndex) {
          return _this7.renderPremium(dataIndex);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 312
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Column, {
        title: "Co Fee Income",
        key: "coFeeExpense",
        width: 100,
        className: "amount-right",
        render: function render(text, record, dataIndex) {
          return _this7.renderIncome(dataIndex);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 321
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Column, {
        title: "Co Fee Income GST",
        key: "coFeeExpenseTurnoverTax",
        width: 90,
        className: "amount-right",
        render: function render(text, record, dataIndex) {
          return _this7.renderGst(dataIndex);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 330
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Column, {
        title: "Co Fee Total",
        key: "totalCoFeeExpense",
        width: 110,
        className: "amount-right",
        render: function render(text, record, dataIndex) {
          return _this7.renderTotal(dataIndex);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 339
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Column, {
        title: "ACTION",
        key: "age",
        className: "amount-center",
        width: 80,
        render: function render(text, record, dataIndex) {
          var actions = [{
            actionName: "Delete",
            handel: function handel() {
              // todo 验证
              var _list = _this7.tableData;
              var that = _this7.props.that;
              if (lodash__WEBPACK_IMPORTED_MODULE_8___default.a.isEmpty(_list)) return;
              if (!that) return;

              var _newList = _list.filter(function (item, index) {
                return index != dataIndex;
              });

              that.setValueToModel([], _this7.generatePropName(""));

              _newList.forEach(function (item, index) {
                that.setValueToModel(item, _this7.generatePropName("".concat(index)));
              });
            }
          }];
          return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_component_table_action_bars__WEBPACK_IMPORTED_MODULE_14__["TableActionBars"], {
            actions: actions,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 371
            },
            __self: this
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 348
        },
        __self: this
      }));
    }
  }, {
    key: "renderFormAction",
    value: function renderFormAction() {
      var _this8 = this;

      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_component_table_actions_under_table__WEBPACK_IMPORTED_MODULE_18__["TableUnderActions"], {
        actions: [{
          actionName: _common__WEBPACK_IMPORTED_MODULE_13__["Language"].en("Add").thai("Add").getMessage(),
          handel: function handel() {
            _this8.handleAdd();
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 380
        },
        __self: this
      });
    }
  }, {
    key: "renderCoInsurer",
    value: function renderCoInsurer(dataIndex) {
      var _propName = this.generatePropName("coInsurerId", "".concat(dataIndex));

      if (this.state.editIndex != dataIndex) {
        return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 394
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_component_useful_form_item_utils_master_table_item__WEBPACK_IMPORTED_MODULE_20__["IdToValueWidget"], {
          id: this.getDataFromModel(_propName),
          fetch: function fetch() {
            return _service__WEBPACK_IMPORTED_MODULE_19__["QuoteIarService"].fetchLeaderList();
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 395
          },
          __self: this
        }));
      }

      var _this$props8 = this.props,
          form = _this$props8.form,
          model = _this$props8.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_16__["StyleWithoutMarginBottom"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 400
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NSelect"], {
        style: {
          width: "100%"
        },
        form: form,
        label: "",
        model: model,
        fetchFunction: function fetchFunction() {
          return _service__WEBPACK_IMPORTED_MODULE_19__["QuoteIarService"].fetchLeaderList();
        },
        size: "default",
        required: true,
        propName: _propName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 401
        },
        __self: this
      }));
    }
  }, {
    key: "renderShare",
    value: function renderShare(dataIndex) {
      var _this9 = this;

      var _propName = this.generatePropName("shareRate", "".concat(dataIndex));

      if (this.state.editIndex != dataIndex) {
        return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
          style: {
            paddingRight: 4
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 418
          },
          __self: this
        }, _common_utils_string_utils__WEBPACK_IMPORTED_MODULE_21__["default"].toPercent(this.getDataFromModel(_propName)));
      }

      var _this$props9 = this.props,
          form = _this$props9.form,
          model = _this$props9.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_16__["StyleWithoutMarginBottom"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 423
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NRate"], {
        style: {
          width: "100%"
        },
        form: form,
        label: "",
        model: model,
        size: "default",
        required: true,
        propName: _propName,
        onBlur: function onBlur() {
          // this.mergeForCalc();
          var _locations = _this9.getDataFromModel("policy.ext.locations") || [];

          var _total = 0;

          _locations.map(function (item) {
            _total = _total + _this9.calcSinglePremium(item);
          });

          var _share = _this9.getDataFromModel(_propName);

          if (_total && _share) {
            var premium = _total * _share;

            _this9.props.that.setValueToModel(premium, _this9.generatePropName("premium", "".concat(dataIndex)));
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 424
        },
        __self: this
      }));
    }
  }, {
    key: "renderPremium",
    value: function renderPremium(dataIndex) {
      var _propName = this.generatePropName("premium", "".concat(dataIndex));

      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        style: {
          paddingRight: 4
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 453
        },
        __self: this
      }, this.formmatCurrency(this.getDataFromModel(_propName)));
    }
  }, {
    key: "calcSinglePremium",
    value: function calcSinglePremium(record) {
      var code = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(record, "location.postalCode", "");

      var breakDown = this.getDataFromModel("policy.policyPremium.insuredPremiums") || [];
      var locationPremium = breakDown.find(function (item) {
        return item.insuredKey === code;
      });
      var premium = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(locationPremium, "gwp", 0) || 0;
      return premium;
    }
  }, {
    key: "renderIncome",
    value: function renderIncome(dataIndex) {
      var _propName = this.generatePropName("coFeeExpense", "".concat(dataIndex));

      if (this.state.editIndex != dataIndex) {
        return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
          style: {
            paddingRight: 4
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 471
          },
          __self: this
        }, this.formmatCurrency(this.getDataFromModel(_propName)));
      }

      var _this$props10 = this.props,
          form = _this$props10.form,
          model = _this$props10.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_16__["StyleWithoutMarginBottom"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 476
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NNumber"], {
        style: {
          width: "100%"
        },
        precision: 2,
        form: form,
        label: "",
        model: model,
        size: "default",
        required: true,
        formatter: _app_component_NPrice__WEBPACK_IMPORTED_MODULE_17__["formatterPrice"],
        parser: _app_component_NPrice__WEBPACK_IMPORTED_MODULE_17__["parserPrice"],
        propName: _propName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 477
        },
        __self: this
      }));
    }
  }, {
    key: "renderGst",
    value: function renderGst(dataIndex) {
      var _propName = this.generatePropName("coFeeExpenseTurnoverTax", "".concat(dataIndex));

      if (this.state.editIndex != dataIndex) {
        return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
          style: {
            paddingRight: 4
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 496
          },
          __self: this
        }, this.formmatCurrency(this.getDataFromModel(_propName)));
      }

      var _this$props11 = this.props,
          form = _this$props11.form,
          model = _this$props11.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_16__["StyleWithoutMarginBottom"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 501
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NNumber"], {
        style: {
          width: "100%"
        },
        precision: 2,
        form: form,
        label: "",
        model: model,
        size: "default",
        required: true,
        formatter: _app_component_NPrice__WEBPACK_IMPORTED_MODULE_17__["formatterPrice"],
        parser: _app_component_NPrice__WEBPACK_IMPORTED_MODULE_17__["parserPrice"],
        propName: _propName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 502
        },
        __self: this
      }));
    }
  }, {
    key: "renderTotal",
    value: function renderTotal(dataIndex) {
      var _this10 = this;

      var _propName = this.generatePropName("totalCoFeeExpense", "".concat(dataIndex));

      if (this.state.editIndex != dataIndex) {
        return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
          style: {
            paddingRight: 4
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 521
          },
          __self: this
        }, this.formmatCurrency(this.getDataFromModel(_propName)));
      }

      var _this$props12 = this.props,
          form = _this$props12.form,
          model = _this$props12.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Popover"], {
        content: react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("a", {
          onClick: function onClick() {
            _this10.calcPre(dataIndex);
          },
          style: {
            cursor: "pointer"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 528
          },
          __self: this
        }, "Calculate"),
        trigger: "focus",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 527
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_16__["StyleWithoutMarginBottom"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 536
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_quote_SAIC_iar_location__WEBPACK_IMPORTED_MODULE_22__["RightDiv"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 537
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NNumber"], {
        style: {
          width: "100%"
        },
        precision: 2,
        form: form,
        label: "",
        model: model,
        size: "default",
        required: true,
        formatter: _app_component_NPrice__WEBPACK_IMPORTED_MODULE_17__["formatterPrice"],
        parser: _app_component_NPrice__WEBPACK_IMPORTED_MODULE_17__["parserPrice"],
        propName: _propName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 538
        },
        __self: this
      }))));
    } // getters

  }, {
    key: "getLocationsLs",
    value: function getLocationsLs() {} // actions

  }, {
    key: "handleAdd",
    value: function handleAdd() {
      var _tableData = [].concat(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(this.tableData), [{}]);

      this.toggleEdit(_tableData.length - 1);
      this.props.that.setValueToModel(_tableData, this.generatePropName(""));
    }
  }, {
    key: "mergeForCalc",
    value: function mergeForCalc() {
      var that = this.props.that;

      var newCart = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.cloneDeep(that.props.mergePolicyToServiceModel());

      _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_13__["Apis"].CART_MERGE, newCart, {
        loading: true
      }).then(function (response) {
        that.props.mergePolicyToUIModel((response.body || {}).respData);
      }).catch(function (error) {
        console.log(error);
      });
    } //methods

  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix, rootPrefix) {
      return _desk_services_model__WEBPACK_IMPORTED_MODULE_12__["ModelSevice"].generatePropName(propName, dataIdPrefix, "policy.co.followers");
    }
  }, {
    key: "getDataFromModel",
    value: function getDataFromModel(key) {
      return this.props.that.getValueFromModel(key);
    }
  }, {
    key: "formmatCurrency",
    value: function formmatCurrency(money) {
      var _money = parseFloat(money || 0).toFixed(2);

      return "".concat(_common__WEBPACK_IMPORTED_MODULE_13__["Utils"].toThousands(_money || 0));
    }
  }, {
    key: "tableData",
    get: function get() {
      return this.props.that.getValueFromModel(this.generatePropName("")) || [];
    }
  }]);

  return CoInsuranceFollowers;
}(react__WEBPACK_IMPORTED_MODULE_7__["Component"]);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L2NvLWluc3VyYW5jZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9TQUlDL2lhci9jb21wb25lbnQvY28taW5zdXJhbmNlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBQb3BvdmVyLCBUYWJsZSB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgTlAgZnJvbSBcIm51bWJlci1wcmVjaXNpb25cIjtcblxuaW1wb3J0IHsgTk51bWJlciwgTlByaWNlLCBOUmFkaW8sIE5SYXRlLCBOU2VsZWN0IH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgeyBNb2RlbFNldmljZSB9IGZyb20gXCJAZGVzay9zZXJ2aWNlcy9tb2RlbFwiO1xuaW1wb3J0IHsgQWpheCwgQXBpcywgQ29kZXMsIExhbmd1YWdlLCBSdWxlcywgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgSUFjdGlvbiwgVGFibGVBY3Rpb25CYXJzIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC90YWJsZS9hY3Rpb24tYmFyc1wiO1xuaW1wb3J0IHsgVGFibGVTdHlsZUZvckRlZmF1bHRTaXplIH0gZnJvbSBcIkBkZXNrL2NsYWltcy9oYW5kaW5nL2RldGFpbHMvU0FJQy9jb21wb25lbnRzL3NldHRsZW1lbnQvY29tcG9uZW50cy9zdHlsZS90YWJsZVwiO1xuaW1wb3J0IHsgU3R5bGVXaXRob3V0TWFyZ2luQm90dG9tIH0gZnJvbSBcIkBkZXNrLXN0eWxlcy9nbG9iYWxcIjtcbmltcG9ydCB7IGZvcm1hdHRlclByaWNlLCBwYXJzZXJQcmljZSB9IGZyb20gXCJAYXBwLWNvbXBvbmVudC9OUHJpY2VcIjtcbmltcG9ydCB7IFRhYmxlVW5kZXJBY3Rpb25zIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC90YWJsZS9hY3Rpb25zLXVuZGVyLXRhYmxlXCI7XG5cbmltcG9ydCB7IFF1b3RlSWFyU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlXCI7XG5pbXBvcnQgeyBJZFRvVmFsdWVXaWRnZXQgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vdXRpbHMvbWFzdGVyLXRhYmxlLWl0ZW1cIjtcbmltcG9ydCBNeVN0cmluZ1V0aWxzIGZyb20gXCJAY29tbW9uL3V0aWxzL3N0cmluZy11dGlsc1wiO1xuaW1wb3J0IHsgUmlnaHREaXYgfSBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9pYXIvbG9jYXRpb25cIjtcblxuXG5jb25zdCB7IENvbHVtbiB9ID0gVGFibGU7XG5cblxuaW50ZXJmYWNlIElQcm9wcyB7XG4gIGZvcm06IGFueSxcbiAgbW9kZWw6IGFueSxcbiAgdGhhdDogYW55LFxuICBpc0VuZG8/OiBib29sZWFuXG59XG5cbmludGVyZmFjZSBJU3RhdGUge1xuXG59XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENvSW5zdXJhbmNlIGV4dGVuZHMgQ29tcG9uZW50PElQcm9wcywgSVN0YXRlPiB7XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKTogdm9pZCB7XG4gICAgTW9kZWxTZXZpY2Uuc2V0VmFsdWVJZlVuZGVmaW5lZCh7XG4gICAgICBwcm9wc05hbWU6IHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImNvaW5zdXJhbmNlSW52b2x2ZWRcIiksXG4gICAgICB2YWx1ZTogXCJOXCIsIC8vIHRvZG8gTlxuICAgICAgdGhhdDogdGhpcy5wcm9wcy50aGF0LFxuICAgIH0pO1xuICB9XG5cbiAgLy8gcmVuZGVyc1xuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuICg8ZGl2PlxuICAgICAge3RoaXMucmVuZGVySW52b2x2ZWQoKX1cbiAgICAgIHt0aGlzLmlzSW52b2x2ZWQgJiYgdGhpcy5yZW5kZXJJbnZvbHZlZEJveCgpfVxuICAgIDwvZGl2Pik7XG4gIH1cblxuICByZW5kZXJJbnZvbHZlZCgpIHtcbiAgICBjb25zdCB7IGZvcm0sIG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiA8TlJhZGlvIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJjb2luc3VyYW5jZUludm9sdmVkXCIpfVxuICAgICAgICAgICAgICAgICAgIGxhYmVsPXtcIkNvLWluc3VyYW5jZSBJbnZvbHZlZFwifVxuICAgICAgICAgICAgICAgICAgIG9wdGlvbnM9e0NvZGVzLkJPT0xFQU5fWU59XG4gICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgIHRoaXMubWVyZ2VGb3JDYWxjKCk7XG4gICAgICAgICAgICAgICAgICAgICAvLyAgdG9kbyA/IOa4heepulxuICAgICAgICAgICAgICAgICAgICAgY29uc3QgbmV3Q28gPSBfLnBpY2sodGhpcy5nZXREYXRhRnJvbU1vZGVsQnlQcm9wTmFtZShcIlwiKSwgW1wiY29pbnN1cmFuY2VJbnZvbHZlZFwiXSk7XG4gICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnRoYXQuc2V0VmFsdWVUb01vZGVsKG5ld0NvLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJcIikpO1xuICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHsgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIlwiKV06IG5ld0NvIH0pO1xuICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5nZXREYXRhRnJvbU1vZGVsQnlQcm9wTmFtZShcIlwiKSwgXCI9PT09PT09XCIsIHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIlwiKSk7XG4gICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnByb3BzLm1vZGVsKTtcbiAgICAgICAgICAgICAgICAgICB9fVxuICAgIC8+O1xuICB9XG5cbiAgcmVuZGVySW52b2x2ZWRCb3goKSB7XG4gICAgcmV0dXJuIDxkaXY+XG4gICAgICB7dGhpcy5yZW5kZXJQZXJzb25UeXBlKCl9XG4gICAgICB7dGhpcy5yZW5kZXJTaGFyZVBlcmNlbnQoKX1cbiAgICAgIHt0aGlzLnJlbmRlclBlcnNvbkJveCgpfVxuICAgIDwvZGl2PjtcbiAgfVxuXG4gIHJlbmRlclBlcnNvbkJveCgpIHtcbiAgICBpZiAodGhpcy5pc0xlYWRlciA9PT0gbnVsbCkgcmV0dXJuIDxkaXY+PC9kaXY+O1xuICAgIGlmICh0aGlzLmlzTGVhZGVyKSByZXR1cm4gdGhpcy5yZW5kZXJMZWFkZXJCb3goKTtcbiAgICByZXR1cm4gdGhpcy5yZW5kZXJGb2xsb3dlcnNCb3goKTtcbiAgfVxuXG4gIHJlbmRlckxlYWRlckJveCgpIHtcbiAgICByZXR1cm4gPGRpdj5cbiAgICAgIHt0aGlzLnJlbmRlckZvbGxvd2VycygpfVxuICAgIDwvZGl2PjtcbiAgfVxuXG4gIHJlbmRlckZvbGxvd2Vyc0JveCgpIHtcbiAgICByZXR1cm4gPGRpdj5cbiAgICAgIHt0aGlzLnJlbmRlckxlYWRlcigpfVxuICAgICAge3RoaXMucmVuZGVyRmVlKCl9XG4gICAgICB7dGhpcy5yZW5kZXJHc3QoKX1cbiAgICAgIHt0aGlzLnJlbmRlclRvdGFsKCl9XG4gICAgPC9kaXY+O1xuICB9XG5cbiAgcmVuZGVyUGVyc29uVHlwZSgpIHtcbiAgICBjb25zdCB7IGZvcm0sIG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiA8TlNlbGVjdCBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJpc0xlYWRlclwiKX1cbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e1wiV2UgQXJlXCJ9XG4gICAgICAgICAgICAgICAgICAgIG9wdGlvbnM9e1t7IGlkOiBcIllcIiwgdGV4dDogXCJMZWFkZXJcIiB9LCB7IGlkOiBcIk5cIiwgdGV4dDogXCJGb2xsb3dlclwiIH1dfVxuICAgICAgICAgICAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgICAgICAgICAgIG9uU2VsZWN0PXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiaXNMZWFkZXJcIiksIFwiPT09PT1hPT1zIGlzaXNpaXNpc2lcIik7XG4gICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5wcm9wcy5tb2RlbCwgXCI9PT09PWE9PXNcIik7XG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgLz47XG4gIH1cblxuICByZW5kZXJTaGFyZVBlcmNlbnQoKSB7XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPGRpdj5cbiAgICAgIDxOUmF0ZSBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIm91clNoYXJlUmF0ZVwiKX1cbiAgICAgICAgICAgICBsYWJlbD17XCJPdXIgU2hhcmVcIn1cbiAgICAgICAgICAgICBvbkJsdXI9eygpID0+IHtcbiAgICAgICAgICAgICAgIHRoaXMubWVyZ2VGb3JDYWxjKCk7XG4gICAgICAgICAgICAgfX1cbiAgICAgIC8+XG4gICAgPC9kaXY+O1xuICB9XG5cbiAgcmVuZGVyRm9sbG93ZXJzKCkge1xuICAgIHJldHVybiA8ZGl2PlxuICAgICAgPENvSW5zdXJhbmNlRm9sbG93ZXJzIHsuLi50aGlzLnByb3BzfS8+XG4gICAgPC9kaXY+O1xuICB9XG5cbiAgcmVuZGVyTGVhZGVyKCkge1xuICAgIGNvbnN0IHsgZm9ybSwgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIDxOU2VsZWN0IGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImxlYWRlckluc3VyZXJJZFwiLCBcImxlYWRlclwiKX1cbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e1wiTGVhZGVyXCJ9XG4gICAgICAgICAgICAgICAgICAgIGZldGNoRnVuY3Rpb249eygpID0+IFF1b3RlSWFyU2VydmljZS5mZXRjaExlYWRlckxpc3QoKX1cbiAgICAgICAgICAgICAgICAgICAgc2l6ZT17XCJsYXJnZVwifVxuICAgICAgICAgICAgICAgICAgICBvblNlbGVjdD17KCkgPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgLz47XG4gIH1cblxuICByZW5kZXJGZWUoKSB7XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPGRpdj5cbiAgICAgIDxOUHJpY2UgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJjb0ZlZUV4cGVuc2VcIiwgXCJsZWFkZXJcIil9XG4gICAgICAgICAgICAgIGxhYmVsPXtcIkNvIEZlZSBFeHBlbnNlXCJ9XG4gICAgICAvPlxuICAgIDwvZGl2PjtcbiAgfVxuXG4gIHJlbmRlckdzdCgpIHtcbiAgICBjb25zdCB7IGZvcm0sIG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiA8ZGl2PlxuICAgICAgPE5QcmljZSBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImNvRmVlRXhwZW5zZVR1cm5vdmVyVGF4XCIsIFwibGVhZGVyXCIpfVxuICAgICAgICAgICAgICBsYWJlbD17XCJDbyBGZWUgRXhwZW5zZSBHU1RcIn1cbiAgICAgIC8+XG4gICAgPC9kaXY+O1xuICB9XG5cbiAgcmVuZGVyVG90YWwoKSB7XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPGRpdj5cbiAgICAgIDxQb3BvdmVyIGNvbnRlbnQ9e1xuICAgICAgICA8YVxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY2FsY0ZvbGxvd2VyVG90YWwoKTtcbiAgICAgICAgICB9fVxuICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICBjdXJzb3I6IFwicG9pbnRlclwiLFxuICAgICAgICAgIH19PkNhbGN1bGF0ZTwvYT5cbiAgICAgIH0gdHJpZ2dlcj1cImZvY3VzXCI+XG4gICAgICAgIDxOUHJpY2UgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgc2l6ZT17XCJsYXJnZVwifVxuICAgICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJ0b3RhbENvRmVlRXhwZW5zZVwiLCBcImxlYWRlclwiKX1cbiAgICAgICAgICAgICAgICBsYWJlbD17XCJDbyBGZWUgVE9UQUxcIn1cbiAgICAgICAgLz5cbiAgICAgIDwvUG9wb3Zlcj5cbiAgICA8L2Rpdj47XG4gIH1cblxuICAvLyBnZXR0ZXJzXG4gIGdldCBpc0xlYWRlcigpOiBib29sZWFuIHwgbnVsbCB8IGFueSB7XG4gICAgY29uc3QgdGhhdCA9IHRoaXMucHJvcHMudGhhdDtcbiAgICBpZiAoIXRoYXQpIHJldHVybiBudWxsO1xuICAgIGNvbnN0IF9pc0xlYWRlciA9IHRoaXMuZ2V0RGF0YUZyb21Nb2RlbEJ5UHJvcE5hbWUoXCJpc0xlYWRlclwiKTtcbiAgICBjb25zb2xlLmxvZyhfaXNMZWFkZXIsIFwiPT09PT0wMGQwc2EwczBhXCIpO1xuICAgIGNvbnNvbGUubG9nKHRoaXMucHJvcHMubW9kZWwsIFwiPT09PT0wMGQwc2EwczBhMjIyXCIpO1xuICAgIGlmIChfaXNMZWFkZXIgPT09IHVuZGVmaW5lZCB8fCBfaXNMZWFkZXIgPT09IG51bGwpIHJldHVybiBudWxsO1xuICAgIHJldHVybiBfaXNMZWFkZXIgPT09IFwiWVwiO1xuICB9XG5cbiAgZ2V0IGlzSW52b2x2ZWQoKTogYm9vbGVhbiB8IG51bGwge1xuICAgIGNvbnN0IHRoYXQgPSB0aGlzLnByb3BzLnRoYXQ7XG4gICAgaWYgKCF0aGF0KSByZXR1cm4gbnVsbDtcbiAgICByZXR1cm4gdGhhdC5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJjb2luc3VyYW5jZUludm9sdmVkXCIpKSA9PT0gXCJZXCI7XG4gIH1cblxuICAvLyBhY3Rpb25zXG4gIG1lcmdlRm9yQ2FsYygpIHtcbiAgICBjb25zdCB0aGF0ID0gdGhpcy5wcm9wcy50aGF0O1xuICAgIGNvbnN0IG5ld0NhcnQgPSBfLmNsb25lRGVlcCh0aGF0LnByb3BzLm1lcmdlUG9saWN5VG9TZXJ2aWNlTW9kZWwoKSk7XG4gICAgQWpheC5wb3N0KEFwaXMuQ0FSVF9NRVJHRSwgbmV3Q2FydCwgeyBsb2FkaW5nOiB0cnVlIH0pXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHRoYXQucHJvcHMubWVyZ2VQb2xpY3lUb1VJTW9kZWwoKHJlc3BvbnNlLmJvZHkgfHwge30pLnJlc3BEYXRhKTtcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG5cbiAgY2FsY0ZvbGxvd2VyVG90YWwgPSAoKSA9PiB7XG4gICAgY29uc3QgX2luY29tZSA9IHRoaXMuZ2V0RGF0YUZyb21Nb2RlbEJ5UHJvcE5hbWUoXCJjb0ZlZUV4cGVuc2VcIiwgYGxlYWRlcmApO1xuICAgIGNvbnN0IF9nc3QgPSB0aGlzLmdldERhdGFGcm9tTW9kZWxCeVByb3BOYW1lKFwiY29GZWVFeHBlbnNlVHVybm92ZXJUYXhcIiwgYGxlYWRlcmApO1xuICAgIGlmIChSdWxlcy5pc051bShfaW5jb21lKSAmJiBSdWxlcy5pc051bShfZ3N0KSkge1xuICAgICAgbGV0IHByZW1pdW0gPSBOUC5wbHVzKF9pbmNvbWUsIF9nc3QpO1xuICAgICAgcHJlbWl1bSA9IE5QLnJvdW5kKHByZW1pdW0sIDIpO1xuICAgICAgY29uc3QgX3Byb3BOYW1lID0gdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwidG90YWxDb0ZlZUV4cGVuc2VcIiwgYGxlYWRlcmApO1xuICAgICAgdGhpcy5wcm9wcy50aGF0LnNldFZhbHVlVG9Nb2RlbChwcmVtaXVtLCBfcHJvcE5hbWUpO1xuICAgICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHsgW19wcm9wTmFtZV06IHByZW1pdW0gfSk7XG4gICAgICBjb25zdCBkYXRhID0gVXRpbHMuZm9ybWF0Q3VycmVuY3lJbmZvKHByZW1pdW0sIFwiXCIpO1xuICAgICAgcmV0dXJuIGAke2RhdGEuYW1vdW50fWA7XG4gICAgfVxuICAgIHJldHVybiBcIlwiO1xuICB9O1xuXG4gIC8vbWV0aG9kc1xuICBnZW5lcmF0ZVByb3BOYW1lKHByb3BOYW1lOiBzdHJpbmcsIGRhdGFJZFByZWZpeD86IHN0cmluZywgcm9vdFByZWZpeD86IHN0cmluZyk6IHN0cmluZyB7XG4gICAgcmV0dXJuIE1vZGVsU2V2aWNlLmdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWUsIGRhdGFJZFByZWZpeCwgXCJwb2xpY3kuY29cIik7XG4gIH1cblxuICBnZXREYXRhRnJvbU1vZGVsKGtleTogc3RyaW5nKSB7XG4gICAgcmV0dXJuIHRoaXMucHJvcHMudGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChrZXkpO1xuICB9XG5cbiAgZ2V0RGF0YUZyb21Nb2RlbEJ5UHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkUHJlZml4Pzogc3RyaW5nKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0RGF0YUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWUsIGRhdGFJZFByZWZpeCkpO1xuICB9XG5cbn1cblxuXG5jbGFzcyBDb0luc3VyYW5jZUZvbGxvd2VycyBleHRlbmRzIENvbXBvbmVudDxJUHJvcHMsIHsgZWRpdEluZGV4OiBudW1iZXIgfT4ge1xuXG4gIHN0YXRlOiB7IGVkaXRJbmRleDogbnVtYmVyIH0gPSB7XG4gICAgZWRpdEluZGV4OiAwLFxuICB9O1xuXG4gIGNvbXBvbmVudERpZE1vdW50KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLnRhYmxlRGF0YSAmJiB0aGlzLnRhYmxlRGF0YS5sZW5ndGggPT0gMCkge1xuICAgICAgdGhpcy5oYW5kbGVBZGQoKTtcbiAgICB9XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIDxkaXY+XG4gICAgICA8VGFibGVTdHlsZUZvckRlZmF1bHRTaXplPlxuICAgICAgICB7dGhpcy5yZW5kZXJUYWJsZSgpfVxuICAgICAgICB7dGhpcy5yZW5kZXJGb3JtQWN0aW9uKCl9XG4gICAgICA8L1RhYmxlU3R5bGVGb3JEZWZhdWx0U2l6ZT5cbiAgICA8L2Rpdj47XG4gIH1cblxuICByZW5kZXJUYWJsZSgpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPFRhYmxlIGRhdGFTb3VyY2U9e3RoaXMudGFibGVEYXRhfVxuICAgICAgICAgICAgIHBhZ2luYXRpb249e2ZhbHNlfVxuICAgICAgICAgICAgIHNjcm9sbD17eyB4OiBcIjEwMCAlXCIgfX1cbiAgICAgICAgICAgICBvblJvdz17KHJlY29yZCwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgIG9uQ2xpY2s6IGV2ZW50ID0+IHtcbiAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZWNvcmQsIGluZGV4KTtcbiAgICAgICAgICAgICAgICAgICB0aGlzLnRvZ2dsZUVkaXQoaW5kZXgpO1xuICAgICAgICAgICAgICAgICB9LCAvLyDngrnlh7vooYxcbiAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgfX1cbiAgICAgID5cbiAgICAgICAgPENvbHVtblxuICAgICAgICAgIHRpdGxlPVwiQ28taW5zdXJlclwiXG4gICAgICAgICAga2V5PVwiY29JbnN1cmVySWRcIlxuICAgICAgICAgIGRhdGFJbmRleD17XCJjb0luc3VyZXJJZFwifVxuICAgICAgICAgIHdpZHRoPXsyMjV9XG4gICAgICAgICAgcmVuZGVyPXsodGV4dCwgcmVjb3JkOiBhbnksIGRhdGFJbmRleCkgPT5cbiAgICAgICAgICAgIHRoaXMucmVuZGVyQ29JbnN1cmVyKGRhdGFJbmRleClcbiAgICAgICAgICB9XG4gICAgICAgIC8+XG4gICAgICAgIDxDb2x1bW5cbiAgICAgICAgICB0aXRsZT1cIlNoYXJlXCJcbiAgICAgICAgICBrZXk9XCJzaGFyZVJhdGVcIlxuICAgICAgICAgIHdpZHRoPXsxMTB9XG4gICAgICAgICAgY2xhc3NOYW1lPVwiYW1vdW50LXJpZ2h0XCJcbiAgICAgICAgICByZW5kZXI9eyh0ZXh0LCByZWNvcmQ6IGFueSwgZGF0YUluZGV4KSA9PlxuICAgICAgICAgICAgdGhpcy5yZW5kZXJTaGFyZShkYXRhSW5kZXgpXG4gICAgICAgICAgfVxuICAgICAgICAvPlxuICAgICAgICA8Q29sdW1uXG4gICAgICAgICAgdGl0bGU9XCJQcmVtaXVtXCJcbiAgICAgICAgICBrZXk9XCJwcmVtaXVtXCJcbiAgICAgICAgICBjbGFzc05hbWU9XCJhbW91bnQtcmlnaHRcIlxuICAgICAgICAgIHJlbmRlcj17KHRleHQsIHJlY29yZDogYW55LCBkYXRhSW5kZXgpID0+XG4gICAgICAgICAgICB0aGlzLnJlbmRlclByZW1pdW0oZGF0YUluZGV4KVxuICAgICAgICAgIH1cbiAgICAgICAgLz5cblxuICAgICAgICA8Q29sdW1uXG4gICAgICAgICAgdGl0bGU9XCJDbyBGZWUgSW5jb21lXCJcbiAgICAgICAgICBrZXk9XCJjb0ZlZUV4cGVuc2VcIlxuICAgICAgICAgIHdpZHRoPXsxMDB9XG4gICAgICAgICAgY2xhc3NOYW1lPVwiYW1vdW50LXJpZ2h0XCJcbiAgICAgICAgICByZW5kZXI9eyh0ZXh0LCByZWNvcmQ6IGFueSwgZGF0YUluZGV4KSA9PlxuICAgICAgICAgICAgdGhpcy5yZW5kZXJJbmNvbWUoZGF0YUluZGV4KVxuICAgICAgICAgIH1cbiAgICAgICAgLz5cbiAgICAgICAgPENvbHVtblxuICAgICAgICAgIHRpdGxlPVwiQ28gRmVlIEluY29tZSBHU1RcIlxuICAgICAgICAgIGtleT1cImNvRmVlRXhwZW5zZVR1cm5vdmVyVGF4XCJcbiAgICAgICAgICB3aWR0aD17OTB9XG4gICAgICAgICAgY2xhc3NOYW1lPVwiYW1vdW50LXJpZ2h0XCJcbiAgICAgICAgICByZW5kZXI9eyh0ZXh0LCByZWNvcmQ6IGFueSwgZGF0YUluZGV4KSA9PlxuICAgICAgICAgICAgdGhpcy5yZW5kZXJHc3QoZGF0YUluZGV4KVxuICAgICAgICAgIH1cbiAgICAgICAgLz5cbiAgICAgICAgPENvbHVtblxuICAgICAgICAgIHRpdGxlPVwiQ28gRmVlIFRvdGFsXCJcbiAgICAgICAgICBrZXk9XCJ0b3RhbENvRmVlRXhwZW5zZVwiXG4gICAgICAgICAgd2lkdGg9ezExMH1cbiAgICAgICAgICBjbGFzc05hbWU9XCJhbW91bnQtcmlnaHRcIlxuICAgICAgICAgIHJlbmRlcj17KHRleHQsIHJlY29yZDogYW55LCBkYXRhSW5kZXgpID0+XG4gICAgICAgICAgICB0aGlzLnJlbmRlclRvdGFsKGRhdGFJbmRleClcbiAgICAgICAgICB9XG4gICAgICAgIC8+XG4gICAgICAgIDxDb2x1bW5cbiAgICAgICAgICB0aXRsZT1cIkFDVElPTlwiXG4gICAgICAgICAga2V5PVwiYWdlXCJcbiAgICAgICAgICBjbGFzc05hbWU9XCJhbW91bnQtY2VudGVyXCJcbiAgICAgICAgICB3aWR0aD17ODB9XG4gICAgICAgICAgcmVuZGVyPXsodGV4dCwgcmVjb3JkOiBhbnksIGRhdGFJbmRleCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgYWN0aW9uczogSUFjdGlvbltdID0gW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgYWN0aW9uTmFtZTogXCJEZWxldGVcIixcbiAgICAgICAgICAgICAgICBoYW5kZWw6ICgpID0+IHsgLy8gdG9kbyDpqozor4FcbiAgICAgICAgICAgICAgICAgIGNvbnN0IF9saXN0ID0gdGhpcy50YWJsZURhdGE7XG4gICAgICAgICAgICAgICAgICBjb25zdCB0aGF0ID0gdGhpcy5wcm9wcy50aGF0O1xuXG4gICAgICAgICAgICAgICAgICBpZiAoXy5pc0VtcHR5KF9saXN0KSkgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgaWYgKCF0aGF0KSByZXR1cm47XG4gICAgICAgICAgICAgICAgICBjb25zdCBfbmV3TGlzdCA9IF9saXN0LmZpbHRlcigoaXRlbTogYW55LCBpbmRleDogbnVtYmVyKSA9PiBpbmRleCAhPSBkYXRhSW5kZXgpO1xuICAgICAgICAgICAgICAgICAgdGhhdC5zZXRWYWx1ZVRvTW9kZWwoW10sIHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIlwiKSk7XG4gICAgICAgICAgICAgICAgICBfbmV3TGlzdC5mb3JFYWNoKChpdGVtOiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhhdC5zZXRWYWx1ZVRvTW9kZWwoaXRlbSwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKGAke2luZGV4fWApKTtcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdO1xuICAgICAgICAgICAgcmV0dXJuIDxUYWJsZUFjdGlvbkJhcnMgYWN0aW9ucz17YWN0aW9uc30vPjtcbiAgICAgICAgICB9fVxuICAgICAgICAvPlxuICAgICAgPC9UYWJsZT5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyRm9ybUFjdGlvbigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPFRhYmxlVW5kZXJBY3Rpb25zIGFjdGlvbnM9e1tcbiAgICAgICAge1xuICAgICAgICAgIGFjdGlvbk5hbWU6IExhbmd1YWdlLmVuKFwiQWRkXCIpLnRoYWkoXCJBZGRcIikuZ2V0TWVzc2FnZSgpLFxuICAgICAgICAgIGhhbmRlbDogKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5oYW5kbGVBZGQoKTtcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgXX0vPlxuICAgICk7XG4gIH1cblxuICByZW5kZXJDb0luc3VyZXIoZGF0YUluZGV4OiBudW1iZXIpIHtcbiAgICBjb25zdCBfcHJvcE5hbWUgPSB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJjb0luc3VyZXJJZFwiLCBgJHtkYXRhSW5kZXh9YCk7XG4gICAgaWYgKHRoaXMuc3RhdGUuZWRpdEluZGV4ICE9IGRhdGFJbmRleCkge1xuICAgICAgcmV0dXJuIDxkaXY+XG4gICAgICAgIDxJZFRvVmFsdWVXaWRnZXQgaWQ9e3RoaXMuZ2V0RGF0YUZyb21Nb2RlbChfcHJvcE5hbWUpfSBmZXRjaD17KCkgPT4gUXVvdGVJYXJTZXJ2aWNlLmZldGNoTGVhZGVyTGlzdCgpfS8+XG4gICAgICA8L2Rpdj47XG4gICAgfVxuICAgIGNvbnN0IHsgZm9ybSwgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxTdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgICAgIDxOU2VsZWN0XG4gICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiMTAwJVwiIH19XG4gICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICBsYWJlbD17XCJcIn1cbiAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgZmV0Y2hGdW5jdGlvbj17KCkgPT4gUXVvdGVJYXJTZXJ2aWNlLmZldGNoTGVhZGVyTGlzdCgpfVxuICAgICAgICAgIHNpemU9XCJkZWZhdWx0XCJcbiAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICBwcm9wTmFtZT17X3Byb3BOYW1lfVxuICAgICAgICAvPlxuICAgICAgPC9TdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgKTtcbiAgfVxuXG4gIHJlbmRlclNoYXJlKGRhdGFJbmRleDogbnVtYmVyKSB7XG4gICAgY29uc3QgX3Byb3BOYW1lID0gdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwic2hhcmVSYXRlXCIsIGAke2RhdGFJbmRleH1gKTtcbiAgICBpZiAodGhpcy5zdGF0ZS5lZGl0SW5kZXggIT0gZGF0YUluZGV4KSB7XG4gICAgICByZXR1cm4gPGRpdlxuICAgICAgICBzdHlsZT17eyBwYWRkaW5nUmlnaHQ6IDQgfX0+e015U3RyaW5nVXRpbHMudG9QZXJjZW50KHRoaXMuZ2V0RGF0YUZyb21Nb2RlbChfcHJvcE5hbWUpKX08L2Rpdj47XG4gICAgfVxuICAgIGNvbnN0IHsgZm9ybSwgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxTdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgICAgIDxOUmF0ZVxuICAgICAgICAgIHN0eWxlPXt7IHdpZHRoOiBcIjEwMCVcIiB9fVxuICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgbGFiZWw9e1wiXCJ9XG4gICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgIHNpemU9XCJkZWZhdWx0XCJcbiAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICBwcm9wTmFtZT17X3Byb3BOYW1lfVxuICAgICAgICAgIG9uQmx1cj17KCkgPT4ge1xuICAgICAgICAgICAgLy8gdGhpcy5tZXJnZUZvckNhbGMoKTtcbiAgICAgICAgICAgIGNvbnN0IF9sb2NhdGlvbnMgPSB0aGlzLmdldERhdGFGcm9tTW9kZWwoXCJwb2xpY3kuZXh0LmxvY2F0aW9uc1wiKSB8fCBbXTtcbiAgICAgICAgICAgIGxldCBfdG90YWwgPSAwO1xuICAgICAgICAgICAgX2xvY2F0aW9ucy5tYXAoKGl0ZW06IGFueSkgPT4ge1xuICAgICAgICAgICAgICBfdG90YWwgPSBfdG90YWwgKyB0aGlzLmNhbGNTaW5nbGVQcmVtaXVtKGl0ZW0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCBfc2hhcmUgPSB0aGlzLmdldERhdGFGcm9tTW9kZWwoX3Byb3BOYW1lKTtcbiAgICAgICAgICAgIGlmIChfdG90YWwgJiYgX3NoYXJlKSB7XG4gICAgICAgICAgICAgIGNvbnN0IHByZW1pdW0gPSBfdG90YWwgKiBfc2hhcmU7XG4gICAgICAgICAgICAgIHRoaXMucHJvcHMudGhhdC5zZXRWYWx1ZVRvTW9kZWwocHJlbWl1bSwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicHJlbWl1bVwiLCBgJHtkYXRhSW5kZXh9YCkpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgfX1cbiAgICAgICAgLz5cbiAgICAgIDwvU3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPlxuICAgICk7XG4gIH1cblxuICByZW5kZXJQcmVtaXVtKGRhdGFJbmRleDogbnVtYmVyKSB7XG4gICAgY29uc3QgX3Byb3BOYW1lID0gdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicHJlbWl1bVwiLCBgJHtkYXRhSW5kZXh9YCk7XG4gICAgcmV0dXJuIDxkaXZcbiAgICAgIHN0eWxlPXt7IHBhZGRpbmdSaWdodDogNCB9fT57dGhpcy5mb3JtbWF0Q3VycmVuY3kodGhpcy5nZXREYXRhRnJvbU1vZGVsKF9wcm9wTmFtZSkpfTwvZGl2PjtcbiAgfVxuXG5cbiAgY2FsY1NpbmdsZVByZW1pdW0ocmVjb3JkOiBhbnkpIHtcbiAgICBjb25zdCBjb2RlID0gXy5nZXQocmVjb3JkLCBcImxvY2F0aW9uLnBvc3RhbENvZGVcIiwgXCJcIik7XG4gICAgY29uc3QgYnJlYWtEb3duID0gdGhpcy5nZXREYXRhRnJvbU1vZGVsKFwicG9saWN5LnBvbGljeVByZW1pdW0uaW5zdXJlZFByZW1pdW1zXCIpIHx8IFtdO1xuICAgIGNvbnN0IGxvY2F0aW9uUHJlbWl1bSA9IGJyZWFrRG93bi5maW5kKChpdGVtOiBhbnkpID0+IHtcbiAgICAgIHJldHVybiBpdGVtLmluc3VyZWRLZXkgPT09IGNvZGU7XG4gICAgfSk7XG4gICAgY29uc3QgcHJlbWl1bSA9IF8uZ2V0KGxvY2F0aW9uUHJlbWl1bSwgXCJnd3BcIiwgMCkgfHwgMDtcbiAgICByZXR1cm4gcHJlbWl1bTtcbiAgfVxuXG4gIHJlbmRlckluY29tZShkYXRhSW5kZXg6IG51bWJlcikge1xuICAgIGNvbnN0IF9wcm9wTmFtZSA9IHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImNvRmVlRXhwZW5zZVwiLCBgJHtkYXRhSW5kZXh9YCk7XG4gICAgaWYgKHRoaXMuc3RhdGUuZWRpdEluZGV4ICE9IGRhdGFJbmRleCkge1xuICAgICAgcmV0dXJuIDxkaXZcbiAgICAgICAgc3R5bGU9e3sgcGFkZGluZ1JpZ2h0OiA0IH19Pnt0aGlzLmZvcm1tYXRDdXJyZW5jeSh0aGlzLmdldERhdGFGcm9tTW9kZWwoX3Byb3BOYW1lKSl9PC9kaXY+O1xuICAgIH1cbiAgICBjb25zdCB7IGZvcm0sIG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8U3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPlxuICAgICAgICA8Tk51bWJlclxuICAgICAgICAgIHN0eWxlPXt7IHdpZHRoOiBcIjEwMCVcIiB9fVxuICAgICAgICAgIHByZWNpc2lvbj17Mn1cbiAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgIGxhYmVsPXtcIlwifVxuICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICBzaXplPVwiZGVmYXVsdFwiXG4gICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgZm9ybWF0dGVyPXtmb3JtYXR0ZXJQcmljZX1cbiAgICAgICAgICBwYXJzZXI9e3BhcnNlclByaWNlfVxuICAgICAgICAgIHByb3BOYW1lPXtfcHJvcE5hbWV9XG4gICAgICAgIC8+XG4gICAgICA8L1N0eWxlV2l0aG91dE1hcmdpbkJvdHRvbT5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyR3N0KGRhdGFJbmRleDogbnVtYmVyKSB7XG4gICAgY29uc3QgX3Byb3BOYW1lID0gdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY29GZWVFeHBlbnNlVHVybm92ZXJUYXhcIiwgYCR7ZGF0YUluZGV4fWApO1xuICAgIGlmICh0aGlzLnN0YXRlLmVkaXRJbmRleCAhPSBkYXRhSW5kZXgpIHtcbiAgICAgIHJldHVybiA8ZGl2XG4gICAgICAgIHN0eWxlPXt7IHBhZGRpbmdSaWdodDogNCB9fT57dGhpcy5mb3JtbWF0Q3VycmVuY3kodGhpcy5nZXREYXRhRnJvbU1vZGVsKF9wcm9wTmFtZSkpfTwvZGl2PjtcbiAgICB9XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gKFxuICAgICAgPFN0eWxlV2l0aG91dE1hcmdpbkJvdHRvbT5cbiAgICAgICAgPE5OdW1iZXJcbiAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIgfX1cbiAgICAgICAgICBwcmVjaXNpb249ezJ9XG4gICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICBsYWJlbD17XCJcIn1cbiAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgc2l6ZT1cImRlZmF1bHRcIlxuICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgIGZvcm1hdHRlcj17Zm9ybWF0dGVyUHJpY2V9XG4gICAgICAgICAgcGFyc2VyPXtwYXJzZXJQcmljZX1cbiAgICAgICAgICBwcm9wTmFtZT17X3Byb3BOYW1lfVxuICAgICAgICAvPlxuICAgICAgPC9TdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgKTtcbiAgfVxuXG4gIHJlbmRlclRvdGFsKGRhdGFJbmRleDogbnVtYmVyKSB7XG4gICAgY29uc3QgX3Byb3BOYW1lID0gdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwidG90YWxDb0ZlZUV4cGVuc2VcIiwgYCR7ZGF0YUluZGV4fWApO1xuICAgIGlmICh0aGlzLnN0YXRlLmVkaXRJbmRleCAhPSBkYXRhSW5kZXgpIHtcbiAgICAgIHJldHVybiA8ZGl2XG4gICAgICAgIHN0eWxlPXt7IHBhZGRpbmdSaWdodDogNCB9fT57dGhpcy5mb3JtbWF0Q3VycmVuY3kodGhpcy5nZXREYXRhRnJvbU1vZGVsKF9wcm9wTmFtZSkpfTwvZGl2PjtcbiAgICB9XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcblxuICAgIHJldHVybiAoXG4gICAgICA8UG9wb3ZlciBjb250ZW50PXtcbiAgICAgICAgPGFcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNhbGNQcmUoZGF0YUluZGV4KTtcbiAgICAgICAgICB9fVxuICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICBjdXJzb3I6IFwicG9pbnRlclwiLFxuICAgICAgICAgIH19PkNhbGN1bGF0ZTwvYT5cbiAgICAgIH0gdHJpZ2dlcj1cImZvY3VzXCI+XG4gICAgICAgIDxTdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgICAgICAgPFJpZ2h0RGl2PlxuICAgICAgICAgICAgPE5OdW1iZXJcbiAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiMTAwJVwiIH19XG4gICAgICAgICAgICAgIHByZWNpc2lvbj17Mn1cbiAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgbGFiZWw9e1wiXCJ9XG4gICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgc2l6ZT1cImRlZmF1bHRcIlxuICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgZm9ybWF0dGVyPXtmb3JtYXR0ZXJQcmljZX1cbiAgICAgICAgICAgICAgcGFyc2VyPXtwYXJzZXJQcmljZX1cbiAgICAgICAgICAgICAgcHJvcE5hbWU9e19wcm9wTmFtZX1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9SaWdodERpdj5cbiAgICAgICAgPC9TdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgICA8L1BvcG92ZXI+XG4gICAgKTtcbiAgfVxuXG4gIC8vIGdldHRlcnNcbiAgZ2V0IHRhYmxlRGF0YSgpIHtcbiAgICByZXR1cm4gdGhpcy5wcm9wcy50aGF0LmdldFZhbHVlRnJvbU1vZGVsKCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJcIikpKSB8fCBbXTtcbiAgfVxuXG4gIGdldExvY2F0aW9uc0xzKCkge1xuXG4gIH1cblxuICAvLyBhY3Rpb25zXG4gIHRvZ2dsZUVkaXQgPSAoaW5kZXg6IG51bWJlcikgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBlZGl0SW5kZXg6IGluZGV4IH0sICgpID0+IHtcblxuICAgIH0pO1xuICB9O1xuXG4gIGhhbmRsZUFkZCgpIHtcbiAgICBjb25zdCBfdGFibGVEYXRhID0gWy4uLnRoaXMudGFibGVEYXRhLCB7fV07XG4gICAgdGhpcy50b2dnbGVFZGl0KF90YWJsZURhdGEubGVuZ3RoIC0gMSk7XG4gICAgdGhpcy5wcm9wcy50aGF0LnNldFZhbHVlVG9Nb2RlbChfdGFibGVEYXRhLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJcIikpO1xuICB9XG5cbiAgbWVyZ2VGb3JDYWxjKCkge1xuICAgIGNvbnN0IHRoYXQgPSB0aGlzLnByb3BzLnRoYXQ7XG4gICAgY29uc3QgbmV3Q2FydCA9IF8uY2xvbmVEZWVwKHRoYXQucHJvcHMubWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbCgpKTtcbiAgICBBamF4LnBvc3QoQXBpcy5DQVJUX01FUkdFLCBuZXdDYXJ0LCB7IGxvYWRpbmc6IHRydWUgfSlcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhhdC5wcm9wcy5tZXJnZVBvbGljeVRvVUlNb2RlbCgocmVzcG9uc2UuYm9keSB8fCB7fSkucmVzcERhdGEpO1xuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cblxuICAvL21ldGhvZHNcbiAgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZTogc3RyaW5nLCBkYXRhSWRQcmVmaXg/OiBzdHJpbmcsIHJvb3RQcmVmaXg/OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIHJldHVybiBNb2RlbFNldmljZS5nZW5lcmF0ZVByb3BOYW1lKHByb3BOYW1lLCBkYXRhSWRQcmVmaXgsIFwicG9saWN5LmNvLmZvbGxvd2Vyc1wiKTtcbiAgfVxuXG4gIGdldERhdGFGcm9tTW9kZWwoa2V5OiBzdHJpbmcpIHtcbiAgICByZXR1cm4gdGhpcy5wcm9wcy50aGF0LmdldFZhbHVlRnJvbU1vZGVsKGtleSk7XG4gIH1cblxuICBmb3JtbWF0Q3VycmVuY3kobW9uZXk6IGFueSkge1xuICAgIGNvbnN0IF9tb25leSA9IHBhcnNlRmxvYXQobW9uZXkgfHwgMCkudG9GaXhlZCgyKTtcbiAgICByZXR1cm4gYCR7VXRpbHMudG9UaG91c2FuZHMoX21vbmV5IHx8IDApfWA7XG4gIH1cblxuICBjYWxjUHJlID0gKGRhdGFJbmRleDogYW55KSA9PiB7XG4gICAgY29uc3QgX2luY29tZSA9IHRoaXMuZ2V0RGF0YUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJjb0ZlZUV4cGVuc2VcIiwgYCR7ZGF0YUluZGV4fWApKTtcbiAgICBjb25zdCBfZ3N0ID0gdGhpcy5nZXREYXRhRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImNvRmVlRXhwZW5zZVR1cm5vdmVyVGF4XCIsIGAke2RhdGFJbmRleH1gKSk7XG4gICAgaWYgKFJ1bGVzLmlzTnVtKF9pbmNvbWUpICYmIFJ1bGVzLmlzTnVtKF9nc3QpKSB7XG4gICAgICBsZXQgcHJlbWl1bSA9IE5QLnBsdXMoX2luY29tZSwgX2dzdCk7XG4gICAgICBwcmVtaXVtID0gTlAucm91bmQocHJlbWl1bSwgMik7XG4gICAgICBjb25zdCBfcHJvcE5hbWUgPSB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJ0b3RhbENvRmVlRXhwZW5zZVwiLCBgJHtkYXRhSW5kZXh9YCk7XG4gICAgICB0aGlzLnByb3BzLnRoYXQuc2V0VmFsdWVUb01vZGVsKHByZW1pdW0sIF9wcm9wTmFtZSk7XG4gICAgICB0aGlzLnByb3BzLmZvcm0uc2V0RmllbGRzVmFsdWUoeyBbX3Byb3BOYW1lXTogcHJlbWl1bSB9KTtcbiAgICAgIGNvbnN0IGRhdGEgPSBVdGlscy5mb3JtYXRDdXJyZW5jeUluZm8ocHJlbWl1bSwgXCJcIik7XG4gICAgICByZXR1cm4gYCR7ZGF0YS5hbW91bnR9YDtcbiAgICB9XG4gICAgcmV0dXJuIFwiXCI7XG4gIH07XG5cblxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFhQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaU1BO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7Ozs7QUE3TUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBSEE7QUFLQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7O0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQXhEQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBakxBO0FBQ0E7QUFEQTtBQUNBO0FBK05BOzs7Ozs7Ozs7Ozs7Ozs7OztBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBZ1RBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQWtDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7Ozs7O0FBbFdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUxBO0FBTUE7QUFWQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFnQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBeEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTRCQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQVBBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQU9BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBckJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXlCQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTs7O0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFjQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFnQkE7QUFDQTs7O0FBTUE7QUFDQTs7O0FBVUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBOUNBO0FBQ0E7QUFDQTs7OztBQTVTQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/component/co-insurance.tsx
