__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return scrollTo; });
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raf */ "./node_modules/raf/index.js");
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(raf__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _getScroll__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./getScroll */ "./node_modules/antd/es/_util/getScroll.js");
/* harmony import */ var _easings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./easings */ "./node_modules/antd/es/_util/easings.js");



function scrollTo(y) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var _options$getContainer = options.getContainer,
      getContainer = _options$getContainer === void 0 ? function () {
    return window;
  } : _options$getContainer,
      callback = options.callback,
      _options$duration = options.duration,
      duration = _options$duration === void 0 ? 450 : _options$duration;
  var container = getContainer();
  var scrollTop = Object(_getScroll__WEBPACK_IMPORTED_MODULE_1__["default"])(container, true);
  var startTime = Date.now();

  var frameFunc = function frameFunc() {
    var timestamp = Date.now();
    var time = timestamp - startTime;
    var nextScrollTop = Object(_easings__WEBPACK_IMPORTED_MODULE_2__["easeInOutCubic"])(time > duration ? duration : time, scrollTop, y, duration);

    if (container === window) {
      window.scrollTo(window.pageXOffset, nextScrollTop);
    } else {
      container.scrollTop = nextScrollTop;
    }

    if (time < duration) {
      raf__WEBPACK_IMPORTED_MODULE_0___default()(frameFunc);
    } else if (typeof callback === 'function') {
      callback();
    }
  };

  raf__WEBPACK_IMPORTED_MODULE_0___default()(frameFunc);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9fdXRpbC9zY3JvbGxUby5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvX3V0aWwvc2Nyb2xsVG8uanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHJhZiBmcm9tICdyYWYnO1xuaW1wb3J0IGdldFNjcm9sbCBmcm9tICcuL2dldFNjcm9sbCc7XG5pbXBvcnQgeyBlYXNlSW5PdXRDdWJpYyB9IGZyb20gJy4vZWFzaW5ncyc7XG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBzY3JvbGxUbyh5LCBvcHRpb25zID0ge30pIHtcbiAgICBjb25zdCB7IGdldENvbnRhaW5lciA9ICgpID0+IHdpbmRvdywgY2FsbGJhY2ssIGR1cmF0aW9uID0gNDUwIH0gPSBvcHRpb25zO1xuICAgIGNvbnN0IGNvbnRhaW5lciA9IGdldENvbnRhaW5lcigpO1xuICAgIGNvbnN0IHNjcm9sbFRvcCA9IGdldFNjcm9sbChjb250YWluZXIsIHRydWUpO1xuICAgIGNvbnN0IHN0YXJ0VGltZSA9IERhdGUubm93KCk7XG4gICAgY29uc3QgZnJhbWVGdW5jID0gKCkgPT4ge1xuICAgICAgICBjb25zdCB0aW1lc3RhbXAgPSBEYXRlLm5vdygpO1xuICAgICAgICBjb25zdCB0aW1lID0gdGltZXN0YW1wIC0gc3RhcnRUaW1lO1xuICAgICAgICBjb25zdCBuZXh0U2Nyb2xsVG9wID0gZWFzZUluT3V0Q3ViaWModGltZSA+IGR1cmF0aW9uID8gZHVyYXRpb24gOiB0aW1lLCBzY3JvbGxUb3AsIHksIGR1cmF0aW9uKTtcbiAgICAgICAgaWYgKGNvbnRhaW5lciA9PT0gd2luZG93KSB7XG4gICAgICAgICAgICB3aW5kb3cuc2Nyb2xsVG8od2luZG93LnBhZ2VYT2Zmc2V0LCBuZXh0U2Nyb2xsVG9wKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNvbnRhaW5lci5zY3JvbGxUb3AgPSBuZXh0U2Nyb2xsVG9wO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aW1lIDwgZHVyYXRpb24pIHtcbiAgICAgICAgICAgIHJhZihmcmFtZUZ1bmMpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHR5cGVvZiBjYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgcmFmKGZyYW1lRnVuYyk7XG59XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFmQTtBQUNBO0FBZ0JBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/_util/scrollTo.js
