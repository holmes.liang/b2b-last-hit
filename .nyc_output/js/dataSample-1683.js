/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var samplers = {
  average: function average(frame) {
    var sum = 0;
    var count = 0;

    for (var i = 0; i < frame.length; i++) {
      if (!isNaN(frame[i])) {
        sum += frame[i];
        count++;
      }
    } // Return NaN if count is 0


    return count === 0 ? NaN : sum / count;
  },
  sum: function sum(frame) {
    var sum = 0;

    for (var i = 0; i < frame.length; i++) {
      // Ignore NaN
      sum += frame[i] || 0;
    }

    return sum;
  },
  max: function max(frame) {
    var max = -Infinity;

    for (var i = 0; i < frame.length; i++) {
      frame[i] > max && (max = frame[i]);
    } // NaN will cause illegal axis extent.


    return isFinite(max) ? max : NaN;
  },
  min: function min(frame) {
    var min = Infinity;

    for (var i = 0; i < frame.length; i++) {
      frame[i] < min && (min = frame[i]);
    } // NaN will cause illegal axis extent.


    return isFinite(min) ? min : NaN;
  },
  // TODO
  // Median
  nearest: function nearest(frame) {
    return frame[0];
  }
};

var indexSampler = function indexSampler(frame, value) {
  return Math.round(frame.length / 2);
};

function _default(seriesType) {
  return {
    seriesType: seriesType,
    modifyOutputEnd: true,
    reset: function reset(seriesModel, ecModel, api) {
      var data = seriesModel.getData();
      var sampling = seriesModel.get('sampling');
      var coordSys = seriesModel.coordinateSystem; // Only cartesian2d support down sampling

      if (coordSys.type === 'cartesian2d' && sampling) {
        var baseAxis = coordSys.getBaseAxis();
        var valueAxis = coordSys.getOtherAxis(baseAxis);
        var extent = baseAxis.getExtent(); // Coordinste system has been resized

        var size = extent[1] - extent[0];
        var rate = Math.round(data.count() / size);

        if (rate > 1) {
          var sampler;

          if (typeof sampling === 'string') {
            sampler = samplers[sampling];
          } else if (typeof sampling === 'function') {
            sampler = sampling;
          }

          if (sampler) {
            // Only support sample the first dim mapped from value axis.
            seriesModel.setData(data.downSample(data.mapDimension(valueAxis.dim), 1 / rate, sampler, indexSampler));
          }
        }
      }
    }
  };
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvcHJvY2Vzc29yL2RhdGFTYW1wbGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9wcm9jZXNzb3IvZGF0YVNhbXBsZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIHNhbXBsZXJzID0ge1xuICBhdmVyYWdlOiBmdW5jdGlvbiAoZnJhbWUpIHtcbiAgICB2YXIgc3VtID0gMDtcbiAgICB2YXIgY291bnQgPSAwO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmcmFtZS5sZW5ndGg7IGkrKykge1xuICAgICAgaWYgKCFpc05hTihmcmFtZVtpXSkpIHtcbiAgICAgICAgc3VtICs9IGZyYW1lW2ldO1xuICAgICAgICBjb3VudCsrO1xuICAgICAgfVxuICAgIH0gLy8gUmV0dXJuIE5hTiBpZiBjb3VudCBpcyAwXG5cblxuICAgIHJldHVybiBjb3VudCA9PT0gMCA/IE5hTiA6IHN1bSAvIGNvdW50O1xuICB9LFxuICBzdW06IGZ1bmN0aW9uIChmcmFtZSkge1xuICAgIHZhciBzdW0gPSAwO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmcmFtZS5sZW5ndGg7IGkrKykge1xuICAgICAgLy8gSWdub3JlIE5hTlxuICAgICAgc3VtICs9IGZyYW1lW2ldIHx8IDA7XG4gICAgfVxuXG4gICAgcmV0dXJuIHN1bTtcbiAgfSxcbiAgbWF4OiBmdW5jdGlvbiAoZnJhbWUpIHtcbiAgICB2YXIgbWF4ID0gLUluZmluaXR5O1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmcmFtZS5sZW5ndGg7IGkrKykge1xuICAgICAgZnJhbWVbaV0gPiBtYXggJiYgKG1heCA9IGZyYW1lW2ldKTtcbiAgICB9IC8vIE5hTiB3aWxsIGNhdXNlIGlsbGVnYWwgYXhpcyBleHRlbnQuXG5cblxuICAgIHJldHVybiBpc0Zpbml0ZShtYXgpID8gbWF4IDogTmFOO1xuICB9LFxuICBtaW46IGZ1bmN0aW9uIChmcmFtZSkge1xuICAgIHZhciBtaW4gPSBJbmZpbml0eTtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZnJhbWUubGVuZ3RoOyBpKyspIHtcbiAgICAgIGZyYW1lW2ldIDwgbWluICYmIChtaW4gPSBmcmFtZVtpXSk7XG4gICAgfSAvLyBOYU4gd2lsbCBjYXVzZSBpbGxlZ2FsIGF4aXMgZXh0ZW50LlxuXG5cbiAgICByZXR1cm4gaXNGaW5pdGUobWluKSA/IG1pbiA6IE5hTjtcbiAgfSxcbiAgLy8gVE9ET1xuICAvLyBNZWRpYW5cbiAgbmVhcmVzdDogZnVuY3Rpb24gKGZyYW1lKSB7XG4gICAgcmV0dXJuIGZyYW1lWzBdO1xuICB9XG59O1xuXG52YXIgaW5kZXhTYW1wbGVyID0gZnVuY3Rpb24gKGZyYW1lLCB2YWx1ZSkge1xuICByZXR1cm4gTWF0aC5yb3VuZChmcmFtZS5sZW5ndGggLyAyKTtcbn07XG5cbmZ1bmN0aW9uIF9kZWZhdWx0KHNlcmllc1R5cGUpIHtcbiAgcmV0dXJuIHtcbiAgICBzZXJpZXNUeXBlOiBzZXJpZXNUeXBlLFxuICAgIG1vZGlmeU91dHB1dEVuZDogdHJ1ZSxcbiAgICByZXNldDogZnVuY3Rpb24gKHNlcmllc01vZGVsLCBlY01vZGVsLCBhcGkpIHtcbiAgICAgIHZhciBkYXRhID0gc2VyaWVzTW9kZWwuZ2V0RGF0YSgpO1xuICAgICAgdmFyIHNhbXBsaW5nID0gc2VyaWVzTW9kZWwuZ2V0KCdzYW1wbGluZycpO1xuICAgICAgdmFyIGNvb3JkU3lzID0gc2VyaWVzTW9kZWwuY29vcmRpbmF0ZVN5c3RlbTsgLy8gT25seSBjYXJ0ZXNpYW4yZCBzdXBwb3J0IGRvd24gc2FtcGxpbmdcblxuICAgICAgaWYgKGNvb3JkU3lzLnR5cGUgPT09ICdjYXJ0ZXNpYW4yZCcgJiYgc2FtcGxpbmcpIHtcbiAgICAgICAgdmFyIGJhc2VBeGlzID0gY29vcmRTeXMuZ2V0QmFzZUF4aXMoKTtcbiAgICAgICAgdmFyIHZhbHVlQXhpcyA9IGNvb3JkU3lzLmdldE90aGVyQXhpcyhiYXNlQXhpcyk7XG4gICAgICAgIHZhciBleHRlbnQgPSBiYXNlQXhpcy5nZXRFeHRlbnQoKTsgLy8gQ29vcmRpbnN0ZSBzeXN0ZW0gaGFzIGJlZW4gcmVzaXplZFxuXG4gICAgICAgIHZhciBzaXplID0gZXh0ZW50WzFdIC0gZXh0ZW50WzBdO1xuICAgICAgICB2YXIgcmF0ZSA9IE1hdGgucm91bmQoZGF0YS5jb3VudCgpIC8gc2l6ZSk7XG5cbiAgICAgICAgaWYgKHJhdGUgPiAxKSB7XG4gICAgICAgICAgdmFyIHNhbXBsZXI7XG5cbiAgICAgICAgICBpZiAodHlwZW9mIHNhbXBsaW5nID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgc2FtcGxlciA9IHNhbXBsZXJzW3NhbXBsaW5nXTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiBzYW1wbGluZyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgc2FtcGxlciA9IHNhbXBsaW5nO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmIChzYW1wbGVyKSB7XG4gICAgICAgICAgICAvLyBPbmx5IHN1cHBvcnQgc2FtcGxlIHRoZSBmaXJzdCBkaW0gbWFwcGVkIGZyb20gdmFsdWUgYXhpcy5cbiAgICAgICAgICAgIHNlcmllc01vZGVsLnNldERhdGEoZGF0YS5kb3duU2FtcGxlKGRhdGEubWFwRGltZW5zaW9uKHZhbHVlQXhpcy5kaW0pLCAxIC8gcmF0ZSwgc2FtcGxlciwgaW5kZXhTYW1wbGVyKSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBakRBO0FBQ0E7QUFtREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUEvQkE7QUFpQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/processor/dataSample.js
