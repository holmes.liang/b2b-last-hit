/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule moveSelectionForward
 * @format
 * 
 */

/**
 * Given a collapsed selection, move the focus `maxDistance` forward within
 * the selected block. If the selection will go beyond the end of the block,
 * move focus to the start of the next block, but no further.
 *
 * This function is not Unicode-aware, so surrogate pairs will be treated
 * as having length 2.
 */

function moveSelectionForward(editorState, maxDistance) {
  var selection = editorState.getSelection();
  var key = selection.getStartKey();
  var offset = selection.getStartOffset();
  var content = editorState.getCurrentContent();
  var focusKey = key;
  var focusOffset;
  var block = content.getBlockForKey(key);

  if (maxDistance > block.getText().length - offset) {
    focusKey = content.getKeyAfter(key);
    focusOffset = 0;
  } else {
    focusOffset = offset + maxDistance;
  }

  return selection.merge({
    focusKey: focusKey,
    focusOffset: focusOffset
  });
}

module.exports = moveSelectionForward;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL21vdmVTZWxlY3Rpb25Gb3J3YXJkLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL21vdmVTZWxlY3Rpb25Gb3J3YXJkLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgbW92ZVNlbGVjdGlvbkZvcndhcmRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBHaXZlbiBhIGNvbGxhcHNlZCBzZWxlY3Rpb24sIG1vdmUgdGhlIGZvY3VzIGBtYXhEaXN0YW5jZWAgZm9yd2FyZCB3aXRoaW5cbiAqIHRoZSBzZWxlY3RlZCBibG9jay4gSWYgdGhlIHNlbGVjdGlvbiB3aWxsIGdvIGJleW9uZCB0aGUgZW5kIG9mIHRoZSBibG9jayxcbiAqIG1vdmUgZm9jdXMgdG8gdGhlIHN0YXJ0IG9mIHRoZSBuZXh0IGJsb2NrLCBidXQgbm8gZnVydGhlci5cbiAqXG4gKiBUaGlzIGZ1bmN0aW9uIGlzIG5vdCBVbmljb2RlLWF3YXJlLCBzbyBzdXJyb2dhdGUgcGFpcnMgd2lsbCBiZSB0cmVhdGVkXG4gKiBhcyBoYXZpbmcgbGVuZ3RoIDIuXG4gKi9cbmZ1bmN0aW9uIG1vdmVTZWxlY3Rpb25Gb3J3YXJkKGVkaXRvclN0YXRlLCBtYXhEaXN0YW5jZSkge1xuICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gIHZhciBrZXkgPSBzZWxlY3Rpb24uZ2V0U3RhcnRLZXkoKTtcbiAgdmFyIG9mZnNldCA9IHNlbGVjdGlvbi5nZXRTdGFydE9mZnNldCgpO1xuICB2YXIgY29udGVudCA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG5cbiAgdmFyIGZvY3VzS2V5ID0ga2V5O1xuICB2YXIgZm9jdXNPZmZzZXQ7XG5cbiAgdmFyIGJsb2NrID0gY29udGVudC5nZXRCbG9ja0ZvcktleShrZXkpO1xuXG4gIGlmIChtYXhEaXN0YW5jZSA+IGJsb2NrLmdldFRleHQoKS5sZW5ndGggLSBvZmZzZXQpIHtcbiAgICBmb2N1c0tleSA9IGNvbnRlbnQuZ2V0S2V5QWZ0ZXIoa2V5KTtcbiAgICBmb2N1c09mZnNldCA9IDA7XG4gIH0gZWxzZSB7XG4gICAgZm9jdXNPZmZzZXQgPSBvZmZzZXQgKyBtYXhEaXN0YW5jZTtcbiAgfVxuXG4gIHJldHVybiBzZWxlY3Rpb24ubWVyZ2UoeyBmb2N1c0tleTogZm9jdXNLZXksIGZvY3VzT2Zmc2V0OiBmb2N1c09mZnNldCB9KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBtb3ZlU2VsZWN0aW9uRm9yd2FyZDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUVBOzs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/moveSelectionForward.js
