/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var Model = __webpack_require__(/*! ./Model */ "./node_modules/echarts/lib/model/Model.js");

var componentUtil = __webpack_require__(/*! ../util/component */ "./node_modules/echarts/lib/util/component.js");

var _clazz = __webpack_require__(/*! ../util/clazz */ "./node_modules/echarts/lib/util/clazz.js");

var enableClassManagement = _clazz.enableClassManagement;
var parseClassType = _clazz.parseClassType;

var _model = __webpack_require__(/*! ../util/model */ "./node_modules/echarts/lib/util/model.js");

var makeInner = _model.makeInner;

var layout = __webpack_require__(/*! ../util/layout */ "./node_modules/echarts/lib/util/layout.js");

var boxLayoutMixin = __webpack_require__(/*! ./mixin/boxLayout */ "./node_modules/echarts/lib/model/mixin/boxLayout.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Component model
 *
 * @module echarts/model/Component
 */


var inner = makeInner();
/**
 * @alias module:echarts/model/Component
 * @constructor
 * @param {Object} option
 * @param {module:echarts/model/Model} parentModel
 * @param {module:echarts/model/Model} ecModel
 */

var ComponentModel = Model.extend({
  type: 'component',

  /**
   * @readOnly
   * @type {string}
   */
  id: '',

  /**
   * Because simplified concept is probably better, series.name (or component.name)
   * has been having too many resposibilities:
   * (1) Generating id (which requires name in option should not be modified).
   * (2) As an index to mapping series when merging option or calling API (a name
   * can refer to more then one components, which is convinient is some case).
   * (3) Display.
   * @readOnly
   */
  name: '',

  /**
   * @readOnly
   * @type {string}
   */
  mainType: '',

  /**
   * @readOnly
   * @type {string}
   */
  subType: '',

  /**
   * @readOnly
   * @type {number}
   */
  componentIndex: 0,

  /**
   * @type {Object}
   * @protected
   */
  defaultOption: null,

  /**
   * @type {module:echarts/model/Global}
   * @readOnly
   */
  ecModel: null,

  /**
   * key: componentType
   * value:  Component model list, can not be null.
   * @type {Object.<string, Array.<module:echarts/model/Model>>}
   * @readOnly
   */
  dependentModels: [],

  /**
   * @type {string}
   * @readOnly
   */
  uid: null,

  /**
   * Support merge layout params.
   * Only support 'box' now (left/right/top/bottom/width/height).
   * @type {string|Object} Object can be {ignoreSize: true}
   * @readOnly
   */
  layoutMode: null,
  $constructor: function $constructor(option, parentModel, ecModel, extraOpt) {
    Model.call(this, option, parentModel, ecModel, extraOpt);
    this.uid = componentUtil.getUID('ec_cpt_model');
  },
  init: function init(option, parentModel, ecModel, extraOpt) {
    this.mergeDefaultAndTheme(option, ecModel);
  },
  mergeDefaultAndTheme: function mergeDefaultAndTheme(option, ecModel) {
    var layoutMode = this.layoutMode;
    var inputPositionParams = layoutMode ? layout.getLayoutParams(option) : {};
    var themeModel = ecModel.getTheme();
    zrUtil.merge(option, themeModel.get(this.mainType));
    zrUtil.merge(option, this.getDefaultOption());

    if (layoutMode) {
      layout.mergeLayoutParam(option, inputPositionParams, layoutMode);
    }
  },
  mergeOption: function mergeOption(option, extraOpt) {
    zrUtil.merge(this.option, option, true);
    var layoutMode = this.layoutMode;

    if (layoutMode) {
      layout.mergeLayoutParam(this.option, option, layoutMode);
    }
  },
  // Hooker after init or mergeOption
  optionUpdated: function optionUpdated(newCptOption, isInit) {},
  getDefaultOption: function getDefaultOption() {
    var fields = inner(this);

    if (!fields.defaultOption) {
      var optList = [];
      var Class = this.constructor;

      while (Class) {
        var opt = Class.prototype.defaultOption;
        opt && optList.push(opt);
        Class = Class.superClass;
      }

      var defaultOption = {};

      for (var i = optList.length - 1; i >= 0; i--) {
        defaultOption = zrUtil.merge(defaultOption, optList[i], true);
      }

      fields.defaultOption = defaultOption;
    }

    return fields.defaultOption;
  },
  getReferringComponents: function getReferringComponents(mainType) {
    return this.ecModel.queryComponents({
      mainType: mainType,
      index: this.get(mainType + 'Index', true),
      id: this.get(mainType + 'Id', true)
    });
  }
}); // Reset ComponentModel.extend, add preConstruct.
// clazzUtil.enableClassExtend(
//     ComponentModel,
//     function (option, parentModel, ecModel, extraOpt) {
//         // Set dependentModels, componentIndex, name, id, mainType, subType.
//         zrUtil.extend(this, extraOpt);
//         this.uid = componentUtil.getUID('componentModel');
//         // this.setReadOnly([
//         //     'type', 'id', 'uid', 'name', 'mainType', 'subType',
//         //     'dependentModels', 'componentIndex'
//         // ]);
//     }
// );
// Add capability of registerClass, getClass, hasClass, registerSubTypeDefaulter and so on.

enableClassManagement(ComponentModel, {
  registerWhenExtend: true
});
componentUtil.enableSubTypeDefaulter(ComponentModel); // Add capability of ComponentModel.topologicalTravel.

componentUtil.enableTopologicalTravel(ComponentModel, getDependencies);

function getDependencies(componentType) {
  var deps = [];
  zrUtil.each(ComponentModel.getClassesByMainType(componentType), function (Clazz) {
    deps = deps.concat(Clazz.prototype.dependencies || []);
  }); // Ensure main type.

  deps = zrUtil.map(deps, function (type) {
    return parseClassType(type).main;
  }); // Hack dataset for convenience.

  if (componentType !== 'dataset' && zrUtil.indexOf(deps, 'dataset') <= 0) {
    deps.unshift('dataset');
  }

  return deps;
}

zrUtil.mixin(ComponentModel, boxLayoutMixin);
var _default = ComponentModel;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvQ29tcG9uZW50LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvQ29tcG9uZW50LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIE1vZGVsID0gcmVxdWlyZShcIi4vTW9kZWxcIik7XG5cbnZhciBjb21wb25lbnRVdGlsID0gcmVxdWlyZShcIi4uL3V0aWwvY29tcG9uZW50XCIpO1xuXG52YXIgX2NsYXp6ID0gcmVxdWlyZShcIi4uL3V0aWwvY2xhenpcIik7XG5cbnZhciBlbmFibGVDbGFzc01hbmFnZW1lbnQgPSBfY2xhenouZW5hYmxlQ2xhc3NNYW5hZ2VtZW50O1xudmFyIHBhcnNlQ2xhc3NUeXBlID0gX2NsYXp6LnBhcnNlQ2xhc3NUeXBlO1xuXG52YXIgX21vZGVsID0gcmVxdWlyZShcIi4uL3V0aWwvbW9kZWxcIik7XG5cbnZhciBtYWtlSW5uZXIgPSBfbW9kZWwubWFrZUlubmVyO1xuXG52YXIgbGF5b3V0ID0gcmVxdWlyZShcIi4uL3V0aWwvbGF5b3V0XCIpO1xuXG52YXIgYm94TGF5b3V0TWl4aW4gPSByZXF1aXJlKFwiLi9taXhpbi9ib3hMYXlvdXRcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBDb21wb25lbnQgbW9kZWxcbiAqXG4gKiBAbW9kdWxlIGVjaGFydHMvbW9kZWwvQ29tcG9uZW50XG4gKi9cbnZhciBpbm5lciA9IG1ha2VJbm5lcigpO1xuLyoqXG4gKiBAYWxpYXMgbW9kdWxlOmVjaGFydHMvbW9kZWwvQ29tcG9uZW50XG4gKiBAY29uc3RydWN0b3JcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25cbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWx9IHBhcmVudE1vZGVsXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBlY01vZGVsXG4gKi9cblxudmFyIENvbXBvbmVudE1vZGVsID0gTW9kZWwuZXh0ZW5kKHtcbiAgdHlwZTogJ2NvbXBvbmVudCcsXG5cbiAgLyoqXG4gICAqIEByZWFkT25seVxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgaWQ6ICcnLFxuXG4gIC8qKlxuICAgKiBCZWNhdXNlIHNpbXBsaWZpZWQgY29uY2VwdCBpcyBwcm9iYWJseSBiZXR0ZXIsIHNlcmllcy5uYW1lIChvciBjb21wb25lbnQubmFtZSlcbiAgICogaGFzIGJlZW4gaGF2aW5nIHRvbyBtYW55IHJlc3Bvc2liaWxpdGllczpcbiAgICogKDEpIEdlbmVyYXRpbmcgaWQgKHdoaWNoIHJlcXVpcmVzIG5hbWUgaW4gb3B0aW9uIHNob3VsZCBub3QgYmUgbW9kaWZpZWQpLlxuICAgKiAoMikgQXMgYW4gaW5kZXggdG8gbWFwcGluZyBzZXJpZXMgd2hlbiBtZXJnaW5nIG9wdGlvbiBvciBjYWxsaW5nIEFQSSAoYSBuYW1lXG4gICAqIGNhbiByZWZlciB0byBtb3JlIHRoZW4gb25lIGNvbXBvbmVudHMsIHdoaWNoIGlzIGNvbnZpbmllbnQgaXMgc29tZSBjYXNlKS5cbiAgICogKDMpIERpc3BsYXkuXG4gICAqIEByZWFkT25seVxuICAgKi9cbiAgbmFtZTogJycsXG5cbiAgLyoqXG4gICAqIEByZWFkT25seVxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgbWFpblR5cGU6ICcnLFxuXG4gIC8qKlxuICAgKiBAcmVhZE9ubHlcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIHN1YlR5cGU6ICcnLFxuXG4gIC8qKlxuICAgKiBAcmVhZE9ubHlcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIGNvbXBvbmVudEluZGV4OiAwLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuICBkZWZhdWx0T3B0aW9uOiBudWxsLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvR2xvYmFsfVxuICAgKiBAcmVhZE9ubHlcbiAgICovXG4gIGVjTW9kZWw6IG51bGwsXG5cbiAgLyoqXG4gICAqIGtleTogY29tcG9uZW50VHlwZVxuICAgKiB2YWx1ZTogIENvbXBvbmVudCBtb2RlbCBsaXN0LCBjYW4gbm90IGJlIG51bGwuXG4gICAqIEB0eXBlIHtPYmplY3QuPHN0cmluZywgQXJyYXkuPG1vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsPj59XG4gICAqIEByZWFkT25seVxuICAgKi9cbiAgZGVwZW5kZW50TW9kZWxzOiBbXSxcblxuICAvKipcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICogQHJlYWRPbmx5XG4gICAqL1xuICB1aWQ6IG51bGwsXG5cbiAgLyoqXG4gICAqIFN1cHBvcnQgbWVyZ2UgbGF5b3V0IHBhcmFtcy5cbiAgICogT25seSBzdXBwb3J0ICdib3gnIG5vdyAobGVmdC9yaWdodC90b3AvYm90dG9tL3dpZHRoL2hlaWdodCkuXG4gICAqIEB0eXBlIHtzdHJpbmd8T2JqZWN0fSBPYmplY3QgY2FuIGJlIHtpZ25vcmVTaXplOiB0cnVlfVxuICAgKiBAcmVhZE9ubHlcbiAgICovXG4gIGxheW91dE1vZGU6IG51bGwsXG4gICRjb25zdHJ1Y3RvcjogZnVuY3Rpb24gKG9wdGlvbiwgcGFyZW50TW9kZWwsIGVjTW9kZWwsIGV4dHJhT3B0KSB7XG4gICAgTW9kZWwuY2FsbCh0aGlzLCBvcHRpb24sIHBhcmVudE1vZGVsLCBlY01vZGVsLCBleHRyYU9wdCk7XG4gICAgdGhpcy51aWQgPSBjb21wb25lbnRVdGlsLmdldFVJRCgnZWNfY3B0X21vZGVsJyk7XG4gIH0sXG4gIGluaXQ6IGZ1bmN0aW9uIChvcHRpb24sIHBhcmVudE1vZGVsLCBlY01vZGVsLCBleHRyYU9wdCkge1xuICAgIHRoaXMubWVyZ2VEZWZhdWx0QW5kVGhlbWUob3B0aW9uLCBlY01vZGVsKTtcbiAgfSxcbiAgbWVyZ2VEZWZhdWx0QW5kVGhlbWU6IGZ1bmN0aW9uIChvcHRpb24sIGVjTW9kZWwpIHtcbiAgICB2YXIgbGF5b3V0TW9kZSA9IHRoaXMubGF5b3V0TW9kZTtcbiAgICB2YXIgaW5wdXRQb3NpdGlvblBhcmFtcyA9IGxheW91dE1vZGUgPyBsYXlvdXQuZ2V0TGF5b3V0UGFyYW1zKG9wdGlvbikgOiB7fTtcbiAgICB2YXIgdGhlbWVNb2RlbCA9IGVjTW9kZWwuZ2V0VGhlbWUoKTtcbiAgICB6clV0aWwubWVyZ2Uob3B0aW9uLCB0aGVtZU1vZGVsLmdldCh0aGlzLm1haW5UeXBlKSk7XG4gICAgenJVdGlsLm1lcmdlKG9wdGlvbiwgdGhpcy5nZXREZWZhdWx0T3B0aW9uKCkpO1xuXG4gICAgaWYgKGxheW91dE1vZGUpIHtcbiAgICAgIGxheW91dC5tZXJnZUxheW91dFBhcmFtKG9wdGlvbiwgaW5wdXRQb3NpdGlvblBhcmFtcywgbGF5b3V0TW9kZSk7XG4gICAgfVxuICB9LFxuICBtZXJnZU9wdGlvbjogZnVuY3Rpb24gKG9wdGlvbiwgZXh0cmFPcHQpIHtcbiAgICB6clV0aWwubWVyZ2UodGhpcy5vcHRpb24sIG9wdGlvbiwgdHJ1ZSk7XG4gICAgdmFyIGxheW91dE1vZGUgPSB0aGlzLmxheW91dE1vZGU7XG5cbiAgICBpZiAobGF5b3V0TW9kZSkge1xuICAgICAgbGF5b3V0Lm1lcmdlTGF5b3V0UGFyYW0odGhpcy5vcHRpb24sIG9wdGlvbiwgbGF5b3V0TW9kZSk7XG4gICAgfVxuICB9LFxuICAvLyBIb29rZXIgYWZ0ZXIgaW5pdCBvciBtZXJnZU9wdGlvblxuICBvcHRpb25VcGRhdGVkOiBmdW5jdGlvbiAobmV3Q3B0T3B0aW9uLCBpc0luaXQpIHt9LFxuICBnZXREZWZhdWx0T3B0aW9uOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGZpZWxkcyA9IGlubmVyKHRoaXMpO1xuXG4gICAgaWYgKCFmaWVsZHMuZGVmYXVsdE9wdGlvbikge1xuICAgICAgdmFyIG9wdExpc3QgPSBbXTtcbiAgICAgIHZhciBDbGFzcyA9IHRoaXMuY29uc3RydWN0b3I7XG5cbiAgICAgIHdoaWxlIChDbGFzcykge1xuICAgICAgICB2YXIgb3B0ID0gQ2xhc3MucHJvdG90eXBlLmRlZmF1bHRPcHRpb247XG4gICAgICAgIG9wdCAmJiBvcHRMaXN0LnB1c2gob3B0KTtcbiAgICAgICAgQ2xhc3MgPSBDbGFzcy5zdXBlckNsYXNzO1xuICAgICAgfVxuXG4gICAgICB2YXIgZGVmYXVsdE9wdGlvbiA9IHt9O1xuXG4gICAgICBmb3IgKHZhciBpID0gb3B0TGlzdC5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgICAgICBkZWZhdWx0T3B0aW9uID0genJVdGlsLm1lcmdlKGRlZmF1bHRPcHRpb24sIG9wdExpc3RbaV0sIHRydWUpO1xuICAgICAgfVxuXG4gICAgICBmaWVsZHMuZGVmYXVsdE9wdGlvbiA9IGRlZmF1bHRPcHRpb247XG4gICAgfVxuXG4gICAgcmV0dXJuIGZpZWxkcy5kZWZhdWx0T3B0aW9uO1xuICB9LFxuICBnZXRSZWZlcnJpbmdDb21wb25lbnRzOiBmdW5jdGlvbiAobWFpblR5cGUpIHtcbiAgICByZXR1cm4gdGhpcy5lY01vZGVsLnF1ZXJ5Q29tcG9uZW50cyh7XG4gICAgICBtYWluVHlwZTogbWFpblR5cGUsXG4gICAgICBpbmRleDogdGhpcy5nZXQobWFpblR5cGUgKyAnSW5kZXgnLCB0cnVlKSxcbiAgICAgIGlkOiB0aGlzLmdldChtYWluVHlwZSArICdJZCcsIHRydWUpXG4gICAgfSk7XG4gIH1cbn0pOyAvLyBSZXNldCBDb21wb25lbnRNb2RlbC5leHRlbmQsIGFkZCBwcmVDb25zdHJ1Y3QuXG4vLyBjbGF6elV0aWwuZW5hYmxlQ2xhc3NFeHRlbmQoXG4vLyAgICAgQ29tcG9uZW50TW9kZWwsXG4vLyAgICAgZnVuY3Rpb24gKG9wdGlvbiwgcGFyZW50TW9kZWwsIGVjTW9kZWwsIGV4dHJhT3B0KSB7XG4vLyAgICAgICAgIC8vIFNldCBkZXBlbmRlbnRNb2RlbHMsIGNvbXBvbmVudEluZGV4LCBuYW1lLCBpZCwgbWFpblR5cGUsIHN1YlR5cGUuXG4vLyAgICAgICAgIHpyVXRpbC5leHRlbmQodGhpcywgZXh0cmFPcHQpO1xuLy8gICAgICAgICB0aGlzLnVpZCA9IGNvbXBvbmVudFV0aWwuZ2V0VUlEKCdjb21wb25lbnRNb2RlbCcpO1xuLy8gICAgICAgICAvLyB0aGlzLnNldFJlYWRPbmx5KFtcbi8vICAgICAgICAgLy8gICAgICd0eXBlJywgJ2lkJywgJ3VpZCcsICduYW1lJywgJ21haW5UeXBlJywgJ3N1YlR5cGUnLFxuLy8gICAgICAgICAvLyAgICAgJ2RlcGVuZGVudE1vZGVscycsICdjb21wb25lbnRJbmRleCdcbi8vICAgICAgICAgLy8gXSk7XG4vLyAgICAgfVxuLy8gKTtcbi8vIEFkZCBjYXBhYmlsaXR5IG9mIHJlZ2lzdGVyQ2xhc3MsIGdldENsYXNzLCBoYXNDbGFzcywgcmVnaXN0ZXJTdWJUeXBlRGVmYXVsdGVyIGFuZCBzbyBvbi5cblxuZW5hYmxlQ2xhc3NNYW5hZ2VtZW50KENvbXBvbmVudE1vZGVsLCB7XG4gIHJlZ2lzdGVyV2hlbkV4dGVuZDogdHJ1ZVxufSk7XG5jb21wb25lbnRVdGlsLmVuYWJsZVN1YlR5cGVEZWZhdWx0ZXIoQ29tcG9uZW50TW9kZWwpOyAvLyBBZGQgY2FwYWJpbGl0eSBvZiBDb21wb25lbnRNb2RlbC50b3BvbG9naWNhbFRyYXZlbC5cblxuY29tcG9uZW50VXRpbC5lbmFibGVUb3BvbG9naWNhbFRyYXZlbChDb21wb25lbnRNb2RlbCwgZ2V0RGVwZW5kZW5jaWVzKTtcblxuZnVuY3Rpb24gZ2V0RGVwZW5kZW5jaWVzKGNvbXBvbmVudFR5cGUpIHtcbiAgdmFyIGRlcHMgPSBbXTtcbiAgenJVdGlsLmVhY2goQ29tcG9uZW50TW9kZWwuZ2V0Q2xhc3Nlc0J5TWFpblR5cGUoY29tcG9uZW50VHlwZSksIGZ1bmN0aW9uIChDbGF6eikge1xuICAgIGRlcHMgPSBkZXBzLmNvbmNhdChDbGF6ei5wcm90b3R5cGUuZGVwZW5kZW5jaWVzIHx8IFtdKTtcbiAgfSk7IC8vIEVuc3VyZSBtYWluIHR5cGUuXG5cbiAgZGVwcyA9IHpyVXRpbC5tYXAoZGVwcywgZnVuY3Rpb24gKHR5cGUpIHtcbiAgICByZXR1cm4gcGFyc2VDbGFzc1R5cGUodHlwZSkubWFpbjtcbiAgfSk7IC8vIEhhY2sgZGF0YXNldCBmb3IgY29udmVuaWVuY2UuXG5cbiAgaWYgKGNvbXBvbmVudFR5cGUgIT09ICdkYXRhc2V0JyAmJiB6clV0aWwuaW5kZXhPZihkZXBzLCAnZGF0YXNldCcpIDw9IDApIHtcbiAgICBkZXBzLnVuc2hpZnQoJ2RhdGFzZXQnKTtcbiAgfVxuXG4gIHJldHVybiBkZXBzO1xufVxuXG56clV0aWwubWl4aW4oQ29tcG9uZW50TW9kZWwsIGJveExheW91dE1peGluKTtcbnZhciBfZGVmYXVsdCA9IENvbXBvbmVudE1vZGVsO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7Ozs7QUFLQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBaklBO0FBbUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/model/Component.js
