/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var layoutUtil = __webpack_require__(/*! ../../util/layout */ "./node_modules/echarts/lib/util/layout.js");

var LegendView = __webpack_require__(/*! ./LegendView */ "./node_modules/echarts/lib/component/legend/LegendView.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Separate legend and scrollable legend to reduce package size.
 */


var Group = graphic.Group;
var WH = ['width', 'height'];
var XY = ['x', 'y'];
var ScrollableLegendView = LegendView.extend({
  type: 'legend.scroll',
  newlineDisabled: true,
  init: function init() {
    ScrollableLegendView.superCall(this, 'init');
    /**
     * @private
     * @type {number} For `scroll`.
     */

    this._currentIndex = 0;
    /**
     * @private
     * @type {module:zrender/container/Group}
     */

    this.group.add(this._containerGroup = new Group());

    this._containerGroup.add(this.getContentGroup());
    /**
     * @private
     * @type {module:zrender/container/Group}
     */


    this.group.add(this._controllerGroup = new Group());
    /**
     *
     * @private
     */

    this._showController;
  },

  /**
   * @override
   */
  resetInner: function resetInner() {
    ScrollableLegendView.superCall(this, 'resetInner');

    this._controllerGroup.removeAll();

    this._containerGroup.removeClipPath();

    this._containerGroup.__rectSize = null;
  },

  /**
   * @override
   */
  renderInner: function renderInner(itemAlign, legendModel, ecModel, api) {
    var me = this; // Render content items.

    ScrollableLegendView.superCall(this, 'renderInner', itemAlign, legendModel, ecModel, api);
    var controllerGroup = this._controllerGroup; // FIXME: support be 'auto' adapt to size number text length,
    // e.g., '3/12345' should not overlap with the control arrow button.

    var pageIconSize = legendModel.get('pageIconSize', true);

    if (!zrUtil.isArray(pageIconSize)) {
      pageIconSize = [pageIconSize, pageIconSize];
    }

    createPageButton('pagePrev', 0);
    var pageTextStyleModel = legendModel.getModel('pageTextStyle');
    controllerGroup.add(new graphic.Text({
      name: 'pageText',
      style: {
        textFill: pageTextStyleModel.getTextColor(),
        font: pageTextStyleModel.getFont(),
        textVerticalAlign: 'middle',
        textAlign: 'center'
      },
      silent: true
    }));
    createPageButton('pageNext', 1);

    function createPageButton(name, iconIdx) {
      var pageDataIndexName = name + 'DataIndex';
      var icon = graphic.createIcon(legendModel.get('pageIcons', true)[legendModel.getOrient().name][iconIdx], {
        // Buttons will be created in each render, so we do not need
        // to worry about avoiding using legendModel kept in scope.
        onclick: zrUtil.bind(me._pageGo, me, pageDataIndexName, legendModel, api)
      }, {
        x: -pageIconSize[0] / 2,
        y: -pageIconSize[1] / 2,
        width: pageIconSize[0],
        height: pageIconSize[1]
      });
      icon.name = name;
      controllerGroup.add(icon);
    }
  },

  /**
   * @override
   */
  layoutInner: function layoutInner(legendModel, itemAlign, maxSize, isFirstRender) {
    var contentGroup = this.getContentGroup();
    var containerGroup = this._containerGroup;
    var controllerGroup = this._controllerGroup;
    var orientIdx = legendModel.getOrient().index;
    var wh = WH[orientIdx];
    var hw = WH[1 - orientIdx];
    var yx = XY[1 - orientIdx]; // Place items in contentGroup.

    layoutUtil.box(legendModel.get('orient'), contentGroup, legendModel.get('itemGap'), !orientIdx ? null : maxSize.width, orientIdx ? null : maxSize.height);
    layoutUtil.box( // Buttons in controller are layout always horizontally.
    'horizontal', controllerGroup, legendModel.get('pageButtonItemGap', true));
    var contentRect = contentGroup.getBoundingRect();
    var controllerRect = controllerGroup.getBoundingRect();
    var showController = this._showController = contentRect[wh] > maxSize[wh];
    var contentPos = [-contentRect.x, -contentRect.y]; // Remain contentPos when scroll animation perfroming.
    // If first rendering, `contentGroup.position` is [0, 0], which
    // does not make sense and may cause unexepcted animation if adopted.

    if (!isFirstRender) {
      contentPos[orientIdx] = contentGroup.position[orientIdx];
    } // Layout container group based on 0.


    var containerPos = [0, 0];
    var controllerPos = [-controllerRect.x, -controllerRect.y];
    var pageButtonGap = zrUtil.retrieve2(legendModel.get('pageButtonGap', true), legendModel.get('itemGap', true)); // Place containerGroup and controllerGroup and contentGroup.

    if (showController) {
      var pageButtonPosition = legendModel.get('pageButtonPosition', true); // controller is on the right / bottom.

      if (pageButtonPosition === 'end') {
        controllerPos[orientIdx] += maxSize[wh] - controllerRect[wh];
      } // controller is on the left / top.
      else {
          containerPos[orientIdx] += controllerRect[wh] + pageButtonGap;
        }
    } // Always align controller to content as 'middle'.


    controllerPos[1 - orientIdx] += contentRect[hw] / 2 - controllerRect[hw] / 2;
    contentGroup.attr('position', contentPos);
    containerGroup.attr('position', containerPos);
    controllerGroup.attr('position', controllerPos); // Calculate `mainRect` and set `clipPath`.
    // mainRect should not be calculated by `this.group.getBoundingRect()`
    // for sake of the overflow.

    var mainRect = this.group.getBoundingRect();
    var mainRect = {
      x: 0,
      y: 0
    }; // Consider content may be overflow (should be clipped).

    mainRect[wh] = showController ? maxSize[wh] : contentRect[wh];
    mainRect[hw] = Math.max(contentRect[hw], controllerRect[hw]); // `containerRect[yx] + containerPos[1 - orientIdx]` is 0.

    mainRect[yx] = Math.min(0, controllerRect[yx] + controllerPos[1 - orientIdx]);
    containerGroup.__rectSize = maxSize[wh];

    if (showController) {
      var clipShape = {
        x: 0,
        y: 0
      };
      clipShape[wh] = Math.max(maxSize[wh] - controllerRect[wh] - pageButtonGap, 0);
      clipShape[hw] = mainRect[hw];
      containerGroup.setClipPath(new graphic.Rect({
        shape: clipShape
      })); // Consider content may be larger than container, container rect
      // can not be obtained from `containerGroup.getBoundingRect()`.

      containerGroup.__rectSize = clipShape[wh];
    } else {
      // Do not remove or ignore controller. Keep them set as place holders.
      controllerGroup.eachChild(function (child) {
        child.attr({
          invisible: true,
          silent: true
        });
      });
    } // Content translate animation.


    var pageInfo = this._getPageInfo(legendModel);

    pageInfo.pageIndex != null && graphic.updateProps(contentGroup, {
      position: pageInfo.contentPosition
    }, // When switch from "show controller" to "not show controller", view should be
    // updated immediately without animation, otherwise causes weird efffect.
    showController ? legendModel : false);

    this._updatePageInfoView(legendModel, pageInfo);

    return mainRect;
  },
  _pageGo: function _pageGo(to, legendModel, api) {
    var scrollDataIndex = this._getPageInfo(legendModel)[to];

    scrollDataIndex != null && api.dispatchAction({
      type: 'legendScroll',
      scrollDataIndex: scrollDataIndex,
      legendId: legendModel.id
    });
  },
  _updatePageInfoView: function _updatePageInfoView(legendModel, pageInfo) {
    var controllerGroup = this._controllerGroup;
    zrUtil.each(['pagePrev', 'pageNext'], function (name) {
      var canJump = pageInfo[name + 'DataIndex'] != null;
      var icon = controllerGroup.childOfName(name);

      if (icon) {
        icon.setStyle('fill', canJump ? legendModel.get('pageIconColor', true) : legendModel.get('pageIconInactiveColor', true));
        icon.cursor = canJump ? 'pointer' : 'default';
      }
    });
    var pageText = controllerGroup.childOfName('pageText');
    var pageFormatter = legendModel.get('pageFormatter');
    var pageIndex = pageInfo.pageIndex;
    var current = pageIndex != null ? pageIndex + 1 : 0;
    var total = pageInfo.pageCount;
    pageText && pageFormatter && pageText.setStyle('text', zrUtil.isString(pageFormatter) ? pageFormatter.replace('{current}', current).replace('{total}', total) : pageFormatter({
      current: current,
      total: total
    }));
  },

  /**
   * @param {module:echarts/model/Model} legendModel
   * @return {Object} {
   *  contentPosition: Array.<number>, null when data item not found.
   *  pageIndex: number, null when data item not found.
   *  pageCount: number, always be a number, can be 0.
   *  pagePrevDataIndex: number, null when no next page.
   *  pageNextDataIndex: number, null when no previous page.
   * }
   */
  _getPageInfo: function _getPageInfo(legendModel) {
    var scrollDataIndex = legendModel.get('scrollDataIndex', true);
    var contentGroup = this.getContentGroup();
    var containerRectSize = this._containerGroup.__rectSize;
    var orientIdx = legendModel.getOrient().index;
    var wh = WH[orientIdx];
    var xy = XY[orientIdx];

    var targetItemIndex = this._findTargetItemIndex(scrollDataIndex);

    var children = contentGroup.children();
    var targetItem = children[targetItemIndex];
    var itemCount = children.length;
    var pCount = !itemCount ? 0 : 1;
    var result = {
      contentPosition: contentGroup.position.slice(),
      pageCount: pCount,
      pageIndex: pCount - 1,
      pagePrevDataIndex: null,
      pageNextDataIndex: null
    };

    if (!targetItem) {
      return result;
    }

    var targetItemInfo = getItemInfo(targetItem);
    result.contentPosition[orientIdx] = -targetItemInfo.s; // Strategy:
    // (1) Always align based on the left/top most item.
    // (2) It is user-friendly that the last item shown in the
    // current window is shown at the begining of next window.
    // Otherwise if half of the last item is cut by the window,
    // it will have no chance to display entirely.
    // (3) Consider that item size probably be different, we
    // have calculate pageIndex by size rather than item index,
    // and we can not get page index directly by division.
    // (4) The window is to narrow to contain more than
    // one item, we should make sure that the page can be fliped.

    for (var i = targetItemIndex + 1, winStartItemInfo = targetItemInfo, winEndItemInfo = targetItemInfo, currItemInfo = null; i <= itemCount; ++i) {
      currItemInfo = getItemInfo(children[i]);

      if ( // Half of the last item is out of the window.
      !currItemInfo && winEndItemInfo.e > winStartItemInfo.s + containerRectSize || // If the current item does not intersect with the window, the new page
      // can be started at the current item or the last item.
      currItemInfo && !intersect(currItemInfo, winStartItemInfo.s)) {
        if (winEndItemInfo.i > winStartItemInfo.i) {
          winStartItemInfo = winEndItemInfo;
        } else {
          // e.g., when page size is smaller than item size.
          winStartItemInfo = currItemInfo;
        }

        if (winStartItemInfo) {
          if (result.pageNextDataIndex == null) {
            result.pageNextDataIndex = winStartItemInfo.i;
          }

          ++result.pageCount;
        }
      }

      winEndItemInfo = currItemInfo;
    }

    for (var i = targetItemIndex - 1, winStartItemInfo = targetItemInfo, winEndItemInfo = targetItemInfo, currItemInfo = null; i >= -1; --i) {
      currItemInfo = getItemInfo(children[i]);

      if ( // If the the end item does not intersect with the window started
      // from the current item, a page can be settled.
      (!currItemInfo || !intersect(winEndItemInfo, currItemInfo.s)) && // e.g., when page size is smaller than item size.
      winStartItemInfo.i < winEndItemInfo.i) {
        winEndItemInfo = winStartItemInfo;

        if (result.pagePrevDataIndex == null) {
          result.pagePrevDataIndex = winStartItemInfo.i;
        }

        ++result.pageCount;
        ++result.pageIndex;
      }

      winStartItemInfo = currItemInfo;
    }

    return result;

    function getItemInfo(el) {
      if (el) {
        var itemRect = el.getBoundingRect();
        var start = itemRect[xy] + el.position[orientIdx];
        return {
          s: start,
          e: start + itemRect[wh],
          i: el.__legendDataIndex
        };
      }
    }

    function intersect(itemInfo, winStart) {
      return itemInfo.e >= winStart && itemInfo.s <= winStart + containerRectSize;
    }
  },
  _findTargetItemIndex: function _findTargetItemIndex(targetDataIndex) {
    var index;
    var contentGroup = this.getContentGroup();

    if (this._showController) {
      contentGroup.eachChild(function (child, idx) {
        if (child.__legendDataIndex === targetDataIndex) {
          index = idx;
        }
      });
    } else {
      index = 0;
    }

    return index;
  }
});
var _default = ScrollableLegendView;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2xlZ2VuZC9TY3JvbGxhYmxlTGVnZW5kVmlldy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2NvbXBvbmVudC9sZWdlbmQvU2Nyb2xsYWJsZUxlZ2VuZFZpZXcuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgZ3JhcGhpYyA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL2dyYXBoaWNcIik7XG5cbnZhciBsYXlvdXRVdGlsID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvbGF5b3V0XCIpO1xuXG52YXIgTGVnZW5kVmlldyA9IHJlcXVpcmUoXCIuL0xlZ2VuZFZpZXdcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBTZXBhcmF0ZSBsZWdlbmQgYW5kIHNjcm9sbGFibGUgbGVnZW5kIHRvIHJlZHVjZSBwYWNrYWdlIHNpemUuXG4gKi9cbnZhciBHcm91cCA9IGdyYXBoaWMuR3JvdXA7XG52YXIgV0ggPSBbJ3dpZHRoJywgJ2hlaWdodCddO1xudmFyIFhZID0gWyd4JywgJ3knXTtcbnZhciBTY3JvbGxhYmxlTGVnZW5kVmlldyA9IExlZ2VuZFZpZXcuZXh0ZW5kKHtcbiAgdHlwZTogJ2xlZ2VuZC5zY3JvbGwnLFxuICBuZXdsaW5lRGlzYWJsZWQ6IHRydWUsXG4gIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICBTY3JvbGxhYmxlTGVnZW5kVmlldy5zdXBlckNhbGwodGhpcywgJ2luaXQnKTtcbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEB0eXBlIHtudW1iZXJ9IEZvciBgc2Nyb2xsYC5cbiAgICAgKi9cblxuICAgIHRoaXMuX2N1cnJlbnRJbmRleCA9IDA7XG4gICAgLyoqXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBAdHlwZSB7bW9kdWxlOnpyZW5kZXIvY29udGFpbmVyL0dyb3VwfVxuICAgICAqL1xuXG4gICAgdGhpcy5ncm91cC5hZGQodGhpcy5fY29udGFpbmVyR3JvdXAgPSBuZXcgR3JvdXAoKSk7XG5cbiAgICB0aGlzLl9jb250YWluZXJHcm91cC5hZGQodGhpcy5nZXRDb250ZW50R3JvdXAoKSk7XG4gICAgLyoqXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBAdHlwZSB7bW9kdWxlOnpyZW5kZXIvY29udGFpbmVyL0dyb3VwfVxuICAgICAqL1xuXG5cbiAgICB0aGlzLmdyb3VwLmFkZCh0aGlzLl9jb250cm9sbGVyR3JvdXAgPSBuZXcgR3JvdXAoKSk7XG4gICAgLyoqXG4gICAgICpcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuXG4gICAgdGhpcy5fc2hvd0NvbnRyb2xsZXI7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBvdmVycmlkZVxuICAgKi9cbiAgcmVzZXRJbm5lcjogZnVuY3Rpb24gKCkge1xuICAgIFNjcm9sbGFibGVMZWdlbmRWaWV3LnN1cGVyQ2FsbCh0aGlzLCAncmVzZXRJbm5lcicpO1xuXG4gICAgdGhpcy5fY29udHJvbGxlckdyb3VwLnJlbW92ZUFsbCgpO1xuXG4gICAgdGhpcy5fY29udGFpbmVyR3JvdXAucmVtb3ZlQ2xpcFBhdGgoKTtcblxuICAgIHRoaXMuX2NvbnRhaW5lckdyb3VwLl9fcmVjdFNpemUgPSBudWxsO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAb3ZlcnJpZGVcbiAgICovXG4gIHJlbmRlcklubmVyOiBmdW5jdGlvbiAoaXRlbUFsaWduLCBsZWdlbmRNb2RlbCwgZWNNb2RlbCwgYXBpKSB7XG4gICAgdmFyIG1lID0gdGhpczsgLy8gUmVuZGVyIGNvbnRlbnQgaXRlbXMuXG5cbiAgICBTY3JvbGxhYmxlTGVnZW5kVmlldy5zdXBlckNhbGwodGhpcywgJ3JlbmRlcklubmVyJywgaXRlbUFsaWduLCBsZWdlbmRNb2RlbCwgZWNNb2RlbCwgYXBpKTtcbiAgICB2YXIgY29udHJvbGxlckdyb3VwID0gdGhpcy5fY29udHJvbGxlckdyb3VwOyAvLyBGSVhNRTogc3VwcG9ydCBiZSAnYXV0bycgYWRhcHQgdG8gc2l6ZSBudW1iZXIgdGV4dCBsZW5ndGgsXG4gICAgLy8gZS5nLiwgJzMvMTIzNDUnIHNob3VsZCBub3Qgb3ZlcmxhcCB3aXRoIHRoZSBjb250cm9sIGFycm93IGJ1dHRvbi5cblxuICAgIHZhciBwYWdlSWNvblNpemUgPSBsZWdlbmRNb2RlbC5nZXQoJ3BhZ2VJY29uU2l6ZScsIHRydWUpO1xuXG4gICAgaWYgKCF6clV0aWwuaXNBcnJheShwYWdlSWNvblNpemUpKSB7XG4gICAgICBwYWdlSWNvblNpemUgPSBbcGFnZUljb25TaXplLCBwYWdlSWNvblNpemVdO1xuICAgIH1cblxuICAgIGNyZWF0ZVBhZ2VCdXR0b24oJ3BhZ2VQcmV2JywgMCk7XG4gICAgdmFyIHBhZ2VUZXh0U3R5bGVNb2RlbCA9IGxlZ2VuZE1vZGVsLmdldE1vZGVsKCdwYWdlVGV4dFN0eWxlJyk7XG4gICAgY29udHJvbGxlckdyb3VwLmFkZChuZXcgZ3JhcGhpYy5UZXh0KHtcbiAgICAgIG5hbWU6ICdwYWdlVGV4dCcsXG4gICAgICBzdHlsZToge1xuICAgICAgICB0ZXh0RmlsbDogcGFnZVRleHRTdHlsZU1vZGVsLmdldFRleHRDb2xvcigpLFxuICAgICAgICBmb250OiBwYWdlVGV4dFN0eWxlTW9kZWwuZ2V0Rm9udCgpLFxuICAgICAgICB0ZXh0VmVydGljYWxBbGlnbjogJ21pZGRsZScsXG4gICAgICAgIHRleHRBbGlnbjogJ2NlbnRlcidcbiAgICAgIH0sXG4gICAgICBzaWxlbnQ6IHRydWVcbiAgICB9KSk7XG4gICAgY3JlYXRlUGFnZUJ1dHRvbigncGFnZU5leHQnLCAxKTtcblxuICAgIGZ1bmN0aW9uIGNyZWF0ZVBhZ2VCdXR0b24obmFtZSwgaWNvbklkeCkge1xuICAgICAgdmFyIHBhZ2VEYXRhSW5kZXhOYW1lID0gbmFtZSArICdEYXRhSW5kZXgnO1xuICAgICAgdmFyIGljb24gPSBncmFwaGljLmNyZWF0ZUljb24obGVnZW5kTW9kZWwuZ2V0KCdwYWdlSWNvbnMnLCB0cnVlKVtsZWdlbmRNb2RlbC5nZXRPcmllbnQoKS5uYW1lXVtpY29uSWR4XSwge1xuICAgICAgICAvLyBCdXR0b25zIHdpbGwgYmUgY3JlYXRlZCBpbiBlYWNoIHJlbmRlciwgc28gd2UgZG8gbm90IG5lZWRcbiAgICAgICAgLy8gdG8gd29ycnkgYWJvdXQgYXZvaWRpbmcgdXNpbmcgbGVnZW5kTW9kZWwga2VwdCBpbiBzY29wZS5cbiAgICAgICAgb25jbGljazogenJVdGlsLmJpbmQobWUuX3BhZ2VHbywgbWUsIHBhZ2VEYXRhSW5kZXhOYW1lLCBsZWdlbmRNb2RlbCwgYXBpKVxuICAgICAgfSwge1xuICAgICAgICB4OiAtcGFnZUljb25TaXplWzBdIC8gMixcbiAgICAgICAgeTogLXBhZ2VJY29uU2l6ZVsxXSAvIDIsXG4gICAgICAgIHdpZHRoOiBwYWdlSWNvblNpemVbMF0sXG4gICAgICAgIGhlaWdodDogcGFnZUljb25TaXplWzFdXG4gICAgICB9KTtcbiAgICAgIGljb24ubmFtZSA9IG5hbWU7XG4gICAgICBjb250cm9sbGVyR3JvdXAuYWRkKGljb24pO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQG92ZXJyaWRlXG4gICAqL1xuICBsYXlvdXRJbm5lcjogZnVuY3Rpb24gKGxlZ2VuZE1vZGVsLCBpdGVtQWxpZ24sIG1heFNpemUsIGlzRmlyc3RSZW5kZXIpIHtcbiAgICB2YXIgY29udGVudEdyb3VwID0gdGhpcy5nZXRDb250ZW50R3JvdXAoKTtcbiAgICB2YXIgY29udGFpbmVyR3JvdXAgPSB0aGlzLl9jb250YWluZXJHcm91cDtcbiAgICB2YXIgY29udHJvbGxlckdyb3VwID0gdGhpcy5fY29udHJvbGxlckdyb3VwO1xuICAgIHZhciBvcmllbnRJZHggPSBsZWdlbmRNb2RlbC5nZXRPcmllbnQoKS5pbmRleDtcbiAgICB2YXIgd2ggPSBXSFtvcmllbnRJZHhdO1xuICAgIHZhciBodyA9IFdIWzEgLSBvcmllbnRJZHhdO1xuICAgIHZhciB5eCA9IFhZWzEgLSBvcmllbnRJZHhdOyAvLyBQbGFjZSBpdGVtcyBpbiBjb250ZW50R3JvdXAuXG5cbiAgICBsYXlvdXRVdGlsLmJveChsZWdlbmRNb2RlbC5nZXQoJ29yaWVudCcpLCBjb250ZW50R3JvdXAsIGxlZ2VuZE1vZGVsLmdldCgnaXRlbUdhcCcpLCAhb3JpZW50SWR4ID8gbnVsbCA6IG1heFNpemUud2lkdGgsIG9yaWVudElkeCA/IG51bGwgOiBtYXhTaXplLmhlaWdodCk7XG4gICAgbGF5b3V0VXRpbC5ib3goIC8vIEJ1dHRvbnMgaW4gY29udHJvbGxlciBhcmUgbGF5b3V0IGFsd2F5cyBob3Jpem9udGFsbHkuXG4gICAgJ2hvcml6b250YWwnLCBjb250cm9sbGVyR3JvdXAsIGxlZ2VuZE1vZGVsLmdldCgncGFnZUJ1dHRvbkl0ZW1HYXAnLCB0cnVlKSk7XG4gICAgdmFyIGNvbnRlbnRSZWN0ID0gY29udGVudEdyb3VwLmdldEJvdW5kaW5nUmVjdCgpO1xuICAgIHZhciBjb250cm9sbGVyUmVjdCA9IGNvbnRyb2xsZXJHcm91cC5nZXRCb3VuZGluZ1JlY3QoKTtcbiAgICB2YXIgc2hvd0NvbnRyb2xsZXIgPSB0aGlzLl9zaG93Q29udHJvbGxlciA9IGNvbnRlbnRSZWN0W3doXSA+IG1heFNpemVbd2hdO1xuICAgIHZhciBjb250ZW50UG9zID0gWy1jb250ZW50UmVjdC54LCAtY29udGVudFJlY3QueV07IC8vIFJlbWFpbiBjb250ZW50UG9zIHdoZW4gc2Nyb2xsIGFuaW1hdGlvbiBwZXJmcm9taW5nLlxuICAgIC8vIElmIGZpcnN0IHJlbmRlcmluZywgYGNvbnRlbnRHcm91cC5wb3NpdGlvbmAgaXMgWzAsIDBdLCB3aGljaFxuICAgIC8vIGRvZXMgbm90IG1ha2Ugc2Vuc2UgYW5kIG1heSBjYXVzZSB1bmV4ZXBjdGVkIGFuaW1hdGlvbiBpZiBhZG9wdGVkLlxuXG4gICAgaWYgKCFpc0ZpcnN0UmVuZGVyKSB7XG4gICAgICBjb250ZW50UG9zW29yaWVudElkeF0gPSBjb250ZW50R3JvdXAucG9zaXRpb25bb3JpZW50SWR4XTtcbiAgICB9IC8vIExheW91dCBjb250YWluZXIgZ3JvdXAgYmFzZWQgb24gMC5cblxuXG4gICAgdmFyIGNvbnRhaW5lclBvcyA9IFswLCAwXTtcbiAgICB2YXIgY29udHJvbGxlclBvcyA9IFstY29udHJvbGxlclJlY3QueCwgLWNvbnRyb2xsZXJSZWN0LnldO1xuICAgIHZhciBwYWdlQnV0dG9uR2FwID0genJVdGlsLnJldHJpZXZlMihsZWdlbmRNb2RlbC5nZXQoJ3BhZ2VCdXR0b25HYXAnLCB0cnVlKSwgbGVnZW5kTW9kZWwuZ2V0KCdpdGVtR2FwJywgdHJ1ZSkpOyAvLyBQbGFjZSBjb250YWluZXJHcm91cCBhbmQgY29udHJvbGxlckdyb3VwIGFuZCBjb250ZW50R3JvdXAuXG5cbiAgICBpZiAoc2hvd0NvbnRyb2xsZXIpIHtcbiAgICAgIHZhciBwYWdlQnV0dG9uUG9zaXRpb24gPSBsZWdlbmRNb2RlbC5nZXQoJ3BhZ2VCdXR0b25Qb3NpdGlvbicsIHRydWUpOyAvLyBjb250cm9sbGVyIGlzIG9uIHRoZSByaWdodCAvIGJvdHRvbS5cblxuICAgICAgaWYgKHBhZ2VCdXR0b25Qb3NpdGlvbiA9PT0gJ2VuZCcpIHtcbiAgICAgICAgY29udHJvbGxlclBvc1tvcmllbnRJZHhdICs9IG1heFNpemVbd2hdIC0gY29udHJvbGxlclJlY3Rbd2hdO1xuICAgICAgfSAvLyBjb250cm9sbGVyIGlzIG9uIHRoZSBsZWZ0IC8gdG9wLlxuICAgICAgZWxzZSB7XG4gICAgICAgICAgY29udGFpbmVyUG9zW29yaWVudElkeF0gKz0gY29udHJvbGxlclJlY3Rbd2hdICsgcGFnZUJ1dHRvbkdhcDtcbiAgICAgICAgfVxuICAgIH0gLy8gQWx3YXlzIGFsaWduIGNvbnRyb2xsZXIgdG8gY29udGVudCBhcyAnbWlkZGxlJy5cblxuXG4gICAgY29udHJvbGxlclBvc1sxIC0gb3JpZW50SWR4XSArPSBjb250ZW50UmVjdFtod10gLyAyIC0gY29udHJvbGxlclJlY3RbaHddIC8gMjtcbiAgICBjb250ZW50R3JvdXAuYXR0cigncG9zaXRpb24nLCBjb250ZW50UG9zKTtcbiAgICBjb250YWluZXJHcm91cC5hdHRyKCdwb3NpdGlvbicsIGNvbnRhaW5lclBvcyk7XG4gICAgY29udHJvbGxlckdyb3VwLmF0dHIoJ3Bvc2l0aW9uJywgY29udHJvbGxlclBvcyk7IC8vIENhbGN1bGF0ZSBgbWFpblJlY3RgIGFuZCBzZXQgYGNsaXBQYXRoYC5cbiAgICAvLyBtYWluUmVjdCBzaG91bGQgbm90IGJlIGNhbGN1bGF0ZWQgYnkgYHRoaXMuZ3JvdXAuZ2V0Qm91bmRpbmdSZWN0KClgXG4gICAgLy8gZm9yIHNha2Ugb2YgdGhlIG92ZXJmbG93LlxuXG4gICAgdmFyIG1haW5SZWN0ID0gdGhpcy5ncm91cC5nZXRCb3VuZGluZ1JlY3QoKTtcbiAgICB2YXIgbWFpblJlY3QgPSB7XG4gICAgICB4OiAwLFxuICAgICAgeTogMFxuICAgIH07IC8vIENvbnNpZGVyIGNvbnRlbnQgbWF5IGJlIG92ZXJmbG93IChzaG91bGQgYmUgY2xpcHBlZCkuXG5cbiAgICBtYWluUmVjdFt3aF0gPSBzaG93Q29udHJvbGxlciA/IG1heFNpemVbd2hdIDogY29udGVudFJlY3Rbd2hdO1xuICAgIG1haW5SZWN0W2h3XSA9IE1hdGgubWF4KGNvbnRlbnRSZWN0W2h3XSwgY29udHJvbGxlclJlY3RbaHddKTsgLy8gYGNvbnRhaW5lclJlY3RbeXhdICsgY29udGFpbmVyUG9zWzEgLSBvcmllbnRJZHhdYCBpcyAwLlxuXG4gICAgbWFpblJlY3RbeXhdID0gTWF0aC5taW4oMCwgY29udHJvbGxlclJlY3RbeXhdICsgY29udHJvbGxlclBvc1sxIC0gb3JpZW50SWR4XSk7XG4gICAgY29udGFpbmVyR3JvdXAuX19yZWN0U2l6ZSA9IG1heFNpemVbd2hdO1xuXG4gICAgaWYgKHNob3dDb250cm9sbGVyKSB7XG4gICAgICB2YXIgY2xpcFNoYXBlID0ge1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAwXG4gICAgICB9O1xuICAgICAgY2xpcFNoYXBlW3doXSA9IE1hdGgubWF4KG1heFNpemVbd2hdIC0gY29udHJvbGxlclJlY3Rbd2hdIC0gcGFnZUJ1dHRvbkdhcCwgMCk7XG4gICAgICBjbGlwU2hhcGVbaHddID0gbWFpblJlY3RbaHddO1xuICAgICAgY29udGFpbmVyR3JvdXAuc2V0Q2xpcFBhdGgobmV3IGdyYXBoaWMuUmVjdCh7XG4gICAgICAgIHNoYXBlOiBjbGlwU2hhcGVcbiAgICAgIH0pKTsgLy8gQ29uc2lkZXIgY29udGVudCBtYXkgYmUgbGFyZ2VyIHRoYW4gY29udGFpbmVyLCBjb250YWluZXIgcmVjdFxuICAgICAgLy8gY2FuIG5vdCBiZSBvYnRhaW5lZCBmcm9tIGBjb250YWluZXJHcm91cC5nZXRCb3VuZGluZ1JlY3QoKWAuXG5cbiAgICAgIGNvbnRhaW5lckdyb3VwLl9fcmVjdFNpemUgPSBjbGlwU2hhcGVbd2hdO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBEbyBub3QgcmVtb3ZlIG9yIGlnbm9yZSBjb250cm9sbGVyLiBLZWVwIHRoZW0gc2V0IGFzIHBsYWNlIGhvbGRlcnMuXG4gICAgICBjb250cm9sbGVyR3JvdXAuZWFjaENoaWxkKGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgICBjaGlsZC5hdHRyKHtcbiAgICAgICAgICBpbnZpc2libGU6IHRydWUsXG4gICAgICAgICAgc2lsZW50OiB0cnVlXG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfSAvLyBDb250ZW50IHRyYW5zbGF0ZSBhbmltYXRpb24uXG5cblxuICAgIHZhciBwYWdlSW5mbyA9IHRoaXMuX2dldFBhZ2VJbmZvKGxlZ2VuZE1vZGVsKTtcblxuICAgIHBhZ2VJbmZvLnBhZ2VJbmRleCAhPSBudWxsICYmIGdyYXBoaWMudXBkYXRlUHJvcHMoY29udGVudEdyb3VwLCB7XG4gICAgICBwb3NpdGlvbjogcGFnZUluZm8uY29udGVudFBvc2l0aW9uXG4gICAgfSwgLy8gV2hlbiBzd2l0Y2ggZnJvbSBcInNob3cgY29udHJvbGxlclwiIHRvIFwibm90IHNob3cgY29udHJvbGxlclwiLCB2aWV3IHNob3VsZCBiZVxuICAgIC8vIHVwZGF0ZWQgaW1tZWRpYXRlbHkgd2l0aG91dCBhbmltYXRpb24sIG90aGVyd2lzZSBjYXVzZXMgd2VpcmQgZWZmZmVjdC5cbiAgICBzaG93Q29udHJvbGxlciA/IGxlZ2VuZE1vZGVsIDogZmFsc2UpO1xuXG4gICAgdGhpcy5fdXBkYXRlUGFnZUluZm9WaWV3KGxlZ2VuZE1vZGVsLCBwYWdlSW5mbyk7XG5cbiAgICByZXR1cm4gbWFpblJlY3Q7XG4gIH0sXG4gIF9wYWdlR286IGZ1bmN0aW9uICh0bywgbGVnZW5kTW9kZWwsIGFwaSkge1xuICAgIHZhciBzY3JvbGxEYXRhSW5kZXggPSB0aGlzLl9nZXRQYWdlSW5mbyhsZWdlbmRNb2RlbClbdG9dO1xuXG4gICAgc2Nyb2xsRGF0YUluZGV4ICE9IG51bGwgJiYgYXBpLmRpc3BhdGNoQWN0aW9uKHtcbiAgICAgIHR5cGU6ICdsZWdlbmRTY3JvbGwnLFxuICAgICAgc2Nyb2xsRGF0YUluZGV4OiBzY3JvbGxEYXRhSW5kZXgsXG4gICAgICBsZWdlbmRJZDogbGVnZW5kTW9kZWwuaWRcbiAgICB9KTtcbiAgfSxcbiAgX3VwZGF0ZVBhZ2VJbmZvVmlldzogZnVuY3Rpb24gKGxlZ2VuZE1vZGVsLCBwYWdlSW5mbykge1xuICAgIHZhciBjb250cm9sbGVyR3JvdXAgPSB0aGlzLl9jb250cm9sbGVyR3JvdXA7XG4gICAgenJVdGlsLmVhY2goWydwYWdlUHJldicsICdwYWdlTmV4dCddLCBmdW5jdGlvbiAobmFtZSkge1xuICAgICAgdmFyIGNhbkp1bXAgPSBwYWdlSW5mb1tuYW1lICsgJ0RhdGFJbmRleCddICE9IG51bGw7XG4gICAgICB2YXIgaWNvbiA9IGNvbnRyb2xsZXJHcm91cC5jaGlsZE9mTmFtZShuYW1lKTtcblxuICAgICAgaWYgKGljb24pIHtcbiAgICAgICAgaWNvbi5zZXRTdHlsZSgnZmlsbCcsIGNhbkp1bXAgPyBsZWdlbmRNb2RlbC5nZXQoJ3BhZ2VJY29uQ29sb3InLCB0cnVlKSA6IGxlZ2VuZE1vZGVsLmdldCgncGFnZUljb25JbmFjdGl2ZUNvbG9yJywgdHJ1ZSkpO1xuICAgICAgICBpY29uLmN1cnNvciA9IGNhbkp1bXAgPyAncG9pbnRlcicgOiAnZGVmYXVsdCc7XG4gICAgICB9XG4gICAgfSk7XG4gICAgdmFyIHBhZ2VUZXh0ID0gY29udHJvbGxlckdyb3VwLmNoaWxkT2ZOYW1lKCdwYWdlVGV4dCcpO1xuICAgIHZhciBwYWdlRm9ybWF0dGVyID0gbGVnZW5kTW9kZWwuZ2V0KCdwYWdlRm9ybWF0dGVyJyk7XG4gICAgdmFyIHBhZ2VJbmRleCA9IHBhZ2VJbmZvLnBhZ2VJbmRleDtcbiAgICB2YXIgY3VycmVudCA9IHBhZ2VJbmRleCAhPSBudWxsID8gcGFnZUluZGV4ICsgMSA6IDA7XG4gICAgdmFyIHRvdGFsID0gcGFnZUluZm8ucGFnZUNvdW50O1xuICAgIHBhZ2VUZXh0ICYmIHBhZ2VGb3JtYXR0ZXIgJiYgcGFnZVRleHQuc2V0U3R5bGUoJ3RleHQnLCB6clV0aWwuaXNTdHJpbmcocGFnZUZvcm1hdHRlcikgPyBwYWdlRm9ybWF0dGVyLnJlcGxhY2UoJ3tjdXJyZW50fScsIGN1cnJlbnQpLnJlcGxhY2UoJ3t0b3RhbH0nLCB0b3RhbCkgOiBwYWdlRm9ybWF0dGVyKHtcbiAgICAgIGN1cnJlbnQ6IGN1cnJlbnQsXG4gICAgICB0b3RhbDogdG90YWxcbiAgICB9KSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWx9IGxlZ2VuZE1vZGVsXG4gICAqIEByZXR1cm4ge09iamVjdH0ge1xuICAgKiAgY29udGVudFBvc2l0aW9uOiBBcnJheS48bnVtYmVyPiwgbnVsbCB3aGVuIGRhdGEgaXRlbSBub3QgZm91bmQuXG4gICAqICBwYWdlSW5kZXg6IG51bWJlciwgbnVsbCB3aGVuIGRhdGEgaXRlbSBub3QgZm91bmQuXG4gICAqICBwYWdlQ291bnQ6IG51bWJlciwgYWx3YXlzIGJlIGEgbnVtYmVyLCBjYW4gYmUgMC5cbiAgICogIHBhZ2VQcmV2RGF0YUluZGV4OiBudW1iZXIsIG51bGwgd2hlbiBubyBuZXh0IHBhZ2UuXG4gICAqICBwYWdlTmV4dERhdGFJbmRleDogbnVtYmVyLCBudWxsIHdoZW4gbm8gcHJldmlvdXMgcGFnZS5cbiAgICogfVxuICAgKi9cbiAgX2dldFBhZ2VJbmZvOiBmdW5jdGlvbiAobGVnZW5kTW9kZWwpIHtcbiAgICB2YXIgc2Nyb2xsRGF0YUluZGV4ID0gbGVnZW5kTW9kZWwuZ2V0KCdzY3JvbGxEYXRhSW5kZXgnLCB0cnVlKTtcbiAgICB2YXIgY29udGVudEdyb3VwID0gdGhpcy5nZXRDb250ZW50R3JvdXAoKTtcbiAgICB2YXIgY29udGFpbmVyUmVjdFNpemUgPSB0aGlzLl9jb250YWluZXJHcm91cC5fX3JlY3RTaXplO1xuICAgIHZhciBvcmllbnRJZHggPSBsZWdlbmRNb2RlbC5nZXRPcmllbnQoKS5pbmRleDtcbiAgICB2YXIgd2ggPSBXSFtvcmllbnRJZHhdO1xuICAgIHZhciB4eSA9IFhZW29yaWVudElkeF07XG5cbiAgICB2YXIgdGFyZ2V0SXRlbUluZGV4ID0gdGhpcy5fZmluZFRhcmdldEl0ZW1JbmRleChzY3JvbGxEYXRhSW5kZXgpO1xuXG4gICAgdmFyIGNoaWxkcmVuID0gY29udGVudEdyb3VwLmNoaWxkcmVuKCk7XG4gICAgdmFyIHRhcmdldEl0ZW0gPSBjaGlsZHJlblt0YXJnZXRJdGVtSW5kZXhdO1xuICAgIHZhciBpdGVtQ291bnQgPSBjaGlsZHJlbi5sZW5ndGg7XG4gICAgdmFyIHBDb3VudCA9ICFpdGVtQ291bnQgPyAwIDogMTtcbiAgICB2YXIgcmVzdWx0ID0ge1xuICAgICAgY29udGVudFBvc2l0aW9uOiBjb250ZW50R3JvdXAucG9zaXRpb24uc2xpY2UoKSxcbiAgICAgIHBhZ2VDb3VudDogcENvdW50LFxuICAgICAgcGFnZUluZGV4OiBwQ291bnQgLSAxLFxuICAgICAgcGFnZVByZXZEYXRhSW5kZXg6IG51bGwsXG4gICAgICBwYWdlTmV4dERhdGFJbmRleDogbnVsbFxuICAgIH07XG5cbiAgICBpZiAoIXRhcmdldEl0ZW0pIHtcbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfVxuXG4gICAgdmFyIHRhcmdldEl0ZW1JbmZvID0gZ2V0SXRlbUluZm8odGFyZ2V0SXRlbSk7XG4gICAgcmVzdWx0LmNvbnRlbnRQb3NpdGlvbltvcmllbnRJZHhdID0gLXRhcmdldEl0ZW1JbmZvLnM7IC8vIFN0cmF0ZWd5OlxuICAgIC8vICgxKSBBbHdheXMgYWxpZ24gYmFzZWQgb24gdGhlIGxlZnQvdG9wIG1vc3QgaXRlbS5cbiAgICAvLyAoMikgSXQgaXMgdXNlci1mcmllbmRseSB0aGF0IHRoZSBsYXN0IGl0ZW0gc2hvd24gaW4gdGhlXG4gICAgLy8gY3VycmVudCB3aW5kb3cgaXMgc2hvd24gYXQgdGhlIGJlZ2luaW5nIG9mIG5leHQgd2luZG93LlxuICAgIC8vIE90aGVyd2lzZSBpZiBoYWxmIG9mIHRoZSBsYXN0IGl0ZW0gaXMgY3V0IGJ5IHRoZSB3aW5kb3csXG4gICAgLy8gaXQgd2lsbCBoYXZlIG5vIGNoYW5jZSB0byBkaXNwbGF5IGVudGlyZWx5LlxuICAgIC8vICgzKSBDb25zaWRlciB0aGF0IGl0ZW0gc2l6ZSBwcm9iYWJseSBiZSBkaWZmZXJlbnQsIHdlXG4gICAgLy8gaGF2ZSBjYWxjdWxhdGUgcGFnZUluZGV4IGJ5IHNpemUgcmF0aGVyIHRoYW4gaXRlbSBpbmRleCxcbiAgICAvLyBhbmQgd2UgY2FuIG5vdCBnZXQgcGFnZSBpbmRleCBkaXJlY3RseSBieSBkaXZpc2lvbi5cbiAgICAvLyAoNCkgVGhlIHdpbmRvdyBpcyB0byBuYXJyb3cgdG8gY29udGFpbiBtb3JlIHRoYW5cbiAgICAvLyBvbmUgaXRlbSwgd2Ugc2hvdWxkIG1ha2Ugc3VyZSB0aGF0IHRoZSBwYWdlIGNhbiBiZSBmbGlwZWQuXG5cbiAgICBmb3IgKHZhciBpID0gdGFyZ2V0SXRlbUluZGV4ICsgMSwgd2luU3RhcnRJdGVtSW5mbyA9IHRhcmdldEl0ZW1JbmZvLCB3aW5FbmRJdGVtSW5mbyA9IHRhcmdldEl0ZW1JbmZvLCBjdXJySXRlbUluZm8gPSBudWxsOyBpIDw9IGl0ZW1Db3VudDsgKytpKSB7XG4gICAgICBjdXJySXRlbUluZm8gPSBnZXRJdGVtSW5mbyhjaGlsZHJlbltpXSk7XG5cbiAgICAgIGlmICggLy8gSGFsZiBvZiB0aGUgbGFzdCBpdGVtIGlzIG91dCBvZiB0aGUgd2luZG93LlxuICAgICAgIWN1cnJJdGVtSW5mbyAmJiB3aW5FbmRJdGVtSW5mby5lID4gd2luU3RhcnRJdGVtSW5mby5zICsgY29udGFpbmVyUmVjdFNpemUgfHwgLy8gSWYgdGhlIGN1cnJlbnQgaXRlbSBkb2VzIG5vdCBpbnRlcnNlY3Qgd2l0aCB0aGUgd2luZG93LCB0aGUgbmV3IHBhZ2VcbiAgICAgIC8vIGNhbiBiZSBzdGFydGVkIGF0IHRoZSBjdXJyZW50IGl0ZW0gb3IgdGhlIGxhc3QgaXRlbS5cbiAgICAgIGN1cnJJdGVtSW5mbyAmJiAhaW50ZXJzZWN0KGN1cnJJdGVtSW5mbywgd2luU3RhcnRJdGVtSW5mby5zKSkge1xuICAgICAgICBpZiAod2luRW5kSXRlbUluZm8uaSA+IHdpblN0YXJ0SXRlbUluZm8uaSkge1xuICAgICAgICAgIHdpblN0YXJ0SXRlbUluZm8gPSB3aW5FbmRJdGVtSW5mbztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBlLmcuLCB3aGVuIHBhZ2Ugc2l6ZSBpcyBzbWFsbGVyIHRoYW4gaXRlbSBzaXplLlxuICAgICAgICAgIHdpblN0YXJ0SXRlbUluZm8gPSBjdXJySXRlbUluZm87XG4gICAgICAgIH1cblxuICAgICAgICBpZiAod2luU3RhcnRJdGVtSW5mbykge1xuICAgICAgICAgIGlmIChyZXN1bHQucGFnZU5leHREYXRhSW5kZXggPT0gbnVsbCkge1xuICAgICAgICAgICAgcmVzdWx0LnBhZ2VOZXh0RGF0YUluZGV4ID0gd2luU3RhcnRJdGVtSW5mby5pO1xuICAgICAgICAgIH1cblxuICAgICAgICAgICsrcmVzdWx0LnBhZ2VDb3VudDtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB3aW5FbmRJdGVtSW5mbyA9IGN1cnJJdGVtSW5mbztcbiAgICB9XG5cbiAgICBmb3IgKHZhciBpID0gdGFyZ2V0SXRlbUluZGV4IC0gMSwgd2luU3RhcnRJdGVtSW5mbyA9IHRhcmdldEl0ZW1JbmZvLCB3aW5FbmRJdGVtSW5mbyA9IHRhcmdldEl0ZW1JbmZvLCBjdXJySXRlbUluZm8gPSBudWxsOyBpID49IC0xOyAtLWkpIHtcbiAgICAgIGN1cnJJdGVtSW5mbyA9IGdldEl0ZW1JbmZvKGNoaWxkcmVuW2ldKTtcblxuICAgICAgaWYgKCAvLyBJZiB0aGUgdGhlIGVuZCBpdGVtIGRvZXMgbm90IGludGVyc2VjdCB3aXRoIHRoZSB3aW5kb3cgc3RhcnRlZFxuICAgICAgLy8gZnJvbSB0aGUgY3VycmVudCBpdGVtLCBhIHBhZ2UgY2FuIGJlIHNldHRsZWQuXG4gICAgICAoIWN1cnJJdGVtSW5mbyB8fCAhaW50ZXJzZWN0KHdpbkVuZEl0ZW1JbmZvLCBjdXJySXRlbUluZm8ucykpICYmIC8vIGUuZy4sIHdoZW4gcGFnZSBzaXplIGlzIHNtYWxsZXIgdGhhbiBpdGVtIHNpemUuXG4gICAgICB3aW5TdGFydEl0ZW1JbmZvLmkgPCB3aW5FbmRJdGVtSW5mby5pKSB7XG4gICAgICAgIHdpbkVuZEl0ZW1JbmZvID0gd2luU3RhcnRJdGVtSW5mbztcblxuICAgICAgICBpZiAocmVzdWx0LnBhZ2VQcmV2RGF0YUluZGV4ID09IG51bGwpIHtcbiAgICAgICAgICByZXN1bHQucGFnZVByZXZEYXRhSW5kZXggPSB3aW5TdGFydEl0ZW1JbmZvLmk7XG4gICAgICAgIH1cblxuICAgICAgICArK3Jlc3VsdC5wYWdlQ291bnQ7XG4gICAgICAgICsrcmVzdWx0LnBhZ2VJbmRleDtcbiAgICAgIH1cblxuICAgICAgd2luU3RhcnRJdGVtSW5mbyA9IGN1cnJJdGVtSW5mbztcbiAgICB9XG5cbiAgICByZXR1cm4gcmVzdWx0O1xuXG4gICAgZnVuY3Rpb24gZ2V0SXRlbUluZm8oZWwpIHtcbiAgICAgIGlmIChlbCkge1xuICAgICAgICB2YXIgaXRlbVJlY3QgPSBlbC5nZXRCb3VuZGluZ1JlY3QoKTtcbiAgICAgICAgdmFyIHN0YXJ0ID0gaXRlbVJlY3RbeHldICsgZWwucG9zaXRpb25bb3JpZW50SWR4XTtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBzOiBzdGFydCxcbiAgICAgICAgICBlOiBzdGFydCArIGl0ZW1SZWN0W3doXSxcbiAgICAgICAgICBpOiBlbC5fX2xlZ2VuZERhdGFJbmRleFxuICAgICAgICB9O1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGludGVyc2VjdChpdGVtSW5mbywgd2luU3RhcnQpIHtcbiAgICAgIHJldHVybiBpdGVtSW5mby5lID49IHdpblN0YXJ0ICYmIGl0ZW1JbmZvLnMgPD0gd2luU3RhcnQgKyBjb250YWluZXJSZWN0U2l6ZTtcbiAgICB9XG4gIH0sXG4gIF9maW5kVGFyZ2V0SXRlbUluZGV4OiBmdW5jdGlvbiAodGFyZ2V0RGF0YUluZGV4KSB7XG4gICAgdmFyIGluZGV4O1xuICAgIHZhciBjb250ZW50R3JvdXAgPSB0aGlzLmdldENvbnRlbnRHcm91cCgpO1xuXG4gICAgaWYgKHRoaXMuX3Nob3dDb250cm9sbGVyKSB7XG4gICAgICBjb250ZW50R3JvdXAuZWFjaENoaWxkKGZ1bmN0aW9uIChjaGlsZCwgaWR4KSB7XG4gICAgICAgIGlmIChjaGlsZC5fX2xlZ2VuZERhdGFJbmRleCA9PT0gdGFyZ2V0RGF0YUluZGV4KSB7XG4gICAgICAgICAgaW5kZXggPSBpZHg7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBpbmRleCA9IDA7XG4gICAgfVxuXG4gICAgcmV0dXJuIGluZGV4O1xuICB9XG59KTtcbnZhciBfZGVmYXVsdCA9IFNjcm9sbGFibGVMZWdlbmRWaWV3O1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUEvVkE7QUFpV0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/legend/ScrollableLegendView.js
