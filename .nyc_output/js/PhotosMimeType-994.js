
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

var PhotosMimeType = {
  isImage: function isImage(mimeString) {
    return getParts(mimeString)[0] === 'image';
  },
  isJpeg: function isJpeg(mimeString) {
    var parts = getParts(mimeString);
    return PhotosMimeType.isImage(mimeString) && ( // see http://fburl.com/10972194
    parts[1] === 'jpeg' || parts[1] === 'pjpeg');
  }
};

function getParts(mimeString) {
  return mimeString.split('/');
}

module.exports = PhotosMimeType;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvUGhvdG9zTWltZVR5cGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9mYmpzL2xpYi9QaG90b3NNaW1lVHlwZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICovXG52YXIgUGhvdG9zTWltZVR5cGUgPSB7XG4gIGlzSW1hZ2U6IGZ1bmN0aW9uIGlzSW1hZ2UobWltZVN0cmluZykge1xuICAgIHJldHVybiBnZXRQYXJ0cyhtaW1lU3RyaW5nKVswXSA9PT0gJ2ltYWdlJztcbiAgfSxcbiAgaXNKcGVnOiBmdW5jdGlvbiBpc0pwZWcobWltZVN0cmluZykge1xuICAgIHZhciBwYXJ0cyA9IGdldFBhcnRzKG1pbWVTdHJpbmcpO1xuICAgIHJldHVybiBQaG90b3NNaW1lVHlwZS5pc0ltYWdlKG1pbWVTdHJpbmcpICYmIChcbiAgICAvLyBzZWUgaHR0cDovL2ZidXJsLmNvbS8xMDk3MjE5NFxuICAgIHBhcnRzWzFdID09PSAnanBlZycgfHwgcGFydHNbMV0gPT09ICdwanBlZycpO1xuICB9XG59O1xuXG5mdW5jdGlvbiBnZXRQYXJ0cyhtaW1lU3RyaW5nKSB7XG4gIHJldHVybiBtaW1lU3RyaW5nLnNwbGl0KCcvJyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gUGhvdG9zTWltZVR5cGU7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUVBOzs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQVRBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/PhotosMimeType.js
