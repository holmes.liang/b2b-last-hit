__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}





var Group = function Group(props) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_2__["ConfigConsumer"], null, function (_ref) {
    var _classNames;

    var getPrefixCls = _ref.getPrefixCls;
    var customizePrefixCls = props.prefixCls,
        _props$className = props.className,
        className = _props$className === void 0 ? '' : _props$className;
    var prefixCls = getPrefixCls('input-group', customizePrefixCls);
    var cls = classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-lg"), props.size === 'large'), _defineProperty(_classNames, "".concat(prefixCls, "-sm"), props.size === 'small'), _defineProperty(_classNames, "".concat(prefixCls, "-compact"), props.compact), _classNames), className);
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
      className: cls,
      style: props.style,
      onMouseEnter: props.onMouseEnter,
      onMouseLeave: props.onMouseLeave,
      onFocus: props.onFocus,
      onBlur: props.onBlur
    }, props.children);
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Group);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9pbnB1dC9Hcm91cC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvaW5wdXQvR3JvdXAuanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuY29uc3QgR3JvdXAgPSBwcm9wcyA9PiAoPENvbmZpZ0NvbnN1bWVyPlxuICAgIHsoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgIGNvbnN0IHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIGNsYXNzTmFtZSA9ICcnIH0gPSBwcm9wcztcbiAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ2lucHV0LWdyb3VwJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICBjb25zdCBjbHMgPSBjbGFzc05hbWVzKHByZWZpeENscywge1xuICAgICAgICBbYCR7cHJlZml4Q2xzfS1sZ2BdOiBwcm9wcy5zaXplID09PSAnbGFyZ2UnLFxuICAgICAgICBbYCR7cHJlZml4Q2xzfS1zbWBdOiBwcm9wcy5zaXplID09PSAnc21hbGwnLFxuICAgICAgICBbYCR7cHJlZml4Q2xzfS1jb21wYWN0YF06IHByb3BzLmNvbXBhY3QsXG4gICAgfSwgY2xhc3NOYW1lKTtcbiAgICByZXR1cm4gKDxzcGFuIGNsYXNzTmFtZT17Y2xzfSBzdHlsZT17cHJvcHMuc3R5bGV9IG9uTW91c2VFbnRlcj17cHJvcHMub25Nb3VzZUVudGVyfSBvbk1vdXNlTGVhdmU9e3Byb3BzLm9uTW91c2VMZWF2ZX0gb25Gb2N1cz17cHJvcHMub25Gb2N1c30gb25CbHVyPXtwcm9wcy5vbkJsdXJ9PlxuICAgICAgICAgIHtwcm9wcy5jaGlsZHJlbn1cbiAgICAgICAgPC9zcGFuPik7XG59fVxuICA8L0NvbmZpZ0NvbnN1bWVyPik7XG5leHBvcnQgZGVmYXVsdCBHcm91cDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFUQTtBQUFBO0FBQ0E7QUFhQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/input/Group.js
