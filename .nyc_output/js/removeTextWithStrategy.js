/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule removeTextWithStrategy
 * @format
 * 
 */


var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");
/**
 * For a collapsed selection state, remove text based on the specified strategy.
 * If the selection state is not collapsed, remove the entire selected range.
 */


function removeTextWithStrategy(editorState, strategy, direction) {
  var selection = editorState.getSelection();
  var content = editorState.getCurrentContent();
  var target = selection;

  if (selection.isCollapsed()) {
    if (direction === 'forward') {
      if (editorState.isSelectionAtEndOfContent()) {
        return content;
      }
    } else if (editorState.isSelectionAtStartOfContent()) {
      return content;
    }

    target = strategy(editorState);

    if (target === selection) {
      return content;
    }
  }

  return DraftModifier.removeRange(content, target, direction);
}

module.exports = removeTextWithStrategy;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL3JlbW92ZVRleHRXaXRoU3RyYXRlZ3kuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvcmVtb3ZlVGV4dFdpdGhTdHJhdGVneS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIHJlbW92ZVRleHRXaXRoU3RyYXRlZ3lcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcblxuLyoqXG4gKiBGb3IgYSBjb2xsYXBzZWQgc2VsZWN0aW9uIHN0YXRlLCByZW1vdmUgdGV4dCBiYXNlZCBvbiB0aGUgc3BlY2lmaWVkIHN0cmF0ZWd5LlxuICogSWYgdGhlIHNlbGVjdGlvbiBzdGF0ZSBpcyBub3QgY29sbGFwc2VkLCByZW1vdmUgdGhlIGVudGlyZSBzZWxlY3RlZCByYW5nZS5cbiAqL1xuZnVuY3Rpb24gcmVtb3ZlVGV4dFdpdGhTdHJhdGVneShlZGl0b3JTdGF0ZSwgc3RyYXRlZ3ksIGRpcmVjdGlvbikge1xuICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gIHZhciBjb250ZW50ID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgdmFyIHRhcmdldCA9IHNlbGVjdGlvbjtcbiAgaWYgKHNlbGVjdGlvbi5pc0NvbGxhcHNlZCgpKSB7XG4gICAgaWYgKGRpcmVjdGlvbiA9PT0gJ2ZvcndhcmQnKSB7XG4gICAgICBpZiAoZWRpdG9yU3RhdGUuaXNTZWxlY3Rpb25BdEVuZE9mQ29udGVudCgpKSB7XG4gICAgICAgIHJldHVybiBjb250ZW50O1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoZWRpdG9yU3RhdGUuaXNTZWxlY3Rpb25BdFN0YXJ0T2ZDb250ZW50KCkpIHtcbiAgICAgIHJldHVybiBjb250ZW50O1xuICAgIH1cblxuICAgIHRhcmdldCA9IHN0cmF0ZWd5KGVkaXRvclN0YXRlKTtcbiAgICBpZiAodGFyZ2V0ID09PSBzZWxlY3Rpb24pIHtcbiAgICAgIHJldHVybiBjb250ZW50O1xuICAgIH1cbiAgfVxuICByZXR1cm4gRHJhZnRNb2RpZmllci5yZW1vdmVSYW5nZShjb250ZW50LCB0YXJnZXQsIGRpcmVjdGlvbik7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gcmVtb3ZlVGV4dFdpdGhTdHJhdGVneTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/removeTextWithStrategy.js
