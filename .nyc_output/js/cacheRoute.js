/* WEBPACK VAR INJECTION */(function(global) {(function (global, factory) {
   true ? factory(exports, __webpack_require__(/*! react */ "./node_modules/react/index.js"), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"), __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js")) : undefined;
})(this, function (exports, React, PropTypes, reactRouterDom) {
  'use strict';

  var React__default = 'default' in React ? React['default'] : React;
  PropTypes = PropTypes && PropTypes.hasOwnProperty('default') ? PropTypes['default'] : PropTypes; // 值类型判断 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  var isUndefined = function isUndefined(val) {
    return typeof val === 'undefined';
  };

  var isNull = function isNull(val) {
    return val === null;
  };

  var isFunction = function isFunction(val) {
    return typeof val === 'function';
  };

  var isString = function isString(val) {
    return typeof val === 'string';
  };

  var isExist = function isExist(val) {
    return !(isUndefined(val) || isNull(val));
  };

  var isArray = function isArray(val) {
    return val instanceof Array;
  };

  var isNaN = function isNaN(val) {
    return val !== val;
  };

  var isNumber = function isNumber(val) {
    return typeof val === 'number' && !isNaN(val);
  }; // 值类型判断 -------------------------------------------------------------


  var get = function get(obj) {
    var keys = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    var defaultValue = arguments[2];

    try {
      if (isNumber(keys)) {
        keys = String(keys);
      }

      var result = (isString(keys) ? keys.split('.') : keys).reduce(function (res, key) {
        return res[key];
      }, obj);
      return isUndefined(result) ? defaultValue : result;
    } catch (e) {
      return defaultValue;
    }
  };

  var run = function run(obj) {
    for (var _len = arguments.length, args = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
      args[_key - 2] = arguments[_key];
    }

    var keys = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    keys = isString(keys) ? keys.split('.') : keys;
    var func = get(obj, keys);
    var context = get(obj, keys.slice(0, -1));
    return isFunction(func) ? func.call.apply(func, [context].concat(args)) : func;
  };

  var value = function value() {
    for (var _len2 = arguments.length, values = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      values[_key2] = arguments[_key2];
    }

    return values.reduce(function (value, nextValue) {
      return isUndefined(value) ? run(nextValue) : run(value);
    }, undefined);
  };

  var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  };

  var classCallCheck = function classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  };

  var createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var defineProperty = function defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  };

  var _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  var inherits = function inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  };

  var objectWithoutProperties = function objectWithoutProperties(obj, keys) {
    var target = {};

    for (var i in obj) {
      if (keys.indexOf(i) >= 0) continue;
      if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
      target[i] = obj[i];
    }

    return target;
  };

  var possibleConstructorReturn = function possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  };

  var slicedToArray = function () {
    function sliceIterator(arr, i) {
      var _arr = [];
      var _n = true;
      var _d = false;
      var _e = undefined;

      try {
        for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
          _arr.push(_s.value);

          if (i && _arr.length === i) break;
        }
      } catch (err) {
        _d = true;
        _e = err;
      } finally {
        try {
          if (!_n && _i["return"]) _i["return"]();
        } finally {
          if (_d) throw _e;
        }
      }

      return _arr;
    }

    return function (arr, i) {
      if (Array.isArray(arr)) {
        return arr;
      } else if (Symbol.iterator in Object(arr)) {
        return sliceIterator(arr, i);
      } else {
        throw new TypeError("Invalid attempt to destructure non-iterable instance");
      }
    };
  }();

  var toConsumableArray = function toConsumableArray(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }

      return arr2;
    } else {
      return Array.from(arr);
    }
  };

  var getImplementation = function getImplementation() {
    if (typeof self !== 'undefined') {
      return self;
    }

    if (typeof window !== 'undefined') {
      return window;
    }

    if (typeof global !== 'undefined') {
      return global;
    }

    return Function('return this')();
  };

  var implementation = getImplementation();

  var getGlobal = function getGlobal() {
    if ((typeof global === 'undefined' ? 'undefined' : _typeof(global)) !== 'object' || !global || global.Math !== Math || global.Array !== Array) {
      return implementation;
    }

    return global;
  };

  var globalThis = getGlobal();

  var flatten = function flatten(array) {
    return array.reduce(function (res, item) {
      return [].concat(toConsumableArray(res), toConsumableArray(isArray(item) ? flatten(item) : [item]));
    }, []);
  };
  /**
   * [钳子] 用来将数字限制在给定范围内
   * @param {Number} value 被限制值
   * @param {Number} min 最小值
   * @param {Number} max 最大值
   */


  var clamp = function clamp(value, min) {
    var max = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : Number.MAX_VALUE;

    if (value < min) {
      return min;
    }

    if (value > max) {
      return max;
    }

    return value;
  };

  var body = get(globalThis, 'document.body');
  var screenScrollingElement = get(globalThis, 'document.scrollingElement', get(globalThis, 'document.documentElement', {}));

  function isScrollableNode() {
    var node = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    if (!isExist(node)) {
      return false;
    }

    return node.scrollWidth > node.clientWidth || node.scrollHeight > node.clientHeight;
  }

  function getScrollableNodes(from) {
    if (!isFunction(get(globalThis, 'document.getElementById'))) {
      return [];
    }

    return [].concat(toConsumableArray(value(run(from, 'querySelectorAll', '*'), [])), [from]).filter(isScrollableNode);
  }

  function saveScrollPosition(from) {
    var nodes = [].concat(toConsumableArray(new Set([].concat(toConsumableArray(flatten((!isArray(from) ? [from] : from).map(getScrollableNodes))), toConsumableArray([screenScrollingElement, body].filter(isScrollableNode))))));
    var saver = nodes.map(function (node) {
      return [node, {
        x: node.scrollLeft,
        y: node.scrollTop
      }];
    });
    return function revert() {
      saver.forEach(function (_ref) {
        var _ref2 = slicedToArray(_ref, 2),
            node = _ref2[0],
            _ref2$ = _ref2[1],
            x = _ref2$.x,
            y = _ref2$.y;

        node.scrollLeft = x;
        node.scrollTop = y;
      });
    };
  }

  var __components = {};

  var getCachedComponentEntries = function getCachedComponentEntries() {
    return Object.entries(__components).filter(function (_ref) {
      var _ref2 = slicedToArray(_ref, 2),
          cache = _ref2[1];

      return cache instanceof CacheComponent ? cache.state.cached : Object.values(cache).some(function (cache) {
        return cache.state.cached;
      });
    });
  };

  var getCache = function getCache() {
    return _extends({}, __components);
  };

  var register = function register(key, component) {
    __components[key] = component;
  };

  var remove = function remove(key) {
    delete __components[key];
  };

  var dropComponent = function dropComponent(component) {
    return run(component, 'reset');
  };

  var dropByCacheKey = function dropByCacheKey(key) {
    var cache = get(__components, [key]);

    if (!cache) {
      return;
    }

    if (cache instanceof CacheComponent) {
      dropComponent(cache);
    } else {
      Object.values(cache).forEach(dropComponent);
    }
  };

  var clearCache = function clearCache() {
    getCachedComponentEntries().forEach(function (_ref3) {
      var _ref4 = slicedToArray(_ref3, 1),
          key = _ref4[0];

      return dropByCacheKey(key);
    });
  };

  var getCachingKeys = function getCachingKeys() {
    return getCachedComponentEntries().map(function (_ref5) {
      var _ref6 = slicedToArray(_ref5, 1),
          key = _ref6[0];

      return key;
    });
  };

  var getCachingComponents = function getCachingComponents() {
    return getCachedComponentEntries().reduce(function (res, _ref7) {
      var _ref8 = slicedToArray(_ref7, 2),
          key = _ref8[0],
          cache = _ref8[1];

      return _extends({}, res, cache instanceof CacheComponent ? defineProperty({}, key, cache) : Object.entries(cache).reduce(function (res, _ref10) {
        var _ref11 = slicedToArray(_ref10, 2),
            pathname = _ref11[0],
            cache = _ref11[1];

        return _extends({}, res, defineProperty({}, key + '.' + pathname, cache));
      }, {}));
    }, {});
  };

  var isUsingNewLifecycle = isExist(React__default.forwardRef);
  var COMPUTED_UNMATCH_KEY = '__isComputedUnmatch';

  var isMatch = function isMatch(match) {
    return isExist(match) && get(match, COMPUTED_UNMATCH_KEY) !== true;
  };

  var getDerivedStateFromProps = function getDerivedStateFromProps(nextProps, prevState) {
    var nextPropsMatch = nextProps.match,
        _nextProps$when = nextProps.when,
        when = _nextProps$when === undefined ? 'forward' : _nextProps$when;
    /**
     * Note:
     * Turn computedMatch from CacheSwitch to a real null value
     *
     * 将 CacheSwitch 计算得到的 computedMatch 值转换为真正的 null
     */

    if (!isMatch(nextPropsMatch)) {
      nextPropsMatch = null;
    }

    if (!prevState.cached && nextPropsMatch) {
      return {
        cached: true,
        matched: true
      };
    }
    /**
     * Determines whether it needs to cancel the cache based on the next unmatched props action
     *
     * 根据下个未匹配状态动作决定是否需要取消缓存
     */


    if (prevState.matched && !nextPropsMatch) {
      var nextAction = get(nextProps, 'history.action');
      var __cancel__cache = false;

      if (isFunction(when)) {
        __cancel__cache = !when(nextProps);
      } else {
        switch (when) {
          case 'always':
            break;

          case 'back':
            if (['PUSH', 'REPLACE'].includes(nextAction)) {
              __cancel__cache = true;
            }

            break;

          case 'forward':
          default:
            if (nextAction === 'POP') {
              __cancel__cache = true;
            }

        }
      }

      if (__cancel__cache) {
        return {
          cached: false,
          matched: false
        };
      }
    }

    return {
      matched: !!nextPropsMatch
    };
  };

  var CacheComponent = function (_Component) {
    inherits(CacheComponent, _Component);

    function CacheComponent(props) {
      var _ref;

      classCallCheck(this, CacheComponent);

      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      var _this = possibleConstructorReturn(this, (_ref = CacheComponent.__proto__ || Object.getPrototypeOf(CacheComponent)).call.apply(_ref, [this, props].concat(args)));

      _this.cacheLifecycles = {
        __listener: {},
        didCache: function didCache(listener) {
          _this.cacheLifecycles.__listener['didCache'] = listener;
        },
        didRecover: function didRecover(listener) {
          _this.cacheLifecycles.__listener['didRecover'] = listener;
        }
        /**
         * New lifecycle for replacing the `componentWillReceiveProps` in React 16.3 +
         * React 16.3 + 版本中替代 componentWillReceiveProps 的新生命周期
         */

      };
      _this.componentWillReceiveProps = !isUsingNewLifecycle ? function (nextProps) {
        var nextState = getDerivedStateFromProps(nextProps, _this.state);

        _this.setState(nextState);
      } : undefined;

      _this.injectDOM = function () {
        try {
          run(_this.__parentNode, 'insertBefore', _this.wrapper, _this.__placeholderNode);
          run(_this.__parentNode, 'removeChild', _this.__placeholderNode);
        } catch (err) {// nothing
        }
      };

      _this.ejectDOM = function () {
        try {
          var parentNode = get(_this.wrapper, 'parentNode');
          _this.__parentNode = parentNode;
          run(_this.__parentNode, 'insertBefore', _this.__placeholderNode, _this.wrapper);
          run(_this.__parentNode, 'removeChild', _this.wrapper);
        } catch (err) {// nothing
        }
      };

      _this.reset = function () {
        delete _this.__revertScrollPos;

        _this.setState({
          cached: false
        });
      };

      _this.__cacheCreateTime = Date.now();
      _this.__cacheUpdateTime = _this.__cacheCreateTime;

      if (props.cacheKey) {
        if (get(props.cacheKey, 'multiple')) {
          var _props$cacheKey = props.cacheKey,
              cacheKey = _props$cacheKey.cacheKey,
              pathname = _props$cacheKey.pathname;
          register(cacheKey, _extends({}, getCache()[cacheKey], defineProperty({}, pathname, _this)));
        } else {
          register(props.cacheKey, _this);
        }
      }

      if (typeof document !== 'undefined') {
        _this.__placeholderNode = document.createComment(' Route cached ' + (props.cacheKey ? 'with cacheKey: "' + get(props.cacheKey, 'cacheKey', props.cacheKey) + '" ' : ''));
      }

      _this.state = getDerivedStateFromProps(props, {
        cached: false,
        matched: false
      });
      return _this;
    }
    /**
     * Compatible React 16.3 -
     * 兼容 React 16.3 - 版本
     */


    createClass(CacheComponent, [{
      key: 'componentDidUpdate',
      value: function componentDidUpdate(prevProps, prevState) {
        if (!prevState.cached || !this.state.cached) {
          return;
        }

        if (prevState.matched === true && this.state.matched === false) {
          if (this.props.unmount) {
            this.ejectDOM();
          }

          this.__cacheUpdateTime = Date.now();
          return run(this, 'cacheLifecycles.__listener.didCache');
        }

        if (prevState.matched === false && this.state.matched === true) {
          if (this.props.saveScrollPosition) {
            run(this.__revertScrollPos);
          }

          this.__cacheUpdateTime = Date.now();
          return run(this, 'cacheLifecycles.__listener.didRecover');
        }
      }
    }, {
      key: 'shouldComponentUpdate',
      value: function shouldComponentUpdate(nextProps, nextState) {
        var willRecover = this.state.matched === false && nextState.matched === true;
        var willDrop = this.state.cached === true && nextState.cached === false;
        var shouldUpdate = this.state.matched || nextState.matched || this.state.cached !== nextState.cached;

        if (shouldUpdate) {
          if (this.props.unmount && willDrop || willRecover) {
            this.injectDOM();
          }

          if (!(willDrop || willRecover) && this.props.saveScrollPosition) {
            this.__revertScrollPos = saveScrollPosition(this.props.unmount ? this.wrapper : undefined);
          }
        }

        return shouldUpdate;
      }
    }, {
      key: 'componentWillUnmount',
      value: function componentWillUnmount() {
        var _props = this.props,
            cacheKeyConfig = _props.cacheKey,
            unmount = _props.unmount;

        if (get(cacheKeyConfig, 'multiple')) {
          var cacheKey = cacheKeyConfig.cacheKey,
              pathname = cacheKeyConfig.pathname;

          var cache = _extends({}, getCache()[cacheKey]);

          delete cache[pathname];

          if (Object.keys(cache).length === 0) {
            remove(cacheKey);
          } else {
            register(cacheKey, cache);
          }
        } else {
          remove(cacheKeyConfig);
        }

        if (unmount) {
          this.injectDOM();
        }
      }
    }, {
      key: 'render',
      value: function render() {
        var _this2 = this;

        var _state = this.state,
            matched = _state.matched,
            cached = _state.cached;
        var _props2 = this.props,
            _props2$className = _props2.className,
            propsClassName = _props2$className === undefined ? '' : _props2$className,
            behavior = _props2.behavior,
            children = _props2.children;

        var _value = value(run(behavior, undefined, !matched), {}),
            _value$className = _value.className,
            behaviorClassName = _value$className === undefined ? '' : _value$className,
            behaviorProps = objectWithoutProperties(_value, ['className']);

        var className = run(propsClassName + ' ' + behaviorClassName, 'trim');
        var hasClassName = className !== '';
        return cached ? React__default.createElement('div', _extends({
          className: hasClassName ? className : undefined
        }, behaviorProps, {
          ref: function ref(wrapper) {
            _this2.wrapper = wrapper;
          }
        }), run(children, undefined, this.cacheLifecycles)) : null;
      }
    }]);
    return CacheComponent;
  }(React.Component);

  CacheComponent.propsTypes = {
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    children: PropTypes.func.isRequired,
    className: PropTypes.string,
    when: PropTypes.oneOfType([PropTypes.func, PropTypes.oneOf(['forward', 'back', 'always'])]),
    behavior: PropTypes.func,
    unmount: PropTypes.bool,
    saveScrollPosition: PropTypes.bool
  };
  CacheComponent.defaultProps = {
    when: 'forward',
    unmount: false,
    saveScrollPosition: false,
    behavior: function behavior(cached) {
      return cached ? {
        style: {
          display: 'none'
        }
      } : undefined;
    }
  };
  CacheComponent.getDerivedStateFromProps = isUsingNewLifecycle ? getDerivedStateFromProps : undefined;

  var Updatable = function (_Component) {
    inherits(Updatable, _Component);

    function Updatable() {
      var _ref;

      var _temp, _this, _ret;

      classCallCheck(this, Updatable);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = Updatable.__proto__ || Object.getPrototypeOf(Updatable)).call.apply(_ref, [this].concat(args))), _this), _this.render = function () {
        return run(_this.props, 'children');
      }, _this.shouldComponentUpdate = function (_ref2) {
        var when = _ref2.when;
        return when;
      }, _temp), possibleConstructorReturn(_this, _ret);
    }

    return Updatable;
  }(React.Component);

  Updatable.propsTypes = {
    when: PropTypes.bool.isRequired
  };

  var isEmptyChildren = function isEmptyChildren(children) {
    return React__default.Children.count(children) === 0;
  };

  var isFragmentable = isExist(React.Fragment);

  var CacheRoute = function (_Component) {
    inherits(CacheRoute, _Component);

    function CacheRoute() {
      var _ref;

      var _temp, _this, _ret;

      classCallCheck(this, CacheRoute);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = CacheRoute.__proto__ || Object.getPrototypeOf(CacheRoute)).call.apply(_ref, [this].concat(args))), _this), _this.cache = {}, _temp), possibleConstructorReturn(_this, _ret);
    }

    createClass(CacheRoute, [{
      key: 'render',
      value: function render() {
        var _this2 = this;

        var _props = this.props,
            children = _props.children,
            render = _props.render,
            component = _props.component,
            className = _props.className,
            when = _props.when,
            behavior = _props.behavior,
            cacheKey = _props.cacheKey,
            unmount = _props.unmount,
            saveScrollPosition$$1 = _props.saveScrollPosition,
            computedMatchForCacheRoute = _props.computedMatchForCacheRoute,
            multiple = _props.multiple,
            restProps = objectWithoutProperties(_props, ['children', 'render', 'component', 'className', 'when', 'behavior', 'cacheKey', 'unmount', 'saveScrollPosition', 'computedMatchForCacheRoute', 'multiple']);
        /**
         * Note:
         * If children prop is a React Element, define the corresponding wrapper component for supporting multiple children
         *
         * 说明：如果 children 属性是 React Element 则定义对应的包裹组件以支持多个子组件
         */

        if (React__default.isValidElement(children) || !isEmptyChildren(children)) {
          render = function render() {
            return children;
          };
        }

        if (computedMatchForCacheRoute) {
          restProps.computedMatch = computedMatchForCacheRoute;
        }

        if (multiple && !isFragmentable) {
          multiple = false;
        }

        if (isNumber(multiple)) {
          multiple = clamp(multiple, 1);
        }

        return (
          /**
           * Only children prop of Route can help to control rendering behavior
           * 只有 Router 的 children 属性有助于主动控制渲染行为
           */
          React__default.createElement(reactRouterDom.Route, restProps, function (props) {
            var match = props.match,
                computedMatch = props.computedMatch,
                location = props.location;
            var isMatchCurrentRoute = isMatch(props.match);
            var currentPathname = location.pathname;
            var maxMultipleCount = isNumber(multiple) ? multiple : Infinity;
            var configProps = {
              when: when,
              className: className,
              behavior: behavior,
              cacheKey: cacheKey,
              unmount: unmount,
              saveScrollPosition: saveScrollPosition$$1
            };

            var renderSingle = function renderSingle(props) {
              return React__default.createElement(CacheComponent, props, function (cacheLifecycles) {
                return React__default.createElement(Updatable, {
                  when: isMatch(props.match)
                }, function () {
                  Object.assign(props, {
                    cacheLifecycles: cacheLifecycles
                  });

                  if (component) {
                    return React__default.createElement(component, props);
                  }

                  return run(render || children, undefined, props);
                });
              });
            };

            if (multiple && isMatchCurrentRoute) {
              _this2.cache[currentPathname] = {
                updateTime: Date.now(),
                render: renderSingle
              };
              Object.entries(_this2.cache).sort(function (_ref2, _ref3) {
                var _ref5 = slicedToArray(_ref2, 2),
                    prev = _ref5[1];

                var _ref4 = slicedToArray(_ref3, 2),
                    next = _ref4[1];

                return next.updateTime - prev.updateTime;
              }).forEach(function (_ref6, idx) {
                var _ref7 = slicedToArray(_ref6, 1),
                    pathname = _ref7[0];

                if (idx >= maxMultipleCount) {
                  delete _this2.cache[pathname];
                }
              });
            }

            return multiple ? React__default.createElement(React.Fragment, null, Object.entries(_this2.cache).map(function (_ref8) {
              var _ref9 = slicedToArray(_ref8, 2),
                  pathname = _ref9[0],
                  render = _ref9[1].render;

              var recomputedMatch = pathname === currentPathname ? match || computedMatch : null;
              return React__default.createElement(React.Fragment, {
                key: pathname
              }, render(_extends({}, props, configProps, {
                cacheKey: cacheKey ? {
                  cacheKey: cacheKey,
                  pathname: pathname,
                  multiple: true
                } : undefined,
                key: pathname,
                match: recomputedMatch
              })));
            })) : renderSingle(_extends({}, props, configProps));
          })
        );
      }
    }]);
    return CacheRoute;
  }(React.Component);

  CacheRoute.componentName = 'CacheRoute';
  CacheRoute.propTypes = {
    component: PropTypes.elementType || PropTypes.any,
    render: PropTypes.func,
    children: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
    computedMatchForCacheRoute: PropTypes.object,
    multiple: PropTypes.oneOfType([PropTypes.bool, PropTypes.number])
  };
  CacheRoute.defaultProps = {
    multiple: false
  };

  function getFragment() {
    if (isExist(React.Fragment)) {
      return function (_ref) {
        var children = _ref.children;
        return React__default.createElement(React.Fragment, null, children);
      };
    }

    if (isExist(React.PropTypes)) {
      return function (_ref2) {
        var children = _ref2.children;
        return React__default.createElement('div', null, children);
      };
    }

    return function (_ref3) {
      var children = _ref3.children;
      return children;
    };
  }

  var SwitchFragment = getFragment();
  SwitchFragment.displayName = 'SwitchFragment';
  var isUsingNewContext = isExist(reactRouterDom.__RouterContext);

  var CacheSwitch = function (_Switch) {
    inherits(CacheSwitch, _Switch);

    function CacheSwitch() {
      var _ref;

      var _temp, _this, _ret;

      classCallCheck(this, CacheSwitch);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = CacheSwitch.__proto__ || Object.getPrototypeOf(CacheSwitch)).call.apply(_ref, [this].concat(args))), _this), _this.getContext = function () {
        if (isUsingNewContext) {
          var _this$props = _this.props,
              location = _this$props.location,
              match = _this$props.match;
          return {
            location: location,
            match: match
          };
        } else {
          var route = _this.context.router.route;

          var _location = _this.props.location || route.location;

          return {
            location: _location,
            match: route.match
          };
        }
      }, _temp), possibleConstructorReturn(_this, _ret);
    }

    createClass(CacheSwitch, [{
      key: 'render',
      value: function render() {
        var _props = this.props,
            children = _props.children,
            which = _props.which;

        var _getContext = this.getContext(),
            location = _getContext.location,
            contextMatch = _getContext.match;

        var __matchedAlready = false;
        return React__default.createElement(Updatable, {
          when: isMatch(contextMatch)
        }, function () {
          return React__default.createElement(SwitchFragment, null, React__default.Children.map(children, function (element) {
            if (!React__default.isValidElement(element)) {
              return null;
            }

            var path = element.props.path || element.props.from;
            var match = __matchedAlready ? null : path ? reactRouterDom.matchPath(location.pathname, _extends({}, element.props, {
              path: path
            }), contextMatch) : contextMatch;
            var child = void 0;

            if (which(element)) {
              child = React__default.cloneElement(element, _extends({
                location: location,
                computedMatch: match
              }, isNull(match) ? {
                computedMatchForCacheRoute: defineProperty({}, COMPUTED_UNMATCH_KEY, true)
              } : null));
            } else {
              child = match && !__matchedAlready ? React__default.cloneElement(element, {
                location: location,
                computedMatch: match
              }) : null;
            }

            if (!__matchedAlready) {
              __matchedAlready = !!match;
            }

            return child;
          }));
        });
      }
    }]);
    return CacheSwitch;
  }(reactRouterDom.Switch);

  if (isUsingNewContext) {
    CacheSwitch.propTypes = {
      children: PropTypes.node,
      location: PropTypes.object.isRequired,
      match: PropTypes.object.isRequired,
      which: PropTypes.func
    };
    CacheSwitch = reactRouterDom.withRouter(CacheSwitch);
  } else {
    CacheSwitch.contextTypes = {
      router: PropTypes.shape({
        route: PropTypes.object.isRequired
      }).isRequired
    };
    CacheSwitch.propTypes = {
      children: PropTypes.node,
      location: PropTypes.object,
      which: PropTypes.func
    };
  }

  CacheSwitch.defaultProps = {
    which: function which(element) {
      return get(element, 'type') === CacheRoute;
    }
  };
  var CacheSwitch$1 = CacheSwitch;
  exports.default = CacheRoute;
  exports.CacheRoute = CacheRoute;
  exports.CacheSwitch = CacheSwitch$1;
  exports.dropByCacheKey = dropByCacheKey;
  exports.getCachingKeys = getCachingKeys;
  exports.clearCache = clearCache;
  exports.getCachingComponents = getCachingComponents;
  Object.defineProperty(exports, '__esModule', {
    value: true
  });
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWNhY2hlLXJvdXRlL2Rpc3QvY2FjaGVSb3V0ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1jYWNoZS1yb3V0ZS9kaXN0L2NhY2hlUm91dGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIChnbG9iYWwsIGZhY3RvcnkpIHtcbiAgdHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnID8gZmFjdG9yeShleHBvcnRzLCByZXF1aXJlKCdyZWFjdCcpLCByZXF1aXJlKCdwcm9wLXR5cGVzJyksIHJlcXVpcmUoJ3JlYWN0LXJvdXRlci1kb20nKSkgOlxuICB0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQgPyBkZWZpbmUoWydleHBvcnRzJywgJ3JlYWN0JywgJ3Byb3AtdHlwZXMnLCAncmVhY3Qtcm91dGVyLWRvbSddLCBmYWN0b3J5KSA6XG4gIChmYWN0b3J5KChnbG9iYWwuQ2FjaGVSb3V0ZSA9IHt9KSxnbG9iYWwuUmVhY3QsZ2xvYmFsLlByb3BUeXBlcyxnbG9iYWwucmVhY3RSb3V0ZXJEb20pKTtcbn0odGhpcywgKGZ1bmN0aW9uIChleHBvcnRzLFJlYWN0LFByb3BUeXBlcyxyZWFjdFJvdXRlckRvbSkgeyAndXNlIHN0cmljdCc7XG5cbiAgdmFyIFJlYWN0X19kZWZhdWx0ID0gJ2RlZmF1bHQnIGluIFJlYWN0ID8gUmVhY3RbJ2RlZmF1bHQnXSA6IFJlYWN0O1xuICBQcm9wVHlwZXMgPSBQcm9wVHlwZXMgJiYgUHJvcFR5cGVzLmhhc093blByb3BlcnR5KCdkZWZhdWx0JykgPyBQcm9wVHlwZXNbJ2RlZmF1bHQnXSA6IFByb3BUeXBlcztcblxuICAvLyDlgLznsbvlnovliKTmlq0gKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK1xuICB2YXIgaXNVbmRlZmluZWQgPSBmdW5jdGlvbiBpc1VuZGVmaW5lZCh2YWwpIHtcbiAgICByZXR1cm4gdHlwZW9mIHZhbCA9PT0gJ3VuZGVmaW5lZCc7XG4gIH07XG5cbiAgdmFyIGlzTnVsbCA9IGZ1bmN0aW9uIGlzTnVsbCh2YWwpIHtcbiAgICByZXR1cm4gdmFsID09PSBudWxsO1xuICB9O1xuXG4gIHZhciBpc0Z1bmN0aW9uID0gZnVuY3Rpb24gaXNGdW5jdGlvbih2YWwpIHtcbiAgICByZXR1cm4gdHlwZW9mIHZhbCA9PT0gJ2Z1bmN0aW9uJztcbiAgfTtcblxuICB2YXIgaXNTdHJpbmcgPSBmdW5jdGlvbiBpc1N0cmluZyh2YWwpIHtcbiAgICByZXR1cm4gdHlwZW9mIHZhbCA9PT0gJ3N0cmluZyc7XG4gIH07XG5cbiAgdmFyIGlzRXhpc3QgPSBmdW5jdGlvbiBpc0V4aXN0KHZhbCkge1xuICAgIHJldHVybiAhKGlzVW5kZWZpbmVkKHZhbCkgfHwgaXNOdWxsKHZhbCkpO1xuICB9O1xuXG4gIHZhciBpc0FycmF5ID0gZnVuY3Rpb24gaXNBcnJheSh2YWwpIHtcbiAgICByZXR1cm4gdmFsIGluc3RhbmNlb2YgQXJyYXk7XG4gIH07XG5cbiAgdmFyIGlzTmFOID0gZnVuY3Rpb24gaXNOYU4odmFsKSB7XG4gICAgcmV0dXJuIHZhbCAhPT0gdmFsO1xuICB9O1xuXG4gIHZhciBpc051bWJlciA9IGZ1bmN0aW9uIGlzTnVtYmVyKHZhbCkge1xuICAgIHJldHVybiB0eXBlb2YgdmFsID09PSAnbnVtYmVyJyAmJiAhaXNOYU4odmFsKTtcbiAgfTtcbiAgLy8g5YC857G75Z6L5Yik5patIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblxuICB2YXIgZ2V0ID0gZnVuY3Rpb24gZ2V0KG9iaikge1xuICAgIHZhciBrZXlzID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiBbXTtcbiAgICB2YXIgZGVmYXVsdFZhbHVlID0gYXJndW1lbnRzWzJdO1xuXG4gICAgdHJ5IHtcbiAgICAgIGlmIChpc051bWJlcihrZXlzKSkge1xuICAgICAgICBrZXlzID0gU3RyaW5nKGtleXMpO1xuICAgICAgfVxuICAgICAgdmFyIHJlc3VsdCA9IChpc1N0cmluZyhrZXlzKSA/IGtleXMuc3BsaXQoJy4nKSA6IGtleXMpLnJlZHVjZShmdW5jdGlvbiAocmVzLCBrZXkpIHtcbiAgICAgICAgcmV0dXJuIHJlc1trZXldO1xuICAgICAgfSwgb2JqKTtcbiAgICAgIHJldHVybiBpc1VuZGVmaW5lZChyZXN1bHQpID8gZGVmYXVsdFZhbHVlIDogcmVzdWx0O1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9O1xuXG4gIHZhciBydW4gPSBmdW5jdGlvbiBydW4ob2JqKSB7XG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuID4gMiA/IF9sZW4gLSAyIDogMCksIF9rZXkgPSAyOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXkgLSAyXSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICB2YXIga2V5cyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogW107XG5cbiAgICBrZXlzID0gaXNTdHJpbmcoa2V5cykgPyBrZXlzLnNwbGl0KCcuJykgOiBrZXlzO1xuXG4gICAgdmFyIGZ1bmMgPSBnZXQob2JqLCBrZXlzKTtcbiAgICB2YXIgY29udGV4dCA9IGdldChvYmosIGtleXMuc2xpY2UoMCwgLTEpKTtcblxuICAgIHJldHVybiBpc0Z1bmN0aW9uKGZ1bmMpID8gZnVuYy5jYWxsLmFwcGx5KGZ1bmMsIFtjb250ZXh0XS5jb25jYXQoYXJncykpIDogZnVuYztcbiAgfTtcblxuICB2YXIgdmFsdWUgPSBmdW5jdGlvbiB2YWx1ZSgpIHtcbiAgICBmb3IgKHZhciBfbGVuMiA9IGFyZ3VtZW50cy5sZW5ndGgsIHZhbHVlcyA9IEFycmF5KF9sZW4yKSwgX2tleTIgPSAwOyBfa2V5MiA8IF9sZW4yOyBfa2V5MisrKSB7XG4gICAgICB2YWx1ZXNbX2tleTJdID0gYXJndW1lbnRzW19rZXkyXTtcbiAgICB9XG5cbiAgICByZXR1cm4gdmFsdWVzLnJlZHVjZShmdW5jdGlvbiAodmFsdWUsIG5leHRWYWx1ZSkge1xuICAgICAgcmV0dXJuIGlzVW5kZWZpbmVkKHZhbHVlKSA/IHJ1bihuZXh0VmFsdWUpIDogcnVuKHZhbHVlKTtcbiAgICB9LCB1bmRlZmluZWQpO1xuICB9O1xuXG4gIHZhciBfdHlwZW9mID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7XG4gICAgcmV0dXJuIHR5cGVvZiBvYmo7XG4gIH0gOiBmdW5jdGlvbiAob2JqKSB7XG4gICAgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7XG4gIH07XG5cbiAgdmFyIGNsYXNzQ2FsbENoZWNrID0gZnVuY3Rpb24gKGluc3RhbmNlLCBDb25zdHJ1Y3Rvcikge1xuICAgIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XG4gICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpO1xuICAgIH1cbiAgfTtcblxuICB2YXIgY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07XG4gICAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcbiAgICAgICAgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlO1xuICAgICAgICBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlO1xuICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHtcbiAgICAgIGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7XG4gICAgICBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTtcbiAgICAgIHJldHVybiBDb25zdHJ1Y3RvcjtcbiAgICB9O1xuICB9KCk7XG5cbiAgdmFyIGRlZmluZVByb3BlcnR5ID0gZnVuY3Rpb24gKG9iaiwga2V5LCB2YWx1ZSkge1xuICAgIGlmIChrZXkgaW4gb2JqKSB7XG4gICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHtcbiAgICAgICAgdmFsdWU6IHZhbHVlLFxuICAgICAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgICAgICBjb25maWd1cmFibGU6IHRydWUsXG4gICAgICAgIHdyaXRhYmxlOiB0cnVlXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgb2JqW2tleV0gPSB2YWx1ZTtcbiAgICB9XG5cbiAgICByZXR1cm4gb2JqO1xuICB9O1xuXG4gIHZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkge1xuICAgIGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldO1xuXG4gICAgICBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7XG4gICAgICAgIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7XG4gICAgICAgICAgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB0YXJnZXQ7XG4gIH07XG5cbiAgdmFyIGluaGVyaXRzID0gZnVuY3Rpb24gKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7XG4gICAgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7XG4gICAgfVxuXG4gICAgc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7XG4gICAgICBjb25zdHJ1Y3Rvcjoge1xuICAgICAgICB2YWx1ZTogc3ViQ2xhc3MsXG4gICAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxuICAgICAgICB3cml0YWJsZTogdHJ1ZSxcbiAgICAgICAgY29uZmlndXJhYmxlOiB0cnVlXG4gICAgICB9XG4gICAgfSk7XG4gICAgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzO1xuICB9O1xuXG4gIHZhciBvYmplY3RXaXRob3V0UHJvcGVydGllcyA9IGZ1bmN0aW9uIChvYmosIGtleXMpIHtcbiAgICB2YXIgdGFyZ2V0ID0ge307XG5cbiAgICBmb3IgKHZhciBpIGluIG9iaikge1xuICAgICAgaWYgKGtleXMuaW5kZXhPZihpKSA+PSAwKSBjb250aW51ZTtcbiAgICAgIGlmICghT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgaSkpIGNvbnRpbnVlO1xuICAgICAgdGFyZ2V0W2ldID0gb2JqW2ldO1xuICAgIH1cblxuICAgIHJldHVybiB0YXJnZXQ7XG4gIH07XG5cbiAgdmFyIHBvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gPSBmdW5jdGlvbiAoc2VsZiwgY2FsbCkge1xuICAgIGlmICghc2VsZikge1xuICAgICAgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpO1xuICAgIH1cblxuICAgIHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmO1xuICB9O1xuXG4gIHZhciBzbGljZWRUb0FycmF5ID0gZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIHNsaWNlSXRlcmF0b3IoYXJyLCBpKSB7XG4gICAgICB2YXIgX2FyciA9IFtdO1xuICAgICAgdmFyIF9uID0gdHJ1ZTtcbiAgICAgIHZhciBfZCA9IGZhbHNlO1xuICAgICAgdmFyIF9lID0gdW5kZWZpbmVkO1xuXG4gICAgICB0cnkge1xuICAgICAgICBmb3IgKHZhciBfaSA9IGFycltTeW1ib2wuaXRlcmF0b3JdKCksIF9zOyAhKF9uID0gKF9zID0gX2kubmV4dCgpKS5kb25lKTsgX24gPSB0cnVlKSB7XG4gICAgICAgICAgX2Fyci5wdXNoKF9zLnZhbHVlKTtcblxuICAgICAgICAgIGlmIChpICYmIF9hcnIubGVuZ3RoID09PSBpKSBicmVhaztcbiAgICAgICAgfVxuICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgIF9kID0gdHJ1ZTtcbiAgICAgICAgX2UgPSBlcnI7XG4gICAgICB9IGZpbmFsbHkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgIGlmICghX24gJiYgX2lbXCJyZXR1cm5cIl0pIF9pW1wicmV0dXJuXCJdKCk7XG4gICAgICAgIH0gZmluYWxseSB7XG4gICAgICAgICAgaWYgKF9kKSB0aHJvdyBfZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gX2FycjtcbiAgICB9XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKGFyciwgaSkge1xuICAgICAgaWYgKEFycmF5LmlzQXJyYXkoYXJyKSkge1xuICAgICAgICByZXR1cm4gYXJyO1xuICAgICAgfSBlbHNlIGlmIChTeW1ib2wuaXRlcmF0b3IgaW4gT2JqZWN0KGFycikpIHtcbiAgICAgICAgcmV0dXJuIHNsaWNlSXRlcmF0b3IoYXJyLCBpKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJJbnZhbGlkIGF0dGVtcHQgdG8gZGVzdHJ1Y3R1cmUgbm9uLWl0ZXJhYmxlIGluc3RhbmNlXCIpO1xuICAgICAgfVxuICAgIH07XG4gIH0oKTtcblxuICB2YXIgdG9Db25zdW1hYmxlQXJyYXkgPSBmdW5jdGlvbiAoYXJyKSB7XG4gICAgaWYgKEFycmF5LmlzQXJyYXkoYXJyKSkge1xuICAgICAgZm9yICh2YXIgaSA9IDAsIGFycjIgPSBBcnJheShhcnIubGVuZ3RoKTsgaSA8IGFyci5sZW5ndGg7IGkrKykgYXJyMltpXSA9IGFycltpXTtcblxuICAgICAgcmV0dXJuIGFycjI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBBcnJheS5mcm9tKGFycik7XG4gICAgfVxuICB9O1xuXG4gIHZhciBnZXRJbXBsZW1lbnRhdGlvbiA9IGZ1bmN0aW9uIGdldEltcGxlbWVudGF0aW9uKCkge1xuICAgIGlmICh0eXBlb2Ygc2VsZiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHJldHVybiBzZWxmO1xuICAgIH1cbiAgICBpZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHJldHVybiB3aW5kb3c7XG4gICAgfVxuICAgIGlmICh0eXBlb2YgZ2xvYmFsICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgcmV0dXJuIGdsb2JhbDtcbiAgICB9XG5cbiAgICByZXR1cm4gRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKTtcbiAgfTtcblxuICB2YXIgaW1wbGVtZW50YXRpb24gPSBnZXRJbXBsZW1lbnRhdGlvbigpO1xuXG4gIHZhciBnZXRHbG9iYWwgPSBmdW5jdGlvbiBnZXRHbG9iYWwoKSB7XG4gICAgaWYgKCh0eXBlb2YgZ2xvYmFsID09PSAndW5kZWZpbmVkJyA/ICd1bmRlZmluZWQnIDogX3R5cGVvZihnbG9iYWwpKSAhPT0gJ29iamVjdCcgfHwgIWdsb2JhbCB8fCBnbG9iYWwuTWF0aCAhPT0gTWF0aCB8fCBnbG9iYWwuQXJyYXkgIT09IEFycmF5KSB7XG4gICAgICByZXR1cm4gaW1wbGVtZW50YXRpb247XG4gICAgfVxuICAgIHJldHVybiBnbG9iYWw7XG4gIH07XG5cbiAgdmFyIGdsb2JhbFRoaXMgPSBnZXRHbG9iYWwoKTtcblxuICB2YXIgZmxhdHRlbiA9IGZ1bmN0aW9uIGZsYXR0ZW4oYXJyYXkpIHtcbiAgICByZXR1cm4gYXJyYXkucmVkdWNlKGZ1bmN0aW9uIChyZXMsIGl0ZW0pIHtcbiAgICAgIHJldHVybiBbXS5jb25jYXQodG9Db25zdW1hYmxlQXJyYXkocmVzKSwgdG9Db25zdW1hYmxlQXJyYXkoaXNBcnJheShpdGVtKSA/IGZsYXR0ZW4oaXRlbSkgOiBbaXRlbV0pKTtcbiAgICB9LCBbXSk7XG4gIH07XG5cbiAgLyoqXHJcbiAgICogW+mSs+WtkF0g55So5p2l5bCG5pWw5a2X6ZmQ5Yi25Zyo57uZ5a6a6IyD5Zu05YaFXHJcbiAgICogQHBhcmFtIHtOdW1iZXJ9IHZhbHVlIOiiq+mZkOWItuWAvFxyXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBtaW4g5pyA5bCP5YC8XHJcbiAgICogQHBhcmFtIHtOdW1iZXJ9IG1heCDmnIDlpKflgLxcclxuICAgKi9cbiAgdmFyIGNsYW1wID0gZnVuY3Rpb24gY2xhbXAodmFsdWUsIG1pbikge1xuICAgIHZhciBtYXggPSBhcmd1bWVudHMubGVuZ3RoID4gMiAmJiBhcmd1bWVudHNbMl0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1syXSA6IE51bWJlci5NQVhfVkFMVUU7XG5cbiAgICBpZiAodmFsdWUgPCBtaW4pIHtcbiAgICAgIHJldHVybiBtaW47XG4gICAgfVxuXG4gICAgaWYgKHZhbHVlID4gbWF4KSB7XG4gICAgICByZXR1cm4gbWF4O1xuICAgIH1cblxuICAgIHJldHVybiB2YWx1ZTtcbiAgfTtcblxuICB2YXIgYm9keSA9IGdldChnbG9iYWxUaGlzLCAnZG9jdW1lbnQuYm9keScpO1xuICB2YXIgc2NyZWVuU2Nyb2xsaW5nRWxlbWVudCA9IGdldChnbG9iYWxUaGlzLCAnZG9jdW1lbnQuc2Nyb2xsaW5nRWxlbWVudCcsIGdldChnbG9iYWxUaGlzLCAnZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50Jywge30pKTtcblxuICBmdW5jdGlvbiBpc1Njcm9sbGFibGVOb2RlKCkge1xuICAgIHZhciBub2RlID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiB7fTtcblxuICAgIGlmICghaXNFeGlzdChub2RlKSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIHJldHVybiBub2RlLnNjcm9sbFdpZHRoID4gbm9kZS5jbGllbnRXaWR0aCB8fCBub2RlLnNjcm9sbEhlaWdodCA+IG5vZGUuY2xpZW50SGVpZ2h0O1xuICB9XG5cbiAgZnVuY3Rpb24gZ2V0U2Nyb2xsYWJsZU5vZGVzKGZyb20pIHtcbiAgICBpZiAoIWlzRnVuY3Rpb24oZ2V0KGdsb2JhbFRoaXMsICdkb2N1bWVudC5nZXRFbGVtZW50QnlJZCcpKSkge1xuICAgICAgcmV0dXJuIFtdO1xuICAgIH1cblxuICAgIHJldHVybiBbXS5jb25jYXQodG9Db25zdW1hYmxlQXJyYXkodmFsdWUocnVuKGZyb20sICdxdWVyeVNlbGVjdG9yQWxsJywgJyonKSwgW10pKSwgW2Zyb21dKS5maWx0ZXIoaXNTY3JvbGxhYmxlTm9kZSk7XG4gIH1cblxuICBmdW5jdGlvbiBzYXZlU2Nyb2xsUG9zaXRpb24oZnJvbSkge1xuICAgIHZhciBub2RlcyA9IFtdLmNvbmNhdCh0b0NvbnN1bWFibGVBcnJheShuZXcgU2V0KFtdLmNvbmNhdCh0b0NvbnN1bWFibGVBcnJheShmbGF0dGVuKCghaXNBcnJheShmcm9tKSA/IFtmcm9tXSA6IGZyb20pLm1hcChnZXRTY3JvbGxhYmxlTm9kZXMpKSksIHRvQ29uc3VtYWJsZUFycmF5KFtzY3JlZW5TY3JvbGxpbmdFbGVtZW50LCBib2R5XS5maWx0ZXIoaXNTY3JvbGxhYmxlTm9kZSkpKSkpKTtcblxuICAgIHZhciBzYXZlciA9IG5vZGVzLm1hcChmdW5jdGlvbiAobm9kZSkge1xuICAgICAgcmV0dXJuIFtub2RlLCB7XG4gICAgICAgIHg6IG5vZGUuc2Nyb2xsTGVmdCxcbiAgICAgICAgeTogbm9kZS5zY3JvbGxUb3BcbiAgICAgIH1dO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIHJldmVydCgpIHtcbiAgICAgIHNhdmVyLmZvckVhY2goZnVuY3Rpb24gKF9yZWYpIHtcbiAgICAgICAgdmFyIF9yZWYyID0gc2xpY2VkVG9BcnJheShfcmVmLCAyKSxcbiAgICAgICAgICAgIG5vZGUgPSBfcmVmMlswXSxcbiAgICAgICAgICAgIF9yZWYyJCA9IF9yZWYyWzFdLFxuICAgICAgICAgICAgeCA9IF9yZWYyJC54LFxuICAgICAgICAgICAgeSA9IF9yZWYyJC55O1xuXG4gICAgICAgIG5vZGUuc2Nyb2xsTGVmdCA9IHg7XG4gICAgICAgIG5vZGUuc2Nyb2xsVG9wID0geTtcbiAgICAgIH0pO1xuICAgIH07XG4gIH1cblxuICB2YXIgX19jb21wb25lbnRzID0ge307XG5cbiAgdmFyIGdldENhY2hlZENvbXBvbmVudEVudHJpZXMgPSBmdW5jdGlvbiBnZXRDYWNoZWRDb21wb25lbnRFbnRyaWVzKCkge1xuICAgIHJldHVybiBPYmplY3QuZW50cmllcyhfX2NvbXBvbmVudHMpLmZpbHRlcihmdW5jdGlvbiAoX3JlZikge1xuICAgICAgdmFyIF9yZWYyID0gc2xpY2VkVG9BcnJheShfcmVmLCAyKSxcbiAgICAgICAgICBjYWNoZSA9IF9yZWYyWzFdO1xuXG4gICAgICByZXR1cm4gY2FjaGUgaW5zdGFuY2VvZiBDYWNoZUNvbXBvbmVudCA/IGNhY2hlLnN0YXRlLmNhY2hlZCA6IE9iamVjdC52YWx1ZXMoY2FjaGUpLnNvbWUoZnVuY3Rpb24gKGNhY2hlKSB7XG4gICAgICAgIHJldHVybiBjYWNoZS5zdGF0ZS5jYWNoZWQ7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfTtcblxuICB2YXIgZ2V0Q2FjaGUgPSBmdW5jdGlvbiBnZXRDYWNoZSgpIHtcbiAgICByZXR1cm4gX2V4dGVuZHMoe30sIF9fY29tcG9uZW50cyk7XG4gIH07XG5cbiAgdmFyIHJlZ2lzdGVyID0gZnVuY3Rpb24gcmVnaXN0ZXIoa2V5LCBjb21wb25lbnQpIHtcbiAgICBfX2NvbXBvbmVudHNba2V5XSA9IGNvbXBvbmVudDtcbiAgfTtcblxuICB2YXIgcmVtb3ZlID0gZnVuY3Rpb24gcmVtb3ZlKGtleSkge1xuICAgIGRlbGV0ZSBfX2NvbXBvbmVudHNba2V5XTtcbiAgfTtcblxuICB2YXIgZHJvcENvbXBvbmVudCA9IGZ1bmN0aW9uIGRyb3BDb21wb25lbnQoY29tcG9uZW50KSB7XG4gICAgcmV0dXJuIHJ1bihjb21wb25lbnQsICdyZXNldCcpO1xuICB9O1xuXG4gIHZhciBkcm9wQnlDYWNoZUtleSA9IGZ1bmN0aW9uIGRyb3BCeUNhY2hlS2V5KGtleSkge1xuICAgIHZhciBjYWNoZSA9IGdldChfX2NvbXBvbmVudHMsIFtrZXldKTtcblxuICAgIGlmICghY2FjaGUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoY2FjaGUgaW5zdGFuY2VvZiBDYWNoZUNvbXBvbmVudCkge1xuICAgICAgZHJvcENvbXBvbmVudChjYWNoZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIE9iamVjdC52YWx1ZXMoY2FjaGUpLmZvckVhY2goZHJvcENvbXBvbmVudCk7XG4gICAgfVxuICB9O1xuXG4gIHZhciBjbGVhckNhY2hlID0gZnVuY3Rpb24gY2xlYXJDYWNoZSgpIHtcbiAgICBnZXRDYWNoZWRDb21wb25lbnRFbnRyaWVzKCkuZm9yRWFjaChmdW5jdGlvbiAoX3JlZjMpIHtcbiAgICAgIHZhciBfcmVmNCA9IHNsaWNlZFRvQXJyYXkoX3JlZjMsIDEpLFxuICAgICAgICAgIGtleSA9IF9yZWY0WzBdO1xuXG4gICAgICByZXR1cm4gZHJvcEJ5Q2FjaGVLZXkoa2V5KTtcbiAgICB9KTtcbiAgfTtcblxuICB2YXIgZ2V0Q2FjaGluZ0tleXMgPSBmdW5jdGlvbiBnZXRDYWNoaW5nS2V5cygpIHtcbiAgICByZXR1cm4gZ2V0Q2FjaGVkQ29tcG9uZW50RW50cmllcygpLm1hcChmdW5jdGlvbiAoX3JlZjUpIHtcbiAgICAgIHZhciBfcmVmNiA9IHNsaWNlZFRvQXJyYXkoX3JlZjUsIDEpLFxuICAgICAgICAgIGtleSA9IF9yZWY2WzBdO1xuXG4gICAgICByZXR1cm4ga2V5O1xuICAgIH0pO1xuICB9O1xuXG4gIHZhciBnZXRDYWNoaW5nQ29tcG9uZW50cyA9IGZ1bmN0aW9uIGdldENhY2hpbmdDb21wb25lbnRzKCkge1xuICAgIHJldHVybiBnZXRDYWNoZWRDb21wb25lbnRFbnRyaWVzKCkucmVkdWNlKGZ1bmN0aW9uIChyZXMsIF9yZWY3KSB7XG4gICAgICB2YXIgX3JlZjggPSBzbGljZWRUb0FycmF5KF9yZWY3LCAyKSxcbiAgICAgICAgICBrZXkgPSBfcmVmOFswXSxcbiAgICAgICAgICBjYWNoZSA9IF9yZWY4WzFdO1xuXG4gICAgICByZXR1cm4gX2V4dGVuZHMoe30sIHJlcywgY2FjaGUgaW5zdGFuY2VvZiBDYWNoZUNvbXBvbmVudCA/IGRlZmluZVByb3BlcnR5KHt9LCBrZXksIGNhY2hlKSA6IE9iamVjdC5lbnRyaWVzKGNhY2hlKS5yZWR1Y2UoZnVuY3Rpb24gKHJlcywgX3JlZjEwKSB7XG4gICAgICAgIHZhciBfcmVmMTEgPSBzbGljZWRUb0FycmF5KF9yZWYxMCwgMiksXG4gICAgICAgICAgICBwYXRobmFtZSA9IF9yZWYxMVswXSxcbiAgICAgICAgICAgIGNhY2hlID0gX3JlZjExWzFdO1xuXG4gICAgICAgIHJldHVybiBfZXh0ZW5kcyh7fSwgcmVzLCBkZWZpbmVQcm9wZXJ0eSh7fSwga2V5ICsgJy4nICsgcGF0aG5hbWUsIGNhY2hlKSk7XG4gICAgICB9LCB7fSkpO1xuICAgIH0sIHt9KTtcbiAgfTtcblxuICB2YXIgaXNVc2luZ05ld0xpZmVjeWNsZSA9IGlzRXhpc3QoUmVhY3RfX2RlZmF1bHQuZm9yd2FyZFJlZik7XG5cbiAgdmFyIENPTVBVVEVEX1VOTUFUQ0hfS0VZID0gJ19faXNDb21wdXRlZFVubWF0Y2gnO1xuICB2YXIgaXNNYXRjaCA9IGZ1bmN0aW9uIGlzTWF0Y2gobWF0Y2gpIHtcbiAgICByZXR1cm4gaXNFeGlzdChtYXRjaCkgJiYgZ2V0KG1hdGNoLCBDT01QVVRFRF9VTk1BVENIX0tFWSkgIT09IHRydWU7XG4gIH07XG5cbiAgdmFyIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyA9IGZ1bmN0aW9uIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgIHZhciBuZXh0UHJvcHNNYXRjaCA9IG5leHRQcm9wcy5tYXRjaCxcbiAgICAgICAgX25leHRQcm9wcyR3aGVuID0gbmV4dFByb3BzLndoZW4sXG4gICAgICAgIHdoZW4gPSBfbmV4dFByb3BzJHdoZW4gPT09IHVuZGVmaW5lZCA/ICdmb3J3YXJkJyA6IF9uZXh0UHJvcHMkd2hlbjtcblxuICAgIC8qKlxyXG4gICAgICogTm90ZTpcclxuICAgICAqIFR1cm4gY29tcHV0ZWRNYXRjaCBmcm9tIENhY2hlU3dpdGNoIHRvIGEgcmVhbCBudWxsIHZhbHVlXHJcbiAgICAgKlxyXG4gICAgICog5bCGIENhY2hlU3dpdGNoIOiuoeeul+W+l+WIsOeahCBjb21wdXRlZE1hdGNoIOWAvOi9rOaNouS4uuecn+ato+eahCBudWxsXHJcbiAgICAgKi9cblxuICAgIGlmICghaXNNYXRjaChuZXh0UHJvcHNNYXRjaCkpIHtcbiAgICAgIG5leHRQcm9wc01hdGNoID0gbnVsbDtcbiAgICB9XG5cbiAgICBpZiAoIXByZXZTdGF0ZS5jYWNoZWQgJiYgbmV4dFByb3BzTWF0Y2gpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGNhY2hlZDogdHJ1ZSxcbiAgICAgICAgbWF0Y2hlZDogdHJ1ZVxuICAgICAgfTtcbiAgICB9XG5cbiAgICAvKipcclxuICAgICAqIERldGVybWluZXMgd2hldGhlciBpdCBuZWVkcyB0byBjYW5jZWwgdGhlIGNhY2hlIGJhc2VkIG9uIHRoZSBuZXh0IHVubWF0Y2hlZCBwcm9wcyBhY3Rpb25cclxuICAgICAqXHJcbiAgICAgKiDmoLnmja7kuIvkuKrmnKrljLnphY3nirbmgIHliqjkvZzlhrPlrprmmK/lkKbpnIDopoHlj5bmtojnvJPlrZhcclxuICAgICAqL1xuICAgIGlmIChwcmV2U3RhdGUubWF0Y2hlZCAmJiAhbmV4dFByb3BzTWF0Y2gpIHtcbiAgICAgIHZhciBuZXh0QWN0aW9uID0gZ2V0KG5leHRQcm9wcywgJ2hpc3RvcnkuYWN0aW9uJyk7XG5cbiAgICAgIHZhciBfX2NhbmNlbF9fY2FjaGUgPSBmYWxzZTtcblxuICAgICAgaWYgKGlzRnVuY3Rpb24od2hlbikpIHtcbiAgICAgICAgX19jYW5jZWxfX2NhY2hlID0gIXdoZW4obmV4dFByb3BzKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHN3aXRjaCAod2hlbikge1xuICAgICAgICAgIGNhc2UgJ2Fsd2F5cyc6XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlICdiYWNrJzpcbiAgICAgICAgICAgIGlmIChbJ1BVU0gnLCAnUkVQTEFDRSddLmluY2x1ZGVzKG5leHRBY3Rpb24pKSB7XG4gICAgICAgICAgICAgIF9fY2FuY2VsX19jYWNoZSA9IHRydWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgJ2ZvcndhcmQnOlxuICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICBpZiAobmV4dEFjdGlvbiA9PT0gJ1BPUCcpIHtcbiAgICAgICAgICAgICAgX19jYW5jZWxfX2NhY2hlID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoX19jYW5jZWxfX2NhY2hlKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgY2FjaGVkOiBmYWxzZSxcbiAgICAgICAgICBtYXRjaGVkOiBmYWxzZVxuICAgICAgICB9O1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICBtYXRjaGVkOiAhIW5leHRQcm9wc01hdGNoXG4gICAgfTtcbiAgfTtcblxuICB2YXIgQ2FjaGVDb21wb25lbnQgPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuICAgIGluaGVyaXRzKENhY2hlQ29tcG9uZW50LCBfQ29tcG9uZW50KTtcblxuICAgIGZ1bmN0aW9uIENhY2hlQ29tcG9uZW50KHByb3BzKSB7XG4gICAgICB2YXIgX3JlZjtcblxuICAgICAgY2xhc3NDYWxsQ2hlY2sodGhpcywgQ2FjaGVDb21wb25lbnQpO1xuXG4gICAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4gPiAxID8gX2xlbiAtIDEgOiAwKSwgX2tleSA9IDE7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgICAgYXJnc1tfa2V5IC0gMV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgICB9XG5cbiAgICAgIHZhciBfdGhpcyA9IHBvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKF9yZWYgPSBDYWNoZUNvbXBvbmVudC5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKENhY2hlQ29tcG9uZW50KSkuY2FsbC5hcHBseShfcmVmLCBbdGhpcywgcHJvcHNdLmNvbmNhdChhcmdzKSkpO1xuXG4gICAgICBfdGhpcy5jYWNoZUxpZmVjeWNsZXMgPSB7XG4gICAgICAgIF9fbGlzdGVuZXI6IHt9LFxuICAgICAgICBkaWRDYWNoZTogZnVuY3Rpb24gZGlkQ2FjaGUobGlzdGVuZXIpIHtcbiAgICAgICAgICBfdGhpcy5jYWNoZUxpZmVjeWNsZXMuX19saXN0ZW5lclsnZGlkQ2FjaGUnXSA9IGxpc3RlbmVyO1xuICAgICAgICB9LFxuICAgICAgICBkaWRSZWNvdmVyOiBmdW5jdGlvbiBkaWRSZWNvdmVyKGxpc3RlbmVyKSB7XG4gICAgICAgICAgX3RoaXMuY2FjaGVMaWZlY3ljbGVzLl9fbGlzdGVuZXJbJ2RpZFJlY292ZXInXSA9IGxpc3RlbmVyO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogTmV3IGxpZmVjeWNsZSBmb3IgcmVwbGFjaW5nIHRoZSBgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wc2AgaW4gUmVhY3QgMTYuMyArXHJcbiAgICAgICAgICogUmVhY3QgMTYuMyArIOeJiOacrOS4reabv+S7oyBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzIOeahOaWsOeUn+WRveWRqOacn1xyXG4gICAgICAgICAqL1xuICAgICAgfTtcbiAgICAgIF90aGlzLmNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMgPSAhaXNVc2luZ05ld0xpZmVjeWNsZSA/IGZ1bmN0aW9uIChuZXh0UHJvcHMpIHtcbiAgICAgICAgdmFyIG5leHRTdGF0ZSA9IGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIF90aGlzLnN0YXRlKTtcblxuICAgICAgICBfdGhpcy5zZXRTdGF0ZShuZXh0U3RhdGUpO1xuICAgICAgfSA6IHVuZGVmaW5lZDtcblxuICAgICAgX3RoaXMuaW5qZWN0RE9NID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgIHJ1bihfdGhpcy5fX3BhcmVudE5vZGUsICdpbnNlcnRCZWZvcmUnLCBfdGhpcy53cmFwcGVyLCBfdGhpcy5fX3BsYWNlaG9sZGVyTm9kZSk7XG4gICAgICAgICAgcnVuKF90aGlzLl9fcGFyZW50Tm9kZSwgJ3JlbW92ZUNoaWxkJywgX3RoaXMuX19wbGFjZWhvbGRlck5vZGUpO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAvLyBub3RoaW5nXG4gICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIF90aGlzLmVqZWN0RE9NID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgIHZhciBwYXJlbnROb2RlID0gZ2V0KF90aGlzLndyYXBwZXIsICdwYXJlbnROb2RlJyk7XG4gICAgICAgICAgX3RoaXMuX19wYXJlbnROb2RlID0gcGFyZW50Tm9kZTtcblxuICAgICAgICAgIHJ1bihfdGhpcy5fX3BhcmVudE5vZGUsICdpbnNlcnRCZWZvcmUnLCBfdGhpcy5fX3BsYWNlaG9sZGVyTm9kZSwgX3RoaXMud3JhcHBlcik7XG4gICAgICAgICAgcnVuKF90aGlzLl9fcGFyZW50Tm9kZSwgJ3JlbW92ZUNoaWxkJywgX3RoaXMud3JhcHBlcik7XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgIC8vIG5vdGhpbmdcbiAgICAgICAgfVxuICAgICAgfTtcblxuICAgICAgX3RoaXMucmVzZXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGRlbGV0ZSBfdGhpcy5fX3JldmVydFNjcm9sbFBvcztcblxuICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgY2FjaGVkOiBmYWxzZVxuICAgICAgICB9KTtcbiAgICAgIH07XG5cbiAgICAgIF90aGlzLl9fY2FjaGVDcmVhdGVUaW1lID0gRGF0ZS5ub3coKTtcbiAgICAgIF90aGlzLl9fY2FjaGVVcGRhdGVUaW1lID0gX3RoaXMuX19jYWNoZUNyZWF0ZVRpbWU7XG4gICAgICBpZiAocHJvcHMuY2FjaGVLZXkpIHtcbiAgICAgICAgaWYgKGdldChwcm9wcy5jYWNoZUtleSwgJ211bHRpcGxlJykpIHtcbiAgICAgICAgICB2YXIgX3Byb3BzJGNhY2hlS2V5ID0gcHJvcHMuY2FjaGVLZXksXG4gICAgICAgICAgICAgIGNhY2hlS2V5ID0gX3Byb3BzJGNhY2hlS2V5LmNhY2hlS2V5LFxuICAgICAgICAgICAgICBwYXRobmFtZSA9IF9wcm9wcyRjYWNoZUtleS5wYXRobmFtZTtcblxuICAgICAgICAgIHJlZ2lzdGVyKGNhY2hlS2V5LCBfZXh0ZW5kcyh7fSwgZ2V0Q2FjaGUoKVtjYWNoZUtleV0sIGRlZmluZVByb3BlcnR5KHt9LCBwYXRobmFtZSwgX3RoaXMpKSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmVnaXN0ZXIocHJvcHMuY2FjaGVLZXksIF90aGlzKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAodHlwZW9mIGRvY3VtZW50ICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICBfdGhpcy5fX3BsYWNlaG9sZGVyTm9kZSA9IGRvY3VtZW50LmNyZWF0ZUNvbW1lbnQoJyBSb3V0ZSBjYWNoZWQgJyArIChwcm9wcy5jYWNoZUtleSA/ICd3aXRoIGNhY2hlS2V5OiBcIicgKyBnZXQocHJvcHMuY2FjaGVLZXksICdjYWNoZUtleScsIHByb3BzLmNhY2hlS2V5KSArICdcIiAnIDogJycpKTtcbiAgICAgIH1cblxuICAgICAgX3RoaXMuc3RhdGUgPSBnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMocHJvcHMsIHtcbiAgICAgICAgY2FjaGVkOiBmYWxzZSxcbiAgICAgICAgbWF0Y2hlZDogZmFsc2VcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIF90aGlzO1xuICAgIH1cblxuICAgIC8qKlxyXG4gICAgICogQ29tcGF0aWJsZSBSZWFjdCAxNi4zIC1cclxuICAgICAqIOWFvOWuuSBSZWFjdCAxNi4zIC0g54mI5pysXHJcbiAgICAgKi9cblxuXG4gICAgY3JlYXRlQ2xhc3MoQ2FjaGVDb21wb25lbnQsIFt7XG4gICAgICBrZXk6ICdjb21wb25lbnREaWRVcGRhdGUnLFxuICAgICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgICBpZiAoIXByZXZTdGF0ZS5jYWNoZWQgfHwgIXRoaXMuc3RhdGUuY2FjaGVkKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHByZXZTdGF0ZS5tYXRjaGVkID09PSB0cnVlICYmIHRoaXMuc3RhdGUubWF0Y2hlZCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICBpZiAodGhpcy5wcm9wcy51bm1vdW50KSB7XG4gICAgICAgICAgICB0aGlzLmVqZWN0RE9NKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuX19jYWNoZVVwZGF0ZVRpbWUgPSBEYXRlLm5vdygpO1xuICAgICAgICAgIHJldHVybiBydW4odGhpcywgJ2NhY2hlTGlmZWN5Y2xlcy5fX2xpc3RlbmVyLmRpZENhY2hlJyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocHJldlN0YXRlLm1hdGNoZWQgPT09IGZhbHNlICYmIHRoaXMuc3RhdGUubWF0Y2hlZCA9PT0gdHJ1ZSkge1xuICAgICAgICAgIGlmICh0aGlzLnByb3BzLnNhdmVTY3JvbGxQb3NpdGlvbikge1xuICAgICAgICAgICAgcnVuKHRoaXMuX19yZXZlcnRTY3JvbGxQb3MpO1xuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLl9fY2FjaGVVcGRhdGVUaW1lID0gRGF0ZS5ub3coKTtcbiAgICAgICAgICByZXR1cm4gcnVuKHRoaXMsICdjYWNoZUxpZmVjeWNsZXMuX19saXN0ZW5lci5kaWRSZWNvdmVyJyk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCB7XG4gICAgICBrZXk6ICdzaG91bGRDb21wb25lbnRVcGRhdGUnLFxuICAgICAgdmFsdWU6IGZ1bmN0aW9uIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMsIG5leHRTdGF0ZSkge1xuICAgICAgICB2YXIgd2lsbFJlY292ZXIgPSB0aGlzLnN0YXRlLm1hdGNoZWQgPT09IGZhbHNlICYmIG5leHRTdGF0ZS5tYXRjaGVkID09PSB0cnVlO1xuICAgICAgICB2YXIgd2lsbERyb3AgPSB0aGlzLnN0YXRlLmNhY2hlZCA9PT0gdHJ1ZSAmJiBuZXh0U3RhdGUuY2FjaGVkID09PSBmYWxzZTtcbiAgICAgICAgdmFyIHNob3VsZFVwZGF0ZSA9IHRoaXMuc3RhdGUubWF0Y2hlZCB8fCBuZXh0U3RhdGUubWF0Y2hlZCB8fCB0aGlzLnN0YXRlLmNhY2hlZCAhPT0gbmV4dFN0YXRlLmNhY2hlZDtcblxuICAgICAgICBpZiAoc2hvdWxkVXBkYXRlKSB7XG4gICAgICAgICAgaWYgKHRoaXMucHJvcHMudW5tb3VudCAmJiB3aWxsRHJvcCB8fCB3aWxsUmVjb3Zlcikge1xuICAgICAgICAgICAgdGhpcy5pbmplY3RET00oKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAoISh3aWxsRHJvcCB8fCB3aWxsUmVjb3ZlcikgJiYgdGhpcy5wcm9wcy5zYXZlU2Nyb2xsUG9zaXRpb24pIHtcbiAgICAgICAgICAgIHRoaXMuX19yZXZlcnRTY3JvbGxQb3MgPSBzYXZlU2Nyb2xsUG9zaXRpb24odGhpcy5wcm9wcy51bm1vdW50ID8gdGhpcy53cmFwcGVyIDogdW5kZWZpbmVkKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gc2hvdWxkVXBkYXRlO1xuICAgICAgfVxuICAgIH0sIHtcbiAgICAgIGtleTogJ2NvbXBvbmVudFdpbGxVbm1vdW50JyxcbiAgICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgICBjYWNoZUtleUNvbmZpZyA9IF9wcm9wcy5jYWNoZUtleSxcbiAgICAgICAgICAgIHVubW91bnQgPSBfcHJvcHMudW5tb3VudDtcblxuXG4gICAgICAgIGlmIChnZXQoY2FjaGVLZXlDb25maWcsICdtdWx0aXBsZScpKSB7XG4gICAgICAgICAgdmFyIGNhY2hlS2V5ID0gY2FjaGVLZXlDb25maWcuY2FjaGVLZXksXG4gICAgICAgICAgICAgIHBhdGhuYW1lID0gY2FjaGVLZXlDb25maWcucGF0aG5hbWU7XG5cbiAgICAgICAgICB2YXIgY2FjaGUgPSBfZXh0ZW5kcyh7fSwgZ2V0Q2FjaGUoKVtjYWNoZUtleV0pO1xuXG4gICAgICAgICAgZGVsZXRlIGNhY2hlW3BhdGhuYW1lXTtcblxuICAgICAgICAgIGlmIChPYmplY3Qua2V5cyhjYWNoZSkubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICByZW1vdmUoY2FjaGVLZXkpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZWdpc3RlcihjYWNoZUtleSwgY2FjaGUpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZW1vdmUoY2FjaGVLZXlDb25maWcpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHVubW91bnQpIHtcbiAgICAgICAgICB0aGlzLmluamVjdERPTSgpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwge1xuICAgICAga2V5OiAncmVuZGVyJyxcbiAgICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICAgIHZhciBfc3RhdGUgPSB0aGlzLnN0YXRlLFxuICAgICAgICAgICAgbWF0Y2hlZCA9IF9zdGF0ZS5tYXRjaGVkLFxuICAgICAgICAgICAgY2FjaGVkID0gX3N0YXRlLmNhY2hlZDtcbiAgICAgICAgdmFyIF9wcm9wczIgPSB0aGlzLnByb3BzLFxuICAgICAgICAgICAgX3Byb3BzMiRjbGFzc05hbWUgPSBfcHJvcHMyLmNsYXNzTmFtZSxcbiAgICAgICAgICAgIHByb3BzQ2xhc3NOYW1lID0gX3Byb3BzMiRjbGFzc05hbWUgPT09IHVuZGVmaW5lZCA/ICcnIDogX3Byb3BzMiRjbGFzc05hbWUsXG4gICAgICAgICAgICBiZWhhdmlvciA9IF9wcm9wczIuYmVoYXZpb3IsXG4gICAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wczIuY2hpbGRyZW47XG5cbiAgICAgICAgdmFyIF92YWx1ZSA9IHZhbHVlKHJ1bihiZWhhdmlvciwgdW5kZWZpbmVkLCAhbWF0Y2hlZCksIHt9KSxcbiAgICAgICAgICAgIF92YWx1ZSRjbGFzc05hbWUgPSBfdmFsdWUuY2xhc3NOYW1lLFxuICAgICAgICAgICAgYmVoYXZpb3JDbGFzc05hbWUgPSBfdmFsdWUkY2xhc3NOYW1lID09PSB1bmRlZmluZWQgPyAnJyA6IF92YWx1ZSRjbGFzc05hbWUsXG4gICAgICAgICAgICBiZWhhdmlvclByb3BzID0gb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3ZhbHVlLCBbJ2NsYXNzTmFtZSddKTtcblxuICAgICAgICB2YXIgY2xhc3NOYW1lID0gcnVuKHByb3BzQ2xhc3NOYW1lICsgJyAnICsgYmVoYXZpb3JDbGFzc05hbWUsICd0cmltJyk7XG4gICAgICAgIHZhciBoYXNDbGFzc05hbWUgPSBjbGFzc05hbWUgIT09ICcnO1xuXG4gICAgICAgIHJldHVybiBjYWNoZWQgPyBSZWFjdF9fZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdkaXYnLFxuICAgICAgICAgIF9leHRlbmRzKHtcbiAgICAgICAgICAgIGNsYXNzTmFtZTogaGFzQ2xhc3NOYW1lID8gY2xhc3NOYW1lIDogdW5kZWZpbmVkXG4gICAgICAgICAgfSwgYmVoYXZpb3JQcm9wcywge1xuICAgICAgICAgICAgcmVmOiBmdW5jdGlvbiByZWYod3JhcHBlcikge1xuICAgICAgICAgICAgICBfdGhpczIud3JhcHBlciA9IHdyYXBwZXI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSksXG4gICAgICAgICAgcnVuKGNoaWxkcmVuLCB1bmRlZmluZWQsIHRoaXMuY2FjaGVMaWZlY3ljbGVzKVxuICAgICAgICApIDogbnVsbDtcbiAgICAgIH1cbiAgICB9XSk7XG4gICAgcmV0dXJuIENhY2hlQ29tcG9uZW50O1xuICB9KFJlYWN0LkNvbXBvbmVudCk7XG5cbiAgQ2FjaGVDb21wb25lbnQucHJvcHNUeXBlcyA9IHtcbiAgICBoaXN0b3J5OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgbWF0Y2g6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgd2hlbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLmZ1bmMsIFByb3BUeXBlcy5vbmVPZihbJ2ZvcndhcmQnLCAnYmFjaycsICdhbHdheXMnXSldKSxcbiAgICBiZWhhdmlvcjogUHJvcFR5cGVzLmZ1bmMsXG4gICAgdW5tb3VudDogUHJvcFR5cGVzLmJvb2wsXG4gICAgc2F2ZVNjcm9sbFBvc2l0aW9uOiBQcm9wVHlwZXMuYm9vbFxuICB9O1xuICBDYWNoZUNvbXBvbmVudC5kZWZhdWx0UHJvcHMgPSB7XG4gICAgd2hlbjogJ2ZvcndhcmQnLFxuICAgIHVubW91bnQ6IGZhbHNlLFxuICAgIHNhdmVTY3JvbGxQb3NpdGlvbjogZmFsc2UsXG4gICAgYmVoYXZpb3I6IGZ1bmN0aW9uIGJlaGF2aW9yKGNhY2hlZCkge1xuICAgICAgcmV0dXJuIGNhY2hlZCA/IHtcbiAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICBkaXNwbGF5OiAnbm9uZSdcbiAgICAgICAgfVxuICAgICAgfSA6IHVuZGVmaW5lZDtcbiAgICB9XG4gIH07XG4gIENhY2hlQ29tcG9uZW50LmdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyA9IGlzVXNpbmdOZXdMaWZlY3ljbGUgPyBnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMgOiB1bmRlZmluZWQ7XG5cbiAgdmFyIFVwZGF0YWJsZSA9IGZ1bmN0aW9uIChfQ29tcG9uZW50KSB7XG4gICAgaW5oZXJpdHMoVXBkYXRhYmxlLCBfQ29tcG9uZW50KTtcblxuICAgIGZ1bmN0aW9uIFVwZGF0YWJsZSgpIHtcbiAgICAgIHZhciBfcmVmO1xuXG4gICAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgICBjbGFzc0NhbGxDaGVjayh0aGlzLCBVcGRhdGFibGUpO1xuXG4gICAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9IHBvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKF9yZWYgPSBVcGRhdGFibGUuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihVcGRhdGFibGUpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5yZW5kZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBydW4oX3RoaXMucHJvcHMsICdjaGlsZHJlbicpO1xuICAgICAgfSwgX3RoaXMuc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gZnVuY3Rpb24gKF9yZWYyKSB7XG4gICAgICAgIHZhciB3aGVuID0gX3JlZjIud2hlbjtcbiAgICAgICAgcmV0dXJuIHdoZW47XG4gICAgICB9LCBfdGVtcCksIHBvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICAgIH1cblxuICAgIHJldHVybiBVcGRhdGFibGU7XG4gIH0oUmVhY3QuQ29tcG9uZW50KTtcblxuICBVcGRhdGFibGUucHJvcHNUeXBlcyA9IHtcbiAgICB3aGVuOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkXG4gIH07XG5cbiAgdmFyIGlzRW1wdHlDaGlsZHJlbiA9IGZ1bmN0aW9uIGlzRW1wdHlDaGlsZHJlbihjaGlsZHJlbikge1xuICAgIHJldHVybiBSZWFjdF9fZGVmYXVsdC5DaGlsZHJlbi5jb3VudChjaGlsZHJlbikgPT09IDA7XG4gIH07XG4gIHZhciBpc0ZyYWdtZW50YWJsZSA9IGlzRXhpc3QoUmVhY3QuRnJhZ21lbnQpO1xuXG4gIHZhciBDYWNoZVJvdXRlID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgICBpbmhlcml0cyhDYWNoZVJvdXRlLCBfQ29tcG9uZW50KTtcblxuICAgIGZ1bmN0aW9uIENhY2hlUm91dGUoKSB7XG4gICAgICB2YXIgX3JlZjtcblxuICAgICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICAgY2xhc3NDYWxsQ2hlY2sodGhpcywgQ2FjaGVSb3V0ZSk7XG5cbiAgICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoX3JlZiA9IENhY2hlUm91dGUuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihDYWNoZVJvdXRlKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuY2FjaGUgPSB7fSwgX3RlbXApLCBwb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKF90aGlzLCBfcmV0KTtcbiAgICB9XG5cbiAgICBjcmVhdGVDbGFzcyhDYWNoZVJvdXRlLCBbe1xuICAgICAga2V5OiAncmVuZGVyJyxcbiAgICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgICByZW5kZXIgPSBfcHJvcHMucmVuZGVyLFxuICAgICAgICAgICAgY29tcG9uZW50ID0gX3Byb3BzLmNvbXBvbmVudCxcbiAgICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgICB3aGVuID0gX3Byb3BzLndoZW4sXG4gICAgICAgICAgICBiZWhhdmlvciA9IF9wcm9wcy5iZWhhdmlvcixcbiAgICAgICAgICAgIGNhY2hlS2V5ID0gX3Byb3BzLmNhY2hlS2V5LFxuICAgICAgICAgICAgdW5tb3VudCA9IF9wcm9wcy51bm1vdW50LFxuICAgICAgICAgICAgc2F2ZVNjcm9sbFBvc2l0aW9uJCQxID0gX3Byb3BzLnNhdmVTY3JvbGxQb3NpdGlvbixcbiAgICAgICAgICAgIGNvbXB1dGVkTWF0Y2hGb3JDYWNoZVJvdXRlID0gX3Byb3BzLmNvbXB1dGVkTWF0Y2hGb3JDYWNoZVJvdXRlLFxuICAgICAgICAgICAgbXVsdGlwbGUgPSBfcHJvcHMubXVsdGlwbGUsXG4gICAgICAgICAgICByZXN0UHJvcHMgPSBvYmplY3RXaXRob3V0UHJvcGVydGllcyhfcHJvcHMsIFsnY2hpbGRyZW4nLCAncmVuZGVyJywgJ2NvbXBvbmVudCcsICdjbGFzc05hbWUnLCAnd2hlbicsICdiZWhhdmlvcicsICdjYWNoZUtleScsICd1bm1vdW50JywgJ3NhdmVTY3JvbGxQb3NpdGlvbicsICdjb21wdXRlZE1hdGNoRm9yQ2FjaGVSb3V0ZScsICdtdWx0aXBsZSddKTtcblxuICAgICAgICAvKipcclxuICAgICAgICAgKiBOb3RlOlxyXG4gICAgICAgICAqIElmIGNoaWxkcmVuIHByb3AgaXMgYSBSZWFjdCBFbGVtZW50LCBkZWZpbmUgdGhlIGNvcnJlc3BvbmRpbmcgd3JhcHBlciBjb21wb25lbnQgZm9yIHN1cHBvcnRpbmcgbXVsdGlwbGUgY2hpbGRyZW5cclxuICAgICAgICAgKlxyXG4gICAgICAgICAqIOivtOaYju+8muWmguaenCBjaGlsZHJlbiDlsZ7mgKfmmK8gUmVhY3QgRWxlbWVudCDliJnlrprkuYnlr7nlupTnmoTljIXoo7nnu4Tku7bku6XmlK/mjIHlpJrkuKrlrZDnu4Tku7ZcclxuICAgICAgICAgKi9cblxuICAgICAgICBpZiAoUmVhY3RfX2RlZmF1bHQuaXNWYWxpZEVsZW1lbnQoY2hpbGRyZW4pIHx8ICFpc0VtcHR5Q2hpbGRyZW4oY2hpbGRyZW4pKSB7XG4gICAgICAgICAgcmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICAgICAgcmV0dXJuIGNoaWxkcmVuO1xuICAgICAgICAgIH07XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoY29tcHV0ZWRNYXRjaEZvckNhY2hlUm91dGUpIHtcbiAgICAgICAgICByZXN0UHJvcHMuY29tcHV0ZWRNYXRjaCA9IGNvbXB1dGVkTWF0Y2hGb3JDYWNoZVJvdXRlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG11bHRpcGxlICYmICFpc0ZyYWdtZW50YWJsZSkge1xuICAgICAgICAgIG11bHRpcGxlID0gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaXNOdW1iZXIobXVsdGlwbGUpKSB7XG4gICAgICAgICAgbXVsdGlwbGUgPSBjbGFtcChtdWx0aXBsZSwgMSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgIC8qKlxyXG4gICAgICAgICAgICogT25seSBjaGlsZHJlbiBwcm9wIG9mIFJvdXRlIGNhbiBoZWxwIHRvIGNvbnRyb2wgcmVuZGVyaW5nIGJlaGF2aW9yXHJcbiAgICAgICAgICAgKiDlj6rmnIkgUm91dGVyIOeahCBjaGlsZHJlbiDlsZ7mgKfmnInliqnkuo7kuLvliqjmjqfliLbmuLLmn5PooYzkuLpcclxuICAgICAgICAgICAqL1xuICAgICAgICAgIFJlYWN0X19kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICByZWFjdFJvdXRlckRvbS5Sb3V0ZSxcbiAgICAgICAgICAgIHJlc3RQcm9wcyxcbiAgICAgICAgICAgIGZ1bmN0aW9uIChwcm9wcykge1xuICAgICAgICAgICAgICB2YXIgbWF0Y2ggPSBwcm9wcy5tYXRjaCxcbiAgICAgICAgICAgICAgICAgIGNvbXB1dGVkTWF0Y2ggPSBwcm9wcy5jb21wdXRlZE1hdGNoLFxuICAgICAgICAgICAgICAgICAgbG9jYXRpb24gPSBwcm9wcy5sb2NhdGlvbjtcblxuICAgICAgICAgICAgICB2YXIgaXNNYXRjaEN1cnJlbnRSb3V0ZSA9IGlzTWF0Y2gocHJvcHMubWF0Y2gpO1xuICAgICAgICAgICAgICB2YXIgY3VycmVudFBhdGhuYW1lID0gbG9jYXRpb24ucGF0aG5hbWU7XG5cbiAgICAgICAgICAgICAgdmFyIG1heE11bHRpcGxlQ291bnQgPSBpc051bWJlcihtdWx0aXBsZSkgPyBtdWx0aXBsZSA6IEluZmluaXR5O1xuICAgICAgICAgICAgICB2YXIgY29uZmlnUHJvcHMgPSB7XG4gICAgICAgICAgICAgICAgd2hlbjogd2hlbixcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICAgICAgICBiZWhhdmlvcjogYmVoYXZpb3IsXG4gICAgICAgICAgICAgICAgY2FjaGVLZXk6IGNhY2hlS2V5LFxuICAgICAgICAgICAgICAgIHVubW91bnQ6IHVubW91bnQsXG4gICAgICAgICAgICAgICAgc2F2ZVNjcm9sbFBvc2l0aW9uOiBzYXZlU2Nyb2xsUG9zaXRpb24kJDFcbiAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICB2YXIgcmVuZGVyU2luZ2xlID0gZnVuY3Rpb24gcmVuZGVyU2luZ2xlKHByb3BzKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFJlYWN0X19kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICAgICAgICBDYWNoZUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICAgIHByb3BzLFxuICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gKGNhY2hlTGlmZWN5Y2xlcykge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gUmVhY3RfX2RlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICAgICAgICAgICBVcGRhdGFibGUsXG4gICAgICAgICAgICAgICAgICAgICAgeyB3aGVuOiBpc01hdGNoKHByb3BzLm1hdGNoKSB9LFxuICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5hc3NpZ24ocHJvcHMsIHsgY2FjaGVMaWZlY3ljbGVzOiBjYWNoZUxpZmVjeWNsZXMgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjb21wb25lbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFJlYWN0X19kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoY29tcG9uZW50LCBwcm9wcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBydW4ocmVuZGVyIHx8IGNoaWxkcmVuLCB1bmRlZmluZWQsIHByb3BzKTtcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICBpZiAobXVsdGlwbGUgJiYgaXNNYXRjaEN1cnJlbnRSb3V0ZSkge1xuICAgICAgICAgICAgICAgIF90aGlzMi5jYWNoZVtjdXJyZW50UGF0aG5hbWVdID0ge1xuICAgICAgICAgICAgICAgICAgdXBkYXRlVGltZTogRGF0ZS5ub3coKSxcbiAgICAgICAgICAgICAgICAgIHJlbmRlcjogcmVuZGVyU2luZ2xlXG4gICAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICAgIE9iamVjdC5lbnRyaWVzKF90aGlzMi5jYWNoZSkuc29ydChmdW5jdGlvbiAoX3JlZjIsIF9yZWYzKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgX3JlZjUgPSBzbGljZWRUb0FycmF5KF9yZWYyLCAyKSxcbiAgICAgICAgICAgICAgICAgICAgICBwcmV2ID0gX3JlZjVbMV07XG5cbiAgICAgICAgICAgICAgICAgIHZhciBfcmVmNCA9IHNsaWNlZFRvQXJyYXkoX3JlZjMsIDIpLFxuICAgICAgICAgICAgICAgICAgICAgIG5leHQgPSBfcmVmNFsxXTtcblxuICAgICAgICAgICAgICAgICAgcmV0dXJuIG5leHQudXBkYXRlVGltZSAtIHByZXYudXBkYXRlVGltZTtcbiAgICAgICAgICAgICAgICB9KS5mb3JFYWNoKGZ1bmN0aW9uIChfcmVmNiwgaWR4KSB7XG4gICAgICAgICAgICAgICAgICB2YXIgX3JlZjcgPSBzbGljZWRUb0FycmF5KF9yZWY2LCAxKSxcbiAgICAgICAgICAgICAgICAgICAgICBwYXRobmFtZSA9IF9yZWY3WzBdO1xuXG4gICAgICAgICAgICAgICAgICBpZiAoaWR4ID49IG1heE11bHRpcGxlQ291bnQpIHtcbiAgICAgICAgICAgICAgICAgICAgZGVsZXRlIF90aGlzMi5jYWNoZVtwYXRobmFtZV07XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICByZXR1cm4gbXVsdGlwbGUgPyBSZWFjdF9fZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgICAgIFJlYWN0LkZyYWdtZW50LFxuICAgICAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICAgICAgT2JqZWN0LmVudHJpZXMoX3RoaXMyLmNhY2hlKS5tYXAoZnVuY3Rpb24gKF9yZWY4KSB7XG4gICAgICAgICAgICAgICAgICB2YXIgX3JlZjkgPSBzbGljZWRUb0FycmF5KF9yZWY4LCAyKSxcbiAgICAgICAgICAgICAgICAgICAgICBwYXRobmFtZSA9IF9yZWY5WzBdLFxuICAgICAgICAgICAgICAgICAgICAgIHJlbmRlciA9IF9yZWY5WzFdLnJlbmRlcjtcblxuICAgICAgICAgICAgICAgICAgdmFyIHJlY29tcHV0ZWRNYXRjaCA9IHBhdGhuYW1lID09PSBjdXJyZW50UGF0aG5hbWUgPyBtYXRjaCB8fCBjb21wdXRlZE1hdGNoIDogbnVsbDtcblxuICAgICAgICAgICAgICAgICAgcmV0dXJuIFJlYWN0X19kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LkZyYWdtZW50LFxuICAgICAgICAgICAgICAgICAgICB7IGtleTogcGF0aG5hbWUgfSxcbiAgICAgICAgICAgICAgICAgICAgcmVuZGVyKF9leHRlbmRzKHt9LCBwcm9wcywgY29uZmlnUHJvcHMsIHtcbiAgICAgICAgICAgICAgICAgICAgICBjYWNoZUtleTogY2FjaGVLZXkgPyB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjYWNoZUtleTogY2FjaGVLZXksXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXRobmFtZTogcGF0aG5hbWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBtdWx0aXBsZTogdHJ1ZVxuICAgICAgICAgICAgICAgICAgICAgIH0gOiB1bmRlZmluZWQsXG4gICAgICAgICAgICAgICAgICAgICAga2V5OiBwYXRobmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICBtYXRjaDogcmVjb21wdXRlZE1hdGNoXG4gICAgICAgICAgICAgICAgICAgIH0pKVxuICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICApIDogcmVuZGVyU2luZ2xlKF9leHRlbmRzKHt9LCBwcm9wcywgY29uZmlnUHJvcHMpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICApXG4gICAgICAgICk7XG4gICAgICB9XG4gICAgfV0pO1xuICAgIHJldHVybiBDYWNoZVJvdXRlO1xuICB9KFJlYWN0LkNvbXBvbmVudCk7XG5cbiAgQ2FjaGVSb3V0ZS5jb21wb25lbnROYW1lID0gJ0NhY2hlUm91dGUnO1xuICBDYWNoZVJvdXRlLnByb3BUeXBlcyA9IHtcbiAgICBjb21wb25lbnQ6IFByb3BUeXBlcy5lbGVtZW50VHlwZSB8fCBQcm9wVHlwZXMuYW55LFxuICAgIHJlbmRlcjogUHJvcFR5cGVzLmZ1bmMsXG4gICAgY2hpbGRyZW46IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5mdW5jLCBQcm9wVHlwZXMubm9kZV0pLFxuICAgIGNvbXB1dGVkTWF0Y2hGb3JDYWNoZVJvdXRlOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIG11bHRpcGxlOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuYm9vbCwgUHJvcFR5cGVzLm51bWJlcl0pXG4gIH07XG4gIENhY2hlUm91dGUuZGVmYXVsdFByb3BzID0ge1xuICAgIG11bHRpcGxlOiBmYWxzZVxuICB9O1xuXG4gIGZ1bmN0aW9uIGdldEZyYWdtZW50KCkge1xuICAgIGlmIChpc0V4aXN0KFJlYWN0LkZyYWdtZW50KSkge1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uIChfcmVmKSB7XG4gICAgICAgIHZhciBjaGlsZHJlbiA9IF9yZWYuY2hpbGRyZW47XG4gICAgICAgIHJldHVybiBSZWFjdF9fZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIFJlYWN0LkZyYWdtZW50LFxuICAgICAgICAgIG51bGwsXG4gICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgKTtcbiAgICAgIH07XG4gICAgfVxuXG4gICAgaWYgKGlzRXhpc3QoUmVhY3QuUHJvcFR5cGVzKSkge1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uIChfcmVmMikge1xuICAgICAgICB2YXIgY2hpbGRyZW4gPSBfcmVmMi5jaGlsZHJlbjtcbiAgICAgICAgcmV0dXJuIFJlYWN0X19kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgbnVsbCxcbiAgICAgICAgICBjaGlsZHJlblxuICAgICAgICApO1xuICAgICAgfTtcbiAgICB9XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKF9yZWYzKSB7XG4gICAgICB2YXIgY2hpbGRyZW4gPSBfcmVmMy5jaGlsZHJlbjtcbiAgICAgIHJldHVybiBjaGlsZHJlbjtcbiAgICB9O1xuICB9XG5cbiAgdmFyIFN3aXRjaEZyYWdtZW50ID0gZ2V0RnJhZ21lbnQoKTtcbiAgU3dpdGNoRnJhZ21lbnQuZGlzcGxheU5hbWUgPSAnU3dpdGNoRnJhZ21lbnQnO1xuXG4gIHZhciBpc1VzaW5nTmV3Q29udGV4dCA9IGlzRXhpc3QocmVhY3RSb3V0ZXJEb20uX19Sb3V0ZXJDb250ZXh0KTtcblxuICB2YXIgQ2FjaGVTd2l0Y2ggPSBmdW5jdGlvbiAoX1N3aXRjaCkge1xuICAgIGluaGVyaXRzKENhY2hlU3dpdGNoLCBfU3dpdGNoKTtcblxuICAgIGZ1bmN0aW9uIENhY2hlU3dpdGNoKCkge1xuICAgICAgdmFyIF9yZWY7XG5cbiAgICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAgIGNsYXNzQ2FsbENoZWNrKHRoaXMsIENhY2hlU3dpdGNoKTtcblxuICAgICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSBwb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIChfcmVmID0gQ2FjaGVTd2l0Y2guX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihDYWNoZVN3aXRjaCkpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLmdldENvbnRleHQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChpc1VzaW5nTmV3Q29udGV4dCkge1xuICAgICAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgICAgICBsb2NhdGlvbiA9IF90aGlzJHByb3BzLmxvY2F0aW9uLFxuICAgICAgICAgICAgICBtYXRjaCA9IF90aGlzJHByb3BzLm1hdGNoO1xuXG5cbiAgICAgICAgICByZXR1cm4geyBsb2NhdGlvbjogbG9jYXRpb24sIG1hdGNoOiBtYXRjaCB9O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHZhciByb3V0ZSA9IF90aGlzLmNvbnRleHQucm91dGVyLnJvdXRlO1xuXG4gICAgICAgICAgdmFyIF9sb2NhdGlvbiA9IF90aGlzLnByb3BzLmxvY2F0aW9uIHx8IHJvdXRlLmxvY2F0aW9uO1xuXG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGxvY2F0aW9uOiBfbG9jYXRpb24sXG4gICAgICAgICAgICBtYXRjaDogcm91dGUubWF0Y2hcbiAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICB9LCBfdGVtcCksIHBvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICAgIH1cblxuICAgIGNyZWF0ZUNsYXNzKENhY2hlU3dpdGNoLCBbe1xuICAgICAga2V5OiAncmVuZGVyJyxcbiAgICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgICB3aGljaCA9IF9wcm9wcy53aGljaDtcblxuICAgICAgICB2YXIgX2dldENvbnRleHQgPSB0aGlzLmdldENvbnRleHQoKSxcbiAgICAgICAgICAgIGxvY2F0aW9uID0gX2dldENvbnRleHQubG9jYXRpb24sXG4gICAgICAgICAgICBjb250ZXh0TWF0Y2ggPSBfZ2V0Q29udGV4dC5tYXRjaDtcblxuICAgICAgICB2YXIgX19tYXRjaGVkQWxyZWFkeSA9IGZhbHNlO1xuXG4gICAgICAgIHJldHVybiBSZWFjdF9fZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIFVwZGF0YWJsZSxcbiAgICAgICAgICB7IHdoZW46IGlzTWF0Y2goY29udGV4dE1hdGNoKSB9LFxuICAgICAgICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBSZWFjdF9fZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgICBTd2l0Y2hGcmFnbWVudCxcbiAgICAgICAgICAgICAgbnVsbCxcbiAgICAgICAgICAgICAgUmVhY3RfX2RlZmF1bHQuQ2hpbGRyZW4ubWFwKGNoaWxkcmVuLCBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIGlmICghUmVhY3RfX2RlZmF1bHQuaXNWYWxpZEVsZW1lbnQoZWxlbWVudCkpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHZhciBwYXRoID0gZWxlbWVudC5wcm9wcy5wYXRoIHx8IGVsZW1lbnQucHJvcHMuZnJvbTtcbiAgICAgICAgICAgICAgICB2YXIgbWF0Y2ggPSBfX21hdGNoZWRBbHJlYWR5ID8gbnVsbCA6IHBhdGggPyByZWFjdFJvdXRlckRvbS5tYXRjaFBhdGgobG9jYXRpb24ucGF0aG5hbWUsIF9leHRlbmRzKHt9LCBlbGVtZW50LnByb3BzLCB7XG4gICAgICAgICAgICAgICAgICBwYXRoOiBwYXRoXG4gICAgICAgICAgICAgICAgfSksIGNvbnRleHRNYXRjaCkgOiBjb250ZXh0TWF0Y2g7XG5cbiAgICAgICAgICAgICAgICB2YXIgY2hpbGQgPSB2b2lkIDA7XG5cbiAgICAgICAgICAgICAgICBpZiAod2hpY2goZWxlbWVudCkpIHtcbiAgICAgICAgICAgICAgICAgIGNoaWxkID0gUmVhY3RfX2RlZmF1bHQuY2xvbmVFbGVtZW50KGVsZW1lbnQsIF9leHRlbmRzKHtcbiAgICAgICAgICAgICAgICAgICAgbG9jYXRpb246IGxvY2F0aW9uLFxuICAgICAgICAgICAgICAgICAgICBjb21wdXRlZE1hdGNoOiBtYXRjaFxuICAgICAgICAgICAgICAgICAgfSwgaXNOdWxsKG1hdGNoKSA/IHtcbiAgICAgICAgICAgICAgICAgICAgY29tcHV0ZWRNYXRjaEZvckNhY2hlUm91dGU6IGRlZmluZVByb3BlcnR5KHt9LCBDT01QVVRFRF9VTk1BVENIX0tFWSwgdHJ1ZSlcbiAgICAgICAgICAgICAgICAgIH0gOiBudWxsKSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIGNoaWxkID0gbWF0Y2ggJiYgIV9fbWF0Y2hlZEFscmVhZHkgPyBSZWFjdF9fZGVmYXVsdC5jbG9uZUVsZW1lbnQoZWxlbWVudCwge1xuICAgICAgICAgICAgICAgICAgICBsb2NhdGlvbjogbG9jYXRpb24sXG4gICAgICAgICAgICAgICAgICAgIGNvbXB1dGVkTWF0Y2g6IG1hdGNoXG4gICAgICAgICAgICAgICAgICB9KSA6IG51bGw7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKCFfX21hdGNoZWRBbHJlYWR5KSB7XG4gICAgICAgICAgICAgICAgICBfX21hdGNoZWRBbHJlYWR5ID0gISFtYXRjaDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gY2hpbGQ7XG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICApO1xuICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9XSk7XG4gICAgcmV0dXJuIENhY2hlU3dpdGNoO1xuICB9KHJlYWN0Um91dGVyRG9tLlN3aXRjaCk7XG5cbiAgaWYgKGlzVXNpbmdOZXdDb250ZXh0KSB7XG4gICAgQ2FjaGVTd2l0Y2gucHJvcFR5cGVzID0ge1xuICAgICAgY2hpbGRyZW46IFByb3BUeXBlcy5ub2RlLFxuICAgICAgbG9jYXRpb246IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICAgIG1hdGNoOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgICB3aGljaDogUHJvcFR5cGVzLmZ1bmNcbiAgICB9O1xuXG4gICAgQ2FjaGVTd2l0Y2ggPSByZWFjdFJvdXRlckRvbS53aXRoUm91dGVyKENhY2hlU3dpdGNoKTtcbiAgfSBlbHNlIHtcbiAgICBDYWNoZVN3aXRjaC5jb250ZXh0VHlwZXMgPSB7XG4gICAgICByb3V0ZXI6IFByb3BUeXBlcy5zaGFwZSh7XG4gICAgICAgIHJvdXRlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWRcbiAgICAgIH0pLmlzUmVxdWlyZWRcbiAgICB9O1xuXG4gICAgQ2FjaGVTd2l0Y2gucHJvcFR5cGVzID0ge1xuICAgICAgY2hpbGRyZW46IFByb3BUeXBlcy5ub2RlLFxuICAgICAgbG9jYXRpb246IFByb3BUeXBlcy5vYmplY3QsXG4gICAgICB3aGljaDogUHJvcFR5cGVzLmZ1bmNcbiAgICB9O1xuICB9XG5cbiAgQ2FjaGVTd2l0Y2guZGVmYXVsdFByb3BzID0ge1xuICAgIHdoaWNoOiBmdW5jdGlvbiB3aGljaChlbGVtZW50KSB7XG4gICAgICByZXR1cm4gZ2V0KGVsZW1lbnQsICd0eXBlJykgPT09IENhY2hlUm91dGU7XG4gICAgfVxuICB9O1xuXG4gIHZhciBDYWNoZVN3aXRjaCQxID0gQ2FjaGVTd2l0Y2g7XG5cbiAgZXhwb3J0cy5kZWZhdWx0ID0gQ2FjaGVSb3V0ZTtcbiAgZXhwb3J0cy5DYWNoZVJvdXRlID0gQ2FjaGVSb3V0ZTtcbiAgZXhwb3J0cy5DYWNoZVN3aXRjaCA9IENhY2hlU3dpdGNoJDE7XG4gIGV4cG9ydHMuZHJvcEJ5Q2FjaGVLZXkgPSBkcm9wQnlDYWNoZUtleTtcbiAgZXhwb3J0cy5nZXRDYWNoaW5nS2V5cyA9IGdldENhY2hpbmdLZXlzO1xuICBleHBvcnRzLmNsZWFyQ2FjaGUgPSBjbGVhckNhY2hlO1xuICBleHBvcnRzLmdldENhY2hpbmdDb21wb25lbnRzID0gZ2V0Q2FjaGluZ0NvbXBvbmVudHM7XG5cbiAgT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcblxufSkpKTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUdBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQURBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUlBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTs7Ozs7OztBQUtBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWRBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7QUFUQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFFQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUF0QkE7QUF3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbEJBO0FBb0JBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTVCQTtBQThCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBRUE7QUFHQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFPQTtBQWpDQTtBQW1DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBSUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUNBO0FBUUE7QUFDQTtBQUlBO0FBRUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUVBO0FBRUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBUEE7QUFVQTtBQUVBO0FBakdBO0FBb0dBO0FBaEpBO0FBa0pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBRUE7QUFFQTtBQUFBO0FBRUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBdkRBO0FBeURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBRUE7QSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/react-router-cache-route/dist/cacheRoute.js
