

var util = __webpack_require__(/*! ./util */ "./node_modules/dom-scroll-into-view/lib/util.js");

function scrollIntoView(elem, container, config) {
  config = config || {}; // document 归一化到 window

  if (container.nodeType === 9) {
    container = util.getWindow(container);
  }

  var allowHorizontalScroll = config.allowHorizontalScroll;
  var onlyScrollIfNeeded = config.onlyScrollIfNeeded;
  var alignWithTop = config.alignWithTop;
  var alignWithLeft = config.alignWithLeft;
  var offsetTop = config.offsetTop || 0;
  var offsetLeft = config.offsetLeft || 0;
  var offsetBottom = config.offsetBottom || 0;
  var offsetRight = config.offsetRight || 0;
  allowHorizontalScroll = allowHorizontalScroll === undefined ? true : allowHorizontalScroll;
  var isWin = util.isWindow(container);
  var elemOffset = util.offset(elem);
  var eh = util.outerHeight(elem);
  var ew = util.outerWidth(elem);
  var containerOffset = undefined;
  var ch = undefined;
  var cw = undefined;
  var containerScroll = undefined;
  var diffTop = undefined;
  var diffBottom = undefined;
  var win = undefined;
  var winScroll = undefined;
  var ww = undefined;
  var wh = undefined;

  if (isWin) {
    win = container;
    wh = util.height(win);
    ww = util.width(win);
    winScroll = {
      left: util.scrollLeft(win),
      top: util.scrollTop(win)
    }; // elem 相对 container 可视视窗的距离

    diffTop = {
      left: elemOffset.left - winScroll.left - offsetLeft,
      top: elemOffset.top - winScroll.top - offsetTop
    };
    diffBottom = {
      left: elemOffset.left + ew - (winScroll.left + ww) + offsetRight,
      top: elemOffset.top + eh - (winScroll.top + wh) + offsetBottom
    };
    containerScroll = winScroll;
  } else {
    containerOffset = util.offset(container);
    ch = container.clientHeight;
    cw = container.clientWidth;
    containerScroll = {
      left: container.scrollLeft,
      top: container.scrollTop
    }; // elem 相对 container 可视视窗的距离
    // 注意边框, offset 是边框到根节点

    diffTop = {
      left: elemOffset.left - (containerOffset.left + (parseFloat(util.css(container, 'borderLeftWidth')) || 0)) - offsetLeft,
      top: elemOffset.top - (containerOffset.top + (parseFloat(util.css(container, 'borderTopWidth')) || 0)) - offsetTop
    };
    diffBottom = {
      left: elemOffset.left + ew - (containerOffset.left + cw + (parseFloat(util.css(container, 'borderRightWidth')) || 0)) + offsetRight,
      top: elemOffset.top + eh - (containerOffset.top + ch + (parseFloat(util.css(container, 'borderBottomWidth')) || 0)) + offsetBottom
    };
  }

  if (diffTop.top < 0 || diffBottom.top > 0) {
    // 强制向上
    if (alignWithTop === true) {
      util.scrollTop(container, containerScroll.top + diffTop.top);
    } else if (alignWithTop === false) {
      util.scrollTop(container, containerScroll.top + diffBottom.top);
    } else {
      // 自动调整
      if (diffTop.top < 0) {
        util.scrollTop(container, containerScroll.top + diffTop.top);
      } else {
        util.scrollTop(container, containerScroll.top + diffBottom.top);
      }
    }
  } else {
    if (!onlyScrollIfNeeded) {
      alignWithTop = alignWithTop === undefined ? true : !!alignWithTop;

      if (alignWithTop) {
        util.scrollTop(container, containerScroll.top + diffTop.top);
      } else {
        util.scrollTop(container, containerScroll.top + diffBottom.top);
      }
    }
  }

  if (allowHorizontalScroll) {
    if (diffTop.left < 0 || diffBottom.left > 0) {
      // 强制向上
      if (alignWithLeft === true) {
        util.scrollLeft(container, containerScroll.left + diffTop.left);
      } else if (alignWithLeft === false) {
        util.scrollLeft(container, containerScroll.left + diffBottom.left);
      } else {
        // 自动调整
        if (diffTop.left < 0) {
          util.scrollLeft(container, containerScroll.left + diffTop.left);
        } else {
          util.scrollLeft(container, containerScroll.left + diffBottom.left);
        }
      }
    } else {
      if (!onlyScrollIfNeeded) {
        alignWithLeft = alignWithLeft === undefined ? true : !!alignWithLeft;

        if (alignWithLeft) {
          util.scrollLeft(container, containerScroll.left + diffTop.left);
        } else {
          util.scrollLeft(container, containerScroll.left + diffBottom.left);
        }
      }
    }
  }
}

module.exports = scrollIntoView;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZG9tLXNjcm9sbC1pbnRvLXZpZXcvbGliL2RvbS1zY3JvbGwtaW50by12aWV3LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZG9tLXNjcm9sbC1pbnRvLXZpZXcvbGliL2RvbS1zY3JvbGwtaW50by12aWV3LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxudmFyIHV0aWwgPSByZXF1aXJlKCcuL3V0aWwnKTtcblxuZnVuY3Rpb24gc2Nyb2xsSW50b1ZpZXcoZWxlbSwgY29udGFpbmVyLCBjb25maWcpIHtcbiAgY29uZmlnID0gY29uZmlnIHx8IHt9O1xuICAvLyBkb2N1bWVudCDlvZLkuIDljJbliLAgd2luZG93XG4gIGlmIChjb250YWluZXIubm9kZVR5cGUgPT09IDkpIHtcbiAgICBjb250YWluZXIgPSB1dGlsLmdldFdpbmRvdyhjb250YWluZXIpO1xuICB9XG5cbiAgdmFyIGFsbG93SG9yaXpvbnRhbFNjcm9sbCA9IGNvbmZpZy5hbGxvd0hvcml6b250YWxTY3JvbGw7XG4gIHZhciBvbmx5U2Nyb2xsSWZOZWVkZWQgPSBjb25maWcub25seVNjcm9sbElmTmVlZGVkO1xuICB2YXIgYWxpZ25XaXRoVG9wID0gY29uZmlnLmFsaWduV2l0aFRvcDtcbiAgdmFyIGFsaWduV2l0aExlZnQgPSBjb25maWcuYWxpZ25XaXRoTGVmdDtcbiAgdmFyIG9mZnNldFRvcCA9IGNvbmZpZy5vZmZzZXRUb3AgfHwgMDtcbiAgdmFyIG9mZnNldExlZnQgPSBjb25maWcub2Zmc2V0TGVmdCB8fCAwO1xuICB2YXIgb2Zmc2V0Qm90dG9tID0gY29uZmlnLm9mZnNldEJvdHRvbSB8fCAwO1xuICB2YXIgb2Zmc2V0UmlnaHQgPSBjb25maWcub2Zmc2V0UmlnaHQgfHwgMDtcblxuICBhbGxvd0hvcml6b250YWxTY3JvbGwgPSBhbGxvd0hvcml6b250YWxTY3JvbGwgPT09IHVuZGVmaW5lZCA/IHRydWUgOiBhbGxvd0hvcml6b250YWxTY3JvbGw7XG5cbiAgdmFyIGlzV2luID0gdXRpbC5pc1dpbmRvdyhjb250YWluZXIpO1xuICB2YXIgZWxlbU9mZnNldCA9IHV0aWwub2Zmc2V0KGVsZW0pO1xuICB2YXIgZWggPSB1dGlsLm91dGVySGVpZ2h0KGVsZW0pO1xuICB2YXIgZXcgPSB1dGlsLm91dGVyV2lkdGgoZWxlbSk7XG4gIHZhciBjb250YWluZXJPZmZzZXQgPSB1bmRlZmluZWQ7XG4gIHZhciBjaCA9IHVuZGVmaW5lZDtcbiAgdmFyIGN3ID0gdW5kZWZpbmVkO1xuICB2YXIgY29udGFpbmVyU2Nyb2xsID0gdW5kZWZpbmVkO1xuICB2YXIgZGlmZlRvcCA9IHVuZGVmaW5lZDtcbiAgdmFyIGRpZmZCb3R0b20gPSB1bmRlZmluZWQ7XG4gIHZhciB3aW4gPSB1bmRlZmluZWQ7XG4gIHZhciB3aW5TY3JvbGwgPSB1bmRlZmluZWQ7XG4gIHZhciB3dyA9IHVuZGVmaW5lZDtcbiAgdmFyIHdoID0gdW5kZWZpbmVkO1xuXG4gIGlmIChpc1dpbikge1xuICAgIHdpbiA9IGNvbnRhaW5lcjtcbiAgICB3aCA9IHV0aWwuaGVpZ2h0KHdpbik7XG4gICAgd3cgPSB1dGlsLndpZHRoKHdpbik7XG4gICAgd2luU2Nyb2xsID0ge1xuICAgICAgbGVmdDogdXRpbC5zY3JvbGxMZWZ0KHdpbiksXG4gICAgICB0b3A6IHV0aWwuc2Nyb2xsVG9wKHdpbilcbiAgICB9O1xuICAgIC8vIGVsZW0g55u45a+5IGNvbnRhaW5lciDlj6/op4bop4bnqpfnmoTot53nprtcbiAgICBkaWZmVG9wID0ge1xuICAgICAgbGVmdDogZWxlbU9mZnNldC5sZWZ0IC0gd2luU2Nyb2xsLmxlZnQgLSBvZmZzZXRMZWZ0LFxuICAgICAgdG9wOiBlbGVtT2Zmc2V0LnRvcCAtIHdpblNjcm9sbC50b3AgLSBvZmZzZXRUb3BcbiAgICB9O1xuICAgIGRpZmZCb3R0b20gPSB7XG4gICAgICBsZWZ0OiBlbGVtT2Zmc2V0LmxlZnQgKyBldyAtICh3aW5TY3JvbGwubGVmdCArIHd3KSArIG9mZnNldFJpZ2h0LFxuICAgICAgdG9wOiBlbGVtT2Zmc2V0LnRvcCArIGVoIC0gKHdpblNjcm9sbC50b3AgKyB3aCkgKyBvZmZzZXRCb3R0b21cbiAgICB9O1xuICAgIGNvbnRhaW5lclNjcm9sbCA9IHdpblNjcm9sbDtcbiAgfSBlbHNlIHtcbiAgICBjb250YWluZXJPZmZzZXQgPSB1dGlsLm9mZnNldChjb250YWluZXIpO1xuICAgIGNoID0gY29udGFpbmVyLmNsaWVudEhlaWdodDtcbiAgICBjdyA9IGNvbnRhaW5lci5jbGllbnRXaWR0aDtcbiAgICBjb250YWluZXJTY3JvbGwgPSB7XG4gICAgICBsZWZ0OiBjb250YWluZXIuc2Nyb2xsTGVmdCxcbiAgICAgIHRvcDogY29udGFpbmVyLnNjcm9sbFRvcFxuICAgIH07XG4gICAgLy8gZWxlbSDnm7jlr7kgY29udGFpbmVyIOWPr+inhuinhueql+eahOi3neemu1xuICAgIC8vIOazqOaEj+i+ueahhiwgb2Zmc2V0IOaYr+i+ueahhuWIsOagueiKgueCuVxuICAgIGRpZmZUb3AgPSB7XG4gICAgICBsZWZ0OiBlbGVtT2Zmc2V0LmxlZnQgLSAoY29udGFpbmVyT2Zmc2V0LmxlZnQgKyAocGFyc2VGbG9hdCh1dGlsLmNzcyhjb250YWluZXIsICdib3JkZXJMZWZ0V2lkdGgnKSkgfHwgMCkpIC0gb2Zmc2V0TGVmdCxcbiAgICAgIHRvcDogZWxlbU9mZnNldC50b3AgLSAoY29udGFpbmVyT2Zmc2V0LnRvcCArIChwYXJzZUZsb2F0KHV0aWwuY3NzKGNvbnRhaW5lciwgJ2JvcmRlclRvcFdpZHRoJykpIHx8IDApKSAtIG9mZnNldFRvcFxuICAgIH07XG4gICAgZGlmZkJvdHRvbSA9IHtcbiAgICAgIGxlZnQ6IGVsZW1PZmZzZXQubGVmdCArIGV3IC0gKGNvbnRhaW5lck9mZnNldC5sZWZ0ICsgY3cgKyAocGFyc2VGbG9hdCh1dGlsLmNzcyhjb250YWluZXIsICdib3JkZXJSaWdodFdpZHRoJykpIHx8IDApKSArIG9mZnNldFJpZ2h0LFxuICAgICAgdG9wOiBlbGVtT2Zmc2V0LnRvcCArIGVoIC0gKGNvbnRhaW5lck9mZnNldC50b3AgKyBjaCArIChwYXJzZUZsb2F0KHV0aWwuY3NzKGNvbnRhaW5lciwgJ2JvcmRlckJvdHRvbVdpZHRoJykpIHx8IDApKSArIG9mZnNldEJvdHRvbVxuICAgIH07XG4gIH1cblxuICBpZiAoZGlmZlRvcC50b3AgPCAwIHx8IGRpZmZCb3R0b20udG9wID4gMCkge1xuICAgIC8vIOW8uuWItuWQkeS4ilxuICAgIGlmIChhbGlnbldpdGhUb3AgPT09IHRydWUpIHtcbiAgICAgIHV0aWwuc2Nyb2xsVG9wKGNvbnRhaW5lciwgY29udGFpbmVyU2Nyb2xsLnRvcCArIGRpZmZUb3AudG9wKTtcbiAgICB9IGVsc2UgaWYgKGFsaWduV2l0aFRvcCA9PT0gZmFsc2UpIHtcbiAgICAgIHV0aWwuc2Nyb2xsVG9wKGNvbnRhaW5lciwgY29udGFpbmVyU2Nyb2xsLnRvcCArIGRpZmZCb3R0b20udG9wKTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8g6Ieq5Yqo6LCD5pW0XG4gICAgICBpZiAoZGlmZlRvcC50b3AgPCAwKSB7XG4gICAgICAgIHV0aWwuc2Nyb2xsVG9wKGNvbnRhaW5lciwgY29udGFpbmVyU2Nyb2xsLnRvcCArIGRpZmZUb3AudG9wKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHV0aWwuc2Nyb2xsVG9wKGNvbnRhaW5lciwgY29udGFpbmVyU2Nyb2xsLnRvcCArIGRpZmZCb3R0b20udG9wKTtcbiAgICAgIH1cbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgaWYgKCFvbmx5U2Nyb2xsSWZOZWVkZWQpIHtcbiAgICAgIGFsaWduV2l0aFRvcCA9IGFsaWduV2l0aFRvcCA9PT0gdW5kZWZpbmVkID8gdHJ1ZSA6ICEhYWxpZ25XaXRoVG9wO1xuICAgICAgaWYgKGFsaWduV2l0aFRvcCkge1xuICAgICAgICB1dGlsLnNjcm9sbFRvcChjb250YWluZXIsIGNvbnRhaW5lclNjcm9sbC50b3AgKyBkaWZmVG9wLnRvcCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB1dGlsLnNjcm9sbFRvcChjb250YWluZXIsIGNvbnRhaW5lclNjcm9sbC50b3AgKyBkaWZmQm90dG9tLnRvcCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaWYgKGFsbG93SG9yaXpvbnRhbFNjcm9sbCkge1xuICAgIGlmIChkaWZmVG9wLmxlZnQgPCAwIHx8IGRpZmZCb3R0b20ubGVmdCA+IDApIHtcbiAgICAgIC8vIOW8uuWItuWQkeS4ilxuICAgICAgaWYgKGFsaWduV2l0aExlZnQgPT09IHRydWUpIHtcbiAgICAgICAgdXRpbC5zY3JvbGxMZWZ0KGNvbnRhaW5lciwgY29udGFpbmVyU2Nyb2xsLmxlZnQgKyBkaWZmVG9wLmxlZnQpO1xuICAgICAgfSBlbHNlIGlmIChhbGlnbldpdGhMZWZ0ID09PSBmYWxzZSkge1xuICAgICAgICB1dGlsLnNjcm9sbExlZnQoY29udGFpbmVyLCBjb250YWluZXJTY3JvbGwubGVmdCArIGRpZmZCb3R0b20ubGVmdCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyDoh6rliqjosIPmlbRcbiAgICAgICAgaWYgKGRpZmZUb3AubGVmdCA8IDApIHtcbiAgICAgICAgICB1dGlsLnNjcm9sbExlZnQoY29udGFpbmVyLCBjb250YWluZXJTY3JvbGwubGVmdCArIGRpZmZUb3AubGVmdCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdXRpbC5zY3JvbGxMZWZ0KGNvbnRhaW5lciwgY29udGFpbmVyU2Nyb2xsLmxlZnQgKyBkaWZmQm90dG9tLmxlZnQpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmICghb25seVNjcm9sbElmTmVlZGVkKSB7XG4gICAgICAgIGFsaWduV2l0aExlZnQgPSBhbGlnbldpdGhMZWZ0ID09PSB1bmRlZmluZWQgPyB0cnVlIDogISFhbGlnbldpdGhMZWZ0O1xuICAgICAgICBpZiAoYWxpZ25XaXRoTGVmdCkge1xuICAgICAgICAgIHV0aWwuc2Nyb2xsTGVmdChjb250YWluZXIsIGNvbnRhaW5lclNjcm9sbC5sZWZ0ICsgZGlmZlRvcC5sZWZ0KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB1dGlsLnNjcm9sbExlZnQoY29udGFpbmVyLCBjb250YWluZXJTY3JvbGwubGVmdCArIGRpZmZCb3R0b20ubGVmdCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBzY3JvbGxJbnRvVmlldzsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/dom-scroll-into-view/lib/dom-scroll-into-view.js
