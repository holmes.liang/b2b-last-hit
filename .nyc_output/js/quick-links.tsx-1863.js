__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react-router */ "./node_modules/react-router/esm/react-router.js");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/npanel-show/quick-links.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n              .quick-links {\n                display: flex;\n                -webkit-box-pack: space-evenly;\n                -webkit-justify-content: space-evenly;\n                -ms-flex-pack: space-evenly;\n                justify-content: space-evenly;\n                align-items: center;\n              }\n              i {\n                font-size: 18px;\n                padding-right: 8px;\n                font-weight: 500;\n                display: inline-block;\n                vertical-align: middle;\n                &.fw800 {\n                    font-weight: 800;\n                }\n              }\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}










var QuickLinks =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(QuickLinks, _ModelWidget);

  function QuickLinks(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, QuickLinks);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(QuickLinks).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(QuickLinks, [{
    key: "render",
    value: function render() {
      var C = this.getComponents();
      var model = this.props.model;

      var allowToViewClaims = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "opers.allowToViewClaims");

      var allowToViewBcpTransactions = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "opers.allowToViewBcpTransactions");

      var allowToViewEndorsements = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "opers.allowToViewEndorsements");

      var allowToViewRIDetails = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "opers.allowToViewRIDetails");

      var numOfEndorsements = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "numOfEndorsements");

      var numOfClaims = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "numOfClaims");

      var isNpanel = allowToViewClaims || allowToViewBcpTransactions || allowToViewEndorsements || allowToViewRIDetails;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.QuickLinks, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        },
        __self: this
      }, isNpanel && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NCollapse"], {
        defaultActiveKey: ["quick-link"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPanel"], {
        key: "quick-link",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Quick Links").thai("Quick Links").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "quick-links",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, allowToViewRIDetails && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("a", {
        onClick: function onClick() {
          window.open(_common__WEBPACK_IMPORTED_MODULE_12__["PATH"].RI_ENQUIRY_VIEW.replace(":policyId", lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "policyId")));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
        type: "insurance",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        },
        __self: this
      }), "Reinsurance"), allowToViewEndorsements && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("a", {
        onClick: function onClick() {
          window.open("".concat(_common__WEBPACK_IMPORTED_MODULE_12__["PATH"].ENDORSEMENTS_QUERY, "?currentTab=all"));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("i", {
        className: "iconfont icon-endo fw800",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        },
        __self: this
      }), "Endorsements (", numOfEndorsements, ")"), allowToViewBcpTransactions && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("a", {
        onClick: function onClick() {
          window.open("".concat(_common__WEBPACK_IMPORTED_MODULE_12__["PATH"].BCP_COLLECTION));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
        type: "pay-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61
        },
        __self: this
      }), "Finance"), allowToViewClaims && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("a", {
        onClick: function onClick() {
          window.open("".concat(_common__WEBPACK_IMPORTED_MODULE_12__["PATH"].CLAIMS_HANDLING, "?currentTab=all"));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
        type: "solution",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 67
        },
        __self: this
      }), "Claims (", numOfClaims, ")")))));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        QuickLinks: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(QuickLinks.prototype), "initState", this).call(this), {});
    }
  }]);

  return QuickLinks;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router__WEBPACK_IMPORTED_MODULE_14__["withRouter"])(QuickLinks));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L25wYW5lbC1zaG93L3F1aWNrLWxpbmtzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9ucGFuZWwtc2hvdy9xdWljay1saW5rcy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXRQcm9wcywgU3R5bGVkRElWIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgTkNvbGxhcHNlLCBOUGFuZWwgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IExhbmd1YWdlLCBQQVRIIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IEljb24gfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgUm91dGVDb21wb25lbnRQcm9wcywgd2l0aFJvdXRlciB9IGZyb20gXCJyZWFjdC1yb3V0ZXJcIjtcblxuZXhwb3J0IHR5cGUgUXVpY2tMaW5rc1Byb3BzID0ge1xuICAgIG1vZGVsOiBhbnk7XG59ICYgTW9kZWxXaWRnZXRQcm9wcyAmIFJvdXRlQ29tcG9uZW50UHJvcHM7XG5cbmV4cG9ydCB0eXBlIFF1aWNrTGlua3NTdGF0ZSA9IHt9O1xuZXhwb3J0IHR5cGUgU3R5bGVkRElWID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHt9LCBuZXZlcj47XG5cbmV4cG9ydCB0eXBlIFF1aWNrTGlua3NDb21wb25lbnRzID0ge1xuICAgIFF1aWNrTGlua3M6IFN0eWxlZERJVjtcbn07XG5cbmNsYXNzIFF1aWNrTGlua3M8UCBleHRlbmRzIFF1aWNrTGlua3NQcm9wcywgUyBleHRlbmRzIFF1aWNrTGlua3NTdGF0ZSwgQyBleHRlbmRzIFF1aWNrTGlua3NDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wczogUXVpY2tMaW5rc1Byb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgICB9XG5cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICAgICAgY29uc3Qge21vZGVsfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IGFsbG93VG9WaWV3Q2xhaW1zID0gXy5nZXQobW9kZWwsIFwib3BlcnMuYWxsb3dUb1ZpZXdDbGFpbXNcIik7XG4gICAgICAgIGNvbnN0IGFsbG93VG9WaWV3QmNwVHJhbnNhY3Rpb25zID0gXy5nZXQobW9kZWwsIFwib3BlcnMuYWxsb3dUb1ZpZXdCY3BUcmFuc2FjdGlvbnNcIik7XG4gICAgICAgIGNvbnN0IGFsbG93VG9WaWV3RW5kb3JzZW1lbnRzID0gXy5nZXQobW9kZWwsIFwib3BlcnMuYWxsb3dUb1ZpZXdFbmRvcnNlbWVudHNcIik7XG4gICAgICAgIGNvbnN0IGFsbG93VG9WaWV3UklEZXRhaWxzID0gXy5nZXQobW9kZWwsIFwib3BlcnMuYWxsb3dUb1ZpZXdSSURldGFpbHNcIik7XG4gICAgICAgIGNvbnN0IG51bU9mRW5kb3JzZW1lbnRzID0gXy5nZXQobW9kZWwsIFwibnVtT2ZFbmRvcnNlbWVudHNcIik7XG4gICAgICAgIGNvbnN0IG51bU9mQ2xhaW1zID0gXy5nZXQobW9kZWwsIFwibnVtT2ZDbGFpbXNcIik7XG4gICAgICAgIGNvbnN0IGlzTnBhbmVsID0gYWxsb3dUb1ZpZXdDbGFpbXMgfHwgYWxsb3dUb1ZpZXdCY3BUcmFuc2FjdGlvbnMgfHwgYWxsb3dUb1ZpZXdFbmRvcnNlbWVudHMgfHwgYWxsb3dUb1ZpZXdSSURldGFpbHM7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8Qy5RdWlja0xpbmtzPlxuICAgICAgICAgICAgICAgIHtpc05wYW5lbCAmJiA8TkNvbGxhcHNlIGRlZmF1bHRBY3RpdmVLZXk9e1tcInF1aWNrLWxpbmtcIl19PlxuICAgICAgICAgICAgICAgICAgICA8TlBhbmVsXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk9XCJxdWljay1saW5rXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlYWRlcj17TGFuZ3VhZ2UuZW4oXCJRdWljayBMaW5rc1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwiUXVpY2sgTGlua3NcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtcInF1aWNrLWxpbmtzXCJ9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHthbGxvd1RvVmlld1JJRGV0YWlscyAmJiA8YSBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5vcGVuKFBBVEguUklfRU5RVUlSWV9WSUVXLnJlcGxhY2UoXCI6cG9saWN5SWRcIiwgXy5nZXQobW9kZWwsIFwicG9saWN5SWRcIikpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT17XCJpbnN1cmFuY2VcIn0vPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWluc3VyYW5jZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT59XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge2FsbG93VG9WaWV3RW5kb3JzZW1lbnRzICYmIDxhIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93Lm9wZW4oYCR7UEFUSC5FTkRPUlNFTUVOVFNfUVVFUll9P2N1cnJlbnRUYWI9YWxsYCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT17XCJpY29uZm9udCBpY29uLWVuZG8gZnc4MDBcIn0vPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBFbmRvcnNlbWVudHMgKHtudW1PZkVuZG9yc2VtZW50c30pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7YWxsb3dUb1ZpZXdCY3BUcmFuc2FjdGlvbnMgJiYgPGEgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cub3BlbihgJHtQQVRILkJDUF9DT0xMRUNUSU9OfWApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SWNvbiB0eXBlPXtcInBheS1jaXJjbGVcIn0vPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBGaW5hbmNlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7YWxsb3dUb1ZpZXdDbGFpbXMgJiYgPGEgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cub3BlbihgJHtQQVRILkNMQUlNU19IQU5ETElOR30/Y3VycmVudFRhYj1hbGxgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT17XCJzb2x1dGlvblwifS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIENsYWltcyAoe251bU9mQ2xhaW1zfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+fVxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvTlBhbmVsPlxuICAgICAgICAgICAgICAgIDwvTkNvbGxhcHNlPn1cbiAgICAgICAgICAgIDwvQy5RdWlja0xpbmtzPlxuICAgICAgICApO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIFF1aWNrTGlua3M6IFN0eWxlZC5kaXZgXG4gICAgICAgICAgICAgIC5xdWljay1saW5rcyB7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAtd2Via2l0LWJveC1wYWNrOiBzcGFjZS1ldmVubHk7XG4gICAgICAgICAgICAgICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICAgICAgICAgICAgICAtbXMtZmxleC1wYWNrOiBzcGFjZS1ldmVubHk7XG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpIHtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1yaWdodDogOHB4O1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgICAgICAgICAgICAgJi5mdzgwMCB7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBgLFxuICAgICAgICB9IGFzIEM7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHt9KSBhcyBTO1xuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFJvdXRlcihRdWlja0xpbmtzKTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBc0JBOzs7QUFFQTtBQUNBO0FBQ0E7Ozs7QUFsRkE7QUFDQTtBQW9GQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/npanel-show/quick-links.tsx
